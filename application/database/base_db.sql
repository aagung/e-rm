-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Jun 2019 pada 08.05
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imedis_erm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl_resources`
--

CREATE TABLE `acl_resources` (
  `id` int(11) NOT NULL,
  `uid` varchar(38) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('module','controller','action','other') NOT NULL DEFAULT 'other',
  `parent` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `acl_resources`
--

INSERT INTO `acl_resources` (`id`, `uid`, `name`, `type`, `parent`, `status`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(2, '66dc9df1-b06d-11e6-ac37-201a06a403a4', 'auth', 'module', NULL, 1, '2012-11-12 04:00:23', NULL, NULL, NULL),
(3, '66dc9eb2-b06d-11e6-ac37-201a06a403a4', 'auth/login', 'controller', 2, 1, '2012-11-12 12:43:42', NULL, '2012-11-12 12:44:06', NULL),
(4, '66dc9f21-b06d-11e6-ac37-201a06a403a4', 'auth/logout', 'controller', 2, 1, '2012-11-12 12:43:56', NULL, NULL, NULL),
(5, '66dc9f7b-b06d-11e6-ac37-201a06a403a4', 'auth/user', 'controller', 2, 1, '2012-11-12 04:07:59', NULL, '2012-11-12 08:29:29', NULL),
(6, '66dc9fd5-b06d-11e6-ac37-201a06a403a4', 'acl', 'module', NULL, 1, '2012-02-02 13:47:43', NULL, NULL, NULL),
(7, '66dca077-b06d-11e6-ac37-201a06a403a4', 'acl/resource', 'controller', 6, 1, '2012-02-02 13:47:57', NULL, NULL, NULL),
(8, '66dca0c8-b06d-11e6-ac37-201a06a403a4', 'acl/resource/index', 'action', 7, 1, '2012-02-02 13:48:21', NULL, NULL, NULL),
(9, '66dca138-b06d-11e6-ac37-201a06a403a4', 'acl/resource/add', 'action', 7, 1, '2012-02-02 13:48:35', NULL, '2012-10-16 17:26:12', NULL),
(10, '66dca18d-b06d-11e6-ac37-201a06a403a4', 'acl/resource/edit', 'action', 7, 1, '2012-02-02 13:48:50', NULL, '2012-07-09 18:44:38', NULL),
(11, '66dca1eb-b06d-11e6-ac37-201a06a403a4', 'acl/resource/delete', 'action', 7, 1, '2012-02-02 13:49:06', NULL, NULL, NULL),
(12, '66dca23c-b06d-11e6-ac37-201a06a403a4', 'acl/role', 'controller', 6, 1, '2012-07-12 17:54:16', NULL, NULL, NULL),
(13, '66dca28e-b06d-11e6-ac37-201a06a403a4', 'acl/role/index', 'action', 12, 1, '2012-07-12 17:55:29', NULL, NULL, NULL),
(14, '66dca2db-b06d-11e6-ac37-201a06a403a4', 'acl/role/add', 'action', 12, 1, '2012-07-12 17:56:00', NULL, NULL, NULL),
(15, '66dca31f-b06d-11e6-ac37-201a06a403a4', 'acl/role/edit', 'action', 12, 1, '2012-07-12 17:56:19', NULL, NULL, NULL),
(16, '66dca368-b06d-11e6-ac37-201a06a403a4', 'acl/role/delete', 'action', 12, 1, '2012-07-12 17:56:55', NULL, NULL, NULL),
(17, '66dca3b5-b06d-11e6-ac37-201a06a403a4', 'acl/rule', 'controller', 6, 1, '2012-07-12 17:53:04', NULL, NULL, NULL),
(18, '66dca406-b06d-11e6-ac37-201a06a403a4', 'acl/rule/edit', 'action', 17, 1, '2012-07-12 17:53:25', NULL, NULL, NULL),
(19, '66dca44f-b06d-11e6-ac37-201a06a403a4', 'utils', 'module', NULL, 1, NULL, NULL, NULL, NULL),
(20, '66dca4a0-b06d-11e6-ac37-201a06a403a4', 'dashboard', 'module', NULL, 1, NULL, NULL, NULL, NULL),
(21, '66dca4e4-b06d-11e6-ac37-201a06a403a4', 'api', 'module', NULL, 1, NULL, NULL, NULL, NULL),
(22, '66dca520-b06d-11e6-ac37-201a06a403a4', 'samples', 'module', NULL, 1, NULL, NULL, NULL, NULL),
(24, 'ab4a5d5f-66ba-11e7-a1d7-4ccc6acb7c99', 'auth/user/profile', 'controller', 2, 1, '2017-07-12 11:29:18', 529, NULL, NULL),
(25, 'c5c40bbc-bfa6-11e8-96b3-a5a85ac9e318', 'home', 'controller', NULL, 1, '2018-09-24 10:06:04', 1, '2018-09-24 10:11:04', 1),
(26, '3b330938-bfa8-11e8-96b3-a5a85ac9e318', 'master', 'module', NULL, 1, '2018-09-24 10:16:30', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl_roles`
--

CREATE TABLE `acl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `acl_roles`
--

INSERT INTO `acl_roles` (`id`, `name`, `status`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(1, 'Administrator', 1, '2011-12-27 12:00:00', NULL, NULL, NULL),
(2, 'Pegawai', 1, '2011-12-27 12:00:00', NULL, '2018-09-24 10:05:40', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl_role_parents`
--

CREATE TABLE `acl_role_parents` (
  `role_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl_rules`
--

CREATE TABLE `acl_rules` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'deny',
  `priviledge` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `acl_rules`
--

INSERT INTO `acl_rules` (`role_id`, `resource_id`, `access`, `priviledge`) VALUES
(2, 2, 'allow', NULL),
(2, 3, 'allow', NULL),
(2, 4, 'allow', NULL),
(2, 24, 'allow', NULL),
(2, 25, 'allow', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_autologin`
--

CREATE TABLE `auth_autologin` (
  `user` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_users`
--

CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `uid` char(36) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `registered` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `ip` text,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `auth_users`
--

INSERT INTO `auth_users` (`id`, `uid`, `first_name`, `last_name`, `username`, `email`, `password`, `lang`, `registered`, `role_id`, `ip`, `status`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(1, 'b443a6d4-00da-11e8-9597-a29a1ec78a61', 'Admin', 'iMedis', 'admin', 'admin@vmt.co.id', '$2a$08$Um/mGEbQiuNElLTiNhnObOlu0ThUU8gBQdDFPC5vtonpv.gxDO8Su', 'id', '2017-07-10 08:17:09', 1, '*', 1, '2017-07-10 08:17:09', 510, '2019-02-19 11:50:30', 1),
(2, '49fd3d62-97c2-11e9-b5f5-80c5f2b26678', 'Agung', 'Budi Santoso', 'agggungs', 'santosobudiagung9@gmail.com', '$2a$08$3I8ZKLquXmdtKXm9/qTBKOUL2xCFZpxwoFpGzVg6Hg5ZRnVYDJgei', 'id', '2019-06-26 10:27:13', 2, '', 1, '2019-06-26 10:27:13', 1, NULL, NULL);

--
-- Trigger `auth_users`
--
DELIMITER $$
CREATE TRIGGER `before_insert_auth_users` BEFORE INSERT ON `auth_users` FOR EACH ROW BEGIN
  IF new.uid IS NULL THEN
    SET new.uid = UUID();
  END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_users_roles`
--

CREATE TABLE `auth_users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `auth_users_roles`
--

INSERT INTO `auth_users_roles` (`id`, `user_id`, `role_id`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(764, 1, 1, NULL, 0, NULL, 0),
(774, 2, 2, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('tbljvfh5t8bu4m87cebc7b5q3tnt0c1c', '::1', 1561519685, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536313531393434393b617574685f757365727c733a313a2231223b617574685f6c6f67676564696e7c623a313b6c616e677c733a323a226964223b726f6c655f69647c733a313a2231223b726f6c655f6e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b6c6173745f6e616d657c733a363a22694d65646973223b69647c733a313a2231223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e40766d742e636f2e6964223b726f6c65737c613a313a7b693a303b4f3a383a22737464436c617373223a323a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261746f72223b7d7d706567617761695f69647c4e3b);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(20170119190000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations_backup`
--

CREATE TABLE `migrations_backup` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `migrations_backup`
--

INSERT INTO `migrations_backup` (`version`) VALUES
(20161223100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_options`
--

CREATE TABLE `sys_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `option_table` varchar(255) DEFAULT NULL,
  `option_type` varchar(20) DEFAULT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  `user_visibility` tinyint(1) DEFAULT '0' COMMENT '// Digunakan apakah option tersebut bisa di edit user ato tidak'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sys_options`
--

INSERT INTO `sys_options` (`option_id`, `option_name`, `option_value`, `option_table`, `option_type`, `autoload`, `user_visibility`) VALUES
(1, 'rs_logo', 'assets/img/logo/ananda.png', '', 'text', 'yes', 0),
(2, 'rs_logo_only', 'assets/img/logo/imedis.png', NULL, 'text', 'yes', 0),
(3, 'rs_logo_without_text', 'assets/img/logo/ananda_text_empty.png', NULL, 'text', 'yes', 0),
(4, 'tp_pendaftaran_baru', '9', '', 'text', 'yes', 0),
(5, 'tp_pendaftaran_ulang', '4', NULL, 'text', 'yes', 0),
(6, 'set_metode_harga_barang', 'max', NULL, 'text', 'yes', 0),
(7, 'rs_biaya_r', '500', '', 'text', 'yes', NULL),
(8, 'rs_alamat', 'Jl. Sultan Agung No. 173', '', 'text', 'yes', 1),
(9, 'rs_nama', 'Rumah Sakit Ananda Bekasi', '', 'text', 'yes', 1),
(10, 'rs_kota', 'Medan Satria, Bekasi', '', 'text', 'yes', 1),
(11, 'rs_telp', '021-085XXX', '', 'text', 'yes', 1),
(12, 'rs_kodepos', '40275', '', 'text', 'yes', 1),
(13, 'rs_direktur', 'Nama Direktur', '', 'text', 'yes', 1),
(14, 'rs_ka_farmasi', 'Nama Ka. Farmasi	', NULL, 'text', 'yes', 1),
(15, 'default_kelas', '5', '', 'text', 'yes', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `acl_resources`
--
ALTER TABLE `acl_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `acl_roles`
--
ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD PRIMARY KEY (`role_id`,`parent`),
  ADD KEY `acl_role_parents_parent` (`parent`);

--
-- Indeks untuk tabel `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`role_id`,`resource_id`),
  ADD KEY `acl_rules_resource_id` (`resource_id`);

--
-- Indeks untuk tabel `auth_autologin`
--
ALTER TABLE `auth_autologin`
  ADD PRIMARY KEY (`user`,`series`);

--
-- Indeks untuk tabel `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_users_username` (`username`),
  ADD KEY `auth_users_email` (`email`),
  ADD KEY `auth_users_role_id` (`role_id`);

--
-- Indeks untuk tabel `auth_users_roles`
--
ALTER TABLE `auth_users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_users_roles_auth_users_FK` (`user_id`);

--
-- Indeks untuk tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indeks untuk tabel `sys_options`
--
ALTER TABLE `sys_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `acl_resources`
--
ALTER TABLE `acl_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `acl_roles`
--
ALTER TABLE `acl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `auth_users_roles`
--
ALTER TABLE `auth_users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=775;

--
-- AUTO_INCREMENT untuk tabel `sys_options`
--
ALTER TABLE `sys_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD CONSTRAINT `acl_role_parents_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_role_parents_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_rules_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `acl_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_users`
--
ALTER TABLE `auth_users`
  ADD CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_users_roles`
--
ALTER TABLE `auth_users_roles`
  ADD CONSTRAINT `auth_users_roles_auth_users_FK` FOREIGN KEY (`user_id`) REFERENCES `auth_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
