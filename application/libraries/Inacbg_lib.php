<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 12/7/16
 * Time: 13:35
 */

use GuzzleHttp\Client;

class Inacbg_lib
{
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('inacbg_helper');
    }

    public function make_request($data)
    {
//        $key = $this->CI->config->item('inacbg')['key'];
//        $inacbg_url = $this->CI->config->item('inacbg')['url_debug'];
//
//        $client = new Client();
//        $res = $client->request('POST', $inacbg_url, [
//            'body' => "----BEGIN ENCRYPTED DATA——\n" . mc_encrypt($data, $key) . "\n----END ENCRYPTED DATA——"
//        ]);
//        return $res->getBody();

        // contoh encryption key, bukan aktual
        $key = $this->CI->config->item('inacbg')['key'];
        // json query
        $request = $data;
        // data yang akan dikirimkan dengan method POST adalah encrypted:
        $payload = mc_encrypt($request, $key);
        // tentukan Content-Type pada http header
        $header = array("Content-Type: application/x-www-form-urlencoded");
        // url server aplikasi E-Klaim,
        // silakan disesuaikan instalasi masing-masing
        $url = $this->CI->config->item('inacbg')['url'];
        // setup curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        // request dengan curl
        $response = curl_exec($ch);
//         terlebih dahulu hilangkan "----BEGIN ENCRYPTED DATA----\r\n"
//         dan hilangkan "----END ENCRYPTED DATA----\r\n" dari response
        $first = strpos($response, "\n") + 1;
        $last = strrpos($response, "\n") - 1;
        $response = substr($response,
            $first,
            strlen($response) - $first - $last);

// decrypt dengan fungsi mc_decrypt
        $response = mc_decrypt($response, $key);
        // hasil decrypt adalah format json, ditranslate kedalam array
//        $msg = json_decode($response, true);
        // variable data adalah base64 dari file pdf
//        $pdf = base64_decode($msg["data"]);
        return $response;


    }

}