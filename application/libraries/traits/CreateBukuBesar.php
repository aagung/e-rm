<?php

trait CreateBukuBesar {

    public function createBukuBesar($transaksi, $posting = true) {
		if ($posting) {
			$aData = array();
			$aData['perkiraan_id']        = $transaksi['perkiraan_id'];
			$aData['transaksi_id']        = $transaksi['transaksi_id'];
			$aData['transaksi_detail_id'] = $transaksi['transaksi_detail_id'];
			$aData['tanggal']             = $transaksi['tanggal'];
			$aData['no_bukti']            = $transaksi['no_bukti'];
			$aData['uraian']              = $transaksi['uraian'];
			$aData['jumlah']              = $transaksi['jumlah'];
			$aData['jenis']               = $transaksi['jenis'];
			$this->db->insert('t_buku_besar', $aData);
		}
		else {
			$this->db->where('perkiraan_id', $transaksi['perkiraan_id']);
			$this->db->where('transaksi_id', $transaksi['transaksi_id']);
			$this->db->where('transaksi_detail_id', $transaksi['transaksi_detail_id']);
			$this->db->delete('t_buku_besar');
		}
	}

}