<?php

trait CreateKartuUtang {

    public function createKartuUtang($transaksi, $posting = true) {
		
		if ($posting) {
			$aData = array();
			$aData['supplier_id']			= $transaksi['supplier_id'];
			$aData['transaksi_id']			= $transaksi['id'];
			$aData['transaksi_detail_id']	= $transaksi['detail_id'];
			$aData['tanggal']				= $transaksi['tanggal'];
			$aData['no_bukti']				= $transaksi['no_bukti'];
			$aData['uraian']				= $transaksi['uraian'];
			$aData['jumlah']				= $transaksi['jumlah'];
			$aData['jenis']					= $transaksi['jenis'];
			$aData['created']				= date('Y-m-d H:i:s');
			$aData['created_by']			= $auth_user->id;
			$this->db->insert('utang', $aData);
		}
		else {
			$this->db->where('supplier_id', $transaksi['supplier_id']);
			$this->db->where('id', $transaksi['transaksi_id']);
			$this->db->delete('utang');
		}
	}

}