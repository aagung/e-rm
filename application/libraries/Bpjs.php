<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use GuzzleHttp\Client;

class Bpjs
{
    protected $CI;
    protected $base_url;
    protected $service_name;
    protected $consid;
    protected $secretKey;

    private $clients;
    private $headers;

    public function __construct()
    {
        // Get the instance
        $this->CI =& get_instance();

        $bpjs_config = $this->CI->config->item('bpjs');

        $this->base_url = rtrim($bpjs_config['base_url'], '/');
        $this->service_name = ltrim(rtrim($bpjs_config['service_name'], '/'), '/');
        $this->consid = $bpjs_config['consid'];
        $this->secretKey = $bpjs_config['secretkey'];

        $this->clients = new Client([
            'verify' => false,
        ]);
        $this->setHeaders();
    }

    private function setHeaders() 
    {
        $data = $this->consid;
        $secretKey = $this->secretKey;
        // Computes the timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);

        // base64 encode…
        $encodedSignature = base64_encode($signature);

        // urlencode…
        // $encodedSignature = urlencode($encodedSignature);

        $this->headers = [
            'X-cons-id' => $data,
            'X-timestamp' => $tStamp,
            'X-signature' => $encodedSignature,
        ];

        return $this;
    }

    public function get($feature) {
        $this->headers['Content-Type'] = 'application/json; charset=utf-8';

        $result = new stdClass();
        $result->status = 200;
        $result->response = null;
        try {
            $response = $this->clients->request(
                'GET',
                $this->base_url . '/' . $this->service_name . '/' . $feature,
                [
                    'headers' => $this->headers
                ]
            )->getBody()->getContents();

            $result->response = $response;
        } catch (\Exception $e) {
            $response = $e->getResponse()->getBody();

            $result->status = $e->getCode();
            $result->response = $response;
        }

        return $result;
    }

    public function post($feature, $data = [], $headers = [])
    {
        $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $result = new stdClass();
        $result->status = 200;
        $result->response = null;
        if(!empty($headers)){
            $this->headers = array_merge($this->headers,$headers);
        }
        try {
            $response = $this->clients->request(
                'POST',
                $this->base_url . '/' . $this->service_name . '/' . $feature,
                [
                    'headers' => $this->headers,
                    'json' => $data,
                ]
            )->getBody()->getContents();

            $result->response = $response;
        } catch (\Exception $e) {
            $response = $e->getResponse()->getBody();

            $result->status = $e->getCode();
            $result->response = $response;
        }

        return $result;
    }

    public function put($feature, $data = [])
    {
        $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $result = new stdClass();
        $result->status = 200;
        $result->response = null;
        try {
            $response = $this->clients->request(
                'PUT',
                $this->base_url . '/' . $this->service_name . '/' . $feature,
                [
                    'headers' => $this->headers,
                    'json' => $data,
                ]
            )->getBody()->getContents();

            $result->response = $response;
        } catch (\Exception $e) {
            $response = $e->getResponse()->getBody();

            $result->status = $e->getCode();
            $result->response = $response;
        }

        return $result;
    }

    public function delete($feature, $data = [])
    {
        $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $result = new stdClass();
        $result->status = 200;
        $result->response = null;
        try {
            $response = $this->clients->request(
                'DELETE',
                $this->base_url . '/' . $this->service_name . '/' . $feature,
                [
                    'headers' => $this->headers,
                    'json' => $data,
                ]
            )->getBody()->getContents();

            $result->response = $response;
        } catch (\Exception $e) {
            $response = $e->getResponse()->getBody();

            $result->status = $e->getCode();
            $result->response = $response;
        }

        return $result;
    }
}