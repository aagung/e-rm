<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_pdf
{
    public $CI;

    public function __construct()
    {
        // Get the instance
        $this->CI = &get_instance();
    }

    public function load($mode = '', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 10, $mgr = 10, $mgt = 40, $mgb = 40, $mgh = 9, $mgf = 5, $orientation = 'P')
    {
        require_once __DIR__.'/../../vendor/mpdf/mpdf/mpdf.php';

        return new mPDF($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
    }
}
