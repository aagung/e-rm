<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pabrik_label'] = 'Principal';
$lang['vendor_label'] = 'Distributor';
$lang['farmasi_unit_label'] = 'Farmasi Unit';
$lang['is_inventaris_label'] = 'Barang Inventaris';