<style type="text/css">
	.page-container {
		background: url("<?= base_url('assets/img/backgrounds/login/background.jpg'); ?>");
		background-size: cover;
	}

	@media (max-width: 1024px) {
		.login-logo {
			text-align: center;
		}
	}
</style>

<form role="form" method="post">
	<div class="panel panel-body login-form">
		<div class="text-center">
			<h4 class="content-group" style="color: #10575e;"><span style="border-bottom: 2px solid #2fb7c5;">Login</span></h4>
			<h6 class="no-margin-bottom text-muted text-bold"><span>Welcome Back</span></h6>
			<p class="content-group text-muted"><span>Login to access application</span></p>
		</div>
		
		<?php echo messages(); ?>

		<div class="form-group has-feedback has-feedback-left">
			<input type="text" class="form-control input input-rounded login-input" style="text-transform: lowercase !important;" placeholder="<?php echo lang('username'); ?>" name="username" autofocus value="<?php echo (isset($username)) ? $username : ''; ?>">
			<div class="form-control-feedback">
				<i class="icon-user text-muted" style="color: #2fb7c5;"></i>
			</div>
		</div>
		
		<div class="form-group has-feedback has-feedback-left mt-20">
			<input type="password" class="form-control input-rounded login-input" placeholder="<?php echo lang('password'); ?>" name="password"  value="" />
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted" style="color: #2fb7c5;"></i>
			</div>
		</div>

		<div class="form-group" style="margin-top: 40px;">
			<button  name="login-button" type="submit" class="btn btn-rounded btn-block login-input" style="background-color: #f58634; color: white;"><i class="icon-key position-left"></i><?php echo lang('login'); ?> </button>
		</div>

		<!-- <div class="form-group text-center">
			<a href="<?php echo site_url('layar/layar'); ?>" target="_blank" class="btn btn-link"><i class="icon-screen3"></i></a>
		</div> -->

	</div>
</form>
