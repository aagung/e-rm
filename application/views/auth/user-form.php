<?php echo messages(); ?>
<div class="row">
	<div class="col-md-12">
		<form id="user-form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo lang('account'); ?></h3>
				</div>
				<div class="panel-body">
					<?php echo $form->fields(); ?>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" name="save-button" value="Simpan" id="save-button" class="btn-success btn-labeled btn">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<input type="submit" name="cancel-button" value="Batal" id="cancel-button" class="btn btn-default">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	let isAdmin = true;
    $(document).ready(function() {
        
    });
        
	$(window).load(function() {
        var typeahead_elem = $("#first_name");
            
        $('input[type=text], input[type=email]').css("text-transform", "none");
		$('#top-save-btn').on('click', function(e) {
			e.preventDefault();
			$('#save-button').click();
		});

        $("#roles_id").change(function (e) {
            var role_id = $("#role_id").val();
            var options = [];
            $(this).find(':selected').each(function (i, el) {
                options.push({ id: $(el).val(), text: $(el).text() });
            });

            // Delete Options in role_id
            $("#role_id option").each(function (i, el) {
                if (i > 0) {
                    $(el).remove();
                }
            });

            for (var i = 0; i < options.length; i++) {
                $("#role_id").append('<option value="' + options[i].id + '">' + options[i].text + '</option>');
            }

            if (role_id > 0 && $("#role_id option[value=" + role_id + "]").length > 0) {
                $("#role_id").val(role_id);
            } else {
                role_id = options.length > 0 ? options[0].id : 0;
                $("#role_id").val(role_id);
            }

            $("#role_id").trigger('change');
        });

        $("#role_id").change(function (e) {
			isAdmin = false;
			let role = $(this).find('option:selected').text();
			if(role.search(/administrator/i) !== -1) isAdmin = true;
		});
	});
</script>