
<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>Register</th>
			<th>MR</th>
			<th>Nama Pasien</th>
			<th>Jenis Kelamin</th>
			<th>Tanggal Daftar</th>
			<th>Tanggal In</th>
			<th>Jam In</th>
			<th>Baru / Lama</th>
			<th>DPJP Utama</th>
			<th>DPJP Lainnya</th>
			<th>Diagnosa Awal</th>
			<th>Kamar</th>
			<th>Kelas</th>
			<th>Jaminan</th>
			<th>Hari Rawat</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					$poli_vendor = $row->poli;
                    $poli_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->register; ?></td>
			<td><?php echo $row->mr; ?></td>
			<td><?php echo $row->nama_pasien; ?></td>
			<td><?php echo $row->jenis_kelamin; ?></td>
			<td><?php echo $row->tanggal_daftar; ?></td>
			<td><?php echo $row->tanggal_in; ?></td>
			<td><?php echo $row->jam_in; ?></td>
			<td><?php echo $row->baru_lama; ?></td>
			<td><?php echo $row->dpjp_utama; ?></td>
			<td><?php echo $row->dpjp_lainnya; ?></td>
			<td><?php echo $row->diagnosa_awal; ?></td>
			<td><?php echo $row->kamar; ?></td>
			<td><?php echo $row->kelas; ?></td>
			<td><?php echo $row->jaminan; ?></td>
			<td><?php echo $row->hari_rawat; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<tr>
			<td style="font-weight: bold;text-align: right;" colspan="8">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="10">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>