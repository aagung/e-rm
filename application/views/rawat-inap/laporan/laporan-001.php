<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_pasien_dirawat_terkini">Pasien Dirawat Terkini</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('jaminan_label'); ?>Jaminan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">All</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('ruang_label'); ?>Ruang</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-ruang">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">All</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('dpjp_label'); ?>DPJP</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-dpjp">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">All</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('kelas_label'); ?>Kelas</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-kelas">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">All</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('dpjp_lain_label'); ?>DPJP Lain</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-dpjp_lain">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">All</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>Register</th>
				<th>MR</th>
				<th>Nama Pasien</th>
				<th>Jenis Kelamin</th>
				<th>Tanggal Daftar</th>
				<th>Tanggal In</th>
				<th>Jam In</th>
				<th>Baru / Lama</th>
				<th>DPJP Utama</th>
				<th>DPJP Lainnya</th>
				<th>Diagnosa Awal</th>
				<th>Kamar</th>
				<th>Kelas</th>
				<th>Jaminan</th>
				<th>Hari Rawat</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_inap/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.ruang_id = $('#search-ruang').val();
              	p.kelas_id = $('#search-kelas').val();
            }
		},
		 "columns": [
	      	{ "data": "register" },
	      	{ "data": "mr" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "jenis_kelamin" },
	      	{ "data": "tanggal_daftar" },
	      	{ "data": "tanggal_in" },
	      	{ "data": "jam_in" },
	      	{ "data": "baru_lama" },
	      	{ "data": "dpjp_utama" },
	      	{ "data": "dpjp_lainnya" },
	      	{ "data": "diagnosa_awal" },
	      	{ "data": "kamar" },
	      	{ "data": "kelas" },
	      	{ "data": "jaminan" },
	      	{ "data": "hari_rawat" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let ruang_id = $('#search-ruang').val();
      	let kelas_id = $('#search-kelas').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&kelas_id=${kelas_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_inap/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let ruang_id = $('#search-ruang').val();
      	let kelas_id = $('#search-kelas').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&kelas_id=${kelas_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_inap/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>