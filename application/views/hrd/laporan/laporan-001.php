<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_informasi_karyawan">Informasi Karyawan</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Pegawai 
				<!-- <?php echo lang('nama_pegawai_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-2">Unit Kerja <!-- <?php echo lang('unit_kerja_label'); ?> --> </label>
			<div class="col-md-10">    
                <select class="form-control input-search" id="search-unit_kerja">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">NIK 
				<!-- <?php echo lang('nik_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-2">Bagian <!-- <?php echo lang('bagian_label'); ?> --> </label>
			<div class="col-md-10">    
                <select class="form-control input-search" id="search-bagian">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-1 col-md-offset-11">
		<button type="reset" class="btn btn-secondary reset-button">
            Reset
        </button>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>No</th>
				<th>NIK</th>
				<th>Nama Pegawai</th>
				<th>Alamat</th>
				<th>Telp / HP</th>
				<th>Tempat Lahir</th>
				<th>Tanggal Lahir</th>
				<th>Umur</th>
				<th>Unit</th>
				<th>Bagian</th>
				<th>Tanggal Masuk</th>
				<th>Masa Kerja</th>
				<th>No KTP</th>
				<th>No BPJS Ket</th>
				<th>No BPJS Kes</th>
				<th>No Asuransi</th>
				<th>Pendidikan Terakhir</th>
				<th>Jabatan</th>
				<th>Status Pegawai</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="19">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/hrd/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
            }
		},
		 "columns": [
	      	{ "data": "no" },
	      	{ "data": "nik" },
	      	{ "data": "nama_pegawai" },
	      	{ "data": "alamat" },
	      	{ "data": "telp_hp" },
	      	{ "data": "tempat_lahir" },
	      	{ "data": "tanggal_lahir" },
	      	{ "data": "umur" },
	      	{ "data": "unit" },
	      	{ "data": "bagian" },
	      	{ "data": "tanggal_masuk" },
	      	{ "data": "masa_kerja" },
	      	{ "data": "no_ktp" },
	      	{ "data": "no_bpjs_ket" },
	      	{ "data": "no_bpjs_kes" },
	      	{ "data": "no_asuransi" },
	      	{ "data": "pendidikan_terakhir" },
	      	{ "data": "jabatan" },
	      	{ "data": "status_pegawai" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let nama_pegawai_id = $('#search-asal-pasien').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&nama_pegawai_id=${nama_pegawai_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/hrd/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let nama_pegawai_id = $('#search-asal-pasien').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&nama_pegawai_id=${nama_pegawai_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/hrd/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>