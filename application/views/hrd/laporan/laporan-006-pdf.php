<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th rowspan="2">NO</th>
			<th rowspan="2">NIK</th>
			<th rowspan="2">NAMA PEGAWAI</th>
			<th rowspan="2">UNIT KERJA</th>
			<th rowspan="2">BAGIAN</th>
			<th rowspan="2">JABATAN</th>
			<th rowspan="2">TANGGAL MASUK</th>
			<th colspan="12">ABSENSI KETERLAMBATAN KARYAWAN(DALAM MENIT) TERAMBIL</th>
			<th rowspan="2">TOTAL KETERLAMBATAN</th>
			<th rowspan="2">TIDAK MASUK KERJA</th>
			<th rowspan="2">JENIS CUTI</th>
			<th rowspan="2">HAK CUTI</th>
			<th colspan="12">CUTI TAHUNAN TERAMBIL</th>
			<th rowspan="2">SISA CUTI TAHUNAN</th>
		</tr>
		<tr>
			<!-- ABSENSI KETERLAMBATAN KARYAWAN(DALAM MENIT) TERAMBIL -->
			<th>JAN</th>
			<th>FEB</th>
			<th>MAR</th>
			<th>APR</th>
			<th>MEI</th>
			<th>JUN</th>
			<th>JUL</th>
			<th>AGU</th>
			<th>SEP</th>
			<th>OKT</th>
			<th>NOV</th>
			<th>DES</th>

			<!-- CUTI TAHUNAN TERAMBIL -->
			<th>JAN</th>
			<th>FEB</th>
			<th>MAR</th>
			<th>APR</th>
			<th>MEI</th>
			<th>JUN</th>
			<th>JUL</th>
			<th>AGU</th>
			<th>SEP</th>
			<th>OKT</th>
			<th>NOV</th>
			<th>DES</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					// $pabrik_vendor = $row->pabrik;
     				// $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					// $grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->nik; ?></td>
			<td><?php echo $row->nama_pegawai; ?></td>
			<td><?php echo $row->unit_kerja; ?></td>
			<td><?php echo $row->bagian; ?></td>
			<td><?php echo $row->jabatan; ?></td>
			<td><?php echo $row->tanggal_masuk; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_januari; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_febuari; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_maret; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_april; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_mei; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_juni; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_juli; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_agustus; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_september; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_oktober; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_november; ?></td>
			<td><?php echo $row->absensi_keterlambatan_karyawan_desember; ?></td>
			<td><?php echo $row->total_keterlambatan; ?></td>
			<td><?php echo $row->tidak_masuk_kerja; ?></td>
			<td><?php echo $row->jenis_cuti; ?></td>
			<td><?php echo $row->hak_cuti; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_januari; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_febuari; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_maret; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_april; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_mei; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_juni; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_juli; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_agustus; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_september; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_oktober; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_november; ?></td>
			<td><?php echo $row->cuti_tahunan_terambil_desember; ?></td>
			<td><?php echo $row->sisa_cuti_tahunan; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<tr>
			<td style="font-weight: bold;text-align: right;" colspan="36">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="36">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>