<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>NO</th>
			<th>NIK</th>
			<th>NAMA PEGAWAI</th>
			<th>UNIT KERJA</th>
			<th>BAGIAN</th>
			<th>JABATAN</th>
			<th>KATEGORI PENGHARGAAN</th>
			<th>NO SK</th>
			<th>TANGGAL SK</th>
			<th>BENTUK PENGHARGAAN</th>
			<th>JENIS PELATIHAN</th>
			<th>NAMA PELATIHAN</th>
			<th>TEMPAT PELATIHAN</th>
			<th>MASA BERLAKU SERTIFIKAT</th>
			<th>NILAI SKP</th>
			<th>JAM PELATIHAN</th>
			<th>PENYELENGGARA</th>
			<th>AKREDITASI DARI</th>
			<th>NO SERTIFIKAT</th>
			<th>NAMA INSTITUSI PENDIDIKAN</th>
			<th>JENIS PENDIDIKAN</th>
			<th>JURUSAN/PRODI</th>
			<th>TAHUN LULUS</th>
			<th>NO IJAZAH</th>
			<th>DIREKTUR PENDIDIKAN</th>
			<th>NO SURAT VERIFIKASI</th>
			<th>JENJANG KARIR</th>
			<th>NO SK</th>
			<th>TANGGAL SK</th>
			<th>NAMA SUAMI/ISTRI</th>
			<th>TANGGAL LAHIR SUAMI/ISTRI</th>
			<th>NAMA ANAK</th>
			<th>TANGGAL LAHIR ANAK</th>
			<th>UMUR ANAK</th>
			<th>JENIS KELAMIN ANAK</th>
			<th>JENIS PELANGGARAN</th>
			<th>DESKRIPSI PELANGGARAN</th>
			<th>SANKSI</th>
			<th>TANGGAL SP</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					// $pabrik_vendor = $row->pabrik;
     				// $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					// $grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->nik; ?></td>
			<td><?php echo $row->nama_pegawai; ?></td>
			<td><?php echo $row->unit_kerja; ?></td>
			<td><?php echo $row->bagian; ?></td>
			<td><?php echo $row->jabatan; ?></td>
			<td><?php echo $row->kategori_penghargaan; ?></td>
			<td><?php echo $row->no_sk; ?></td>
			<td><?php echo $row->tanggal_sk; ?></td>
			<td><?php echo $row->bentuk_penghargaan; ?></td>
			<td><?php echo $row->jenis_pelatihan; ?></td>
			<td><?php echo $row->nama_pelatihan; ?></td>
			<td><?php echo $row->tempat_latihan; ?></td>
			<td><?php echo $row->masa_berlaku_sertifikat; ?></td>
			<td><?php echo $row->nilai_skp; ?></td>
			<td><?php echo $row->jam_pelatihan; ?></td>
			<td><?php echo $row->penyelanggara; ?></td>
			<td><?php echo $row->akreditasi_dari; ?></td>
			<td><?php echo $row->no_sertifikat; ?></td>
			<td><?php echo $row->nama_institusi_pendidikan; ?></td>
			<td><?php echo $row->jenjang_pendidikan; ?></td>
			<td><?php echo $row->jurusan_prodi; ?></td>
			<td><?php echo $row->tahun_lulus; ?></td>
			<td><?php echo $row->no_ijazah; ?></td>
			<td><?php echo $row->direktur_pendidikan; ?></td>
			<td><?php echo $row->no_surat_verifikasi; ?></td>
			<td><?php echo $row->jenjang_karir; ?></td>
			<td><?php echo $row->no_sk_2; ?></td>
			<td><?php echo $row->tanggal_sk_2; ?></td>
			<td><?php echo $row->nama_suami_istri; ?></td>
			<td><?php echo $row->tanggal_lahir_suami_istri; ?></td>
			<td><?php echo $row->nama_anak; ?></td>
			<td><?php echo $row->tanggal_lahir_anak; ?></td>
			<td><?php echo $row->umur_anak; ?></td>
			<td><?php echo $row->jenis_kelamin_anak; ?></td>
			<td><?php echo $row->jenis_pelanggaran; ?></td>
			<td><?php echo $row->deskripsi_pelanggaran; ?></td>
			<td><?php echo $row->sanksi; ?></td>
			<td><?php echo $row->tanggal_sp; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<tr>
			<td style="font-weight: bold;text-align: right;" colspan="39">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="39">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>