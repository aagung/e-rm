<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>

 <div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Pegawai 
				<!-- <?php echo lang('nama_pegawai_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Cuti Tahunan Terambil <!-- <?php echo lang('cuti_tahunan_terambil_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-cuti_tahunan_terambil">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nik 
				<!-- <?php echo lang('nik_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Sisa Cuti Tahunan <!-- <?php echo lang('sisa_cuti_tahunan_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-sisa_cuti_tahunan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th rowspan="2">NO</th>
				<th rowspan="2">NIK</th>
				<th rowspan="2">NAMA PEGAWAI</th>
				<th rowspan="2">UNIT KERJA</th>
				<th rowspan="2">BAGIAN</th>
				<th rowspan="2">JABATAN</th>
				<th rowspan="2">TANGGAL MASUK</th>
				<th colspan="12">ABSENSI KETERLAMBATAN KARYAWAN(DALAM MENIT) TERAMBIL</th>
				<th rowspan="2">TOTAL KETERLAMBATAN</th>
				<th rowspan="2">TIDAK MASUK KERJA</th>
				<th rowspan="2">JENIS CUTI</th>
				<th rowspan="2">HAK CUTI</th>
				<th colspan="12">CUTI TAHUNAN TERAMBIL</th>
				<th rowspan="2">SISA CUTI TAHUNAN</th>
			</tr>
			<tr>
				<!-- ABSENSI KETERLAMBATAN KARYAWAN(DALAM MENIT) TERAMBIL -->
				<th>JAN</th>
				<th>FEB</th>
				<th>MAR</th>
				<th>APR</th>
				<th>MEI</th>
				<th>JUN</th>
				<th>JUL</th>
				<th>AGU</th>
				<th>SEP</th>
				<th>OKT</th>
				<th>NOV</th>
				<th>DES</th>

				<!-- CUTI TAHUNAN TERAMBIL -->
				<th>JAN</th>
				<th>FEB</th>
				<th>MAR</th>
				<th>APR</th>
				<th>MEI</th>
				<th>JUN</th>
				<th>JUL</th>
				<th>AGU</th>
				<th>SEP</th>
				<th>OKT</th>
				<th>NOV</th>
				<th>DES</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="36">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/hrd/laporan/laporan_006'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.cuti_tahunan_terambil = $('#search-cuti_tahunan_terambil').val();
                p.sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      
            			
            }
		},
		 "columns": [
	      	{ "data": "no",
	      	  "searchable":false },
	      	{ "data": "nik" },
	      	{ "data": "nama_pegawai" },
	      	{ "data": "unit_kerja" },
	      	{ "data": "bagian" },
	      	{ "data": "jabatan" },
	      	{ "data": "tanggal_masuk",
	      	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_januari",
      	  	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_febuari",
      	  	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_maret",
      	  	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_april",
	      	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_mei",
      	  	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_juni",
	      	  "searchable":false },
	      	{ "data": "absensi_keterlambatan_karyawan_juli",
	      	  "searchable":false },
      	  	{ "data": "absensi_keterlambatan_karyawan_agustus",
      	  	  "searchable":false },
      	  	{ "data": "absensi_keterlambatan_karyawan_september",
      	  	  "searchable":false },
      	  	{ "data": "absensi_keterlambatan_karyawan_oktober",
      	      "searchable":false },
      	  	{ "data": "absensi_keterlambatan_karyawan_november",
      	      "searchable":false },
      	  	{ "data": "absensi_keterlambatan_karyawan_desember",
      	      "searchable":false },
      	 	{ "data": "total_keterlambatan",
      	      "searchable":false },
      	  	{ "data": "tidak_masuk_kerja",
      	      "searchable":false },
      	  	{ "data": "jenis_cuti",
      	      "searchable":false },
      	  	{ "data": "hak_cuti",
	      	  "searchable":false },
      	    { "data": "cuti_tahunan_terambil_januari",
      	  	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_febuari",
      	  	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_maret",
      	  	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_april",
	      	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_mei",
      	  	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_juni",
	      	  "searchable":false },
	      	{ "data": "cuti_tahunan_terambil_juli",
	      	  "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil_agustus",
      	  	  "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil_september",
      	  	  "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil_oktober",
      	      "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil_november",
      	      "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil_desember",
      	      "searchable":false },
      	    { "data": "sisa_cuti_tahunan",
	      	  "searchable":false },
      	   	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.punishment ? ` / ${row.punishment}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let cuti_tahunan_terambil = $('#search-punishment').val();
      	let sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&cuti_tahunan_terambil=${cuti_tahunan_terambil}&sisa_cuti_tahunan=${sisa_cuti_tahunan}`;
		window.location.assign(`<?php echo site_url('api/hrd/laporan/print_006'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let cuti_tahunan_terambil = $('#search-punishment').val();
      	let sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&cuti_tahunan_terambil=${cuti_tahunan_terambil}&sisa_cuti_tahunan=${sisa_cuti_tahunan}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/hrd/laporan/print_006'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>