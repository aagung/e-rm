<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Pegawai 
				<!-- <?php echo lang('nama_pegawai_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Punishment <!-- <?php echo lang('punishment_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-punishment">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Unit Kerja <!-- <?php echo lang('unit_kerja_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-unit_kerja">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Nik 
				<!-- <?php echo lang('nik_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Masa Berlaku Punishment <!-- <?php echo lang('masa_berlaku_punishment_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-masa_berlaku_punishment">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label col-md-4">Bagian <!-- <?php echo lang('bagian_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-bagian">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>NIK</th>
				<th>NAMA PEGAWAI</th>
				<th>ALAMAT</th>
				<th>TELP/HP</th>
				<th>TEMPAT LAHIR</th>
				<th>TANGGAL LAHIR</th>
				<th>UMUR</th>
				<th>UNIT KERJA</th>
				<th>BAGIAN</th>
				<th>JABATAN</th>
				<th>TANGGAL MASUK</th>
				<th>MASA KERJA</th>
				<th>PERKIRAAN TAHUN PENSIUN</th>
				<th>NO KTP</th>
				<th>NO STR</th>
				<th>MASA BERLAKU STR</th>
				<th>NO BPJS KESEHATAN</th>
				<th>NO BPJS KETENAGAKERJAAN</th>
				<th>PENDIDIKAN TERKAHIR</th>
				<th>PUNISHMENT</th>
				<th>MASA BERLAKU PUNISHMENT</th>
				<th>STATUS PEGAWAI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="23">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/hrd/laporan/laporan_002'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.punishment_id = $('#search-punishment').val();
              	p.masa_berlaku_punishment = $('#search-masa_berlaku_punishment').val();
              	p.unit_kerja_id = $('#search-unit_kerja').val();
                p.bagian_id = $('#search-bagian').val();
      
            			
            }
		},
		 "columns": [
	      	{ "data": "no",
	      	  "searchable":false },
	      	{ "data": "nik" },
	      	{ "data": "nama_pegawai" },
	      	{ "data": "alamat" },
	      	{ "data": "telepon_hp" },
	      	{ "data": "tempat_lahir",
	      	  "searchable":false },
	      	{ "data": "tanggal_lahir" },
	      	{ "data": "umur",
	      	  "searchable":false},
	      	{ "data": "unit_kerja",
	      	  "searchable":false},
	      	{ "data": "bagian",
	      	  "searchable":false },
	      	{ "data": "jabatan",
	      	  "searchable":false },
	      	{ "data": "tanggal_masuk",
	      	  "searchable":false },
	      	{ "data": "masa_kerja",
	      	  "searchable":false  },
	      	{ "data": "perkiraan_tahun_pensiun",
	      	  "searchable":false },
	      	{ "data": "no_ktp",
	      	  "searchable":false },
	      	{ "data": "no_str",
	      	  "searchable":false  },
	      	{ "data": "masa_berlaku_str",
	      	  "searchable":false  },
	      	{ "data": "no_bpjs_kesehatan",
	      	  "searchable":false  },
	      	{ "data": "no_bpjs_ketenagakerjaan",
	      	  "searchable":false  },
	      	{ "data": "pendidikan_terakhir",
	      	  "searchable":false  },
	      	{ "data": "punishment" },
	      	{ "data": "masa_berlaku_punishment",
	      	  "searchable":false  },
	      	{ "data": "status_pegawai",
	      	  "searchable":false  },
	      	
	      	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.punishment ? ` / ${row.punishment}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let punishment_id = $('#search-punishment').val();
      	let masa_berlaku_punishment = $('#search-masa_berlaku_punishment').val();
      	let unit_kerja_id = $('#search-unit_kerja').val();
      	let bagian_id = $('#search-bagian').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&punishment_id=${punishment_id}&masa_berlaku_punishment=${masa_berlaku_punishment}&unit_kerja_id=${unit_kerja_id}&bagian_id=${bagian_id}`;
		window.location.assign(`<?php echo site_url('api/hrd/laporan/print_002'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let punishment_id = $('#search-punishment').val();
      	let masa_berlaku_punishment = $('#search-masa_berlaku_punishment').val();
      	let unit_kerja_id = $('#search-unit_kerja').val();
      	let bagian_id = $('#search-bagian').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&punishment_id=${punishment_id}&masa_berlaku_punishment=${masa_berlaku_punishment}&unit_kerja_id=${unit_kerja_id}&bagian_id=${bagian_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/hrd/laporan/print_002'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>