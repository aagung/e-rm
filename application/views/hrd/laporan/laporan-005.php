<hr>
<!-- <div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
 -->
 <div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Pegawai 
				<!-- <?php echo lang('nama_pegawai_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Cuti Tahunan Terambil <!-- <?php echo lang('cuti_tahunan_terambil_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-cuti_tahunan_terambil">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nik 
				<!-- <?php echo lang('nik_label'); ?> --> 
			</label>
			<div class="col-md-8">    
                <input type="text" class="form-control" name="">
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Sisa Cuti Tahunan <!-- <?php echo lang('sisa_cuti_tahunan_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-sisa_cuti_tahunan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>NIK</th>
				<th>NAMA PEGAWAI</th>
				<th>TANGGAL MASUK</th>
				<th>JANUARI</th>
				<th>FEBUARI</th>
				<th>MARET</th>
				<th>APRIL</th>
				<th>MEI</th>
				<th>JUNI</th>
				<th>JULI</th>
				<th>AGUSTUS</th>
				<th>SEPTEMBER</th>
				<th>OKTOBER</th>
				<th>NOVEMBER</th>
				<th>DESEMBER</th>
				<th>CUTI BERAMBIL</th>
				<th>CUTI TAHUNAN TERAMBIL</th>
				<th>SISA CUTI TAHUNAN</th>
				<th>STATUS</th>
				<th>BAGIAN</th>
				<th>JABATAN</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="22">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/hrd/laporan/laporan_005'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.cuti_tahunan_terambil = $('#search-cuti_tahunan_terambil').val();
                p.sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      
            			
            }
		},
		 "columns": [
	      	{ "data": "no",
	      	  "searchable":false },
	      	{ "data": "nik" },
	      	{ "data": "nama_pegawai" },
	      	{ "data": "tanggal_masuk" },
	      	{ "data": "januari",
      	  	  "searchable":false },
	      	{ "data": "febuari",
      	  	  "searchable":false },
	      	{ "data": "maret",
      	  	  "searchable":false },
	      	{ "data": "april",
	      	  "searchable":false },
	      	{ "data": "mei",
      	  	  "searchable":false },
	      	{ "data": "juni",
	      	  "searchable":false },
	      	{ "data": "juli",
	      	  "searchable":false },
      	  	{ "data": "agustus",
      	  	  "searchable":false },
      	  	{ "data": "september",
      	  	  "searchable":false },
      	  	{ "data": "oktober",
      	      "searchable":false },
      	  	{ "data": "november",
      	      "searchable":false },
      	  	{ "data": "desember",
      	      "searchable":false },
      	 	{ "data": "cuti_berambil",
      	      "searchable":false },
      	  	{ "data": "cuti_tahunan_terambil",
      	      "searchable":false },
      	  	{ "data": "sisa_cuti_tahunan",
      	      "searchable":false },
      	  	{ "data": "status" },
      	    { "data": "bagian" },
      	    { "data": "jabatan" },
      	  	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.punishment ? ` / ${row.punishment}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let cuti_tahunan_terambil = $('#search-cuti_tahunan_terambil').val();
      	let sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&cuti_tahunan_terambil=${cuti_tahunan_terambil}&sisa_cuti_tahunan=${sisa_cuti_tahunan}`;
		window.location.assign(`<?php echo site_url('api/hrd/laporan/print_005'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let cuti_tahunan_terambil = $('#search-punishment').val();
      	let sisa_cuti_tahunan = $('#search-sisa_cuti_tahunan').val();
      	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&cuti_tahunan_terambil=${cuti_tahunan_terambil}&sisa_cuti_tahunan=${sisa_cuti_tahunan}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/hrd/laporan/print_005'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>