<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<center>
                        <table border="1">
                            <thead>
                                <tr>
                                    <th colspan="2" height="20px">
                                    	<center>
                                    		<b>CATATAN ANASTESI</b>
                                    	</center>
                                    </th>
                                </tr>
                                <tr>
                                    <th height="20px">
                                    	<center>
                                    		<b>RM-20 / Rev:1 / VI / 09</b>
                                    	</center>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </center>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-12">Jenis Anastesi :</label>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="umum" value="ya">Umum
								</label>
                       		</div>
						</div>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="regional" value="ya">Regional
								</label>
                       		</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Jam Mulai Anastesi</label>
						<div class="col-md-10">
							<input type="time" class="form-control" name="jam_mulai_anastesi">
						</div>
						<label class="control-label col-md-2">Jam Selesai Anastesi :</label>
						<div class="col-md-10">
							<input type="time" class="form-control" name="jam_selesai_anastesi">
						</div>
						<label class="control-label col-md-2">Lama Anastesi</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="lama_anastesi">
						</div>
						<label class="control-label col-md-2">Lama Operasi</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="lama_operasi">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Keadaan Prabedah :</label>
						<label class="control-label col-md-2">TB</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="tb">
						</div>
						<label class="control-label col-md-1">Cm.</label>
						<label class="control-label col-md-2">BB</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="bb">
						</div>
						<label class="control-label col-md-1">Kg.</label>
						<label class="control-label col-md-2">Golongan Darah :</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="golongan_darah">
						</div>
						<label class="control-label col-md-2">Rh. :</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="rh">
						</div>
						<label class="control-label col-md-2">TD :</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="td">
						</div>
						<label class="control-label col-md-2">Nadi :</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="nadi">
						</div>
						<label class="control-label col-md-2">Temp. :</label>
						<div class="col-md-2">
							<input type="text" class="form-control" name="temp">
						</div>
						<label class="control-label col-md-2">Hb. :</label>
						<div class="col-md-2">
							<input type="text" class="form-control" name="hb">
						</div>
						<label class="control-label col-md-2">Ht. :</label>
						<div class="col-md-2">
							<input type="text" class="form-control" name="ht">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Sirkulasi :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="sirkulasi" rows="3" cols="2"></textarea>
                       	</div>
                       	<label class="control-label col-md-12">Respirasi :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="respirasi" rows="3" cols="2"></textarea>
                       	</div>
						<label class="control-label col-md-12">Saraf :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="saraf" rows="3" cols="2"></textarea>
                       	</div>
						<label class="control-label col-md-12">Gastro-Intestinal :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="gastro_intestinal" rows="3" cols="2"></textarea>
                       	</div>
						<label class="control-label col-md-12">Renal :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="renal" rows="3" cols="2"></textarea>
                       	</div>						
                       	<label class="control-label col-md-12">Metabolik :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="metabolik" rows="3" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Status Fisik : ASA 1 2 3 4 5E</label>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="asa_1" value="ya">ASA 1 Pasien normal yang sehat
								</label>
                       		</div>
						</div>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="asa_2" value="ya">ASA 2 Pasien dengan penyakit sistemik ringan
								</label>
                       		</div>
						</div>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="asa_3" value="ya">ASA 3 Pasien dengan penyakit sistemik berat
								</label>
                       		</div>
						</div>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="asa_4" value="ya">ASA 4 Pasien dengan penyakit sistemik berat mengancam nyawa
								</label>
                       		</div>
						</div>
						<div class="form-group">
							<div class="col-md-11 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="emergency" value="ya">EMERGENCY
								</label>
                       		</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Terapi Pra Bedah :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="terapi_pra_bedah" rows="3" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Premedikasi :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="premedikasi" rows="3" cols="2"></textarea>
                       	</div>
                       	<label class="control-label col-md-12">Pemberian : SC / I.M / I.V / P.ORAL</label>
                       	<label class="control-label col-md-2">Waktu : Jam</label>
						<div class="col-md-10">
							<input type="time" class="form-control" name="waktu_jam">
						</div>
						<label class="control-label col-md-2">Efek :</label>
						<div class="col-md-10">
                           	<textarea class="form-control" name="efek" rows="2" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Keadaan psikhe sesudah premedikasi :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="keadaan_psikhe_sesudah_premedikasi" rows="3" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Anastesi dengan :</label>
						<label class="control-label col-md-2">- Inhalasi :</label>
						<div class="col-md-10">
                           	<textarea class="form-control" name="inhalasi" rows="2" cols="2"></textarea>
                       	</div>
                       	<label class="control-label col-md-2">- Im / iv :</label>
						<div class="col-md-10">
                           	<textarea class="form-control" name="im_iv" rows="2" cols="2"></textarea>
                       	</div>
                       	<label class="control-label col-md-2">- Lain-lain :</label>
						<div class="col-md-10">
                           	<textarea class="form-control" name="lain_lain" rows="2" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Relaxasi dengan :</label>
						<div class="form-group">
							<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="tehnik_anestesi" value="ya">Tehnik Anestesi
								</label>
                       		</div>
                       		<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="face_mask" value="ya">Face Mask
								</label>
                       		</div>
                       		<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="intubasi" value="ya">Intubasi
								</label>
                       		</div>
						</div>
						<div class="col-md-12">
                           	<textarea class="form-control" name="freetext" rows="4" cols="2"></textarea>
                       	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Respirasi :</label>
						<div class="form-group">
							<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="spontan" value="ya">Spontan
								</label>
                       		</div>
                       		<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="controlled" value="ya">Controlled
								</label>
                       		</div>
                       		<div class="col-md-3 col-md-offset-1">
                           		<label class="checkbox-inline">
									<input type="checkbox" class="styled" name="assisted" value="ya">Assisted
								</label>
                       		</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-12">Posisi :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="posisi" rows="3" cols="2"></textarea>
                       	</div>
                       	<label class="control-label col-md-12">Infus :</label>
						<div class="col-md-12">
                           	<textarea class="form-control" name="infus" rows="3" cols="2"></textarea>
                       	</div>
					</div>
				</div>
			</div>						
		</div>
	</div>
</form>