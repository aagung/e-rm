<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Asal Pasien</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-asal_pasien">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Shift</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-shift">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Dokter</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-dokter">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. REGISTER</th>
				<th>NO. MR</th>
				<th>TGL DATANG</th>
				<th>JAM DATANG</th>
				<th>NAMA PASIEN</th>
				<th>ICD10</th>
				<th>DIAGNOSA</th>
				<th>STATUS</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="8">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/igd/laporan/laporan_002'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien_id = $('#search-asal_pasien').val();
              	p.shift_id = $('#search-shift').val();
            }
		},
		 "columns": [
	      	{ "data": "no_register" },
	      	{ "data": "no_mr" },
	      	{ "data": "tgl_datang" },
	      	{ "data": "jam_datang" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "icd10" },
	      	{ "data": "dianosa" },
	      	{ "data": "status" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal_pasien').val();
      	let shift_id = $('#search-shift').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&shift_id=${shift_id}`;
		window.location.assign(`<?php echo site_url('api/igd/laporan/print_002'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal_pasien').val();
      	let shift_id = $('#search-shift').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&shift_id=${shift_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/igd/laporan/print_002'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>