<script>
function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
	return getDate(date);
}

$(document).ready(function () {
	$("#laporan").change(function () {
		let laporan = $(this).val();

		$('#section-laporan').show();
		$('#div-laporan').html('<div class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i> Tunggu Sebentar ...</div>');
		
		if (laporan !== '') {
			$('#btn-group_float').show('slow');
			$("#div-laporan").load("<?php echo site_url('igd/laporan/'); ?>" + laporan);
		} else {
			$('#btn-group_float').hide();
			$('#div-laporan').html('');
			$('#section-laporan').hide();
		}
	});
}); 
</script>