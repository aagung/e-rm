<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
						<i class="icon-calendar22"></i>
					</span>
					<input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Asal Pasein</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-asal-pasien">
					<option value="" selected="selected">- Pilih -</option>
					<option value="" selected="selected">Rawat Jalan</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Poli/Ruang</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-poli-ruang">
					<option value="" selected="selected">- Pilih -</option>
					<option value="" selected="selected">Poli Gigi</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">DPJP</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-dpjp">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">DPJP Lain</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-dpjp-lain">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-top: 9%">
			<label class="control-label col-md-4">Jaminan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-jaminan">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Jenis Kelamin </label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-jenis-kelamin">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Umur</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-umur">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. REG</th>
				<th>NO. RM</th>
				<th>NAMA</th>
				<th>ALAMAT</th>
				<th>J. KEL</th>
				<th>UMUR</th>
				<th>POLI/RUANG</th>
				<th>KELAS</th>
				<th>TGL MASUK</th>
				<th>TGL KELUAR</th>
				<th>HARI RAWAT</th>
				<th>ICD10</th>
				<th>DIAGNOSA</th>
				<th>DPJP</th>
				<th>DPJP LAIN</th>
				<th>SPESIALISASI</th>
				<th>STS PULANG</th>
				<th>CARA PULANG</th>
				<th>JAMINAN</th>
				<th>DIRUJUK KE</th>
				<th>ALASAN DIRUJUK</th>
				<th>ALASAN PULANG PAKSA</th>
				<th>BERAT LAHIR</th>
				<th>PANJANG LAHIR</th>
				<th>DATA SOSIAL</th>
				<th>RESUME MEDIS</th>
				<th>INFORM CONCERN</th>
				<th>INSTRUKSI DOKTER</th>
				<th>JENIS TRANSFUSI</th>
				<th>CC TRANSFUSI</th>
				<th>CATATAN PERAWAT</th>
				<th>KANTONG DARAH</th>
				<th>RUJUKAN DARI</th>
				<th>FORM EDUKASI PASIEN</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="34">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_004'); ?>",
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
					p.asal_pasien_id = $('#search-asal-pasien').val();
					p.poli_ruang_id = $('#search-poli-ruang').val();
				}
			},
			"columns": [
			{ "data": "no_reg" },
			{ "data": "no_rm" },
			{ "data": "nama" },
			{ "data": "alamat" },
			{ "data": "j_kel" },
			{ "data": "umur" },
			{ "data": "poli_ruang" },
			{ "data": "kelas" },
			{ "data": "tanggal_masuk" },
			{ "data": "tanggal_keluar" },
			{ "data": "hari_rawat" },
			{ "data": "icd10" },
			{ "data": "diagnosa" },
			{ "data": "dpjp" },
			{ "data": "dpjp_lain" },
			{ "data": "spesialisasi" },
			{ "data": "sts_pulang" },
			{ "data": "cara_pulang" },
			{ "data": "jaminan" },
			{ "data": "dirujuk_ke" },
			{ "data": "alasan_dirujuk" },
			{ "data": "alasan_pulang_paksa" },
			{ "data": "berat_lahir" },
			{ "data": "panjang_lahir" },
			{ "data": "data_sosial" },
			{ "data": "resume_medis" },
			{ "data": "inform_concern" },
			{ "data": "instruksi_dokter" },
			{ "data": "jenis_transfusi" },
			{ "data": "cc_transfusi" },
			{ "data": "catatan_perawat" },
			{ "data": "kantong_darah" },
			{ "data": "rujukan_dari" },
			{ "data": "form_edukasi_pasien" },
			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let asal_pasien_id = $('#search-asal-pasien').val();
			let poli_ruang_id = $('#search-poli-ruang').val();
			let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&poli_ruang_id=${poli_ruang_id}`;
			window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_004'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let asal_pasien_id = $('#search-asal-pasien').val();
			let poli_ruang_id = $('#search-poli-ruang').val();
			let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&poli_ruang_id=${poli_ruang_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_004'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>