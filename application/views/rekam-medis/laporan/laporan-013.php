<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
						<i class="icon-calendar22"></i>
					</span>
					<input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Jenis Rawat</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-jenis-rawat">
					<option value="" selected="selected">- Pilih -</option>
					<option value="" selected="selected">Rawat Jalan</option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Poli/Ruangan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-poli-ruangan">
					<option value="" selected="selected">- Pilih -</option>
					<option value="" selected="selected">Poli Gigi</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NAMA TINDAKAN</th>
				<th>POLI/RUANG</th>
				<th>JUMLAH</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="3">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_013'); ?>",
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
					p.jenis_rawat_id = $('#search-jenis-rawat').val();
					p.poli_ruangan_id = $('#search-poli-ruangan').val();
				}
			},
			"columns": [
			{ "data": "nama_tindakan" },
			{ "data": "poli_ruang" },
			{ "data": "jumlah" },


			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let jenis_rawat_id = $('#search-jenis-rawat').val();
			let poli_ruangan_id = $('#search-poli-ruangan').val();
			let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jenis_rawat_id=${jenis_rawat_id}&poli_ruangan_id=${poli_ruangan_id}`;
			window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_013'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let jenis_rawat_id = $('#search-jenis-rawat').val();
			let poli_ruangan_id = $('#search-poli-ruangan').val();
			let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jenis_rawat_id=${jenis_rawat_id}&poli_ruangan_id=${poli_ruangan_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_013'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>