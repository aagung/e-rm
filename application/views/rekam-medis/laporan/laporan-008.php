<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_informasi_pasien_meninggal_dirujuk_dari_igd">Pasien Meninggal, Dipulangkan, Dan Dirujuk Dari UGD</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('tindak_lanjut_label'); ?>Tindak Lanjut</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-tindak-lanjut">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Dirawat</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('dokter_label'); ?>Dokter</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-dokter">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('diagnosa_label'); ?>Diagnosa</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-diagnosa">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-1 col-md-offset-11">
		<button type="reset" class="btn btn-secondary reset-button">
            Reset
        </button>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>Tanggal</th>
				<th>No RM</th>
				<th>Nama Pasien</th>
				<th>Umur</th>
				<th>Dokter</th>
				<th>IGD10</th>
				<th>Diagnosa</th>
				<th>Tempat Rujuk</th>
				<th>Alasan Rujuk</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_008'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.diagnosa_id = $('#search-diagnosa').val();
              	p.dokter_id = $('#search-dokter').val();
            }
		},
		 "columns": [
	      	{ "data": "tanggal" },
	      	{ "data": "no_rm" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "umur" },
	      	{ "data": "dokter" },
	      	{ "data": "igd10" },
	      	{ "data": "diagnosa" },
	      	{ "data": "tempat_rujuk" },
	      	{ "data": "alasan_rujuk" },
	      	{ "data": "keterangan" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let diagnosa_id = $('#search-diagnosa').val();
      	let dokter_id = $('#search-dokter').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&diagnosa_id=${diagnosa_id}&dokter_id=${dokter_id}`;
		window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_008'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let diagnosa_id = $('#search-diagnosa').val();
      	let dokter_id = $('#search-dokter').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&diagnosa_id=${diagnosa_id}&dokter_id=${dokter_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_008'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>