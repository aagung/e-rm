<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
						<i class="icon-calendar22"></i>
					</span>
					<input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Jaminan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-jaminan">
					<option value="" selected="selected">- Pilih -</option>
					<option value="" selected="selected">Umum</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>TGL</th>
				<th>NO. MEDREC</th>
				<th>NAMA PASIEN</th>
				<th>WALI/ORANG TUA</th>
				<th>JENIS KELAMIN</th>
				<th>TTL</th>
				<th>AGAMA</th>
				<th>ALAMAT</th>
				<th>KOTA</th>
				<th>JAMINAN</th>
				<th>NO. HP</th>
				<th>JAMINAN</th>
				<th>PERUSAHAAN</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="13">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_012'); ?>",
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
					p.jaminan_id = $('#search-jaminan').val();
				}
			},
			"columns": [
			{ "data": "tanggal" },
			{ "data": "no_medrec" },
			{ "data": "nama_pasien" },
			{ "data": "wali_orang_tua" },
			{ "data": "Jenis_kelamin" },
			{ "data": "tempat_tanggal_lahir" },
			{ "data": "agama" },
			{ "data": "alamat" },
			{ "data": "kota" },
			{ "data": "jaminan" },
			{ "data": "no_hp" },
			{ "data": "jaminan" },
			{ "data": "perusahaan" },


			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let jaminan_id = $('#search-jaminan').val();
			let vendor_id = $('#search-vendor').val();
			let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jaminan_id=${jaminan_id}&vendor_id=${vendor_id}`;
			window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_012'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let jaminan_id = $('#search-jaminan').val();
			let vendor_id = $('#search-vendor').val();
			let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jaminan_id=${jaminan_id}&vendor_id=${vendor_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_012'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>