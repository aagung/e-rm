<style type="text/css">
.table > thead > tr > th, .table > tbody > tr > td {
    white-space: nowrap;
    vertical-align: middle;
}

#btn-group_float {
	position: fixed;
	bottom: 10%;
	right: 20px;
	z-index: 10000;
	text-align: right;
}
</style>
<div id="btn-group_float" style="display: none;">
	<p>
		<button type="button" id="btn-print-excel" class="btn bg-success btn-float btn-rounded" data-popup="popover" title="Export to Excel" data-trigger="hover" data-content="Klik tombol ini untuk mengexport laporan dalam bentuk Excel." data-placement="left" data-delay="600">
			<b><i class="icon-file-excel"></i></b>
		</button>
	</p>
	<p>
		<button type="button" id="btn-print-pdf" class="btn bg-orange btn-float btn-rounded" data-popup="popover" title="Print PDF" data-trigger="hover" data-content="Klik tombol ini untuk mencetak laporan dalam bentuk PDF." data-placement="left" data-delay="600">
			<b><i class="icon-file-pdf"></i></b>
		</button>
	</p>
</div>
<div class="panel panel-flat">
	<div class="panel-body form-horizontal"> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3" for="laporan">Laporan</label>
					<div class="col-md-9">
						<div class="input-group" style="width: 100%";>
							<select class="form-control select" id="laporan">
								<option value="">- Pilih Laporan -</option>
								<option value="laporan_001">1. Informasi Lokasi Pasien</option>
								<option value="laporan_002">2. Informasi Kunjungan Rawat Jalan</option>
								<option value="laporan_003">3. Informasi Statistik Diagnosa</option>
								<option value="laporan_004">4. Informasi Morbiditas Pasien</option>
								<option value="laporan_005">5. Informasi Statistik Jumlah Pasien yang Dilayani Dokter</option>
								<option value="laporan_006">6. Informasi Statistik Pelayanan KB dan Imunisasi</option>
								<option value="laporan_007">7. Informasi Pelayanan Unit Gawat Darurat</option>
								<option value="laporan_008">8. Informasi Pasien Meninggal dan Dirujuk dari IGD</option>
								<option value="laporan_009">9. Informasi Pelayanan Rawat Inap</option>
								<option value="laporan_010">10. Informasi Statistik Kunjungan Per Jaminan</option>
								<option value="laporan_011">11. Informasi Statistik Kunjungan Per Kunjungan</option>
								<option value="laporan_012">12. Informasi Kartu Induk Pasien</option>
								<option value="laporan_013">13. Informasi Statistik Tindakan dan Pemeriksaan Sewa Alat</option>
								<option value="laporan_014">14. Informasi Pasien Pulang Rawat Inap</option>
								<option value="laporan_015">15. Informasi Pasien Masuk Rawat Inap</option>
								<option value="laporan_016">16. Informasi Detail Tindakan dan VK</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="section-laporan" class="panel panel-flat" style="display: none;">
	<div class="panel-body form-horizontal">
		<div class="row">
			<div class="col-md-12">
				<div id="div-laporan"></div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-print" tabindex="-1" role="dialog" aria-labelledby="modal-printLabel">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<div class="close">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
				<h4 class="modal-title">Print Preview</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>