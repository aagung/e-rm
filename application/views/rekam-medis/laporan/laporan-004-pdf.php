<html>
<body>

	<style type="text/css" media="print">
		body {
			line-height: 1.2em;
			font-size: 8px;
			font-family: Arial, sans-serif;
		}
		h1, h2, h3, h4, h5, h6 {
			font-family: inherit;
			font-weight: 400;
			line-height: 1.5384616;
			color: inherit;
			margin-top: 0;
			margin-bottom: 5px;
			text-align: center;
		}
		h1 {
			font-size: 24px;
		}
		h2 {
			font-size: 16px;
		}
		h3 {
			font-size: 14px;
		}
		h4 {
			font-size: 12px;
		}
		h5 {
			font-size: 10px;
		}
		h6 {
			font-size: 8px;
		}
		table {
			border-collapse: collapse;
			font-size: 8px;
		}
		.table {
			border-spacing: 0;
			width: 100%;
			border: 1px solid #555;
			font-size: 10px;
		}
		.table thead th,
		.table tbody td {
			border: 1px solid #555;
			vertical-align: middle;
			padding: 5px 10px;
			line-height: 1.5384616;
		}
		.table thead th {
			color: #fff;
			background-color: #607D8B;
			font-weight: bold;
			text-align: center;
		}
	</style>

	<style>
		.footer_current_date_user {
			text-align: right;
			color: #d10404;
			font-size: 8px;
			vertical-align: top;
			margin-top: 10px;
		}
	</style>
	<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
	<h4 class="text-center"><?php echo $title; ?></h4>
	<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
	<br>
	<table class="table table-bordered table-striped">
		<thead>
			<tr class="bg-slate">
				<th>NO. REG</th>
				<th>NO. RM</th>
				<th>NAMA</th>
				<th>ALAMAT</th>
				<th>J. KEL</th>
				<th>UMUR</th>
				<th>POLI/RUANG</th>
				<th>KELAS</th>
				<th>TGL MASUK</th>
				<th>TGL KELUAR</th>
				<th>HARI RAWAT</th>
				<th>ICD10</th>
				<th>DIAGNOSA</th>
				<th>DPJP</th>
				<th>DPJP LAIN</th>
				<th>SPESIALISASI</th>
				<th>STS PULANG</th>
				<th>CARA PULANG</th>
				<th>JAMINAN</th>
				<th>DIRUJUK KE</th>
				<th>ALASAN DIRUJUK</th>
				<th>ALASAN PULANG PAKSA</th>
				<th>BERAT LAHIR</th>
				<th>PANJANG LAHIR</th>
				<th>DATA SOSIAL</th>
				<th>RESUME MEDIS</th>
				<th>INFORM CONCERN</th>
				<th>INSTRUKSI DOKTER</th>
				<th>JENIS TRANSFUSI</th>
				<th>CC TRANSFUSI</th>
				<th>CATATAN PERAWAT</th>
				<th>KANTONG DARAH</th>
				<th>RUJUKAN DARI</th>
				<th>FORM EDUKASI PASIEN</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					$pabrik_vendor = $row->pabrik;
					$pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
					?>
					<tr>
						<td><?php echo $no_reg; ?></td>
						<td><?php echo $row->no_rm; ?></td>
						<td><?php echo $row->nama; ?></td>
						<td><?php echo $row->alamat; ?></td>
						<td><?php echo $row->j_kel; ?></td>
						<td><?php echo $row->umur; ?></td>
						<td><?php echo $row->poli_ruang; ?></td>
						<td><?php echo $row->kelas; ?></td>
						<td><?php echo $row->tanggal_masuk; ?></td>
						<td><?php echo $row->tanggal_keluar; ?></td>
						<td><?php echo $row->hari_rawat; ?></td>
						<td><?php echo $row->icd10; ?></td>
						<td><?php echo $row->diagnosa; ?></td>
						<td><?php echo $row->dpjp; ?></td>
						<td><?php echo $row->dpjp_lain; ?></td>
						<td><?php echo $row->spesialisasi; ?></td>
						<td><?php echo $row->sts_pulang; ?></td>
						<td><?php echo $row->jaminan; ?></td>
						<td><?php echo $row->dirujuk_ke; ?></td>
						<td><?php echo $row->alasan_dirujuk; ?></td>
						<td><?php echo $row->alasan_pulang_paksa; ?></td>
						<td><?php echo $row->berat_lahir; ?></td>
						<td><?php echo $row->panjang_lahir; ?></td>
						<td><?php echo $row->data_sosial; ?></td>
						<td><?php echo $row->resume_medis; ?></td>
						<td><?php echo $row->inform_concern; ?></td>
						<td><?php echo $row->instruksi_dokter; ?></td>
						<td><?php echo $row->jenis_transfusi; ?></td>
						<td><?php echo $row->cc_transfusi; ?></td>
						<td><?php echo $row->catatan_perawat; ?></td>
						<td><?php echo $row->kantong_darah; ?></td>
						<td><?php echo $row->rujukan_dari; ?></td>
						<td><?php echo $row->form_edukasi_pasien; ?></td>
					</tr>
					<?php 
					$no++;
				endforeach; 
				?>
				<?php else: ?>
					<tr>
						<td style="font-weight: bold;text-align: center;" colspan="34">TIDAK ADA DATA</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px;">
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
			</tr>
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
			</tr>
		</table>
	</body>
	</html>