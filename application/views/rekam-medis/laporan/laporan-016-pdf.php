<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>NO</th>
			<th>TANGGAL OPERASI</th>
			<th>REGISTER</th>
			<th>NAMA PASIEN</th>
			<th>MR</th>
			<th>SEX</th>
			<th>TANGGAL LAHIR</th>
			<th>UMUR</th>
			<th>MULAI RAWAT</th>
			<th>JAMINAN</th>
			<th>DX PRE OPERASI</th>
			<th>KONSUL OPERATOR</th>
			<th>KONSUL ANASTESI</th>
			<th>DX POST OPERASI</th>
			<th>DR.OPERATOR</th>
			<th>DR.ANASTESI</th>
			<th>DR.KONSULEN</th>
			<th>GOLONGAN DARAH</th>
			<th>NAMA PROSEDUR OPERASI</th>
			<th>KD.ICD9</th>
			<th>CLEAN</th>
			<th>DIRTY</th>
			<th>CONTAMINATED</th>
			<th>ELEFTIF/CITO</th>
			<th>PENANDAAN LOKASI</th>
			<th>TANGGAL OPERASI</th>
			<th>RENCANA</th>
			<th>MULIA</th>
			<th>SELESAI</th>
			<th>DELAY (MENIT)</th>
			<th>ALASAN DELAY OPERASI</th>
			<th>PROFILAKSIS</th>
			<th>PERIKSA PA</th>
			<th>BUTUH DARAH</th>
			<th>JNS DARAH</th>
			<th>KANTONG DARAH</th>
			<th>JNS ANASTESIS</th>
			<th>SIGN IN</th>
			<th>DURING</th>
			<th>SIGN OUT</th>
			<th>TIME OUT</th>
			<th>RUANG PRE OP</th>
			<th>RUANG POST OP</th>
			<th>LOKASI OPERASI</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					// $pabrik_vendor = $row->pabrik;
     //                $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					// $grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->tanggal_operasi; ?></td>
			<td><?php echo $row->register; ?></td>
			<td><?php echo $row->nama_pasien; ?></td>
			<td><?php echo $row->mr; ?></td>
			<td><?php echo $row->sex; ?></td>
			<td><?php echo $row->tanggal_lahir; ?></td>
			<td><?php echo $row->umur; ?></td>
			<td><?php echo $row->mulai_rawat; ?></td>
			<td><?php echo $row->jaminan; ?></td>
			<td><?php echo $row->dx_pre_operasi; ?></td>
			<td><?php echo $row->konsul_operator; ?></td>
			<td><?php echo $row->konsul_anastesi; ?></td>
			<td><?php echo $row->dx_post_operasi; ?></td>
			<td><?php echo $row->dr_operator; ?></td>
			<td><?php echo $row->dr_anastesi; ?></td>
			<td><?php echo $row->dr_konsulen; ?></td>
			<td><?php echo $row->golongan_operasi; ?></td>
			<td><?php echo $row->nama_prosedur_operasi; ?></td>
			<td><?php echo $row->kd_icd9; ?></td>
			<td><?php echo $row->clean; ?></td>
			<td><?php echo $row->dirty; ?></td>
			<td><?php echo $row->contaminated; ?></td>
			<td><?php echo $row->elektif_cito; ?></td>
			<td><?php echo $row->penandaan_lokasi; ?></td>
			<td><?php echo $row->tanggal_operasi; ?></td>
			<td><?php echo $row->rencana; ?></td>
			<td><?php echo $row->mulai; ?></td>
			<td><?php echo $row->selesai; ?></td>
			<td><?php echo $row->delay_menit; ?></td>
			<td><?php echo $row->alasan_delay_operasi; ?></td>
			<td><?php echo $row->profilaksis; ?></td>
			<td><?php echo $row->periksa_pa; ?></td>
			<td><?php echo $row->butuh_darah; ?></td>
			<td><?php echo $row->jns_darah; ?></td>
			<td><?php echo $row->kantong_darah; ?></td>
			<td><?php echo $row->jns_anastesi; ?></td>
			<td><?php echo $row->sign_in; ?></td>
			<td><?php echo $row->during; ?></td>
			<td><?php echo $row->sign_out; ?></td>
			<td><?php echo $row->time_out; ?></td>
			<td><?php echo $row->ruang_pre_op; ?></td>
			<td><?php echo $row->ruang_post_op; ?></td>
			<td><?php echo $row->lokasi_operasi; ?></td>
					
			<!-- <td><?php echo $row->kode; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($row->tanggal)); ?></td>
			<td><?php echo $row->analisis ? $row->analisis : "-"; ?></td>
			<td><?php echo $this->config->item('sifat_po')[$row->sifat]; ?></td>
			<td><?php echo $pabrik_vendor; ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->total, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->total_ppn, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->grand_total, 2, ",", "."); ?></td>
			<td><?php echo $row->persetujuan_by; ?></td> -->
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<!-- <tr>
			<td style="font-weight: bold;text-align: right;" colspan="8">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr> -->
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="44">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>
