<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Jenis Tindakan <!-- <?php echo lang('jenis_tindakan_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-jenis_tindakan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>


<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>TANGGAL OPERASI</th>
				<th>REGISTER</th>
				<th>NAMA PASIEN</th>
				<th>MR</th>
				<th>SEX</th>
				<th>TANGGAL LAHIR</th>
				<th>UMUR</th>
				<th>MULAI RAWAT</th>
				<th>JAMINAN</th>
				<th>DX PRE OPERASI</th>
				<th>KONSUL OPERATOR</th>
				<th>KONSUL ANASTESI</th>
				<th>DX POST OPERASI</th>
				<th>DR.OPERATOR</th>
				<th>DR.ANASTESI</th>
				<th>DR.KONSULEN</th>
				<th>GOLONGAN OPERASI</th>
				<th>NAMA PROSEDUR OPERASI</th>
				<th>KD.ICD9</th>
				<th>CLEAN</th>
				<th>DIRTY</th>
				<th>CONTAMINATED</th>
				<th>ELEFTIF/CITO</th>
				<th>PENANDAAN LOKASI</th>
				<th>TANGGAL OPERASI</th>
				<th>RENCANA</th>
				<th>MULAI</th>
				<th>SELESAI</th>
				<th>DELAY (MENIT)</th>
				<th>ALASAN DELAY OPERASI</th>
				<th>PROFILAKSIS</th>
				<th>PERIKSA PA</th>
				<th>BUTUH DARAH</th>
				<th>JNS DARAH</th>
				<th>KANTONG DARAH</th>
				<th>JNS ANASTESIS</th>
				<th>SIGN IN</th>
				<th>DURING</th>
				<th>SIGN OUT</th>
				<th>TIME OUT</th>
				<th>RUANG PRE OP</th>
				<th>RUANG POST OP</th>
				<th>LOKASI OPERASI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_016'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
                p.jenis_tindakan = $('#search-jenis_tindakan').val();
                
             
            }
		},
		 "columns": [
	      	{ "data": "tanggal_operasi" },
	      	{ "data": "register" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "mr" },
	      	{ "data": "sex" },
	      	{ "data": "tanggal_lahir" },
	      	{ "data": "umur" },
	      	{ "data": "mulai_rawat" },
	      	{ "data": "jaminan" },
	      	{ "data": "dx_pre_operasi" },
	      	{ "data": "konsul_operator" },
	      	{ "data": "konsul_anastesi" },
	      	{ "data": "dx_post_operasi" },
	      	{ "data": "dr_operator" },
	      	{ "data": "dr_anastesi" },
	      	{ "data": "dr_konsulen" },
	      	{ "data": "golongan_operasi" },
	      	{ "data": "nama_prosedur_operasi" },
	      	{ "data": "kd_icd9" },
	      	{ "data": "clean" },
	      	{ "data": "dirty" },
	      	{ "data": "contaminated" },
	      	{ "data": "elektif_cito" },
	      	{ "data": "penandaan_lokasi" },
	      	{ "data": "tanggal_operasi" },
	      	{ "data": "rencana" },
	      	{ "data": "mulai" },
	      	{ "data": "selesai" },
	      	{ "data": "delay_menit" },
	      	{ "data": "alasan_delay_operasi" },
	      	{ "data": "profilaksis" },
	      	{ "data": "periksa_pa" },
	      	{ "data": "butuh_darah" },
	      	{ "data": "jns_darah" },
	      	{ "data": "kantong_darah" },
	      	{ "data": "jns_anastesi" },
	      	{ "data": "sign_in" },
	      	{ "data": "during" },
	      	{ "data": "sign_out" },
	      	{ "data": "time_out" },
	      	{ "data": "ruang_pre_op" },
	      	{ "data": "ruang_post_op" },
	      	{ "data": "lokasi_operasi" },
	      	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.vendor ? ` / ${row.vendor}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });
	$("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
        let jenis_tindakan = $('#search-jenis_tindakan').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jenis_tindakan=${jenis_tindakan}`;
		window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_016'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
        let jenis_tindakan = $('#search-jenis_tindakan').val();
      	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&jenis_tindakan=${jenis_tindakan}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_016'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>