<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Asal Pasien <!-- <?php echo lang('asal_pasien_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-asal_pasien">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>KWITANSI</th>
				<th>TANGGAL</th>
				<th>JAM</th>
				<th>REGISTER</th>
				<th>MR</th>
				<th>NAMA PASIEN</th>
				<th>JASA PEMERIKSAAN</th>
				<th>ADMINISTRASI</th>
				<th>CETAK KARTU</th>
				<th>TINDAKAN</th>
				<th>BHP TINDAKAN</th>
				<th>BHP DOKTER</th>
				<th>CSSD</th>
				<th>LINEN</th>
				<th>OBAT FARMASI</th>
				<th>FISIOTHERAPI</th>
				<th>LABORATORIUM</th>
				<th>RADIOLOGI</th>
				<th>CT SCAN</th>
				<th>USG RADIOLOGI</th>
				<th>ESWL</th>
				<th>USG OBGYN</th>
				<th>EKG</th>
				<th>EEG</th>
				<th>ECHO</th>
				<th>TREADMILL</th>
				<th>CTG</th>
				<th>PENUNJANG LAINNYA</th>
				<th>MCU</th>
				<th>HEMODIALISA</th>
				<th>AMBULANCE</th>
				<th>BAYAR PIUTANG</th>
				<th>ADM BANK</th>
				<th>TUNAI KREDIT</th>
				<th>TUNAI DEBET</th>
				<th>KARTU KREDIT/DEBIT</th>
				<th>PIUTANG</th>
				<th>DISKON</th>
				<th>KLAIM</th>
				<th>PENGELUARAN RETUR OBAT</th>
				<th>PENGELUARAN LAIN-LAIN</th>
				<th>TOTAL BAYAR</th>
				<th>PELAYANAN</th>
				<th>DOKTER</th>
				<th>JAMINAN</th>
				<th>KETERANGAN</th>
				<th>KASIR</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="48">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_002'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien = $('#search-asal_pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "no",
	      	  "searchable":false },
	      	{ "data": "kwitansi" },
	      	{ "data": "tanggal",
	      	  "searchable":false },
	      	{ "data": "jam",
	      	  "searchable":false },
	      	{ "data": "register" },
	      	{ "data": "mr" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "jasa_pemeriksaan",
	      	  "searchable":false},
	      	{ "data": "administrasi",
	      	  "searchable":false},
	      	{ "data": "cetak_kartu",
	      	  "searchable":false },
	      	{ "data": "tindakan",
	      	  "searchable":false },
	      	{ "data": "bhp_tindakan",
	      	  "searchable":false },
	      	{ "data": "bhp_dokter",
	      	  "searchable":false  },
	      	{ "data": "cssd",
	      	  "searchable":false },
	      	{ "data": "linen",
	      	  "searchable":false },
	      	{ "data": "obat_farmasi",
	      	  "searchable":false  },
	      	{ "data": "fisiotherapi",
	      	  "searchable":false  },
	      	{ "data": "laboratorium",
	      	  "searchable":false  },
	      	{ "data": "radiologi",
	      	  "searchable":false  },
	      	{ "data": "ct_scan",
	      	  "searchable":false  },
	      	{ "data": "usg_radiologi",
	      	  "searchable":false  },
	      	{ "data": "eswl",
	      	  "searchable":false  },
	      	{ "data": "usg_obgyn",
	      	  "searchable":false  },
	      	{ "data": "ekg",
	      	  "searchable":false  },
	      	{ "data": "eeg",
	      	  "searchable":false  },
	      	{ "data": "echo",
	      	  "searchable":false  },
	      	{ "data": "treadmill",
	      	  "searchable":false  },
	      	{ "data": "ctg",
	      	  "searchable":false  },
	      	{ "data": "penunjang_lainnya",
	      	  "searchable":false  },
	      	{ "data": "mcu",
	      	  "searchable":false  },
	      	{ "data": "hemodialisa",
	      	  "searchable":false  },
	      	{ "data": "ambulance",
	      	  "searchable":false  },
	      	{ "data": "bayar_piutang",
	      	  "searchable":false  },
	      	{ "data": "adm_bank",
	      	  "searchable":false  },
	      	{ "data": "tunai_kredit",
	      	  "searchable":false  },
	      	{ "data": "tunai_debet",
	      	  "searchable":false  },
	      	{ "data": "kartu_kredit_debit",
	      	  "searchable":false  },
	      	{ "data": "piutang",
	      	  "searchable":false  },
	      	{ "data": "diskon",
	      	  "searchable":false  },
	      	{ "data": "klaim",
	      	  "searchable":false  },
	      	{ "data": "pengeluaran_retur_obat",
	      	  "searchable":false  },
	      	{ "data": "pengeluaran_lain_lain",
	      	  "searchable":false  },
	      	{ "data": "total_bayar",
	      	  "searchable":false  },
	      	{ "data": "pelayanan",
	      	  "searchable":false  },
	      	{ "data": "dokter"  },
	      	{ "data": "jaminan"  },
	      	{ "data": "keterangan",
	      	  "searchable":false  },
	      	{ "data": "kasir",
	      	  "searchable":false  },
	      	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.vendor ? ` / ${row.vendor}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien = $('#search-vendor').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien=${asal_pasien}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_002'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien = $('#search-vendor').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien=${asal_pasien}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_002'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>