<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_rekapitulasi_pembayaran_kartu_debit_kredit">Rekap Pembayaran Kartu Debit / Kredit</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('asal_pasien_label'); ?>Asal Pasien</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-asal-pasien">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Rawat Jalan</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>s
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>Lokasi</th>
				<th>No</th>
				<th>NoKwitansi</th>
				<th>Tanggal</th>
				<th>Jam</th>
				<th>Nama Pasien</th>
				<th>No Kartu</th>
				<th>Nama Pemegang Kartu</th>
				<th>Nominal</th>
				<th>Charge</th>
				<th>Total</th>
				<th>User</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_006'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien_id = $('#search-asal-pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "lokasi" },
	      	{ "data": "no" },
	      	{ "data": "no_kwitansi" },
	      	{ "data": "tanggal" },
	      	{ "data": "jam" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "no_kartu" },
	      	{ "data": "nama_pemegang_kartu" },
	      	{ "data": "nominal" },
	      	{ "data": "charge" },
	      	{ "data": "total" },
	      	{ "data": "user" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_006'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_006'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>