<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_rekapitulasi_pendapatan_dan_pengeluaran_kasir">Rekapitulasi Pendapatan Dan Pengeluaran Kasir</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('asal_pasien_label'); ?>Asal Pasien</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-asal-pasien">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Rawat Jalan</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_rawat_inap">Rawat Inap</label>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>Tanggal</th>
				<th>Jasa Dokter Poli</th>
				<th>Kamar Cenda</th>
				<th>Kamar Cema</th>
				<th>Kam Saku</th>
				<th>Kamar Flambo</th>
				<th>Kamar Icu</th>
				<th>Kamar Hcu</th>
				<th>Kam Perin</th>
				<th>Obat Reguler</th>
				<th>Obat Bpjs</th>
				<th>Radiologi</th>
				<th>Ct Scan</th>
				<th>Laboratorium</th>
				<th>Esv</th>
				<th>Fisioter</th>
				<th>Transf Darah</th>
				<th>Hemodialisa</th>
				<th>Usg Obgyn</th>
				<th>EKG</th>
				<th>EEG</th>
				<th>Echo</th>
				<th>MCU</th>
				<th>Tread</th>
				<th>Ct</th>
				<th>Penunjang Lainnya</th>
				<th>Sewa Alat</th>
				<th>Visite</th>
				<th>Js Tinda Dokter Ruangan</th>
				<th>Js Tinda Dokter Keperawatan</th>
				<th>Js Tinda Lainnya</th>
				<th>Operatif Dr Open Ok</th>
				<th>Operatif Dr Anas O</th>
				<th>Operatif Dr Konsu Ok</th>
				<th>Operatif Asisten Ok</th>
				<th>Opera Sewa Kama Ok</th>
				<th>Opera Perawa Ok</th>
				<th>Opera Pkt Obat Ok</th>
				<th>Opera Jasa Rs Ok</th>
				<th>Line</th>
				<th>Css</th>
				<th>Opera Beauty Free</th>
				<th>Dokter Vk</th>
				<th>Dr Anas Vk</th>
				<th>Sewa Kam Vk</th>
				<th>Pkt Oba Vk</th>
				<th>Js Bid Vk</th>
				<th>Ambulance</th>
				<th>Adm Ir</th>
				<th>Pembayaran Deposit</th>
				<th>Pembayaran Piutang Pasien</th>
				<th>Penerima Kas Masuk Lain2</th>
				<th>Tun Kred</th>
				<th>Tun Deb</th>
				<th>Kartu Debit / Kredit</th>
				<th>Diskon</th>
				<th>Piutang</th>
				<th>Klaim Utama</th>
				<th>Klaim Kedua</th>
				<th>Desposit Setlem</th>
				<th>Pengembangan Deposit Lebih</th>
				<th>Pengeluaran Lain-Lain</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_005'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien_id = $('#search-asal-pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "tanggal" },
	      	{ "data": "jasa_dokter_poli" },
	      	{ "data": "kamar_cenda" },
	      	{ "data": "kamar_cema" },
	      	{ "data": "kamar_saku" },
	      	{ "data": "kamar_flambo" },
	      	{ "data": "kamar_icu" },
	      	{ "data": "kamar_hcu" },
	      	{ "data": "kamar_peri" },
	      	{ "data": "obat_reguler" },
	      	{ "data": "obat_bpjs" },
	      	{ "data": "radiologi" },
	      	{ "data": "ct_scan" },
	      	{ "data": "usg_radiologi" },
	      	{ "data": "laboratorium" },
	      	{ "data": "esv" },
	      	{ "data": "fisioter" },
	      	{ "data": "transf_darah" },
	      	{ "data": "hemodialisa" },
	      	{ "data": "usg_obgyn" },
	      	{ "data": "ekg" },
	      	{ "data": "eeg" },
	      	{ "data": "echo" },
	      	{ "data": "trad" },
	      	{ "data": "ct" },
	      	{ "data": "penunjang_lainnya" },
	      	{ "data": "sewa_alat" },
	      	{ "data": "visiter" },
	      	{ "data": "js_tinda_dokter_ruangan" },
	      	{ "data": "js_tinda_keperawatan" },
	      	{ "data": "js_tinda_lainnya" },
	      	{ "data": "operatif_dr_opera_ok" },
	      	{ "data": "operatif_dr_anas_ok" },
	      	{ "data": "operatif_dr_konsu_ok" },
	      	{ "data": "operatif_asisten_ok" },
	      	{ "data": "operatif_sewa_kamar_ok" },
	      	{ "data": "operatif_perawa_ok" },
	      	{ "data": "operatif_pkt_obat_ok" },
	      	{ "data": "opertif_jasa_rs_ok" },
	      	{ "data": "line" },
	      	{ "data": "css" },
	      	{ "data": "opera_beauty_free" },
	      	{ "data": "dokter_vk" },
	      	{ "data": "dr_anas_vk" },
	      	{ "data": "sewa_kamar_vk" },
	      	{ "data": "pkt_obat_vk" },
	      	{ "data": "js_bid_vk" },
	      	{ "data": "ambulan" },
	      	{ "data": "adm_in" },
	      	{ "data": "pembayaran_deposit" },
	      	{ "data": "pembayaran_piutang_pasien" },
	      	{ "data": "penerima_kas_masuk_lain_lain" },
	      	{ "data": "tunai_kredit" },
	      	{ "data": "tunai_debet" },
	      	{ "data": "kartu_kredit_debit" },
	      	{ "data": "diskon" },
	      	{ "data": "piutang" },
	      	{ "data": "klaim_utama" },
	      	{ "data": "klaim_kedua" },
	      	{ "data": "deposit_setlem" },
	      	{ "data": "pengemb_deposit_lebih" },
	      	{ "data": "pengeluaran_lain_lain" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_005'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_005'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>