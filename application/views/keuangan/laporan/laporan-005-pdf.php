<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>Tanggal</th>
			<th>Jasa Dokter Poli</th>
			<th>Kamar Cenda</th>
			<th>Kamar Cema</th>
			<th>Kam Saku</th>
			<th>Kamar Flambo</th>
			<th>Kamar Icu</th>
			<th>Kamar Hcu</th>
			<th>Kam Perin</th>
			<th>Obat Reguler</th>
			<th>Obat Bpjs</th>
			<th>Radiologi</th>
			<th>Ct Scan</th>
			<th>Laboratorium</th>
			<th>Esv</th>
			<th>Fisioter</th>
			<th>Transf Darah</th>
			<th>Hemodialisa</th>
			<th>Usg Obgyn</th>
			<th>EKG</th>
			<th>EEG</th>
			<th>Echo</th>
			<th>MCU</th>
			<th>Tread</th>
			<th>Ct</th>
			<th>Penunjang Lainnya</th>
			<th>Sewa Alat</th>
			<th>Visite</th>
			<th>Js Tinda Dokter Ruangan</th>
			<th>Js Tinda Dokter Keperawatan</th>
			<th>Js Tinda Lainnya</th>
			<th>Operatif Dr Open Ok</th>
			<th>Operatif Dr Anas O</th>
			<th>Operatif Dr Konsu Ok</th>
			<th>Operatif Asisten Ok</th>
			<th>Opera Sewa Kama Ok</th>
			<th>Opera Perawa Ok</th>
			<th>Opera Pkt Obat Ok</th>
			<th>Opera Jasa Rs Ok</th>
			<th>Line</th>
			<th>Css</th>
			<th>Opera Beauty Free</th>
			<th>Dokter Vk</th>
			<th>Dr Anas Vk</th>
			<th>Sewa Kam Vk</th>
			<th>Pkt Oba Vk</th>
			<th>Js Bid Vk</th>
			<th>Ambulance</th>
			<th>Adm Ir</th>
			<th>Pembayaran Deposit</th>
			<th>Pembayaran Piutang Pasien</th>
			<th>Penerima Kas Masuk Lain2</th>
			<th>Tun Kred</th>
			<th>Tun Deb</th>
			<th>Kartu Debit / Kredit</th>
			<th>Diskon</th>
			<th>Piutang</th>
			<th>Klaim Utama</th>
			<th>Klaim Kedua</th>
			<th>Desposit Setlem</th>
			<th>Pengembangan Deposit Lebih</th>
			<th>Pengeluaran Lain-Lain</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					$pabrik_vendor = $row->pabrik;
                    $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->tanggal; ?></td>
			<td><?php echo $row->jasa_dokter_poli ; ?></td>
			<td><?php echo $row->kamar_cenda; ?></td>
			<td><?php echo $row->kamar_cemd; ?></td>
			<td><?php echo $row->kamar_saku; ?></td>
			<td><?php echo $row->kamar_flambo; ?></td>
			<td><?php echo $row->kamar_icu; ?></td>
			<td><?php echo $row->kamar_hcu; ?></td>
			<td><?php echo $row->kamar_perin; ?></td>
			<td><?php echo $row->obat_reguler; ?></td>
			<td><?php echo $row->obat_bpjs; ?></td>
			<td><?php echo $row->radiologi; ?></td>
			<td><?php echo $row->ct_scan; ?></td>
			<td><?php echo $row->usg_radiologi; ?></td>
			<td><?php echo $row->laboratorium; ?></td>
			<td><?php echo $row->esv; ?></td>
			<td><?php echo $row->fisioter; ?></td>
			<td><?php echo $row->trans_darah; ?></td>
			<td><?php echo $row->hemodialisa; ?></td>
			<td><?php echo $row->usg_obgyn; ?></td>
			<td><?php echo $row->ekg; ?></td>
			<td><?php echo $row->eeg; ?></td>
			<td><?php echo $row->echo; ?></td>
			<td><?php echo $row->trad; ?></td>
			<td><?php echo $row->ct; ?></td>
			<td><?php echo $row->penunjang_lainnya; ?></td>
			<td><?php echo $row->sewa_alat; ?></td>
			<td><?php echo $row->visite; ?></td>
			<td><?php echo $row->js_tinda_dokter_ruangan; ?></td>
			<td><?php echo $row->js_tinda_keperawatan; ?></td>
			<td><?php echo $row->js_tinda_lainnya; ?></td>
			<td><?php echo $row->operatif_dr_open_ok; ?></td>
			<td><?php echo $row->operatif_dr_anas_ok; ?></td>
			<td><?php echo $row->operatif_dr_kons_ok; ?></td>
			<td><?php echo $row->operatif_asist_ok; ?></td>
			<td><?php echo $row->opera_sewa_kamar_ok; ?></td>
			<td><?php echo $row->operatif_perawat_ok; ?></td>
			<td><?php echo $row->operatif_pkt_obat_ok; ?></td>
			<td><?php echo $row->operatif_jasa_rs_ok; ?></td>
			<td><?php echo $row->line; ?></td>
			<td><?php echo $row->css; ?></td>
			<td><?php echo $row->opera_beauty_free; ?></td>
			<td><?php echo $row->dokter_vk; ?></td>
			<td><?php echo $row->dr_anas_vk; ?></td>
			<td><?php echo $row->sewa_kamar_vk; ?></td>
			<td><?php echo $row->pkt_obat_vk; ?></td>
			<td><?php echo $row->js_bid_vk; ?></td>
			<td><?php echo $row->ambulan; ?></td>
			<td><?php echo $row->adm_ir; ?></td>
			<td><?php echo $row->pembayaran_deposit; ?></td>
			<td><?php echo $row->pembayaran_piutang_pasien; ?></td>
			<td><?php echo $row->penerima_kas_masuk_lain_lain; ?></td>
			<td><?php echo $row->tunai_kredit; ?></td>
			<td><?php echo $row->tunai_debet; ?></td>
			<td><?php echo $row->kartu_kredit_debet; ?></td>
			<td><?php echo $row->diskon; ?></td>
			<td><?php echo $row->piutang; ?></td>
			<td><?php echo $row->klaim_utama; ?></td>
			<td><?php echo $row->klaim_kedua; ?></td>
			<td><?php echo $row->deposit_setlem; ?></td>
			<td><?php echo $row->pengembangan_deposit_lebih; ?></td>
			<td><?php echo $row->pengeluaran_lain_lain; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<tr>
			<td style="font-weight: bold;text-align: right;" colspan="8">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="10">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>