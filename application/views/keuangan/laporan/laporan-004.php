<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_rekapitulasi_pendapatan_dan_pengeluaran_kasir">Rekapitulasi Pendapatan Dan Pengeluaran Kasir</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('asal_pasien_label'); ?>Asal Pasien</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-asal-pasien">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Rawat Jalan</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_rawat_jalan">Rawat Jalan</label>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>Tanggal</th>
				<th>Jasa Pemeriksaan</th>
				<th>Administrasi</th>
				<th>Cetak Kartu</th>
				<th>Tindakan</th>
				<th>Bhp Tindakan</th>
				<th>Bhp Dokter</th>
				<th>Cssd</th>
				<th>Linen</th>
				<th>Obat Farmasi</th>
				<th>Fisiotherapi</th>
				<th>Laboratorium</th>
				<th>Radiologi</th>
				<th>Ct Scan</th>
				<th>Usg Radiologi</th>
				<th>Eswl</th>
				<th>Usg Obgyn</th>
				<th>EKG</th>
				<th>EEG</th>
				<th>Echo</th>
				<th>Treadmil</th>
				<th>Ctg</th>
				<th>Penunjang Lainnya</th>
				<th>MCU</th>
				<th>Hemodialisa</th>
				<th>Ambulance</th>
				<th>Bayar Piutang</th>
				<th>Adm Bank</th>
				<th>Tunai Kredit</th>
				<th>Tunai Debet</th>
				<th>Kartu Kredit / Debit</th>
				<th>Piutang</th>
				<th>Diskon</th>
				<th>Klaim</th>
				<th>Pengeluaran Retur Obat</th>
				<th>Pengeluaran Lain-Lain</th>
				<th>Total Bayar</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_004'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien_id = $('#search-asal-pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "tanggal" },
	      	{ "data": "jasa_pemeriksaan" },
	      	{ "data": "administrasi" },
	      	{ "data": "cetak_kartu" },
	      	{ "data": "tindakan" },
	      	{ "data": "bhp_tindakan" },
	      	{ "data": "bhp_dokter" },
	      	{ "data": "cssd" },
	      	{ "data": "linen" },
	      	{ "data": "obat_farmasi" },
	      	{ "data": "fisiotherapi" },
	      	{ "data": "laboratorium" },
	      	{ "data": "radiologi" },
	      	{ "data": "ct_scan" },
	      	{ "data": "usg_radiologi" },
	      	{ "data": "eswl" },
	      	{ "data": "usg_obgyn" },
	      	{ "data": "ekg" },
	      	{ "data": "eeg" },
	      	{ "data": "echo" },
	      	{ "data": "treadmill" },
	      	{ "data": "ctg" },
	      	{ "data": "penunjang_lainnya" },
	      	{ "data": "mcu" },
	      	{ "data": "hemodialisa" },
	      	{ "data": "ambulance" },
	      	{ "data": "bayar_piutang" },
	      	{ "data": "adm_bank" },
	      	{ "data": "tunai_kredit" },
	      	{ "data": "tunai_debet" },
	      	{ "data": "kartu_kredit_debit" },
	      	{ "data": "piutang" },
	      	{ "data": "diskon" },
	      	{ "data": "klaim" },
	      	{ "data": "pengeluaran_retur_obat" },
	      	{ "data": "pengeluaran_lain_lain" },
	      	{ "data": "total_bayar" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_004'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien_id = $('#search-asal-pasien').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien_id=${asal_pasien_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_004'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>