<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>Tanggal</th>
			<th>Jasa Pemeriksaan</th>
			<th>Administrasi</th>
			<th>Cetak Kartu</th>
			<th>Tindakan</th>
			<th>Bhp Tindakan</th>
			<th>Bhp Dokter</th>
			<th>Cssd</th>
			<th>Linen</th>
			<th>Obat Farmasi</th>
			<th>Fisiotherapi</th>
			<th>Laboratorium</th>
			<th>Radiologi</th>
			<th>Ct Scan</th>
			<th>Usg Radiologi</th>
			<th>Eswl</th>
			<th>Usg Obgyn</th>
			<th>EKG</th>
			<th>EEG</th>
			<th>Echo</th>
			<th>Treadmil</th>
			<th>Ctg</th>
			<th>Penunjang Lainnya</th>
			<th>MCU</th>
			<th>Hemodialisa</th>
			<th>Ambulance</th>
			<th>Bayar Piutang</th>
			<th>Adm Bank</th>
			<th>Tunai Kredit</th>
			<th>Tunai Debet</th>
			<th>Kartu Kredit / Debit</th>
			<th>Piutang</th>
			<th>Diskon</th>
			<th>Klaim</th>
			<th>Pengeluaran Retur Obat</th>
			<th>Pengeluaran Lain-Lain</th>
			<th>Total Bayar</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					$pabrik_vendor = $row->pabrik;
                    $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->tanggal; ?></td>
			<td><?php echo $row->jasa ; ?></td>
			<td><?php echo $row->administrasi; ?></td>
			<td><?php echo $row->cetak_kartu; ?></td>
			<td><?php echo $row->tindakan; ?></td>
			<td><?php echo $row->bhp_tindakan; ?></td>
			<td><?php echo $row->bhp_dokter; ?></td>
			<td><?php echo $row->cssd; ?></td>
			<td><?php echo $row->linen; ?></td>
			<td><?php echo $row->obat_farmasi; ?></td>
			<td><?php echo $row->fisiotherapi; ?></td>
			<td><?php echo $row->laboratorium; ?></td>
			<td><?php echo $row->radiologi; ?></td>
			<td><?php echo $row->ct_scan; ?></td>
			<td><?php echo $row->usg_radiologi; ?></td>
			<td><?php echo $row->eswl; ?></td>
			<td><?php echo $row->usg_obgyn; ?></td>
			<td><?php echo $row->ekg; ?></td>
			<td><?php echo $row->eeg; ?></td>
			<td><?php echo $row->echo; ?></td>
			<td><?php echo $row->treadmill; ?></td>
			<td><?php echo $row->ctg; ?></td>
			<td><?php echo $row->penunjang_lainnya; ?></td>
			<td><?php echo $row->mcu; ?></td>
			<td><?php echo $row->hemodialisa; ?></td>
			<td><?php echo $row->ambulance; ?></td>
			<td><?php echo $row->bayar_piutang; ?></td>
			<td><?php echo $row->adm_bank; ?></td>
			<td><?php echo $row->tunai_kredit; ?></td>
			<td><?php echo $row->tunai_debet; ?></td>
			<td><?php echo $row->kartu_kredit_debit; ?></td>
			<td><?php echo $row->piutang; ?></td>
			<td><?php echo $row->diskon; ?></td>
			<td><?php echo $row->klaim; ?></td>
			<td><?php echo $row->pengeluaran_retur_obat; ?></td>
			<td><?php echo $row->pengeluaran_lain_lain; ?></td>
			<td><?php echo $row->total_bayar; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<tr>
			<td style="font-weight: bold;text-align: right;" colspan="8">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="10">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>