<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Asal Pasien</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-asal_pasien">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Rawat Jalan</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>NO NOTA</th>
				<th>NAMA PASIEN</th>
				<th>TANGGAL</th>
				<th>DITERIMA</th>
				<th>PROSES</th>
				<th>SELESAI</th>
				<th>SERAH</th>
				<th>WAKTU TUNGGU (mnt)</th>
				<th>MR</th>
				<th>REGISTER</th>
				<th>ASAL</th>
				<th>RUJUK</th>
				<th>BILL FARMASI</th>
				<th>JAMINAN</th>
				<th>USER r\Far</th>
				<th>KW FARMASI</th>
				<th>KW DOKTER</th>
				<th>REF. KWITANSI</th>
				<th>TGL KWITANSI</th>
				<th>JAM KWITANSI</th>
				<th>USER KASIR</th>
				<th>KWITANSI ATAS NAMA</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="23">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_009'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.asal_pasien = $('#search-asal_pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "no" },
	      	{ "data": "no_nota" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "tanggal" },
	      	{ "data": "diterima" },
	      	{ "data": "proses" },
	      	{ "data": "selesai" },
	      	{ "data": "serah" },
	      	{ "data": "waktu_tunggu_mnt" },
	      	{ "data": "mr" },
	      	{ "data": "register" },
	      	{ "data": "asal" },
	      	{ "data": "rujuk" },
	      	{ "data": "bill_farmasi" },
	      	{ "data": "jaminan" },
	      	{ "data": "user_r_far" },
	      	{ "data": "kw_farmasi" },
	      	{ "data": "kw_dokter" },
	      	{ "data": "ref_kwitansi" },
	      	{ "data": "tgl_kwitansi" },
	      	{ "data": "jam_kwitansi" },
	      	{ "data": "user_kasir" },
	      	{ "data": "kwitansi_atas_nama" },
	      	
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien = $('#search-vendor').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien=${asal_pasien}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_009'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let asal_pasien = $('#search-vendor').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&asal_pasien=${asal_pasien}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_009'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>