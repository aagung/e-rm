<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Asal Pasien <!-- <?php echo lang('asal_pasien_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-asal_pasien">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Shift <!-- <?php echo lang('shift_label'); ?> --> </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-shift">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>REGISTER</th>
				<th>NO. MR</th>
				<th>NAMA PASIEN</th>
				<th>JAMINAN</th>
				<th>DOKTER</th>
				<th>NO KWITANSI</th>
				<th>JAM</th>
				<th>KETERANGAN</th>
				<th>PENERIMAAN</th>
				<th>PENGELUARAN</th>
				<th>PIUTANG</th>
				<th>KLAIM UTAMA</th>
				<th>KLAIM KE DUA</th>
				<th>SUB TOTAL</th>
				<th>KASIR / SHIFT</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="7">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/keuangan/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.shift_id = $('#search-shift').val();
              	p.asal_pasien = $('#search-asal_pasien').val();
            }
		},
		 "columns": [
	      	{ "data": "no" },
	      	{ "data": "register" },
	      	{ "data": "no_mr" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "jaminan" },
	      	{ "data": "dokter" },
	      	{ "data": "no_kwitansi" },
	      	{ "data": "jam",
	      	  "searchable" : false  },
	      	{ "data": "keterangan",
	      	  "searchable" : false  },
	      	{ "data": "penerimaan" ,
	      	  "searchable" : false },
	      	{ "data": "pengeluaran",
	      	  "searchable" : false  },
	      	{ "data": "piutang",
	      	  "searchable" : false  },
	      	{ "data": "klaim_utama",
	      	  "searchable" : false  },
	      	{ "data": "klaim_ke_dua",
	      	  "searchable" : false  },
	      	{ "data": "sub_total",
	      	  "searchable" : false  },
	      	{ "data": "kasir_shift",
	      	  "searchable" : false  },
	      	
	      	// { 
	      	// 	"data": "tanggal",
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return moment(data).format('DD-MM-YYYY HH:mm');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.vendor ? ` / ${row.vendor}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let shift_id = $('#search-pabrik').val();
      	let asal_pasien = $('#search-vendor').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&shift_id=${shift_id}&asal_pasien=${asal_pasien}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let shift_id = $('#search-pabrik').val();
      	let asal_pasien = $('#search-vendor').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&shift_id=${shift_id}&asal_pasien=${asal_pasien}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>