<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>JENIS PEMERIKSAAN</th>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
				<th>16</th>
				<th>17</th>
				<th>18</th>
				<th>19</th>
				<th>20</th>
				<th>21</th>
				<th>22</th>
				<th>23</th>
				<th>24</th>
				<th>25</th>
				<th>26</th>
				<th>27</th>
				<th>28</th>
				<th>29</th>
				<th>30</th>
				<th>31</th>
				<th>TOTAL</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="34">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/radiologi/laporan/laporan_006'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
            }
		},
		 "columns": [
		 	{ "data": "no",
		 	  "searchable": false, },
	      	{ "data": "jenis_pemeriksaan", },
	      	{ "data": "hari_1",
	      	  "searchable": false, },
	      	{ "data": "hari_2",
	      	  "searchable": false, },
	      	{ "data": "hari_3",
	      	  "searchable": false, },
	      	{ "data": "hari_4",
	      	  "searchable": false, },
	      	{ "data": "hari_5",
	      	  "searchable": false, },
	      	{ "data": "hari_6",
	      	  "searchable": false, },
	      	{ "data": "hari_7",
	      	  "searchable": false, },
	      	{ "data": "hari_8",
	      	  "searchable": false, },
	      	{ "data": "hari_9",
	      	  "searchable": false, },
	      	{ "data": "hari_10",
	      	  "searchable": false, },
	      	{ "data": "hari_11",
	      	  "searchable": false, },
	      	{ "data": "hari_12",
	      	  "searchable": false, },
	      	{ "data": "hari_13",
	      	  "searchable": false, },
	      	{ "data": "hari_14",
	      	  "searchable": false, },
	      	{ "data": "hari_15",
	      	  "searchable": false, },
	      	{ "data": "hari_16",
	      	  "searchable": false, },
	      	{ "data": "hari_17",
	      	  "searchable": false, },
	      	{ "data": "hari_18",
	      	  "searchable": false, },
	      	{ "data": "hari_19",
	      	  "searchable": false, },
	      	{ "data": "hari_20",
	      	  "searchable": false, },
	      	{ "data": "hari_21",
	      	  "searchable": false, },
	      	{ "data": "hari_22",
	      	  "searchable": false, },
	      	{ "data": "hari_23",
	      	  "searchable": false, },
	      	{ "data": "hari_24",
	      	  "searchable": false, },
	      	{ "data": "hari_25",
	      	  "searchable": false, },
	      	{ "data": "hari_26",
	      	  "searchable": false, },
	      	{ "data": "hari_27",
	      	  "searchable": false, },
	      	{ "data": "hari_28",
	      	  "searchable": false, },
	      	{ "data": "hari_29",
	      	  "searchable": false, },
	      	{ "data": "hari_30",
	      	  "searchable": false, },
	      	{ "data": "hari_31",
	      	  "searchable": false, },
	      	{ "data": "total",
	      	  "searchable": false, },
	      	
	      	
	      	// { 
	      	// 	"data": "analisis",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return data ? data : "&mdash;";
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "sifat",
	      	// 	"searchable": false,
	      	// },
	      	// { 
	      	// 	"data": "pabrik",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		let tmp = data;
	      	// 		tmp += row.vendor ? ` / ${row.vendor}` : "";
	      	// 		return tmp;
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "total_ppn",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "grand_total",
	      	// 	"searchable": false,
	      	// 	"render": function (data, type, row, meta) {
	      	// 		return 'Rp. ' + numeral(data).format('0.0,');
		      //   }
	      	// },
	      	// { 
	      	// 	"data": "persetujuan_by",
	      	// 	"searchable": false,
	      	// },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		window.location.assign(`<?php echo site_url('api/radiologi/laporan/print_006'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/radiologi/laporan/print_006'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>