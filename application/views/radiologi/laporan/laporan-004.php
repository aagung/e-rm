<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label col-md-4">Bulan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-tanggal">
					<option value="" selected="selected">- Pilih -</option>
					<?php
					$bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
					for($a=1;$a<=12;$a++){
						if($a==date("m"))
						{ 
							$pilih="selected";
						}
						else 
						{
							$pilih="";
						}
						echo("<option value=\"$a\" $pilih>$bulan[$a]</option>"."\n");
					}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label col-md-4">Tahun</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-tanggal">
					<option value="" selected="selected">- Pilih -</option>
					<?php
					$tahun = array("2000", "2001", "2002", "2003", "2004", "2005", "2006","2007", "2008", "2009", "2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022");
					for($a=1;$a<=23;$a++){
						if($a==date("y"))
						{ 
							$pilih="selected";
						}
						else 
						{
							$pilih="";
						}
						echo("<option value=\"$a\" $pilih>$tahun[$a]</option>"."\n");
					}
					?>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr class="bg-slate" role="row">
				<th class="text-center" rowspan="2" colspan="1">JENIS PEMERIKSAAN</th>
				<th class="text-center" colspan="3" rowspan="1">UMUM</th>
				<th rowspan="2" colspan="1">BPJS</th>
				<th rowspan="2" colspan="1">JAMKESDA</th>
				<th class="text-center" colspan="3" rowspan="">ASURANSI LAIN</th>
				<th class="text-center" rowspan="2" colspan="1">TOTAL</th>
			</tr>
			<tr class="bg-slate" role="row">
				<th class="text-center" rowspan="1" colspan="1">R.J</th>
				<th class="text-center" rowspan="1" colspan="1">R.I</th>
				<th class="text-center" rowspan="1" colspan="1">MCU</th>
				<th class="text-center" rowspan="1" colspan="1">R.J</th>
				<th class="text-center" rowspan="1" colspan="1">R.I</th>
				<th class="text-center" rowspan="1" colspan="1">MCU</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="6">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>


	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/radiologi/laporan/laporan_004'); ?>",
				"type": "POST",
				"data": function(p) {
					p.bulan_id = subsDate($("#search-tanggal").val(), 'dari');
					p.tahun_id = subsDate($("#search-tanggal").val(), 'sampai');
				}
			},
			"columns": [
			{ "data": "jaminan_pemeriksaan" },
	      	{ "data": "umum" },
	      	{ "data": "bpjs" },
	      	{ "data": "jamkesda" },
	      	{ "data": "asurasi_lain" },
	      	{ "data": "total" },
	      	{ "data": "r_j" },
	      	{ "data": "r_i" },
	      	{ "data": "r_j" },
	      	{ "data": "r_i" },

			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let bulan_id = subsDate($("#search-tanggal").val(), 'dari');
			let tahun_id = subsDate($("#search-tanggal").val(), 'sampai');
			let param = `?d=excel&bulan_id=${bulan_id}&tahun_id=${tahun_id}`;
			window.location.assign(`<?php echo site_url('api/radiologi/laporan/print_004'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let bulan_id = subsDate($("#search-tanggal").val(), 'dari');
			let tahun_id = subsDate($("#search-tanggal").val(), 'sampai');
			let param = `?d=pdf&bulan_id=${bulan_id}&tahun_id=${tahun_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/radiologi/laporan/print_004'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>
