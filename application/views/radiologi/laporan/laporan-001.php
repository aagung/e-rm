<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_infprmasi_pemeriksaan_radiologi">Informasi Pemeriksaan Radiologi</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('pemeriksaan_label'); ?>Pemeriksaan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-pemeriksaan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('jaminan_label'); ?>Jaminan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-1 col-md-offset-5">
		<button type="reset" class="btn btn-secondary reset-button">
            Reset
        </button>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>No</th>
				<th>No Foto</th>
				<th>Tanggal</th>
				<th>Register</th>
				<th>No RM</th>
				<th>Nama Pasien</th>
				<th>Jaminan</th>
				<th>Nama Pemeriksaan</th>
				<th>Sub Total</th>
				<th>User RO</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rekam_medis/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.pemeriksaan_id = $('#search-pemeriksaan').val();
              	p.jaminan_id = $('#search-jaminan').val();
            }
		},
		 "columns": [
	      	{ "data": "no" },
	      	{ "data": "no_foto" },
	      	{ "data": "tanggal" },
	      	{ "data": "register" },
	      	{ "data": "no_rm" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "jaminan" },
	      	{ "data": "nama_pemeriksaan" },
	      	{ "data": "sub_total" },
	      	{ "data": "user_ro" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let pemeriksaan_id = $('#search-ruang').val();
      	let jaminan_id = $('#search-kelas').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&pemeriksaan_id=${pemeriksaan_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/rekam_medis/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let pemeriksaan_id = $('#search-ruang').val();
      	let jaminan_id = $('#search-kelas').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&pemeriksaan_id=${pemeriksaan_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rekam_medis/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>