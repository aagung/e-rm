<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Bulan </label>
			<div class="col-md-4">    
                <select class="form-control select" id="search-bulan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value=""> Januari </option>
                	<option value=""> Februari </option>
                	<option value=""> Maret </option>
                	<option value=""> April </option>
                	<option value=""> Mei </option>
                	<option value=""> Juni </option>
                	<option value=""> Juli </option>
                	<option value=""> Agustus </option>
                	<option value=""> September </option>
                	<option value=""> Oktober </option>
                	<option value=""> November </option>
                	<option value=""> Desember </option>
                </select>
            </div>
            <div class="col-md-4">    
                <select class="form-control select" id="search-bulan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value=""> Januari </option>
                	<option value=""> Februari </option>
                	<option value=""> Maret </option>
                	<option value=""> April </option>
                	<option value=""> Mei </option>
                	<option value=""> Juni </option>
                	<option value=""> Juli </option>
                	<option value=""> Agustus </option>
                	<option value=""> September </option>
                	<option value=""> Oktober </option>
                	<option value=""> November </option>
                	<option value=""> Desember </option>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Tahun</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-tahun">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="">2001</option>
                	<option value="">2002</option>
                	<option value="">2003</option>
                	<option value="">2004</option>
                	<option value="">2005</option>
                	<option value="">2001</option>
                	<option value="">2002</option>
                	<option value="">2003</option>
                	<option value="">2004</option>
                	<option value="">2005</option>
                	<option value="">2006</option>
                	<option value="">2007</option>
                	<option value="">2008</option>
                	<option value="">2008</option>
                	<option value="">2009</option>
                	<option value="">2010</option>
                	<option value="">2011</option>
                	<option value="">2012</option>
                	<option value="">2013</option>
                	<option value="">2014</option>
                	<option value="">2015</option>
                	<option value="">2016</option>
                	<option value="">2017</option>
                	<option value="">2018</option>
                	<option value="">2019</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">tahun</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-status">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal"></label>
			<div class="col-md-6">
                <div class="input-group">
                    <span id="btn-search_tanggal">
                    </span>
                    <input type="hidden" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NAMA ASET</th>
				<th>TGL. PEROLEHAN</th>
				<th>HARGA BELI (Rp.)</th>
				<th>PENYUSUTAN BULAN NOVEMBER</th>
				<th>PENYUSUTAN s/d BULAN NOVEMBER</th>
				<th>NILAI BUKU (Rp.)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="6">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();
	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/aset/laporan/laporan_003'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.bulan_id = $('#search-bulan').val();
              	p.tahun_id = $('#search-tahun').val();
            }
		},
		 "columns": [
	      	{ "data": "nama_aset" },
	      	{ "data": "tgl_perolehan" },
	      	{ "data": "harga_beli" },
	      	{ "data": "penyusutan_bulan_november" },
	      	{ "data": "penyusutan_s_d_bulan_november" },
	      	{ "data": "nilai_buku"}
	      	
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let bulan_id = $('#search-bulan').val();
      	let tahun_id = $('#search-tahun').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&bulan_id=${bulan_id}&tahun_id=${tahun_id}`;
		window.location.assign(`<?php echo site_url('api/aset/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let bulan_id = $('#search-bulan').val();
      	let tahun_id = $('#search-tahun').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&bulan_id=${bulan_id}&tahun_id=${tahun_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/aset/laporan/print_003'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>