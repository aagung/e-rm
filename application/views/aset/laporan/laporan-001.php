<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Ruang </label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-ruang">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Golongan</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-golongan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Status</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-status">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal"></label>
			<div class="col-md-6">
                <div class="input-group">
                    <span id="btn-search_tanggal">
                    </span>
                    <input type="hidden" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>ID. ASSET</th>
				<th>NO INVENTARIS</th>
				<th>NAMA BARANG INVENTARIS</th>
				<th>MERK</th>
				<th>TIPE</th>
				<th>NO. SERI</th>
				<th>THN BELI</th>
				<th>JML</th>
				<th>SAT</th>
				<th>HARGA AWAL</th>
				<th>DAYA LISTRIK</th>
				<th>GOLONGAN</th>
				<th>STATUS</th>
				<th>KETERANGAN</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="15">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();
	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/aset/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.ruang_id = $('#search-ruang').val();
              	p.golongan_id = $('#search-golongan').val();
            }
		},
		 "columns": [
	      	{ "data": "no" },
	      	{ "data": "id_asset" },
	      	{ "data": "no_inventaris" },
	      	{ "data": "nama_barang_inventaris" },
	      	{ "data": "merk" },
	      	{ "data": "tipe" },
	      	{ "data": "no_seri" },
	      	{ "data": "thn_beli" },
	      	{ "data": "jml" },
	      	{ "data": "sat" },
	      	{ "data": "harga awal" },
	      	{ "data": "daya_listrik" },
	      	{ "data": "golongan" },
	      	{ "data": "status" },
	      	{ "data": "keterangan" },
	      	
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let ruang_id = $('#search-ruang').val();
      	let golongan_id = $('#search-golongan').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&golongan_id=${golongan_id}`;
		window.location.assign(`<?php echo site_url('api/aset/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let ruang_id = $('#search-ruang').val();
      	let golongan_id = $('#search-golongan').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&golongan_id=${golongan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/aset/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>