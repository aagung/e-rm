<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
						<i class="icon-calendar22"></i>
					</span>
					<input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Kecamatan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-kecamatan">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. MR</th>
				<th>NAMA PASIEN/NAMA IBU</th>
				<th>UMUR</th>
				<th>KK/SUAMI</th>
				<th>ALAMAT</th>
				<th>TGL LAHIR</th>
				<th>BERAT BADAN (N/T)</th>
				<th>HEP BO</th>
				<th>DPthBIB1</th>
				<th>DPthBIB2</th>
				<th>DPthBIB3</th>
				<th>POLIO 1</th>
				<th>POLIO 2</th>
				<th>POLIO 3</th>
				<th>POLIO 4</th>
				<th>CAMPAK</th>
				<th>BOOSTER DPthBIB</th>
				<th>HEP B1</th>
				<th>HEP B2</th>
				<th>HEP B3</th>
				<th>DPT 1</th>
				<th>DPT 2</th>
				<th>DPT 3</th>
				<th>COMBO 1</th>
				<th>COMBO 2</th>
				<th>COMBO 3</th>
				<th>VIT A BIRU</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="27">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_007'); ?>",
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
					p.kecamatan_id = $('#search-kecamatan').val();
				}
			},
			"columns": [
				{ "data": "no_mr" },
				{ "data": "nama_pasien_nama_ibu" },
				{ "data": "umur" },
				{ "data": "kk_suami" },
				{ "data": "alamat" },
				{ "data": "tanggal_lahir" },
				{ "data": "berat_badan" },
				{ "data": "hep_bo" },
				{ "data": "DPTHBib1" },
				{ "data": "DPTHBib2" },
				{ "data": "DPTHBib3" },
				{ "data": "polio_1" },
				{ "data": "polio_2" },
				{ "data": "polio_3" },
				{ "data": "polio_4" },
				{ "data": "campak" },
				{ "data": "booster_DPTHBib" },
				{ "data": "hep_b1" },
				{ "data": "hep_b2" },
				{ "data": "hep_b3" },
				{ "data": "dpt_1" },
				{ "data": "dpt_2" },
				{ "data": "dpt_3" },
				{ "data": "combo_1" },
				{ "data": "combo_2" },
				{ "data": "combo_3" },
				{ "data": "vit_a_biru" },


			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let kecamatan_id = $('#search-kecamatan').val();
			let vendor_id = $('#search-vendor').val();
			let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&kecamatan_id=${kecamatan_id}&vendor_id=${vendor_id}`;
			window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_007'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let kecamatan_id = $('#search-kecamatan').val();
			let vendor_id = $('#search-vendor').val();
			let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&kecamatan_id=${kecamatan_id}&vendor_id=${vendor_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_007'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>