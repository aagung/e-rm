<html>
<body>

	<style type="text/css" media="print">
		body {
			line-height: 1.2em;
			font-size: 8px;
			font-family: Arial, sans-serif;
		}
		h1, h2, h3, h4, h5, h6 {
			font-family: inherit;
			font-weight: 400;
			line-height: 1.5384616;
			color: inherit;
			margin-top: 0;
			margin-bottom: 5px;
			text-align: center;
		}
		h1 {
			font-size: 24px;
		}
		h2 {
			font-size: 16px;
		}
		h3 {
			font-size: 14px;
		}
		h4 {
			font-size: 12px;
		}
		h5 {
			font-size: 10px;
		}
		h6 {
			font-size: 8px;
		}
		table {
			border-collapse: collapse;
			font-size: 8px;
		}
		.table {
			border-spacing: 0;
			width: 100%;
			border: 1px solid #555;
			font-size: 10px;
		}
		.table thead th,
		.table tbody td {
			border: 1px solid #555;
			vertical-align: middle;
			padding: 5px 10px;
			line-height: 1.5384616;
		}
		.table thead th {
			color: #fff;
			background-color: #607D8B;
			font-weight: bold;
			text-align: center;
		}
	</style>

	<style>
		.footer_current_date_user {
			text-align: right;
			color: #d10404;
			font-size: 8px;
			vertical-align: top;
			margin-top: 10px;
		}
	</style>
	<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
	<h4 class="text-center"><?php echo $title; ?></h4>
	<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
	<br>
	<table class="table table-bordered table-striped">
		<thead>
			<tr class="bg-slate">
				<th>NO</th>
				<th>REGISTER</th>
				<th>MR</th>
				<th>PASIEN</th>
				<th>KETERANGAN</th>
				<th>TARIF</th>
				<th>JASA</th>
				<th>POTONGAN</th>
				<th>JASA TDK UGD</th>
				<th>JASA TDK VK</th>
				<th>JASA VARTUS VK</th>
				<th>JASA BERSIH</th>
				<th>TANGGAL</th>
				<th>JAM</th>
				<th>LAYANAN</th>
				<th>KELAS</th>
				<th>JAMINAN</th>
				<th>REF. KWITANSI</th>
				<th>USER KASIR</th>
				<th>USER ENTRY STRUK</th>
				<th>KODE TARIF</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$kode = 1;
				foreach ($rows as $i => $row): 
					$pabrik_vendor = $row->pabrik;
					$pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
					?>
					<tr>
						<td><?php echo $kode; ?></td>
						<td><?php echo $row->no; ?></td>
						<td><?php echo $row->dokter; ?></td>
						<td><?php echo $row->p_pagi; ?></td>
						<td><?php echo $row->p_sore; ?></td>
						<td><?php echo $row->p_baru; ?></td>
						<td><?php echo $row->p_lama; ?></td>
						<td><?php echo $row->pribadi; ?></td>
						<td><?php echo $row->asuransi; ?></td>
						<td><?php echo $row->perusahaan; ?></td>
						<td><?php echo $row->bpjs_kes; ?></td>
						<td><?php echo $row->bpjs_tk; ?></td>
						<td><?php echo $row->ks_nik; ?></td>
						<td><?php echo $row->rsa; ?></td>
						<td><?php echo $row->pulang; ?></td>
						<td><?php echo $row->dirawat; ?></td>
						<td><?php echo $row->total; ?></td>
						<td><?php echo $row->batal_daftar; ?></td>
						<td><?php echo $row->resep_dokter; ?></td>
						<td><?php echo $row->non_resep; ?></td>
						<td><?php echo $row->persen_resep; ?></td>
						<td><?php echo $row->pasien_doa; ?></td>
						<td><?php echo $row->pasien_dea; ?></td>
						<td><?php echo $row->pasien_rujuk; ?></td>

					</tr>
					<?php 
					$no++;
				endforeach; 
				?>
				<?php else: ?>
					<tr>
						<td style="font-weight: bold;text-align: center;" colspan="24">TIDAK ADA DATA</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px;">
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
			</tr>
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
			</tr>
		</table>
	</body>
	</html>