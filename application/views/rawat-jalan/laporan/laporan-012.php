<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Poli <!-- <?php echo lang('poli_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-poli">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Jaminan <!-- <?php echo lang('jaminan_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" >Umum</option>
                </select>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Rujukan <!-- <?php echo lang('rujukan_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-rujukan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>

<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NAMA RUJUKAN</th>
				<th>TANGGAL DAFTAR</th>
				<th>REGISTER</th>
				<th>MR</th>
				<th>NAMA PASIEN</th>
				<th>PELAYANAN</th>
				<th>PENJAMIN</th>
				<th>KETERANGAN</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="15">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_012'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.poli_id = $('#search-poli').val();
              	p.jaminan_id = $('#search-jaminan').val();
              	p.rujukan_id = $('#search-rujukan').val();
            }
		},
		 "columns": [
		 	// { "data": "kode" },
	   //    	{ 
	   //    		"data": "tanggal",
	   //    		"render": function (data, type, row, meta) {
	   //    			return moment(data).format('DD-MM-YYYY HH:mm');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "analisis",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return data ? data : "&mdash;";
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "sifat",
	   //    		"searchable": false,
	   //    	},
	   //    	{ 
	   //    		"data": "pabrik",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			let tmp = data;
	   //    			tmp += row.vendor ? ` / ${row.vendor}` : "";
	   //    			return tmp;
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total_ppn",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "grand_total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "persetujuan_by",
	   //    		"searchable": false,
	   //    	},

	      	{ "data": "nama_rujukan"},
	      	{ "data": "tanggal_daftar"},
	      	{ "data": "no_register"},
	      	{ "data": "mr"},
	      	{ "data": "nama_pasien"},
	      	{ "data": "pelayanan"},
	      	{ "data": "penjamin"},
	      	{ "data": "keterangan",
	      	  "searchable": false,
	      	},
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
	  	let jaminan_id = $('#search-jaminan').val();
	  	let rujukan_id = $('#search-rujukan').val();
	  	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${status_id}&rujukan_id=${paket_id}&jaminan_id=${jaminan_id}`;
	  	window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_012'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
	  	let jaminan_id = $('#search-jaminan').val();
	  	let rujukan_id = $('#search-rujukan').val();
	  	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${status_id}&rujukan_id=${paket_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_012'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>