<html>
<body>

	<style type="text/css" media="print">
		body {
			line-height: 1.2em;
			font-size: 8px;
			font-family: Arial, sans-serif;
		}
		h1, h2, h3, h4, h5, h6 {
			font-family: inherit;
			font-weight: 400;
			line-height: 1.5384616;
			color: inherit;
			margin-top: 0;
			margin-bottom: 5px;
			text-align: center;
		}
		h1 {
			font-size: 24px;
		}
		h2 {
			font-size: 16px;
		}
		h3 {
			font-size: 14px;
		}
		h4 {
			font-size: 12px;
		}
		h5 {
			font-size: 10px;
		}
		h6 {
			font-size: 8px;
		}
		table {
			border-collapse: collapse;
			font-size: 8px;
		}
		.table {
			border-spacing: 0;
			width: 100%;
			border: 1px solid #555;
			font-size: 10px;
		}
		.table thead th,
		.table tbody td {
			border: 1px solid #555;
			vertical-align: middle;
			padding: 5px 10px;
			line-height: 1.5384616;
		}
		.table thead th {
			color: #fff;
			background-color: #607D8B;
			font-weight: bold;
			text-align: center;
		}
	</style>

	<style>
		.footer_current_date_user {
			text-align: right;
			color: #d10404;
			font-size: 8px;
			vertical-align: top;
			margin-top: 10px;
		}
	</style>
	<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
	<h4 class="text-center"><?php echo $title; ?></h4>
	<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
	<br>
	<table class="table table-bordered table-striped">
		<thead>
			<tr class="bg-slate">
				<th>No. MR</th>
				<th>Nama Pasien/Nama Ibu</th>
				<th>Umur</th>
				<th>KK/Suami</th>
				<th>Alamat</th>
				<th>Tgl lahir</th>
				<th>Berat Badan (N/T)</th>
				<th>HEP BO</th>
				<th>DPTHBib1</th>
				<th>DPTHBib2</th>
				<th>DPTHBib3</th>
				<th>POLIO 1</th>
				<th>POLIO 2</th>
				<th>POLIO 3</th>
				<th>POLIO 4</th>
				<th>Campak</th>
				<th>Booster DPTHBib</th>
				<th>HEP B1</th>
				<th>HEP B2</th>
				<th>HEP B3</th>
				<th>DPT 1</th>
				<th>DPT 2</th>
				<th>DPT 3</th>
				<th>Combo 1</th>
				<th>Combo 2</th>
				<th>Combo 3</th>
				<th>VIT A Biru</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no_mr = 1;
				foreach ($rows as $i => $row): 
					$pabrik_vendor = $row->pabrik;
					$pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					$grand_total += $row->grand_total;
					?>
					<tr>
						<td><?php echo $no_mr; ?></td>
						<td><?php echo $row->nama_pasien_nama_ibu; ?></td>
						<td><?php echo $row->umur; ?></td>
						<td><?php echo $row->kk_suami; ?></td>
						<td><?php echo $row->tanggal_lahir; ?></td>
						<td><?php echo $row->berat_badan; ?></td>
						<td><?php echo $row->hep_bo; ?></td>
						<td><?php echo $row->DPTHBib1; ?></td>
						<td><?php echo $row->DPTHBib2; ?></td>
						<td><?php echo $row->DPTHBib3; ?></td>
						<td><?php echo $row->polio_1; ?></td>
						<td><?php echo $row->polio_2; ?></td>
						<td><?php echo $row->polio_3; ?></td>
						<td><?php echo $row->polio_4; ?></td>
						<td><?php echo $row->campak; ?></td>
						<td><?php echo $row->booster_DPTHBib; ?></td>
						<td><?php echo $row->hep_b1; ?></td>
						<td><?php echo $row->hep_b2; ?></td>
						<td><?php echo $row->hep_b3; ?></td>
						<td><?php echo $row->dpt_1; ?></td>
						<td><?php echo $row->dpt_2; ?></td>
						<td><?php echo $row->dpt_3; ?></td>
						<td><?php echo $row->combo_1; ?></td>
						<td><?php echo $row->combo_2; ?></td>
						<td><?php echo $row->combo_3; ?></td>
						<td><?php echo $row->vit_a_biru; ?></td>
					</tr>
					<?php 
					$no++;
				endforeach; 
				?>
				<?php else: ?>
					<tr>
						<td style="font-weight: bold;text-align: center;" colspan="27">TIDAK ADA DATA</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px;">
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
			</tr>
			<tr>
				<td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
				<td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
			</tr>
		</table>
	</body>
	</html>