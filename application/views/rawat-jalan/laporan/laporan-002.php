<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_pasien_rawat_jalan_tidak_transaksi_dikasir">Pasien Rawat Jalan Tidak Transaksi Dikasir</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('status_label'); ?>Status</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-status">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Tidak Rujuk Rawat Inap</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('poli_label'); ?>Poli</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-poli">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('jenis_jaminan_label'); ?>Jenis Jaminan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-jenis-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Pribadi</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('dokter_label'); ?>Dokter</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-dokter">
                	<option value="" selected="selected">- Pilih -</option>                
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('jaminan_label'); ?>Jaminan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Umum</option>                 
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>TANGGAL DAFTAR</th>
				<th>JAM DAFTAR</th>
				<th>NAMA PASIEN</th>
				<th>NO REGISTER</th>
				<th>NO MR</th>
				<th>DOKTER</th>
				<th>JAMINAN</th>
				<th>STATUS</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_002'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.poli_id = $('#search-poli').val();
              	p.dokter_id = $('#search-dokter').val();
            }
		},
		 "columns": [
	      	{ "data": "tanggal_daftar" },
	      	{ "data": "jam_daftar" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "no_register" },
	      	{ "data": "no_mr" },
	      	{ "data": "dokter" },
	      	{ "data": "jaminan" },
	      	{ "data": "status" },
	      	{ "data": "tanggal_daftar" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
      	let dokter_id = $('#search-dokter').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${poli_id}&dokter_id=${dokter_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_002'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
      	let dokter_id = $('#search-dokter').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${poli_id}&dokter_id=${dokter_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_002'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>