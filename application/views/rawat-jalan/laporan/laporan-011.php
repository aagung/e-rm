<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_informasi_pendaftaran_pasien_harian">Informasi Pendaftaran Pasien Harian</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-2" for="search_tanggal">Tanggal</label>
			<div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo lang('asal_label'); ?>Asal Pasien</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-asal">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo lang('jaminan_label'); ?>Jaminan</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Umum</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo lang('poli_label'); ?>Poli / Ruang</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-poli">
                	<option value="" selected="selected">- Pilih -</option>                
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>No Reg</th>
				<th>No MR</th>
				<th>Nama Pasien</th>
				<th>asal</th>
				<th>Alamat</th>
				<th>Jaminan</th>
				<th>Jenis Pasien</th>
				<th>Tanggal Masuk</th>
				<th>Jam Daftar</th>
				<th>User</th>
				<th>Kasir</th>
				<th>Dirawat</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_011'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.poli_id = $('#search-poli').val();
              	p.asal_id = $('#search-asal').val();
            }
		},
		 "columns": [
	      	{ "data": "no_reg" },
	      	{ "data": "no_mr" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "asal" },
	      	{ "data": "alamat" },
	      	{ "data": "jaminan" },
	      	{ "data": "jenis_pasien" },
	      	{ "data": "tanggal_masuk" },
	      	{ "data": "jam_daftar" },
	      	{ "data": "user" },
	      	{ "data": "Kasir" },
	      	{ "data": "dirawat" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
      	let asal_id = $('#search-asal').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${poli_id}&asal_id=${asal_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_011'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let poli_id = $('#search-poli').val();
      	let asal_id = $('#search-asal').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&poli_id=${poli_id}&asal_id=${asal_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_011'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>