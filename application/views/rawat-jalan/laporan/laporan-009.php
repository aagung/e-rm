<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Jenis Paket <!-- <?php echo lang('paket_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-paket">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" >Haji</option>
                </select>
            </div>
		</div>
	</div>
</div>

<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Status <!-- <?php echo lang('status_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-status">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" >Sudah Kuitansi</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Jaminan <!-- <?php echo lang('jaminan_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-jaminan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" >Umum</option>
                </select>
            </div>
		</div>
	</div>
</div>


<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO REGISTER</th>
				<th>NO MEDREC</th>
				<th>NAMA PASIEN</th>
				<th>ALAMAT</th>
				<th>JAMINAN</th>
				<th>TGL MASUK</th>
				<th>JAM DAFTAR</th>
				<th>JENIS PAKET</th>
				<th>USER</th>
				<th>NO KWITANSI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="15">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
	    autoApply: true,
	    locale: {
	        format: "DD/MM/YYYY",
	    },
	    startDate: moment(),
	    endDate: moment(),
	});

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
	    "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_009'); ?>",
			"type": "POST",
	        "data": function(p) {
	        	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
	            p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
	          	p.status_id = $('#search-status').val();
	          	p.paket_id = $('#search-paket').val();
	          	p.jaminan_id = $('#search-jaminan').val();
	        }
		},
		 "columns": [
		 	// { "data": "kode" },
	   //    	{ 
	   //    		"data": "tanggal",
	   //    		"render": function (data, type, row, meta) {
	   //    			return moment(data).format('DD-MM-YYYY HH:mm');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "analisis",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return data ? data : "&mdash;";
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "sifat",
	   //    		"searchable": false,
	   //    	},
	   //    	{ 
	   //    		"data": "pabrik",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			let tmp = data;
	   //    			tmp += row.vendor ? ` / ${row.vendor}` : "";
	   //    			return tmp;
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total_ppn",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "grand_total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "persetujuan_by",
	   //    		"searchable": false,
	   //    	},

	      	{ "data": "no_register"},
	      	{ "data": "no_medrec"},
	      	{ "data": "nama_pasien"},
	      	{ "data": "alamat"},
	      	{ "data": "jaminan"},
	      	{ "data": "tgl_masuk",
	      	  "render": function (data, type, row, meta) {
	      	  	  return moment(data).format('DD-MM-YYYY');
		       }
	      	},
	      	{ "data": "jam_daftar"},
	      	{ "data": "jenis_paket"},
	      	{ "data": "user"},
	      	{ "data": "no_kwitansi"},
	    ],
	});

	$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
	    table.draw();
	});

	$("#btn-search_tanggal").click(function () {
	    $("#search-tanggal").data('daterangepicker').toggle();
	});

	$(".input-search").on('change', function() {
	  table.draw();
	});

	$("#btn-print-excel").click(function () {
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
	    let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
	  	let status_id = $('#search-status').val();
	  	let paket_id = $('#search-paket').val();
	  	let jaminan_id = $('#search-jaminan').val();
	  	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&status_id=${status_id}&paket_id=${paket_id}&jaminan_id=${jaminan_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_009'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
	    let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
	  	let status_id = $('#search-status').val();
	  	let paket_id = $('#search-paket').val();
	  	let jaminan_id = $('#search-jaminan').val();
	  	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&status_id=${status_id}&paket_id=${paket_id}&jaminan_id=${jaminan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_009'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
	})();
</script>