<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4" for="search_tanggal">Tanggal</label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
						<i class="icon-calendar22"></i>
					</span>
					<input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Polio</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-polio">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Dokter</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-dokter">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Jaminan</label>
			<div class="col-md-8">    
				<select class="form-control input-search" id="search-jaminan">
					<option value="" selected="selected">- Pilih -</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO</th>
				<th>DOKTER</th>
				<th>P. PAGI</th>
				<th>P. SORE</th>
				<th>P. BARU</th>
				<th>P. LAMA</th>
				<th>PRIBADI</th>
				<th>ASURANSI</th>
				<th>PERUSAHAAN</th>
				<th>BPJS KES</th>
				<th>BPJS TK</th>
				<th>KS-NIK</th>
				<th>RSA</th>
				<th>PULANG</th>
				<th>DIRAWAT</th>
				<th>TOTAL</th>
				<th>BATAL DAFTAR</th>
				<th>RESEP DOKTER</th>
				<th>NON RESEP</th>
				<th>% RESEP</th>
				<th>RESEP TERJUAL</th>
				<th>PASIEN DOA</th>
				<th>PASIEN DEA</th>
				<th>PASIEN RUJUK</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="24">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	(function () {
		$("select").select2();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(),
			endDate: moment(),
		});

		var table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
				"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_001'); ?>",
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
					p.polio_id = $('#search-polio').val();
					p.dokter_id = $('#search-dokter').val();
				}
			},
			"columns": [
			{ "data": "kode" },
			{ "data": "no" },
			{ "data": "dokter" },
			{ "data": "p_pagi" },
			{ "data": "p_sore" },
			{ "data": "p_baru" },
			{ "data": "p_lama" },
			{ "data": "pribadi" },
			{ "data": "asuransi" },
			{ "data": "perusahaan" },
			{ "data": "bpjs_kes" },
			{ "data": "bpjs_tk" },
			{ "data": "ks-nik" },
			{ "data": "rsa" },
			{ "data": "pulang" },
			{ "data": "dirawat" },
			{ "data": "total" },
			{ "data": "batal_daftar" },
			{ "data": "resep_dokter" },
			{ "data": "non_resep" },
			{ "data": "persen_resep" },
			{ "data": "resep_terjual" },
			{ "data": "pasien_doa" },
			{ "data" :"pasien_dea"},
			{ "data": "pasien_rujuk" },
			],
		});

		$("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn-search_tanggal").click(function () {
			$("#search-tanggal").data('daterangepicker').toggle();
		});

		$(".input-search").on('change', function() {
			table.draw();
		});

		$("#btn-print-excel").click(function () {
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let polio_id = $('#search-polio').val();
			let dokter_id = $('#search-dokter').val();
			let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&polio_id=${polio_id}&dokter_id=${dokter_id}`;
			window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_001'); ?>${param}`);
		});

		$("#btn-print-pdf").click(function () {
			let iframeHeight = $(window).height() - 220;
			let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
			let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
			let polio_id = $('#search-polio').val();
			let dokter_id = $('#search-dokter').val();
			let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&polio_id=${polio_id}&dokter_id=${dokter_id}`;
			$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
			$('#modal-print').modal('show');
		});
	})();
</script>