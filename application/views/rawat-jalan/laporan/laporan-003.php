<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"> Kecamatan <!-- <?php echo lang('kecamatan_label'); ?> --></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-kecamatan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>


<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. MR</th>
				<th>NAMA PASIEN</th>
				<th>NAMA IBU</th>
				<th>UMUR</th>
				<th>KK/SUAMI</th>
				<th>ALAMAT</th>
				<th>K1</th>
				<th>K4</th>
				<th>USIA < 20 / > 35 THN</th>
				<th>JARAK 2 < THN</th>
				<th>JML ANAK > 4</th>
				<th>TB > 145</th>
				<th>BB TRIW3 < 45 KG</th>
				<th>LILA < 23,5 CM</th>
				<th>HB < 11 GR%</th>
				<th>FAKTOR RESIKO LAIN</th>
				<th>KOMPLIKASI KEHAMILAN</th>
				<th>IMUNISASI TT1-TT5</th>
				<th>FE1</th>
				<th>FE3</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="20">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_003'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.kecamatan_id = $('#search-kecamatan').val();
            }
		},
		 "columns": [
		 	// { "data": "kode" },
	   //    	{ 
	   //    		"data": "tanggal",
	   //    		"render": function (data, type, row, meta) {
	   //    			return moment(data).format('DD-MM-YYYY HH:mm');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "analisis",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return data ? data : "&mdash;";
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "sifat",
	   //    		"searchable": false,
	   //    	},
	   //    	{ 
	   //    		"data": "pabrik",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			let tmp = data;
	   //    			tmp += row.vendor ? ` / ${row.vendor}` : "";
	   //    			return tmp;
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "total_ppn",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "grand_total",
	   //    		"searchable": false,
	   //    		"render": function (data, type, row, meta) {
	   //    			return 'Rp. ' + numeral(data).format('0.0,');
		  //       }
	   //    	},
	   //    	{ 
	   //    		"data": "persetujuan_by",
	   //    		"searchable": false,
	   //    	},

	      	{ "data": "no_mr" },
	      	{ "data": "nama_pasien"},
	      	{ "data": "nama_ibu"},
	      	{ "data": "umur"},
	      	{ "data": "kk_suami"},
	      	{ "data": "alamat"},
	      	{ "data": "k1"},
	      	{ "data": "k4"},
	      	{ "data": "usia_<_20_>_35"},
	      	{ "data": "jarak_<_2_thn"},
	      	{ "data": "jml_anak_>_4"},
	      	{ "data": "tb_<_145_cm"},
	      	{ "data": "bb_triw3_<_45_kg"},
	      	{ "data": "lila_<_23.5_cm"},
	      	{ "data": "hb_<_11_gr%"},
	      	{ "data": "faktor_resiko_lain"},
	      	{ "data": "komplikasi_kehamilan"},
	      	{ "data": "imunisasi_tt1_tt5"},
	      	{ "data": "fe1"},
	      	{ "data": "fe3"},
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let kecamatan_id = $('#search-kecamatan').val();
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&kecamatan_id=${kecamatan_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_003'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let kecamatan_id = $('#search-kecamatan').val();
      	let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&kecamatan_id=${kecamatan_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_003'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>