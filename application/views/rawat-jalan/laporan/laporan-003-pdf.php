<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>NO. MR</th>
			<th>NAMA PASIEN</th>
			<th>NAMA IBU</th>
			<th>UMUR</th>
			<th>KK/SUAMI</th>
			<th>ALAMAT</th>
			<th>K1</th>
			<th>K4</th>
			<th>USIA < 20 / > 35 THN</th>
			<th>JARAK 2 < THN</th>
			<th>JML ANAK > 4</th>
			<th>TB > 145</th>
			<th>BB TRIW3 < 45 KG</th>
			<th>LILA < 23,5 CM</th>
			<th>HB < 11 GR%</th>
			<th>FAKTOR RESIKO LAIN</th>
			<th>KOMPLIKASI KEHAMILAN</th>
			<th>IMUNISASI TT1-TT5</th>
			<th>FE1</th>
			<th>FE3</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row): 
					// $pabrik_vendor = $row->pabrik;
     //                $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
					// $grand_total += $row->grand_total;
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $row->nama_pasien; ?></td>
			<td><?php echo $row->nama_ibu; ?></td>
			<td><?php echo $row->umur; ?></td>
			<td><?php echo $row->kk_suami; ?></td>
			<td><?php echo $row->alamat; ?></td>
			<td><?php echo $row->k1; ?></td>
			<td><?php echo $row->k4; ?></td>
			<td><?php echo $row->usia_20_35_thn; ?></td>
			<td><?php echo $row->jarak_2_thn; ?></td>
			<td><?php echo $row->jml_anak_4; ?></td>
			<td><?php echo $row->tb_145; ?></td>
			<td><?php echo $row->bb_triw3_45_kg; ?></td>
			<td><?php echo $row->lila_23_5_cm; ?></td>
			<td><?php echo $row->hb_11_gr; ?></td>
			<td><?php echo $row->faktor_resiko_lain; ?></td>
			<td><?php echo $row->komplikasi_kehamilan; ?></td>
			<td><?php echo $row->imunisasi_tt1_tt5; ?></td>
			<td><?php echo $row->fe1; ?></td>
			<td><?php echo $row->fe3; ?></td>
			
		<!-- 	<td><?php echo $row->kode; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($row->tanggal)); ?></td>
			<td><?php echo $row->analisis ? $row->analisis : "-"; ?></td>
			<td><?php echo $this->config->item('sifat_po')[$row->sifat]; ?></td>
			<td><?php echo $pabrik_vendor; ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->total, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->total_ppn, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->grand_total, 2, ",", "."); ?></td>
			<td><?php echo $row->persetujuan_by; ?></td> -->
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<!-- <tr>
			<td style="font-weight: bold;text-align: right;" colspan="8">TOTAL</td>
			<td style="text-align: right;font-weight: bold;"><?php echo number_format($grand_total, 2, ",", "."); ?></td>
			<td>&nbsp;</td>
		</tr> -->
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="20">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>