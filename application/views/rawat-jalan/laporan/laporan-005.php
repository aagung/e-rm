<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_pelayanan_bayi_baru_lahir_neonatal">Pelayanan Bayi Baru Lahir Dan Neonatal</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('jenis_laporan_label'); ?>Jenis Laporan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-jenis-laporan">
                	<option value="" selected="selected">- Pilih -</option>
                	<option value="" selected="selected">Pelayanan Kesehatan Ibu Hamil</option>
                </select>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-3"><?php echo lang('kecamatan_label'); ?>Kecamatan</label>
			<div class="col-md-9">    
                <select class="form-control input-search" id="search-kecamatan">
                	<option value="" selected="selected">- Pilih -</option>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>No MR</th>
				<th>Nama Pasien / Nama Ibu</th>
				<th>Umur</th>
				<th>KK / Suami</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Keadaan Umum / Komplikasi</th>
				<th>BB Lahir</th>
				<th>PB Lahir</th>
				<th>Jenis Kelamin</th>
				<th>KN 1 (0-3hr)</th>
				<th>KN 2 (3-8hr)</th>
				<th>KN 3 (8-28hr)</th>
				<th>Komplikasi / Neonatal Sakit</th>
				<th>HB 0</th>
				<th>Polio 1</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="16">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/rawat_jalan/laporan/laporan_005'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
            }
		},
		 "columns": [
	      	{ "data": "no_mr" },
	      	{ "data": "nama_pasien_ibu" },
	      	{ "data": "umur" },
	      	{ "data": "kk_suami" },
	      	{ "data": "alamat" },
	      	{ "data": "tanggal_lahir" },
	      	{ "data": "keadaan_umum_komplikasi" },
	      	{ "data": "bb_lahir" },
	      	{ "data": "pb_lahir" },
	      	{ "data": "jenis_kelamin" },
	      	{ "data": "kn_1" },
	      	{ "data": "kn_2" },
	      	{ "data": "kn_3" },
	      	{ "data": "komplikasi_neonatal_sakit" },
	      	{ "data": "hb_0" },
	      	{ "data": "polio_1" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&pabrik_id=${pabrik_id}&vendor_id=${vendor_id}`;
		window.location.assign(`<?php echo site_url('api/rawat_jalan/laporan/print_005'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&pabrik_id=${pabrik_id}&vendor_id=${vendor_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/rawat_jalan/laporan/print_005'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>