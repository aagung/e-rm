<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>

        <title><?php echo $template['title']; ?></title>

        <link rel="icon" href="<?php echo assets_url('img/favicon.png') ?>" type="image/png">
         <?php echo $template['css']; ?>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo css_url('icons/icomoon/styles.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('icons/webfont-medical-icons/wfmi-style.css'); ?>" rel="stylesheet">
        <link href="<?php echo css_url('bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('core.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('components.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('colors.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('vmt.custom.css') ?>" rel="stylesheet">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js');?>"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery_ui/core.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/wizards/form_wizard/form.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/wizards/form_wizard/form_wizard.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jasny_bootstrap.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/validation/validate.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/notifications/sweet_alert.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo bower_url('moment/min/moment-with-locales.min.js') ?>"></script>
        
        <?php echo $template['js_header']; ?>
        <script type="text/javascript" src="<?php echo js_url('core/app.js');?>"></script>
        <script type="text/javascript" src="<?php echo js_url('app.js');?>"></script>
        <!-- /theme JS files -->

    </head>
    
    <body class="login-container">
        <!-- Page container -->
        <div class="page-container">
            <div class="login-logo">
                <div class="col-md-offset-1">
                    <img src="<?= base_url(get_option('rs_logo')) ?>" width="200" />
                </div>
            </div>

            <div class="content mt-20">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $template['content']; ?>
                    </div>
                </div>

                <div class="footer text-muted text-center">
                    &copy; <?php echo date('Y'); ?> Venus Media Teknologi
                </div>
            </div>
        </div>
        <!-- /page container -->
    <script type="text/javascript">
    // Select2 selects
    $('.select').select2();

    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: Infinity
    });

    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });

    </script>
    </body>
    <?php
    if (isset($template['script']) && $template['script'] != '')
        $this->load->view($template['script']);
    ?>
</html>
