<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr class="bg-slate" role="row">
                <th class="text-center" rowspan="3" colspan="1">TGL</th>
                <th class="text-center" colspan="6" rowspan="1">RAWAT JALAN</th>
                <th class="text-center" rowspan="1" colspan="15">RAWAT INAP</th>
                <th class="text-center" rowspan="3" colspan="1">PERI</th>
                <th class="text-center" rowspan="3" colspan="1">ICU</th>
                <th class="text-center" rowspan="3" colspan="1">UGD</th>
                <th class="text-center" rowspan="3" colspan="1">VK</th>
                <th class="text-center" rowspan="3" colspan="1">HCU</th>
                <th class="text-center" rowspan="3" colspan="1">FLAM</th>
                <th class="text-center" rowspan="3" colspan="1">JML</th>
            </tr>
            <tr class="bg-slate" role="row">
                <th class="text-center" rowspan="2" colspan="1">APS</th>
                <th class="text-center" rowspan="2" colspan="1">DLM</th>
                <th class="text-center" rowspan="2" colspan="1">LUAR</th>
                <th class="text-center" rowspan="2" colspan="1">KARY</th>
                <th class="text-center" rowspan="2" colspan="1">CK-UP</th>
                <th class="text-center" rowspan="2" colspan="1">JKN</th>
                <th class="text-center" rowspan="1" colspan="5">PERAWATAN UMUM</th>
                <th class="text-center" rowspan="1" colspan="5">KEBIDANAN</th>
                <th class="text-center" rowspan="1" colspan="5">PERAWATAN ANAK</th>
            </tr>
            <tr>
                <th class="text-center" rowspan="1" colspan="1">III</th>
                <th class="text-center" rowspan="1" colspan="1">II</th>
                <th class="text-center" rowspan="1" colspan="1">I</th>
                <th class="text-center" rowspan="1" colspan="1">VIP</th>
                <th class="text-center" rowspan="1" colspan="1">VVIP</th>
                <th class="text-center" rowspan="1" colspan="1">III</th>
                <th class="text-center" rowspan="1" colspan="1">II</th>
                <th class="text-center" rowspan="1" colspan="1">I</th>
                <th class="text-center" rowspan="1" colspan="1">VVIP</th>
                <th class="text-center" rowspan="1" colspan="1">VIP</th>
                <th class="text-center" rowspan="1" colspan="1">III</th>
                <th class="text-center" rowspan="1" colspan="1">II</th>
                <th class="text-center" rowspan="1" colspan="1">I</th>
                <th class="text-center" rowspan="1" colspan="1">VIP</th>
                <th class="text-center" rowspan="1" colspan="1">VVIP</th>
            </tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="29">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/laboratorium/laporan/laporan_002'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
            }
		},
		 "columns": [
	      	{ "data": "tanggal" },
	      	{ "data": "rawat_jalan" },
	      	{ "data": "rawat_inap" },
	      	{ "data": "peri" },
	      	{ "data": "icu" },
	      	{ "data": "ugd" },
	      	{ "data": "vk" },
	      	{ "data": "hcu" },
	      	{ "data": "flam" },
	      	{ "data": "jml" },
	      	{ "data": "aps" },
	      	{ "data": "dlm" },
	      	{ "data": "luar" },
	      	{ "data": "kary" },
	      	{ "data": "ck_up" },
	      	{ "data": "kjn" },
	      	{ "data": "keperawatan_umum" },
	      	{ "data": "kebidanan" },
	      	{ "data": "perawatan_anak" },
	      	{ "data": "III" },
	      	{ "data": "II" },
	      	{ "data": "I" },
	      	{ "data": "VIP" },
	      	{ "data": "VVIP" },
	      	{ "data": "III" },
	      	{ "data": "II" },
	      	{ "data": "VVIP" },
	      	{ "data": "VIP" },
	      	{ "data": "III" },
	      	{ "data": "II" },
	      	{ "data": "I" },
	      	{ "data": "VIP" },
	      	{ "data": "VVIP" },
	      	
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		window.location.assign(`<?php echo site_url('api/laboratorium/laporan/print_002'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/laboratorium/laporan/print_002'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>