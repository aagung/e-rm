<html>
<body>

    <style type="text/css" media="print">
        body {
           line-height: 1.2em;
           font-size: 8px;
           font-family: Arial, sans-serif;
       }
       h1, h2, h3, h4, h5, h6 {
           font-family: inherit;
           font-weight: 400;
           line-height: 1.5384616;
           color: inherit;
           margin-top: 0;
           margin-bottom: 5px;
           text-align: center;
       }
       h1 {
           font-size: 24px;
       }
       h2 {
           font-size: 16px;
       }
       h3 {
           font-size: 14px;
       }
       h4 {
           font-size: 12px;
       }
       h5 {
           font-size: 10px;
       }
       h6 {
           font-size: 8px;
       }
       table {
           border-collapse: collapse;
           font-size: 8px;
       }
       .table {
        border-spacing: 0;
        width: 100%;
        border: 1px solid #555;
        font-size: 10px;
    }
    .table thead th,
    .table tbody td {
       border: 1px solid #555;
       vertical-align: middle;
       padding: 5px 10px;
       line-height: 1.5384616;
   }
   .table thead th {
       color: #fff;
       background-color: #607D8B;
       font-weight: bold;
       text-align: center;
   }
</style>

<style>
    .footer_current_date_user {
       text-align: right;
       color: #d10404;
       font-size: 8px;
       vertical-align: top;
       margin-top: 10px;
   }
</style>
<h3 class="text-center"><?php echo $this->config->item('rs_nama') ? strtoupper($this->config->item('rs_nama')) : "RUMAH SAKIT ANANDA"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate" role="row">
            <th class="text-center" rowspan="3" colspan="1">TGL</th>
            <th class="text-center" colspan="6" rowspan="1">RAWAT JALAN</th>
            <th class="text-center" rowspan="1" colspan="15">RAWAT INAP</th>
            <th class="text-center" rowspan="3" colspan="1">PERI</th>
            <th class="text-center" rowspan="3" colspan="1">ICU</th>
            <th class="text-center" rowspan="3" colspan="1">UGD</th>
            <th class="text-center" rowspan="3" colspan="1">VK</th>
            <th class="text-center" rowspan="3" colspan="1">HCU</th>
            <th class="text-center" rowspan="3" colspan="1">FLAM</th>
            <th class="text-center" rowspan="3" colspan="1">JML</th>
        </tr>
        <tr class="bg-slate" role="row">
            <th class="text-center" rowspan="2" colspan="1">APS</th>
            <th class="text-center" rowspan="2" colspan="1">DLM</th>
            <th class="text-center" rowspan="2" colspan="1">LUAR</th>
            <th class="text-center" rowspan="2" colspan="1">KARY</th>
            <th class="text-center" rowspan="2" colspan="1">CK-UP</th>
            <th class="text-center" rowspan="2" colspan="1">JKN</th>
            <th class="text-center" rowspan="1" colspan="5">PERAWATAN UMUM</th>
            <th class="text-center" rowspan="1" colspan="5">KEBIDANAN</th>
            <th class="text-center" rowspan="1" colspan="5">PERAWATAN ANAK</th>
        </tr>
        <tr>
            <th class="text-center" rowspan="1" colspan="1">III</th>
            <th class="text-center" rowspan="1" colspan="1">II</th>
            <th class="text-center" rowspan="1" colspan="1">I</th>
            <th class="text-center" rowspan="1" colspan="1">VIP</th>
            <th class="text-center" rowspan="1" colspan="1">VVIP</th>
            <th class="text-center" rowspan="1" colspan="1">III</th>
            <th class="text-center" rowspan="1" colspan="1">II</th>
            <th class="text-center" rowspan="1" colspan="1">I</th>
            <th class="text-center" rowspan="1" colspan="1">VVIP</th>
            <th class="text-center" rowspan="1" colspan="1">VIP</th>
            <th class="text-center" rowspan="1" colspan="1">III</th>
            <th class="text-center" rowspan="1" colspan="1">II</th>
            <th class="text-center" rowspan="1" colspan="1">I</th>
            <th class="text-center" rowspan="1" colspan="1">VIP</th>
            <th class="text-center" rowspan="1" colspan="1">VVIP</th>
        </tr>
    </thead>
    <tbody>
      <?php 
      $grand_total = 0;
      if($total_rows > 0):
        $no = 1;
        foreach ($rows as $i => $row): 
           $pabrik_vendor = $row->pabrik;
           $pabrik_vendor .= $row->vendor ? " / {$row->vendor}" : "";
           $grand_total += $row->grand_total;
           ?>
           <tr>
             <td><?php echo $tanggal; ?></td>
             <td><?php echo $row->rawat_jalan; ?></td>
             <td><?php echo $row->rawat_inap; ?></td>
             <td><?php echo $row->peri; ?></td>
             <td><?php echo $row->icu; ?></td>
             <td><?php echo $row->ugd; ?></td>
             <td><?php echo $row->vk; ?></td>
             <td><?php echo $row->hcu; ?></td>
             <td><?php echo $row->flam; ?></td>
             <td><?php echo $row->jml; ?></td>
             <td><?php echo $row->aps; ?></td>
             <td><?php echo $row->dlm; ?></td>
             <td><?php echo $row->luar; ?></td>
             <td><?php echo $row->kary; ?></td>
             <td><?php echo $row->ck_up; ?></td>
             <td><?php echo $row->kjn; ?></td>
             <td><?php echo $row->total; ?></td>
             <td><?php echo $row->keperawatan_umum; ?></td>
             <td><?php echo $row->kebidanan; ?></td>
             <td><?php echo $row->perawatan_anak; ?></td>
             <td><?php echo $row->III; ?></td>
             <td><?php echo $row->II; ?></td>
             <td><?php echo $row->I; ?></td>
             <td><?php echo $row->VIP; ?></td>
             <td><?php echo $row->VVIP; ?></td>
             <td><?php echo $row->III; ?></td>
             <td><?php echo $row->II; ?></td>
             <td><?php echo $row->VVIP; ?></td>
             <td><?php echo $row->VIP; ?></td>
             <td><?php echo $row->III; ?></td>
             <td><?php echo $row->II; ?></td>
             <td><?php echo $row->I; ?></td>
             <td><?php echo $row->VIP; ?></td>
             <td><?php echo $row->VVIP; ?></td>
         </tr>
         <?php 
         $no++;
     endforeach; 
     ?>
     <?php else: ?>
      <tr>
         <td style="font-weight: bold;text-align: center;" colspan="29">TIDAK ADA DATA</td>
     </tr>
 <?php endif; ?>
</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Bekasi, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>