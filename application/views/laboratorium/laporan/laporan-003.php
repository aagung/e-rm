<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<label class="control-label col-md-12" for="search_transaksi_unit_transfusi_darah">Transaksi Unit Transfusi Darah</label>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-2" for="search_tanggal">Tanggal</label>
			<div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search-tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-1 col-md-offset-11">
		<button type="reset" class="btn btn-secondary reset-button">
            Reset
        </button>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>No</th>
				<th>No Nota</th>
				<th>Tanggal Struk</th>
				<th>No Register</th>
				<th>Nama Pasien</th>
				<th>No RM</th>
				<th>Pemeriksaan</th>
				<th>Harga</th>
				<th>Jaminan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Total</td>
			</tr>
			<tr>
				<td class="text-center" colspan="9">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
(function () {
	$("select").select2();

	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(),
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
        "ajax": {
			"url": "<?php echo site_url('api/laboratorium/laporan/laporan_003'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');

            }
		},
		"columns": [
	      	{ "data": "no" },
	      	{ "data": "no_nota" },
	      	{ "data": "tanggal_struk" },
	      	{ "data": "no_register" },
	      	{ "data": "nama_pasien" },
	      	{ "data": "no_rm" },
	      	{ "data": "pemeriksaan" },
	      	{ "data": "harga" },
	      	{ "data": "jaminan" },
	    ],
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&kelas_id=${kelas_id}`;
		window.location.assign(`<?php echo site_url('api/laboratorium/laporan/print_003'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&ruang_id=${ruang_id}&kelas_id=${kelas_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/laboratorium/laporan/print_003'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>