<tr>
	<td colspan="2" rowspan="2">Diagnosis sekunder :
		<div class="form-group">
			<label class="col-lg-1 control-label">1 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_1" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">2 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_2" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">3 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_3" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">4 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_4" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">5 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_5" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">6 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_6" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-1 control-label">7 .</label>
			<div class="col-lg-11">
				<input type="text" name="diagnosa_sekunder_7" class="form-control input-xs" name="no_rm" value="" >
			</div>
		</div>
	</td>
	<td colspan="3">Alasan pemindahan pasien :
		<div class="panel-body">
			<div class="form-group">
				1. Kondisi Pasien : memburuk / stabil / tidak ada perubahan
			</div>
			<div class="form-group">
				2. Fasilitas : kurang memadai / membutuhkan peralatan yang lebih baik
			</div>
			<div class="form-group">
				3.Tenaga : membutuhkan tenaga yang lebih ahli / jumlah tenaga kurang
			</div>
			<div class="form-group">
				4. Lain-lain <br>
				<label class="col-lg-2 control-label">sebutkan</label>
				<div class="col-lg-9">
					<input type="text" name="alasan_pemindahan_pasien_lain_lain" class="form-control input-xs"  value="" >
				</div>
			</div>
		</div>
	</td>
</tr>
<tr>
	<td colspan="3">Metode pemindahan :
		<div class="panel-body">
			<div class="form-group">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="metode_pemindahan_kursi_roda" class="styled">
						Kursi roda
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="metode_pemindahan_tempat_tidur" class="styled">
						<input type="checkbox" name="" class="styled">
						Tempat tidur
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="metode_pemindahan_brankar" class="styled">
						<input type="checkbox" name="" class="styled">
						Brankar
					</label>
				</div>
			</div>
		</div>
	</td>
</tr>