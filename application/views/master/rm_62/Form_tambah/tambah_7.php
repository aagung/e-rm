<tr>
	<td colspan="5">Keadaan pasien saat pindah :
		<div class="form-group">
			<div class="form-group col-md-5">
				<label class="col-lg-4 control-label">Keadaan umum</label>
				<div class="col-lg-8">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_keadaan_umum" value="" >
				</div>
			</div>
			<div class="form-group col-md-3">
				<label class="col-lg-6 control-label">Tekanan darah</label>
				<div class="col-lg-6">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_tekanan_darah" value="" >
				</div>
			</div>
			<div class="form-group col-md-2">
				<label class="col-lg-4 control-label">Suhu</label>
				<div class="col-lg-8">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_suhu" value="" >
				</div>
			</div>
			<div class="form-group col-md-2">
				<label class="col-lg-4 control-label">Nadi</label>
				<div class="col-lg-8">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_nadi" value="" >
				</div>
			</div>
			<div class="form-group col-md-5">
				<label class="col-lg-4 control-label">Kesadaran</label>
				<div class="col-lg-8">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_kesadaran" value="" >
				</div>
			</div>
			<div class="form-group col-md-3">
				<label class="col-lg-6 control-label">Pernafasan</label>
				<div class="col-lg-6">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_pernafasan" value="" >
				</div>
			</div>
			<div class="form-group col-md-4">
				<label class="col-lg-4 control-label">Status nyeri</label>
				<div class="col-lg-8">
					<input type="text" class="form-control input-xs" name="keadaan_pasien_status_nyeri" value="" >
				</div>
			</div>
		</div>
	</td>
</tr>