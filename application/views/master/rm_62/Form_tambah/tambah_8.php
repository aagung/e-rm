<tr>
	<th colspan="2"><center>INFORMASI MEDIS</center></th>
	<th colspan="3">Pendampingan saat pasien pindah
		<div class="form-group">
			<label class="col-lg-3 control-label">Nama Petugas :</label>
			<div class="col-lg-7">
				<input type="text" class="form-control input-xs" name="nama_petugas" value="" >
			</div>
		</div>
	</th>
</tr>
<tr>
	<td colspan="2">Beri tanda pada kondisi yang paling sesuai
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="disabilitas" class="styled">
						Disabilitas
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="amputasi" class="styled">
						Amputasi
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="paralisis" class="styled">
						Paralisis
					</label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="kontraktur" class="styled">
						Kontraktur
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">


					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="ulkus_dekubitus" class="styled">
						Ulkus dekubitus
					</label>
				</div>
			</div>
			<span class="col-md-12" style="margin-top: 3%">Gangguan</span>
			<div class="col-md-6">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="gangguan_mental" class="styled">
						Mental
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="gangguan_pendengaran" class="styled">
						Pendengaran
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="gangguan_sensasi" class="styled">
						Sensasi
					</label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="gangguan_bicara" class="styled">
						Bicara
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">

					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="gangguan_penglihatan" class="styled">
						Penglihatan
					</label>
				</div>
			</div>

			<span class="col-md-12" style="margin-top: 3%">Inkontinensia</span>
			<div class="col-md-3">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="inkontinesia_urin" class="styled">
						Urin
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="inkontinesia_alvi" class="styled">
						Alvi
					</label>
				</div>
			</div>
			<div class="col-md-5">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="inkontinesia_saliva" class="styled">
						Saliva
					</label>
				</div>
			</div>
			<span class="col-md-12" style="margin-top: 3%">Potensial untuk dilakukan rehabilitasi</span>
			<div class="col-md-3">
				<div class="radio no-padding">
					<label class="radio-inline">
						<input type="radio" name="potensial_untuk_dilakukan_rehabilitasi" class="styled">
						Baik
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="radio no-padding">
					<label class="radio-inline">
						<input type="radio" name="potensial_untuk_dilakukan_rehabilitasi" class="styled">
						Sedang
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="radio no-padding">
					<label class="radio-inline">
						<input type="radio" name="potensial_untuk_dilakukan_rehabilitasi" class="styled">
						Buruk
					</label>
				</div>
			</div>
		</div>
		<div class="form-group">
		</div>
		<div class="form-group">
			<div class="table-responsive">
				<table border="1" class="table table-xxs">
					<tr>
						<th colspan="2">Status Kemandirian</th>
						<th class="demo-vertical-text">Mandiri</th>
						<th class="demo-vertical-text">Butuh Bantuan</th>
						<th class="demo-vertical-text">Tidak dapat melakukan</th>
					</tr>
					<tr>
						<td rowspan="2" width="5px">Aktifitas di tempat tidur</td>
						<td width="10px">Berguling</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td width="10px">Duduk</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td rowspan="5">Higiene pribadi</td>
						<td>Wajah,<br>rambut,<br>tangan</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Batang tubuh & perinium</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Ekstrimitas bawah</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Traktus digestivus</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Traktus digestivus</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td rowspan="3">Berpakaian</td>
						<td>Ekstrimitas atas</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Batang tubuh</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Ekstrimitas bawah</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="2">Makan</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td rowspan="2">Pergerakan</td>
						<td>Jalan kaki</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Roda</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="form-group" style="margin-top: 11%"> Terapi saat pindah :
			<div class="table-responsive">
				<table  class="table table-hover">
					<tr>
						<th width="100px">Nama Obat</th>
						<th>Jumlah</th>
						<th>Dosis</th>
						<th>Frekuensi</th>
						<th>Cara pemberian</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="form-group" style="margin-top: 15%">
			<label class="col-md-6">Petugas yang mengirim</label>Jam :
		</div>
		<label style="margin-top: 20%">Tanda tangan & Nama lengkap</label>
	</td>
	<td colspan="3">Pemeriksaan fisik
		<div class="form-group"> Status Generalis (temuan yang sigfikan)
			<textarea rows="6" cols=60 name="status_generalis"></textarea>
		</div>
		<div class="form-group"> Status Lokalis (temuan yang signifikan)
			<textarea rows="6" cols=60 name="status_lokalis"></textarea>
		</div>
		<div class="form-group" > Pemeriksaan penunjang / diagnostik yang sudah dilakukan (EKG,lab,dll) :
			<textarea rows="8" cols=60 name="pemeriksaan_penunjang"></textarea>
		</div>
		<div class="form-group">Intervensi / tindakan yang sudah di lakukan :
			<textarea rows="8" cols=60 name="intervensi"></textarea>
		</div>
		<div class="form-group">Diet :
			<textarea rows="4" cols=60 name="diet"></textarea>
		</div>
		<div class="form-group">Rencana perawatan selanjutnya :
			<textarea rows="8" cols=60 name="pencana_perawatan_selanjutnya"></textarea>
		</div>
		<div class="form-group" style="margin-top: 52%">
			<div class="table-responsive">
				<table  class="table table-hover">
					<tr>
						<th>Nama Obat</th>
						<th>Jumlah</th>
						<th>Dosis</th>
						<th>Frekuensi</th>
						<th>Cara pemberian</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="form-group" style="margin-top: 7%">
			<div class="form-group col-md-12">
				<label class="col-lg-2 control-label">Bekasi</label>
				<div class="col-lg-6">
					<input type="text" class="form-control input-xs" name="bekasi" value="">
				</div>
			</div>
			<label class="col-md-6">Petugas yang mengirim</label>Jam :
		</div>
		<label style="margin-top: 20%">Tanda tangan & Nama lengkap</label>
	</td>
</tr>