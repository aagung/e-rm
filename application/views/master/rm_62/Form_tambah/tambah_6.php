<tr>
	<td colspan="2">pasien/keluarga mengetahui dan menyetujui mengenai alasan pemindahan :
		<div class="radio no-padding">
			<label class="radio-inline">
				<input type="radio" name="pasien_keluarga_megetahui_pemindahan" class="styled">
				Ya
			</label>
		</div>
		<div class="radio no-padding">
			<label class="radio-inline">
				<input type="radio" name="pasien_keluarga_megetahui_pemindahan" class="styled">
				Tidak
			</label>
		</div>
		Bila pemberi persetujuan adalah keluarga pasien, lengkapi isian berikut 
		<div class="form-group">
			<label class="col-lg-2 control-label">Nama</label>
			<div class="col-lg-10">
				<input type="text" class="form-control input-xs" name="bila_pemberi_persetujuan_nama" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Hubungan</label>
			<div class="col-lg-10">
				<input type="text" class="form-control input-xs" name="bila_pemberi_persetujuan_hubungan" value="" >
			</div>
		</div>
	</td>
	<td colspan="3">
		Peralatan yang menyertai pasien saat pindah :
		<div class="panel-body">
			<div class="form-group col-md-5">
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_portable" class="styled">
						Portable O<sub>2</sub>
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_suction" class="styled">
						<i>Suction</i>
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_ventilator" class="styled">
						Ventilator
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_bagging" class="styled">
						<b><i>Bagging</i></b>
					</label>
				</div>
			</div>
			<div class="form-group col-md-8">
				<div class="form-group">
					<label class="col-lg-4 control-label">kebutuhan</label>
					<div class="col-md-8">
						<div class="input-group">
							<input type="text" class="form-control input-xs" name="peralatan_yang_menyertai_pasien_kebutuhan" value="" >
							<span class="input-group-addon">I/menit</span>
						</div>
					</div>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_ngt" class="styled">
						NGT
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_kateter_urin" class="styled">
						Kateter urin
					</label>
				</div>
				<div class="checkbox no-padding">
					<label class="checkbox-inline">
						<input type="checkbox" name="peralatan_yang_menyertai_pasien_infus_pump" class="styled">
						Infus pump / <i>Syringe pump</i>
					</label>
				</div>
			</div>
		</div>
	</td>
</tr>