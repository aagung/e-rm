<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th class="col-md-6" colspan="2"><center><h1>Laporan Operasi :</h1></center></th>
								<th class="col-md-6" colspan="2">
									<div class="form-group">
										<label class="control-label col-md-4">No. Med Rec :</label>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
										<div class="col-md-1">
											<input type="text" class="form-control" name="no_med_rec">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nama Pasien :</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="nama_pasien" >
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Umur / Jenis Kelamin :</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="umur_jenis_kelamin">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Ruangan / Kelas :</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="ruangan_kelas">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Dr. Yang Merawat :</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="dr_yang_merawat">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Nama Ahli Bedah :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="nama_ahli_bedah">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Asisten :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="asisten">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Nama Ahli Enestesi :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="nama_ahli_enestesi">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Nama Perawat :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="nama_perawat">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-12" colspan="4">
									<div class="form-group">
										<label class="control-label col-md-2">Diagnosa Pra Operatif :</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="diagnosa_pra_operatif">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-12" colspan="4">
									<div class="form-group">
										<label class="control-label col-md-2">Diagnosa Post Operatif :</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="diagnosa_post_operatif">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Tanggal Operasi :</label>
										<div class="col-md-12">
											<input type="date" class="form-control" name="taggal_operasi">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Jam Operasi Dimulai :</label>
										<div class="col-md-12">
											<input type="time" class="form-control" name="jam_operasi_dimulai">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Jam Operasi Selesai :</label>
										<div class="col-md-12">
											<input type="time" class="form-control" name="jam_operasi_selesai">
										</div>
									</div>
								</th>
								<th class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-12">Lama Operasi :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="lama_operasi">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-6" colspan="2" rowspan="2">
									<div class="form-group">
										<label class="control-label col-md-3">Jenis Operasi :</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="jenis_operasi">
										</div>
									</div>
								</th>
								<th class="col-md-6" colspan="2">
									<label class="control-label col-md-12">Klasifikasi :</label>
								</th>
							</tr>
							<tr>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="emergency" value="ya">Emergency
											</label>
	                               		</div>
									</div>
								</th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="mayor_besar" value="ya">Mayor / Besar
											</label>
	                               		</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-6" colspan="2">
									<div class="form-group">
										<label class="control-label col-md-3">Ditemukan Penyakit :</label>
										<div class="col-md-4">
		                            		<label class="radio-inline">
												<input type="radio" class="styled" name="ditemukan_penyakit" value="ya">Ya
											</label>
		                            	</div>
		                            	<div class="col-md-5">
		                            		<label class="radio-inline">
												<input type="radio" class="styled" name="ditemukan_penyakit" value="tidak">Tidak
											</label>
		                            	</div>
									</div>
								</th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="elektif" value="ya">Elektif
											</label>
	                               		</div>
									</div>
								</th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="medium_sedang" value="ya">Medium / Sedang
											</label>
	                               		</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-6" colspan="2" rowspan="2">
									<div class="form-group">
										<label class="control-label col-md-3">Uraian Penyakit :</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="uraian_penyakit">
										</div>
									</div>
								</th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="poliklinik" value="ya">Poliklinik
											</label>
	                               		</div>
									</div>
								</th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="minor_kecil" value="ya">Minor / Kecil
											</label>
	                               		</div>
									</div>
								</th>
							</tr>
							<tr>
								<th></th>
								<th>
									<div class="form-group">
										<div class="col-md-12">
	                                   		<label class="checkbox-inline">
												<input type="checkbox" class="styled" name="khusus" value="ya">Khusus
											</label>
	                               		</div>
									</div>
								</th>	
							</tr>
							<tr>
								<th class="col-md-4">
									<div class="form-group">
										<label class="control-label col-md-12">Jaringan Yang Dieksisi / Diinsisi :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="jaringan_yang_dieksisi_diinsisi">
										</div>
									</div>
								</th>
								<th class="col-md-4">
									<div class="form-group">
										<label class="control-label col-md-12">Jaringan Yang Dikirim Untuk Pemeriksaan PA :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="jaringan_yang_dikirim_untuk_pemeriksaan_pa">
										</div>
										<div class="col-md-6">
		                            		<label class="radio-inline">
												<input type="radio" class="styled" name="jaringan_yang_dikirim_untuk_pemeriksaan_pa" value="ya">Ya
											</label>
		                            	</div>
		                            	<div class="col-md-6">
		                            		<label class="radio-inline">
												<input type="radio" class="styled" name="jaringan_yang_dikirim_untuk_pemeriksaan_pa" value="tidak">Tidak
											</label>
		                            	</div>
									</div>
								</th>
								<th class="col-md-4" colspan="2">
									<div class="form-group">
										<label class="control-label col-md-12">Tanggal Pengiriman PA :</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="tanggal_pengiriman_pa">
										</div>
									</div>
								</th>
							</tr>
							<tr>
								<th class="col-md-12" colspan="4">
									<label class="control-label col-md-12"><center><b>URAIAN OPERASI (Jika Perlu Dilanjutkan Dihalaman Sebaliknya)</b></center></label>
								</th>
							</tr>
							<tr>
								<th class="col-md-12" colspan="4">
									<div class="form-group">
										<div class="col-md-12">
		                                	<textarea class="form-control" name="uraian_operasi" rows="50" cols="4"></textarea>
		                                </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Tanda Tangan Ahli Bedah :</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="tanda_tangan_ahli_bedah">
										</div>
									</div>
								</th>
							</tr>					
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="" />
			<input type="hidden" name="option_name" value="" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>