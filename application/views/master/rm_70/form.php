<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="row">
					<div class="col-md-6">
						<center>
							<img src="<?php echo base_url('assets/img/rsananda.jpg')?>" width="290px" height="70px">
						</center>
					</div>
					<div class="col-md-6">
						<center>
                            <table border="1">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                        	<center>
                                        		<b>FORMULIR PERSETUJUAN TINDAKAN KEDOKTERAN</b>
                                        	</center>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        	<center>
                                        		<b>RM-70 / Rev.1 / VI / 16</b>
                                        	</center>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </center>
					</div>
					<div class="col-md-12">
						<center>
							<b>FORMULIR PERSETUJUAN / PENOLAKAN TINDAKAN KEDOKTERAN</b>
						</center>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class="col-md-12" colspan="4"><center><b>PEMBERIAN INFORMASI</b></center></th>
									</tr>
									<tr>
										<th class="col-md-6" colspan="2"><b>Dokter Pelaksana Tindakan</b></th>
										<th class="col-md-6" colspan="2">
											<div class="col-md-12">
												<input type="text" class="form-control" name="dokter_pelaksana_tindakan" >
											</div>
										</th>
									</tr>
									<tr>
										<th class="col-md-6" colspan="2"><b>Pemberi Informasi</b></th>
										<th class="col-md-6" colspan="2">
											<div class="col-md-12">
												<input type="text" class="form-control" name="pemberi_informasi" >
											</div>
										</th>
									</tr>
									<tr>
										<th class="col-md-6" colspan="2"><b>Penerima Informasi / Pemberi Persetujuan / Penolakan *</b></th>
										<th class="col-md-6" colspan="2">
											<div class="col-md-12">
												<input type="text" class="form-control" name="penerima_informasi_pemberi_persetujuan_penolakan" >
											</div>
										</th>
									</tr>
									<tr>
										<th class="col-md-3"></th>
										<th class="col-md-3"><center><b>JENIS INFORMASI</b></center></th>
										<th class="col-md-3"><center><b>ISI INFORMASI</b></center></th>
										<th class="col-md-3"><center><b>TANDA (&#10003;)</b></center></th>
									</tr>					
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Diagnosis Dan Diagnosa Banding</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="diagnosis_diagnosa_banding" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="diagnosis_diagnosa_banding" value="ya" checked>
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Dasar Diagnosis</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="dasar_diagnosis" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="dasar_diagnosis" value="ya" checked>
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Tindakan Kedokteran</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="tindakan_kedokteran" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="tindakan_kedokteran" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Indikasi Tindakan</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="indikasi_tindakan" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="indikasi_tindakan" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>5</td>
										<td>Tata Cara</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="tata_cara" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="tata_cara" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>6</td>
										<td>Tujuan</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="tujuan" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="tujuan" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>7</td>
										<td>Risiko</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="risiko" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="risiko" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>8</td>
										<td>Komplikasi</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="komplikasi" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="komplikasi" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>9</td>
										<td>Prognosis</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="prognosis" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="prognosis" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>10</td>
										<td>Alternatif & Risiko</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="alternatif_risiko" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="alternatif_risiko" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>11</td>
										<td>Hal Ini Yang Akan Dilakukan Untuk Menyelamatkan Pasien Seperti : Transfusi Dan Perluasan Tindakan</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="transfusi_perluasan" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="transfusi_perluasan" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td>12</td>
										<td>Lain-lain</td>
										<td>
											<div class="form-group">
												<div class="col-md-12">
				                                	<textarea class="form-control" name="lain_lain" rows="4" cols="4"></textarea>
				                                </div>
											</div>
										</td>
										<td>
											<div class="col-md-12">
												<center>
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="lain_lain" value="ya" checked="">
													</label>
												</center>
		                               		</div>
										</td>
									</tr>
									<tr>
										<td colspan="2">Dengan Ini Menyatakan Bahwa Saya Telah Menerangkan Hal-hal Diatas Secara Benar Dan Jelas Dan Memberikan Kesempatan Untuk Bertanya Dan / Atau Berdiskusi</td>
										<td>
											<div class="form-group">
												<label class="control-label col-md-12">Tanda Tangan Dokter</label>
												<div class="col-md-12">
				                                	<textarea class="form-control" name="pernyataan1" rows="4" cols="4"></textarea>
				                                </div>											
				                            </div>
										</td>
										<td>
											<div class="form-group">
												<label class="control-label col-md-12">Tanggal :</label>
												<div class="col-md-12">
													<input type="date" class="form-control" name="pernyataan1">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-12">Pukul :</label>
												<div class="col-md-12">
													<input type="time" class="form-control" name="pernyataan1">
												</div>
											</div>

										</td>
									</tr>
									<tr>
										<td colspan="2">Dengan Ini Menyatakan Bahwa Saya Telah Menerima Informasi Dari Dokter Sebagaimana Diatas Kemudian Saya Beri Tanda / Paraf Dikolom Kanannya, Serta Telah Diberi Kesempatan Untuk Bertanya / Berdiskusi Dan Telah Memahaminya</td>
										<td>
											<div class="form-group">
												<label class="control-label col-md-12">Tanda Tangan Pasien / Keluarga</label>
												<div class="col-md-12">
				                                	<textarea class="form-control" name="pernyataan2" rows="4" cols="4"></textarea>
				                                </div>											
				                            </div>
										</td>
										<td>
											<div class="form-group">
												<label class="control-label col-md-12">Tanggal :</label>
												<div class="col-md-12">
													<input type="date" class="form-control" name="pernyataan2">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-12">Pukul :</label>
												<div class="col-md-12">
													<input type="time" class="form-control" name="pernyataan2">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="col-md-12" colspan="4">
											<label class="control-label col-md-12">* Bila Pasien Tidak Kompeten Atau Tidak Mau Menerima Informasi, Maka Penerima Informasi Adalah Wali Atau Keluarga Terdekat</label>
											<label class="control-label col-md-12">* * Coret Pernyataan Yang Tidak Sesuai</label>
										</td>
									</tr>
								</tbody>
								<thead>
									<tr>
										<th class="col-md-12" colspan="4"><center><b>PERSETUJUAN / PENOLAKAN TINDAKAN KEDOKTERAN</b></center></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="col-md-12" colspan="4">
											<div class="form-group">
												<label class="control-label col-md-4">Yang Bertanda Tangan Dibawah Ini, Saya, Nama</label>
												<div class="col-md-2">
													<input type="date" class="form-control" name="nama">
												</div>
												<label class="control-label col-md-1">Umur</label>
												<div class="col-md-2">
													<input type="text" class="form-control" name="umur">
												</div>
												<label class="control-label col-md-3">Laki - Laki / Perempuan * * ,</label>
											</div>
											<div class="form-group">
												<label class="control-label col-md-1">Alamat</label>
												<div class="col-md-9">
				                                	<textarea class="form-control" name="alamat"></textarea>
				                                </div>
				                                <label class="control-label col-md-2"><b>Menyatakan :</b></label>	
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-12">1. Saya memahami perlunya dan manfaat prosedur tindakan tersebut sebagaimana telah dijelaskan seperti diatas kepada saya, termasuk resiko dan komplikasi yang mungkin terjadi. Saya juga menyadari oleh karena itu kedokteran bukanlah ilmu pasti, maka keberhasilan tindakan kedokteran bukanlah keniscayaan, melainkan sangat bergantung kepada izin Tuhan Yang Maha</label>	
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-4">2. <b>SETUJU * *</b> dilakukannya prosedur tindakan berupa</label>
												<div class="col-md-8">
				                                	<textarea class="form-control" name="2_tindakan"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
				                            	<label class="control-label col-md-5">terhadap diri saya / pihak yang saya wakili yang bernama</label>
												<div class="col-md-7">
				                                	<textarea class="form-control" name="2_nama"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
				                            	<label class="control-label col-md-2">Tanggal Lahir</label>
												<div class="col-md-4">
				                                	<input type="date" name="2_tanggal_lahir" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">No.RM</label>
												<div class="col-md-5">
				                                	<input type="text" name="2_no_rm" class="form-control">
				                                </div>
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-1">Alamat</label>
												<div class="col-md-5">
				                                	<textarea class="form-control" name="2_alamat"></textarea>
				                                </div>
				                                <label class="control-label col-md-1">Dirawat di</label>
				                                <div class="col-md-5">
				                                	<textarea class="form-control" name="2_dirawat"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-4">3. <b>MENOLAK * *</b> dilakukannya prosedur tindakan berupa</label>
												<div class="col-md-8">
				                                	<textarea class="form-control" name="3_tindakan"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
				                            	<label class="control-label col-md-5">terhadap diri saya / pihak yang saya wakili yang bernama</label>
												<div class="col-md-7">
				                                	<textarea class="form-control" name="3_nama"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
				                            	<label class="control-label col-md-2">Tanggal Lahir</label>
												<div class="col-md-4">
				                                	<input type="date" name="3_tanggal_lahir" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">No.RM</label>
												<div class="col-md-5">
				                                	<input type="text" name="3_no_rm" class="form-control">
				                                </div>
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-1">Alamat</label>
												<div class="col-md-5">
				                                	<textarea class="form-control" name="3_alamat"></textarea>
				                                </div>
				                                <label class="control-label col-md-1">Dirawat di</label>
				                                <div class="col-md-5">
				                                	<textarea class="form-control" name="3_dirawat"></textarea>
				                                </div>
				                            </div>
				                            <div class="form-group">
												<label class="control-label col-md-5">Penolakan tersebut saya ambil berdasarkan pertimbangan</label>
												<div class="col-md-7">
				                                	<textarea class="form-control" name="3_alamat" rows="4"></textarea>
				                                </div>
				                            </div>	
				                            <div class="form-group">
												<div class="col-md-2">
				                                	<input type="text" name="tempat" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">, Tanggal</label>
												<div class="col-md-2">
				                                	<input type="date" name="tanggal" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">Pukul</label>
												<div class="col-md-2">
				                                	<input type="time" name="pukul" class="form-control">
				                                </div>
				                            </div>
				                            <div class="form-group">
				                                <label class="control-label col-md-5 col-md-offset-7">Saksi</label>
				                                <label class="control-label col-md-4">Yang menyatakan * Pasien / Penanggung Jawab</label>
				                                <label class="control-label col-md-1 col-md-offset-1 text-center">Keluarga</label>
				                                <label class="control-label col-md-3 col-md-offset-3">Pihak Rumah Sakit</label>
				                            </div><br><br><br>
				                            <div class="form-group">
				                            	<label class="control-label col-md-1 text-right">(</label>
												<div class="col-md-2">
				                                	<input type="text" name="pasien_penanggung_jawab" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">)</label>
				                                <label class="control-label col-md-1 text-right">(</label>
												<div class="col-md-2">
				                                	<input type="text" name="keluarga" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">)</label>
				                                <label class="control-label col-md-1 text-right">(</label>
												<div class="col-md-2">
				                                	<input type="text" name="pihak_rumah_sakit" class="form-control">
				                                </div>
				                                <label class="control-label col-md-1">)</label>
				                            </div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="" />
			<input type="hidden" name="option_name" value="" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>