<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
				<a href="" class="btn btn-default cancel-button">Kembali</a>
			</div>
			<hr>
			<div class="table-responsive">
				<table class="table table-hover">
					<tr>
						<th class="text-center" colspan="12">SKORE ALDRETE GENERAL ANASTEIS (DEWASA)</th>
						<th class="text-center" colspan="10">STEWARD SCORE PASCAANASTESI (ANAK-ANAK)</th>
					</tr>
					<tr>
						<th rowspan="2" colspan="2">ID pra anastesi</th>
						<th rowspan="2">skor</th>
						<th class="text-center"colspan="8">waktu</th>
						<th class="" rowspan="6">Pergerakan</th>
						<th rowspan="2">TINDAKAN</th>
						<th rowspan="2">SKOR</th>
						<th class="text-center" colspan="8">WAKTU</th>
					</tr>
					<tr>
						<th>5</th>
						<th>15</th>
						<th>30</th>
						<th>45</th>
						<th>60</th>
						<th>90</th>
						<th>120</th>
						<th>keluar</th>
						<th>5</th>
						<th>15</th>
						<th>30</th>
						<th>45</th>
						<th>60</th>
						<th>90</th>
						<th>120</th>
						<th>keluar</th>

					</tr>
					<tr>
						<th rowspan="3">sirkulasi</th>
						<th>TD+/-20</th>
						<th>2</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete1" name="awaktu5" value="2">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete2" name="awaktu15" value="2">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete3" name="awaktu30" value="2">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete4" name="awaktu45" value="2">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete5" name="awaktu60" value="2">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete6" name="awaktu90" value="2">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete7" name="awaktu120" value="2">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete8" name="awaktukeluar" value="2">
						</th>
						<th>Gerak bertujuan</th>
						<th>2</th>
						<th><input class="styled stewardsatu" type="radio" id="steward1" name="spwaktu5" value="2">
						</th>
						<th><input class="styled stewarddua" type="radio" id="steward2" name="spwaktu15" value="2">
						</th>
						<th><input class="styled stewardtiga" type="radio" id="steward3" name="spwaktu30" value="2">
						</th>
						<th><input class="styled stewardempat" type="radio" id="steward4" name="spwaktu45" value="2">
						</th>
						<th><input class="styled stewardlima" type="radio" id="steward5" name="spwaktu60" value="2">
						</th>
						<th><input class="styled stewardenam" type="radio" id="steward6" name="spwaktu90" value="2">
						</th>
						<th><input class="styled stewardtujuh" type="radio" id="steward7" name="spwaktu120" value="2">
						</th>
						<th><input class="styled stewardkeluar" type="radio" id="steward8" name="spwaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>TD+/-20-50</th>
						<th>1</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete9" name="awaktu5" value="1">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete10" name="awaktu15" value="1">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete11" name="awaktu30" value="1">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete12" name="awaktu45" value="1">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete13" name="awaktu60" value="1">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete14" name="awaktu90" value="1">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete15" name="awaktu120" value="1">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete16" name="awaktukeluar" value="1">
						</th>
						<th>Gerak tak bertujuan</th>
						<th>1</th>
						<th><input class="styled stewardsatu" type="radio" id="steward9" name="spwaktu5" value="1">
						</th>
						<th><input class="styled stewarddua" type="radio" id="steward10" name="spwaktu15" value="1">
						</th>
						<th><input class="styled stewardtiga" type="radio" id="steward11" name="spwaktu30" value="1">
						</th>
						<th><input class="styled stewardempat" type="radio" id="steward12" name="spwaktu45" value="1">
						</th>
						<th><input class="styled stewardlima" type="radio" id="steward13" name="spwaktu60" value="1">
						</th>
						<th><input class="styled stewardenam" type="radio" id="steward14" name="spwaktu90" value="1">
						</th>
						<th><input class="styled stewardtujuh" type="radio" id="steward15" name="spwaktu120" value="1">
						</th>
						<th><input class="styled stewardkeluar" type="radio" id="steward16" name="spwaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>TD>50</th>
						<th>0</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete17" name="awaktu5" value="0">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete18" name="awaktu15" value="0">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete19" name="awaktu30" value="0">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete20" name="awaktu45" value="0">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete21" name="awaktu60" value="0">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete22" name="awaktu90" value="0">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete23" name="awaktu120" value="0">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete24" name="awaktukeluar" value="0">
						</th>
						<th rowspan="2">Tidak bergerak</th>
						<th rowspan="2">0</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward17" name="spwaktu5" value="0">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward18" name="spwaktu15" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward19" name="spwaktu30" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward20" name="spwaktu45" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward21" name="spwaktu60" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward22" name="spwaktu90" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward23" name="spwaktu120" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward24" name="spwaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th rowspan="3">kesadaran</th>
						<th>Sadar penuh</th>
						<th>2</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete25" name="akwaktu5" value="2">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete26" name="akwaktu15" value="2">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete27" name="akwaktu30" value="2">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete28" name="akwaktu45" value="2">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete29" name="akwaktu60" value="2">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete30" name="akwaktu90" value="2">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete31" name="akwaktu120" value="2">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete32" name="akwaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>Respon panggilan</th>
						<th>1</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete33" name="akwaktu5" value="1">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete34" name="akwaktu15" value="1">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete35" name="akwaktu30" value="1">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete36" name="akwaktu45" value="1">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete37" name="akwaktu60" value="1">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete38" name="akwaktu90" value="1">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete39" name="akwaktu120" value="1">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete40" name="akwaktukeluar" value="1">
						</th>
						<th rowspan="6">Pernafasan</th>
						<th rowspan="2">Batuk Menangis</th>
						<th rowspan="2">2</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward25" name="spewaktu5" value="2">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward26" name="spewaktu15" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward27" name="spewaktu30" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward28" name="spewaktu45" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward29" name="spewaktu60" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward30" name="spewaktu90" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward31" name="spewaktu120" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward32" name="spewaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>Tidak berespon</th>
						<th>0</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete41" name="akwaktu5" value="0">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete42" name="akwaktu15" value="0">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete43" name="akwaktu30" value="0">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete44" name="akwaktu45" value="0">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete45" name="akwaktu60" value="0">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete46" name="akwaktu90" value="0">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete47" name="akwaktu120" value="0">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete48" name="akwaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th rowspan="3">oksigenasi</th>
						<th>SPO2 > 92%</th>
						<th>2</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete49" name="aowaktu5" value="2">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete50" name="aowaktu15" value="2">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete51" name="aowaktu30" value="2">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete52" name="aowaktu45" value="2">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete53" name="aowaktu60" value="2">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete54" name="aowaktu90" value="2">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete55" name="aowaktu120" value="2">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete56" name="aowaktukeluar" value="2">
						</th>
						<th rowspan="2">Pertahankan jalan nafas</th>
						<th rowspan="2">1</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward33" name="spewaktu5" value="1">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward34" name="spewaktu15" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward35" name="spewaktu30" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward36" name="spewaktu45" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward37" name="spewaktu60" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward38" name="spewaktu90" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward39" name="spewaktu120" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward40" name="spewaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>SPO2 > 90%</th>
						<th>1</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete57" name="aowaktu5" value="1">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete58" name="aowaktu15" value="1">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete59" name="aowaktu30" value="1">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete60" name="aowaktu45" value="1">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete61" name="aowaktu60" value="1">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete62" name="aowaktu90" value="1">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete63" name="aowaktu120" value="1">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete64" name="aowaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>SPO2 > 90%</th>
						<th>0</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete65" name="aowaktu5" value="0">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete66" name="aowaktu15" value="0">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete67" name="aowaktu30" value="0">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete68" name="aowaktu45" value="0">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete69" name="aowaktu60" value="0">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete70" name="aowaktu90" value="0">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete71" name="aowaktu120" value="0">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete72" name="aowaktukeluar" value="0">
						</th>
						<th rowspan="2">Perlu bantuan</th>
						<th rowspan="2">0</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward41" name="spewaktu5" value="0">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward42" name="spewaktu15" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward43" name="spewaktu30" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward44" name="spewaktu45" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward45" name="spewaktu60" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward46" name="spewaktu90" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward47" name="spewaktu120" value="0">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward48" name="spewaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th rowspan="3">Pernafasan</th>
						<th>Sadar bebas</th>
						<th>2</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete73" name="apwaktu5" value="2">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete74" name="apwaktu15" value="2">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete75" name="apwaktu30" value="2">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete76" name="apwaktu45" value="2">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete77" name="apwaktu60" value="2">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete78" name="apwaktu90" value="2">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete79" name="apwaktu120" value="2">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete80" name="apwaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>Dispnew</th>
						<th>1</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete81" name="apwaktu5" value="1">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete82" name="apwaktu15" value="1">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete83" name="apwaktu30" value="1">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete84" name="apwaktu45" value="1">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete85" name="apwaktu60" value="1">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete86" name="apwaktu90" value="1">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete87" name="apwaktu120" value="1">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete88" name="apwaktukeluar" value="1">
						</th>
						<th rowspan="5">Kesadaran</th>
						<th rowspan="2">menangis</th>
						<th rowspan="2">2</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward49" name="skwaktu5" value="2">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward50" name="skwaktu15" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward51" name="skwaktu30" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward52" name="skwaktu45" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward53" name="skwaktu60" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward54" name="skwaktu90" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward55" name="skwaktu120" value="2">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward56" name="skwaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>apneu</th>
						<th>0</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete89" name="apwaktu5" value="0">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete90" name="apwaktu15" value="0">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete91" name="apwaktu30" value="0">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete92" name="apwaktu45" value="0">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete93" name="apwaktu60" value="0">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete94" name="apwaktu90" value="0">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete95" name="apwaktu120" value="0">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete96" name="apwaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th rowspan="3">Aktifitas</th>
						<th>Menggerakan 4 extermitas</th>
						<th>2</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete97" name="aawaktu5" value="2">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete98" name="aawaktu15" value="2">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete99" name="aawaktu30" value="2">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete100" name="aawaktu45" value="2">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete101" name="aawaktu60" value="2">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete102" name="aawaktu90" value="2">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete103" name="aawaktu120" value="2">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete104" name="aawaktukeluar" value="2">
						</th>
						<th rowspan="2">Bereaksi rangsangan</th>
						<th rowspan="2">1</th>
						<th rowspan="2"><input class="styled stewardsatu" type="radio" id="steward57" name="skwaktu5" value="1">
						</th>
						<th rowspan="2"><input class="styled stewarddua" type="radio" id="steward58" name="skwaktu15" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardtiga" type="radio" id="steward59" name="skwaktu30" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardempat" type="radio" id="steward60" name="skwaktu45" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardlima" type="radio" id="steward61" name="skwaktu60" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardenam" type="radio" id="steward62" name="skwaktu90" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardtujuh" type="radio" id="steward63" name="skwaktu120" value="1">
						</th>
						<th rowspan="2"><input class="styled stewardkeluar" type="radio" id="steward64" name="skwaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>Menggerakan 2 extermitas</th>
						<th>1</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete105" name="aawaktu5" value="1">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete106" name="aawaktu15" value="1">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete107" name="aawaktu30" value="1">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete108" name="aawaktu45" value="1">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete109" name="aawaktu60" value="1">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete110" name="aawaktu90" value="1">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete111" name="aawaktu120" value="1">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete112" name="aawaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>Tidak menggerakan ext</th>
						<th>0</th>
						<th><input class="styled aldretesatu" type="radio" id="aldrete113" name="aawaktu5" value="0">
						</th>
						<th><input class="styled aldretedua" type="radio" id="aldrete114" name="aawaktu15" value="0">
						</th>
						<th><input class="styled aldretetiga" type="radio" id="aldrete115" name="aawaktu30" value="0">
						</th>
						<th><input class="styled aldreteempat" type="radio" id="aldrete116" name="aawaktu45" value="0">
						</th>
						<th><input class="styled aldretelima" type="radio" id="aldrete117" name="aawaktu60" value="0">
						</th>
						<th><input class="styled aldreteenam" type="radio" id="aldrete118" name="aawaktu90" value="0">
						</th>
						<th><input class="styled aldretetujuh" type="radio" id="aldrete119" name="aawaktu120" value="0">
						</th>
						<th><input class="styled aldretekeluar" type="radio" id="aldrete120" name="aawaktukeluar" value="0">
						</th>
						<th>Tidak bereaksi</th>
						<th>0</th>
						<th><input class="styled stewardsatu" type="radio" id="steward65" name="skwaktu5" value="0">
						</th>
						<th><input class="styled stewarddua" type="radio" id="steward66" name="skwaktu15" value="0">
						</th>
						<th><input class="styled stewardtiga" type="radio" id="steward67" name="skwaktu30" value="0">
						</th>
						<th><input class="styled stewardempat" type="radio" id="steward68" name="skwaktu45" value="0">
						</th>
						<th><input class="styled stewardlima" type="radio" id="steward69" name="skwaktu60" value="0">
						</th>
						<th><input class="styled stewardenam" type="radio" id="steward70" name="skwaktu90" value="0">
						</th>
						<th><input class="styled stewardtujuh" type="radio" id="steward71" name="skwaktu120" value="0">
						</th>
						<th><input class="styled stewardkeluar" type="radio" id="steward72" name="skwaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th colspan="3"><b><i>TOTAL</i></b></th>
						<th><span class="skoraldretesatu"></span></th>
						<th><span class="skoraldretedua"></span></th>
						<th><span class="skoraldretetiga"></span></th>
						<th><span class="skoraldreteempat"></span></th>
						<th><span class="skoraldretelima"></span></th>
						<th><span class="skoraldreteenam"></span></th>
						<th><span class="skoraldretetujuh"></span></th>
						<th><span class="skoraldretekeluar"></span></th>
						<th></th>
						<th colspan="2"><b><i>TOTAL</i></b></th>
						<th><span class="skorstewardsatu"></span></th>
						<th><span class="skorstewarddua"></span></th>
						<th><span class="skorstewardtiga"></span></th>
						<th><span class="skorstewardempat"></span></th>
						<th><span class="skorstewardlima"></span></th>
						<th><span class="skorstewardenam"></span></th>
						<th><span class="skorstewardtujuh"></span></th>
						<th><span class="skorstewardkeluar"></span></th>
					</tr>
					<tr>
						<th colspan="12"><b><i>Pasien bisa pindah ke ruangan jika skor > 8</i></b></th>
						<th colspan="10"><b><i>Pasien bisa pindah ke ruangan jika skor > 5</i></b></th>
					</tr>
				</table>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-hover">
					<tr>
						<th class="text-center" colspan="10"><b>SKORE BROMAGE PASCA SPINAL ANASTESI</b></th>
					</tr>
					<tr>
						<th class="text-center" rowspan="2" >KRITERIA</th>
						<th rowspan="2" >SKOR</th>
						<th class="text-center" colspan="8">WAKTU</th>
					</tr>
					<tr>
						<th>5</th>
						<th>15</th>
						<th>30</th>
						<th>45</th>
						<th>60</th>
						<th>90</th>
						<th>120</th>
						<th>KELUAR</th>
					</tr>
					<tr>
						<th>GERAKAN PENUH DARI TUNGKAI</th>
						<th>0</th>
						<th><input class="styled bromage bsatu" type="radio" id="bromage1" name="bwaktu5" value="0">
						</th>
						<th><input class="styled bromage bdua" type="radio" id="bromage2" name="bwaktu15" value="0">
						</th>
						<th><input class="styled bromage btiga" type="radio" id="bromage3" name="bwaktu30" value="0">
						</th>
						<th><input class="styled bromage bempat" type="radio" id="bromage4" name="bwaktu45" value="0">
						</th>
						<th><input class="styled bromage blima" type="radio" id="bromage5" name="bwaktu60" value="0">
						</th>
						<th><input class="styled bromage benam" type="radio" id="bromage6" name="bwaktu90" value="0">
						</th>
						<th><input class="styled bromage btujuh" type="radio" id="bromage7" name="bwaktu120" value="0">
						</th>
						<th><input class="styled bromage bkeluar" type="radio" id="bromage8" name="bwaktukeluar" value="0">
						</th>
					</tr>
					<tr>
						<th>TIDAK MAMPU MENGEKSTENSI</th>
						<th>1</th>
						<th><input class="styled bromage bsatu" type="radio" id="bromage9" name="bwaktu5" value="1">
						</th>
						<th><input class="styled bromage bdua" type="radio" id="bromage10" name="bwaktu15" value="1">
						</th>
						<th><input class="styled bromage btiga" type="radio" id="bromage11" name="bwaktu30" value="1">
						</th>
						<th><input class="styled bromage bempat" type="radio" id="bromage12" name="bwaktu45" value="1">
						</th>
						<th><input class="styled bromage blima" type="radio" id="bromage13" name="bwaktu60" value="1">
						</th>
						<th><input class="styled bromage benam" type="radio" id="bromage14" name="bwaktu90" value="1">
						</th>
						<th><input class="styled bromage btujuh" type="radio" id="bromage15" name="bwaktu120" value="1">
						</th>
						<th><input class="styled bromage bkeluar" type="radio" id="bromage16" name="bwaktukeluar" value="1">
						</th>
					</tr>
					<tr>
						<th>TIDAK MAMPU MEMFLEKSI TUNGKAI</th>
						<th>2</th>
						<th><input class="styled bromage bsatu" type="radio" id="bromage17" name="bwaktu5" value="2">
						</th>
						<th><input class="styled bromage bdua" type="radio" id="bromage18" name="bwaktu15" value="2">
						</th>
						<th><input class="styled bromage btiga" type="radio" id="bromage19" name="bwaktu30" value="2">
						</th>
						<th><input class="styled bromage bempat" type="radio" id="bromage20" name="bwaktu45" value="2">
						</th>
						<th><input class="styled bromage blima" type="radio" id="bromage21" name="bwaktu60" value="2">
						</th>
						<th><input class="styled bromage benam" type="radio" id="bromage22" name="bwaktu90" value="2">
						</th>
						<th><input class="styled bromage btujuh" type="radio" id="bromage23" name="bwaktu120" value="2">
						</th>
						<th><input class="styled bromage bkeluar" type="radio" id="bromage24" name="bwaktukeluar" value="2">
						</th>
					</tr>
					<tr>
						<th>TIDAK MAMPU MEMFLEKSI PERGELANGAN KAKI</th>
						<th>3</th>
						<th><input class="styled bromage bsatu" type="radio" id="bromage25" name="bwaktu5" value="3">
						</th>
						<th><input class="styled bromage bdua" type="radio" id="bromage26" name="bwaktu15" value="3">
						</th>
						<th><input class="styled bromage btiga" type="radio" id="bromage27" name="bwaktu30" value="3">
						</th>
						<th><input class="styled bromage bempat" type="radio" id="bromage28" name="bwaktu45" value="3">
						</th>
						<th><input class="styled bromage blima" type="radio" id="bromage29" name="bwaktu60" value="3">
						</th>
						<th><input class="styled bromage benam" type="radio" id="bromage30" name="bwaktu90" value="3">
						</th>
						<th><input class="styled bromage btujuh" type="radio" id="bromage31" name="bwaktu120" value="3">
						</th>
						<th><input class="styled bromage bkeluar" type="radio" id="bromage32" name="bwaktukeluar" value="3">
						</th>
					</tr>
					<tr>
						<th colspan="2">SKOR SAAT DIPINDAHKAN</th>
						<th><span class="skorbromage1"></span></th>
						<th><span class="skorbromage2"></span></th>
						<th><span class="skorbromage3"></span></th>
						<th><span class="skorbromage4"></span></th>
						<th><span class="skorbromage5"></span></th>
						<th><span class="skorbromage6"></span></th>
						<th><span class="skorbromage7"></span></th>
						<th><span class="skorkeluar"></span></th>
					</tr>
				</table>
			</div>
			<div class="col-md-12">
				<b><i>PASIEN BISA DIPINDAHKAN KE RUANGAN JIKA SKOR < 3</i></b>
			</div>
			<center>
				<div class="col-md-12">
					BARANG DAN MATERIAL YANG DISERAHKAN
				</div>
			</center>
			<div class="col-md-4" style="margin-left: 35%">
				<div class="form-group">
					<label class="col-md-6 control-label ">a. STATUS/RM PASIEN</label>
					<label class="radio-inline">
						<input type="radio" name="status" class="styled">
						YA
					</label>
					<label class="radio-inline">
						<input type="radio" name="status" class="styled">
						TIDAK
					</label>
				</div> 
				<div class="form-group">
					<label class="col-md-6 control-label ">b. FOTO RONTGEN</label>
					<label class="radio-inline">
						<input type="radio" name="foto_rontgen" class="styled">
						YA
					</label>
					<label class="radio-inline">
						<input type="radio" name="foto_rontgen" class="styled">
						TIDAK
					</label>
				</div> 
				<div class="form-group">
					<label class="col-md-6 control-label ">c. USG</label>
					<label class="radio-inline">
						<input type="radio" name="usg" class="styled">
						YA
					</label>
					<label class="radio-inline">
						<input type="radio" name="usg" class="styled">
						TIDAK
					</label>
				</div> 
				<div class="form-group">
					<label class="col-md-5 control-label">d. LAIN LAIN</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="" value="" >
					</div>
				</div>
			</div>
			<div class="panel-heading">
				<div class="panel-body">
					<table class="table table-hover">
						<th>
							<div class="col-md-8" style="margin-left: 20%">
								<div class="form-group">
									<label class="col-md-3 control-label ">DIPINDAHKAN KE :</label>
									<label class="radio-inline col-md-3">
										<input type="radio" name="dipindahkan_ke" class="styled">
										Ruang Rawat
									</label>
									<label class="radio-inline col-md-2">
										<input type="radio" name="dipindahkan_ke" class="styled">
										ICU
									</label>
									<label class="radio-inline col-md-2">
										<input type="radio" name="dipindahkan_ke" class="styled">
										Pulang
									</label>
								</div> 
							</div>
						</th>
					</table>
				</div>
			</div>


		</div>
		<div class="panel-footer">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
		</div>
	</div>
</form>