	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>

		        <legend class="text-bold" style="margin-top:0px"></legend>
				    
				<table class="table table-bordered">
				    <thead>
			    	</thead>
			    	<tbody>
			    		<?php include 'form/form1.php' ?>
				    	<?php include 'form/form2.php' ?>
				    	<?php include 'form/form3.php' ?>
				    	<?php include 'form/form4.php' ?>
				    	<?php include 'form/form5.php' ?>
				    	<?php include 'form/form6.php' ?>
				    	<?php include 'form/form7.php' ?>
				    	<?php include 'form/form8.php' ?>					    		
				    </tbody>
				</table>		
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	