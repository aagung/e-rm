						<tr>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Apakah mesin anastesi dan cek medikasi sudah lengkap? ?
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="mesin_anestesi_cek_medikasi_lengkap">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="mesin_anestesi_cek_medikasi_lengkap">
			    									<label>Tidak </label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Apakah antibiotik pencegahan telah diberikan dalam rentang waktu 60 menit sebelum ini?
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="antibiotik_sudah_diberikan_60_menit_sebelum_ini">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="antibiotik_sudah_diberikan_60_menit_sebelum_ini">
			    									<label>Tidak dapat diberlakukan</label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    		<td style="width:33%" class="">
				    			<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<p><!-- Tanda tangan --> perawat  </p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="row">
				    							<div class="form-group">
				    								<div class="row">
					    								<label class="col-md-4">Nama</label>
					    								<div class="col-md-8">
					    									<input type="text" class="form-control input-xs" name="nama_perawat">
					    								</div>
				    								</div>
				    							</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
				    		</td>
				    	</tr>
				    	