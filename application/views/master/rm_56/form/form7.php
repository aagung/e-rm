						<tr>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Apakah Pulse Oksimeter sudah terpasang pada pasien dan berfungsi ?
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="pulse_oksimeter_terpasang_pada_pasien">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="pulse_oksimeter_terpasang_pada_pasien">
			    									<label>Tidak </label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="" rowspan="2">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Kemungkinan Kejadian Tidak Diharapkan 
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<label class="row">Terhadap Ahli Bedah	</label> 
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_ahli_bedah_langkah_kritis_non_rutin">
			    									<label>Apakah langkah-langkah kritis atau non-rutin</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_ahli_bedah_lama_kasus_mengambil_waktu">
			    									<label>Berapa lama kasus ini mengambil waktu</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_ahli_bedah_antisipasi_perdarahan">
			    									<label>Apa antisipasi bila terjadi perdarahan </label>
			    								</div>
			    							</div>
			    							<div class="form-group">	
			    								<label class="row">Terhadap Ahli Anestesi</label>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_ahli_anestesi_ada_perhatian_khusus">
			    									<label>Apakah ada perhatian khusus </label>
			    								</div>
			    							</div> 
			    							<div class="form-group"> 
			    								<label class="row">Terhadap tim perawat</label>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_tim_perawat_konfirmasi_sterilitas">
			    									<label>Apakah sterilitas (termasuk indikator) sudah dikonfirmasi ?</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="kemungkinan_kejadian_terhadap_tim_perawat_ada_masalah_kepedulian">
			    									<label>Apakah ada masalah atau kepedulian lain?</label>
			    								</div>
			    							</div>
			    							<div class="form-group">
			    								<label class="row">Apakah hasil MRI, CT scan, Rontgen diperlihatkan?</label>
			    								<div class="col-md-12">
				    								<div class="radio">
				    									<input type="radio" class="styled" name="hasil_mri_ctscan_rontgen_diperlihatkan">
				    									<label>Ya</label>
				    								</div>
				    								<div class="radio">
				    									<input type="radio" class="styled" name="hasil_mri_ctscan_rontgen_diperlihatkan">
				    									<label>Tidak dapat diberlakukan</label>
				    								</div>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    		<td style="width:33%" class="">
				    			<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<p><!-- Tanda tangan --> dokter anastesi :</p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="row">
				    							<div class="form-group">
				    								<div class="row">
					    								<label class="col-md-4">Nama</label>
					    								<div class="col-md-8">
					    									<input type="text" class="form-control input-xs" name="nama_dokter_anastesi">
					    								</div>
				    								</div>
				    							</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
				    		</td>
				    	</tr>
				    	