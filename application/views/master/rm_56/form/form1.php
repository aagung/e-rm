						<tr>
			    			<td style="width:33%" class="text-center">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<h3><b>Ceklis Keselamatan Operasi</b></h3>
			    						<h3>Surgical Safety Checklist</h3>
			    						<h5>RM-56/REV:0/XI/2015</h5>
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="form-group">
			    							<div class="row">
				    							<label class="col-md-4">Nama Pasien </label>
				    							<div class="col-md-8">
				    								<input type="text" class="form-control input-xs" name="nama_pasien">
				    							</div>
			    							</div>
			    						</div>
			    						<div class="form-group">
			    							<div class="row">
				    							<label class="col-md-4">Tanggal lahir </label>
				    							<div class="col-md-8">
				    								<input type="text" class="form-control input-xs" name="tanggal_lahir">
				    							</div>
			    							</div>
			    						</div>
			    						<div class="form-group">
			    							<div class="row">
				    							<label class="col-md-4">NO. RM </label>
				    							<div class="col-md-8">
				    								<input type="text" class="form-control input-xs" name="no_rm">
				    							</div>
			    							</div>
			    						</div>
			    						<div class="form-group">
			    							<div class="row">
				    							<label class="col-md-4">TANGGAL </label>
				    							<div class="col-md-8">
				    								<input type="text" class="form-control input-xs" name="tanggal">
				    							</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    		<td style="width:33%" class="text-center">
				    			<div class="col-md-12">
					    			<div class="row">
					    				<img src="#">
					    				<h5><b>RS ANANDA BEKASI</b></h5>
					    			</div>
				    			</div>
				    		</td>
				    	</tr>