						<tr>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Apakah Pasien sudah memberi konfirmasi identitas, Lokasi Operasi, Prosedur dan Persetujuannya ?
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="konfirmasi_pasien">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="konfirmasi_pasien">
			    									<label>Tidak</label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<div class="form-group">
			    									<div class="checkbox">
			    										<input type="checkbox" class="styled" name="konfirmasi_tim_bedah">
			    										<label>Lakukan Konfirmasi semua anggota tim bedah telah memperkenalkan diri (nama dan perannya)</label>
			    									</div>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    		<td style="width:33%" class="">
				    			<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Perawat secara lisan mengkonfirmasi :
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="konfirmasi_perawat_nama_prosedur">
			    									<label>Nama Prosedur</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="konfirmasi_perawat_kelengkapan_alat">
			    									<label>Kelengkapan alat, spons dan jumlah jarum</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="konfirmasi_perawat_labelisasi_spesimen">
			    									<label>Labelisasi spesimen (bacakan keras label specimen, termasuk nama pasien)</label>
			    								</div>
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="konfirmasi_perawat_ada_problem_alat_alat">
			    									<label>Apakah ada problem alat-alat yang harus disampaikan</label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
				    		</td>
				    	</tr>
				    	