						<tr>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<p>Apakah pasien memiliki :</p>
			    								<p>Alergi tertentu?</p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="alergi_pasien">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="alergi_pasien">
			    									<label>Tidak </label>
			    								</div>
			    							</div>
			    						</div>

			    						<div class="col-md-12">
			    							<div class="row">
			    								<p>Kesulitan jalan nafas atau risiko aspirasi ?</p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="kesulitan_jalan_nafas_risiko_aspirasi_pasien">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="kesulitan_jalan_nafas_risiko_aspirasi_pasien">
			    									<label>Tidak </label>
			    								</div>
			    							</div>
			    						</div>

			    						<div class="col-md-12">
			    							<div class="row">
			    								<p>Risiko perdarahan >500ml (7ml/kg pada anak)</p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="perdarahan_risiko_perdarahan">
			    									<label>Tidak </label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="perdarahan_risiko_perdarahan">
			    									<label>Ya, dan adakah akses sentra vena yang direncanakan</label>
			    								</div>
			    							</div>
			    						</div>
			    						
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="" rowspan="2">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<p><!-- Tanda tangan --> dokter bedah 1 : </p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="row">
				    							<div class="form-group">
				    								<div class="row">
					    								<label class="col-md-4">Nama</label>
					    								<div class="col-md-8">
					    									<input type="text" class="form-control input-xs" name="nama_dokter_bedah_1">
					    								</div>
				    								</div>
				    							</div>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="row">
			    								<p><!-- Tanda tangan --> dokter bedah 2 : </p>
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="row">
				    							<div class="form-group">
				    								<div class="row">
					    								<label class="col-md-4">Nama</label>
					    								<div class="col-md-8">
					    									<input type="text" class="form-control input-xs" name="nama_dokter_bedah_2">
					    								</div>
				    								</div>
				    							</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    	</tr>
						