						<tr>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Apakah lokasi operasi ditandai ?
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="radio">
			    									<input type="radio" class="styled" name="lokasi_operasi_ditandai">
			    									<label>Ya</label>
			    								</div>
			    								<div class="radio">
			    									<input type="radio" class="styled" name="lokasi_operasi_ditandai">
			    									<label>Tidak dapat diberlakukan</label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
			    			<td style="width:33%" class="">
			    				<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								<div class="form-group">
			    									<div class="checkbox">
			    										<input type="checkbox" class="styled" name="">
			    										<label>Lakukan Konfirmasi nama pasien, prosedur operasi, dan lokasi insisi yang akan dikerjakan</label>
			    									</div>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
			    			</td>
				    		<td style="width:33%" class="">
				    			<div class="col-md-12">
			    					<div class="row">
			    						<div class="col-md-12">
			    							<div class="row">
			    								Untuk ahli Bedah, Ahli Anastesi dan Perawat 
			    							</div>
			    						</div>
			    						<div class="col-md-12">
			    							<div class="form-group">
			    								<div class="checkbox">
			    									<input type="checkbox" class="styled" name="ahli_bedah_ahli_anstesi_perawat_perhatian_khusus_pasien">
			    									<label>Apakah ada perhatian khusus untuk kesembuhan dan pengobatan pasien ini?</label>
			    								</div>
			    							</div>
			    						</div>
			    					</div>
			    				</div>
				    		</td>
				    	</tr>
				    	