<tr>
	<th>1</th>
	<th colspan="2">EKSPRESI WAJAH</th>
	<th></th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ekspresi_wajah1" name="ekspresi_wajah" value="0"></th>
	<th><li>Relax,neutral</li></th>
	<th>Tak tampak ketegangan /kontraksi otot wajah</th>
	<th>0</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ekspresi_wajah2" name="ekspresi_wajah" value="1"></th>
	<th><li>Tegang</li></th>
	<th>Terlihat tegang,dahi mengerut,alis mata menurun,area sekitar mata mengencang atau perubahan lain (seperti membuka mata atau menangis selama prosedur menyakitkan dilakukan).</th>
	<th>1</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ekspresi_wajah3" name="ekspresi_wajah" value="2"></th>
	<th><li>Meringis</li></th>
	<th>Semua gerakan diatas ditambah kelopak mata menutup rapat (biasanya pasien membuka mulut atau mengigit ETT saat prosedur dilakukan)</th>
	<th>2</th>
</tr>