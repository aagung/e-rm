			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">Nama</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" name="nama" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">No.RM</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" name="no_rm" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">Tanggal Lahir/Umur</label>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-md-8">
									<input type="date" name="tgl_lahir" id="tanggal" placeholder="" class="form-control">
								</div>
								<div class="col-md-4">
									<input type="number" name="umur" id="umur" placeholder="umur" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">Tanggal Masuk</label>
						<div class="col-lg-6">
							<input type="date" class="form-control" name="tanggal_masuk" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">Ruang</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" name="ruang" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-4 control-label input-required">Diagnosa Medis</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" name="diagnosa_medis" value="" >
						</div>
					</div>
				</div>
			</div>