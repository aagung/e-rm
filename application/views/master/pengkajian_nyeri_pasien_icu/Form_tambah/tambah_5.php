<tr>
	<th>4</th>
	<th colspan="2">KETEGANGAN OTOT( dengan cara mengevaluasi pada saat melakukan fleksi dan ekstensi pasif ekstremitas
	atas di saat pasien istirahat atau dipindah posisikan/ MIKA/MIKI )</th>
	<th></th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ketegangan_otot1" name="ketegangan_otot" value="0"></th></th>
	<th><li>Relax</li></th>
	<th>Tidak melawan terhadap pergerakan pasif</th>
	<th>0</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ketegangan_otot2" name="ketegangan_otot" value="1"></th></th>
	<th><li>Tegang, kaku</li></th>
	<th>Ada perlawanan terhadap pergerakan pasif</th>
	<th>1</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="ketegangan_otot3" name="ketegangan_otot" value="2"></th></th>
	<th><li>Sangat tegang, kaku</li></th>
	<th>Perlawanan sangat kuat terhadap pergerakan pasif</th>
	<th>2</th>
</tr>