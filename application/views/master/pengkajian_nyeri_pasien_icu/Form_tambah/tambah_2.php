<tr>
	<th>2</th>
	<th colspan="2">GERAKAN TUBUH</th>
	<th></th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="gerakan_tubuh1" name="gerkan_tubuh" value="0"></th></th>
	<th><li>Tidak ada pergerakan atau posisi normal</li></th>
	<th>Tidak bergerak sama sekali ( hal ini berarti pasien tidak
		merasa sakit ) atau posisi normal ( pergerakan tidak
		dilakukan untuk merespon rangsang nyeri atau membuat
	pasien melindungi dirinya.</th>
	<th>0</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="gerakan_tubuh2" name="gerkan_tubuh" value="1"></th></th>
	<th><li>Perlindungan</li></th>
	<th>Gerakan lambat uasaha menyentuh atau menggosok
	daerah nyeri, mencari perhatian melalui gerakan.</th>
	<th>1</th>
</tr>
<tr>
	<th><input class="styled pengkajian" type="radio" id="gerakan_tubuh3" name="gerkan_tubuh" value="2"></th></th>
	<th><li>Gelisah / agitasi</li></th>
	<th>Menarik tabung atau mencabut selang, berusaha duduk,
		menggerakan kaki dan meronta – ronta, tidak mengikuti
		perintah, menyerang petugas, berusaha keluar dari
	tempat tidur.</th>
	<th>2</th>
</tr>