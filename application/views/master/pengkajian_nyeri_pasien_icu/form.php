<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
			<hr>
			<!-- BIODATA -->
			<?php include "Form_tambah/biodata.php"; ?>
		</div>
		<center><b>PENGKAJIAN NYERI PASIEN ICU</b></center>
		<div class="panel-body">
			<label><b>SKALA NYERI CPOT MENURUT GELINAS</b></label>
			<div class="table">
				<table class="table table-responsive">
					<tr>
						<th><b>No</b></th>
						<th class="text-center" colspan="2"><b>Kategori</b></th>
						<th>SKOR</th>
					</tr>
					<?php include "Form_tambah/tambah_1.php"; ?>
					<!-- FORM TAMBAH 1 -->
					<?php include "Form_tambah/tambah_2.php"; ?>
					<!-- FORM TAMBAH 2 -->
					<?php include "Form_tambah/tambah_3.php"; ?>
					<!-- FORM TAMABH 3 -->
					<?php include "Form_tambah/tambah_4.php"; ?>
					<!-- FORM TAMBAH 4 -->
					<?php include "Form_tambah/tambah_5.php"; ?>
					<!-- FORM TAMBAH 5 -->
					<tr>
						<th colspan="3" class="text-center"><b>NILAI TOTAL SKOR 1-8</b></th>
						<th><span class="totalskor"></span></th>
					</tr>
				</table>
				<div class="panel-body">
					<label>Catatan :</label><br>		
					<label>1. Skor 0 : tidak nyeri </label><br>
					<label>2. Skor 1-2 : nyeri ringan </label><br>
					<label>3. Skor 3-4 : nyeri sedang</label><br>
					<label>4. Skor 5-6 : nyeri berat</label><br>
					<label>5. Skor 7-8 : nyeri sangat berat</label>			
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
		</div>
	</div>
</form>