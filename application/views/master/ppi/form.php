	<!-- Untuk Radio dan checkbox styled -->
	<span class="switchery-warning switchery-info switchery-danger switchery-primary"></span>
   	<!-- Tutup checkbok dan Radio Styled -->

	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button> 
		        </div>
		        
		        <legend class="text-bold" style="margin-top:0px"></legend>
			         			
			    <div class="row">
		            <div class="col-md-12">
		            	<div class="col-md-12">
		            		<div class="row">
		            			<div class="tabbable">
									<ul class="nav nav-tabs nav-tabs-component">
										<li class="active"><a href="#jenis_tindakan" data-toggle="tab">JENIS TINDAKAN</a></li>
										<li><a href="#jenis_infeksi" data-toggle="tab">JENIS INFEKSI</a></li>
										<li><a href="#lainnya" data-toggle="tab">LAINNYA</a></li>
									</ul>
									<div class="tab-content">
										<!-- Form Jenis Tindakan -->
										<?php include 'form/jenis_tindakan.php' ?>

										<!-- Form Jenis Infeksi -->
										<?php include 'form/jenis_infeksi.php' ?>
											
										<!-- Form Lainnya -->
										<?php include 'form/lainnya.php' ?>											

									</div>
								</div>
		            		</div>
		            	</div>
		            </div>
			    </div>
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	<script>
	
</script>
