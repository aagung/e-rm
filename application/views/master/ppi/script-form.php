<style>
	
	table > tbody > tr > td,th {
		border:1px solid;
		border-right:none;
		border-left:none;
	
	}
	
	.verticalTop {
		vertical-align: top;
	}
	.margin-top-custom{
		margin-top:5px;
	}	 



</style>
<script>
$(document).ready(function(){
        //Jenis infeksi IADP
        $("#iadp_cb").on('change click',function(){
            if ($('#iadp_cb').is(':checked')) {
               $("#iadp_table").show();                                   
            }
            else{
            $("#iadp_table").hide();
           	$('#iadp_table').find("input[type=date], input[type=checkbox]").val("");
            $('#iadp_table').find("span input[type=checkbox]").prop('checked',false);
            $('#iadp_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#iadp_cb').is(':checked')) {
            $("#iadp_table").show();                                   
          }
          else{
            $("#iadp_table").hide();
            $('#iadp_table').find("input[type=date], input[type=checkbox]").val("");
            $('#iadp_table').find("span input[type=checkbox]").prop('checked',false);
            $('#iadp_table').find("span.checked").removeAttr('class');
               }

        //Jenis infeksi VAP
        $("#vap_cb").on('change click',function(){
            if ($('#vap_cb').is(':checked')) {
               $("#vap_table").show();                                   
            }
            else{
            $("#vap_table").hide();
           	$('#vap_table').find("input[type=date], input[type=checkbox]").val("");
            $('#vap_table').find("span input[type=checkbox]").prop('checked',false);
            $('#vap_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#vap_cb').is(':checked')) {
            $("#vap_table").show();                                   
          }
          else{
            $("#vap_table").hide();
            $('#vap_table').find("input[type=date], input[type=checkbox]").val("");
            $('#vap_table').find("span input[type=checkbox]").prop('checked',false);
            $('#vap_table').find("span.checked").removeAttr('class');
               }


        //Jenis infeksi IDO
        $("#ido_cb").on('change click',function(){
            if ($('#ido_cb').is(':checked')) {
               $("#ido_table").show();                                   
            }
            else{
            $("#ido_table").hide();
           	$('#ido_table').find("input[type=date], input[type=checkbox]").val("");
            $('#ido_table').find("span input[type=checkbox]").prop('checked',false);
            $('#ido_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#ido_cb').is(':checked')) {
            $("#ido_table").show();                                   
          }
          else{
            $("#ido_table").hide();
            $('#ido_table').find("input[type=date], input[type=checkbox]").val("");
            $('#ido_table').find("span input[type=checkbox]").prop('checked',false);
            $('#ido_table').find("span.checked").removeAttr('class');
               }


        //Jenis infeksi ISK
        $("#isk_cb").on('change click',function(){
            if ($('#isk_cb').is(':checked')) {
               $("#isk_table").show();                                   
            }
            else{
            $("#isk_table").hide();
           	$('#isk_table').find("input[type=date], input[type=checkbox]").val("");
            $('#isk_table').find("span input[type=checkbox]").prop('checked',false);
            $('#isk_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#isk_cb').is(':checked')) {
            $("#isk_table").show();                                   
          }
          else{
            $("#isk_table").hide();
            $('#isk_table').find("input[type=date], input[type=checkbox]").val("");
            $('#isk_table').find("span input[type=checkbox]").prop('checked',false);
            $('#isk_table').find("span.checked").removeAttr('class');
               }

       	//Lainnya Tirah Baring
        $("#tirah_baring_cb").on('change click',function(){
            if ($('#tirah_baring_cb').is(':checked')) {
               $("#tirah_baring_table").show();                                   
            }
            else{
            $("#tirah_baring_table").hide();
           	$('#tirah_baring_table').find("input[type=date], input[type=checkbox]").val("");
            $('#tirah_baring_table').find("span input[type=checkbox]").prop('checked',false);
            $('#tirah_baring_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#tirah_baring_cb').is(':checked')) {
            $("#tirah_baring_table").show();                                   
          }
          else{
            $("#tirah_baring_table").hide();
            $('#tirah_baring_table').find("input[type=date], input[type=checkbox]").val("");
            $('#tirah_baring_table').find("span input[type=checkbox]").prop('checked',false);
            $('#tirah_baring_table').find("span.checked").removeAttr('class');
               }

        //Lainnya Dekubitus
        $("#dekubitus_cb").on('change click',function(){
            if ($('#dekubitus_cb').is(':checked')) {
               $("#dekubitus_table").show();                                   
            }
            else{
            $("#dekubitus_table").hide();
           	$('#dekubitus_table').find("input[type=date], input[type=checkbox]").val("");
            $('#dekubitus_table').find("span input[type=checkbox]").prop('checked',false);
            $('#dekubitus_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#dekubitus_cb').is(':checked')) {
            $("#dekubitus_table").show();                                   
          }
          else{
            $("#dekubitus_table").hide();
            $('#dekubitus_table').find("input[type=date], input[type=checkbox]").val("");
            $('#dekubitus_table').find("span input[type=checkbox]").prop('checked',false);
            $('#dekubitus_table').find("span.checked").removeAttr('class');
               }

        //Lainnya PHLEBITIS
        $("#phlebitis_cb").on('change click',function(){
            if ($('#phlebitis_cb').is(':checked')) {
               $("#phlebitis_table").show();                                   
            }
            else{
            $("#phlebitis_table").hide();
           	$('#phlebitis_table').find("input[type=date], input[type=checkbox]").val("");
            $('#phlebitis_table').find("span input[type=checkbox]").prop('checked',false);
            $('#phlebitis_table').find("span.checked").removeAttr('class');
            }
        });

          if ($('#phlebitis_cb').is(':checked')) {
            $("#phlebitis_table").show();                                   
          }
          else{
            $("#phlebitis_table").hide();
            $('#phlebitis_table').find("input[type=date], input[type=checkbox]").val("");
            $('#phlebitis_table').find("span input[type=checkbox]").prop('checked',false);
            $('#phlebitis_table').find("span.checked").removeAttr('class');
               }


});
</script>