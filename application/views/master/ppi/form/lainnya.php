
											<div class="tab-pane" id="lainnya">
												<label class="col-md-12"> LAINNYA</label>
												<div class="col-md-12">
													<div class="row">														
														<ul class="nav nav-tabs nav-tabs-component nav-justified">
															<li class="active col-md-6"><a href="#tirah_baring" class="form-control" data-toggle="tab">1. TIRAH BARING</a></li>
															<li class="col-md-6"><a href="#dekubitus" class="form-control" data-toggle="tab">2. DEKUBITUS</a></li>
															<li class="col-md-6"><a href="#phlebitis" class="form-control" data-toggle="tab">3. PHLEBITIS</a></li>
															
														</ul>
														<div class="tab-content">
															<div class="tab-pane active" id="tirah_baring">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="lainnya_tirah_baring" id="tirah_baring_cb">
																			<label>TIRAH BARING</label>
																		</div>
																		<table class="" style="width:100%" id="tirah_baring_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:30%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:45%;">KRITERIA</td>
																					<td class="text-center" style="width:25%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="col-md-8">
																									<div class="form-group">
																										<div class="row">
																											<input type="date" class="form-control" name="lainnya_tirah_baring_tanggal_kejadian">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-12" style="background-color:white;">
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_tirah_baring_kriteria_adanya_advis_doketer">
																												<label>Adanya Advis Doketer Terkait penyakit yang diderita</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_tirah_baring_kondisi_pasien_tidak_memungkinkan_untuk_mobilisasi">
																												<label>Kondisi Pasien yang tidak memungkinkan untuk mobilisasi</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_tirah_baring_fisiologis">
																												<label>Fisiologis</label>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">

																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="tab-pane" id="dekubitus">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="lainnya_dekubitus" id="dekubitus_cb">
																			<label>DEKUBITUS</label>
																		</div>
																		<table class="" style="width:100%" id="dekubitus_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:30%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:40%;">KRITERIA</td>
																					<td class="text-center" style="width:30%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="col-md-8">
																									<div class="form-group">
																										<div class="row">
																											<input type="date" class="form-control" name="lainnya_dekubitus_tanggal_kejadian">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td >
																						<div class="col-md-12">
																							<div class="col-md-12" style="background-color:white;">
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_dekubitus_kriteria_reaksi_peradangan_terbatas">
																												<label>Reaksi Peradangan terbatas pada epidermis, tampak kemerahan, Indurasi, atau lecet</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_dekubitus_kriteria_reaksi_sampai_seluruh_dermis_lapisan_lemak_subkutan">
																												<label>Reaksi sampai seluruh dermis lapisan lemak subkutan, ulkus dangakal dengan tepi tegas dan perubahan pigmen kulit</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_dekubitus_kriteria_ulkus_meliputi_jaringan_lemak_subkutan_dan_menggaung">
																												<label>Ulkus meliputi jaringan lemak subkutan dan menggaung, terdapat jaringan nekrotik yang berbau</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="col-md-12">
																											<div class="checkbox">
																												<input type="checkbox" class="styled" name="lainnya_dekubitus_kriteria_ulkus_menembus_otot_hingga_tulang_besar">
																												<label>Ulkus menembus otot hingga tulangnbesar didasar ulkus yang dapat mengakibatkan infeksi pada tulang</label>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_dekubitus_penerapan_bundle_merubah_posisis_minimal_2_jam_sekali">
																											<label>Merubah posisi minimal 2 jam sekali</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_dekubitus_penerapan_bundle_pemeberian_cairan_dan_nutrisi_tepat_dan_adekuat">
																											<label>Pemberian cairan dan nutrisi yang tepat dan adekuat</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_dekubitus_penerapan_bundle_menjaga_kulit_tetap_kering_dan_bersih">
																											<label>Menjaga kulit tetap kering dan bersih</label>
																										</div>
																									</div>
																								</div>
																								
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="tab-pane" id="phlebitis">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="lainnya_phlebitis" id="phlebitis_cb">
																			<label>PHLEBITIS</label>
																		</div>
																		<table class="" style="width:100%" id="phlebitis_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:30%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:40%;">KRITERIA</td>
																					<td class="text-center" style="width:30%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="col-md-8">
																									<div class="form-group">
																										<div class="row">
																											<input type="date" class="form-control" name="lainnya_phlebitis_tanggal_kejadian">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-8" style="background-color:white;">
																								<div class="row">
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_nyeri">
																													<label>Nyeri</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_kemerahan">
																													<label>Kemerahan</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_pembengkakan">
																													<label>Pembengkakan</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_indurasi">
																													<label>Indurasi</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_venous_cord_teraba">
																													<label>Venous cord teraba</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="lainnya_phlebitis_kriteria_demam">
																													<label>Demam</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_phlebitis_penerapan_bundle_kebersihan_tangan">
																											<label>Kebersihan tangan</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_phlebitis_penerapan_bundle_pemakaiana_adp_yang_sesuai">
																											<label>Pemakaian APD yang sesuai</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_phlebitis_penerapan_bundle_pemilihan_lokasi_inseri">
																											<label>Pemilihan lokasi inseri</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_phlebitis_penerapan_bundle_desinfeksi">
																											<label>Desinfeksi dengan Chlorhexidine 0,2%</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="lainnya_phlebitis_penerapan_bundle_monitoring_dan_evaluasi_balutan">
																											<label>Monitoring dan evaluasi balutan setiap hari</label>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>