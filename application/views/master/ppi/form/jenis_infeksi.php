											<div class="tab-pane" id="jenis_infeksi">
												<label class="col-md-12"> JENIS INFEKSI RUMAH SAKIT</label>
												<div class="col-md-12">
													<div class="row">														
														<ul class="nav nav-tabs nav-tabs-component nav-justified" id="nav">
															<li class="col-md-6"><a href="#IDO" class="form-control" data-toggle="tab">3. IDO(Infeksi Daerah Operasi)</a></li>
															<li class="col-md-6"><a href="#ISK" class="form-control" data-toggle="tab">4. ISK (Infeksi Saluran Kemih)</a></li>
															<br>
															<li class="active col-md-6"><a href="#IADP" class="form-control" data-toggle="tab">1. IADP (Infeksi Aliran Darah Primer)</a></li>
															<li class="col-md-6"><a href="#VAP" class="form-control" data-toggle="tab">2. VAP (Ventilator Associated Pneumonia)</a></li>
														</ul>
														<div class="tab-content">
															<div class="tab-pane active" id="IADP">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="jenis_infeksi_iadp" id="iadp_cb">
																			<label>IADP (Infeksi Aliran Darah Primer)</label>
																		</div>
																		<table class="" style="width:100%" id="iadp_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:20%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:54%;">KRITERIA</td>
																					<td class="text-center" style="width:25%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="col-md-12">
																									<div class="form-group">
																										<div class="row">
																											<input type="date" class="form-control" name="jenis_infeksi_iadp_tanggal_kejadian">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<div class="checkbox">
																										<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1">
																										<label>KRITERIA 1 : (UMUM)</label>
																									</div>
																								</div>

																								<div class="col-md-12">
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1_demam">
																														<label class="col-md-12"> Demam >= 30
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1_mengigil">
																														<label class="col-md-12"> Mengigil
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1_hipotensi">
																														<label class="col-md-12"> Hipotensi
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1_infeksi_ditempat_lain_negatif">
																														<label class="col-md-12"> Bukti infeksi ditempat lain : negatif
																														</label>
																														<label class="col-md-12">Laboratorium : ditemukan
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_1_mikrooganisme_pada_pemeriksaan_kultur_darah">
																														<label class="col-md-12"> mikroorganisme pada pemeriksaan kultur darah
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<div class="checkbox">
																										<input type="checkbox" class="styled" name="">
																										<label>KRITERIA 2 : ANAK <= 1 TH</label>
																									</div>
																								</div>
																								<div class="col-md-12">
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_2_demam">
																														<label class="col-md-12"> Demam >= 30
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="">
																														<label class="col-md-12"> Mengigil
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_2_apnoe">
																														<label class="col-md-12"> Apnoe
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_2_infeksi_ditempat_lain_negatif">
																														<label class="col-md-12"> Bukti infeksi ditempat lain : negatif
																														</label>
																														<label class="col-md-12">Laboratorium : ditemukan
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="col-md-12">
																													<div class="checkbox">
																														<input type="checkbox" class="styled" name="jenis_infeksi_iadp_kriteria_2_mikroganisme_pada_pemeriksaan_kultur_darah">
																														<label class="col-md-12"> mikroorganisme pada pemeriksaan kultur darah
																														</label>
																													</div>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_iadp_penerapan_bundle_hand_hygiene">
																											<label>Hand Hygiene 
																											(Kebersihan Tangan)</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_iadp_penerapan_bundle_pemakaian_apd">
																											<label>Pemakaian APD </label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_iadp_penerapan_bundle_pemilihan_lokasi_nyeri">
																											<label>Pemilihan Lokasi Inseri</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_iadp_penerapan_bundle_desinfeksi">
																											<label>Desinfeksi dengan Chlorhexidine 0,2%</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_iadp_penerapan_bundle_perawatan_harian">
																											<label>Perawatan harian</label>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="tab-pane" id="VAP">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="jenis_infeksi_vap" id="vap_cb">
																			<label>VAP (Venitilator Associated Pneumonia)</label>
																		</div>
																		<table class="" style="width:100%" id="vap_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:20%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:56%;">KRITERIA</td>
																					<td class="text-center" style="width:24%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="col-md-12">
																									<div class="form-group">
																										<div class="row">
																											<input type="date" class="form-control" name="jenis_infeksi_vap_tanggal_kejadian">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-4" style="background-color:white;">
																								<div class="row">
																									<label>Tanda dan Gejala :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_demam">
																													<label>Demam >= 38 C</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_hipothermi">
																													<label>Hipothermi (<= 35 C)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_sputum_purulent">
																													<label>Sputum purulent</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_batuk">
																													<label>Batuk</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_dyspnoe">
																													<label>Dyspnoe</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_tachypnoe">
																													<label>Tachypnoe</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_kriteria_tanda_dan_gejala_bronchial">
																													<label>Suara nafas : Rales / Bronchial</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>

																							<div class="col-md-4" style="background-color:white;">
																								<div class="row">
																									<label>PEMERIKSAAN X-RAY :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_xray_tachypnoe">
																													<label>Inlitrat baru, persisten atau progresif</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_xray_kavitasi">
																													<label>Kavitasi</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_xray_konosolidasi">
																													<label>Konsolidasi</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_xray_pneumatocole">
																													<label>Pneumatocole (usia <= 1 th)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>

																							<div class="col-md-4" style="background-color:white;">
																								<div class="row">
																									<label>PEMERIKSAAN LABORATORIUM :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_laboratorium_leukosit">
																													<label>Leukosit >= 12.000/mm3, atau <= 4.000/mm3</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_laboratorium_pa02_fi02">
																													<label>Pa02/Fi02 <= 240 mmHg</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_vap_pemeriksaan_laboratorium_kultur_sputum_posistif_ditemukan_mikroorganisme">
																													<label>Kultur sputum positif ditemukan mikroorganisme</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_hand_hygiene">
																											<label>Hand Hygiene 
																											(Kebersihan Tangan)</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_kebersihan_mulut">
																											<label>Kebersihan mulut dengan chlorhexidine antiseptic 0.2%</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_posisi_kepala_pasien">
																											<label>Posisi kepala pasien 30 - 45</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_manajemen_sekresi">
																											<label>manajemen sekresi</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_pengkajian_sedasi_daj_esklubasi">
																											<label>Pengkajian setiap hari "sedasi" dan "ekslubasi"</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_pressure_cuff">
																											<label>Pressure Cuff 20-30 cm H20</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_dvt">
																											<label>DVT Profilaksis</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_vap_penerapan_bundle_peptic_ucler_profilaksis">
																											<label>Peptic Ulcer Profilaksis</label>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="tab-pane" id="IDO">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="jenis_infeksi_ido" id="ido_cb">
																			<label>IDO (Infeksi Daerah Operasi)</label>
																		</div>
																		<table class="" style="width:100%" id="ido_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:20%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:56%;">KRITERIA</td>
																					<td class="text-center" style="width:24%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<input type="date" class="form-control" name="jenis_infeksi_ido_tanggal_kejadian">
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<label>TANDA dan GEJALA :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_ido_kriteria_tanda_dan_gejala_keluar_cairan_purulen">
																													<label>Keluaran cairan purulen dari luka insisi (bukan rongga atau organ)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_ido_kriteria_tanda_dan_gejala_demam">
																													<label>Demam (>= 38 C)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_ido_kriteria_tanda_dan_gejala_tanda_infeksi_lokal">
																													<label>Tanda Infeksi Lokal (Nyeri daerah operasi, kemerahan dan bengkak lokal)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>

																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<label>PEMERIKSAAN LABORATORIUM :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_ido_kriteria_pemeriksaan_laboratorium_kultur_positif_cairan_yang_keluar_luka_operasi">
																													<label>Kultur posistif dari cairan yang keluar / jaringan dari luka operasi</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_ido_penerapan_bundle_pencukuran_daerah_operasi">
																											<label>Pencukuran daerah operasi bila perlu</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_ido_penerapan_bundle_antibiotik_profilaksis">
																											<label>Antibiotik profilaksis</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_ido_penerapan_bundle_temperatur_pasien_normal">
																											<label>Temperatur pasien normal</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_ido_penerapan_bundle_hasil_gula_darah_normal">
																											<label>Hasil gula darah normal</label>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="tab-pane" id="ISK">
																<div class="col-md-12">
																	<div class="row">
																		<div class="checkbox">
																			<input type="checkbox" class="styled" name="jenis_infeksi_isk" id="isk_cb">
																			<label>ISK (Infeksi Saluran Kemih)</label>
																		</div>
																		<table class="" style="width:100%" id="isk_table">
																			<thead style="border-bottom:1px solid;">
																				<tr>
																					<td style="width:20%;">TANGGAL KEJADIAN</td>
																					<td class="text-center" style="width:56%;">KRITERIA</td>
																					<td class="text-center" style="width:24%;">PENERAPAN BUNDLE</td>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<input type="date" class="form-control" name="jenis_infeksi_isk_tanggal_kejadian">
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<label>TANDA dan GEJALA :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_isk_kriteria_tanda_dan_gejala_demam">
																													<label>Demam (>= 38 C)</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_isk_kriteria_tanda_dan_gejala_nyeri_suprapublik">
																													<label>Nyeri suprapublik,atau nyeri sudut kostovertebral</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>

																							<div class="col-md-6" style="background-color:white;">
																								<div class="row">
																									<label>PEMERIKSAAN LABORATORIUM :</label>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_isk_kriteria_pemeriksaan_laboratorium_urine_lengkap_ditemukan_bakteri">
																													<label>Urine lengkap : ditemukan bakteri</label>
																												</div>
																											</div>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-md-12">
																											<div class="row">
																												<div class="checkbox">
																													<input type="checkbox" class="styled" name="jenis_infeksi_isk_kriteria_pemeriksaan_laboratorium_kultur_urine_positif">
																													<label>Kultur urine positif</label>
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						
																					</td>
																					<td class="verticalTop">
																						<div class="col-md-12 margin-top-custom">
																							<div class="row">
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_kaji_indikasi">
																											<label>Kaji indikasi</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_hand_hygiene">
																											<label>Hand Hygiene (kebersihan tangan)</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_teknik_inseri">
																											<label>Teknik Inseri</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_pemeliharaan_catheter">
																											<label>Pemeliharaan catheter</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_perawatan_catheter">
																											<label>Perawatan catheter</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<div class="row">
																										<div class="checkbox">
																											<input type="checkbox" class="styled" name="jenis_infeksi_isk_penerapan_bundle_remove_catheter">
																											<label>Remove catheter</label>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>