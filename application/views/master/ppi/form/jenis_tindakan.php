											<div class="tab-pane active" id="jenis_tindakan">
												<label class="col-md-12">	:: Jenis Tindakan :: </label>
												<div class="col-md-12">
													<div class="row">
														<!-- <div class="table-responsive">
															<table class="" style="border:none; width:100%; border-bottom:1px solid;">
																<thead style="border:none;">
																	<tr>
																		<th style="width:30%; border:none;">Jenis Tindakan</th>
																		<th style="width:17%; border:none;">TANGGAL PEMASANGAN</th>
																		<th style="width:17%; border:none;">TANGGAL PENGGANTIAN</th>
																		<th style="width:17%; border:none;">LOKASI PEMASANGAN</th>
																		<th style="width:17%; border:none;">TANGGAL AFF</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="border:none;" class="none-border">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<div class="checkbox">
																							<input type="checkbox" class="styled" name="jenis_tindakan_ett">
																							<label class="col-md-12">ETT (Endo Tracheal Tube)</label>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																		<td style="border:none;" class="none-border">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_pemasangan_ett">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td style="" class="none-border">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_penggantian_ett">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td style="" class="none-border">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="lokasi_pemasangan_ett">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td style="" class="none-border">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_aff_ett">
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<div class="checkbox">
																							<input type="checkbox" class="styled" name="jenis_tindakan_cvc">
																							<label class="col-md-12">CVC (Central Vena Catheler)</label>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_pemasangan_cvc">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_penggantian_cvc">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="lokasi_pemasangan_cvc">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_aff_cvc">
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr >
																		<td class="verticalTop" style>
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<div class="checkbox">
																							<input type="checkbox" class="styled" name="jenis_tindakan_IVL_1">
																							<label class="col-md-12">
																								IVL ke-1 (Intra Vena Line / Infus Perifes)
																							</label>
																						</div>
																						
																					</div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<div class="form-group">
																					<div class="col-md-12">
																						<div class="row">
																							<div class="col-md-12">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="jenis_tindakan_IVL_2">
																									<label class="col-md-12">ILV ke-2
																									</label>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-md-12">
																						<div class="row">
																							<div class="col-md-12">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="jenis_tindakan_IVL_3">
																									<label class="col-md-12">ILV ke-3
																									</label>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_pemasangan_IVL_1">
																					</div>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_pemasangan_IVL_1">
																					</div>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_pemasangan_IVL_1">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_penggantian_IVL_1">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="lokasi_pemasangan_IVL_1">
																					</div>
																				</div>
																			</div>
																		</td>
																		<td class="verticalTop">
																			<div class="form-group">
																				<div class="col-md-12">
																					<div class="row">
																						<input type="text" class="form-control input-xs input-xs" name="tanggal_aff_IVL_1">
																					</div>
																				</div>
																			</div>

																		</td>
																	</tr>
																</tbody>
															</table>
														</div> -->
														<div class="table-responsive">
															<table class="table">
																<thead>
																	<tr>
																		<th>
																			<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="row">
																							<label>Jenis Tindakan</label>
																						</div>	
																					</div>
																					<div class="col-md-2 text-center">
																						<div class="row">
																							<label>TGL PEMASANGAN</label>
																						</div>	
																					</div>
																					<div class="col-md-2 text-center">
																						<div class="row">
																							<label>TGL PENGGANTIAN</label>
																						</div>	
																					</div>
																					<div class="col-md-2 text-center">
																						<div class="row">
																							<label>LOKASI PEMASANGAN</label>
																						</div>	
																					</div>
																					<div class="col-md-2 text-center">
																						<div class="row">
																							<label>TGL AFF</label>
																						</div>	
																					</div>
																				</div>
																			</div>
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>
																			<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="form-group">
																							<div class="row">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="ett_jenis_tindakan">
																									<label>ETT (Enddo Trachead Tube)</label>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs " name="ett_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ett_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ett_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ett_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="form-group">
																							<div class="row">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="cvc_jenis_tindakan">
																									<label>CVC (Central Vena Catheter)</label>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs " name="cvc_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="cvc_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="cvc_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="cvc_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="form-group">
																							<div class="row">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="ivl_1_jenis_tindakan">
																									<label class="col-md-12">IVL ke-1 (Intra Vena Line / Infus Perifer)</label>
																								</div>
																								
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs " name="ivl_1_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_1_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_1_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_1_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>

																			<div class="col-md-12 margin-top-custom">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="col-md-12">
																							<div class="form-group">
																								<div class="col-md-12">
																									<div class="checkbox">
																										<input type="checkbox" class="styled" name="ivl_2_jenis_tindakan">
																										<label class="col-md-12">IVL ke-2</label>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs " name="ivl_2_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_2_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_2_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_2_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>

																			<div class="col-md-12 margin-top-custom">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="col-md-12">
																							<div class="form-group">
																								<div class="col-md-12">
																									<div class="checkbox">
																										<input type="checkbox" class="styled" name="ivl_3_jenis_tindakan">
																										<label class="col-md-12">IVL ke-3</label>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs " name="ivl_3_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_3_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_3_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="ivl_3_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-4">
																						<div class="form-group">
																							<div class="row">
																								<div class="checkbox">
																									<input type="checkbox" class="styled" name="dc_jenis_tindakan">
																									<label>DC (Dower Catether)</label>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="dc_tanggal_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="dc_tanggal_penggantian">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="dc_lokasi_pemasangan">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="row">
																							<div class="form-group">
																								<div class="col-md-11">
																									<div class="row">
																										<input type="text" class="form-control input-xs" name="dc_tanggal_aff">
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>