<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-4 control-label input-required"><?php echo strtoupper($option->option_name); ?></label>
						<div class="col-lg-6">
							<input type="text" class="form-control" name="option_value" value="<?php echo $option->option_value; ?>" >
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="<?php echo $option->option_id; ?>" />
			<input type="hidden" name="option_name" value="<?php echo $option->option_name; ?>" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>