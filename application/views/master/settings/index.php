<?php echo messages(); ?>
<style type="text/css">
    #btn-refresh {
        position: fixed;
        bottom: 20px;
        right: 20px;
        z-index: 10000;
    }
    a.disabled {
        pointer-events: none;
    }
    .text-white {
        color: white !important;
    }
    #table td {
        text-transform: none;
    }
</style>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body uppercase">
            <button type="button" id="btn-refresh" class="btn bg-slate btn-float btn-rounded">
                <b>
                    <i class="icon-database-refresh"></i>
                </b>
            </button>
            <div class="no-padding-top">
                <div class="table-responsive">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                            <tr class="bg-slate">
                                <th class="text-center">
                                    Setting Name
                                </th>
                                <th class="text-center">
                                    Value
                                </th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>