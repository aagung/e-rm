<script>
let form = '#form',
	url = {
		index: "<?php echo site_url('master/settings'); ?>", 
  		save: "<?php echo site_url('api/master/options/save'); ?>",
	};


$(document).ready(function() {
  $(".cancel-button").click(function () {
    window.location.assign(url.index);
  });

	$(form).validate({
    	rules: {
      		value: { required: true },
    	},
    	messages: {},
    	focusInvalid: true,
    	submitHandler: function (form) {
    		blockElement($(form));
      		
      		let formData = $(form).serialize();
      		$.ajax({
        		data: formData,
        		type: 'POST',
        		dataType: 'JSON', 
        		url: url.save,
        		success: function(data){
            		$(form).unblock();
            		successMessage('Berhasil', "Data berhasil disimpan.");
            		window.location.assign(url.index);
        		},
        		error: function(data){
            		$(form).unblock();
            		errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");

            		$('.input-decimal').each(function() {
						$(this).autoNumeric('set', $(this).val());
					});
        		}
      		});
      		return false;
    	}
  	});
});
</script>