	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>

		        <legend class="text-bold" style="margin-top:0px"></legend>
				    
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-10">
									<div class="row">
										<!-- <ul style="list-style-type:none; margin:0; padding:0;">
											<li>
												<div class="form-group">
													<div class="row">
														<label class="col-md-3">kesadaran</label>
														<div class="col-md-6">
															<input type="text" class="form-control input-xs" name="">
														</div>
														
													</div>
												</div>
											</li>
										</ul> -->
										<br><br>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">kesadaran</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="kesadaran">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Tekanan darah</label>
												<div class="col-md-6">
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="tekanan_darah">
														<span class="input-group-addon">mmHg</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Nadi</label>
												<div class="col-md-6">
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="madi">
														<span class="input-group-addon">x/menit</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Pernapasan</label>
												<div class="col-md-6">
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="pernafasan">
														<span class="input-group-addon">x/menit</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Suhu</label>
												<div class="col-md-6">
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="suhu">
														<span class="input-group-addon">&#8451;</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Berat Badan</label>
												<div class="col-md-6">
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="berat_badan">
														<span class="input-group-addon">kg</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Surat Izin Operasi</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="surat_izin_operasi">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Dokumen Medik Pasien</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="dokumen_medik_pasien">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Persiapan Darah:Golongan</label>
												<div class="col-md-7">
													<label class="radio-inline">
			    										<input type="radio" class="styled" value="A" name="golongan_darah" >A
			    									</label>
			    									<label class="radio-inline">
			    										<input type="radio" class="styled" value="B" name="golongan_darah" >B
			    									</label>
			    									<label class="radio-inline">
			    										<input type="radio" class="styled" value="AB" name="golongan_darah" >AB
			    									</label>
			    									<label class="radio-inline">
			    										<input type="radio" class="styled" value="O" name="golongan_darah" >O
			    									</label>
												</div>
											</div>
										</div>
										<ul style="list-style-type:none">
											<li>
												<div class="col-md-4">
													<div class="form-group">
														<div class="row">
															<label class="col-md-6">
																<div style="text-align:right">Rhesus:</div>
															</label>
															<div class="col-md-6">
																<input type="number" placeholder="+/-" class="form-control input-xs" name="rhesus">
															</div>
															
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="row">
															<label class="col-md-2" >Jumlah:</label>
															<div class="col-md-4">
																<div class="input-group">
																	<input type="text" class="form-control input-xs" name="jumlah">
																	<span class="input-group-addon">
																		cc
																	</span>
																</div>
															</div>
															
														</div>
													</div>
												</div>
											</li>
										</ul>
										<!-- <div class="form-group">
											<div class="row">
												<label class="col-md-3">Rhesus</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="">
												</div>
												
											</div>
										</div> -->
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Premedikasi</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="premedikasi">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Nama Obat</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="nama_obat">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Jam Pemberian</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="jam_pemberian">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Puasa</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="puasa">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Huknah</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="huknah">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Persiapan kulit / cukur</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="persapan_kulit_cukur">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Protese yang dilepas</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="protese_yang_dilepas">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Perhiasan yang dilepas</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="perhiasan_yang_dilepas">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Mandi</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="mandi">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Ganti baju ok</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="ganti_baju_ok">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Penandaan lokasi operasi</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="penandaan_lokasi_operasi">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Hasil Laboratorium (terlampir) </label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="hasil_laboratorium">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Hasil Radiologi (terlampir)</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="hasil_radiologi">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Hasil EKG (terlampir)</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="hasil_ekg">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Infus:Lokasi:</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="infus">
												</div>
												
											</div>
										</div>

										<ul style="list-style-type:none;  margin:0;">
											<li>
												<div class="form-group">
													<div class="row">
														<label class="col-md-3">Jenis cairan</label>
														<div class="col-md-6">
															<input type="text" class="form-control input-xs" name="infus_jenis_cairan">
														</div>
														
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-3">Kecepatan</label>
														<div class="col-md-6">
															<input type="text" class="form-control input-xs" name="infus_kecepatan">
														</div>
														
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-3">Sisa cairan</label>
														<div class="col-md-6">
															<input type="text" class="form-control input-xs" name="infus_sisa_cairan">
														</div>
														
													</div>
												</div>
											</li>
										</ul>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Terpasang kateter urine</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="terpasang_kateter_urine">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Alergi Obat (nama obat)</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-xs" name="alergi_obat">
												</div>
												
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Lain-lain / keterangan</label>
												<div class="col-md-6">
													<textarea class="form-control" name="lain_lain_keterangan"></textarea>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2" >
									<div class="row">
										<table class="table-cus" style="border:1px solid black; padding:10px;">
											<tr style="">
												<th class="text-center">YA</th>
												<th class="text-center">TIDAK</th>
											</tr>
											<tr id="kesadaran">
												<td class="text-center">
													<input type="radio" class="styled" name="kesadaran2">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="kesadaran2">
												</td>
											</tr>
											<tr id="tekanan_darah">
												<td class="text-center">
													<input type="radio" class="styled" name="tekanan_darah">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="tekanan_darah">
												</td>
											</tr>
											<tr id="nadi">
												<td class="text-center">
													<input type="radio" class="styled" name="nadi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="nadi">
												</td>
											</tr>
											<tr id="pernapasan">
												<td class="text-center">
													<input type="radio" class="styled" name="pernapasan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="pernapasan">
												</td>
											</tr>
											<tr id="suhu">
												<td class="text-center">
													<input type="radio" class="styled" name="suhu">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="suhu">
												</td>
											</tr>
											<tr id="berat_badan">
												<td class="text-center">
													<input type="radio" class="styled" name="berat_badan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="berat_badan">
												</td>
											</tr>
											<tr id="surat_izin_operasi">
												<td class="text-center">
													<input type="radio" class="styled" name="surat_izin_operasi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="surat_izin_operasi">
												</td>
											</tr>
											<tr id="dokumen_medik_pasien">
												<td class="text-center">
													<input type="radio" class="styled" name="dokumen_medik_pasien">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="dokumen_medik_pasien">
												</td>
											</tr>
											<tr id="persiapan_golongan_darah">
												<td class="text-center">
													<input type="radio" class="styled" name="persiapan_golongan_darah">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="persiapan_golongan_darah">
												</td>
											</tr>
											<tr id="rhesus_jumlah">
												<td class="text-center">
													<input type="radio" class="styled" name="rhesus_jumlah">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="rhesus_jumlah">
												</td>
											</tr>
											<tr id="premedikasi">
												<td class="text-center">
													<input type="radio" class="styled" name="premedikasi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="premedikasi">
												</td>
											</tr>
											<tr id="nama_obat">
												<td class="text-center">
													<input type="radio" class="styled" name="nama_obat">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="nama_obat">
												</td>
											</tr>
											<tr id="jam_pemberian">
												<td class="text-center">
													<input type="radio" class="styled" name="jam_pemberian">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="jam_pemberian">
												</td>
											</tr>
											<tr id="puasa">
												<td class="text-center">
													<input type="radio" class="styled" name="puasa">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="puasa">
												</td>
											</tr>
											<tr id="huknah">
												<td class="text-center">
													<input type="radio" class="styled" name="huknah">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="huknah">
												</td>
											</tr>
											<tr id="persapan_kulit_cukur">
												<td class="text-center">
													<input type="radio" class="styled" name="persapan_kulit_cukur">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="persapan_kulit_cukur">
												</td>
											</tr>
											<tr id="protese_yang_dilepas">
												<td class="text-center">
													<input type="radio" class="styled" name="protese_yang_dilepas">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="protese_yang_dilepas">
												</td>
											</tr>
											<tr id="perhiasan_yang_dilepas">
												<td class="text-center">
													<input type="radio" class="styled" name="perhiasan_yang_dilepas">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="perhiasan_yang_dilepas">
												</td>
											</tr>
											<tr id="mandi">
												<td class="text-center">
													<input type="radio" class="styled" name="mandi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="mandi">
												</td>
											</tr>
											<tr id="ganti_baju_ok">
												<td class="text-center">
													<input type="radio" class="styled" name="ganti_baju_ok">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="ganti_baju_ok">
												</td>
											</tr>
											<tr id="penandaan_lokasi_operasi">
												<td class="text-center">
													<input type="radio" class="styled" name="penandaan_lokasi_operasi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="penandaan_lokasi_operasi">
												</td>
											</tr>
											<tr id="hasil_laboratorium">
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_laboratorium">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_laboratorium">
												</td>
											</tr>
											<tr id="hasil_radiologi">
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_radiologi">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_radiologi">
												</td>
											</tr>
											<tr id="hasil_ekg">
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_ekg">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="hasil_ekg">
												</td>
											</tr>
											<tr id="infus">
												<td class="text-center">
													<input type="radio" class="styled" name="infus">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="infus">
												</td>
											</tr>
											<tr id="infus_jenis_cairan">
												<td class="text-center">
													<input type="radio" class="styled" name="infus_jenis_cairan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="infus_jenis_cairan">
												</td>
											</tr>
											<tr id="infus_kecepatan">
												<td class="text-center">
													<input type="radio" class="styled" name="infus_kecepatan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="infus_kecepatan">
												</td>
											</tr>
											<tr id="infus_sisa_cairan">
												<td class="text-center">
													<input type="radio" class="styled" name="infus_sisa_cairan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="infus_sisa_cairan">
												</td>
											</tr>
											<tr id="terpasang_kateter_urine">
												<td class="text-center">
													<input type="radio" class="styled" name="terpasang_kateter_urine">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="terpasang_kateter_urine">
												</td>
											</tr>
											<tr id="alergi_obat">
												<td class="text-center">
													<input type="radio" class="styled" name="alergi_obat">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="alergi_obat">
												</td>
											</tr>
											<tr id="lain_lain_keterangan">
												<td class="text-center">
													<input type="radio" class="styled" name="lain_lain_keterangan">
												</td>
												<td class="text-center">
													<input type="radio" class="styled" name="lain_lain_keterangan">
												</td>
											</tr>
										</table>
										<!-- <div class="col-md-12 text-center">
											<label class="col-md-6">YA</label>
											<label class="col-md-6">TIDAK</label>
											<div>
												
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-md-6">
														<label>
															<input type="radio" class="styled" name="">
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<label>
														<input type="radio" class="styled" name="">
													</label>
												</div>
											</div>
											
										</div> -->
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="row">
								<p><b><ins>ASA CLASSIFICATION</ins></b></p>
								<div class="col-md-12">
									<div class="form-group">
										<div class="checkbox">
											<ul style="list-style-type:none;">
												<li>
													<label class="col-md-12">
														<input type="checkbox" class="styled" name="">
														ASA 1 Pasien normal yang sehat
													</label>
												</li>
												<li>
													<label class="col-md-12">
														<input type="checkbox" class="styled" name="">
														ASA 2 Pasien dengan penyakit sistematik ringan
													</label> 
												</li>
												<li>
													<label class="col-md-12">
														<input type="checkbox" class="styled" name="">
														ASA 3 Pasien dengan penyakit sistematik berat
													</label>
												</li>
												<li>
													<label class="col-md-12">
														<input type="checkbox" class="styled" name="">
														ASA 4 Pasien dengan penyakit sistematik sangat berat mengancam nyawa
													</label>
												</li>
												<li>
													<label class="col-md-12">
														<input type="checkbox" class="styled" name="">
														EMERGENCY
													</label>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br><br><br><br>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label class="col-md-12">Perawat Ruangan</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12">Perawat Ok</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="">
										</div>
									</div>
								</div>
							</div>
						</div>
				
						
					</div>
				</div>		
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	