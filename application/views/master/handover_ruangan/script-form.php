<style>
	.table-cus tr th{
		padding:9.5px 25px;
		border:1px solid black;
	}
	.table-cus tr td{
		padding:6.5px 25px 10.5px 25px;
		border:1px solid black;
	}

	.table-cus tr#persiapan_golongan_darah td{
		padding:4px 25px 7.5px 25px;
		border:1px solid black;
	}

	.table-cus tr#lain_lain_keterangan td{
		padding:16px 25px 20px 25px;
		border:1px solid black;
	}
</style>
<script>
	  $(".styled").uniform({
        radioClass: 'choice'
      });

</script>