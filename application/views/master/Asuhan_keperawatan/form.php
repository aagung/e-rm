	<!-- Untuk Radio dan checkbox styled -->
	<span class="switchery-warning switchery-info switchery-danger switchery-primary"></span>
   	<!-- Tutup checkbok dan Radio Styled -->
	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a>
		        </div>
		        
		        <legend class="text-bold" style="margin-top:0px"></legend>
				
				<div class="row">
		            <div class="col-md-12">
		            	<div class="col-md-12">
		            		<div class="row">
				            	<div class="col-md-5 pull-right">
				            		<div class="row">
						            	<div class="panel panel-flat">
						            		<div class="panel-body text-center">
						            			<div class="row">
							            			<label class="control-label col-md-12">ASUHAN KEPERAWATAN PASIEN HEMODIALISA</label>
							            			<label class="control-label col-md-12">(Diisi maksimal 10 menit saat melakukan assessment pre-HD)</label>
						            			</div>
						            		</div>
						            	</div>
				            		</div>
				            	</div>
		            		</div>
		            	</div>

		            	<div class="col-md-12">
		            		<div class="row">
				            	<div class="col-md-7">
				            		<div class="row">
				            			<center>
				            				<label cla>TINDAKAN KEPERAWATAN</label>
				            			</center>
				            		</div>
				            	</div>
		            		</div>
		            	</div>
		            	<br>

		            	<div class="col-md-12">
		            		<div class="row" class="table-responsive" style="overflow-x:auto">
				            	<table class="table table-bordered" style="white-space:nowrap;">
				            		
				            		<!-- Head -->
				            		<?php include 'form/form1.php' ?>

				            		<!-- PRE-HD -->
									<?php include 'form/form2.php' ?>
				            		
									<!-- INTRA-HD -->
									<?php include 'form/form3.php' ?>
				            		
									<!-- POST-HD -->
									<?php include 'form/form4.php' ?>


				            	</table>
		            		</div>
		            	</div>

		            	<div class="col-md-12">
		            		<div class="row">
		            			<label><h5>Terima asih atas kerjasamanya telah mengisi formulir ini dengan benar dan jelas</h5></label>
		            		</div>
		            	</div>
		            </div>
			    </div>
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	
