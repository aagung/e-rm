	<style>
		#verticalCenter {
		vertical-align: middle;
		}
		#verticalTop {
			vertical-align: top;
		}
		 .verticalTableHeader {
		    text-align:center;
		    transform-origin:50% 50%;
		    transform: rotate(-90deg);

		}
		.verticalTableHeader:before {
		    content:'';
		    padding-top:50%;
		    display:inline-block;
		    vertical-align:middle;
		}

   	</style>