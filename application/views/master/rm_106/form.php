<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="row">
					<div class="col-md-4">
						<center>
							<img src="<?php echo base_url('assets/img/rsananda.jpg')?>" width="290px" height="75px">
						</center>
					</div>
					<div class="col-md-4">
						<center>
							SENSUS HARIAN PASIEN RIWAYAT INAP<br>
							(antara jam : 00 s/d 24.00)
							<div class="form-group">
								<label class="control-label col-md-3">Tanggal :</label>
								<div class="col-md-9">
									<input type="date" class="form-control" name="tanggal">
								</div>
							</div>
						</center>
					</div>
					<div class="col-md-4">
						<center>
                            <table border="1">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                        	<center>
                                        		RM . 106 / Rev . 0 / VII / 16
                                        	</center>
                                        </th>
                                    </tr>
                                </thead>
                            </table><br>
                    		<div class="form-group">
								<label class="control-label col-md-4">Ruangan :</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="ruangan">
								</div>
							</div>
                        </center>
					</div>	
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class="col-md-12" colspan="7">PASIEN MASUK</th>
										<th class="col-md-12" colspan="8">PASIEN KELUAR</th>
										<th class="col-md-12" colspan="5">CARA KELUAR</th>
									</tr>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Nomor RM</th>
										<th>Jenis Pelayanan</th>
										<th>Jaminan</th>
										<th>Kelas</th>
										<th>Diagnosa Masuk</th>
										<th>No</th>
										<th>Nama</th>
										<th>Nomor RM</th>
										<th>Jenis Pelayanan</th>
										<th>Jaminan</th>
										<th>Kelas</th>
										<th>Tanggal Masuk</th>
										<th>Diagnosa Masuk</th>
										<th>DIIZINKAN</th>
										<th>DIRUJUK</th>
										<th>PINDAH KE RS LAIN</th>
										<th>PULPAK</th>
										<th>KABUR</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>5</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>6</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>7</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>8</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>9</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>10</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td class="col-md-12" colspan="6">PINDAHAN :</td>
										<td class="col-md-12" colspan="1">Dari Ruangan :</td>
										<td class="col-md-12" colspan="7">DIPINDAHKAN</td>
										<td class="col-md-12" colspan="6">KE RUANGAN</td>
									</tr>
									<tr>
										<td>1</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<td>2</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<td>3</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<td>4</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<td>5</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td colspan="5"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class="col-md-1">No</th>
										<th class="col-md-12">REKAPITULASI</th>
										<th class="col-md-12">JUMLAH</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Pasien Kemarin</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Pasien Masuk</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pasien Pindahan (Dari Ruang Lain)</td>
										<td></td>
									</tr>
									<tr>
										<td><b>4</b></td>
										<td><b>Jumlah (1 + 2 + 3)</b></td>
										<td></td>
									</tr>
									<tr>
										<td>5</td>
										<td>Diizinkan</td>
										<td></td>
									</tr>
									<tr>
										<td>6</td>
										<td>Dieujuk</td>
										<td></td>
									</tr>
									<tr>
										<td>7</td>
										<td>Pindah RS Lain</td>
										<td></td>
									</tr>
									<tr>
										<td>8</td>
										<td>Pulang Paksa</td>
										<td></td>
									</tr>
									<tr>
										<td>9</td>
										<td>Kabur</td>
										<td></td>
									</tr>
									<tr>
										<td><b>10</b></td>
										<td><b>Total Pasien Keluar Hidup (5 + 6 + 7 + 8 +9)</b></td>
										<td></td>
									</tr>
									<tr>
										<td>11</td>
										<td>Dipindahkan (Keruang Lain)</td>
										<td></td>
									</tr>
									<tr>
										<td>12</td>
										<td>Pasien Meninggal < 48 Jam</td>
										<td></td>
									</tr>
									<tr>
										<td>13</td>
										<td>Pasien Meninggal > 48 Jam</td>
										<td></td>
									</tr>
									<tr>
										<td><b>14</b></td>
										<td><b>Jumlah (10 + 11 + 12 + 13 + 14)</b></td>
										<td></td>
									</tr>
									<tr>
										<td>15</td>
										<td>Sisa Pasien Jam 24.00 (4 ~ 14)</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<label class="control-label col-md-12">SenHari / I / RI / VI / 2005</label>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class="col-md-6" colspan="7" rowspan="2">Meninggal</th>
										<th class="col-md-6" colspan="2">KET. MATI</th>
									</tr>
									<tr>
										<th class="col-md-3">< 48 Jam</th>
										<th class="col-md-3">> 48 Jam</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>5</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-12">JENIS PELAYANAN :</label>
							<label class="control-label col-md-1">Pd =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="pd">
							</div>
							<label class="control-label col-md-1">Jw =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="jw">
							</div>
							<label class="control-label col-md-1">Ank =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="ank">
							</div>
							<label class="control-label col-md-1">THT =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="tht">
							</div>
							<label class="control-label col-md-1">Bdh =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="bdh">
							</div>
							<label class="control-label col-md-1">Mt =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="mt">
							</div>
							<label class="control-label col-md-1">Bd O =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="bdo">
							</div>
							<label class="control-label col-md-1">KK =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="kk">
							</div>
							<label class="control-label col-md-1">Bd U =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="bdu">
							</div>
							<label class="control-label col-md-1">GM =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="gm">
							</div>
							<label class="control-label col-md-1">Bd S =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="bds">
							</div>
							<label class="control-label col-md-1">Krd =</label>
								<div class="col-md-5">
							<input type="text" class="form-control" name="krd">
							</div>
							<label class="control-label col-md-1">Obs =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="obs">
							</div>
							<label class="control-label col-md-1">PP =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="pp">
							</div>
							<label class="control-label col-md-1">Gyn =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="gyn">
							</div>
							<label class="control-label col-md-1">Sy =</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="sy">
							</div>
				            <label class="control-label col-md-1 col-md-offset-9">Bekasi , </label>
				            <div class="col-md-2">
	                        	<input type="date" name="tanggal" class="form-control">
	                        </div>
				            <label class="control-label col-md-2 col-md-offset-1">TT. TERSEDIA</label>
				            <label class="control-label col-md-3 col-md-offset-6">Penanggung Jawab Ruangan</label>
	                    	<label class="control-label col-md-1 text-right">(</label>
							<div class="col-md-2">
	                        	<input type="text" name="tt_tersedia" class="form-control">
	                        </div>
	                        <label class="control-label col-md-1">)</label>
	                        <label class="control-label col-md-1 col-md-offset-4 text-right">(</label>
							<div class="col-md-2">
	                        	<input type="text" name="penanggung_jawab_ruangan" class="form-control">
	                        </div>
	                        <label class="control-label col-md-1">)</label>
	                    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="" />
			<input type="hidden" name="option_name" value="" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>