<div class="col-md-6">
	<img src="<?php echo base_url ('assets/img/ananda.png') ?>" style="margin-top: 30px" width="200px"  >
</div>
<div class="col-md-6">
	<div class="panel-body">
		<div class="panel-heading">
			<center>
				<h5><b>FORM PEMERIKSAAN FISIK UMUM</b></h5>
			</center>
		</div>
	</div>
</div>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<?php include "Form_tambah/biodata.php"; ?>
		<div class="panel-body">
			<label><b>A. ANAMNESIS</b></label>
			<div class="panel-boody">

				<?php include "Form_tambah/tambah_A1.php"; ?>
				<!-- A 1.Riwayat penyakit Sebelumnya -->

				<label><b>2. Keluhan Utama Saat Ini </b></label>

				<div class="panel-boody">
					<div class="panel-heading">
						<label><b>B. PEMERIKSAAN FISIK</b></label>
						<div class="panel-body">
							<?php include "Form_tambah/tambah_B1.php"; ?>
							<!-- B 1.Pemeriksaan umum -->
							<?php include "Form_tambah/tambah_B2.php"; ?>
							<!-- B 2.Mata -->
							<?php include "Form_tambah/tambah_B3.php"; ?>
							<!-- B 3.Telinga -->
							<?php include "Form_tambah/tambah_B4.php"; ?>
							<!-- B 4.Hidung -->
							<?php include "Form_tambah/tambah_B5.php"; ?>
							<!-- B 5.Rongga mulut -->
							<?php include "Form_tambah/tambah_B6.php"; ?>
							<!-- B 6.Gigi -->
							<?php include "Form_tambah/tambah_B7.php"; ?>
							<!-- B 7.Leher -->
							<?php include "Form_tambah/tambah_B8.php"; ?>
							<!-- B 8.Thorak -->
							<?php include "Form_tambah/tambah_B9.php"; ?>
							<!-- B 9.Abnomen -->
							<?php include "Form_tambah/tambah_B10.php"; ?>
							<!-- B 10.Ekstremitas atas -->
							<?php include "Form_tambah/tambah_B11.php"; ?>
							<!-- B 11.Ekstremitas bawah -->
							<?php include "Form_tambah/tambah_B12.php"; ?>
							<!-- B 12.Punggung dan tulang belakang -->
							<?php include "Form_tambah/tambah_B12.php"; ?>
							<!-- B 13.Keadaan kulit -->
							<?php include "Form_tambah/tambah_B14.php"; ?>
							<!-- B 14.status kejiwaan -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
		</div>
	</div>
</form>
