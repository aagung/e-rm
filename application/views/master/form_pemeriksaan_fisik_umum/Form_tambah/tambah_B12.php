<div class="col-md-12">
	<div class="control-label"><b>12. Punggung da Tulang Belakang</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Skeliosis</label>
			<label class="radio-inline">
				<input type="radio" name="B12_skeliosis" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B12_skeliosis" class="styled">
				Tidak
			</label>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-4 control-label">b.Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B12_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>