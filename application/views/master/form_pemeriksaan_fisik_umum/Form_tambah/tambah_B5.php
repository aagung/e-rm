<div class="col-md-12">
	<div class="control-label"><b>5. Rongga Mulut</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Labio Palatoschizis</label>
			<label class="radio-inline">
				<input type="radio" name="B5_labio_palatoschizis" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B5_labio_palatoschizis" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Kelainan Faring</label>
			<label class="radio-inline">
				<input type="radio" name="B5_kelainan" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B5_kelainan" class="styled">
				Tidak
			</label>
		</div> 
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Tonsil </label>
			<label class="radio-inline">
				<input type="radio" name="B5_tonsil" class="styled">
				T
			</label>

			<label class="radio-inline">
				<input type="radio" name="B5_tonsil" class="styled">
				T
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">d. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B5_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>