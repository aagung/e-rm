<div class="col-md-12">
	<div class="control-label"><b>8. Thorak</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Simetris</label>
			<label class="radio-inline">
				<input type="radio" name="B8_simetris" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B8_simetris" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Kelainan Paru</label>
			<label class="radio-inline">
				<input type="radio" name="B8_kelainan_paru" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B8_kelainan_paru" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Jika ya,Jelaskan</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B8_kelainan_paru_lbl" value="" >
			</div>
		</div> 
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Kelainan Jantung </label>
			<label class="radio-inline">
				<input type="radio" name="B8_kelainan_jantung" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B8_kelainan_jantung" class="styled">
				Tidak
			</label>
		</div> 
		<div class="form-group">
			<label class="col-md-4 control-label">Jika ya,Jelaskan</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B8_kelainan_jantung_lbl" value="" >
			</div>
		</div>  
		<div class="form-group">
			<label class="col-md-4 control-label">d. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B8_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>