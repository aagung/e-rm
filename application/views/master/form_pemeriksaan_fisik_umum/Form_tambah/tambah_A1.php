<div class="panel-heading">
	<label><b>1. Riwayat Penyakit Sebelumnya</b></label>
	<div class="panel-body">
		<div class="col-md-6">
			<div class="form-group">
				<label class="col-md-12">Apakah Anda Pernah Sakit :</label>
				<label class="col-md-6 control-label ">Asma</label>
				<label class="radio-inline">
					<input type="radio" name="A_asma" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_asma" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Kencing Manis (DM)</label>
				<label class="radio-inline">
					<input type="radio" name="A1_kencing_manis" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_kencing_manis" class="styled">
					Tidak
				</label>
			</div> 
			<div class="form-group">
				<label class="col-md-6 control-label ">Riwayat Kejang</label>
				<label class="radio-inline">
					<input type="radio" name="A1_riwayat_kejang" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_riwayat_kejang" class="styled">
					Tidak
				</label>
			</div> 
			<div class="form-group">
				<label class="col-md-6 control-label ">Penyakit Jantung</label>
				<label class="radio-inline">
					<input type="radio" name="A1_penyakit_jantung" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_penyakit_jantung" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Batuk disertai dahak berdarah</label>
				<label class="radio-inline">
					<input type="radio" name="A1_batuk_disertai_dahak_berdarah" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_batuk_disertai_dahak_berdarah" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Rheumatik</label>
				<label class="radio-inline">
					<input type="radio" name="A1_rheumatik" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_rheumatik" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Tekanan Darah Tinggi</label>
				<label class="radio-inline">
					<input type="radio" name="A1_tekanan_darah_tinggi" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_tekanan_darah_tinggi" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Sering bengkak di wajah/kaki</label>
				<label class="radio-inline">
					<input type="radio" name="A1_sering_bengkak" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_sering_bengkak" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Riwayat Operasi</label>
				<label class="radio-inline">
					<input type="radio" name="A1_riwayat_operasi" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_riwayat_operasi" class="styled">
					Tidak
				</label>
			</div>   
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label  class="col-md-12" type="hidden" style="margin-bottom: 25px"></label>
				<label class="col-md-6 control-label">Alergi/gatal-gatal</label>
				<label class="radio-inline">
					<input type="radio" name="A1_alergi_gatal_gatal" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_alergi_gatal_gatal" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Sakit Kuning/Hepatitis</label>
				<label class="radio-inline">
					<input type="radio" name="A1_sakit_kuning" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_sakit_kuning" class="styled">
					Tidak
				</label>
			</div> 
			<div class="form-group">
				<label class="col-md-6 control-label ">Pernah dirawat</label>
				<label class="radio-inline">
					<input type="radio" name="A1_pernah_dirawat" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_pernah_dirawat" class="styled">
					Tidak
				</label>
			</div> 
			<div class="form-group">
				<label class="col-md-6 control-label ">Patah Tulang</label>
				<label class="radio-inline">
					<input type="radio" name="A1_patah_tulang" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_patah_tulang" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Gangguan Pendengaran</label>
				<label class="radio-inline">
					<input type="radio" name="A1_gangguan_pendengaran" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_gangguan_pendengaran" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Merokok</label>
				<label class="radio-inline">
					<input type="radio" name="A1_merokok" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_merokok" class="styled">
					Tidak
				</label>
			</div>  
			<div class="form-group">
				<label class="col-md-6 control-label ">Riw. Ambeien/Hemoroid</label>
				<label class="radio-inline">
					<input type="radio" name="A1_riw_ambeien" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_riw_ambeien" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Hamil (Wanita)</label>
				<label class="radio-inline">
					<input type="radio" name="A1_hamil" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_hamil" class="styled">
					Tidak
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-6 control-label ">Menstruasi (Wanita)</label>
				<label class="radio-inline">
					<input type="radio" name="A1_menstruasi" class="styled">
					Ya
				</label>

				<label class="radio-inline">
					<input type="radio" name="A1_menstruasi" class="styled">
					Tidak
				</label>
			</div> 
		</div>
	</div>  
	<div class="panel-body" style="margin-left:10px">
		<div class="form-group" style="margin-top: -40px">
			<label class="col-md-2 control-label " >Jika ya,Jenis Operasi</label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="A1_riwayat_operasi_lbl" value="" >
			</div>
			<label class="col-md-1 control-label ">Tahun</label>
			<div class="col-md-2">
				<input type="text" class="form-control" name="A1_tahun" value="" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-7 control-label ">Apakah anda pernah/sekarang menggunakan obat tertentu secara terus-menerus</label>
			<label class="radio-inline">
				<input type="radio" name="A1_menggunakan_obat_tertentu" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="A1_menggunakan_obat_tertentu" class="styled">
				Tidak
			</label>
		</div> 
		<div class="form-group">
			<label class="col-md-2 control-label">Jika ya,Jenis Obat</label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="A1_menggunakan_obat_tertentu_lbl" value="" >
			</div>
		</div>    
	</div>
</div>