<div class="col-md-12">
	<div class="control-label"><b>7. Leher</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Struma</label>
			<label class="radio-inline">
				<input type="radio" name="B7_struma" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B7_struma" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Limafadenopati</label>
			<label class="radio-inline">
				<input type="radio" name="B7_limafadenopati" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B7_limafadenopati" class="styled">
				Tidak
			</label>
		</div> 
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">c. JVP </label>
			<label class="radio-inline">
				<input type="radio" name="B7_jvp" class="styled">
				Normal
			</label>

			<label class="radio-inline">
				<input type="radio" name="B7_jvp" class="styled">
				Abnormal
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">d. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B7_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>