<div class="col-md-12">
	<div class="control-label"><b>9. Abnomen</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Kelainan Bentuk</label>
			<label class="radio-inline">
				<input type="radio" name="B9_kelainan_bentuk" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_kelainan_bentuk" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Bekas Operasi</label>
			<label class="radio-inline">
				<input type="radio" name="B9_bekas_operasi" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_bekas_operasi" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Jika ya,Jelaskan</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B9_bekas_operasi_lbl" value="" >
			</div>
		</div>  
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Hepatomegali </label>
			<label class="radio-inline">
				<input type="radio" name="B9_hepatomegali" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_hepatomegali" class="styled">
				Tidak
			</label>
		</div> 
		<div class="form-group">
			<label class="col-md-6 control-label ">d. Spelenomegali </label>
			<label class="radio-inline">
				<input type="radio" name="B9_splenomegali" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_splenomegali" class="styled">
				Tidak
			</label>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">e. Nyeri Tekan Epigastrium </label>
			<label class="radio-inline">
				<input type="radio" name="B9_nyeri_tekan_epigastrium" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_nyeri_tekan_epigastrium" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">f. Nyeri teka titik MC Burney </label>
			<label class="radio-inline">
				<input type="radio" name="B9_nyeri_teka_titik_mc_burney" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_nyeri_teka_titik_mc_burney" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">g. Striae </label>
			<label class="radio-inline">
				<input type="radio" name="B9_striae" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_striae" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">h. Teraba Tumor </label>
			<label class="radio-inline">
				<input type="radio" name="B9_teraba_tumor" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B9_teraba_tumor" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">i. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B9_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>