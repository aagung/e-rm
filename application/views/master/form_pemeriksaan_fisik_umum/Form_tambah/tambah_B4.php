<div class="col-md-12">
	<div class="control-label"><b>4. Hidung</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Septum Deviasi</label>
			<label class="radio-inline">
				<input type="radio" name="B4_septum_deviasi" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B4_septum_deviasi" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Sekret</label>
			<label class="radio-inline">
				<input type="radio" name="B4_sekret" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B4_sekret" class="styled">
				Tidak
			</label>
		</div> 
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-4 control-label">c. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B4_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>