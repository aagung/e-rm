<div class="col-md-12">
	<div class="control-label"><b>10. Ektremitas Atas</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Kelainan Bentuk</label>
			<label class="radio-inline">
				<input type="radio" name="B10_kelainan_bentuk" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B10_kelainan_bentuk" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Jari Tabuh/Drum's Stick</label>
			<label class="radio-inline">
				<input type="radio" name="B10_jari_tabuh" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B10_jari_tabuh" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Polidactili/Sindactili </label>
			<label class="radio-inline">
				<input type="radio" name="B10_polidactili" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B10_polidactili" class="styled">
				Tidak
			</label>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">d. Bekas Luka Sayatan </label>
			<label class="radio-inline">
				<input type="radio" name="B10_beka_luka_sayatan" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B10_beka_luka_sayatan" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">e. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B10_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>