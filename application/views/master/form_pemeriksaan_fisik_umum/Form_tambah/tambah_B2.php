<div class="col-md-12">
	<div class="control-label"><b>2. Mata</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Buta Warna</label>
			<label class="radio-inline">
				<input type="radio" name="B2_buta_warna" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B2_buta_warna" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Kacamata</label>
			<label class="radio-inline">
				<input type="radio" name="B2_kacamata" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B2_kacamata" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Jika ya,Ukuran</label>
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon">OD :</span>
				<input type="text" name="kacamata_ukuran_od" class="form-control" name="B2_kacamata_lbl" value="" >
			</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon">OS :</span>
				<input type="text" name="kacamata_ukuran_os" class="form-control" name="B2_kacamata_lbl" value="" >
			</div>
			</div>
		</div>  
		<div class="form-group">
			<label class="col-md-4 control-label">c. Pemeriksaan Virus</label>
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon">OD :</span>
				<input type="text" name="pemeriksaan_virus_od" class="form-control" name="B2_pemeriksaan_visus" value="" >
			</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon">OS :</span>
				<input type="text" name="pemeriksaan_virus_os" class="form-control" name="B2_kacamata_lbl" value="" >
			</div>
			</div>
		</div>  
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">d. Strabismus</label>
			<label class="radio-inline">
				<input type="radio" name="B2_strabismus" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B2_strabismus" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">e. Konjungtiva Anemis</label>
			<label class="radio-inline">
				<input type="radio" name="B2_konjungtiva_anemis" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B2_konjungtiva_anemis" class="styled">
				Tidak
			</label>
		</div> 
		<div class="form-group">
			<label class="col-md-6 control-label ">f. Sklera Ikterik</label>
			<label class="radio-inline">
				<input type="radio" name="B2_sklera_ikterik" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B2_sklera_ikterik" class="styled">
				Tidak
			</label>
		</div>  
		<div class="form-group">
			<label class="col-md-4 control-label">g. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B2_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>