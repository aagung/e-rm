<div class="col-md-12">
	<div class="control-label"><b>3. Telinga</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Gangguan Pendengaran</label>
			<label class="radio-inline">
				<input type="radio" name="B3_gangguan_pendengaran" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B3_gangguan_pendengaran" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Kelainan Bentuk</label>
			<label class="radio-inline">
				<input type="radio" name="B3_kelainan_bentuk" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B3_kelainan_bentuk" class="styled">
				Tidak
			</label>
		</div> 
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Perforasi Membran Timpani</label>
			<label class="radio-inline">
				<input type="radio" name="B3_perforasi_membran_timpani" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B3_perforasi_membran_timpani" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">d. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B3_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>