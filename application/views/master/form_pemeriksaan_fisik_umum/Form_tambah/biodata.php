<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Nama</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="nama" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Umur</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="umur" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required ">Jenis Kelamin</label>
						<label class="radio-inline" style="margin-left: 10px">
							<input type="radio" name="jenis_kelamin" class="styled">
							Laki-laki
						</label>
						<label class="radio-inline">
							<input type="radio" name="jenis_kelamin" class="styled">
							Perempuan
						</label>
					</div>  
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Alamat</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="alamat" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">No. Reg</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="no_reg" value="" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label input-required">Departemen</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="departemen" value="" >
						</div>
					</div>
				</div>
				<!-- data pasien -->			
			</div>
		</div>