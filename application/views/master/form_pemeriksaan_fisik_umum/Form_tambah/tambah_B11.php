
<div class="col-md-12">
	<div class="control-label"><b>11. Ektremitas Atas</b></div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">a. Kelainan Bentuk</label>
			<label class="radio-inline">
				<input type="radio" name="B11_kelainan_bentuk" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B11_kelainan_bentuk" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">b. Varises</label>
			<label class="radio-inline">
				<input type="radio" name="B11_varises" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B11_varises" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">c. Polio </label>
			<label class="radio-inline">
				<input type="radio" name="B11_polio" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B11_polio" class="styled">
				Tidak
			</label>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="col-md-6 control-label ">d. Hemiparesis  </label>
			<label class="radio-inline">
				<input type="radio" name="B11_hemiparesis" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B11_hemiparesis" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-6 control-label ">e. Pembengkakan Sendi </label>
			<label class="radio-inline">
				<input type="radio" name="B11_pembengkakan_sendi" class="styled">
				Ya
			</label>

			<label class="radio-inline">
				<input type="radio" name="B11_pembengkakan_sendi" class="styled">
				Tidak
			</label>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">f. Lain-lain</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="B11_lain_lain_lbl" value="" >
			</div>
		</div>  
	</div>
</div>