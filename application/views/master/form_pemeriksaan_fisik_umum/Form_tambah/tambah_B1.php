<div class="col-md-12">
	<div class="control-label"><b>1. Pemeriksaan Umum</b></div>
	<div class="table-responsive">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td><label class="col-md-6 control-label">a.Tekanan Darah</label>
						<div class="col-md-6">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_tekanan_darah" value="" >
								<span class="input-group-addon">mmHg</span>
							</div>
						</div>
					</td>
					<td><label class="col-md-5 control-label">c.Tinggi Badan</label>
						<div class="col-md-7">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_tinggi_badan" value="">
								<span class="input-group-addon">x/menit</span>
							</div>
						</div>
					</td>
					<td><label class="col-md-3 control-label">e.Suhu</label>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_suhu" value="">
								<span class="input-group-addon">Febrish/Afebris</span>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td><label class="col-md-5 control-label">b.Denyut Nadi</label>
						<div class="col-md-7">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_denyut_nadi" value="" >
								<span class="input-group-addon">x/menit</span>
							</div>
						</div>
					</td>
					<td><label class="col-md-5 control-label">d.Berat Badan</label>
						<div class="col-md-7">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_berat_badan" value="">
								<span class="input-group-addon">Kg</span>
							</div>
						</div>
					</td>
					<td><label class="col-md-4 control-label">f.Pernafasan</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" class="form-control" name="B1_pernafasan" value="" >
								<span class="input-group-addon">x/menit</span>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>