<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<center>
						<img src="<?php echo base_url('assets/img/rsananda.jpg')?>" width="250px" height="80px">
						<b><br>Jl. Sultan Agung No. 173 Bekasi 17133<br>Telp : (021)8854338<br>Fax : (021)88950141 - 88855526</b>
					</center>
				</div>
				<div class="col-md-6">
					<center>
                        <table border="1">
                            <thead>
                                <tr>
                                    <th colspan="2" height="70px">
                                    	<center>
                                    		<b>Formulir Perencanaan Pemulangan Pasien</b>
                                    	</center>
                                    </th>
                                </tr>
                                <tr>
                                    <th height="70px">
                                    	<center>
                                    		<b>RM-100 / Rev.0 / VI / 2016</b>
                                    	</center>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </center>
				</div>
				<div class="col-md-12">
					<br>
					<center>
						<b>FORMULIR PERENCANAAN PEMULANGAN PASIEN</b>
					</center>
					<br>
				</div>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-4">No. Medical Record :</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="no_medical_record">
											</div>
											<label class="control-label col-md-4">Nama Pasien :</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="nama_pasien">
											</div>
											<label class="control-label col-md-4">Tanggal Lahir / Usia :</label>
											<div class="col-md-4">
												<input type="date" class="form-control" name="tanggal_lahir">
											</div>
											<div class="col-md-4">
												<input type="text" class="form-control" name="usia">
											</div>
											<label class="control-label col-md-4">Diagnosa :</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="diagnosa">
											</div>
										</div>
									</th>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-6">Ruang / Kelas :</label>
											<div class="col-md-6">
												<input type="text" class="form-control" name="ruang_kelas">
											</div>
											<label class="control-label col-md-6">Tanggal Masuk :</label>
											<div class="col-md-6">
												<input type="date" class="form-control" name="tanggak_masuk">
											</div>
											<label class="control-label col-md-6">Rencana Tanggal Pulang :</label>
											<div class="col-md-6">
												<input type="date" class="form-control" name="rencana_tanggal_pulang">
											</div>
											<label class="control-label col-md-6">Rencana Jam Pulang :</label>
											<div class="col-md-6">
												<input type="time" class="form-control" name="rencana_jam_pulang">
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">Usia Lanjut (60 tahun atau lebih)</th>
									<th class="col-md-3" colspan="3">
										<div class="form-group">
											<div class="col-md-6">
												<label class="radio-inline">
													<input type="radio" class="styled" name="usia_lanjut" value="ya">Ya
												</label>
											</div>
			                            	<div class="col-md-6">
			                            		<label class="radio-inline">
													<input type="radio" class="styled" name="usia_lanjut" value="tidak">Tidak
												</label>
			                            	</div>
			                            </div>
									</th>
									<th class="col-md-3" colspan="3" rowspan="4">Jika satu saja terpenuhi, berarti pasien membutuhkan perencanaan pulang khusus.</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">Hambatan Mobilisasi</th>
									<th class="col-md-3" colspan="3">
										<div class="form-group">
											<div class="col-md-6">
												<label class="radio-inline">
													<input type="radio" class="styled" name="hambatan_mobilisasi" value="ya">Ya
												</label>
											</div>
			                            	<div class="col-md-6">
			                            		<label class="radio-inline">
													<input type="radio" class="styled" name="hambatan_mobilisasi" value="tidak">Tidak
												</label>
			                            	</div>
			                            </div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">Membutuhkan pelayanan medis dan perawatan berkelanjutan</th>
									<th class="col-md-3" colspan="3">
										<div class="form-group">
											<div class="col-md-6">
												<label class="radio-inline">
													<input type="radio" class="styled" name="pelayanan_medis_perawatan_berkelanjutan" value="ya">Ya
												</label>
											</div>
			                            	<div class="col-md-6">
			                            		<label class="radio-inline">
													<input type="radio" class="styled" name="pelayanan_medis_perawatan_berkelanjutan" value="tidak">Tidak
												</label>
			                            	</div>
			                            </div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">Tergantung dengan orang lain dalam aktifitas harian</th>
									<th class="col-md-3" colspan="3">
										<div class="form-group">
											<div class="col-md-6">
												<label class="radio-inline">
													<input type="radio" class="styled" name="orang_lain_aktifitas_harian" value="ya">Ya
												</label>
											</div>
			                            	<div class="col-md-6">
			                            		<label class="radio-inline">
													<input type="radio" class="styled" name="orang_lain_aktifitas_harian" value="tidak">Tidak
												</label>
			                            	</div>
			                            </div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Transportasi Pulang :</b></label>
											<div class="col-md-12">
												<input type="text" class="form-control" name="transportasi_pulang">
											</div>
										</div>
									</th>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Orang yang mendampingi & merawat pasien dirumah :</b></label>
											<div class="col-md-12">
												<input type="text" class="form-control" name="orang_mendampingi_dirumah">
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-12" colspan="12">
										<div class="form-group">
											<label class="control-label col-md-4"><b>Pengobatan yang dilanjutkan dirumah :</b></label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="pengobatan_dilanjutkan_dirumah">
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-1" colspan="1" rowspan="2"><b>NO</b></th>
									<th class="col-md-3" colspan="3" rowspan="2"><b>NAMA OBAT</b></th>
									<th class="col-md-1" colspan="1" rowspan="2"><b>JUMLAH</b></th>
									<th class="col-md-2" colspan="2" rowspan="2"><b>ATURAN PAKAI</b></th>
									<th class="col-md-1" colspan="1"><b>PAGI</b></th>
									<th class="col-md-1" colspan="1"><b>SIANG</b></th>
									<th class="col-md-1" colspan="1"><b>SORE</b></th>
									<th class="col-md-1" colspan="1"><b>MALAM</b></th>
									<th class="col-md-2" colspan="2" rowspan="2"><b>INTRUKSI KHUSUS</b></th>
								</tr>
								<tr>
									<th class="col-md-4" colspan="4"><b>JAM PEMBERIAN</b></th>
								</tr>
								<tr>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-3" colspan="3"></th>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-2" colspan="2"></th>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-1" colspan="1"></th>
									<th class="col-md-2" colspan="2"></th>
								</tr>
								<tr>
									<th class="col-md-12" colspan="12">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Diet Khusus Pasien :</b></label>
											<div class="col-md-12">
			                                	<textarea class="form-control" name="diet_khusus_pasien" rows="2" cols="4"></textarea>
			                                </div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Perawatan / Peralatan Medis yang dilanjutkan dirumah :</b></label>
											<label class="control-label col-md-12"><b>NAMA ALAT MEDIS</b></label>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="oxygen_portable" value="ya">Oxygen Portable
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="tracheostomi" value="ya">Tracheostomi
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="d_kateter" value="ya">D-Kateter
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="ngt" value="ya">NGT / Nasa Gastroc Tube
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-1">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="1" value="ya">
													</label>
			                               		</div>
			                               		<div class="col-md-11">
													<input type="text" class="form-control" name="1">
												</div>
											</div>
										</div>
									</th>
									<th class="col-md-6" colspan="6">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Alat bantu yang dipakai dirumah :</b></label>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="kursi_roda" value="ya">Kursi Roda
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="tongkat" value="ya">Tongkat
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-1">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="2" value="ya">
													</label>
			                               		</div>
			                               		<div class="col-md-11">
													<input type="text" class="form-control" name="2">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-1">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="3" value="ya">
													</label>
			                               		</div>
			                               		<div class="col-md-11">
													<input type="text" class="form-control" name="3">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-1">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="4" value="ya">
													</label>
			                               		</div>
			                               		<div class="col-md-11">
													<input type="text" class="form-control" name="4">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-1">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="5" value="ya">
													</label>
			                               		</div>
			                               		<div class="col-md-11">
													<input type="text" class="form-control" name="5">
												</div>
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-12" colspan="12">
										<div class="form-group">
											<label class="control-label col-md-12"><b>Pendidikan kesehatan untuk dirumah :</b></label>
											<div class="col-md-6">
												<div class="form-group">
													<div class="col-md-6">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="balutan_jangan_basah_kotor" value="ya">Balutan  jangan basah / kotor
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="hindari_mengangkat_beban_berat" value="ya">Hindari Mengangkat Beban Berat
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="jangan_menaiki_tangga" value="ya">Jangan menaiki tangga lebih dari dua atau tiga kali sehari
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="jangan_menyupir_sendiri" value="ya">Jangan mengendarai kendaraan sendiri / menyupir
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="hindari_mengangkat_beban_berat" value="ya">Cek laboratorium sebelum kontrol
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-1">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="6" value="ya">
														</label>
				                               		</div>
				                               		<div class="col-md-11">
														<input type="text" class="form-control" name="6">
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-1">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="7" value="ya">
														</label>
				                               		</div>
				                               		<div class="col-md-11">
														<input type="text" class="form-control" name="7">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="batasi_pekerjaan_rumah_tangga" value="ya">Batasi pekerjaan rumah tangga dan kegiatan sosial. Melakukan aktifitas secara bertahap sampai kesehatan pulih kembali
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="kegawatdaruratan" value="ya">Jika muncul kegawatdaruratan atau keadaan menjadi lebih buruk, segera datang ke RS.Ananda No. Telp :
															<input type="text" class="form-control" name="11">
														</label>
				                               		</div>
												</div>
												<div class="form-group">
													<div class="col-md-1">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="8" value="ya">
														</label>
				                               		</div>
				                               		<div class="col-md-11">
														<input type="text" class="form-control" name="8">
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-1">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="9" value="ya">
														</label>
				                               		</div>
				                               		<div class="col-md-11">
														<input type="text" class="form-control" name="9">
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-1">
				                                   		<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="10" value="ya">
														</label>
				                               		</div>
				                               		<div class="col-md-11">
														<input type="text" class="form-control" name="10">
													</div>
												</div>
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-6" colspan="6" rowspan="2"><b>Diberikan kepada pasien / keluarga :</b>
										<div class="form-group">
											<div class="col-md-12">
		                                   		<label class="checkbox-inline">
													<input type="checkbox" class="styled" name="obat_obatan" value="ya">Obat-obatan
												</label>
		                               		</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
		                                   		<label class="checkbox-inline">
													<input type="checkbox" class="styled" name="peralatan_barang_pribadi" value="ya">Peralatan / barang pribadi
												</label>
		                               		</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
		                                   		<label class="checkbox-inline">
													<input type="checkbox" class="styled" name="resep_obat" value="ya">Resep Obat
												</label>
		                               		</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
		                                   		<label class="checkbox-inline">
													<input type="checkbox" class="styled" name="hasil_pemeriksaan_penunjang" value="ya">Hasil pemeriksaan penunjang
												</label>
		                               		</div>
										</div>
										<div class="form-group">
		                               		<label class="control-label col-md-1 col-md-offset-1">1.</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="12">
											</div>
										</div>
										<div class="form-group">
		                               		<label class="control-label col-md-1 col-md-offset-1">2.</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="13">
											</div>
										</div>
										<div class="form-group">
		                               		<label class="control-label col-md-1 col-md-offset-1">3.</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="14">
											</div>
										</div>
										<div class="form-group">
		                               		<label class="control-label col-md-1 col-md-offset-1">4.</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="15">
											</div>
										</div>
									</th>
									<th class="col-md-1" colspan="6"><b>Jadwal kontrol berikutnya :</b>
										<div class="form-group">
		                               		<label class="control-label col-md-4">Nama Dokter</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="nama_dokter">
											</div>
										</div>
										<div class="form-group">
		                               		<label class="control-label col-md-4">Tanggal dan Jam Praktek</label>
											<div class="col-md-4">
												<input type="date" class="form-control" name="15">
											</div>
											<div class="col-md-4">
												<input type="time" class="form-control" name="15">
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th class="col-md-4" colspan="6">
										<label class="col-md-12"><b>Intruksi diberikan kepada :</b></label>
										<div class="col-md-6">
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="pasien" value="ya">Pasien
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="orang_terdekat" value="ya">Orang Terdekat
													</label>
			                               		</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="lain_lain" value="ya">Lain-lain
														<input type="text" class="form-control" name="lain_lain">
													</label>
			                               		</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="col-md-6">
			                                   		<label class="checkbox-inline">
														<input type="checkbox" class="styled" name="keluarga" value="ya">Keluarga
													</label>
			                               		</div>
											</div>
										</div>
									</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="form-group">
	            <label class="control-label col-md-12">Saya dapat menerima dan mengerti tentang intruksi pulang</label>
	            <label class="control-label col-md-3 col-md-offset-1"><b>PERAWAT YANG MENJELASKAN</b></label>
	            <label class="control-label col-md-2 col-md-offset-1 text-center"><b>PETUGAS FARMASI</b></label>
	            <label class="control-label col-md-3 col-md-offset-2"><b>PIHAK PASIEN</b></label>
	        </div><br><br><br>
	        <div class="form-group">
	        	<label class="control-label col-md-1 text-right">(</label>
				<div class="col-md-2">
	            	<input type="text" name="pasien_penanggung_jawab" class="form-control">
	            </div>
	            <label class="control-label col-md-1">)</label>
	            <label class="control-label col-md-1 text-right">(</label>
				<div class="col-md-2">
	            	<input type="text" name="keluarga" class="form-control">
	            </div>
	            <label class="control-label col-md-1">)</label>
	            <label class="control-label col-md-1 text-right">(</label>
				<div class="col-md-2">
	            	<input type="text" name="pihak_rumah_sakit" class="form-control">
	            </div>
	            <label class="control-label col-md-1">)</label>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-md-3 col-md-offset-1">Nama dan Tanda Tangan</label>
	            <label class="control-label col-md-2 col-md-offset-1 text-center">Nama dan Tanda Tangan</label>
	            <label class="control-label col-md-3 col-md-offset-2">Nama dan Tanda Tangan</label>
	        </div>
	        <div class="form-group">
				<label class="control-label col-md-3 col-md-offset-7">* Hubungan dengan pasien :</label>
				<div class="col-md-2">
					<input type="text" class="form-control" name="nama">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-md-offset-7">No Telepon :</label>
				<div class="col-md-2">
					<input type="text" class="form-control" name="umur">
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="" />
			<input type="hidden" name="option_name" value="" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>

<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div  class="col-md-12">
					<center>
						<img src="<?php echo base_url('assets/img/rsananda.jpg')?>" width="1000px" height="200px">
					</center>
				</div>
				<div class="col-md-6">
					
				</div>
				<div class="col-md-6">
					<center>
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>
                                    	<b>RM.74 / Rev.0 / VI / 16</b>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </center>
				</div>
				<div class="col-md-12">
					<br>
					<center>
						<b>SURAT IZIN PULANG</b>
					</center>
					<br>
				</div>
			</div>
			<div class="form-group">
	            <label class="control-label col-md-12"><b>Yang bertanda tangan di bawah ini menerangkan bahwa Tn. / Ny. / Nn. :</b></label>
	            <label class="control-label col-md-1"><b>Nama :</b></label>
				<div class="col-md-11">
					<input type="text" class="form-control" name="nama">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-1"><b>Umur :</b></label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="umur">
				</div>
				<label class="control-label col-md-1"><b>L / P</b></label>
			</div>		
			<div class="form-group">
				<label class="control-label col-md-1"><b>No RMK :</b></label>
				<div class="col-md-11">
					<input type="text" class="form-control" name="no_rmk">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><b>Dirawat di RS Ananda ruang / kelas :</b></label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="dirawat">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4"><b>Diizinkan untuk pulang pada hari / tanggal :</b></label>
				<div class="col-md-4">
					<input type="text" class="form-control" name="diizinkan_pulang">
				</div>
				<div class="col-md-4">
					<input type="date" class="form-control" name="diizinkan_pulang">
				</div>
				<label class="control-label col-md-12"><b>Demikian keterangan ini, agar dapat dipergunakan sebagaimana mestinya.</b></label>
			</div>
			<div class="form-group">
				<label class="control-label col-md-1 col-md-offset-9"><b>Bekasi , </b></label>
	            <div class="col-md-2">
                	<input type="date" name="tanggal" class="form-control">
                </div>
                <label class="control-label col-md-1 col-md-offset-8 text-right">(</label>
				<div class="col-md-2">
                	<input type="text" name="penanggung_jawab_ruangan" class="form-control">
                </div>
                <label class="control-label col-md-1">)</label>
                <label class="control-label col-md-3 col-md-offset-9"><b><i>Dokter yang merawat</i></b></label>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="" />
			<input type="hidden" name="option_name" value="" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" name="option_table" value="" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>