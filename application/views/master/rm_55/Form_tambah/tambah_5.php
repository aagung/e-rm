<div class="form-group">
<label class="control-label col-md-12">5. Riwayat Psikososial : (Dikaji saat kunjungan pertama atau kunjungan terakhir > 1 tahun)</label>
<div class="panel-body">
</div>
<div class="table-responsive">
<table class="table table-xxs table-hover">
<tr>
<th>
<label class="col-md-9 	control-label"><li>Adakah keyakinan/tradisi/budaya yang berkaitan dengan pelayanan kesehatan yang akan di berikan </li></label>
<div class="col-md-1">
	<div class="radio no-padding-top">
		<label class="radio">
			<input class="styled" name="adakah_keyakinan" type="radio" value="">
			Tidak
		</label>
	</div>
</div>
<div class="col-md-2">
	<div class="radio no-padding-top">
		<label class="radio">
			<input class="styled" name="adakah_keyakinan" type="radio" value="">
			Ya
		</label>
	</div>
</div></th>
</tr>
<tr>
<th><label class="col-md-3 	control-label"><li>Kendala komunikasi</li></label>
	<div class="col-md-2">
		<div class="radio no-padding-top">
			<label class="radio">
				<input class="styled kendala" type="radio" name="kendala_komunikasi" value="1">
				Tidak ada
			</label>
		</div>
	</div>
	<div class="col-md-6">
		<div class="radio no-padding-top">
			<input type="radio" class="styled kendala" name="kendala_komunikasi" value="2"/>
			<label class="control-label col-md-3">Ada,jelaskan</label>
			<div class="col-md-9" id="kendala_ya" style="display:none">
				<input name="input" class="form-control input-xs" type="text"/>
			</div>
		</div>
	</div></th>
</tr>
<tr>
	<th><label class="col-md-3 	control-label"><li>Yang merawat di rumah</li></label>
		<div class="col-md-2">
			<div class="radio no-padding-top">
				<label class="radio">
					<input class="styled merawat" type="radio" name="merawat" value="1">
					Tidak ada
				</label>
			</div>
		</div>
		<div class="col-md-6">
			<div class="radio no-padding-top">
				<input type="radio" class="styled merawat" name="merawat" value="2"/>
				<label class="control-label col-md-3">Ada,jelaskan</label>
				<div class="col-md-9" id="merawat_ya" style="display:none">
					<input name="input" class="form-control input-xs" type="text"/>
				</div>
			</div>
		</div></th>
	</tr>
	<tr>
		<th><label class="col-md-2 	control-label"><li>Kondisi saat ini </li></label>
			<label class="radio-inline">
				<input type="radio" name="kondisi_saat_ini" class="styled">
				Tenang
			</label>

			<label class="radio-inline">
				<input type="radio" name="kondisi_saat_ini" class="styled">
				Gelisah
			</label>
			<label class="radio-inline">
				<input type="radio" name="kondisi_saat_ini" class="styled">
				Takut terhadap tindakan
			</label>
			<label class="radio-inline">
				<input type="radio" name="kondisi_saat_ini" class="styled">
				Marah
			</label>
			<label class="radio-inline">
				<input type="radio" name="kondisi_saat_ini" class="styled">
				Mudah tersinggung
			</label>
		</th>
	</tr>
</table>
</div>
</div>
<div class="form-group">
<label class="control-label col-md-4"><b><u>DIAGNOSA KEPERAWATAN DX</u></b></label>
<div class="col-lg-8">
<input type="text" name="diagnosa_keperawatan_dx" class="form-control">
</div>
</div>

<div class="form-group">
<div class="col-md-4">
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="kelebihan_volume_cairan" type="checkbox"value="">
		<label class="col-md-12">1. Kelebihan volume cairan</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="gangguan_pertukaran_gas" type="checkbox"value="">
		<label class="col-md-12">2. Gangguan pertukaran gas</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="gangguan_keseimbangan_lektrolit" type="checkbox"value="">
		<label class="col-md-12">3. Gangguan keseimbangan elektrolit</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="penurunan_curah_jantung" type="checkbox"value="">
		<label class="col-md-12">4. Penurunan curah jantung</label>
	</div>
</div>
</div>
<div class="col-md-8">
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="nutrisi_kurang_dari_kebutuhan_tubuh" type="checkbox"value="">
		<label class="col-md-12">5. Nutrisi kurang dari kebutuhan tubuh</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="ketidakpatuhan_terhadap_diit" type="checkbox"value="">
		<label class="col-md-12">6. ketidakpatuhan terhadap diit</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox no-padding-top">
		<input class="styled" name="gangguan_keseimbangan" type="checkbox" onchange="diagnosa_keseimbangan(this.checked)" value="">
		<label class="control-label col-md-6">7. Gangguan keseimbangan asam basa :</label>
		<div class="col-md-6">
			<input type="text" name="gangguan_keseimbangan_lbl" id="hiddenkeseimbangan" style="display:none" class="form-control  input-xs">
		</div>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="gangguan_rasa_nyaman" type="checkbox"value="">
		<label class="col-md-12">8. Gangguan rasa nyaman : nyeri</label>
	</div>
</div>
</div>
<div class="col-md-12">
<div class="checkbox no-padding">
	<input class="styled" name="diagnosa_null" type="checkbox"value="">
	<label class="control-label col-md-1"></label>
	<div class="col-md-11">
		<input type="text" name="diagnosa_null_lbl" class="form-control" name="">
	</div>
</div>
</div>
</div>
<div class="form-group">
<label class="control-label col-md-12"><b><u>INTERVENSI KEPERAWATAN (rekapit ulasi pre-intra dan post-HD) :</u></b></label>
</div>
<div class="form-group">
<div class="col-md-6">
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="monitor_berat_badan" type="checkbox"value="">
		<label class="col-md-12">Monitor berat badan,intake out put</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="berikan_terapi_oksigen" type="checkbox"value="">
		<label class="col-md-12">Berikan terapi oksigen sesuai kebutuhan</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="monitor_tanda_dan_gejala_infeksi" type="checkbox"value="">
		<label class="col-md-12">Monitor tanda dan gejala infeksi produser</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="monitor_tanda_dan_gejala_hipoglikemi" type="checkbox"value="">
		<label class="col-md-12">Monitor tanda dan gejala hipoglikemi</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="hentikan_hd" type="checkbox"value="">
		<label class="col-md-12">Hentikan HD sesuai indikasi</label>
	</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="atur_posisi_pasien" type="checkbox"value="">
		<label class="col-md-12">Atur posisi pasien agar ventilasi adekuat</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="observasi_pasien" type="checkbox"value="">
		<label class="col-md-12">Observasi pasien (monitor vital sign) dan mesin</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="ganti_balutan_luka" type="checkbox"value="">
		<label class="col-md-12">Ganti balutan luka sesuai dengan produser</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="lakukan_teknik_distraksi" type="checkbox"value="">
		<label class="col-md-12">Lakukan teknik distraksi, relaksasi</label>
	</div>
</div>
</div>
<div class="col-md-12">
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="kaji_kemampuan_pasien_mendapatkan_nutrisi" type="checkbox"value="">
		<label class="col-md-12">Kaji kemampuan pasien mendapatkan nutrisi yang dibutuhkan</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="posisikan_supinasi_dengan_elevasi" type="checkbox"value="">
		<label class="col-md-12">Posisikan supinasi dengan elevasi kepala 30&deg; dan elevasi kaki</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox">
		<input class="styled" name="bila_pasien_mulai_hipotensi" type="checkbox"value="">
		<label class="col-md-12">Bila pasien mulai hipotensi (mulai, muntah, keringat dingin, pusing),kram, hipoglikemi berikan cairan sesuai SPO)</label>
	</div>
</div>
<div class="form-group">
	<div class="checkbox no-padding">
		<input class="styled" name="penkes" type="checkbox"value="">
		<label class="control-label col-md-3">PENKES : diit, AV-Shunt</label>
		<div class="col-md-9">
			<input type="text" name="penkes_lbl" class="form-control" name="">
		</div>
	</div>
</div>

<div class="checkbox no-padding">
	<input class="styled" name="intervensi_null" type="checkbox"value="">
	<label class="control-label col-md-1"></label>
	<div class="col-md-11">
		<input type="text" name="intervensi_null_lbl" class="form-control" name="">
	</div>
</div>
</div>

</div>
<div class="form-group">
<div class="col-md-12">
<label class="display-block text-semibold">Intervensi Koloborasi</label>
<label class="checkbox-inline">
	<input type="checkbox" name="program_hd" class="styled">
	Program HD
</label>

<label class="checkbox-inline">
	<input type="checkbox" name="transfusi_darah" class="styled">
	Tranfusi darah
</label>
<label class="checkbox-inline">
	<input type="checkbox" name="koloborasi_diit" class="styled">
	Koloborasi diit
</label>
<label class="checkbox-inline">
	<input type="checkbox" name="pemberian_ca_glukonas" class="styled">
	Pemberiaan Ca Glukonas
</label>
<label class="checkbox-inline">
	<input type="checkbox" name="pemberian_antipiretik" class="styled">
	Pemberian Antipiretik
</label>
<label class="checkbox-inline">
	<input type="checkbox" name="analgetik" class="styled">
	Analgetik
</label>
</div>
</div>
<div class="form-group">
<label class="control-label col-md-12"><u><b>INSTRUKSI MEDIK :</b></u></label>
</div>
<div class="form-group" style="margin-left: 105px">
<label class="checkbox-inline">
<input type="checkbox" name="inisiasi" class="styled">
Inisiasi
</label>

<label class="checkbox-inline">
<input type="checkbox"  name="akut" class="styled">
Akut
</label>
<label class="checkbox-inline">
<input type="checkbox" name="rutin" class="styled">
Rutin
</label>
<label class="checkbox-inline">
<input type="checkbox" name="pre_op" class="styled">
Pre-Op
</label>
<label class="checkbox-inline">
<input type="checkbox" name="sled" class="styled">
SLED
</label>
<label class="checkbox-inline" style="margin-top: 12px">
<input type="checkbox" name="intervensi_koloborasi_null" class="styled">
<input type="text" name="intervensi_koloborasi_null_lbl" class="form-control input-xs" name="" style="width: 100px">
</label>
<label class="form-label">Dialisat :</label>
<label class="checkbox-inline">
<input type="checkbox" name="asetat" class="styled">
Asetat
</label>
<label class="checkbox-inline">
<input type="checkbox" name="bicarbonat" class="styled">
Bicarbonat
</label>
</div>
<div class="box col-md-2" style="width: 95px">
Resep HD :
</div>
<div class="form-group" style="margin-top: 5px">
<div class="col-md-4">
<div class="row">
	<label class="col-md-2 control-label">TD
	</label>
	<div class="col-md-4">
		<div class="input-group">
			<input type="text" name="td_slash" class="form-control input-xs">
			<span class="input-group-addon">/</span>
		</div>
	</div>
	<div class="col-md-6">
		<div class="input-group">
			<input type="text" name="td_mmhg" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">mmHg</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-3">
<div class="row">
	<label class="col-md-3 control-label">QB
	</label>
	<div class="col-md-9">
		<div class="input-group">
			<input type="text" name="qb" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">ml/mnt</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-3">
<div class="row">
	<label class="col-md-3 control-label">QD
	</label>
	<div class="col-md-9">
		<div class="input-group">
			<input type="text" name="qd" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">ml/mnt</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-4">
<div class="row">
	<label class="col-md-4 control-label">UF Goal
	</label>
	<div class="col-md-8">
		<div class="input-group">
			<input type="text" name="uf_goal" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">ml</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-4">
<div class="checkbox no-padding">
	<input class="styled" name="condactivity" type="checkbox"value="">
	<label class="control-label col-md-4">Condactivity</label>
	<div class="col-md-5">
		<input type="text" name="condactivity_lbl" class="form-control input-xs" name="" >
	</div>
</div>
</div>
<div class="col-md-4">
<div class="row">
	<label class="col-md-4 control-label">Lama HD (TD)
	</label>
	<div class="col-md-8">
		<div class="input-group">
			<input type="text" name="lama_hd" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">jam</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-3">
<div class="row">
	<label class="col-md-3 control-label">Dialisis
	</label>
	<div class="col-md-9">
		<div class="input-group">
			<input type="text" name="dialisis" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">jam</span>
		</div>
	</div>
</div>
</div>
<div class="col-md-3">
<div class="row">
	<label class="col-md-3 control-label">ISO&nbspUF
	</label>
	<div class="col-md-9">
		<div class="input-group">
			<input type="text" name="iso_uf" class="form-control input-xs" placeholder="">
			<span class="input-group-addon">jam</span>
		</div>
	</div>
</div>
</div>
</div>
<div class="form-group">
<label class="control-label col-md-2">Prog.Profiling :</label>
<div class="col-md-2">
<div class="checkbox no-padding">
	<input class="styled" name="na" type="checkbox"value="">
	<label class="control-label col-md-5">Na</label>
	<div class="col-md-7">
		<input type="text" name="na_lbl" class="form-control input-xs" name="" >
	</div>
</div>
</div>
<div class="col-md-2">
<div class="checkbox no-padding">
	<input class="styled" name="uf" type="checkbox"value="">
	<label class="control-label col-md-5">UF</label>
	<div class="col-md-7">
		<input type="text" name="uf_lbl" class="form-control input-xs" name="" >
	</div>
</div>
</div>
<div class="col-md-3">
<div class="checkbox no-padding">
	<input class="styled" name="bicarbonat" type="checkbox"value="">
	<label class="control-label col-md-5">Bicarbonat</label>
	<div class="col-md-7">
		<input type="text" name="bicarbonat_lbl" class="form-control input-xs" name="" >
	</div>
</div>
</div>
<div class="col-md-3">
<div class="checkbox no-padding">
	<input class="styled" name="emperatur" type="checkbox"value="">
	<label class="control-label col-md-5">Temperatur</label>
	<div class="col-md-7">
		<input type="text" name="temperatur_lbl" class="form-control input-xs" name="" >
	</div>
</div>
</div>
</div>
<div class="form-group">
<div class="col-md-6">
<label>Heparinisasi</label>
<div class="checkbox">
	<div class="checkbox no-padding">
		<input class="styled" name="dosis_sirkulasi" type="checkbox"value="">
		<label class="control-label col-md-5">Dosis sirkulasi</label>
		<div class="col-md-5">
			<div class="input-group">
				<input type="text" name="dosis_sirkulasi_lbl" class="form-control input-xs" name="" >
				<span class="input-group-addon">iu</span>
			</div>
		</div>
	</div>
</div>
<div class="checkbox">
	<div class="checkbox no-padding">
		<input class="styled" name="dosis_awal" type="checkbox"value="">
		<label class="control-label col-md-5">Dosis awal</label>
		<div class="col-md-5">
			<div class="input-group">
				<input type="text" name="dosis_awal_lbl" class="form-control input-xs" name="" >
				<span class="input-group-addon">iu</span>
			</div>
		</div>
	</div>
</div>
<div class="checkbox">
	<label class="control-label col-md-12" >
		<input type="checkbox" name="stacked-checkbox-left" class="styled">
		Dosis Maintanace :
	</label>
</div>
<div class="panel-heading">
	<div class="checkbox">
		<div class="checkbox no-padding">
			<input class="styled" name="continue" type="checkbox"value="">
			<label class="control-label col-md-5">Continue</label>
			<div class="col-md-7">
				<div class="input-group">
					<input type="text" name="continue_lbl" class="form-control input-xs" name="" >
					<span class="input-group-addon">iu/jam</span>
				</div>
			</div>
		</div>
	</div>
	<div class="checkbox">
		<div class="checkbox no-padding">
			<input class="styled" name="intermitten" type="checkbox"value="">
			<label class="control-label col-md-5">Intermitten</label>
			<div class="col-md-7">
				<div class="input-group">
					<input type="text" name="intermitten_lbl" class="form-control input-xs" name="" >
					<span class="input-group-addon">iu/jam</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="checkbox">
	<div class="checkbox no-padding">
		<input class="styled" name="lmwh" type="checkbox"value="">
		<label class="control-label col-md-5">LMWH </label>
		<div class="col-md-5">
			<input type="text" name="lmwh_lbl" class="form-control input-xs" name="" >
		</div>
	</div>
</div>
<div class="checkbox">
	<div class="checkbox no-padding">
		<input class="styled" name="tanpa_heoarin" type="checkbox"value="">
		<label class="control-label col-md-5">Tanpa Heoarin,penyebab </label>
		<div class="col-md-5">
			<input type="text" name="tanpa_heoarin_lbl" class="form-control input-xs" name="" >
		</div>
	</div>
</div>
<div class="checkbox">
	<div class="checkbox no-padding">
		<input class="styled" name="program_bilas" type="checkbox"value="">
		<label class="control-label col-md-12">Program bilas NaCI 0,9% 100cc/jam/ <sup>1</sup>/<sub>2</sub> jam
		</div>
	</div>
</div>
<div class="col-md-4">
	<textarea rows="17" name="catatan_lain" cols="37">Catatan lain :</textarea>
</div>
<div class="col-md-2">
	<textarea rows="17" name="ttd_dan_nama_dokter" cols="15">TTD  dan  Nama  Dokter :</textarea>
</div>