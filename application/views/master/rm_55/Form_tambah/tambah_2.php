<div class="form-group">
	<label class="col-md-12  control-label">2. PEMERIKSAAN FISIK</label>
	<div class="panel-body">
	</div>
	<div class="table-responsive">
		<table class="table table-xxs table-hover">
			<tr>
				<th style="width:16%"><li>Keadaan Umum</li></th>
				<th colspan="12"><div class="col-md-2">
					<div class="radio no-padding-top">
						<label class="radio">
							<input class="styled keadaan" type="radio" name="keadaan_utama" value="1">
							Baik
						</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="radio no-padding-top">
						<label class="radio">
							<input class="styled keadaan" type="radio" name="keadaan_utama" value="1">
							Sedang
						</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="radio no-padding-top">
						<label class="radio">
							<input class="styled keadaan" type="radio" name="keadaan_utama" value="1">
							Buruk
						</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="radio no-padding-top">
						<input type="radio" class="styled keadaan" name="keadaan_utama" value="2"/>
						<label class="control-label col-md-5">Lain-lain</label>
						<div class="col-md-7" id="keadaan_ya" style="display:none">
							<input name="input" class="form-control input-xs" type="text"/>
						</div>
					</div>
				</div>
			</th>
		</tr>
		<!-- no 1 -->
		<tr>
			<th><li>Tekanan darah</li></th>
			<th colspan="12"><div class="col-md-4">
				<div class="row">
					<label class="col-md-4 control-label text-right">mmHG :
					</label>
					<div class="col-md-8">
						<input type="text" class="form-control input-xs" placeholder="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<label class="col-md-4 control-label text-right">MAP :
					</label>
					<div class="col-md-8">
						<input type="text" class="form-control input-xs" placeholder="">
					</div>
				</div>
			</div></th>
		</tr>
		<!-- no 2 -->
		<tr>
			<th><li>Kesadaran</li></th>
			<th colspan="12"> 
				<div class="col-md-3">
					<div class="radio no-padding-top">
						<label class="radio">
							<input type="radio" class="styled kesadaran" name="rad" value="1"/> 
							Compos mentis
						</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="radio no-padding-top">
						<input type="radio" class="styled kesadaran" name="rad" value="2"/>
						<label class="control-label col-md-5">Lain-lain</label>
						<div class="col-md-7" id="kesadaran_ya" style="display:none">
							<input name="input" class="form-control input-xs" type="text"/>
						</div>
					</div>
				</div>
			</th>
		</tr>
		<!-- no 3 -->
		<tr>
			<th><li>Nadi</li></th>
			<th colspan="12"><div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Reguler
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Iregular
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<label class="col-md-4 control-label text-right">Frekw
					</label>
					<div class="col-md-8">
						<div class="input-group">
							<input type="text" class="form-control input-xs" placeholder="">
							<span class="input-group-addon">x/mnt</span>
						</div>
					</div>
				</div>
			</div></th>
		</tr>
		<!-- no 4 -->
		<tr>
			<th><li>Respirasi</li></th>
			<th colspan="12"><div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Normal
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Kusmaul
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Dispnea
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						Ronchi
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="radio no-padding">
						<input class="styled" type="radio" value="">
						<label class="control-label col-md-3">Frek</label>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" class="form-control  input-xs">
								<span class="input-group-addon">x/mnt</span>
							</div>
						</div>
					</div>
				</div>
			</div></th>
		</tr>
		<!-- no 5 -->
		<tr>
			<th><li>Konjungtiva</li></th>
			<th colspan="12"><div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled konjungtiva" name="konjungtiva" type="radio" value="1">
						Tidak anemis
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled konjungtiva" name="konjungtiva" type="radio" value="1">
						Anemis
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="radio no-padding-top">
					<input type="radio" class="styled konjungtiva" name="konjungtiva" value="2"/>
					<label class="control-label col-md-5">Lain-lain</label>
					<div class="col-md-7" id="konjungtiva_ya" style="display:none">
						<input name="input" class="form-control input-xs" type="text"/>
					</div>
				</div>
			</div></th>
		</tr>
		<!-- no 6 -->
		<tr>
			<th><li>Ekstremitas</li></th>
			<th colspan="12"><div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" name="ekstemitas" value="">
						<font size="1">Tidak edema/dehidrasi</font>
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" name="ekstemitas" value="">
						<font size="1">Dehidrasi</font>
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" name="ekstemitas" value="">
						<font size="1">Oedema</font>
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" name="ekstemitas" value="">
						<font size="1">Edema anasarka</font>
					</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" name="ekstemitas" value="">
						<font size="1">Pucat & dingin</font>
					</label>
				</div>
			</div>
		</th>
	</tr>
	<!-- no 7 -->
	<tr>
		<th><li>Berat Badan</li></th>
		<th colspan="12">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-4">Pre HD :</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" name="tanda_vital_td" class="form-control input-xs">
										<span class="input-group-addon">kg</span>
									</div>
								</div>
								<!-- <label class="col-md-3">mmHG</label> -->
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-4">BB HD y.i :</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" name="tanda_vital_td" class="form-control input-xs">
										<span class="input-group-addon">kg</span>
									</div>
								</div>
								<!-- <label class="col-md-3">mmHG</label> -->
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-4">BB kering :</label>
								<div class="col-md-6">
									<input type="text" name="tanda_vital_td" class="form-control input-xs">
								</div>
								<!-- <label class="col-md-3">mmHG</label> -->
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-4">BB +/- :</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" name="tanda_vital_td" class="form-control input-xs">
									</div>
								</div>
								<!-- <label class="col-md-3">mmHG</label> -->
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-4">Post HD:</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" name="tanda_vital_td" class="form-control input-xs">
										<span class="input-group-addon">kg</span>
									</div>
								</div>
								<!-- <label class="col-md-3">mmHG</label> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</th>
	</tr>
	<!-- no 8 -->
	<tr>
		<th ><li>Akses Vaskuler</li></th>
		<th colspan="12">
			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				AV-fistula
			</label>

			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				Femoral
			</label>
			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				HD kateter
			</label>
			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				Subclavia
			</label>
			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				jugular
			</label>
			<label class="radio-inline">
				<input type="radio" name="ekstremitas" class="styled ekstremitas" value="1">
				Femoral
			</label>
			<label class="radio-inline">
				<input type="radio" class="styled ekstremitas" name="ekstremitas" value="2"/>
				Lain-lain
				<div class="control-group" id="ekstremitas_ya" style="display:none">
					<input name="input" class="form-control input-xs" type="text"/>
				</div>
			</label>
		</th>
	</tr>
	<tr>
		<th style="width:58%" colspan="3">Resiko jatuh : berikan = (CHEKKLIST) pada kotak skor</th>
		<th colspan="3"></th>
		<th colspan="3" style="width: 11%">Skor</th>
	</tr>
	<tr>
		<th  colspan="3" rowspan="2">1. Riwayat jatuh yang baru atau dalam bulan terakhir</th>
		<th colspan="3">Tidak</th>
		<th colspan="3"><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios1" name="riwayat_jatuh" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Ya</th>
		<th colspan="3"><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios2" name="riwayat_jatuh" value="25">
				<font size="1">25</font>
			</label>
		</div></th>
	</tr>
	<!-- no 1 -->
	<tr>
		<th style="width:58%" colspan="3" rowspan="2">2. Diagnose medis sekunder > 1</th>
		<th colspan="3">Tidak</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios3" name="diagnose" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Ya</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios4" name="diagnose" value="15">
				<font size="1">15</font>
			</label>
		</div></th>
	</tr>
	<!-- no 2 -->
	<tr>
		<th style="width:58%" colspan="3" rowspan="3">3. Alat bantu jalan</th>
		<th colspan="3">Bed rest</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios5" name="alat_bantu_jalan" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Penopang,tongkat</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios6" name="alat_bantu_jalan" value="15">
				<font size="1">15</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Furniture</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios7" name="alat_bantu_jalan" value="30">
				<font size="1">30</font>
			</label>
		</div></th>
	</tr>
	<!-- no 3 -->
	<tr>
		<th style="width:58%" colspan="3" rowspan="2">4. Memakai terapi heparin lock/iv</th>
		<th colspan="3">Tidak</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios8" name="mamakai_terapi" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Ya</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios9" name="mamakai_terapi" value="25">
				<font size="1">25</font>
			</label>
		</div></th>
	</tr>
	<!-- no 4 -->
	<tr>
		<th style="width:58%" colspan="3" rowspan="3">5. Cara berjalan/berpindah</th>
		<th colspan="3">Normal/bedrest/imobilisasi</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios10" name="cara_berjalan" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Lemah</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios11" name="cara_berjalan" value="15">
				<font size="1">15</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Terganggu</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios12" name="cara_berjalan" value="30">
				<font size="1">30</font>
			</label>
		</div></th>
	</tr>
	<!-- no 5 -->
	<tr>
		<th style="width:58%" colspan="3" rowspan="2">6. Status mental</th>
		<th colspan="3">Orientasi sesuai kemampuan</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios13" name="status_mental" value="0">
				<font size="1">0</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th colspan="3">Lupa keterbatasan</th>
		<th><div class="radio no-padding-top">
			<label class="radio">
				<input class="styled pemeriksaan_fisik" type="radio" id="radios14" name="status_mental" value="15">
				<font size="1">15</font>
			</label>
		</div></th>
	</tr>
	<tr>
		<th style="width:5%" colspan="6"><label class="col-md-2 control-label">Kesimpulan : </label>
			<div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						0-24 (resiko rendah)
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						24-45 (resiko sedang)
					</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="radio no-padding-top">
					<label class="radio">
						<input class="styled" type="radio" value="">
						>45 (resoki tinggi)
					</label>
				</div>
			</div></th>
			<th colspan="2">Skor total=<span class="totalskor" ></span></th>
		</tr>
		<!-- no 9 -->
	</table>
</div>
</div>