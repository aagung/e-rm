 					<div class="form-group">
 						<label class="col-md-2  control-label">1. KELUHAN UTAMA</label>
 						<div class="col-md-2">
 							<div class="radio no-padding-top">
 								<label class="radio">
 									<input class="styled keluhan" type="radio" name="keluhan_utama" value="1">
 									Sesak napas
 								</label>
 							</div>
 						</div>
 						<div class="col-md-2">
 							<div class="radio no-padding-top">
 								<label class="radio">
 									<input class="styled keluhan" type="radio" name="keluhan_utama" value="1">
 									Mual,Muntah
 								</label>
 							</div>
 						</div>
 						<div class="col-md-1">
 							<div class="radio no-padding-top">
 								<label class="radio">
 									<input class="styled keluhan" type="radio" name="keluhan_utama" value="1">
 									Gatal
 								</label>
 							</div>
 						</div>
 						<div class="col-md-4">
 							<div class="radio no-padding-top">
 								<input type="radio" class="styled keluhan" name="keluhan_utama" value="2"/>
 								<label class="control-label col-md-5">Lain-lain</label>
 								<div class="col-md-7" id="keluhan_ya" style="display:none">
 									<input name="input" class="form-control input-xs" type="text"/>
 								</div>
 							</div>
 						</div>

 						<div class="form-group">
 							<label class="col-md-2"></label>
 							<label class="col-md-1 control-label">Nyeri </label>
 							<div class="col-md-1">
 								<div class="form-group">
 									<div class="col-md-12">
 										<div class="radio no-padding-top">
 											<label class="radio">
 												<input class="styled" type="radio" name="sn_adakah_rasa_nyeri" value="Tidak">
 												Tidak
 											</label>
 										</div>
 									</div>
 								</div>
 							</div>
 							<div class="col-md-1">
 								<div class="form-group">
 									<div class="col-md-12">
 										<div class="radio no-padding-top">
 											<label class="radio">
 												<input class="styled" type="radio" name="sn_adakah_rasa_nyeri" value="Ya">
 												Ya
 											</label>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 						<div class="col-md-10 text-right">  								
 							Ringan : 0 - 3<br>
 							Sedang : 4 - 6<br>
 							Berat  : 7 - 10
 							<div class="col-md-12">
 								<center>
 									<img src="<?php echo base_url ('assets/img/emotskor.jpeg') ?>" style="margin-top: -60px">
 								</center>
 							</div>
 						</div>
 						<div class="col-md-12" style="margin-top: 10px">
 							<label class="col-md-2 control-label"></label>
 							<div class="col-md-4">
 								<div class="row">
 									<label class="col-md-4 control-label text-right">Lokasi :
 									</label>
 									<div class="col-md-8">
 										<input type="text" class="form-control" placeholder="">
 									</div>
 								</div>
 							</div>
 							<div class="col-md-4">
 								<div class="row">
 									<label class="col-md-4 control-label text-right">Durasi :
 									</label>
 									<div class="col-md-8">
 										<input type="text" class="form-control" placeholder="">
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>