<div class="col-md-12">
	<div class="form-group">
		<label class="col-lg-3 control-label">Pendelegasian tugas dari dr</label>
		<div class="col-lg-9">
			<div class="row">
				<div class="col-md-4">
					<input type="text" name="pendelegasian" placeholder="" id="" class="form-control" required="required">
				</div>
				<div class="col-md-4">
					<input type="text" name="pendelegasian_ke" placeholder="" id="" class="form-control" required="required">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">Hari/Tgl/Jam</label>
		<div class="col-lg-9">
			<div class="row">
				<div class="col-md-6">
					<input type="date" name="tanggal" placeholder="" id="" class="form-control" required="required">
				</div>

				<div class="col-md-6">
					<input type="text" name="jam" placeholder="" id="" class="form-control" required="required">
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label input-required ">Nama Pasien</label>
		<label class="radio-inline">
			<input type="radio" name="jenis_kelamin" class="styled">
			L
		</label>
		<label class="radio-inline">
			<input type="radio" name="jenis_kelamin" class="styled">
			P
		</label>
		<div class="col-lg-6">
			<input type="text" class="form-control" name="nama_pasien" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">Tanggal Lahir</label>
		<div class="col-lg-9">
			<input type="date" class="form-control" name="tanggal_lahir" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">No. RM</label>
		<div class="col-lg-9">
			<input type="text" class="form-control" name="no_rm" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">Dx. Medis</label>
		<div class="col-lg-9">
			<div class="row">
				<div class="col-md-6">
					<input type="text" name="dx_medis_e" placeholder="" id="" class="form-control" required="required">
				</div>

				<div class="col-md-6">
					<input type="text" name="dx_medis_c" placeholder="" id="" class="form-control" required="required">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">No. Mesin</label>
		<div class="col-lg-9">
			<input type="text" class="form-control" name="no_mesin" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">Hemodiliasi ke-</label>
		<div class="col-lg-9">
			<input type="text" class="form-control" name="hemodiliasi" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-4 control-label input-required">Tipe Dialsiser,N/R</label>
		<div class="col-lg-8">
			<div class="row">
				<div class="col-md-6">
					<input type="text" name="tipe_dialsier" placeholder="" id="" class="form-control" required="required">
				</div>

				<div class="col-md-6">
					<input type="text" name="n_r" placeholder="" id="" class="form-control" required="required">
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label input-required ">Riw. Alergi Obat</label>
		<div class="col-md-4">
			<label class="radio-inline">
				<input type="radio" name="alergi_obat" class="styled">
				Tidak
			</label>
			<label class="radio-inline">
				<input type="radio" name="alergi_obat" class="styled">
				Ya
			</label>
		</div>
		<div class="col-lg-5">
			<input type="text" class="form-control" name="alergi_obat_lbl" value="" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label input-required">Cara Bayar</label>
		<div class="col-md-6">
			<label class="radio-inline" >
				<input type="radio" name="cara_bayar" class="styled">
				A
			</label>
			<label class="radio-inline">
				<input type="radio" name="cara_bayar" class="styled">
				G
			</label>
			<label class="radio-inline">
				<input type="radio" name="cara_bayar" class="styled">
				U
			</label>
			<label class="radio-inline">
				<input type="radio" name="cara_bayar" class="styled">
				K
			</label>
		</div>
		<div class="col-lg-3">
			<input type="text" class="form-control" name="pekerjaan" value="" >
		</div>
	</div>
</div>