 <style>
   .box {
      border: 2px solid grey;
      margin-bottom: 10px;
      margin-top: 10px; 
      padding-bottom: 20px;
      padding-top: 20px;

   }
</style>
<script type="text/javascript">
   $(document).on('click', '.pemeriksaan_fisik' ,function(){
      var a = "a";
      var $check =[];
      var tambah =0;
      var $check = $(".pemeriksaan_fisik:checked").each(function(){
         var  checks = $(this).attr('id');
         var defin= $('#'+checks+'').val();
         tambah += (parseInt(defin));
         var $cek =[];
         var $cek = $('#'+checks+'').val();
         $('.totalskor').html(tambah);
         $('#skor_sga').val(tambah);
      });
   }).get();

   $(".styled").uniform({
      radioClass: 'choice'
   });
   $(function(){
     $(":radio.keluhan").click(function(){
      $("#keluhan_tidak, #keluhan_ya").hide()
       if($(this).val() == "1"){
         $("#keluhan_tidak").show();
      }else{
         $("#keluhan_ya").show();
      }
   });
  });
   $(function(){
     $(":radio.keadaan").click(function(){
      $("#keadaan_tidak, #keadaan_ya").hide()
       if($(this).val() == "1"){
         $("#keadaan_tidak").show();
      }else{
         $("#keadaan_ya").show();
      }
   });
  });
   $(function(){
     $(":radio.kesadaran").click(function(){
      $("#kesadaran_tidak, #kesadaran_ya").hide()
       if($(this).val() == "1"){
         $("#kesadaran_tidak").show();
      }else{
         $("#kesadaran_ya").show();
      }
   });
  });
   $(function(){
     $(":radio.konjungtiva").click(function(){
      $("#konjungtiva_tidak, #konjungtiva_ya").hide()
       if($(this).val() == "1"){
         $("#konjungtiva_tidak").show();
      }else{
         $("#konjungtiva_ya").show();
      }
   });
  });
     $(function(){
     $(":radio.ekstremitas").click(function(){
      $("#ekstremitas_tidak, #ekstremitas_ya").hide()
       if($(this).val() == "1"){
         $("#ekstremitas_tidak").show();
      }else{
         $("#ekstremitas_ya").show();
      }
   });
  });
       $(function(){
     $(":radio.kendala").click(function(){
      $("#kendala_tidak, #kendala_ya").hide()
       if($(this).val() == "1"){
         $("#kendala_tidak").show();
      }else{
         $("#kendala_ya").show();
      }
   });
  });
          $(function(){
     $(":radio.merawat").click(function(){
      $("#merawat_tidak, #merawat_ya").hide()
       if($(this).val() == "1"){
         $("#merawat_tidak").show();
      }else{
         $("#merawat_ya").show();
      }
   });
  });
   function keluhanutama (checked){
      if (checked == true)
         $ ("#hiddenkeluhanutama").fadeIn();
      else  $ ("#hiddenkeluhanutama").fadeOut();
   }
   function keadaanumum (checked){
      if (checked == true)
         $ ("#hiddenkeadaanumum").fadeIn();
      else  $ ("#hiddenkeadaanumum").fadeOut();
   }
   function kesadarans (checked){
      if (checked == true)
         $ ("#hiddenkesadaran").fadeIn();
      else  $ ("#hiddenkesadaran").fadeOut();
      if (checked == false)
         $ ("#hiddenkesadaranout").fadeIn();
      else  $ ("#hiddenkesadaranout").fadeOut();
   }
   function konjungtiva (checked){
      if (checked == true)
         $ ("#hiddenkonjungtiva").fadeIn();
      else  $ ("#hiddenkonjungtiva").fadeOut();
   }
   function aksesv (checked){
      if (checked == true)
         $ ("#hiddenaksesv").fadeIn();
      else  $ ("#hiddenaksesv").fadeOut();
   }
   function kendala_komunikasi (checked){
      if (checked == true)
         $ ("#hiddenkendala").fadeIn();
      else  $ ("#hiddenkendala").fadeOut();
   }
   function merawat_dirumah (checked){
      if (checked == true)
         $ ("#hiddenmerawat").fadeIn();
      else  $ ("#hiddenmerawat").fadeOut();
   }
   function diagnosa_keseimbangan (checked){
      if (checked == true)
         $ ("#hiddenkeseimbangan").fadeIn();
      else  $ ("#hiddenkeseimbangan").fadeOut();
   }
   
</script>