 <div class="col-md-12" style="margin-top: 20px">
 	<div class="col-md-6">
 		<img src="<?php echo base_url ('assets/img/ananda.png') ?>" width="350px">
 	</div>
 	<div class="col-md-6">
 		<div class="panel">
 			<div class="panel-heading">
 				<center>
 					<h5><b>ASUHAN KEPERAWATAN PASIEN HELMODIALISA</b><br></h5>
 					(Diisi maksimal 10 menit saat melakukan assessment pre-HD)
 					<hr width="100%">
 					RM 55 / Rev.0 / VIII / 14
 				</center>
 			</div>
 		</div>
 	</div>
 </div>
 <form class="form-horizontal" method="post" id="form">
 	<div class="panel panel-flat">
 		<div class="panel-body">
 			<div class="row">
 				<?php include 'Form_tambah/biodata.php' ?>
 				<!-- form biodata -->
 			</div>
 		</div>
 		<div class="panel-body">
 			<label><b>PENGKAJIAN KEPERAWATAN</b></label>
 			<div class="panel-body">
 				<div class="panel-heading">
 					<?php include 'Form_tambah/tambah_1.php' ?>
 					<!-- no 1 keluhan utama -->
 					<?php include 'Form_tambah/tambah_2.php' ?>
 					<!-- no 2 pemeriksaan fisik -->
 					<?php include 'Form_tambah/tambah_3.php' ?>
 					<!-- no 3 pemeriksaan penunjang -->
 					<?php include 'Form_tambah/tambah_4.php' ?>
 					<!-- no 4 gizi -->
 					<?php include 'Form_tambah/tambah_5.php' ?>
 					<!-- no 5 riwayat psikososial -->
 					
 				</div>
 				<div class="panel-footer">
 					<!-- Input Hidden -->
 					<input type="hidden" name="option_id" value="" />
 					<input type="hidden" name="option_name" value="" />
 					<input type="hidden" name="option_type" value="text" />
 					<input type="hidden" name="option_table" value="" />
 					<input type="hidden" name="autoload" value="yes" />
 					<input type="hidden" name="user_visibility" value="1" />
 					<div class="text-right">
 						<button type="submit" class="btn btn-success btn-labeled btn-save">
 							<b><i class="icon-floppy-disk"></i></b>
 							Simpan
 						</button>
 						<button type="button" class="btn btn-default cancel-button">
 							Batal
 						</button>
 					</div>
 				</div>
 			</div>
 		</form>	