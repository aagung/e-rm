						<div class="col-md-12">
		            		<div class="row">
		            			<table class="table table-bordered">
		            				<tr>
		            					<td colspan="3">
		            						<div class="col-md-12">
		            							<div class="row">
			            							<div class="form-group">
			            								<label class="col-md-12">EVALUASI KEPERAWATAN :</label>
			            								<div class="col-md-12">
			            									<div class="row">
			            										<textarea class="form-control" name="evaluasi_keperawatan"></textarea>
			            									</div>
			            								</div>
			            							</div>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            				<tr>
		            					<td colspan="3">
		            						<div class="col-md-12">
		            							<div class="row">
			            							<div class="form-group">
			            								<label class="col-md-12">Discart Planing (gunakan edukasi jika diperlukan) :</label>
			            								<div class="col-md-12">
			            									<div class="row">
			            										<textarea class="form-control" name="discart_planing"></textarea>
			            									</div>
			            								</div>
			            							</div>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            				<tr>
		            					<td colspan="3">
		            						<div class="col-md-12">
		            							<div class="row">
			            							<div class="col-md-6">
			            								<div class="row">
			            									<div class="form-group">
					            								<label class="col-md-4">Akses Vaskular oleh </label>
					            								<div class="col-md-8">
					            									<input type="text" class="form-control input-xs" name="akses_vaskular">
			            										</div>
			            									</div>
			           									</div>
			           								</div>
			            							
			           								<div class="col-md-6">
			           									<div class="form-group">
				            								<div class="pull-right">
				            									<label class="col-md-12">Nama Perawat Yang Bertugas</label>
				            									<div class="col-md-12">
				            										<div class="row">
					            										<input type="text" class="form-control input-xs" name="nama_perawat">
					           										</div>
					           									</div>
					           								</div>
			            								</div>
			            							</div>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            				<tr>
		            					<td colspan="3">
		            						<div class="col-md-12">
		            							<div class="row">
			            							<div class="form-group">
			            								<label class="col-md-12">Evaluasi Medik</label>
			            								<div class="col-md-12">
			            									<div class="row">
			            										<textarea class="form-control" name="evaluasi_medik"></textarea>
			            									</div>
			            								</div>
			            							</div>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            				<tr>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<label>Obat</label>
		            							</div>
		            						</div>
		            					</td>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<label>Catatan Medis</label>
		            							</div>
		            						</div>
		            					</td>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<label>Nama Dokter</label>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            				<tr>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<textarea class="form-control " name="obat"></textarea>
		            							</div>
		            						</div>
		            					</td>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<textarea class="form-control" name="catatan_medis"></textarea>
		            							</div>
		            						</div>
		            					</td>
		            					<td class="text-center">
		            						<div class="col-md-12">
		            							<div class="row">
		            								<textarea class="form-control" name="nama_dokter"></textarea>
		            							</div>
		            						</div>
		            					</td>
		            				</tr>
		            			</table>
		            		</div>
		            	</div>