						<div class="col-md-12">
		            		<div class="row">

		            			<div class="col-md-12">
		            				<label>
		            					<div class="row">		
		            						<h5>Penyulit selama HD :</h5>
		            					</div>
		            				</label>
		            			</div>
		            			<div class="col-md-6"> 
		            				<div class="row">
		            					
				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_masalah_akses" class="styled">
					            						<label>Masalah akses</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_hiperkalemi" class="styled">
					            						<label>Hiperkalemi</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_demam" class="styled">
					            						<label>Demam</label>
					            					</div>
				            					</div>
				            				</div>
				            			</div>

				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_pendarahan" class="styled">
					            						<label>Pendarahan</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
					            				<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_hipotensi" class="styled">
					            						<label>Hipotensi</label>
					            					</div>
					            				</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" name="penyulit_hd_mengigil_dingin" class="styled">
					            						<label>Mengigil/dingin</label>
					            					</div>
				            					</div>
				            				</div>
				            			</div>

				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_first_use_syndrom">
					            						<label>First use Syndfrom</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_masalah_hipertensi">
					            						<label>Hipertensi</label>
					            					</div>
				            					</div>
				            				</div>	
				            			</div>

				            		</div>
		            			</div>

		            			<div class="col-md-6">
		            				<div class="row">
				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_masalah_sakit_kepala">
					            						<label>Sakit Kepala</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_masalah_nyeri_dada">
					            						<label>Nyeri Dada</label>
					            					</div>
				            					</div>
				            				</div>	
				            			</div>

				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_mual_dan_muntah">
					            						<label>Mual & muntah</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_aritmia">
					            						<label>Aritmia</label>
					            					</div>
				            					</div>
				            				</div>	
				            			</div>

				            			<div class="col-md-4">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_kram_otot">
					            						<label>Kram otot</label>
					            					</div>
				            					</div>
				            				</div>
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" class="styled" name="penyulit_hd_gatal_gatal">
					            						<label>Gatal-gatal</label>
					            					</div>
				            					</div>
				            				</div>	
				            			</div>

				            			<div class="col-md-12">
				            				<div class="form-group">
				            					<div class="row">
					            					<div class="checkbox">
					            						<input type="checkbox" id="penyulit_selama_hd_lain_lain" name="penyulit_hd_lain_lain" class="styled">
					            						<label class="col-md-3">Lain-lain</label>
					            					</div>
					            					<div class="col-md-9" id="penyulit_selama_hd_lain_lain_hidden" style="display:none;">
					            						<input type="text" class="form-control input-xs reset_penyulit_hd_nama_lain_lain" name="penyulit_hd_nama_lain_lain">
					            					</div>
				            					</div>
				            				</div>	
				            			</div>
		            				</div>
		            			</div>
		            		</div>
		            	</div>