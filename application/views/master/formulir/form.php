	<!-- Untuk Radio dan checkbox styled -->
	<span class="switchery-warning switchery-info switchery-danger switchery-primary"></span>
   	<!-- Tutup checkbok dan Radio Styled -->
	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>
		        
		        <legend class="text-bold" style="margin-top:0px"></legend>
			         			
			    <div class="row">
		            <div class="col-md-12">
		            		
		            	<!-- Form Penyulit selama HD -->
		            	<?php include 'form/form1.php' ?>

						<!-- Form Table -->
		            	<?php include 'form/form2.php' ?>
		            	
		            	

		            	<div class="col-md-12">
		            		<div class="row">
		            			<label class="col-md-12">
		            				<h5>Terima kasih atas kerjasamanya telah mengisi formulir ini dengan benar dan jelas</h5>
		            			</label>
		            		</div>
		            	</div>
		            </div>
			    </div>
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	<script>
	
</script>
