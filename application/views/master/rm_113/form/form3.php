							<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="anastesi_cb">
					    				<label>Anastesi</label>
					    			</div>
				    			</td>
				    			<td colspan="12" class="text-center">
				    				<div class="col-md-12">
				    					<label>
				    						<b>RIWAYAT PSIKOLOGIS</b>
				    					</label>
									</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="orthopedi_cb">
					    				<label>Orthopedi</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-12">
				    					<div class="row">
				    						<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_tidak_semangat">
				    										<label>Tidak Semangat</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>
				    						
				    						<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_tidak_rasa_tertekan">
				    										<label>Rasa Tertekan</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_tidak_depresi">
				    										<label>Depresi</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

											<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_tidak_sulit_tidur">
				    										<label>Sulit Tidur</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_tidak_cepat_lelah">
				    										<label>Cepat Lelah</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-2">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_sulit_bicara">
				    										<label>Sulit Bicara</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>



				    						
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="riwayat_psikologis_paru">
					    				<label>Paru</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-8">
				    					<div class="row">
				    						<div class="col-md-6">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_kurang_nafsu_makan">
				    										<label>Kurang Nafsu makan</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>
				    						
				    						<div class="col-md-6">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_sulit_konsentrasi">
				    										<label>Sulit Konsentrasi</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-6">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_menggunakan_obat_penenang">
				    										<label>Menggunakan Obat penenang</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

											<div class="col-md-6">
				    							<div class="row">
				    								<div class="form-group">
				    									<div class="checkbox-inline">
				    										<input type="checkbox" class="styled" name="riwayat_psikologis_merasa_bersalah">
				    										<label>Merasa Bersalah</label>
				    									</div>
				    								</div>
				    							</div>
				    						</div>

				    						
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="mata_cb">
					    				<label>Mata</label>
					    			</div>
				    			</td>
				    			<td colspan="12" class="text-center">
				    				<label><b>SKOR SKALA NYERi</b></label>
				    			</td>
				    		</tr>