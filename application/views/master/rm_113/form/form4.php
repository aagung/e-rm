				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="">
					    				<label>THT</label>
					    			</div>
				    			</td>
				    			<td colspan="12" rowspan="6" class="text-center">
				    				<div class="col-md-12">
				    					<div class="row">
				    						<div class="col-md-12">
				    							<div class="row">
				    								<table class="table table-bordered">
				    									<tr>
				    										<td>
				    											<label style="font-size:55px; ">&#128515;</label>
				    										</td>
				    										<td>
				    											<label style="font-size:55px; ">&#128527;</label>
				    										</td>
				    										<td>
				    											<label style="font-size:55px; ">&#128528;</label>
				    										</td>
				    										<td>
				    											<label style="font-size:55px; ">&#128533;</label>
				    										</td>
				    										<td>
				    											<label style="font-size:55px; ">&#128542;</label>
				    										</td>
				    										<td>
				    											<label style="font-size:55px; ">&#128557;</label>
				    										</td>
				    									</tr>
				    									<tr>
				    										<td class="text-center">
				    											<label class="control-label"> 0 </label> 
				    											<label class="control-label">Tidak sakit</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    										<td class="text-center">
				    											<label class="control-label">2</label>
				    											<label class="control-label">Sedikit sakit</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    										<td class="text-center">
				    											<label class="control-label">4</label>
				    											<label class="control-label">Agak mengganggu</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    										<td class="text-center">
				    											<label class="control-label">6</label>
				    											<label class="control-label">Mengganggu aktivitas</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    										<td class="text-center">
				    											<label class="control-label">8</label>
				    											<label class="control-label">Sangat Mengganggu</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    										<td class="text-center">
				    											<label class="control-label">10</label>
				    											<label class="control-label">Tak tertahankan</label>
				    											<div class="form-group">
																	<input type="radio" class="styled" name="skor_skala_nyeri1">
				    											</div>
				    										</td>
				    									</tr>
				    								</table>
				    							</div>
				    						</div>
				    						<div class="col-md-1">
				    							
				    						</div>
				    						<div class="col-md-12">
				    							<div class="row">
				    								<div id="ion-grid-snap"></div>
				    							</div>
				    							<div class="row">
				    								<label class="col-md-1 pull-left row">Tidak Nyeri</label>
				    								<label class="col-md-4 text-center">Nyeri Ringan</label>
				    								<label class="col-md-3 text-center">Nyeri Sedang</label>
				    								<label class="col-md-3 text-center">Nyeri Berat Terkontrol</label>
				    								<label class="col-md-1 pull-right">Nyeri berat tidak terkontrol</label>
				    							</div>
				    						</div>
				    					</div>
				    				</div>	
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="gigi_cb">
					    				<label>Gigi</label>
					    			</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="gizi_cb">
					    				<label>Gizi</label>
					    			</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="kul_kel_cb">
					    				<label>Kul kel</label>
					    			</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="jiwa_cb">
					    				<label>jiwa</label>
					    			</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="p_umum_cb">
					    				<label>P. Umum</label>
					    			</div>
				    			</td>
				    		</tr>
