							<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="anak_cb">
					    				<label>Anak</label>
					    			</div>
				    			</td>
				    			<td colspan="12" class="text-center">
				    				<label><b>PENGKAJIAN KEPERAWATAN</b></label>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="obsgyn_cb">
					    				<label>Obsgyn</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-9">
				    					<div class="row">
				    						<label class="col-md-4"><b>TANDA VITAL</b></label>
				    						<label class="col-md-4"><b>SKRINING NUTRISI</b></label>
				    						<label class="col-md-4"><b>FUNSIONAL</b></label>
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="p_dalam_cb">
					    				<label>P. Dalam</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-9">
				    					<div class="row">
				    						<div class="col-md-4">
				    							<div class="row">
				    					
				    								<div class="form-group">
				    									<label class="col-md-3">TD :</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="tanda_vital_td" class="form-control input-xs">
				    											<span class="input-group-addon">mmHg</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">mmHG</label> -->
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">BB :</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="skrining_nutrisi_bb" class="form-control input-xs">
				    											<span class="input-group-addon">g/Kg</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">g/Kg</label> -->
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-6">Alat Bantu :</label>
				    									<div class="col-md-6">
				    										<input type="text"  name="funsional_alat_bantu" class="form-control input-xs">
				    									</div>
				    									
				    								</div>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="bedah_cb">
					    				<label>Bedah</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-9">
				    					<div class="row">
				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">Frek Nadi</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="tanda_vital_frek_nadi" class="form-control input-xs">
				    											<span class="input-group-addon">x/mnt</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">x/mnt</label> -->
				    								</div>
				    							</div>
				    						</div>
				    						
				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">TB</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="skrining_nutrisi_tb" class="form-control input-xs">
				    											<span class="input-group-addon">cm</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">cm</label> -->
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-6">Prothese</label>
				    									<div class="col-md-6">
				    										<input type="text" name="funsional_prothese" class="form-control input-xs">
				    									</div>
				    									
				    								</div>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="saraf_cb">
					    				<label>Saraf</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-9">
				    					<div class="row">
				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">Frek Nafas</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="tanda_vital_frek_nafas" class="form-control input-xs">
				    											<span class="input-group-addon">x/mnt</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">x/mnt</label> -->
				    								</div>
				    							</div>
				    						</div>
				    						
				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">Ling Perut</label>
				    									<div class="col-md-9">
				    										<div class="input-group">	
					    										<input type="text" name="tanda_vital_ling_perut" class="form-control input-xs">
					    										<span class="input-group-addon">cm</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">cm</label> -->
				    								</div>
				    							</div>
				    						</div>

				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-6">Cacat tubuh</label>
				    									<div class="col-md-6">
				    										<input type="text" name="funsional_cacat_tubuh" class="form-control input-xs">
				    									</div>
				    									
				    								</div>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="urologi_cb">
					    				<label>Urologi</label>
					    			</div>
				    			</td>
				    			<td colspan="12">
				    				<div class="col-md-9">
				    					<div class="row">
				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-3">Suhu</label>
				    									<div class="col-md-9">
				    										<div class="input-group">
				    											<input type="text" name="tanda_vital_suhu" class="form-control input-xs">
				    											<span class="input-group-addon">&#8451;</span>
				    										</div>
				    									</div>
				    									<!-- <label class="col-md-3">C</label> -->
				    								</div>
				    							</div>
				    						</div>
				    						
				    						<div class="col-md-4"></div>

				    						<div class="col-md-4">
				    							<div class="row">
				    								<div class="form-group">
				    									<label class="col-md-6">ADL</label>
				    									<div class="col-md-6">
				    										<div class="form-group">
				    											<div class="checkbox">
				    												<input type="checkbox" class="styled" name="funsional_adl_mandiri">
				    												<label>Mandiri</label>
				    											</div>
				    											<div class="checkbox">
				    												<input type="checkbox" class="styled" name="funsional_adl_dibantu">
				    												<label>Dibantu</label>
				    											</div>
				    										</div>
				    									</div>
				    									
				    								</div>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</td>
				    		</tr>