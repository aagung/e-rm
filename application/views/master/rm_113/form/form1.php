							<tr>
					    		<td colspan="12" style="width:70%;">
					    			<div class="col-md-12">
					    				<div class="row">
					    					<div class="col-md-12">
					    						<div class="row">
							    					<div class="form-group">
							    						<div class="row">
										    				<label class="col-md-3">Nama</label>
										    				<div class="col-md-9">
										    					<input type="text" class="form-control input-xs" name="nama">
										    				</div>
							    						</div>
							    					</div>

							    					<div class="form-group">
							    						<div class="row">
										    				<label class="col-md-3">Tanggal Lahir</label>
										    				<div class="col-md-9">
										    					<input type="date" class="form-control input-xs" name="tanggal_lahir">
										    				</div>
							    						</div>
							    					</div>

							    					<div class="form-group">
							    						<div class="row">
										    				<label class="col-md-3">No Rekam Medis</label>
										    				<div class="col-md-9">
										    					<input type="text" class="form-control input-xs" name="no_rekam_medis">
										    				</div>
							    						</div>
							    					</div>

							    					<div class="form-group">
							    						<div class="row">
										    				<label class="col-md-3">Jenis Kelamin</label>
										    				<div class="col-md-9">
										    					<div class="radio-inline">
											    					<input type="radio" value="Laki-laki" class="styled" name="jenis_kelamin">Laki laki
										    					</div>
										    					<div class="radio-inline">
											    					<input type="radio" value="Perempuan" class="styled" name="jenis_kelamin">Perempuan
										    					</div>
										    					
										    				</div>
							    						</div>
							    					</div>
							    					<label>( Bila ada bisa tempel striker identitas pasien ).</label>
												</div>
					    					</div>

					    					<div style="margin-top:15px;"></div>
					    					
					    					<div class="col-md-12">
					    						<div class="row">					    							
					    							<div class="form-group">
					    								<div class="row">	
							    							<label class="col-md-12"><b> ALASAN MASUK RS (Keluhan utama saat masuk RS) </b></label>
										    				<div class="col-md-12">
										    					<textarea class="form-control" name="alasan_masuk_rs"></textarea>
										    				</div>
					    								</div>
					    							</div>
					    						</div>
					    					</div>

					    					<div class="col-md-12">
					    						<div class="row">		
					    							<div class="form-group">
					    								<div class="row">	
							    							<label class="col-md-12"><b> RIWAYAT KESEHATAN/ PENGOBATAN/ PERAWATAN SEBELUMNYA : </b></label>
										    				<div class="col-md-12">
										    					<div class="row">
										    						<label class="col-md-2">Pernah di rawayat</label>
											    					<div class="col-md-3">
											    						<div class="form-group">
											    							<div class="radio-inline">
											    								<input type="radio" class="styled pernah_dirawat" id="pernah_dirawat_tidak" name="pernah_dirawat" value="Tidak"> <label>Tidak</label>
											    							</div>
											    							<div class="radio-inline">
											    								<input type="radio" class="styled pernah_dirawat" id="pernah_dirawat_ya" name="pernah_dirawat" value="Ya"> <label>Ya,</label>
											    							</div>
											    						</div>
											    					</div>
											    					
											    					<div id="pernah_dirawathidden" style="display:none;">
												    					<div class="col-md-3">
												    						<div class="form-group">
													    						<div class="row">
													    							<label class="col-md-3">Kapan</label>
													    							<div class="col-md-9">
													    								<input type="text" class="form-control input-xs" name="pernah_dirawat_kapan">
													    							</div>
													    						</div>
												    						</div>
												    					</div>
												    					<div class="col-md-4">
												    						<div class="form-group">
												    							<div class="row">
												    								<label class="col-md-3">Dimana</label>
												    								<div class="col-md-9">
												    									<input type="text" class="form-control input-xs" name="pernah_dirawat_dimana">
												    								</div>
												    							</div>
												    						</div>
												    					</div>
												    					<div class="col-md-5" style="margin-top:10px;">
												    						<div class="form-group">
												    							<div class="row">
												    								<label class="col-md-3">Diagnosis</label>
												    								<div class="col-md-9">
												    									<input type="text" class="form-control input-xs" name="pernah_dirawat_diagnosis">
												    								</div>
												    							</div>
												    						</div>
												    					</div>
											    					</div>

										    					</div>
										    				</div>
					    								</div>
					    							</div>
					    						</div>
					    					</div>

					    					<div class="col-md-12">
					    						<div class="row">
					    							<div class="form-group">
					    								<div class="row">
					    									<label class="col-md-12"><b>STATUS SOSIAL</b>:Hubungan pasien dengan anggota keluarga</label>
					    									<div class="col-md-12">
					    										<div class="form-group">
					    											<div class="col-md-12">
						    											<div class="radio-inline">
						    												<input type="radio" class="styled" name="status_sosial_hubungan_pasien_dengan_keluarga" value="Baik"><label>Baik</label>
						    											</div>
						    											<div class="radio-inline">
						    												<input type="radio" class="styled" name="status_sosial_hubungan_pasien_dengan_keluarga" value="Tidak Baik"><label>Tidak Baik</label>
						    											</div>
					    											</div>
					    										</div>
					    									</div>
					    								</div>
					    							</div>
					    						</div>
					    					</div>

					    					<div class="col-md-12">
					    						<div class="row">
					    							<div class="form-group">
					    								<div class="row">
					    									<label class="col-md-3"><b>STATUS EKONOMI</b>:</label>
					    									<div class="col-md-9">
					    										<div class="form-group">
					    											<div class="col-md-12">
					    												<div class="col-md-4">
					    													<div class="row">
					    														<div class="form-group">
									    											<div class="checkbox-inline">
									    												<input type="checkbox" class="styled" name="status_ekonomi_asuransi">
									    												<label>Asuransi</label> 
									    											</div>
					    														</div>
					    													</div>
					    												</div>
					    												<div class="col-md-4">
						    												<div class="row">
						    													<div class="form-group">
									    											<div class="checkbox-inline">
									    												<input type="checkbox" class="styled" name="status_ekonomi_jaminan">
									    												<label>Jaminan</label> 
									    											</div>
						    													</div>
						    												</div>
					    												</div>
					    												<div class="col-md-4">
					    													<div class="row">
					    														<div class="form-group">
									    											<div class="checkbox-inline">
									    												<input type="checkbox" class="styled" name="status_ekonomi_biaya_sendiri">
									    												<label> Biaya Sendiri</label>
									    											</div>
					    														</div>
					    													</div>
					    												</div>
					    												<div class="col-md-6">
					    													<div class="row">
								    											<div class="form-group">
								    												<div class="checkbox-inline">
										    											<input type="checkbox" id="status_ekonomi_lainnya" class="styled" name="status_ekonomi_lainnya"> 
										    											<label class="col-md-7 row">Lainnya,Sebutkan</label>

										    											<div class="col-md-5" id="status_ekonomi_lainnya_hidden" style="display:none;">
										    												<div class="row">
										    													<input type="text" class="form-control input-xs" name="status_ekonomi_nama_lainnya">
										    												</div>
										    											</div>
									    											</div>
									    										</div>
							    											</div>
					    												</div>

					    											</div>
					    										</div>
					    									</div>
					    								</div>
					    							</div>
					    						</div>
					    					</div>
					    				</div>
					    			</div>
					    		</td>

					    		<td style="width:30%">
					    			<div class="col-md-12">
					    				<label class="col-md-12"><b> Alergi : </b></label>
					    				<div class="col-md-12">
					    					<div class="row">
					    						<div class="form-group">
					    							<div class="col-md-12">
						    							<div class="radio">
						    								<input type="radio" class="styled alergi" id="alergi_tidak" name="alergi">
						    								<label>Tidak</label>
						    							</div>
					    							</div>
					    							<div class="col-md-12">
					    								<div class="row">
						    								<div class="col-md-4"> 
								    							<div class="radio">
								    								<input type="radio" class="styled alergi" id="alergi_ya" name="alergi">
								    								<label>Ya</label>
								    							</div>
						    								</div>

						    								<div id="alergihidden" style="display:none;">
							    								<div class="col-md-8">
							    									<div class="row">
							    										<div class="form-group">
							    											<div class="row">
								    											<div class="checkbox">
								    												<input type="checkbox" id="alergi_obat" class="styled" name="alergi_obat">
								    												<label class="col-md-5">Obat</label>
								    											</div>
								    											<div class="col-md-7" id="alergi_obat_hidden">
								    												<input type="text" name="alergi_nama_obat" class="form-control input-xs">
								    											</div>
							    											</div>
							    										</div>

																		<div class="form-group">
							    											<div class="row">
								    											<div class="checkbox">
								    												<input type="checkbox" name="alergi_makanan" class="styled" id="alergi_makanan">
								    												<label class="col-md-5">Makanan</label>
								    											</div>
								    											<div class="col-md-7" id="alergi_makanan_hidden">
								    												<input type="text" name="alergi_nama_makanan" class="form-control input-xs">
								    											</div>
							    											</div>
							    										</div>

							    										<div class="form-group">
							    											<div class="row">
								    											<div class="checkbox">
								    												<input type="checkbox" name="alergi_lainnya" class="styled" id="alergi_lainnya">
								    												<label class="col-md-5">Lainnya</label>
								    											</div>
								    											<div class="col-md-7" name="alergi_nama_lainnya" id="alergi_lainnya_hidden">
								    												<input type="text" class="form-control input-xs">
								    											</div>
							    											</div>
							    										</div>						    									
							    									</div>
							    								</div>

							    								<div class="col-md-12" style="margin-top:10px;">
							    									<div class="row">
							    										<div class="form-group">
							    											<div class="row">
								    											<label class="col-md-3">Reaksi</label>
								    											<div class="col-md-9">
								    												<input type="text" name="alergi_reaksi" class="form-control input-xs" name="">
								    											</div>
							    											</div>
							    										</div>
							    									</div>
							    								</div>
						    								</div>

					    								</div>
					    							</div>
					    						</div>
					    					</div>
					    				</div>
					    			</div>
					    		</td>
					    	</tr>