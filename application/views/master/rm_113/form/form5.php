							<tr>
				    			<td>
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="reh_med_cb">
					    				<label>Reh Med</label>
					    			</div>
				    			</td>
				    			<td colspan="12" class="text-center">
				    				<label><b>PENGKAJIAN RESIKO JATUH</b></label>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="jantung_cb">
					    				<label>Jantung</label>
					    			</div>
				    			</td>
				    			<td colspan="4" style="width:30%">
				    				<label>Riwayat Jatuh Dalam 3 Bulan</label>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio1" name="riwayat_jatuh_3_bulan" value="25">	
				    					<label>Ya : 25</label>
				    				</div>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio2" name="riwayat_jatuh_3_bulan" value="0">	
				    					<label>Tidak : 0</label>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="_1">
					    				<label></label>
					    			</div>
				    			</td>
				    			<td colspan="4" style="width:30%">
				    				<label>Ada Diagnosis Sekunder</label>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio3" name="ada_diagnosis_sekunder" value="25">	
				    					<label>Ya : 25</label>
				    				</div>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio4" name="ada_diagnosis_sekunder" value="0">	
				    					<label>Tidak : 0</label>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="_2">
					    				<label></label>
					    			</div>
				    			</td>
				    			<td colspan="4" style="width:30%">
				    				<label>Menggunakan alat bantu</label>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio5" name="menggunakan_alat_bantu" value="15">	
				    					<label>Kruk/Kursi Roda : 15</label>
				    				</div>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio6" name="menggunakan_alat_bantu" value="30">	
				    					<label>Berpegangan : 30</label>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="_3">
					    				<label></label>
					    			</div>
				    			</td>
				    			<td colspan="4" style="width:30%">
				    				<label>Gaya berjalan</label>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" id="radio7" class="styled radioskor" name="gaya_berjalan" value="10">	
				    					<label>Lemah : 10</label>
				    				</div>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" id="radio8" class="styled radioskor" name="gaya_berjalan" value="20">	
				    					<label>Ada gangguan : 20</label>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="_4">
					    				<label></label>
					    			</div>
				    			</td>
				    			<td colspan="4" style="width:30%">
				    				<label>Status mental</label>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
					    				<input type="radio" class="styled radioskor" id="radio9" name="status_mental" value="25">	
					    				<label class="">Orientasi mental : 25</label>
					    			</div>
				    			</td>

				    			<td colspan="4" class="text-center" style="width:30%">
				    				<div class="form-group">
				    					<input type="radio" class="styled radioskor" id="radio10" name="status_mental" value="0">	
				    					<label>Disorietasi : 0</label>
				    				</div>
				    			</td>
				    		</tr>

				    		<tr>
				    			<td >
					    			<div class="checkbox">
					    				<input type="checkbox" class="styled" name="_5">
					    				<label></label>
					    			</div>
				    			</td>
				    			<td colspan="12" style="width:30%">
				    				<label>Jumlah Skor :</label>
				    				<span class="jumlah_skor">0</span>
				    				<input type="hidden" class="jumlah_skor" name="skor_skala_nyeri_jumlah_skor">
				    			</td>
				    		</tr>

				    		<tr>
				    			<td></td>
				    			<td colspan="3" style="width:20%">
				    				<label>Kriteria Penilaian :</label>
								</td>

				    			<td colspan="3" class="text-center" style="width:20%">
				    				<label>	Skor 0-24 Tidak Resiko Jatuh</label>
								</td>

				    			<td colspan="3" class="text-center" style="width:20%">
				    				<label>	Skor 25-50 Tidak Jatuh Rendah</label>
								</td>

				    			<td colspan="3" class="text-center" style="width:20%"> 
				    				<label>	Skor > 50 Tidak Jatuh Tinggi</label>
								</td>
				    		</tr>
