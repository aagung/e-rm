	<!-- Untuk Radio dan checkbox styled -->
	<span class="switchery-warning switchery-info switchery-danger switchery-primary"></span>
   	<!-- Tutup checkbok dan Radio Styled -->
	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>

		        <legend class="text-bold" style="margin-top:0px"></legend>
				    
				<table class="table table-bordered table-xlg">
				    <thead>
				   		<!-- Form Nama -->
				    	<?php include 'form/form1.php' ?>
			    	</thead>
			    	<tbody>
			    		<tr>
			    			<td style="width:20%">Poli :</td>
				    		<td colspan="12" style="width:80%">
				    			<div class="row">
				    				<div class="form-group">
				    					<label class="col-md-3">Tanggal/Jam Kunjungan :</label>
				    					<div class="col-md-9">
				    						<input type="text" class="form-control" name="">
				    					</div>
			    					</div>
				    			</div>
				    		</td>
				    	</tr>

				    	<!-- Form PENGKAJIAN KEPERAWATAN -->
				    	<?php include 'form/form2.php' ?>

						<!-- Form PENGKAJIAN RIWAYAT PSIKOLOGIS -->
				    	<?php include 'form/form3.php' ?>
				    		
					    <!-- Form SKOR SKALA NYERI -->
					   	<?php include 'form/form4.php' ?>
				    	
				    	<!-- Form PENGKAJIAN RESIKO JATUH -->
				    	<?php include 'form/form5.php' ?>
			    		
			    		<!-- Form MASALAH KEPERAWATAN DAN RENCANA ASUHAN KEPERAWATAN -->
					    <?php include 'form/form6.php' ?>
				    	
				    	<!-- Form Tanggal dan Nama -->
					    <?php include 'form/form7.php' ?>
				    	
											    		
				    </tbody>
				</table>		
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	