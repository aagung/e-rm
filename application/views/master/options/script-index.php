<script>
var url = {
    loadData: "<?php echo site_url('api/master/options/load_data'); ?>",
    delete: "<?php echo site_url('api/master/options/delete'); ?>",
    formCreate: "<?php echo site_url('master/options/form_create'); ?>",
    formEdit: "<?php echo site_url('master/options/form_edit/:OPTION_ID'); ?>"
};

var table = $("#table"),
    tableDt,
    tableTimer;

tableDt = table.dataTable({
    "sPaginationType": "full_numbers",
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": url.loadData,
    "columns": [
        {
            "data": "option_name",
            "render": function (data, type, row, meta) {
                var link = url.formEdit.replace(':OPTION_ID', row.option_id);
                return '<a href="' + link + '">' + data + '</a>';
            },
            "className": ""
        },
        {
            "data": "option_value",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": ""
        },
        {
            "data": "option_type",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": ""
        },
        {
            "data": "autoload",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": "text-center"
        },
        {
            "data": "option_id",
            "render": function (data, type, row, meta) {
                var btnEdit = '<button type="button" class="btn btn-link btn-xs edit" data-option_id="' + data + '"><i class="fa fa-edit"></i></button>';
                var btnDelete = '<button type="button" class="btn btn-link btn-xs delete" data-option_id="' + data + '"><i class="fa fa-trash-o"></i></button>';

                return btnEdit + '&nbsp;' + btnDelete;
            },
            "className": "text-center"
        }
    ],
    "order": [ [0, "asc"] ],
    "stateSave": true,
    "stateSaveParams": function (settings, data) {
        data.search.search = "";
        // remove filters
        for (var i = 0; i < data.columns.length; i++) {
            switch (i) {
                default:
                    data.columns[i].search.search = "";
                    break;
            }
        }
    },
    "fnServerData": function ( sSource, aoData, fnCallback ) {
        if (tableTimer) clearTimeout(tableTimer);
        tableTimer = setTimeout(function () {
            blockElement(table.selector);
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json);
            });
        }, 500);
    },
    "fnDrawCallback": function (oSettings) {
      var n = oSettings._iRecordsTotal;
      
      table.unblock();

      table.find('[data-popup=tooltip]').tooltip();
    }
});

table.on('click', '.edit', function (e) {
    var option_id = $(this).data('option_id');
    window.location.assign(url.formEdit.replace(':OPTION_ID', option_id));
});

table.on('click', '.delete', function (e) {
    var option_id = $(this).data('option_id');
    var postData = {
        option_id: option_id
    };
    swal({
        title: "Hapus Option Tersebut?",
        text: "",
        html: true,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        confirmButtonColor: "#F44336",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    },
    function(){
        $.ajax({
            url: url.delete,
            type: 'POST',
            dataType: "json",
            data: postData,
            success: function (data) {
                successMessage('Success', 'Data berhasil dihapus.');
                tableDt.fnDraw(false);
            },
            error: function (error) {
                errorMessage('Error', 'Terjadi kesalahan saat hendak menghapus data.');
            },
            complete: function () {
                swal.close();
            }
        });
    });
});

$("#btn-refresh").click(function (e) {
    tableDt.fnDraw(false);
});
</script>