	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>

		        <legend class="text-bold" style="margin-top:0px"></legend>
				    
				<table class="table table-bordered">
				    <thead>
			    	</thead>
			    	<tbody>
		    			<tr>
		    				<td>RUMAH SAKIT ANANDA</td>
		    				<td>PENILAIANAN PRA SEDASI DAN ANASTESI</td>
		    			</tr>
		    			<tr>
		    				<td>BEKASI</td>
		    				<td>RM-58/II/2016</td>
		    			</tr>			    		
				    </tbody>
				</table>
				<br>
				<!-- Biodata -->
				<?php include 'form/form1.php' ?>
				<br>
				<div class="col-md-12">
					<div class="row">
						<p>Subjective</p>
						<div class="col-md-6">
							<div class="row">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-md-2">Anamnesa</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="anamnesa" placeholder="*(Diisi oleh Perawat)">
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Data Sosial -->
						<?php include 'form/form2.php' ?>
						
						<!-- Kebiasaan -->
						<?php include 'form/form3.php' ?>

						<!-- Pengobatan -->
						<?php include 'form/form4.php' ?>
						
						<!-- Riwayat Keluarga -->
						<?php include 'form/form5.php' ?>
						
						<!-- Riwayat Penyakit Pasien -->
						<?php include 'form/form6.php' ?>
						
					</div>
				</div>		
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	