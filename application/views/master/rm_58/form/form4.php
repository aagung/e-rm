						<div class="col-md-12">
							<div class="row">
								<p>
									<b><ins>PENGOBATAN</ins></b>
									(Sebutkan dosis atau jumlah pil perhari)
								</p>
								<div class="col-md-12">
									<div class="row">
										<label class="col-md-12 row">Obat yang biasa diminum :</label>
										<div class="col-md-6">
											<div class="row">
												<div class="form-group">
													<div class="row">	
														<label class="col-md-4">
															Pengunaan aspirin rutin
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_penggunaan_aspirin_rutin">		Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_penggunaan_aspirin_rutin">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-4">
															Alergi obat
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_obat">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_obat">Tidak
															</label>
														</div>
													</div>	
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-4">
															Alergi lateks
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_lateks">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_lateks">Tidak
															</label>
														</div>
													</div>	
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="form-group">
													<div class="row">
														<label class="col-md-4">
															Alergi plester
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_plester">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_plester">Tidak
															</label>
														</div>
													</div>	
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-4">
															Alergi makanan
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_makanan">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="pengobatan_alergi_makanan">Tidak
															</label>
														</div>
													</div>	
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						