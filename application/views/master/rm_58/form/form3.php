						<div class="col-md-6">
							<div class="row">
								<p><b><ins>KEBIASAAN</ins></b></p>
								<div class="col-md-12">
									<div class="row">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label col-md-4">
															Merokok :
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled kebiasaan_merokok" 
																id="kebiasaan_merokok_ya" 
																name="kebiasaan_merokok" value="Ya">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled kebiasaan_merokok" 
																id="kebiasaan_merokok_tidak" 
																name="kebiasaan_merokok" value="Tidak">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-6" id="kebiasaan_merokok_hidden" style="display:none;">
													<div class="row">
														<label class="control-label col-md-4">
															Sebanyak :
														</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="kebiasaan_merokok_sebanyak" >
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label col-md-4">
															Alkohol :
														</label>
														<div class="col-md-8">
															<label class="radio-inline">
																<input type="radio" class="styled kebiasaan_alkohol"
																	id="kebiasaan_alkohol_ya" value="Ya" 
																	name="kebiasaan_alkohol">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled kebiasaan_alkohol"	  id="kebiasaan_alkohol_tidak" 
																	value="Tidak" 
																	name="kebiasaan_alkohol">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-6" id="kebiasaan_alkohol_hidden" style="display:none;">
													<div class="row">
														<label class="control-label col-md-4">
															Sebanyak :
														</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="kebiasaan_alkohol_sebanyak">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						