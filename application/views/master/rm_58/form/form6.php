						<div class="col-md-12">
							<div class="row">
								<p>
									<b><ins>RIWAYAT PENYAKIT PASIEN :</ins></b>
									Apakah pasien pernah menderita penyakit dibawah ini?
								</p>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Pendarahan tidak normal
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_pendarahan_tidak_normal">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_pendarahan_tidak_normal">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Hepatitis / sakit kuning
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_hepatitis_sakit_kuning">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_hepatitis_sakit_kuning">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Regurgitas: asam lambung / maag
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_asam_lambung_maag">		Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_asam_lambung_maag">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Anemia / kurang darah
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_anemia_kurang_darah">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_anemia_kurang_darah">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Nyeri dada
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_nyeri_dada">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_nyeri_dada">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Asma
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_asma">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_asma">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Diabetes / sakit gula
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_diabetes_sakit_gula">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_diabetes_sakit_gula">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Pingsan
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_pingsan">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_pingsan">Tidak
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Serangan jantung 
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_serangan_jantung">		Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_serangan_jantung">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Mengorok
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_mengorok">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_mengorok">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Hipertensi
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_hipertensi">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_hipertensi">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Sumbatan jalan nafas
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled"
																 name="rwyt_pnykt_pasien_sumbatan_jalan_nafas">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled"
																 name="rwyt_pnykt_pasien_sumbatan_jalan_nafas">Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															saat tidur / sleep apnea
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_saat_tidur_sleep_apnea">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rwyt_pnykt_pasien_saat_tidur_sleep_apnea">Tidak
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="row">
												<div class="form-group">
													<div class="row">
														<label class="col-md-4">
															Apakah pasien pernah mendapatkan transfuse darah?
														</label>
														<div class="col-md-2">
															<label class="radio-inline">
																<input type="radio" class="styled rwyt_pnykt_pasien_pernah_transfuse_darah" id="rwyt_pnykt_pasien_pernah_transfuse_darah_ya" 
																 name="rwyt_pnykt_pasien_pernah_transfuse_darah" value="Ya">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled rwyt_pnykt_pasien_pernah_transfuse_darah" id="rwyt_pnykt_pasien_pernah_transfuse_darah_tidak" 
																 name="rwyt_pnykt_pasien_pernah_transfuse_darah" value="Tidak">Tidak
															</label>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6" id="rwyt_pnykt_pasien_pernah_transfuse_darah_hidden" style="display:none">
															<div class="form-group">
																<div class="row">
																	<label class="col-md-4">
																		Bila Ya, tahun berapa
																	</label>
																	<div class="col-md-4">
																		<input type="text" class="form-control" name="rwyt_pnykt_pasien_tahun_transfuse_darah">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<div class="row">
																<label class="col-md-4">
																	Apakah pasien pernah di periksa untuk diagnosis HIV?
																</label>
																<div class="col-md-2">
																	<label class="radio-inline">
																		<input type="radio" class="styled rwyt_pnykt_pasien_pernah_pemeriksaan_hiv" id="rwyt_pnykt_pasien_pernah_pemeriksaan_hiv_ya" name="rwyt_pnykt_pasien_pernah_pemeriksaan_hiv">Ya
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="styled rwyt_pnykt_pasien_pernah_pemeriksaan_hiv" id="rwyt_pnykt_pasien_pernah_pemeriksaan_hiv_tidak" name="rwyt_pnykt_pasien_pernah_pemeriksaan_hiv">Tidak
																	</label>
																</div>
															</div>
														</div>
														<div id="rwyt_pnykt_pasien_pernah_pemeriksaan_hiv_hidden" style="display:none;">
															<div class="col-md-6">
																<div class="form-group">
																	<div class="row">
																		<label class="col-md-4">
																			Bila Ya, tahun berapa
																		</label>
																		<div class="col-md-4">
																			<input type="text" class="form-control" name="rwyt_pnykt_pasien_tahun_pemeriksaan_hiv">
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-7">
																<div class="form-group">
																	<div class="row">
																		<label class="col-md-4">
																			Hasil pemerikasaan HIV
																		</label>
																		<div class="col-md-4">
									    									<label class="radio-inline">
									    										<input type="radio" class="styled" value="Positive" name="rwyt_pnykt_pasien_hasil_pemeriksaan_hiv" >Positive
									    									</label>
									    									<label class="radio-inline">
									    										<input type="radio" class="styled" value="Negative" name="rwyt_pnykt_pasien_hasil_pemeriksaan_hiv" >Negative
									    									</label>
																		</div>
								    								</div>
																</div>
															</div>	
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-12">
															Apakah pasien memakai:
														</label>
														<div class="col-md-3">
															<div class="row">
																<label class="col-md-4">
																	Kacamata
																</label>
																<div class="col-md-8">
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_kacamata">Ya
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_kacamata">Tidak
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="row">
																<label class="col-md-5">
																	Alat bantu dengar
																</label>
																<div class="col-md-7">
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_alat_bantu_dengar">Ya
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_alat_bantu_dengar">Tidak
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="row">
																<label class="col-md-4">
																	Gigi palsu
																</label>
																<div class="col-md-8">
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_gigi_palsu">Ya
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="styled" name="rwyt_pnykt_pasien_pasien_memakai_gigi_palsu">Tidak
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-2">
															Riwayat Operasi:
														</label>
														<div class="col-md-3">
															<div class="row">
																<label class="col-md-12 radio-inline">
																	<input type="radio" class="styled rwyt_pnykt_pasien_riwayat_operasi" id="rwyt_pnykt_pasien_riwayat_operasi_tidak" name="rwyt_pnykt_pasien_riwayat_operasi">
																	belum pernah operasi
																</label>
															</div>
														</div>
														<div class="col-md-7">
															<div class="row">
																<label class="col-md-12 radio-inline">
																	<input type="radio" class="styled rwyt_pnykt_pasien_riwayat_operasi" id="rwyt_pnykt_pasien_riwayat_operasi_ya" name="rwyt_pnykt_pasien_riwayat_operasi">
																	 pernah operasi
																</label>
																<br>
																<div class="radio">	
																	<div class="col-md-9" id="rwyt_pnykt_pasien_riwayat_operasi_hidden" style="display:none;">
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Tahun 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_tahun">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Jenis Operasi 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_jenis_operasi">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Jenis Anestesia 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_jenis_anastesia">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Anestesia-local kompilkasi / reaksi 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_anastesia_local">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Anestesia-regional kompilkasi / reaksi 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_anastesia_regional">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="row">
																				<label class="col-md-7"> Anestesia-umum kompilkasi / reaksi 
																				</label>
																				<div class="col-md-5">
																					<input type="text" class="form-control input-xs" name="rwyt_pnykt_pasien_rwyt_operasi_anastesia_umum">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>