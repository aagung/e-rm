						<div class="col-md-12">
							<div class="row">
								<p>
									<b><ins>RIWAYAT KELUARGA</ins></b>
									Apakah keluarga pernah menderita penyakit seperti dibawah ini?
								</p>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group">
													<div class="row">	
														<label class="col-md-6">
															Pendarahan yang tidak normal
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_pendarahan_tidak_normal">		Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_pendarahan_tidak_normal">		Tidak
															</label>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-6">
															Permasalahan dalam pembiusan
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_permasalahan_pembiusan">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_permasalahan_pembiusan">Tidak
															</label>
														</div>
													</div>	
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-6">
															Diabetes
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_diabetes">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_diabetes">Tidak
															</label>
														</div>
													</div>	
												</div>
												<div class="form-group">
													<div class="row">
														<label class="col-md-6">
															Asma
														</label>
														<div class="col-md-6">
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_asma">Ya
															</label>
															<label class="radio-inline">
																<input type="radio" class="styled" name="rk_asma">Tidak
															</label>
														</div>
													</div>	
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						