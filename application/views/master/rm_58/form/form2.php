						<div class="col-md-12">
							<div class="row">
								<p><b><ins>DATA SOSIAL</ins></b></p>
								<div class="col-md-2">
									<div class="row">
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Umur</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="data_sosial_umur">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="row">
										<div class="form-group">
											<div class="row">
												<label class="col-md-4">Jenis kelamin :</label>
												<div class="col-md-8">
													<label class="radio-inline">
														<input type="radio" class="styled" name="data_sosial_jenis_kelamin" value="Laki-laki">Laki-laki
													</label>
													<label class="radio-inline">
														<input type="radio" class="styled" name="data_sosial_jenis_kelamin" value="Perempuan">Perempuan
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="row">
										<div class="form-group">
											<div class="row">
												<label class="col-md-4">Menikah :</label>
												<div class="col-md-8">
													<label class="radio-inline">
														<input type="radio" class="styled" name="data_sosial_menikah">Ya
													</label>
													<label class="radio-inline">
														<input type="radio" class="styled" name="data_sosial_menikah">Tidak
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="row">
										<div class="form-group">
											<div class="row">
												<label class="col-md-3">Pekerjaan</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="data_sosial_pekerjaan">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						