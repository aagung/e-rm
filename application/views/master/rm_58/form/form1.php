				<table class="table table-bordered">
				    <thead>
			    	</thead>
			    	<tbody>
		    			<tr>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
			    								<label class="control-label col-md-4">
			    									Nama Pasien :
			    								</label>
			    								<div class="col-md-8">
			    									<input type="text" class="form-control" name="nama_pasien">
			    								</div>
		    								</div>
		    							</div>
		    						</div>
			    				</div>
			    			</td>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
			    								<label class="control-label col-md-4">
			    									No RM :
			    								</label>
			    								<div class="col-md-8">
			    									<input type="text" class="form-control" name="no_rm">
			    								</div>
		    								</div>
		    							</div>
		    						</div>
			    				</div>
			    			</td>
		    			</tr>
		    			<tr>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
			    								<label class="control-label col-md-4">
			    									Jenis Kelamin :
			    								</label>
			    								<div class="col-md-8">
			    									<label class="radio-inline">
			    										<input type="radio" class="styled" value="Laki-laki" name="jenis_kelamin" >Laki-laki
			    									</label>
			    									<label class="radio-inline">
			    										<input type="radio" class="styled" value="Perempuan" name="jenis_kelamin" >Perempuan
			    									</label>
			    								</div>
		    								</div>
		    							</div>
		    						</div>
			    				</div>
			    			</td>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
		    									<label class="control-label col-md-4">
		    										Waktu :
		    									</label>
		    									<div class="col-md-8">
		    										<input type="text" class="form-control" name="waktu">
		    									</div>
		    								</div>
		    							</div>
		    						</div>
		    					</div>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
			    								<label class="control-label col-md-4">
			    									Tanggal Lahir :
			    								</label>
			    								<div class="col-md-8">
			    									<input type="date" class="form-control" name="tanggal_lahir">
			    								</div>
		    								</div>
		    							</div>
		    						</div>
			    				</div>
			    			</td>
		    				<td>
		    					<div class="col-md-12">
		    						<div class="row">
		    							<div class="form-group">
		    								<div class="row">
			    								<label class="control-label col-md-4">
			    									Hari/Tanggal :
			    								</label>
			    								<div class="col-md-8">
			    									<input type="text" class="form-control" name="hari_tanggal">
			    								</div>
		    								</div>
		    							</div>
		    						</div>
			    				</div>
			    			</td>
		    			</tr>			    		
				    </tbody>
				</table>
				