<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
			<hr>
			<div class="col-md-6">
				<!--BIODATA-->
				<?php include "Form_tambah/biodata.php"; ?>
				<div class="panel-body">
					<div class="form-group">
						<label class="text-danger">*(Data diatas diambil dari data sebelumnya)</label>
					</div>
				</div>
				<!-- FORM TAMBAH 1 -->
				<?php include "Form_tambah/tambah_1.php"; ?>
				<div class="form-group">
					<label class="col-lg-4 control-label">Obat:<span class="text-danger">*(freetext)</span></label>
				</div>	
				<hr>
				<label>Barang & material yang diserahkan :</label>
				<!--FORM TAMBAH 2  -->
				<?php include "Form_tambah/tambah_2.php"; ?>
				<label>Lain-lain / Keterangan : <span class="text-danger">*(freetext)</span></label>
			</div>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
		<!-- FORM TAMBAH 3 -->
		<?php include "Form_tambah/tambah_3.php"; ?>
		<div class="panel-footer">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
					Batal
				</button>
			</div>
		</div>
	</form>