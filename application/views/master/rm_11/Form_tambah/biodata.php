<div class="form-group">
	<label class="col-lg-4 control-label input-required">Nama</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="nama" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">No.RM</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="no_rm" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label input-required ">Umur</label>
	<label class="radio-inline">
		<input type="radio" name="jenis_kelamin" class="styled">
		L
	</label>
	<label class="radio-inline">
		<input type="radio" name="jenis_kelamin" class="styled">
		P
	</label>
	<div class="col-lg-3">
		<input type="number" class="form-control" name="umur" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Ruangan</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="ruangan" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Kelas</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="kelas" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Dokter Operator</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="dokter_operator" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Diagnosa Medis</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="diagnosa_medis" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Jenis Tindakan/Operasi</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="jenis_tindakan" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Mulai Operasi</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="mulai_operasi" value="" >
	</div>
</div>
<div class="form-group">
	<label class="col-lg-4 control-label input-required">Selesai Operasi</label>
	<div class="col-lg-6">
		<input type="text" class="form-control" name="selesai_operasi" value="" >
	</div>
</div> 