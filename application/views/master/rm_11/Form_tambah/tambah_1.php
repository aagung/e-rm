				<div class="form-group">
					<label class="col-lg-4 control-label">Kesadaran</label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="kesadaran" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Tekanan Darah</label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="tekanan_darah" value="" >
							<span class="input-group-addon">mmHg</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Nadi</label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="nadi" value="" >
							<span class="input-group-addon">x/menit</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Pernafasan</label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="pernafasan" value="" >
							<span class="input-group-addon">x/menit</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Suhu</label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="suhu" value="" >
							<span class="input-group-addon">&#8451;</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Kulit<span style="margin-left: 23%">: Warna</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="kulit" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Turgor</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="turgor" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Luka Op.Panjang</label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="luka_op_panjang" value="" >
							<span class="input-group-addon">cm</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Lokasi </span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="lokasi" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Drain<span style="margin-left: 19%">: Jml Cairan</span></label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="drain" value="" >
							<span class="input-group-addon">cc</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Warna</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="warna" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Infus<span style="margin-left: 20%">: Lokasi</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="lokasi" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Jenis Cairan</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="jenis_cairan" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Kecepatan</span></label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="kecepatan" value="" >
							<span class="input-group-addon">tts/menit</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Sisa Cairan</span></label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="sisa_cairan" value="" >
							<span class="input-group-addon">cc</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Kateter Urin</label>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Fiksasi Balon</span></label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="fiksasi_balon" value="" >
							<span class="input-group-addon">cc</span>
						</div>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Jumlah Urin</span></label>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control" name="jumlah_urin" value="" >
							<span class="input-group-addon">cc</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label"><span style="margin-left: 40%">Warna</span></label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="warna" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-4 control-label">Jenis Jaringan</label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="jenis_jaringan" value="" >
					</div>
				</div>