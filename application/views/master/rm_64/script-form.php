<script>
$(document).ready(function(){

          //Tingkat Pendidikan
          $(".bersedia_menerima_edukasi").on('change click',function(){
            if ($('#bersedia_menerima_edukasi_ya').is(':checked')) {
               $("#bersedia_menerima_edukasi_hidden").show();                                   
                                                  }
            else{
            $("#bersedia_menerima_edukasi_hidden").hide();
            
            $("#kosong_hidden").hide();
            $('#kosong_hidden').find("input[type=text]").val("");

            $("#tingkat_pendidikan_lain_lain_hidden").hide();
            $('#tingkat_pendidikan_lain_lain_hidden').find("input[type=text]").val("");

            $("#tingkat_pendidikan_lain_lain_hidden").hide();
            $('#tingkat_pendidikan_lain_lain_hidden').find("input[type=text]").val("");

            $("#baca_dan_tulis_lain_lain_hidden").hide();
            $('#baca_dan_tulis_lain_lain_hidden').find("input[type=text]").val("");
         
            $("#bahasa_yang_digunakan_bahasa_asing_hidden").hide();
            $('#bahasa_yang_digunakan_bahasa_asing_hidden').find("input[type=text]").val("");

            $("#perlu_penterjemah_lain_lain_hidden").hide();
            $('#perlu_penterjemah_lain_lain_hidden').find("input[type=text]").val("");

            $("#hamabatan_menerima_edukasi_lain_lain_hidden").hide();
            $('#hamabatan_menerima_edukasi_lain_lain_hidden').find("input[type=text]").val("");

            $("#metode_edukasi_lain_lain_hidden").hide();
            $('#metode_edukasi_lain_lain_hidden').find("input[type=text]").val("");
           
            
        
           

            $('#bersedia_menerima_edukasi_hidden').find("input[type=text], input[type=checkbox], input[type=radio], textarea").val("");
            $('#bersedia_menerima_edukasi_hidden').find("span input[type=checkbox]").prop('checked',false);
            $('#bersedia_menerima_edukasi_hidden').find("span.checked").removeAttr('class');
                }
          });

          if ($('#bersedia_menerima_edukasi_ya').is(':checked')) {
            $("#bersedia_menerima_edukasi_hidden").show();                                   
                                         }
           if ($('#bersedia_menerima_edukasi_tidak').is(':checked')) {
            $("#bersedia_menerima_edukasi_hidden").hide();                                   
                                         }
          else{
            $("#bersedia_menerima_edukasi_hidden").hide();
               }

           $("#kosong").on('change click',function(){
            if ($('#kosong').is(':checked')) {
               $("#kosong_hidden").show();                                   
                                                  }
            else{
            $("#kosong_hidden").hide();
            $('#kosong_hidden').find("input[type=text]").val("");
                }
          });

          if ($('#kosong').is(':checked')) {
            $("#kosong_hidden").show();                                   
                                         }
          else{
            $("#kosong_hidden").hide();
               }

           $("#tingkat_pendidikan_lain_lain").on('change click',function(){
            if ($('#tingkat_pendidikan_lain_lain').is(':checked')) {
               $("#tingkat_pendidikan_lain_lain_hidden").show();                                   
                                                  }
            else{
            $("#tingkat_pendidikan_lain_lain_hidden").hide();
            $('#tingkat_pendidikan_lain_lain_hidden').find("input[type=text]").val("");
                }
          });

          if ($('#tingkat_pendidikan_lain_lain').is(':checked')) {
            $("#tingkat_pendidikan_lain_lain_hidden").show();                                   
                                         }
          else{
            $("#tingkat_pendidikan_lain_lain_hidden").hide();
             $('#tingkat_pendidikan_lain_lain_hidden').find("input[type=text]").val("");
               }
          //Tutup Tingkat pendidikan

          //Baca dan tulis
           $("#baca_dan_tulis_lain_lain").on('change click',function(){
            if ($('#baca_dan_tulis_lain_lain').is(':checked')) {
               $("#baca_dan_tulis_lain_lain_hidden").show();                                   
                                                  }
            else{
            $("#baca_dan_tulis_lain_lain_hidden").hide();
            $('#baca_dan_tulis_lain_lain_hidden').find("input[type=text]").val("");
           
                }
          });

          
          if ($('#baca_dan_tulis_lain_lain').is(':checked')) {
            $("#baca_dan_tulis_lain_lain_hidden").show();                                   
                                         }
          else{
            $("#baca_dan_tulis_lain_lain_hidden").hide();
            $('#baca_dan_tulis_lain_lain_hidden').find("input[type=text]").val("");
           
               }
          //Tutup baca dan tulis

          //bahasa yang digunakan
          $("#bahasa_yang_digunakan_bahasa_asing").on('change click',function(){
            if ($('#bahasa_yang_digunakan_bahasa_asing').is(':checked')) {
               $("#bahasa_yang_digunakan_bahasa_asing_hidden").show();                                   
                                                  }
            else{
            $("#bahasa_yang_digunakan_bahasa_asing_hidden").hide();
            $('#bahasa_yang_digunakan_bahasa_asing_hidden').find("input[type=text]").val("");
           
                }
          });

          if ($('#bahasa_yang_digunakan_bahasa_asing').is(':checked')) {
            $("#bahasa_yang_digunakan_bahasa_asing_hidden").show();                                   
                                         }
          else{
            $("#bahasa_yang_digunakan_bahasa_asing_hidden").hide();
             $('#bahasa_yang_digunakan_bahasa_asing_hidden').find("input[type=text]").val("");
               }
          //tutup bahasa yang digunakan

          //perlu penterjemah
          $("#perlu_penterjemah_lain_lain").on('change click',function(){
            if ($('#perlu_penterjemah_lain_lain').is(':checked')) {
               $("#perlu_penterjemah_lain_lain_hidden").show();                                   
                                                  }
            else{
            $("#perlu_penterjemah_lain_lain_hidden").hide();
            $('#perlu_penterjemah_lain_lain_hidden').find("input[type=text]").val("");
           
                }
          });

          if ($('#perlu_penterjemah_lain_lain').is(':checked')) {
            $("#perlu_penterjemah_lain_lain_hidden").show();                                   
                                         }
          else{
            $("#perlu_penterjemah_lain_lain_hidden").hide();
            $('#perlu_penterjemah_lain_lain_hidden').find("input[type=text]").val("");
               }
          //tutup perlu penterjemah

          //hambatan menerima edukasi
          $("#hamabatan_menerima_edukasi_lain_lain").on('change click',function(){
            if ($('#hamabatan_menerima_edukasi_lain_lain').is(':checked')) {
               $("#hamabatan_menerima_edukasi_lain_lain_hidden").show();                                   
                                                  }
            else{
            $("#hamabatan_menerima_edukasi_lain_lain_hidden").hide();
            $('#hamabatan_menerima_edukasi_lain_lain_hidden').find("input[type=text]").val("");
           
                }
          });

          if ($('#hamabatan_menerima_edukasi_lain_lain').is(':checked')) {
            $("#hamabatan_menerima_edukasi_lain_lain_hidden").show();                                   
                                         }
          else{
            $("#hamabatan_menerima_edukasi_lain_lain_hidden").hide();
            $('#hamabatan_menerima_edukasi_lain_lain_hidden').find("input[type=text]").val("");
           
               }
          //tutup hambatan menerima edukasi

          //metode edukasi
          $("#metode_edukasi_lain_lain").on('change click',function(){
            if ($('#metode_edukasi_lain_lain').is(':checked')) {
               $("#metode_edukasi_lain_lain_hidden").show();                                   
                                                  }
            else{
            $("#metode_edukasi_lain_lain_hidden").hide();
            $('#metode_edukasi_lain_lain_hidden').find("input[type=text]").val("");
           
                }
          });

          if ($('#metode_edukasi_lain_lain').is(':checked')) {
            $("#metode_edukasi_lain_lain_hidden").show();                                   
                                         }
          else{
            $("#metode_edukasi_lain_lain_hidden").hide();
            $('#metode_edukasi_lain_lain_hidden').find("input[type=text]").val("");
           
               }
          //tutup metode edukasi

            

 });
</script>