<div class="col-md-12">
							<div class="row">
								<label class="col-md-12">
									<div class="row">
										<b>PEMBERIAN EDUKASI </b>
									</div>
								</label>
						        
								<div class="col-md-12">
					            	<div class="row">
						            	<div class="col-md-12">
							            	<div class="col-md-6">
							            			
							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-2">Tanggal</label>
								            			<div class="col-md-5">
								            				<input type="text" class="form-control" name="pemberian_edukasi_tanggal_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
							            			<div class="row"> 
								            			<label class="col-md-2">Topik</label>
								            			<div class="col-md-5">
								            				<input type="text" class="form-control" name="pemberian_edukasi_topik_3">
								            			</div>
							            			</div>
							            		</div>

							            		<div class="form-group">
							            			<div class="row"> 
								            			<label class="col-md-12">Hal Yang dijelaskan</label>
								            			<div class="col-md-12">
								            				<textarea class="form-control input-lg" name="pemberian_edukasi_hal_yang_dijelaskan"></textarea>
								            			</div>
							            			</div>
							            		</div>
							            	</div>

							            	<div class="col-md-6">
							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-3">Metode</label>
								            			<div class="col-md-9">
								            				<input type="text" class="form-control" name="pemberian_edukasi_metode_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-4">Interaksi proses edukasi</label>
								            			<div class="col-md-8">
								            				<input type="text" class="form-control" name="pemberian_edukas_interaksi_proses_edukasi">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-3">Hambatan</label>
								            			<div class="col-md-4">
								            				<input type="text" class="form-control" name="pemberian_edukasi_hambatan_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-3">Yang diedukasi</label>
								            			<div class="col-md-9">
								            				<input type="text" class="form-control" name="pemberian_edukasi_yang_diedukasi_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-3">Edukator</label>
								            			<div class="col-md-9">
								            				<input type="text" class="form-control" name="pemberian_edukasi_edukator_3">
								            			</div>
								            		</div>
							            		</div>
							            	</div>

										</div>
					            	</div>
								</div>
							</div>

							<div class="row">
								<label class="col-md-12">
									<div class="row">
										<b>VERIFIKASI EDUKASI </b>
									</div>
								</label>
						        
								<div class="col-md-12">
					            	<div class="row">
						            	<div class="col-md-12">
							            	<div class="col-md-6">
							            		
							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-2">tanggal</label>
								            			<div class="col-md-5">
								            				<input type="text" class="form-control" name="verifikasi_edukasi_tanggal_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
								            		<div class="row">
								            			<label class="col-md-2">Topik</label>
								            			<div class="col-md-5">
								            				<input type="text" class="form-control" name="verifikasi_edukasi_topik_3">
								            			</div>
								            		</div>
							            		</div>

							            		<div class="form-group">
							            			<div class="row"> 
								            			<label class="col-md-3">Metode Evaluasi</label>
								            			<div class="col-md-8">

								            				<div class="form-group">
								            					<div class="row">
										            				<div class="checkbox">
										            					<input type="checkbox" class="styled" name="verifikasi_edukasi_metode_evaluasi_3">
										            					<label>Diskusi dan tanya jawab</label>
										            				</div>
								            					</div>
								            				</div>

								            				<div class="form-group">
								            					<div class="row">
										            				<div class="checkbox">
										            					<input type="checkbox" class="styled" name="verifikasi_edukasi_simulasi_3">
										            					<label>Simulasi</label>
										            				</div>
								            					</div>
								            				</div>
								            			</div>
							            			</div>
							            		</div>

							            		
							            	</div>

							            	<div class="col-md-6">
							            		<div class="col-md-12">
							            			<div class="row">
									            		<div class="form-group">
										            		<div class="row">
										            			<label class="col-md-12">Hasil Edukasi</label>
										            		</div>
									            		</div>
							            			</div>
							            		</div>

							            		<div class="col-md-12">
							            			<div class="form-group">
								            			<div class="checkbox">
								            				<input type="checkbox" class="styled" name="hasil_edukasi_1">
								            				<label>Dapat menjawab/ menjelaskan kembali dengan benar</label>
								            			</div>
								           			</div>

								           			<div class="form-group">
								            			<div class="checkbox">
								            				<input type="checkbox" class="styled" name="hasil_edukasi_2">
								            				<label>Dapat mendemonstrasikan ulang dengan benar</label>
								            			</div>
								           			</div>
							            		</div>

											</div>
										</div>
					            	</div>
								</div>

							</div>
						</div>