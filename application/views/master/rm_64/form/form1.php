						<div class="col-md-12">
							<div class="row">
								<label class="col-md-8">
									<div class="row">
										<div class="col-md-12">
											<b>PETUNJUK PENGISIAN FORM </b>
										</div>

										<div class="col-md-12">
							            	<ul>
							            		<li>
							            			Beri tanda (&#10003;) pada kotak sesuai informasi yang anda peroleh
							            		</li>
							            		<li>
							            			Beri tanda (-) pda kotak yang yang tidak di pilih
							            		</li>
							            		<li>
							            			lengkapi titik-titik yang tersedia dengan pernyataan yang sesuai
							            		</li>
							            	</ul>
										</div>
									</div>
								</label>
						        <label class="col-md-4">
									<div class="row">
										<div class="form-group">
											<label class="col-md-7">
							        			Bersedia menerima edukasi : 
											</label>
											<div class="col-md-4">
												<div class="row">
													<div class="radio">
														<div class="row">
															<div class="col-md-12">
																<input type="radio"class="styled bersedia_menerima_edukasi" id="bersedia_menerima_edukasi_ya" name="bersedia_menerima_edukasi">
																<label class="col-md-12">Ya</label>
															</div>
															<div class="col-md-12">
																<input type="radio" class="styled bersedia_menerima_edukasi" id="bersedia_menerima_edukasi_tidak" name="bersedia_menerima_edukasi">
																<label class="col-md-12">Tidak</label> 
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
						        </label>
							</div>
						</div>