						<div class="col-md-12">
							<div class="row">
								<label class="col-md-12">
									<div class="row">
										<b>PENGKAJIAN KEBUTUHAN EDUKASI</b> 
							    	</div>
								</label>

								<div class="col-md-12">
					            	<div class="row">
						            	<div class="col-md-12">
						            		<div class="row">
							        			<label class="col-md-12"><b> Informasi diperoleh dari </b></label>
						            		</div>
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="informasi_diperoleh_dari_pasien">
							            						<label>Pasien</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="informasi_diperoleh_dari_keluarga">
							            						<label>Keluarga</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-4">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-1">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" type="checkbox" id="kosong" name="informasi_diperoleh_dari_"
								                                            value="" >
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-11" id="kosong_hidden" style="display:none;">
							                                    	<div class="row">
							                                    		<input type="text" class="form-control  input-xs" >
							                                    	</div>
							                                    </div>
							                                </div>
							            				</div>
							            			</div>

							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
						            		<div class="row">
							        			<label class="col-md-12"><b> Tingkat Pendidikan </b></label>
						            		</div>
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="tingkat_pendidikan_tidak_sekolah">
							            						<label>Tidak Sekolah</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="tingkat_pendidikan_sd">
							            						<label>SD</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="tingkat_pendidikan_sltp">
							            						<label>SLTP</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="tingkat_pendidikan_slta">
							            						<label>SLTA</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="tingkat_pendidikan_pt">
							            						<label>PT</label>
							            					</div>
							            				</div>
							            			</div>

													<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-5">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" id="tingkat_pendidikan_lain_lain" type="checkbox"
								                                            value="" name="tingkat_pendidikan_lain_lain">
								                                        	<label>Lain-lain</label>
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-7" id="tingkat_pendidikan_lain_lain_hidden" style="display:none;">
							                                    	<div class="row">
							                                    		<input type="text" id="lain_lain" class="form-control  input-xs" name="tingkat_pendidikan_nama_lain_lain" >
							                                    	</div>
							                                    </div>

							                                </div>
							            				</div>
							            			</div>					            			
							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
										            		<div class="row">
											        			<label class=""><b> Baca dan Tulis </b></label>
										            		</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="baca_tulis_baik">
							            						<label>Baik</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="baca_tulis_kurang_baik">
							            						<label>Kurang Baik</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-5">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" id="baca_dan_tulis_lain_lain" type="checkbox"
								                                            value="" name="baca_tulis_lain_lain" >
								                                        	<label>Lain-lain</label>
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-7" id="baca_dan_tulis_lain_lain_hidden" style="display:none;">
							                                    	<div class="row">
							                                    		<input type="text" class="form-control  input-xs" name="baca_tulis_nama_lain_lain" >
							                                    	</div>
							                                    </div>
							                                </div>
							            				</div>
							            			</div>

							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
										            		<div class="row">
											        			<label class=""><b> Bahasa yang digunakan : </b></label>
										            		</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="bahasa_yang_digunakan_bahasa_indonesia">
							            						<label>Bahasa indonesia</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="bahasa_yang_digunakan_bahasa_daerah">
							            						<label>Bahasa Daerah</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-5">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" id="bahasa_yang_digunakan_bahasa_asing" type="checkbox"
								                                            value="" name="bahasa_yang_digunakan_bahasa_asing">
								                                        	<label>Bahasa Asing</label>
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-7" id="bahasa_yang_digunakan_bahasa_asing_hidden" style="display: none;">
							                                    	<div class="row">
							                                    		<input type="text" name="bahasa_yang_digunakan_nama_bahasa_asing" class="form-control  input-xs" >
							                                    	</div>
							                                    </div>
							                                </div>
							            				</div>
							            			</div>

							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
										            		<div class="row">
											        			<label class=""><b> Perlu penterjemah </b></label>
										            		</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="radio">
								            					<div class="col-md-6">
								            						<input type="radio" class="styled" name="perlu_penterjemah">
								            						<label class="col-md-12">Ya</label>
								            					</div>
							            						<div class="col-md-6">
								            						<input type="radio" class="styled" name="perlu_penterjemah">
								            						<label class="col-md-12">Tidak</label>
							            						</div>
							            					</div>
							            				</div>
							            			</div>

							            		
							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-5">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" id="perlu_penterjemah_lain_lain" type="checkbox"
								                                            value="" name="perlu_penterjemah_lain_lain">
								                                        	<label>Lain-lain</label>
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-7" id="perlu_penterjemah_lain_lain_hidden" style="display:none;">
							                                    	<div class="row">
							                                    		<input type="text" name="perlu_penterjemah_nama_lain_lain" class="form-control  input-xs" >
							                                    	</div>
							                                    </div>
							                                </div>
							            				</div>
							            			</div>

							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
						            		<div class="row">
							        			<label class="col-md-12"><b> Hambatan untuk menerima edukasi </b></label>
						            		</div>
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_tidak_ada">
							            						<label>Tidak ada</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_gangguan_pendengaran">
							            						<label>Gangguan pendengaran</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_keterbatasan_kognitif">
							            						<label>Keterbatasan kognitif</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_keterbatasan_fisik">
							            						<label>Keterbatasan fisik</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_usia">
							            						<label>Usia</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_emosional">
							            						<label>Emosional</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_gangguan_penglihatan">
							            						<label>Gangguan penglihatan</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_budaya_agama_kepercayaan">
							            						<label>Budaya/agama/kepercayaan</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_bahasa">
							            						<label>Bahasa</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_kurang_motivasi">
							            						<label>Kurang motivasi</label>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							            						<input type="checkbox" class="styled" name="hambatan_menerima_edukasi_gangguan_bicara">
							            						<label>Gangguan bicara</label>
							            					</div>
							            				</div>
							            			</div>

													<div class="col-md-3">
							            				<div class="form-group">
							            					<div class="checkbox">
							                                    <div class="col-md-5">
							                                    	<div class="row">
								                                        <span>
								                                            <input class="styled" id="hamabatan_menerima_edukasi_lain_lain" type="checkbox"
								                                            value="" name="hambatan_menerima_edukasi_kurang_lain_lain">
								                                        	<label>Lain-lain</label>
								                                        </span>
							                                    	</div>
							                                    </div>
							                                    <div class="col-md-7" id="hamabatan_menerima_edukasi_lain_lain_hidden" style="display:none;">
							                                    	<div class="row">
							                                    		<input type="text" class="form-control  input-xs" name="hambatan_menerima_edukasi_nama_lain_lain" >
							                                    	</div>
							                                    </div>
							                                </div>
							            				</div>
							            			</div>

							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
						            		<div class="row">
							        			<label class="col-md-12"><b> Metode edukasi </b></label>
						            		</div>
							            	
							            	<div class="col-md-12">
							            		<div class="row">
							            			<div class="col-md-6">
							            				<div class="row">
									            			<div class="col-md-6">
									            				<div class="form-group">
									            					<div class="checkbox">
									            						<input type="checkbox" class="styled" name="metode_edukasi_penjelasan_diskusi">
									            						<label>Penjelasan & diskusi</label>
									            					</div>
									            				</div>
									            			</div>

									            			<div class="col-md-6">
									            				<div class="form-group">
									            					<div class="checkbox">
									            						<input type="checkbox" class="styled" name="metode_edukasi_phamlet">
									            						<label>Phamlet</label>
									            					</div>
									            				</div>
									            			</div>

									            			<div class="col-md-6">
									            				<div class="form-group">
									            					<div class="checkbox">
									            						<input type="checkbox" class="styled" name="metode_edukasi_penjelasan_demonstrasi">
									            						<label>Demonstrasi</label>
									            					</div>
									            				</div>
									            			</div>

									            			<div class="col-md-6">
									            				<div class="form-group">
									            					<div class="checkbox">
									                                    <div class="col-md-5">
									                                    	<div class="row">
										                                        <span>
										                                            <input class="styled" id="metode_edukasi_lain_lain" type="checkbox" name="metode_edukasi_penjelasan_lain_lain" 
										                                            value="" >
										                                        	<label>Lain-lain</label>
										                                        </span>
									                                    	</div>
									                                    </div>
									                                    <div class="col-md-7" id="metode_edukasi_lain_lain_hidden" style="display:none;">
									                                    	<div class="row">
									                                    		<input type="text" name="metode_edukasi_nama_lain_lain" class="form-control  input-xs" >
									                                    	</div>
									                                    </div>
									                                </div>
									            				</div>
									            			</div>
							            				</div>
							            			</div>

							            			
																		            			
							            		</div>
							            	</div>
										</div>

										<div class="col-md-12">
						            		<div class="row">
						            			<div class="col-md-4">
						            				<div class="row">
							        					<label class="col-md-12"><b>Kebutuhan materi edukasi</b></label>
							        					<label class="col-md-12"><b>Topik</b></label>
							        					<div class="col-md-12">
								        					<ol type="1">
								        						<li>Hak dan kewajiban pasien & keluarga</li>
								        						<li>Teknik cuci tangan yang benar</li>
								        						<li>Penggunaan Alat Pelindung Diri</li>
								        						<li>Obat-obatan</li>
								        						<li>Program diet dan nutrisi</li>
								        						<li>Manajemen nyeri</li>
								        						<li>Perawatan luka</li>
								        						<li>Imunisasi</li>
								        						<li>Pemberian anti koagulan</li>
								        						<li>perawatan trakeostomi</li>
								        						<li>Penyuntikan insulin</li>
								        						<li>Pemberian obat inhalasi</li>
								        						<li>Pemberian makan lewat NGT</li>
								        						<li>Teknik Rehabilitasi</li>
								        						<li>Penggunaan alat-alat medik/khusus</li>
								        						<li>Manajemen laktasi</li>
								        						<li>Teknik memandikan bayi</li>
								        						<li>Teknik perawatan tali pusat</li>
								        						<li>Perawatan lanjutan di rumah</li>
								        						<li>Waktu kontrol dan penggunaan obat</li>
								        						<li>Tranfusi darah</li>
								        						<li>
								        							<div class="form-inline">
								        								<input type="text" class="form-control input-xs" name="-">
								        							</div>
								        						</li>
								        					</ol>
							        					</div>
						            				</div>
						            			</div>

						            			<div class="col-md-2">
						            				<div class="row">
							        					<label class="col-md-12"><b>Rencana edukasi Tanggal/jam</b></label>
							        					<div class="col-md-12">
							        						<div class="form-group">
																<textarea class="form-control input-lg" name="rencana_edukasi_tanggal_jam" style="height:460px;"></textarea>
							        						</div>
							        					</div>
						            				</div>
						            			</div>

						            			<div class="col-md-5">
						            				<div class="row">
							        					<label class="col-md-12"><b>Hasil yang diharapkan</b></label>
							        					<div class="col-md-12">

									        				<div class="form-group">
								            					<div class="checkbox">
								            						<input type="checkbox" class="styled" name="hasil_yang_diharapkan_1">
								            						<label>Dapat menjelaskan kembali materi edukasi yang sudah diterima</label>
								            					</div>
								            				</div>

								            				<div class="form-group">
								            					<div class="checkbox">
								            						<input type="checkbox" class="styled" name="hasil_yang_diharapkan_2">
								            						<label>Dapat medemonstrasikan kembali materi latihan yang sudah diterima</label>
								            					</div>
								            				</div>
							            				</div>
						            				</div>
						            			</div>

							            		<div class="col-md-12">
							            			<div class="col-md-5 pull-right">
							            				<div class="row">
							            					<div class="form-group">
									            				<label class="col-md-5">Petugas yang mengkaji</label>
									            				<div class="col-md-7">
									            					<input type="text" class="form-control input-xs" name="nama_petugas_yang_mengkaji">
									            				</div>
							            					</div>
							            				</div>
							            			</div>
							            		</div>
											</div>
						            	</div>
					            	</div>
								</div>


							</div>
						</div>