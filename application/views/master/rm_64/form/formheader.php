						    <div class="row">
					            <div class="col-md-2">	
					                <img src="img/rs.png" style="width:125px; height:125px;"> 
					            </div>
					            <div class="col-md-3">	
					                <div class="row">
						                <b>
						                    <div class="col-md-12">
						                    	<h4>
						                       		<bdo dir="rtl">RUMAH SAKIT ANANDA</bdo>
						                    	</h4>
						                    </div>
						                    
						                </b>
						                <b>
						                    <div class="col-md-10">
						                       	<div class="row">
						                       		<h4>	
							                        	<div class="col-xs-2"> B</div>
							                        	<div class="col-xs-2"> E</div>
							                        	<div class="col-xs-2"> K</div>
							                        	<div class="col-xs-2"> A</div>
							                        	<div class="col-xs-2"> S</div>
							                        	<div class="col-xs-2"> I</div>
						                        	</h4>
						                        </div>
						                    </div>
						                </b>
					                </div>
					            </div>

					            <div class="col-md-7" >
					            	<div class="row">
						            	<div class="col-md-4 pull-right">
						            		<div class="form-group">
							            		<table class="table-bordered table-xxs" >
							            			<tr>
							            				<td>RM 64/REV 01/VI/2016</td>
							            			</tr>
							            		</table>
						            		</div>
						            	</div>
						            	<div class="col-md-12">
							                <table class="table table-bordered table-xxs">
							                   	<tr>
							                        <td class="text-center text-white" bgcolor="green">
							                            <h4><b>EDUKASI PASIEN DAN KELUARGA TERINTEGRASI</b></h4>
							                        </td>
												</tr>
							                </table>
						            	</div>
						            	<div class="col-md-9 pull-right">
						            		<div class="row">
							            		<div class="col-md-12">
							            			<div class="form-group">
							            				<div class="row">
								            				<div class="col-md-5">
								            					<label>Nomor Medical Record <span class="text-danger">*</span></label>
								            				</div>
								            				<div class="col-md-7">
								            					<input type="text" name="nomor_medical_record" class="form-control input-xs">
								            				</div>
							            				</div>
							            			</div>
							            		</div>
							            		<div class="col-md-12">
							            			<div class="form-group">
							            				<div class="row">
								            				<div class="col-md-5">
								            					<label>Nama Pasien <span class="text-danger">*</span></label>
								            				</div>
								            				<div class="col-md-7">
								            					<input type="text" name="nama_pasien" class="form-control input-xs">
								            				</div>
							            				</div>
							            			</div>
							            		</div>
							            		<div class="col-md-12">
							            			<div class="form-group">
							            				<div class="row">
								            				<div class="col-md-5">
								            					<label>Tanggal lahir <span class="text-danger">*</span></label>
								            				</div>
								            				<div class="col-md-7">
								            					<input type="date" name="tanggal_lahir" class="form-control input-xs">
								            				</div>
							            				</div>
							            			</div>
							            		</div>
							            		<div class="col-md-12">
							            			<div class="form-group">
							            				<div class="row">
								            				<div class="col-md-5">
								            					<label>Diagnosa Medik <span class="text-danger">*</span></label>
								            				</div>
								            				<div class="col-md-7">
								            					<input type="text" name="diagnosa_medik" class="form-control input-xs">
								            				</div>
							            				</div>
							            			</div>
							            		</div>
						            		</div>
						            	</div>
					            	</div>
					            </div>
					        </div>
					    