	<!-- Untuk Radio dan checkbox styled -->
	<span class="switchery-warning switchery-info switchery-danger switchery-primary"></span>
   	<!-- Tutup checkbok dan Radio Styled -->
	
	<form method="POST" id="form">
		<div class="panel panel-flat">
		    <div class="panel-body">
		        <div class="text-right">
		            <button type="submit" class="btn btn-success btn-labeled btn-submit"><b><i class="icon-floppy-disk"></i></b>Simpan </button>
		            <a href="<?php echo base_url()."index.php/Crud/index" ?>" class="btn btn-default cancel-button">Kembali</a> 
		        </div>
		        <legend class="text-bold" style="margin-top:0px"></legend>
						<!-- Form PETUNJUK PENGISIAN FORM -->
						<?php include 'form/formheader.php'?>
		            			
				<legend></legend>
		        <div class="row">
		            <div class="col-md-12">

		            	<!-- Form PETUNJUK PENGISIAN FORM -->
						<?php include 'form/form1.php'?>
		                
		                <div id="bersedia_menerima_edukasi_hidden" style="display:none;">
						<!-- Form PENGKAJIAN KEBUTUHAN EDUKASI -->
						<?php include 'form/form2.php'?>
		                
						<!-- Form CATATAN -->
						<?php include 'form/form3.php'?>
		                
						<legend class="text-bold" >&nbsp;</legend>
		               		
						<!-- Form PEMBERIAN EDUKASI 1 -->
						<?php include 'form/form4.php'?>
		                
						<legend class="text-bold" >&nbsp;</legend>
		               		
						<!-- Form PEMBERIAN EDUKASI 2 -->
						<?php include 'form/form5.php'?>
		                

						<legend class="text-bold" >&nbsp;</legend>
		               		
						<!-- Form PEMBERIAN EDUKASI 3 -->
						<?php include 'form/form6.php'?>
		                						

						<legend class="text-bold" >&nbsp;</legend>
		               		
						<!-- Form PEMBERIAN EDUKASI 4 -->
						<?php include 'form/form7.php'?>
		                </div>						

		            </div>
			    </div>
			</div>

			<div class="panel-footer">
				<!-- Input Hidden -->
				<div class="text-right">
					<button type="submit" class="btn btn-success btn-labeled btn-save">
						<b><i class="icon-floppy-disk"></i></b>
						Simpan
					</button>
					<button type="button" class="btn btn-default cancel-button">
	                    Batal
	                </button>
				</div>
			</div>
		</div>
	</form>
	<script>
	$(document).ready(function(){
    $("#lain_lain").on('keyup click change mouseenter mouseout mouseup mouseleave mousedown mouseover mousemove',function(){
    	    $("#lain_lain_cb").load(window.location.href + " #lain_lain" ); 
			$("#lain_lain_cb").prop('hidden',true);
	 });
});
</script>
