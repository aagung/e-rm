 <div class="col-md-12" style="margin-top: 20px">
 	<div class="col-md-6">
 		<img src="<?php echo base_url ('assets/img/ananda.png') ?>" width="350px">
 	</div>
 	<div class="col-md-6">
 		<div class="panel">
 			<div class="panel-heading">
 				<center>
 					<h5><b>FORMULIR PELAYANAN MEDIS RAWAT JALAN</b>
 						<hr width="100%" ></h5>
 						RM - 02/Rev.0/VI/09
 					</center>
 				</div>
 			</div>
 		</div>
 	</div>
 	<form class="form-horizontal" method="post" id="form">
 		<div class="panel panel-flat">
 			<div class="panel-body ">
 				<?php include "Form_tambah/biodata.php"; ?>
 				<!-- biodata pasien -->
 				<?php include "Form_tambah/tambah_1.php"; ?>
 				<!-- jenis pelayanan medis -->
 				<?php include "Form_tambah/tambah_2.php"; ?>
 				<!-- petugas pendaftaran -->
 				<?php include "Form_tambah/tambah_3.php"; ?>
 				<!-- riwayat penyakit -->
 				<?php include "Form_tambah/tambah_4.php"; ?>
 				<!-- khusus pasien dokter gigi -->
 				<?php include "Form_tambah/tambah_5.php"; ?>
 				<!-- anjuran / saran untuk pasien -->
 			</div>
 		</div>
 		<div class="panel-footer">
 			<!-- Input Hidden -->
 			<div class="text-right">
 				<button type="submit" class="btn btn-success btn-labeled btn-save">
 					<b><i class="icon-floppy-disk"></i></b>
 					Simpan
 				</button>
 				<button type="button" class="btn btn-default cancel-button">
 					Batal
 				</button>
 			</div>
 		</div>
 	</div>
 </form>