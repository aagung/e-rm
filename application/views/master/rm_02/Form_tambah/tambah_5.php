<div class="form-group">
	<center><label class="display-block text-semibold"><h4><b>ANJURAN / SARAN UNTUK PASIEN</b></h4></label></center>
	<div class="panel-body">
		<div class="col-md-4">
			<div class="form-group">
				<div class="checkbox">
					<input class="styled" type="checkbox" name="A_dirawat" value="">
					<label class="col-md-8">DIRAWAT / RWT INAP</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox">
					<input class="styled" type="checkbox" name="A_dirujuk" value="">
					<label class="col-md-8">DIRUJUK</label>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<div class="checkbox">
					<input class="styled" type="checkbox" name="A_dikonsulkan" value="">
					<label class="col-md-8">DIKONSULKAN</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox">
					<input class="styled" type="checkbox" name="A_istirahat" value="">
					<label class="col-md-8">ISTIRAHAT</label>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<div class="checkbox">
					<input class="styled" type="checkbox" name="A_kontrol_ulang" value="">
					<label class="col-md-8">KONTROL ULANG</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="A_lain_lain" onchange="kpdgl(this.checked)" value="">
					<label class="control-label col-md-4">LAIN-LAIN</label>
					<div class="col-md-8">
						<input type="text" id="hiddenkpdgl" style="display:none" name="A_lain_lain_lbl" class="form-control" name="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>