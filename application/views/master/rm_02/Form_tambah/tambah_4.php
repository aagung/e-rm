
	<div class="form-group box">
		<center><label class="display-block text-semibold"><h4><b>KHUSUS PASIEN DOKTER GIGI</b></h4></label></center>
		<div class="panel-body">
			<div class="col-md-12">
				<div class="form-group">
					<label>1. DIAGNOSA</label>
					<textarea rows="2" cols="2" name="KPDG_diagnosa" class="form-control" placeholder=""></textarea>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_8_kiri_atas" class="styled">
						8
					</label>

					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_7_kiri_atas" class="styled">
						7
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_6_kiri_atas" class="styled">
						6
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_5_kiri_atas" class="styled">
						5
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_5_kiri_atas" class="styled">
						4
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_3_kiri_atas" class="styled">
						3
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_2_kiri_atas" class="styled">
						2
					</label>
					<label class="checkbox-inline tab">
						<input type="checkbox" name="diagnosa_1_kiri_atas" class="styled">
						1
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_1_kanan_atas" class="styled">
						1
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_2_kanan_atas" class="styled">
						2
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_3_kanan_atas" class="styled">
						3
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_4_kanan_atas" class="styled">
						4
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_5_kanan_atas" class="styled">
						5
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_6_kanan_atas" class="styled">
						6
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_7_kanan_atas" class="styled">
						7
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_8_kanan_atas" class="styled">
						8
					</label>

				</div>
				<hr width="100%" />
				<div class="form-group">
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_8_kiri_bawah" class="styled">
						8
					</label>

					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_7_kiri_bawah" class="styled">
						7
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_6_kiri_bawah" class="styled">
						6
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_5_kiri_bawah" class="styled">
						5
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_4_kiri_bawah" class="styled">
						4
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_3_kiri_bawah" class="styled">
						3
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_2_kiri_bawah" class="styled">
						2
					</label>
					<label class="checkbox-inline tab">
						<input type="checkbox" name="diagnosa_1_kiri_bawah" class="styled">
						1
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_1_kanan_bawah" class="styled">
						1
					</label>

					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_2_kanan_bawah" class="styled">
						2
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_3_kanan_bawah" class="styled">
						3
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_4_kanan_bawah" class="styled">
						4
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_5_kanan_bawah" class="styled">
						5
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_6_kanan_bawah" class="styled">
						6
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_7_kanan_bawah" class="styled">
						7
					</label>
					<label class="checkbox-inline">
						<input type="checkbox" name="diagnosa_8_kanan_bawah" class="styled">
						8
					</label>
				</div>
			</div>	
			<div class="col-md-3">
				<label>2. URAIAN TINDAKAN</label>
				<div class="form-group">
					<div class="checkbox">
						<input class="styled" name="UT_konsultasi" type="checkbox"value="">
						<label class="col-md-8">KONSULTASI</label>
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<input class="styled" name="UT_cabut_gigi" type="checkbox"value="">
						<label class="col-md-8">CABUT GIGI</label>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<label></label>
				<div class="form-group">
					<div class="checkbox">
						<input class="styled" name="UT_rontgen_gigi" type="checkbox"value="">
						<label class="col-md-8">RONTGEN GIGI</label>
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<input class="styled" name="UT_scalling" type="checkbox"value="">
						<label class="col-md-8">SCALLING</label>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<label></label>
				<div class="form-group">
					<div class="checkbox">
						<input class="styled" name="UT_perawatan_saluran_akar_gigi" type="checkbox"value="">
						<label class="col-md-8">PERAWATAN SALURAN AKAR GIGI</label>
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox no-padding">
						<input class="styled" name="UT_tambal" type="checkbox" onchange="utt(this.checked)"value="">
						<label class="control-label col-md-4">TAMBAL</label>
						<div class="col-md-7">
							<input type="text" name="UT_tambal_lbl" id="hiddenutt" style="display:none" class="form-control input-xs">
						</div>
					</div>

				</div>
			</div>
			
		</div>
	</div>