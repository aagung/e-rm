<div class="form-group box">
	<center><label class="display-block text-semibold"><h4><b>JENIS PELAYANAN MEDIS</b></h4></label></center>
	<div class="panel-body">
		<div class="col-md-6">
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_dokter_umum" value="">
					<label class="control-label col-md-5">DOKTER UMUM</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_ugd_emergency" value="">
					<label class="control-label col-md-5">UGD / EMERGENCY</label>
					<div class="col-md-7">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_dokter_gigi" value="">
					<label class="control-label col-md-5">DOKTER GIGI</label>
					<div class="col-md-7">
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_dokter_spesialis" onchange="dokterspesialis(this.checked) "value="">
					<label class="control-label col-md-5">DOKTER SPESIALIS</label>
					<div class="col-md-7">
						<input type="text" id="hiddendokter" style="display:none" name="JPM_dokter_spesialis_lbl" class="form-control  input-xs">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_bidan" onchange="bidan(this.checked)" value="">
					<label class="control-label col-md-5">BIDAN</label>
					<div class="col-md-7">
						<input type="text" id="hiddenbidan" style="display:none" name="JPM_bidan_lbl" class="form-control  input-xs">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox no-padding">
					<input class="styled" type="checkbox" name="JPM_penunjang_medis" onchange="penunjangmedis(this.checked)" value="">
					<label class="control-label col-md-5">PENUNJANG MEDIS</label>
					<div class="col-md-7">
						<input type="text" id="hiddenmedis" style="display:none" name="JPM_penunjang_medis" class="form-control  input-xs">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>