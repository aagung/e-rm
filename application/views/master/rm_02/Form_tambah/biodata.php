 				<div class="row">
 					<div class="col-md-6">
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">NAMA PASIEN</label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="nama_pasien" value="" >
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">NAMA KARYAWAN </label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="nama_karyawan" value="" >
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-lg-4 control-label input-required">TGL LAHIR / UMUR</label>
 							<div class="col-lg-6">
 								<div class="row">
 									<div class="col-md-8">
 										<input type="date" name="tgl_lahir" placeholder="" class="form-control">
 									</div>

 									<div class="col-md-4">
 										<input type="text" name="umur" placeholder="umur" class="form-control">
 									</div>
 								</div>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required ">JENIS KELAMIN</label>
 							<label class="radio-inline" style="margin-left: 10px">
 								<input type="radio" name="jenis_kelamin" class="styled" checked="checked">
 								Laki-laki
 							</label>
 							<label class="radio-inline">
 								<input type="radio" name="jenis_kelamin" class="styled">
 								Perempuan
 							</label>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">TELP. RMH / KANTOR </label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="telp_rmh" value="" >
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">TELP. HP </label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="telp_hp" value="" >
 							</div>
 						</div>  
 					</div>
 					<div class="col-md-6">
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">NAMA PERUSAHAAN</label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="nama_perusahaan" value="" >
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">NO. NIK / NIP</label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="no_nik" value="" >
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-4 control-label input-required">BAGIAN</label>
 							<div class="col-md-6">
 								<input type="text" class="form-control" name="bagian" value="" >
 							</div>
 						</div>
 						<div class="form-group box">
 							<div class="form-group">
 								<label class="col-md-4 control-label input-required">NAMA ASURANSI</label>
 								<div class="col-md-8">
 									<input type="text" class="form-control" name="nama_asuransi" value="" >
 								</div>
 							</div>
 							<div class="form-group">
 								<label class="col-md-4 control-label input-required">NO. POLIS</label>
 								<div class="col-md-8">
 									<input type="text" class="form-control" name="no_polis" value="" >
 								</div>
 							</div>
 							<div class="form-group">
 								<label class="col-md-4 control-label input-required">NO. PESERTA</label>
 								<div class="col-md-8">
 									<input type="text" class="form-control" name="no_peserta" value="" >
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>