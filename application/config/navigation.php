<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php.
 */
$config['navigation'] = array(
    'dashboard' => array(
        'uri' => 'home',
        'title' => 'Dashboard',
        'icon' => 'fa fa-dashboard',
    ),
    'user-management' => array(
        'uri' => 'auth/user',
        'title' => 'User Management',
        'icon' => 'fa fa-user',
    ),
    'acl' => array(
        'title' => 'ACL',
        'icon' => 'fa fa-unlock-alt',
        'children' => array(
            'rules' => array(
                'uri' => 'acl/rule',
                'title' => 'Rules',
            ),
            'roles' => array(
                'uri' => 'acl/role',
                'title' => 'Roles',
            ),
            'resources' => array(
                'uri' => 'acl/resource',
                'title' => 'Resources',
            ),
        ),
    ),
    'master' => array(
        'title' => 'Master',
        'icon' => 'fa fa-gear',
        'children' => array(
            'rules' => array(
                'uri' => 'master/laporan_operasi',
                'title' => 'Laporan Operasi',
            ),
            'rm_70' => array(
                 'uri' => 'master/rm_70',
                'title' => 'RM-70',
            ),
            'rm_106' => array(
                'uri' => 'master/rm_106',
                'title' => 'RM-106',
            ),
             'rm_100' => array(
                'uri' => 'master/rm_100',
                'title' => 'RM-100',
            ),
        ),
    ),
    'Form Asep' => array(
        'title' => 'Form Asep',
        'icon' => 'fa fa-user',
        'children' => array(
            'pemeriksaan_umum' => array(
                'uri' => 'master/form_pemeriksaan_fisik_umum',
                'title' => 'Form Pemeriksaan Fisik Umum',
            ),
            'rawat_jalan' => array(
                 'uri' => 'master/rm_02',
                'title' => 'RM-02',
            ),
            'asuhan_keperawatan' => array(
                 'uri' => 'master/rm_55',
                'title' => 'RM-55',
            ),
             'pemindahan_pasien_antar_ruangan' => array(
                 'uri' => 'master/rm_62',
                'title' => 'RM-62',
            ),
             'Pemulihan_pasca_anastesi' => array(
                 'uri' => 'master/no_57a',
                'title' => 'NO-57A',
            ),
             'hand_over' => array(
                 'uri' => 'master/rm_11',
                'title' => 'RM-11',
            ),
             'pengkajian_nyeri' => array(
                 'uri' => 'master/pengkajian_nyeri_pasien_icu',
                'title' => 'Pengkajian Nyeri Pasien ICU',
            ),
        ),
    ),
    'Form Alfi' => array(
        'title' => 'Form Alfi',
        'icon' => 'fa fa-user',
        'children' => array(
            'rm_56' => array(
                'uri' => 'master/rm_56',
                'title' => 'RM-56',
            ),
            'rm_58' => array(
                'uri' => 'master/rm_58',
                'title' => 'RM-58',
            ),
            'handover_ruangan' => array(
                'uri' => 'master/handover_ruangan',
                'title' => 'Handover Ruangan',
            ),
        ),
    ),
    'gudang_farmasi' => array(
        'title' => 'Gudang Farmasi',
        'icon' => 'icon-med-i-pharmacy',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'gudang_farmasi/laporan',
            ),
        ),
    ),
    'rawat-jalan' => array(
        'title' => 'Rawat jalan',
        'icon' => 'fa fa-user',
        'children' => array(
            'edukasi1' => array(
                'uri' => 'master/rm_113',
                'title' => 'PENGKAJIAN AWAL KEPERAWATAN',
            ),
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'rawat_jalan/laporan',
            ),
        ),
    ),
    'rekam-medis' => array(
        'title' => 'Rekam Medis',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'rekam_medis/laporan',
            ),
        ),
    ),
    'rawat-inap' => array(
        'title' => 'Rawat Inap',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'rawat_inap/laporan',
            ),
        ),
    ),
    'radiologi' => array(
        'title' => 'Radiologi',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'radiologi/laporan',
            ),
        ),
    ),
    'laboratorium' => array(
        'title' => 'Laboratorium',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'laboratorium/laporan',
            ),
        ),
    ),
    'keuangan' => array(
        'title' => 'Keuangan',
        'icon' => 'fa fa-money',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'keuangan/laporan',
            ),
        ),
    ),
     'aset' => array(
        'title' => 'Aset',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'aset/laporan',
            ),
        ),
    ),
     'hrd' => array(
        'title' => 'HRD',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'hrd/laporan',
            ),
        ),
    ),
      'igd' => array(
        'title' => 'IGD',
        'icon' => 'fa fa-user',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'igd/laporan',
            ),
        ),
    ),
    'Edukasi' => array(
        'title' => 'Edukasi',
        'icon' => 'icon-graduation',
        'children' => array(
            'edukasi1' => array(
                'uri' => 'master/rm_64',
                'title' => 'EDUKASI PASIEN DAN KELUARGA TERINTEGRITASI',
            ),
        ),
    ),
    'Formulir' => array(
        'title' => 'Formulir',
        'icon' => 'icon-magazine',
        'children' => array(
            'formulir1' => array(
                'uri' => 'master/formulir',
                'title' => 'Formulir',
            ),
            'formulir2' => array(
                'uri' => 'master/asuhan_keperawatan',
                'title' => 'Asuhan Keperawatan',
            ),
        ),
    ),
    'catatan-anastesi' => array(
        'title' => 'Catatan Anastesi',
        'icon' => 'fa fa-book',
        'children' => array(
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'catatan_anastesi/laporan',
            ),
        ),
    ),

    /*'utils' => array(
        'title' => 'Utils',
        'icon' => 'fa fa-wrench',
        'children' => array(
            'style_guides' => array(
                'uri' => 'utils/style_guides',
                'title' => 'Style Guides',
            ),
            'system_logs' => array(
                'uri' => 'utils/logs/system',
                'title' => 'System Logs',
            ),
            'deploy_logs' => array(
                'uri' => 'utils/logs/deploy',
                'title' => 'Deploy Logs',
            ),
            'info' => array(
                'uri' => 'utils/info',
                'title' => 'Info',
            ),
        ),
    ),*/
);
