<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends Admin_Controller {
	protected $page_title = '<i class="icon-paste"></i> Laporan Rekam Medis';
	
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	public function index()
	{
		$this->data['page_title'] = $this->page_title;
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/tables/datatables/extensions/fixed_columns.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_script('rekam-medis/laporan/script-index')
					->build('rekam-medis/laporan/index', $this->data);
	}

	public function laporan_001() {
		$this->load->view('rekam-medis/laporan/laporan-001');
	}

	public function laporan_002() {
		$this->load->view('rekam-medis/laporan/laporan-002');
	}

	public function laporan_003() {
		$this->load->view('rekam-medis/laporan/laporan-003');
	}

	public function laporan_004() {
		$this->load->view('rekam-medis/laporan/laporan-004');
	}

	public function laporan_005() {
		$this->load->view('rekam-medis/laporan/laporan-005');
	}

	public function laporan_006() {
		$this->load->view('rekam-medis/laporan/laporan-006');
	}

	public function laporan_007() {
		$this->load->view('rekam-medis/laporan/laporan-007');
	}

	public function laporan_008() {
		$this->load->view('rekam-medis/laporan/laporan-008');
	}

	public function laporan_009() {
		$this->load->view('rekam-medis/laporan/laporan-009');
	}

	public function laporan_010() {
		$this->load->view('rekam-medis/laporan/laporan-010');
	}

	public function laporan_011() {
		$this->load->view('rekam-medis/laporan/laporan-011');
	}

	public function laporan_012() {
		$this->load->view('rekam-medis/laporan/laporan-012');
	}

	public function laporan_013() {
		$this->load->view('rekam-medis/laporan/laporan-013');
	}

	public function laporan_014() {
		$this->load->view('rekam-medis/laporan/laporan-014');
	}

	public function laporan_015() {
		$this->load->view('rekam-medis/laporan/laporan-015');
	}

	public function laporan_016() {
		$this->load->view('rekam-medis/laporan/laporan-016');
	}
}