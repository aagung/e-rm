<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class No_57a extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = '<i class="icon-clipboard"></i> Kriteria Pemulihan Pasca Anastesi';
    }

    public function index()
    {
        $this->template
        ->set_js('plugins/tables/datatables/datatables.min', FALSE)
        ->set_js('plugins/notifications/bootbox.min', FALSE)
        ->set_js('plugins/notifications/sweet_alert.min', FALSE)
        ->set_js('plugins/ui/moment/moment.min', FALSE)
        ->set_js('plugins/buttons/spin.min', FALSE)
        ->set_js('plugins/buttons/ladda.min', FALSE)

        ->set_js('plugins/forms/styling/uniform.min.js', FALSE)
        ->set_js('plugins/forms/styling/switchery.min.js', FALSE)
        ->set_js('plugins/forms/styling/switch.min.js', FALSE)
        ->set_script('master/no_57a/script-form')

        ->build('master/no_57a/form', $this->data);
    }
}