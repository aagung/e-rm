<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rm_113 extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = '<i class="icon-clipboard"></i> RM 113';
    }

    public function index()
    {
        $this->template
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_js('plugins/forms/styling/switchery.min.js', FALSE)
            ->set_js('plugins/forms/styling/uniform.min.js', FALSE)
            ->set_js('plugins/sliders/ion_rangeslider.min.js', FALSE)
            ->set_js('pages/extra_sliders_ion.js', FALSE)
            ->set_js('pages/form_checkboxes_radios.js', FALSE)
            ->set_script('master/rm_113/script-form')
           

        
            ->build('master/rm_113/form', $this->data);
    }
}