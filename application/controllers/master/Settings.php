<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller {
    protected $table_def = 'sys_options';

    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = '<i class="icon-clipboard"></i> Settings';
    }

    public function index()
    {
        $this->template
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_script('master/settings/script-index')
            ->build('master/settings/index', $this->data);
    }

    public function form() {
        $id = $this->input->get('id') ? checkBase64Value($this->input->get('id')) : 0;
        if(!$id) {
            show_404();
            exit();   
        }

        $option = $this->db->where('option_id', $id)
                    ->get($this->table_def)->row();
        if(!$option) {
            show_404();
            exit();   
        }

        $this->data['option'] = $option;
        $this->template
            ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
            ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
            ->set_js('plugins/forms/validation/validate.min.js', TRUE)
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
            ->set_js('plugins/forms/styling/uniform.min', TRUE)
            ->set_js('plugins/forms/styling/switchery.min', TRUE)
            ->set_js('plugins/forms/styling/switch.min', TRUE)
            ->set_script('master/settings/script-form')
            ->build('master/settings/form', $this->data);
    }
}