<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rm_6d extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = '<i class="icon-clipboard"></i> RM 6d';
    }

    public function index()
    {
        $this->template
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            //->set_script('master/rm_6d/script-form')
            ->build('master/rm_6d/form', $this->data);
    }
}