<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends Admin_Controller {
	protected $page_title = '<i class="icon-paste"></i> Laporan Rawat Inap';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['page_title'] = $this->page_title;
		$this->template
		->set_js('plugins/tables/datatables/datatables.min', FALSE)
		->set_js('plugins/tables/datatables/extensions/fixed_columns.min', FALSE)
		->set_js('plugins/notifications/bootbox.min', FALSE)
		->set_js('plugins/notifications/sweet_alert.min', FALSE)
		->set_js('plugins/ui/moment/moment.min', FALSE)
		->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
		->set_script('rawat-inap/laporan/script-index')
		->build('rawat-inap/laporan/index', $this->data);
	}

	public function laporan_001() {
		$this->load->view('rawat-inap/laporan/laporan-001');
	}

	public function laporan_002() {
		$this->load->view('rawat-inap/laporan/laporan-002');
	}

}