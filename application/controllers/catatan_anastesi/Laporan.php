<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends Admin_Controller {
	protected $page_title = '<i class="icon-paste"></i> Laporan';
	
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	public function index()
	{
		$this->data['page_title'] = $this->page_title;
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/tables/datatables/extensions/fixed_columns.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_script('catatan-anastesi/laporan/script-index')
					->build('catatan-anastesi/laporan/index', $this->data);
	}

	public function laporan_001() {
		$this->load->view('catatan-anastesi/laporan/laporan-001');
	}
}