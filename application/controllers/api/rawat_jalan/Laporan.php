<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
    {
        parent::__construct();
        
        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
    }

      // LAPORAN 001
    public function laporan_001(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $polio_id  = $_POST['polio_id'];
        $dokter_id  = $_POST['dokter_id'];

        $aColumns = array('no', 'dokter', 'p_pagi', 'p_sore', 'p_baru', 'p_lama', 'pribadi', 'asuransi', 'perusahaan','bpjs_kes','bpjs_tk','ks-nik','rsa','pulang','dirawat','total','batal_daftar','resep_dokter','non_resep','persen_resep','resep_terjual','pasien_doa','pasien_dea','pasien_rujuk');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $polio_id  = $_GET['polio_id'];
        $dokter_id  = $_GET['dokter_id'];


        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rawat-jalan/laporan/laporan-001.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rawat-jalan/laporan/laporan-001-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 002
    public function laporan_002(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $poli_id  = $_POST['poli_id'];
        $dokter_id  = $_POST['dokter_id'];

        $aColumns = array('tanggal_daftar', 'jam_daftar', 'nama_pasien', 'no_register', 'no_mr', 'dokter', 'jaminan', 'status');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_002(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $poli_id  = $_GET['poli_id'];
        $dokter_id  = $_GET['dokter_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-002.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-002-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 003
    public function laporan_003(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $kecamatan_id  = $_POST['kecamatan_id'];
      
        $aColumns = array('no_mr', 'nama_pasien', 'nama_ibu', 'umur', 'kk_suami', 'k1', 'k4', 'usia_<_20_>_35', 'jarak_<_2_thn', 'jml_anak_>_4', 'tb_<_145_cm', 'bb_triw3_<_45_kg', 'bb_triw3_<_45_kg', 'lila_<_23.5_cm', 'hb_<_11_gr', 'faktor_resiko_lain', 'komplikasi_kehamilan', 'imunisasi_tt1_tt5', 'fe1', 'fe3');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_003(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $kecamatan_id  = $_GET['kecamatan_id'];
      
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat_jalan/laporan/laporan-003.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PELAYANAN KESEHATAN IBU HAMIL PER WILAYAH");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Pelayanan Kesehatan Ibu Hamil Per Wilayah.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PELAYANAN KESEHATAN IBU HAMIL PER WILAYAH",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-003-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Pelayanan Kesehatan Ibu Hamil Per Wilayah.pdf', "I");
                break;
        }
    }

    // LAPORAN 004
    public function laporan_004(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $kecamatan_id  = $_POST['kecamatan_id'];

        $aColumns = array('no_mr', 'nama_pasien', 'umur', 'kk_suami', 'alamat', 'tanggal_persalinan', 'komplikasi_persalinan', 'kb_pasca_bersalin', 'vit_bulan','k_nifas_1','k_nifas_2','k_nifas_3','kb_baru','kb_aktif','kb_4t','kb_alki','drop_out');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }
    
    public function print_004(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $kecamatan_id  = $_GET['kecamatan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rawat-jalan/laporan/laporan-004.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rawat-jalan/laporan/laporan-004-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 005
    public function laporan_005(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];

        $aColumns = array('no_mr', 'nama_pasien_ibu', 'umur', 'kk_suami', 'alamat', 'tanggal_lahir', 'keadaan_umum_komplikasi', 'bb_lahir', 'pb_lahir','jenis_kelamin', 'kn_1', 'kn_2', 'kn_3', 'komplikasi_neonatal_sakit', 'hb_0', 'polio_1');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_005(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-005.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-005-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 006
    public function laporan_006(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $kecamatan_id  = $_POST['kecamatan_id'];
      
        $aColumns = array('kecamatan', 'no', 'kelurahan', 'puskesmas', 'hep_bo', 'bcg', 'dpthbib_1', 'dpthbib_2', 'dpthbib_3', 'polio_1', 'polio_2', 'polio_3', 'polio_4', 'campak', 'booster_dpthib', 'booster_campak');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_006(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $kecamatan_id  = $_GET['kecamatan_id'];
      
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat_jalan/laporan/laporan-006.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "REKAP LAPORAN CAKUPAN IMUNISASI BAYI DAN BALITA");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekap Laporan Cakupan Imunisasi Bayi dan Balita.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "REKAP LAPORAN CAKUPAN IMUNISASI BAYI DAN BALITA",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-006-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rekap Laporan Cakupan Imunisasi Bayi dan Balita.pdf', "I");
                break;
        }
    }

     // LAPORAN 007
    public function laporan_007(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $kecamatan_id  = $_POST['kecamatan_id'];

        $aColumns = array('no_mr', 'nama_pasien_nama_ibu', 'umur', 'kk_suami', 'alamat', 'tanggal_lahir', 'berat_badan', 'hep_bo', 'DPTHBib1','DPTHBib2','DPTHBib3','polio_1','polio_2','polio_3','polio_4','campak','booster_DPTHBib','hep_b1','hep_b2','hep_b3','dpt_1','dpt_2','dpt_3','combo_1','combo_2','combo_3','vit_a_biru');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }
    
    public function print_007(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $kecamatan_id  = $_GET['kecamatan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rawat-jalan/laporan/laporan-007.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rawat-jalan/laporan/laporan-007-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 008
    public function laporan_008(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $poli_id  = $_POST['poli_id'];
        $dokter_id  = $_POST['dokter_id'];

        $aColumns = array('no_kartu_bpjs', 'sep', 'nama_pasien', 'no_medrec', 'alamat', 'diagnosa', 'tanggal_prb', 'no_hp','nama_dokter','asal_ppk_1');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_008(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $poli_id  = $_GET['poli_id'];
        $dokter_id  = $_GET['dokter_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-008.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-008-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 009
    public function laporan_009(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $status_id  = $_POST['status_id'];
        $paket_id  = $_POST['paket_id'];
        $jaminan_id  = $_POST['jaminan_id'];
      
        $aColumns = array('no_register', 'no_medrec', 'nama_pasien', 'alamat', 'jaminan', 'tgl_masuk', 'jam_daftar', 'jenis_paket', 'user', 'no_kwitansi');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_009(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $status_id  = $_GET['status_id'];
        $paket_id  = $_GET['paket_id'];
        $jaminan_id  = $_GET['jaminan_id'];
      
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat_jalan/laporan/laporan-009.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI HARIAN KUNJUNGAN PASIEN MCU");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Harian Kunjungan Pasien MCU.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI HARIAN KUNJUNGAN PASIEN MCU",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-009-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Harin Kunjungan Pasien MCU.pdf', "I");
                break;
        }
    }

     // LAPORAN 010
    public function laporan_010(){
      $tanggal_dari  = $_POST['tanggal_dari'];
      $tanggal_sampai  = $_POST['tanggal_sampai'];
      $dokter_id  = $_POST['dokter_id'];
      $layanan_id  = $_POST['layanan_id'];

      $aColumns = array('nama_dokter', 'jumlah_pasien', 'dirawat', 'dirujuk');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_010(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $dokter_id  = $_GET['dokter_id'];
        $layanan_id  = $_GET['layanan_id'];


        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rawat-jalan/laporan/laporan-010.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rawat-jalan/laporan/laporan-010-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 011
    public function laporan_011(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $asal_id  = $_POST['asal_id'];
        $poli_id  = $_POST['poli_id'];

        $aColumns = array('no_reg', 'no_mr', 'nama_pasien', 'dokter', 'alamat', 'jaminan', 'jenis_pasien', 'tanggal_masuk','jam_daftar','user','kasir','dirawat');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_011(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $asal_id  = $_GET['asal_id'];
        $poli_id  = $_GET['poli_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-011.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-011-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 012
    public function laporan_012(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $poli_id  = $_POST['poli_id'];
        $jaminan_id  = $_POST['jaminan_id'];
        $rujukan_id  = $_POST['rujukan_id'];
      
        $aColumns = array('nama_rujukan', 'tanggal_daftar', 'no_register', 'mr', 'nama_pasien', 'pelayanan', 'penjamin', 'keterangan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_012(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $poli_id  = $_GET['poli_id'];
        $jaminan_id  = $_GET['jaminan_id'];
        $rujukan_id  = $_GET['rujukan_id'];
      
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat_jalan/laporan/laporan-012.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PASIEN RUJUKAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Pasien Rujukan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PASIEN RUJUKAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-012-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Pasien Rujukan.pdf', "I");
                break;
        }
    }

    // LAPORAN 013
    public function laporan_013(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $poli_id  = $_POST['poli_id'];
        $dokter_id  = $_POST['dokter_id'];
        
        $aColumns = array('tanggal', 'nama_dokter', 'jadwal', 'jumlah_pasien', 'jam_datang', 'jam_pulang', 'menit_keterlambatan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_013(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $poli_id  = $_GET['poli_id'];
        $dokter_id  = $_GET['dokter_id'];
       
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat_jalan/laporan/laporan-013.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Laporan 013.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-013-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }
}