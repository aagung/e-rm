<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
    {
    	parent::__construct();

        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
    }

    // LAPORAN 001
    public function laporan_001(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        
        
        $aColumns = array('nama_pasien', 'alamat', 'asal_pasien', 'poli_ruang');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-001.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI LOKASI PASIEN");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Lokasi Pasien.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI LOKASI PASEIN",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-001-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Lokasi Pasien.pdf', "I");
            break;
        }
    }

    // LAPORAN 002
    public function laporan_002(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $dokter_id  = $_POST['dokter_id'];
        $jaminan_id  = $_POST['jaminan_id'];
        $poli_id  = $_POST['poli_id'];
        $jenis_kelamin  = $_POST['jenis_kelamin'];
        $pasien_lama_baru  = $_POST['pasien_lama_baru'];
        $wilayah_id  = $_POST['wilayah_id'];
        
        $aColumns = array('nama_pasien', 'no_rm', 'tanggal_lahir', 'tanggal_berobat', 'dokter', 'umur', 'jaminan', 'jenis_kelamin', 'pasien_lama_baru', 'poli', 'tindak_lanjut', 'total');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_002(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $dokter_id  = $_GET['dokter_id'];
        $jaminan_id  = $_GET['jaminan_id'];
        $poli_id  = $_GET['poli_id'];
        $jenis_kelamin  = $_GET['jenis_kelamin'];
        $pasien_lama_baru  = $_GET['pasien_lama_baru'];
        $wilayah_id  = $_GET['wilayah_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-002.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI KUNJUNGAN RAWAT JALAN");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Kunjungan Rawat Jalan.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI KUNJUNGAN RAWAT JALAN",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-002-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Kunjungan Rawat Jalan.pdf', "I");
            break;
        }
    }

    // LAPORAN 003
    public function laporan_003(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $asal_pasien_id  = $_POST['asal_pasien_id'];
        $poli_ruang_id  = $_POST['poli_ruang_id'];

        $aColumns = array('poli_ruang', 'icd10', 'nama_diagnosa', 'laki_laki', 'perempuan', 'jumlah');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_003(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $asal_pasien_id  = $_GET['asal_pasien_id'];
        $poli_ruang_id  = $_GET['poli_ruang_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-003.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-003-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

     // LAPORAN 004
    public function laporan_004(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $asal_pasien_id  = $_POST['asal_pasien_id'];
        $poli_ruang_id  = $_POST['poli_ruang_id'];

        $aColumns = array('no_reg', 'no_rm', 'nama', 'alamat', 'j_kel', 'umur', 'poli_ruang', 'kelas', 'tanggal_masuk','tanggal_keluar','hari_rawat','icd10','diagnosa','dpjp','dpjp_lain','spesialisasi','sts_pulang','cara_pulang','jaminan','dirujuk_ke','alasan_dirujuk','alasan_pulang_paksa','berat_lahir','panjang_lahir','data_sosial','resume_medis','inform_concern','instruksi_dokter','jenis_transfusi','cc_transfusi','catatan_perawat','kantong_darah','rujukan_dari','form_edukasi_pasien');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_004(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $asal_pasien_id  = $_GET['asal_pasien_id'];
        $poli_ruang_id  = $_GET['poli_ruang_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-004.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-004-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

      // LAPORAN 005
    public function laporan_005(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $dokter_id  = $_POST['dokter_id'];
        $asal_pasien_id  = $_POST['asal_pasien_id'];

        $aColumns = array('nama_dokter', 'jumlah_pasien', 'dirawat', 'jumlah_dipulangkan', 'rujuk');

        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_005(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $dokter_id  = $_GET['dokter_id'];
        $asal_pasien_id  = $_GET['asal_pasien_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-005.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-005-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 006
    public function laporan_006(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];

        $aColumns = array('jenis_pelayanan', 'baru', 'lama', 'jumlah');
        $aColumns = array('jenis_imunisasi', 'jumlah');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_006(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-006.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-006-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 007
    public function laporan_007(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jenis_kasus_id  = $_POST['jenis_kasus_id'];
        $jenis_kecelakaan_id  = $_POST['jenis_kecelakaan_id'];

        $aColumns = array('tindak_lanjut', 'jenis_kasus', 'jenis_kecelakaan', 'igd10', 'diagnosa', 'jumlah');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_007(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jenis_kasus_id  = $_GET['jenis_kasus_id'];
        $jenis_kecelakaan_id  = $_GET['jenis_kecelakaan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-007.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-007-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 008
    public function laporan_008(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $diagnosa_id  = $_POST['diagnosa_id'];
        $dokter_id  = $_POST['dokter_id'];

        $aColumns = array('tanggal', 'no_rm', 'nama_pasien', 'umur', 'dokter', 'igd10', 'diagnosa', 'tempat_rujuk', 'alasan_rujuk', 'keterangan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_008(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $diagnosa_id  = $_GET['diagnosa_id'];
        $dokter_id  = $_GET['dokter_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-008.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-008-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 009
    public function laporan_009(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $ruang_id  = $_POST['ruang_id'];
        $kelas_id  = $_POST['kelas_id'];

        $aColumns = array('ruang', 'jum_bed', 'pasien_awal_periode', 'umum', 'asuransi', 'bpjs_kes', 'ks_nik', 'jum_pasien_masuk', 'pindah_ruang', 'keluar_sehat', 'keluar_rujuk', 'keluar_pulang_paksa', 'keluar_mati', 'jum_pasien_keluar', 'jum_hari_rawat', 'pasien_akhir_periode');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_009(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $ruang_id  = $_GET['ruang_id'];
        $kelas_id  = $_GET['kelas_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rekam-medis/laporan/laporan-009.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rekam-medis/laporan/laporan-009-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 010
    public function laporan_010(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];

        $aColumns = array('jaminan', 'rawat_jalan', 'igd', 'rawat_inap', 'jumlah');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_010(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-010.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-010-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 011
    public function laporan_011(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];

        $aColumns = array('rujukan_dari', 'rawat_jalan', 'rawat_inap', 'jumlah');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_011(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $diagnosa_id  = $_GET['diagnosa_id'];
        $dokter_id  = $_GET['dokter_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rawat-jalan/laporan/laporan-011.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rawat-jalan/laporan/laporan-011-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 0012
    public function laporan_012(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jaminan_id  = $_POST['jaminan_id'];

        $aColumns = array('tanggal', 'no_medrec', 'nama_pasien', 'nama_pasien', 'wali_orang_tua', 'Jenis_kelamin', 'tempat_tanggal_lahir', 'agama','alamat','kota', 'kecamatan','no_hp','no_hp','perusahaan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_012(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jaminan_id  = $_GET['jaminan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-012.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-012-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

     // LAPORAN 0013
    public function laporan_013(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jenis_rawat_id  = $_POST['jenis_rawat_id'];
        $poli_ruangan_id  = $_POST['poli_ruangan_id'];

        $aColumns = array('tanggal', 'no_medrec', 'nama_pasien', 'nama_pasien', 'wali_orang_tua', 'Jenis_kelamin', 'tempat_tanggal_lahir', 'agama','alamat','kota', 'kecamatan','no_hp','no_hp','perusahaan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_013(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jenis_rawat_id  = $_GET['jenis_rawat_id'];
        $poli_ruangan_id  = $_GET['poli_ruangan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-012.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-012-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

     // LAPORAN 0014
    public function laporan_014(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $kelas_id  = $_POST['kelas_id'];
        $layanan_ruang_id  = $_POST['layanan_ruang_id'];

        $aColumns = array('tanggal', 'no_medrec', 'nama_pasien', 'nama_pasien', 'wali_orang_tua', 'Jenis_kelamin', 'tempat_tanggal_lahir', 'agama','alamat','kota', 'kecamatan','no_hp','no_hp','perusahaan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                        $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_014(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $kelas_id  = $_GET['kelas_id'];
        $layanan_ruang_id  = $_GET['layanan_ruang_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
            $tpl_filename = 'rekam-medis/laporan/laporan-012.xlsx';
            $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
            $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
            $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
            $data = array();
            $no = 1;
            $start = 6;
            $increment = 6;
            $subTotalCells = array();
            if($list['total_rows'] > 0) {
                foreach ($list['data'] as $i => $row) {
                    $row->no = $no;
                    $aRow = get_object_vars($row);

                    $no++;
                    $colnum = 0;
                    foreach ($aRow as $val) {
                        $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                        $colnum++;
                    }
                    if($i != $list['total_rows'] - 1) $increment++;
                }

                    # Row Grand Total
                $grandTotal = implode('+', $subTotalCells);
                $sheet->mergeCells("A{$increment}:H{$increment}");
                $sheet->setCellValue("A{$increment}", "TOTAL");
                $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            } else {
                $sheet->mergeCells("A{$increment}:J{$increment}");
                $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                    'font' => array(
                        'bold' => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                ));
            }
                # Apply styles
            $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                'numberformat' => array(
                    'code' => '#,##0'
                )
            ));

            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF555555'),
                    ),
                ),
            ));

            $increment++;

                # get current user
            $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
            $sheet->mergeCells("I{$increment}:J{$increment}");
            $sheet->getStyle('I'.$increment)->applyFromArray(array(
                'font' => array(
                    'size' => 8,
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                )
            ));
            $sheet->setCellValue('I'.$increment, $date_current_user);
            $sheet->setSelectedCell("A{$start}");

                # Send excel document
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            break;
            case 'pdf':
            $data = array(
                'title' => "INFORMASI PURCHASE ORDER",
                'rows' => $list['data'],
                'total_rows' => $list['total_rows'],
                'current_date' => konversi_to_id(date("d M Y")),
                'current_user' => $this->current_user,
                'periode_date' => $periode_date,
            );

            $html = $this->load->view('rekam-medis/laporan/laporan-012-pdf', $data, TRUE);

                # Create PDF
            $mpdf = new mPDF('c', 'A4-P');
            $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($html);
            $mpdf->Output('Informasi Purchase Order.pdf', "I");
            break;
        }
    }

    // LAPORAN 015
    public function laporan_015(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $dokter_id  = $_POST['dokter_id'];
        $kelas_id  = $_POST['kelas_id'];
        $layanan_ruangan_id  = $_POST['layanan_ruangan_id'];
        $pasien  = $_POST['pasien'];
        $aColumns = array('ruangan', 'kelas', 'tanggal_masuk', 'jam_masuk', 'no_register', 'no_medrec', 'nama_pasien', 'dokter');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_015(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $dokter_id  = $_GET['dokter_id'];
        $kelas_id  = $_GET['kelas_id'];
        $layanan_ruangan_id  = $_GET['layanan_ruangan_id'];
        $pasien  = $_GET['pasien'];
        
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rekam-medis/laporan/laporan-015.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PASIEN MASUK RAWAT INAP");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Pasien Masuk Rawat Inap.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PASIEN MASUK RAWAT INAP",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rekam-medis/laporan/laporan-015-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Pasien Masuk Rawat Inap.pdf', "I");
                break;
        }
    }

    // LAPORAN 016
    public function laporan_016(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jenis_tindakan  = $_POST['jenis_tindakan'];
        $aColumns = array('tanggal_operasi', 'register', 'nama_pasien', 'mr', 'sex', 'tanggal_lahir', 'umur', 'mulai_rawat','jaminan','dx_pre_operasi','konsul_operator','konsul_anastesi','dx_post_operasi','dr_operator','dr_anastesi','dr_konsulen','golongan_operasi','nama_prosedur_operasi','kd_icd9','clean','dirty','contaminated','elektif_cito','penandaan_lokasi','tanggal_operasi','rencana','mulai','selesai','delay_menit','alasan_delay_operasi','profilaksis','periksa_pa','butuh_darah','jns_darah','kantong_darah','jns_anastesi','sign_in','during','sign_out','time_out','ruang_pre_op','ruang_post_op','lokasi_operasi');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_016(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jenis_tindakan  = $_GET['jenis_tindakan'];
       
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'rekam-medis/laporan/laporan-016.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI DETAIL TINDAKAN DAN VK");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Detail Tindakan dan VK.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI DETAIL TINDAKAN DAN VK",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('rekam-medis/laporan/laporan-016-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Detail Tindakan dan VK.pdf', "I");
                break;
        }
    }
}