<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
  	{
    	parent::__construct();
        
        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
  	}

    // LAPORAN 001
    public function laporan_001(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jaminan_id  = $_POST['jaminan_id'];
        $pemeriksaan_id  = $_POST['pemeriksaan_id'];

        $aColumns = array('no', 'no_foto', 'tanggal', 'no_rm', 'nama_pasien', 'jaminan', 'nama_pemeriksaan', 'sub_total', 'user_ro');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jaminan_id  = $_GET['jaminan_id'];
        $pemeriksaan_id  = $_GET['pemeriksaan_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-001.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

      // LAPORAN 002
    public function laporan_002(){
        $bulan_id  = $_POST['bulan_id'];
        $tahun_id  = $_POST['tahun_id'];
        

        $aColumns = array('jaminan_pemeriksaan', 'target_tahunan', 'target_bulanan', 'sep_18', 'okt_18', 'prosentase');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_002(){
        $mode = $this->input->get("d");

        $bulan_id  = $_GET['bulan_id'];
        $tahun_id  = $_GET['tahun_id'];
        

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($bulan == $tahun) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)))." s/d ".konversi_to_id(date("d M Y", strtotime($tahun)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-002.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-002-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

      // LAPORAN 003
    public function laporan_003(){
        $bulan_id  = $_POST['bulan_id'];
        $tahun_id  = $_POST['tahun_id'];
        

        $aColumns = array('jaminan_pemeriksaan', 'target_tahunan', 'target_bulanan', 'sep_18', 'okt_18', 'prosentase');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_003(){
        $mode = $this->input->get("d");

        $bulan_id  = $_GET['bulan_id'];
        $tahun_id  = $_GET['tahun_id'];
        

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($bulan == $tahun) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)))." s/d ".konversi_to_id(date("d M Y", strtotime($tahun)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-003.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-003-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

     // LAPORAN 004
    public function laporan_004(){
        $bulan_id  = $_POST['bulan_id'];
        $tahun_id  = $_POST['tahun_id'];
        

        $aColumns = array('jaminan_pemeriksaan', 'umum', 'bpjs', 'jamkesda', 'asurasi_lain', 'total');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_004(){
        $mode = $this->input->get("d");

        $bulan_id  = $_GET['bulan_id'];
        $tahun_id  = $_GET['tahun_id'];
        

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($bulan == $tahun) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($bulan)))." s/d ".konversi_to_id(date("d M Y", strtotime($tahun)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-004.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI PURCHASE ORDER");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Purchase Order.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI PURCHASE ORDER",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-004-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Purchase Order.pdf', "I");
                break;
        }
    }

    // LAPORAN 005
    public function laporan_005(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $status_baca  = $_POST['status_baca'];
        
        $aColumns = array('tanggal', 'pemeriksaan', 'no_foto', 'no_hasil', 'register', 'nama
                          ', 'nama_dokter', 'dokter_pengirim', 'dokter_pembaca','asal_pasien','tanggal_daftar','jam','tanggal_baca','jam_baca','waktu_tunggu');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_005(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $status_baca  = $_GET['status_baca'];
       
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-005.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI HISTORY PASIEN RADIOLOGI");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi History Pasien Radiologi.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI HISTORY PASIEN RADIOLOGI",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-005-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi History Pasien Radiologi.pdf', "I");
                break;
        }
    }

    // LAPORAN 006
    public function laporan_006(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        
        $aColumns = array('no', 'jenis_pemeriksaan', 'hari_1', 'hari_2', 'hari_3', 'hari_4', 'hari_5', 'hari_6', 'hari_7','hari_8','hari_9','hari_10','hari_11','hari_12','hari_13','hari_14','hari_15','hari_16','hari_17','hari_18','hari_19','hari_20','hari_21','hari_22','hari_23','hari_24','hari_25','hari_26','hari_27','hari_28','hari_29','hari_30','hari_31','total');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_006(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
       
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'radiologi/laporan/laporan-006.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "STATISTIK JUMLAH PEMERIKSAAN RADIOLOGI");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Statistik Jumlah Pemeriksaan Radiologi.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "STATISTIK JUMLAH PEMERIKSAAN RADIOLOGI",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('radiologi/laporan/laporan-006-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Statistik Jumlah Pemeriksaan Radiologi.pdf', "I");
                break;
        }
    }

}