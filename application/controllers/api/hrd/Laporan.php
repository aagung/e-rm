<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
  	{
    	parent::__construct();
        
        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
  	}

    // LAPORAN 001
    public function laporan_001(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $unit_id  = $_POST['unit_id'];
        $bagian_id  = $_POST['bagian_id'];

        $aColumns = array('no', 'nik', 'nama_pegawai', 'alamat', 'telepon_hp', 'tempat_lahir', 'tanggal_lahir', 'umur', 'unit', 'bagian', 'tanggal_masuk', 'masa_kerja', 'no_ktp', 'no_bpjs_ket', 'no_bpjs_kes', 'no_asuransi', 'pendidikan_terakhir', 'jabatan', 'status_pegawai');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");
        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $unit_id  = $_GET['unit_id'];
        $bagian_id  = $_GET['bagian_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-001.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI KARYAWAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Karyawan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI KARYAWAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Karyawan.pdf', "I");
                break;
        }
    }

    // LAPORAN 002
    public function laporan_002(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $punishment_id  = $_POST['punishment_id'];
        $masa_berlaku_punishment  = $_POST['masa_berlaku_punishment'];
        $unit_kerja_id  = $_POST['unit_kerja_id'];
        $bagian_id  = $_POST['bagian_id'];

        
        $aColumns = array('nik', 'nama_pegawai', 'alamat', 'telepon_hp', 'tempat_lahir', 'tanggal_lahir', 'umur', 'unit_kerja', 'bagian','jabatan', 'tanggal_masuk', 'masa_kerja','perkiraan_tahun_pensiun', 'no_ktp', 'no_str', 'masa_berlaku_str', 'no_bpjs_kesehatan', 'no_bpjs_ketenagakerjaan', 'pendidikan_terakhir', 'punishment', 'masa_berlaku_punishment', 'status_pegawai');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_002(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $punishment_id  = $_GET['punishment_id'];
        $masa_berlaku_punishment  = $_GET['masa_berlaku_punishment'];
        $unit_kerja_id  = $_GET['unit_kerja_id'];
        $bagian_id  = $_GET['bagian_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-002.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "REKAP LAPORAN KARYAWAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekap Laporan Karyawan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "REKAP LAPORAN KARYAWAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-002-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rekap Laporan Karyawan.pdf', "I");
                break;
        }
    }

    // LAPORAN 003
    public function laporan_003(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jenis_pelanggaran_id  = $_POST['jenis_pelanggaran_id'];
        $sanksi_id  = $_POST['sanksi_id'];

        $aColumns = array('nama_pegawai', 'nik', 'jenis_pelanggaran', 'deskripsi_pelanggaran', 'sanksi', 'tanggal_sp', 'dikeluarkan_oleh');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_003(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jenis_pelanggaran_id  = $_GET['jenis_pelanggaran_id'];
        $sanksi_id  = $_GET['sanksi_id'];
     
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-003.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "INFORMASI RIWAYAT PUNISHMENT");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Informasi Riwayat Punishment.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "INFORMASI RIWAYAT PUNISHMENT",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-003-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Informasi Riwayat Punishment.pdf', "I");
                break;
        }
    }

    // LAPORAN 004
    public function laporan_004(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $jenis_pelanggaran_id  = $_POST['jenis_pelanggaran_id'];
        $sanksi_id  = $_POST['sanksi_id'];
     
        $aColumns = array('nik', 'nama_pegawai', 'unit_kerja', 'bagian', 'jabatan', 'kategori_penghargaan', 'no_sk', 'tanggal_sk', 'bentuk_penghargaan', 'jenis_pelatihan', 'nama_pelatihan', 'tempat_latihan', 'masa_berlaku_sertifikat', 'nilai_skp', 'jam_pelatihan', 'penyelanggara', 'akreditasi_dari', 'no_sertifikat', 'nama_institusi_pendidikan', 'jenjang_pendidikan', 'jurusan_prodi', 'tahun_lulus', 'no_ijazah', 'direktur_pendidikan', 'no_surat_verifikasi', 'jenjang_karir', 'no_sk_2', 'tanggal_sk_2', 'nama_suami_istri', 'tanggal_lahir_suami_istri', 'nama_anak', 'tanggal_lahir_anak', 'umur_anak', 'jenis_kelamin_anak', 'jenis_pelanggaran', 'deskripsi_pelanggaran', 'sanksi', 'tanggal_sp');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_004(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $jenis_pelanggaran_id  = $_GET['jenis_pelanggaran_id'];
        $sanksi_id  = $_GET['sanksi_id'];
     
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-004.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "RINCIAN LAPORAN KARYAWAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rincian Laporan Karyawan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "RINCIAN LAPORAN KARYAWAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-004-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rincian Laporan Karyawan.pdf', "I");
                break;
        }
    }

    // LAPORAN 005
    public function laporan_005(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $cuti_tahunan_terambil  = $_POST['cuti_tahunan_terambil'];
        $sisa_cuti_tahunan  = $_POST['sisa_cuti_tahunan'];
     
        $aColumns = array('nik', 'nama_pegawai', 'tanggal_masuk', 'januari', 'febuari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'september', 'oktober', 'november', 'desember', 'cuti_berambil', 'cuti_tahunan_terambil', 'sisa_cuti_tahunan', 'status', 'bagian', 'jabatan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_005(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $cuti_tahunan_terambil  = $_GET['cuti_tahunan_terambil'];
        $sisa_cuti_tahunan  = $_GET['sisa_cuti_tahunan'];
     
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-005.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "REKAPITULASI CUTI KARYAWAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekapitulasi Cuti Karyawan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "REKAPITULASI CUTI KARYAWAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-005-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rekapitulasi Cuti Karyawan.pdf', "I");
                break;
        }
    }

    // LAPORAN 006
    public function laporan_006(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $cuti_tahunan_terambil  = $_POST['cuti_tahunan_terambil'];
        $sisa_cuti_tahunan  = $_POST['sisa_cuti_tahunan'];
     
        $aColumns = array('nik', 'nama_pegawai', 'unit_kerja', 'bagian', 'jabatan', 'tanggal_masuk', 'absensi_keterlambatan_karyawan_januari', 'absensi_keterlambatan_karyawan_febuari', 'absensi_keterlambatan_karyawan_maret', 'absensi_keterlambatan_karyawan_april', 'absensi_keterlambatan_karyawan_mei', 'absensi_keterlambatan_karyawan_juni', 'absensi_keterlambatan_karyawan_juli', 'absensi_keterlambatan_karyawan_agustus', 'absensi_keterlambatan_karyawan_september', 'absensi_keterlambatan_karyawan_oktober', 'absensi_keterlambatan_karyawan_november', 'absensi_keterlambatan_karyawan_desember', 'total_keterlambatan', 'tidak_masuk_kerja', 'jenis_cuti', 'hak_cuti', 'cuti_tahunan_terambil_januari', 'cuti_tahunan_terambil_febuari', 'cuti_tahunan_terambil_maret', 'cuti_tahunan_terambil_april', 'cuti_tahunan_terambil_mei', 'cuti_tahunan_terambil_juni', 'cuti_tahunan_terambil_juli', 'cuti_tahunan_terambil_agustus', 'cuti_tahunan_terambil_september', 'cuti_tahunan_terambil_oktober', 'cuti_tahunan_terambil_november', 'cuti_tahunan_terambil_desember', 'sisa_cuti_tahunan');
        
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $rResult = array();
        $iFilteredTotal = 0;
        $iTotal = 0;

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_006(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $cuti_tahunan_terambil  = $_GET['cuti_tahunan_terambil'];
        $sisa_cuti_tahunan  = $_GET['sisa_cuti_tahunan'];
     
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = array();
        $list['data'] = array();
        $list['total_rows'] = 0;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'hrd/laporan/laporan-006.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', $this->config->item('rs_nama') ? : "RUMAH SAKIT ANANDA");
                $sheet->setCellValue('A2', "REKAP CUTI KARYAWAN");
                $sheet->setCellValue('A3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 6;
                $increment = 6;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->no = $no;
                        $aRow = get_object_vars($row);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                    
                    # Row Grand Total
                    $grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:J{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':J'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:J$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':J' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:J{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekap Cuti Karyawan.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "REKAP CUTI KARYAWAN",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('hrd/laporan/laporan-006-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new mPDF('c', 'A4-P');
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rekap Cuti Karyawan.pdf', "I");
                break;
        }
    }

}