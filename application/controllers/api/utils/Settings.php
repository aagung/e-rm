<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller 
{
    protected $table_def_settings = "m_settings";
    protected $table_def_unit_usaha = "m_unitusaha";

    protected $table_def_recent = "m_recent";

    function __construct()
    {
        parent::__construct();

        $this->load->model('master/Settings_model');
    }

    public function load_data() 
    {
        $query = $this->db->order_by('kode', 'asc')->get($this->table_def_unit_usaha);
        $unit_usaha = array();
        foreach ($query->result() as $row) {
            $unit_usaha[$row->kode] = $row;
        }

        $aColumns = array('icon', 'unit', 'hostname', 'status', 'action');

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "ORDER BY ".$this->table_def_settings.".key ASC";

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = $this->table_def_settings.".key LIKE 'db_%'";
        $aWheres[] = $this->table_def_settings.".type = '". $this->Settings_model::TYPE_PENGADAAN ."'";

        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->Settings_model->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $unit = array_key_exists(substr($obj->key, 3), $unit_usaha) ? $unit_usaha[substr($obj->key, 3)] : null;

            $obj->unit = $unit ? $unit->nama : strtoupper(substr($obj->key, 3));
            $obj->status = 0;

            // Check Connection
            if (isset($obj->value->hostname)) {
                $obj->connected = check_database_connection($obj->value);
            } else {
                $obj->connected = false;
            }

            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function get_data() {
        if (! $this->input->is_ajax_request()) 
            exit();

        $uid = $this->input->get('uid');

        $setting = $this->Settings_model->get_by("WHERE ".$this->table_def_settings.".uid = '".$uid."'");

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $setting]));
    }

    public function check_connection() {
        if (! $this->input->is_ajax_request())
            exit();

        $setting = new stdClass();
        $setting->driver = $this->input->post('driver') == 'mysqli' ? 'mysql' : $this->input->post('driver');
        $setting->hostname = $this->input->post('hostname');
        $setting->port = $this->input->post('port') ? $this->input->post('port') : '3306';
        $setting->username = $this->input->post('username');
        $setting->password = $this->input->post('password');
        $setting->database = $this->input->post('database');

        $connected = check_database_connection($setting);

        if ($connected) {
            $this->output->set_status_header(200)
                    ->set_output(json_encode(['message' => 'Connected to database.']));
        } else {
            $this->output->set_status_header(503)
                    ->set_output(json_encode(['code' => $ex->getCode(), 'message' => $ex->getMessage()]));
        }
    }

    public function save()
    {
        if (! $this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();

        if (! isset($obj->uid) && empty($obj->uid)) {
            $this->output->set_status_header(404);
        } else {
            $uid = $obj->uid;

            unset($obj->id);
            unset($obj->uid);

            $data = [
                'value' => json_encode($obj)
            ];

            if (! ($object = $this->Settings_model->updateByUid($uid, $data)) ) {
                $this->output->set_status_header(500)
                    ->set_output(json_encode(['message' => 'Terjadi kesalahan ketika menyimpan ke database']));
            } else {
                $this->output->set_status_header(200)
                    ->set_output(json_encode($object));
            }
        }
    }

    public function save_rkap() {
        if (! $this->input->is_ajax_request())
            exit();

        // RKAP IT
        $rkapIt = new stdClass();
        $rkapIt->uid = $this->input->post('rkap_it_uid');
        $rkapIt->value = $this->input->post('rkap_it_id');

        // RKAP Medis
        $rkapMedis = new stdClass();
        $rkapMedis->uid = $this->input->post('rkap_medis_uid');
        $rkapMedis->value = $this->input->post('rkap_medis_id');

        // RKAP Logistik
        $rkapLogistik = new stdClass();
        $rkapLogistik->uid = $this->input->post('rkap_logistik_uid');
        $rkapLogistik->value = $this->input->post('rkap_logistik_id');

        // RKAP Jasa
        $rkapJasa = new stdClass();
        $rkapJasa->uid = $this->input->post('rkap_jasa_uid');
        $rkapJasa->value = $this->input->post('rkap_jasa_id');


        // Save to Database
        $this->db->trans_start();

        // Save RKAP IT
        if ($rkapIt->uid != 0 && !empty($rkapIt) && $rkapIt != "0") { // Update
            $this->db->where('uid', $rkapIt->uid);
            $this->db->set('value', $rkapIt->value);
            $this->db->set('update_at', date('Y-m-d H:i:s'));
            $this->db->set('update_by', $this->session->userdata('auth_user'));
            $this->db->update($this->table_def_settings);
        } else { // Create
            $data = array(
                'type' => $this->Settings_model::TYPE_PENGADAAN,
                'datatype' => $this->Settings_model::DATATYPE_NUMBER,
                'key' => 'rkap_it_id',
                'value' => $rkapIt->value,
                'deskripsi' => 'RKAP yang digunakan untuk Pengadaan IT',
                'created_by' => $this->session->userdata('auth_user'),
                'created_at' => date('Y-m-d H:i:s'),
                'update_by' => $this->session->userdata('auth_user'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $this->db->set('uid', 'UUID()', FALSE);
            $this->db->insert($this->table_def_settings, $data);
        }

        // Save RKAP Medis
        if ($rkapMedis->uid != 0 && !empty($rkapMedis) && $rkapMedis != "0") { // Update
            $this->db->where('uid', $rkapMedis->uid);
            $this->db->set('value', $rkapMedis->value);
            $this->db->set('update_at', date('Y-m-d H:i:s'));
            $this->db->set('update_by', $this->session->userdata('auth_user'));
            $this->db->update($this->table_def_settings);
        } else { // Create
            $data = array(
                'type' => $this->Settings_model::TYPE_PENGADAAN,
                'datatype' => $this->Settings_model::DATATYPE_NUMBER,
                'key' => 'rkap_medis_id',
                'value' => $rkapMedis->value,
                'deskripsi' => 'RKAP yang digunakan untuk Pengadaan Medis',
                'created_by' => $this->session->userdata('auth_user'),
                'created_at' => date('Y-m-d H:i:s'),
                'update_by' => $this->session->userdata('auth_user'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $this->db->set('uid', 'UUID()', FALSE);
            $this->db->insert($this->table_def_settings, $data);
        }

        // Save RKAP Logistik
        if ($rkapLogistik->uid != 0 && !empty($rkapLogistik) && $rkapLogistik != "0") { // Update
            $this->db->where('uid', $rkapLogistik->uid);
            $this->db->set('value', $rkapLogistik->value);
            $this->db->set('update_at', date('Y-m-d H:i:s'));
            $this->db->set('update_by', $this->session->userdata('auth_user'));
            $this->db->update($this->table_def_settings);
        } else { // Create
            $data = array(
                'type' => $this->Settings_model::TYPE_PENGADAAN,
                'datatype' => $this->Settings_model::DATATYPE_NUMBER,
                'key' => 'rkap_logistik_id',
                'value' => $rkapLogistik->value,
                'deskripsi' => 'RKAP yang digunakan untuk Pengadaan Logistik',
                'created_by' => $this->session->userdata('auth_user'),
                'created_at' => date('Y-m-d H:i:s'),
                'update_by' => $this->session->userdata('auth_user'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $this->db->set('uid', 'UUID()', FALSE);
            $this->db->insert($this->table_def_settings, $data);
        }

        // Save RKAP Jasa
        if ($rkapJasa->uid != 0 && !empty($rkapJasa) && $rkapJasa != "0") { // Update
            $this->db->where('uid', $rkapJasa->uid);
            $this->db->set('value', $rkapJasa->value);
            $this->db->set('update_at', date('Y-m-d H:i:s'));
            $this->db->set('update_by', $this->session->userdata('auth_user'));
            $this->db->update($this->table_def_settings);
        } else { // Create
            $data = array(
                'type' => $this->Settings_model::TYPE_PENGADAAN,
                'datatype' => $this->Settings_model::DATATYPE_NUMBER,
                'key' => 'rkap_jasa_id',
                'value' => $rkapJasa->value,
                'deskripsi' => 'RKAP yang digunakan untuk Pengadaan Jasa',
                'created_by' => $this->session->userdata('auth_user'),
                'created_at' => date('Y-m-d H:i:s'),
                'update_by' => $this->session->userdata('auth_user'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $this->db->set('uid', 'UUID()', FALSE);
            $this->db->insert($this->table_def_settings, $data);
        }


        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $this->output->set_status_header(200)
                    ->set_output(json_encode(['message' => 'Data berhasil disimpan']));
        } else {
            $this->db->trans_rollback();
            $this->output->set_status_header(500)
                    ->set_output(json_encode(['message' => 'Terjadi kesalahan ketika menyimpan ke database']));
        }
    }

    private function _getDataObject()
    {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;
        $obj->driver = $this->input->post('driver');
        $obj->hostname = $this->input->post('hostname');
        $obj->port = $this->input->post('port');
        $obj->username = $this->input->post('username');
        $obj->password = $this->input->post('password');
        $obj->database = $this->input->post('database');

        return $obj;
    }

    # Tambahan Agung untuk setting maks. biaya pengadaan langsung
    public function saveMaksBiaya()
    {
        if (! $this->input->is_ajax_request())
            exit();

        $obj = new stdClass();
        $obj->uid = $this->input->post('biaya_uid') && ($this->input->post('biaya_uid') != "") ? $this->input->post('biaya_uid') : "";
        $obj->key = "maksimal_biaya_pengadaan_langsung";
        $obj->value = $this->input->post('biaya');
        $obj->deskripsi = "Setting untuk biaya maksimal pengadaan langsung";
        $obj->type = 2; // Type Pengadaan
        $obj->datatype = 3; // Type Float

        if ($obj->uid == "") {
            if (! ($object = $this->Settings_model->create($obj)) ) {
                $this->output->set_status_header(500)
                    ->set_output(json_encode(['message' => 'Terjadi kesalahan ketika menyimpan ke database']));
            } else {
                $this->output->set_status_header(200)
                    ->set_output(json_encode($object));
            }
        } else {
            $uid = $obj->uid;
            $data = get_object_vars($obj);
            if (! ($object = $this->Settings_model->updateByUid($uid, $data)) ) {
                $this->output->set_status_header(500)
                    ->set_output(json_encode(['message' => 'Terjadi kesalahan ketika menyimpan ke database']));
            } else {
                $this->output->set_status_header(200)
                    ->set_output(json_encode($object));
            }
        }
    }

}