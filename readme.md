# ![Logo](assets/img/logo/imedis.png) Sistem Informasi Manajemen E-RM

## Instalasi
Pastikan [Composer](https://getcomposer.org) dan [Bower](https://bower.io) telah terinstall di komputer, sebelum 
menggunakan CI-Beam.

Lakukan langkah-langkah berikut untuk melakukan instalasi CI-Beam:

1. Download CI-Beam ke direktori tujuan.
2. Pindah ke direktori CI-Beam.
3. Jalankan `composer install`.
4. Pindah ke direktori `assets`.
5. Jalankan `bower update`.

## Environment
Untuk pengerjaan di lokal, pastikan Anda membuat folder `development` di `application/config`.
Sebagai contoh untuk menset konfigurasi database di lokal, buat file `application/config/development/database.php`.
File konfigurasi di folder `application/config/development` akan menimpa konfigurasi di `application/config`.

###Catatan
CodeIgniter selalu me-load file config global terlebih dahulu (yaitu yang ada di `application/config/`), kemudian ia akan mencoba me-load file-file konfigurasi untuk environment aktif. Hal ini berarti Anda tidak perlu menimpa semua file konfigurasi dalam folder environment. Hanya file-file yang berubah saja yang perlu dibuat. Anda juga tidak perlu mengkopi semua item config ke dalam file config environment. Hanya item-item config yang ingin diubah saja yang perlu dimasukkan dalam file environment Anda. Item-item config yang dideklarasikan dalam folder environment akan selalu menimpa item-item yang terdapat dalam file-file config global.

Berikut lokasi di mana modul-modul lain bebas digunakan:

* `application/controllers`

	Pastikan dibuat folder unik untuk menyimpan file-file controller.  
	**Contoh**: controller `Rawat_jalan.php` modul Pendaftaran harus membuat file di `application/controllers/pendaftaran/Rawat_jalan.php`. 

* `application/controllers/api`
	
	Direktori ini dibuat khusus untuk pemanggilan melalui ajax. Umumnya digunakan untuk memanggil layanan API di `http://api.ptrsbt.com/`.  
	Pastikan dibuat folder unik untuk menyimpan file-file controller api.  
	**Contoh**: controller `Kunjungan.php` modul Pendaftaran harus membuat file di `application/controllers/api/pendaftaran/Kunjungan.php`. 

* `application/views`

	Pastikan dibuat folder unik untuk menyimpan file-file view.  
	**Contoh**: view `index.php` yang digunakan oleh controller `Rawat_jalan.php` modul Pendaftaran harus membuat file di `application/views/pendaftaran/rawat_jalan/index.php`. 

* `application/models`

	Pastikan dibuat folder unik untuk menyimpan file-file model.  
	**Contoh**: model `model_kunjungan.php` modul Pendaftaran harus membuat file di `application/models/pendaftaran/model_kunjungan.php`. 

* `application/database`

	Ini adalah lokasi tempat menyimpan *initial database dump* (dump database awal). File *default* `ptrsbt_app.sql` disimpan di sini, berisi struktur dan data tabel **User** dan **ACL**. Pastikan jika ada perubahan struktur/data pada tabel-tabel ini beritahukan kepada **Admin** untuk memastikan perubahan tersebut di-share ke modul-modul lain. Jika file ini berubah, dan perubahan tersebut telah di *merge* ke repository modul, import kembali di database lokal agar perubahan data bisa terlihat di lokal.

	Dump database khusus untuk modul bisa disimpan di sini juga, beri nama file sesuai dengan nama modulnya. Pastikan update database selalu tercermin pada file ini. Pastikan **tidak ada** perintah untuk membuat atau menggunakan database khusus seperti `CREATE DATABASE` atau `USE DATABASE`.  
	**Contoh**: dump database untuk modul Pendaftaran diberi nama dengan `application/database/ptrsbt_app_pendaftaran.sql`.  
	Jika modul dikerjakan oleh beberapa orang, pertimbangkan untuk menggunakan teknik **migration** sehingga tidak menghapus seluruh data yang sudah ada.

###Catatan
**Pastikan** pada saat melakukan perubahan **diluar** daftar lokasi di atas, beritahukan admin untuk memastikan *source* tersebut dimasukkan ke `ptrsbt-app-template` dan dapat di-share ke modul-modul lain.
