$(function(){
    var tanggal = false;
    var the_table;
    var change_tgl = false;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitusaha  = $('#unitusaha');
    $cari       = $('#cari');
    $btncari       = $('#btn-cari');
    $departemen = $('#departemen');
    $field      = $('#field');

    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        tanggal = true;
        console.log('jalan');
        tgl_mulai   = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir   = picker.endDate.format('YYYY-MM-DD');
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        field       = $field.val();  
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field);
    });

    $unitusaha.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        field       = $field.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field);
    });

    $cari.on('keyup',function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        field       = $field.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field);
        // $(document).find('#dataTable_pegawai_filter input[type=search]').val(cari);
    });

    $btncari.click(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        field       = $field.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field);
        // $(document).find('#dataTable_pegawai_filter input[type=search]').val(cari);
    });

    $departemen.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        field       = $field.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field);
    });
    var unitusaha = $unitusaha.val();
    console.log(unitusaha);
    get_data('','','',unitusaha);
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen,field){
    console.log('unitusaha : ',unitusaha);
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_pegawai").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/pegawai',
            "type": "POST",
            "dataType":"json",
            "data": {cari:cari,tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitusaha:unitusaha,departemen:departemen,field:field}
        },
        "order": [[ 0, "desc" ]],
        "columns": [
            {
                "data": "nik",
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<a href="'+base_url+'/hrd/pegawai/edit/' + row.uid + '">' + data + '</a>';
                }
            },
            {
                "data": "tgl_diangkat",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "unitkerja_nama"
            },
            {
                "data": "jabatan_nama"
            },
            {
                "data": "nik",
                "render": function(data, type, row, meta) {
                    return '<center><a href="'+base_url+'/hrd/pegawai/delete/' + row.uid + '" data-button="delete" class="text-danger"><i class="fa fa-trash-o"></i></a><center>';
                },
                'sortable':false,
                'searchable':false
            },

        ]
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {

    } ).draw();*/
}
