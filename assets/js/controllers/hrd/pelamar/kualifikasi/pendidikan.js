$formPendidikan     = $('#form-pendidikan');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalPendidikan    = $('#add_pendidikan_modal');

$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $formPendidikan.validate({
        rules: {
            "tahun_masuk": {
                required: "required",
                onlyNumber: true,
            },
            "tahun_lulus": {
                required: "required",
                onlyNumber: true,
            }
        },
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_pendidikan/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_pendidikan(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Pendidikan.getPendidikan();
                $modalPendidikan.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formPendidikan[0].reset();
            });
            return false;
        }
    });
});

var Pendidikan = {
    getPendidikan: function() {
        var page_url    = '/api/hrd/rekrutment/pelamar/get_pendidikan/'+pelamar_id;
        var columns     = [
            {
                "data": "pendidikan_nama",
            },
            {
                "data": "tahun_masuk",
            },
            {
                "data": "tahun_lulus",
            },
            {
                "data": "gpa",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_pendidikan/' + row.uid + '" onClick="Pendidikan.deletePendidikan(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-pendidikan',columns);
    },
    deletePendidikan: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Pendidikan.getPendidikan();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahPendidikan: function(){
        $modalPendidikan.modal('show');
    }
}
