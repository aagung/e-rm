/**
 * Created by agungrizkyana on 10/5/16.
 */
/**
 * Modal Reservasi OK
 * @type {string[]}
 */
app.controller('ModalReservasiOKController', modalReservasiOKController);
modalReservasiOKController.$inject = [
    '$rootScope',
    '$scope',
    '$http',
    '$uibModalInstance',
    '$log',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'DTColumnDefBuilder',
    '$compile',
    'items',
    '$httpParamSerializer',
    'CONFIG',
    'toastr',
    '$timeout',
    'EVENT'
];
function modalReservasiOKController($rootScope,
                                    $scope,
                                    $http,
                                    $uibModalInstance,
                                    $log,
                                    DTOptionsBuilder,
                                    DTColumnBuilder,
                                    DTColumnDefBuilder,
                                    $compile,
                                    items,
                                    $httpParamSerializer,
                                    CONFIG,
                                    toastr,
                                    $timeout,
                                    EVENT) {
    // $rootScope.isProcessing = true;
    // $rootScope.$emit('LOADING:EVENT:PROCESSING');

    $scope.data = items;
    $scope.openJadwalOperasi = false;
    $scope.jenisOperasi = [];
    $scope.dataRuangOk = [];

    $scope.data.diagnosaText = "[";
    console.log("ini data reservasi ok")
    console.log($scope.data);

if($scope.data.data_rujukan.ok.length !=0){
    $scope.data.tanggalOperasi= moment($scope.data.data_rujukan.ok[0].tgl_operasi).format('DD-MM-YYYY');
        $scope.data.jamMulaiOperasi = $scope.data.data_rujukan.ok[0].jam_mulai;
        $scope.data.jamSelesaiOperasi = $scope.data.data_rujukan.ok[0].jam_selesai;
        $scope.data.jenis_operasi = $scope.data.data_rujukan.ok[0].jenis_operasi;
        $scope.data.operasi = $scope.data.data_rujukan.ok[0].operasi;
        $scope.data.ruang_ok = $scope.data.data_rujukan.ok[0].ruang_ok_id ;
        $scope.data.tingkat_urgency = $scope.data.data_rujukan.ok[0].tingkat_urgency;

}

    $scope.onChangeTanggalOperasi = onChangeTanggalOperasi;
    $scope.close = close;
    $scope.dismiss = dismiss;
    $scope.pesanJadwal = pesanJadwal;
    $scope.batal = batal;

    $http.get(CONFIG.JENIS_OPERASI).then(successReqJenisOperasi, errorReqJenisOperasi);
    $http.get(CONFIG.RUANG_OK).then(successReqRuangOk, errorReqRuangOk);

    function successReqJenisOperasi(result) {
        $rootScope.isProcessing = false;
        $scope.jenisOperasi = result.data.response;
    }

    function errorReqJenisOperasi(err) {
        console.log(err);
    }

    function successReqRuangOk(result) {
        $rootScope.isProcessing = false;
        $scope.dataRuangOk = result.data.response
    }

    function errorReqRuangOk(err) {
        console.log(err);
    }

    function pesanJadwal() {

        var reservasi = {
            tanggal: moment( angular.copy($scope.data.tanggalOperasi)).format('YYYY-MM-DD'),
            mulai: angular.copy($scope.data.jamMulaiOperasi),
            selesai: angular.copy($scope.data.jamSelesaiOperasi),
            jenis_operasi: angular.copy($scope.data.jenis_operasi),
            operasi: angular.copy($scope.data.operasi),
            ruang_ok_id: angular.copy($scope.data.ruang_ok),
            pasien:angular.copy( $scope.data),
            diagnosa: angular.copy(items.diagnosa),
            tingkat_urgency: angular.copy($scope.data.tingkat_urgency)
        };

        console.log("cek log ");
        console.log(reservasi);
        // console.log($.param(reservasi));

        var req = {
            method: 'POST',
            url: CONFIG.RESERVASI_JADWAL_OK,
           // data: $httpParamSerializer({data: reservasi}),
            data : "data=" + JSON.stringify(reservasi),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http.post(req.url, angular.copy(reservasi)).then(success, error);
        function success(result) {
            console.log(result);
            toastr.success('Berhasil Reservasi OK', 'Sukses!');
            $timeout(function () {
                close({
                    result: result,
                    rujuk_ok : 1
                });
            }, 500);
        }

        function error(err) {
            console.log(err);
        }
    }

    function batal(){
        var uid = items.reservasiOk.data.result.uid;

        $http.post(base_url + '/api/ok/jadwal_operasi/batal', {uid: uid}).then(function(result){
            close({result: result, rujuk_ok: 0});
        }, function(err){
            console.log(err);
        });
    }

    function onChangeTanggalOperasi() {
        var tanggalOperasi = $scope.data.tanggalOperasi;
        $scope.openJadwalOperasi = true;


    }

    function close(reservasiOk) {
        $uibModalInstance.close(reservasiOk);
        $rootScope.$emit(EVENT.CANCEL_RESERVASI_OK);
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }

    $timeout(function(){
        var selectOperasi = $('#operasi'),
            selectJenisOperasi = $('#jenis_operasi'),
            selectRuangOk = $('#ruang_ok');
        selectOperasi.select2({
            placeholder: 'PILIH OPERASI'
        });
        selectJenisOperasi.select2({
            placeholder: 'PILIH JENIS OPERASI'
        });
        selectRuangOk.select2({
            placeholder: 'PILIH RUANG OK'
        });



    }, 1000);

}