var url = "<?php echo site_url('self_registration/registration/form/'); ?>";

var $nomorKartu = $("#nomor_kartu");
var $btn_cari = $("btn_cari");
var $pasien;
var $poli_idd = 0;
var $dokter;
var $dokter_idd = 0;
var $tindakan = [];
var $nama_poli;
var $poli_temp = 0;
var $dokter_temp = 0;

// Variables
var $poliklinik = {}; // { POLIKLINIK_ID: {id: id, nama: nama, dokter_id: dokter_id} }

$btn_cari.attr('disabled', 'disabled');
var $modalCancelKunjungan = $('#modal-cancel-kunjungan');

$('.click-poliklinik').click(function (e) {
    var id = $(this).data('id');
    var nama = $(this).data('nama');

    $('#btn-batal_poli').data('poli_id', id);
    openModalPilihDokter(id, nama);
    // TODO openModalPilihDokter(id, nama)
});

$('#btn-batal_poli').click(function (e) {
    var poli_id = $(this).data('poli_id');
    batalPoli(poli_id);
});

function openModalPilihDokter(id, nama_poli) {

    $modalCancelKunjungan.modal('show');
    $poli_temp = id;
    $nama_poli = nama_poli

    $.ajax({
        url: base_url + '/api/master/dokter_poli_/layanan?layanan=' + id,
        method: 'get',
    }).then(function (result) {
        var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var date = new Date();
        var day = date.getDate();
        var thisDay = date.getDay(),
            thisDay = myDays[thisDay];

        // alert(thisDay + ', ' + day );
        $dokter = JSON.parse(result);
        $('#dokterrr').empty();
        $.each($dokter.results, function (key, value) {
            switch (thisDay) {
                case "Minggu":
                    if (value.minggu_mulai != "00:00:00" || value.minggu_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Senin":
                    if (value.senin_mulai != "00:00:00" || value.senin_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Selasa":
                    if (value.selasa_mulai != "00:00:00" || value.selasa_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Rabu" :
                    if (value.rabu_mulai != "00:00:00" || value.rabu_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Kamis" :
                    if (value.kamis_mulai != "00:00:00" || value.kamis_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Jumat" :
                    if (value.jumat_mulai != "00:00:00" || value.jumat_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '" type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                case "Sabtu" :
                    if (value.sabtu_mulai != "00:00:00" || value.sabtu_mulai2 != "00:00:00") {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    } else {
                        $("#dokterrr").append('<button id="d' + value.id_pegawai + '"  type="button" onclick="pilihDokter(' + value.id_pegawai + ',' + id + ', \'' + value.nama +'\', \'' + nama_poli + '\')" class="btn btn-primary btn-xlg" style="margin:10px;width: 248px;" disabled="true">' +
                            '<i class="icon-reading position-left"></i>' + value.nama + '</button>'
                        );
                    }
                    break;
                default :
                    $("#dokterrr").append('<h5>error</h5>');
            }
        });

        var dokter_id = $poliklinik[id] ? $poliklinik[id]['dokter_id'] : 0;
        $("#d" + dokter_id).css('background-color', '#7FFF00');
    }, function (err) {
        console.log(err);
    });
}

function pilihDokter(dokter_id, poliklinik_id, dokter, poliklinik) {
    $modalCancelKunjungan.modal('hide');

    //ubah warna default
    var btnPoli = $(".click-poliklinik[data-id=" + poliklinik_id + "]");
    var btnDokter = $("#d" + dokter_id);

    btnPoli.css('background-color', '#7FFF00');
    btnDokter.css('background-color', '#7FFF00');

    delete $poliklinik[poliklinik_id];
    $poliklinik[poliklinik_id] = {
        id: poliklinik_id,
        dokter_id: dokter_id,
        pasien_id: $pasien.id,
        dokter: dokter,
        poliklinik: poliklinik
    };
    updateDispPoliklinik($poliklinik);
}

function batalPoli(poli_id) {
    $modalCancelKunjungan.modal('hide');

    var btnPoli = $(".click-poliklinik[data-id=" + poli_id + "]");
    btnPoli.css('background-color', '#01bcd4');

    delete $poliklinik[poli_id];
    updateDispPoliklinik($poliklinik);
}

function updateDispPoliklinik(poliklinik) {
    $("#disp_poliklinik").empty();
    var dokter, poli;
    for (var key in poliklinik) {
        dokter = poliklinik[key]['dokter'];
        poli = poliklinik[key]['poliklinik'];
        $("#disp_poliklinik").append('<p>' + poli + ' - ' + dokter + '</p>');
    }
}







function onHideModalBatalKunjungan() {
    $textAlasan.val(null);

}
/*function batalPoli() {
    $modalCancelKunjungan.modal('hide');
    var btn = "p" + $poli_idd;
    var btnDokter = "d" + $dokter_idd;
    var propertyDokter = document.getElementById(btnDokter);
    var property = document.getElementById(btn);
    property.style.backgroundColor = "#01bcd4";
    propertyDokter.style.backgroundColor = "#01bcd4";
    $poli_temp = 0;
    $dokter_temp = 0;
    $poli_idd = 0;
    $dokter_idd = 0;

    document.getElementById('poli').value = "belum terpilih";
    document.getElementById('dokter_').value = "belum terpilih";
}*/
/*function pilihDokter(id, idbtn) {
    $modalCancelKunjungan.modal('hide');
    $dokter_temp = id;

    //ubah warna default
    var btn = "p" + $poli_idd;
    var btnDokter = "d" + $dokter_idd;
    var propertyDokter = document.getElementById(btnDokter);
    var property = document.getElementById(btn);

    if (property != null) {
        property.style.backgroundColor = "#01bcd4";
    }
    if (propertyDokter != null) {
        propertyDokter.style.backgroundColor = "#01bcd4";
    }


    $poli_idd = $poli_temp;
    $dokter_idd = $dokter_temp;

    btn = "p" + $poli_idd;
    btnDokter = "d" + $dokter_idd;
    propertyDokter = document.getElementById(btnDokter);
    property = document.getElementById(btn);
    property.style.backgroundColor = "#7FFF00";
    propertyDokter.style.backgroundColor = "#7FFF00";

    $dokter_idd = id;
    document.getElementById('no_kartu_').value = $pasien.no_rekam_medis;
    document.getElementById('no_ktp_').value = ($pasien.no_identitas != "null") ? $pasien.no_identitas : "-";
    document.getElementById('nama_').value = $pasien.nama;
    document.getElementById('tempat_lahir_').value = ($pasien.tempat_lahir != "null" ) ? $pasien.tempat_lahir.trim() : "-";
    document.getElementById('tgl_lahir_').value = ($pasien.tgl_lahir != "null") ? $pasien.tgl_lahir : "-";
    document.getElementById('jenis_kelamin_').value = ($pasien.jenis_kelamin == '1') ? "LAKI-LAKI" : "PEREMPUAN";
    document.getElementById('golongan_').value = ($pasien.golongan != "null") ? $pasien.golongan : "-";
    document.getElementById('poli').value = $nama_poli;

    $.each($dokter.results, function (key, value) {
        if (value.id_pegawai == $dokter_idd) {
            document.getElementById('dokter_').value = value.nama;

        }
    });
    $.ajax({
        url: base_url + '/api/master/tindakan_/tarif',
        method: 'get',
        data: {
            layanan: $poli_idd,
            jenis_dokter: 1,
            kelas: 0
        }

    }).then(function (result) {
        $tindakan[1] = result;
    }, function (err) {
        console.log(err);
    });


}*/

function cetakNota(id, nomor) {
    window.open(base_url + '/api/rawat_jalan/pendaftaran_/cetak_nota/' + param.id + '/' + param.nomor, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
}

function save() {
    console.log($tindakan, $poliklinik);
    var poliklinik_length = 0;
    for (var key in $poliklinik) {
        poliklinik_length++;
    }

    if (poliklinik_length != 0) {
        var rujukan = {
            rujuk_ok: 0,
            kunjungan_ulang: 0,
            rujuk_rawatinap: 0,
            rujuk_audiometri: 0,
            rujuk_cathlab: 0,
            rujuk_ctscan: 0,
            rujuk_diagnostik_fungsional: 0,
            rujuk_fisioterapi: 0,
            rujuk_laboratorium: 0,
            rujuk_ods_odc: 0,
            rujuk_radiologi: 0
        };

        var timer = null;
        var poliklinik_id, dokter_id;
        for (var key in $poliklinik) {
            poliklinik_id = $poliklinik[key].id;
            dokter_id = $poliklinik[key].dokter_id;

            $.ajax({
                url: base_url + '/api/rawat_jalan/pendaftaran_/save',
                method: 'POST',
                data: $.param({
                    rujukan: rujukan,
                    pasien: $pasien,
                    poli_id: poliklinik_id,
                    dokter: $dokter,
                    dokter_id: dokter_id,
                    tindakan: $tindakan

                })
            }).then(function (result) {
                var hasil = JSON.parse(result);
                successMessage("Berhasil", "silahkan cetak nota,hubungi petugas jika mengalami kendala");
                print(hasil.id_pelayanan, hasil.nomor_antrian);

                if (timer) {
                    clearTimeout(timer);
                }

                timer = setTimeout(function () {
                    swal({
                        title: "KONFIRMASI",
                        text: "Apakah anda ingin mendaftar ke poli lain ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "rgb(89, 199, 61)",
                        confirmButtonText: "YA, Daftar Lagi",
                        cancelButtonText: "TIDAK!, Sudah Selesai",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var pathArray = window.location.pathname.split( '/' );
                            window.location=base_url +'/self_registration/registration/form/'+pathArray[pathArray.length-1];
                        } else {
                            window.location=base_url +'/self_registration/registration/';
                        }
                    });
                }, 3000);
            }, function (err) {
                // console.log(err);
                swal("GAGAL,", "maaf server mengalami gangguan", "error");
                // window.location = base_url + '/self_registration/registration';
            });

            $tindakan = [];
        }
    } else {
        swal("ANDA BELUM MEMILIH POLI", "silahkan pilih poli dan dokter terlebih dahulu)", "error");
    }
}
function print(id, antrian) {
    // var iframeHeight = $(window).height() - 220;
    // $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="' + base_url + '/api/rawat_jalan/pendaftaran_/cetak_nota/' + id + '/' + antrian + '?d=print" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url(' + base_url + 'assets/img/spinner.gif); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
    // $('#printModal-print-btn').attr('disabled', 'disabled');
    // $('#printModal-iframe').on('load', function () {
    //     $('#printModal-print-btn').removeAttr('disabled');
    // });
    // $('#printModal').modal('show');
    window.open(base_url + '/api/rawat_jalan/pendaftaran_/cetak_nota/' + id);
}

$(window).load(function () {

    $('.stepy-step').find('.button-next').removeClass('btn-primary');
    $('.stepy-step').find('.button-next').addClass(' button-next btn btn-default');
    $('.button-next').hover(function () {
        $('.stepy-step').find('.button-next').removeClass('btn-primary');
        $('.stepy-step').find('.button-next').addClass('button-next btn btn-primary');
    }, function () {
        $('.stepy-step').find('.button-next').removeClass('btn-primary');
        $('.stepy-step').find('.button-next').addClass('button-next btn btn-default');
    });

    $('.button-next').click(function (index) {
        console.log(index);
    }, function () {

    });
    var id = document.getElementById("no_kartu").value;
    $("#btn_batal").click(function () {
        window.location = base_url + '/self_registration/registration';
    });

    $.ajax({
        url: base_url + '/api/master/pasien_/get_by_rm/' + id,
        method: 'get',
    }).then(function (result) {

        $pasien = result;
        document.getElementById('no_kartu').value = result.no_rekam_medis;
        document.getElementById('no_ktp').value = (result.no_identitas != "null") ? result.no_identitas : "-";
        document.getElementById('nama').value = result.nama;
        document.getElementById('disp_tempat_lahir').value = (result.tempat_lahir != "null") ? result.disp_tempat_lahir : "-";
        document.getElementById('tempat_lahir').value = (result.tempat_lahir != "null") ? result.tempat_lahir.trim() : "-";
        document.getElementById('tgl_lahir').value = (result.tgl_lahir != "null") ? result.tgl_lahir : "-";
        document.getElementById('jenis_kelamin').value = (result.jenis_kelamin == '1') ? "LAKI-LAKI" : "PEREMPUAN";
        document.getElementById('golongan').value = (result.golongan != "null") ? result.golongan : "-";

        document.getElementById('no_kartu_').value = $pasien.no_rekam_medis;
        document.getElementById('no_ktp_').value = ($pasien.no_identitas != "null") ? $pasien.no_identitas : "-";
        document.getElementById('nama_').value = $pasien.nama;
        document.getElementById('disp_tempat_lahir_').value = ($pasien.tempat_lahir != "null") ? $pasien.disp_tempat_lahir : "-";
        document.getElementById('tempat_lahir_').value = ($pasien.tempat_lahir != "null" ) ? $pasien.tempat_lahir.trim() : "-";
        document.getElementById('tgl_lahir_').value = ($pasien.tgl_lahir != "null") ? $pasien.tgl_lahir : "-";
        document.getElementById('jenis_kelamin_').value = ($pasien.jenis_kelamin == '1') ? "LAKI-LAKI" : "PEREMPUAN";
        document.getElementById('golongan_').value = ($pasien.golongan != "null") ? $pasien.golongan : "-";

    }, function (err) {
        console.log(err);
    });

    $.ajax({
        url: base_url + '/api/master/tindakan_/init',
        method: 'get',
        data: {
            status_pasien: 'lama',
            jenis_kel: '1' // tambahan tri
        }
    }).then(function (result) {
        console.log(result);
        $tindakan[0] = result;

    }, function (err) {
        console.log(err);
    });
});


