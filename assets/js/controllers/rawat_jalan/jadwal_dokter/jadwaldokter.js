var the_table;
var urlReq = base_url + "/rawat_jalan/jadwal_dokter/list_jadwal_dokter";
var def = false;
var pdokter = false
var ppoli = false;
var save_method; //for save method string
var table;
var filterPoli;
var filterKlinik;
var visible = true;
var depo_uid = "<?php echo $depo_uid; ?>";
depo_uid = depo_uid === "" ? 0 : depo_uid;

$(window).load(function () {
    //Bagian Pasien

    $('#btn-cari').click(function () {
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        pdokter = false;
        ppoli = false;
        create_table();
    });

    $('#poli').change(function () {
        console.log($(this).val());
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        ppoli = true;
        filterPoli = $(this).val();
        fillDokter("#dokter", "#loading_search_depo", $(this).val());
        $('#dokter').attr('disabled', false);
        create_table();
    });
    $('#dokter').change(function () {
        if ( !$('#dokter').val() ) {

        }else {
            if (typeof the_table != 'undefined') {
                the_table.destroy();
            }
            pdokter = true;
            create_table();
        }
    });
    $('#klinik_praktek').change(function () {
        console.log($(this).val());
        filterKlinik = $(this).val();
        fillDokter_praktek("#dokter_praktek", "#loading_search_depo", $(this).val());
        $('#dokter_praktek').attr('disabled', false);

    });
    $('#dokter_praktek').change(function () {
        console.log($(this).val());
    });
    $('#daftar-jadwal').click(function () {
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        pdokter = false;
        ppoli = false;
        $('#tambah').attr('disabled', false); //set button enable
        create_table();
    });
    $('#daftar-harian').click(function () {

        $('#tambah').attr('disabled', true); //set button disabled
        location.reload();
    });


    pdokter = false;
    ppoli = false;
    create_table();

});

function create_table() {

    var dokter = $('#dokter').val();
    var poli = $('#poli').val();

    the_table = $("#dataTable_user").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": urlReq,
            "type": "POST",

            "data": {
                dokter: dokter,
                poli: poli,
                pdokter: pdokter,
                ppoli: ppoli
            },
        },
        "order": [[5, 'asc'], [2, 'asc']],
        "columns": [
            {
                "data": "poliklinik"
            },
            {
                "data": "dokter"
            },
            {
                "render" : function (data, type, row) {
                    return row.senin +"</br></br>" +row.senin2 ;
                },

                "searchable": false
            },
            {
                "render" : function (data, type, row) {
                    return row.selasa +"</br></br>" +row.selasa2 ;
                },

                "searchable": false
            },
            {
                "render" : function (data, type, row) {
                    return row.rabu +"</br></br>" +row.rabu2 ;
                },

                "searchable": false
            },
            {
                "render" : function (data, type, row) {
                    return row.kamis +"</br></br>" +row.kamis2 ;
                },

                "searchable": false
            },
            {
                "render" : function (data, type, row) {
                    return row.jumat +"</br></br>" +row.jumat2 ;
                },

                "searchable": false
            },
            {

                "render" : function (data, type, row) {
                    return row.sabtu +"</br></br>" +row.sabtu2 ;
                },

                "searchable": false
            },
            {
                "render" : function (data, type, row) {
                    return row.minggu +"</br></br>" +row.minggu2 ;
                },

                "searchable": false
            },

            {
                "data": "action",
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person(' + "'" + row.id + "'" + ')"><i class="glyphicon glyphicon-pencil"></i></a>  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person(' + "'" + row.id + "'" + ')"><i class="glyphicon glyphicon-trash"></i> </a>';
                }
            }
        ]
    });
}


$(document).ready(function () {
    var seninn = 0;
    var selasaa = 0;
    var rabuu = 0;
    var kamiss = 0;
    var jumatt = 0;
    var sabtuu = 0;
    var mingguu = 0;
    // $('#mulai_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $('#akhir_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $("#mulai_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });
    // $("#akhir_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#senin_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#senin_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#senin_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#senin_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#selasa_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#selasa_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#selasa_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#selasa_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#rabu_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#rabu_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#rabu_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#rabu_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#kamis_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#kamis_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#kamis_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#kamis_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#jumat_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#jumat_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#jumat_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#jumat_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#sabtu_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#sabtu_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#sabtu_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#sabtu_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $("#minggu_mulai").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#minggu_akhir").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#minggu_mulai2").inputmask("h:s", {"placeholder": "hh/mm"});
    $("#minggu_akhir2").inputmask("h:s", {"placeholder": "hh/mm"});

    $('#tambah').attr('disabled', true); //set button disabled
    $('#dokter').attr('disabled', true);
    $('#dokter_praktek').attr('disabled', true);

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    $("#poli").select2();
    $("#dokter").select2()
    $("#klinik_praktek").select2();
    $("#dokter_praktek").select2();

    fillDepo("#poli", "#loading_search_depo");
    fillDepo("#klinik_praktek", "#loading_search_depo");

    $(".form-senin2").hide();
    $(".form-selasa2").hide();
    $(".form-rabu2").hide();
    $(".form-kamis2").hide();
    $(".form-jumat2").hide();
    $(".form-sabtu2").hide();
    $(".form-minggu2").hide();


    $('#btnSenin').click(function () {
        if (seninn == 0) {
            $(".form-senin2").show();
            seninn = 1;
        } else {
            $(".form-senin2").hide();
            seninn = 0;
        }
    });
    $('#btnSelasa').click(function () {
        if (selasaa == 0) {
            $(".form-selasa2").show();
            selasaa = 1;
        } else {
            $(".form-selasa2").hide();
            selasaa = 0;
        }
    });
    $('#btnRabu').click(function () {
        if (rabuu == 0) {
            $(".form-rabu2").show();
            rabuu = 1;
        } else {
            $(".form-rabu2").hide();
            rabuu = 0;
        }
    });
    $('#btnKamis').click(function () {
        if (kamiss == 0) {
            $(".form-kamis2").show();
            kamiss = 1;
        } else {
            $(".form-kamis2").hide();
            kamiss = 0;
        }
    });
    $('#btnJumat').click(function () {
        if (jumatt == 0) {
            $(".form-jumat2").show();
            jumatt = 1;
        } else {
            $(".form-jumat2").hide();
            jumatt = 0;
        }
    });
    $('#btnSabtu').click(function () {
        if (sabtuu == 0) {
            $(".form-sabtu2").show();
            sabtuu = 1;
        } else {
            $(".form-sabtu2").hide();
            sabtuu = 0;
        }
    });
    $('#btnMinggu').click(function () {
        if (mingguu == 0) {
            $(".form-minggu2").show();
            mingguu = 1;
        } else {
            $(".form-minggu2").hide();
            mingguu = 0;
        }
    });

    // $('#dataTable_user').DataTable( {
    //     order: [[ 5, 'asc' ], [ 2, 'asc' ]]
    // } );

});

function fillDepo(element, loading) {
    // $(loading).show();
    $.getJSON(base_url + "/api/layanan/get_layanan_all", function (data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Poliklinik- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {
                // if (depo_uid === data.depo_list[i].uid) {
                //     option += '<option value="' + data.depo_list[i].uid + '" selected="selected">' + data.depo_list[i].nama + '</option>';
                // }
                // else {
                option += '<option value="' + data.depo_list[i].id + '">' + data.depo_list[i].nama + '</option>';
                // }
            }

            $(element).html(option);
            $(element).trigger("change");
            // if (depo_uid != "") {
            //     $(element).prop("disabled", true);
            // }
            // $("#hdepo_uid").val(depo_uid);
        }
        $(loading).hide();
    });
}
;
function fillDokter(element, loading, filterr) {
    // $(loading).show();
    $.getJSON(base_url + "/api/dokter/get_dokter_all?filter=" + filterr, function (data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Dokter- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {
                // if (depo_uid === data.depo_list[i].uid) {
                //     option += '<option value="' + data.depo_list[i].uid + '" selected="selected">' + data.depo_list[i].nama + '</option>';
                // }
                // else {
                option += '<option value="' + data.depo_list[i].id + '">' + data.depo_list[i].nama + '</option>';
                // }
            }

            $(element).html(option);
            $(element).trigger("change");
            // if (depo_uid != "") {
            //     $(element).prop("disabled", true);
            // }
            // $("#hdepo_uid").val(depo_uid);
        }
        $(loading).hide();
    });
}
;
function fillDokter_praktek(element, loading, filterr) {

    $.getJSON(base_url + "/api/dokter/get_dokter_all?filter=" + filterr, function (data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Dokter- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {
                option += '<option value="' + data.depo_list[i].id + '">' + data.depo_list[i].nama + '</option>';
                // }
            }

            $(element).html(option);
            $(element).trigger("change");
        }
        $(loading).hide();
    });
}
;

function add_jadwal() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addJadwal').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Jadwal'); // Set Title to Bootstrap modal title
}

function edit_person(id) {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: base_url + "/rawat_jalan/jadwal_dokter/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            $('[name="id"]').val(data.id);
            $('[name="klinik_praktek"]').select2().val(data.id_klinik).trigger("change");
            $('[name="senin_mulai"]').val(data.senin_mulai);
            $('[name="senin_akhir"]').val(data.senin_akhir);
            $('[name="selasa_mulai"]').val(data.selasa_mulai);
            $('[name="selasa_akhir"]').val(data.selasa_akhir);
            $('[name="rabu_mulai"]').val(data.rabu_mulai);
            $('[name="rabu_akhir"]').val(data.rabu_akhir);
            $('[name="kamis_mulai"]').val(data.kamis_mulai);
            $('[name="kamis_akhir"]').val(data.kamis_akhir);
            $('[name="jumat_mulai"]').val(data.jumat_mulai);
            $('[name="jumat_akhir"]').val(data.jumat_akhir);
            $('[name="sabtu_mulai"]').val(data.sabtu_mulai);
            $('[name="sabtu_akhir"]').val(data.sabtu_akhir);
            $('[name="minggu_mulai"]').val(data.minggu_mulai);
            $('[name="minggu_akhir"]').val(data.minggu_akhir);

            $('[name="senin_mulai2"]').val(data.senin_mulai2);
            $('[name="senin_akhir2"]').val(data.senin_akhir2);
            $('[name="selasa_mulai2"]').val(data.selasa_mulai2);
            $('[name="selasa_akhir2"]').val(data.selasa_akhir2);
            $('[name="rabu_mulai2"]').val(data.rabu_mulai2);
            $('[name="rabu_akhir2"]').val(data.rabu_akhir2);
            $('[name="kamis_mulai2"]').val(data.kamis_mulai2);
            $('[name="kamis_akhir2"]').val(data.kamis_akhir2);
            $('[name="jumat_mulai2"]').val(data.jumat_mulai2);
            $('[name="jumat_akhir2"]').val(data.jumat_akhir2);
            $('[name="sabtu_mulai2"]').val(data.sabtu_mulai2);
            $('[name="sabtu_akhir2"]').val(data.sabtu_akhir2);
            $('[name="minggu_mulai2"]').val(data.minggu_mulai2);
            $('[name="minggu_akhir2"]').val(data.minggu_akhir2);

            setTimeout(function(){
                $('[name="dokter_praktek"]').select2().val(data.id_dokter).trigger("change");
            }, 4000);
            // $('[name="ruangan_praktek"]').val(data.catatan);

            $('#addJadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
}

function save() {
   // $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = base_url + "/rawat_jalan/jadwal_dokter/ajax_add";
    } else {
        url = base_url + "/rawat_jalan/jadwal_dokter/ajax_update";
    }
    // console.log("asdaaaaaaa");
    // console.log ($('#form').serialize());
    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function (data) {
            console.log("INIIIIIIIII");
            console.log(data);
            if (data.status) //if success close modal and reload ajax table
            {
                toastr.success('Data Telah Tersimpan', 'BERHASIL!');
                $('#addJadwal').modal('hide');
                the_table.draw();
            }
            else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }

           // $('#btnSave').text("<b><i class='icon-floppy-disk'></i></b> Simpan"); //change button text
            $('#btnSave').attr('disabled', false); //set button enable
            // toastr.success('Simpan Jadwal Berhasil !', 'Sukses');
            // the_table.destroy();
            // create_table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error('Error adding / update data', 'GAGAL!');

            //$('#btnSave').text("<b><i class='icon-floppy-disk'></i></b> Simpan"); //change button text
            $('#btnSave').attr('disabled', false); //set button enable

        }
    });
}

function delete_person(id) {

    swal({
            title: "Apakah anda yakin ?",
            text: "data yang telah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "YA, Hapus",
            cancelButtonText: "TIDAK!, Batalkan",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: base_url + "/rawat_jalan/jadwal_dokter/ajax_delete/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        //if success reload ajax table
                        $('#modal_form').modal('hide');
                        the_table.draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });
                toastr.success('Data Telah Terhapus', 'BERHASIL!');

            } else {
                swal("Batal", "Data Tidak Terhapus :)", "error");
            }
        });

}