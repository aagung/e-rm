/*application initialization*/
var app = angular.module('rsbt', [
    'rsbt.api',
    'rsbt.components',
    'ngAnimate',
    'toastr',
    'ui.bootstrap',
    'datatables',
    'angucomplete-alt',
    'leaflet-directive',
    'ngMask',
    'ngResource',
    'ngTagsInput'
]);



/*component directives*/

app.factory('httpInterceptor', ['$q', '$rootScope',
    function ($q, $rootScope) {
        var loadingCount = 0;

        return {
            request: function (config) {
                if(++loadingCount === 1) $rootScope.$broadcast('loading:progress');
                return config || $q.when(config);
            },

            response: function (response) {
                if(--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                return response || $q.when(response);
            },

            responseError: function (response) {
                if(--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                return $q.reject(response);
            }
        };
    }
]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');

    $httpProvider.interceptors.push(['$q', function($q) {
        return {
            request: function(config) {
                if (config.data && typeof config.data === 'object') {
                    // Check https://gist.github.com/brunoscopelliti/7492579
                    // for a possible way to implement the serialize function.
                    config.data = serialize(config.data);
                }
                return config || $q.when(config);
            }
        };
    }]);

    var serialize = function(obj, prefix) {
        // http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
        var str = [];
        for(var p in obj) {
            var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
            str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
        return str.join("&");
    }

}]);

app.factory('select2ChainedData', ['$http', function ($http) {
    function initData(data) {
        this.data = data;
        this.setData = function (data) {

            this.data = data;
        };
        this.getData = function () {

            return this.data;
        }
    }

    return new initData();
}]);

/*end component directives*/

/*application runtime*/
app.run(['$rootScope', '$http', 'CONFIG', 'EVENT',
    function ($rootScope, $http, CONFIG, EVENT) {
        $rootScope.searchUrl = CONFIG.SEARCH_URL;
        $rootScope.searchPasienIgd = CONFIG.SEARCH_PASIEN_IGD;
        $rootScope.searchPasienDayCare = CONFIG.SEARCH_PASIEN_DAY_CARE;
        $rootScope.searchPasien = CONFIG.SEARCH_PASIEN;
        $rootScope.searchPegawai = CONFIG.SEARCH_PEGAWAI;
        $rootScope.searchLayanan = CONFIG.SEARCH_LAYANAN;
        $rootScope.searchDokterPoli = CONFIG.SEARCH_DOKTER_POLI;
        $rootScope.searchPemeriksaanDokter = CONFIG.SEARCH_PEMERIKSAAN_DOKTER;
        $rootScope.searchPemeriksaanDokterIgd = CONFIG.SEARCH_PEMERIKSAAN_DOKTER_IGD;
        $rootScope.searchPemeriksaanDokterDayCare = CONFIG.SEARCH_PEMERIKSAAN_DAY_CARE;
        $rootScope.searchLaboratorium = CONFIG.SEARCH_LAB;
        $rootScope.searchObat = CONFIG.SEARCH_OBAT;

        $rootScope.onlyNumber = /^\d+$/;

        $rootScope.profilePic = '/assets/img/backgrounds/boxed_bg.png';

        //isProcessing
        $rootScope.isProcessing = false;

        //switchShift
        $rootScope.isBuka = true;
        $rootScope.switchShift = function (status) {
            $rootScope.$emit(EVENT.SWITCH_SHIFT_CLOSE);

        }
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8;";

    }
]);
/*end of application runtime*/

/*application config*/
/**
 * Http Provider Config
 * @desc : Modify request and response
 */
app.config(['$httpProvider',
    function ($httpProvider) {
        console.log("dari config provider");
    }
]);

/**
 * Toastr Config
 * @desc : Configuration for popup toast
 */
app.config(['toastrConfig', function (toastrConfig) {
    console.log("masuk toastr config");
    angular.extend(toastrConfig, {
        allowHtml: false,
        closeButton: false,
        closeHtml: '<button>&times;</button>',
        extendedTimeOut: 1000,
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        messageClass: 'toast-message',
        onHidden: null,
        onShown: null,
        onTap: null,
        progressBar: false,
        tapToDismiss: true,
        templates: {
            toast: 'toast.html',
        },
        timeOut: 5000,
        titleClass: 'toast-title',
        toastClass: 'toast'
    });
}]);

app.constant('EVENT', {
    SWITCH_SHIFT_OPEN: 'SWITCH::SHIFT::OPEN',
    SWITCH_SHIFT_CLOSE: 'SWITCH::SHIFT::CLOSE',
    OLD_PATIENT: 'OLD::PATIENT',
    CANCEL_RESERVASI_OK : 'CANCEL::RESERVASI::OK'
});
/*end of application config*/