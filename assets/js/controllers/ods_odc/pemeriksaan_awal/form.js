app.controller('PemeriksaanAwalFormController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$location',
    function ($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $location) {

        $scope.data = {};

        var url = base_url + '/api/ods_odc/pemeriksaan_awal/save';

        if (window.location.search) {
            $http.get(base_url + '/api/ods_odc/pemeriksaan_awal/search' + window.location.search).then(function (result) {
                console.log(result);
                var obj = angular.copy(result.data.results[0]);
                var rawatJalan = angular.copy(result.data.rawat_jalan[0]);
                $scope.data = obj;
                $scope.data.tanggal = new Date($scope.data.tanggal);
                $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
                $scope.data.tPelayananId = obj.t_pelayananId;
                $scope.noRekamMedis = obj.no_rekam_medis;
                $scope.data.tinggiBadan = rawatJalan.tinggi;
                $scope.data.beratBadan = rawatJalan.berat;
                $scope.data.suhuBadan = rawatJalan.suhu;
                $scope.data.tensi1 = rawatJalan.tensi;
                $scope.data.tensi2 = rawatJalan.tensi;
                $scope.data.anamnesaAwal = rawatJalan.anamnesa;
                $scope.data.rawatJalanId = rawatJalan.id;
                url = base_url + '/api/rawat_jalan/pemeriksaan_awal/update';

            });
        } else {
            console.log('ga ada search');
        }

        $scope.selected = function (item) {

            if (item) {
                console.log(item);
                var obj = angular.copy(item.originalObject);
                $scope.data = obj;
                $scope.data.tanggal = new Date($scope.data.tanggal);
                $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
                $scope.data.tPelayananId = obj.t_pelayananId;
            } else {
                $scope.data = {};
            }
        };

        $scope.tes = 'tes';

        $scope.save = function () {

            var _data = angular.copy($scope.data);
            $rootScope.$emit('LOADING:EVENT:PROCESSING');
            var req = {
                method: 'POST',
                url: url,
                data: 'data=' + JSON.stringify(_data),
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            };
            console.log("req data , ", _data);
            $http(req).then(function (result) {
                console.log("result save, ", result);
                $rootScope.$emit('LOADING:EVENT:FINISH');
                toastr.success('Simpan ODS / ODC Pemeriksaan Awal Berhasil !', 'Sukses');

                buildTable();
            }, function (err) {
                console.log("err", err);
                toastr.error(err.data, 'Error');
            });

        };

        $scope.reset = function () {
            window.location.href = base_url + '/igd/pemeriksaan_awal';
        };



        function buildTable() {
            var url = base_url + '/api/ods_odc/pemeriksaan_awal/list_rawat_jalan/0';

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: url,
                    type: 'POST',
                    data: function (data, dtInstance) {

                        // data.columns[data.length - 1].name = "m_pegawai.nama";
                        // Modify the data object properties here before being passed to the server
                    }
                })
                // or here
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('order', [[0, 'desc']])
                .withOption('createdRow', function (row) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType('full_numbers');

            $scope.dtInstance = {};
            $scope.dtColumns = [
                DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {

                    return '<a href="' + base_url + '/igd/pemeriksaan_awal?s=' + row.pelayanan_id + '&rawat_jalan=' + row.id + '">' + moment(row.tanggal).format('DD-MMM-YYYY H:mm:s') + '</a>';
                }).withOption('searchable', false),
                DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No MR'),
                DTColumnBuilder.newColumn('no_register').withTitle('No Register'),
                DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
                DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
                DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter')

            ];

            $scope.dtInstance = {};
        }

        buildTable();

    }
]);