/**
 * Created by agungrizkyana on 10/5/16.
 */
/**
 * Modal Reservasi OK
 * @type {string[]}
 */
app.controller('ModalAddRawatInapController', modalAddRawatInapController);
modalAddRawatInapController.$inject = [
    '$rootScope',
    '$scope',
    '$http',
    '$uibModalInstance',
    '$log',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'DTColumnDefBuilder',
    '$compile',
    'items',
    'CONFIG',
    'toastr',
    '$timeout',
    'EVENT'
];
function modalAddRawatInapController($rootScope,
                                    $scope,
                                    $http,
                                    $uibModalInstance,
                                    $log,
                                    DTOptionsBuilder,
                                    DTColumnBuilder,
                                    DTColumnDefBuilder,
                                    $compile,
                                    items,
                                    CONFIG,
                                    toastr,
                                    $timeout,
                                    EVENT) {
    var _ruang = {};
    var _bed = {};
    var _kelas = {};

    $scope.data = items;
    $scope.openJadwalOperasi = false;
    $scope.dataRuang = [];
    $scope.dataKelas = [];
    $scope.dataBed = [];


    $scope.onChangeKelas = onChangeKelas;
    $scope.onChangeRuang = onChangeRuang;
    $scope.onChangeBed = onChangeBed;

    $scope.pesanKamarRawatInap = pesanKamarRawatInap;

    $scope.close = close;
    $scope.dismiss = dismiss;

    $http.get(CONFIG.RUANG).then(successReqRuang, errorReqRuang);
    $http.get(CONFIG.KELAS).then(successReqKelas, errorReqKelas);

    function successReqRuang(result) {

        $scope.dataRuang = result.data.response;
    }

    function errorReqRuang(err) {
        console.log(err);
    }

    function successReqKelas(result) {

        $scope.dataKelas = result.data.response
    }

    function errorReqKelas(err) {
        console.log(err);
    }

    function onChangeRuang(ruang){
        _ruang = ruang;
    }

    function onChangeKelas(kelas){
        _kelas = kelas;
        $http.get(CONFIG.BED + '?ruang=' + _ruang.id + '&kelas=' + kelas.id).then(successReqBed, errorReqBed);

        function successReqBed(result){
            $scope.dataBed = result.data.response;
        }

        function errorReqBed(err){
            console.log(err);
        }
    }

    function onChangeBed(bed){
        _bed = bed;
    }

    function pesanKamarRawatInap(){
        var _item = angular.copy($scope.data);
        var _data = {
            tanggal : moment().format('YYYY-MM-DD hh:mm:ss'),
            rawat_inap : {
                ruang : _ruang,
                kelas : _kelas,
                bed : _bed,
                jumlah_hari : _item.jumlahHari
            },
            pasien : _item
        };
        console.log('post', _data);
        var req = {
            method: 'POST',
            url: base_url + '/api/rawat_inap/registrasi/save',
            data: 'data=' + JSON.stringify(_data),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).then(success, error);
        function success(result){
            toastr.success('Berhasil Pesan Kamar Rawat Inap', 'Sukses!');
            $timeout(function () {
                close(result);
            }, 500);
        }
        function error(err){
            console.log(err);
        }
    }

    function close(reservasiOk) {
        $uibModalInstance.close(reservasiOk);
        $rootScope.$emit(EVENT.CANCEL_RESERVASI_OK);
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }


}