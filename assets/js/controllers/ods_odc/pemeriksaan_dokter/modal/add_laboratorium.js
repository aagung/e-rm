/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddLaboratoriumController', modalAddLaboratorium);
modalAddLaboratorium.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal', 'items', 'toastr', '$timeout'];
function modalAddLaboratorium($rootScope, $scope, api, $http, $uibModalInstance, $log, $uibModal, items, toastr, $timeout) {

    var tindakan = [];

    $scope.rujukan = {};

    $scope.data = items;
    $scope.data.tanggalPendaftaran = moment().format('DD/MM/YYYY');
    $scope.data.kelamin = $scope.data.jenis_kelamin == 1 ? 'Laki - Laki' : 'Perempuan';

    $scope.choose = {};
    $scope.qty = 1;
    $scope.labs = [];

    $scope.save = save;
    $scope.batal = batal;
    $scope.close = close;
    $scope.dismiss = dismiss;


    $scope.selectedNodes = function (nodes) {
        tindakan.push(nodes);
    };

    $scope.delete = function (i) {
        $scope.labs.splice(i, 1);
    };

    $scope.selected = function (item) {
        if (item) {
            $scope.labs.push(item.originalObject);

        }
    };

    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };

    $scope.openModalSubLaboratorium = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddSubLaboratorium.html',
            controller: 'ModalAddSubLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataObat.push(selectedItem);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    function batal() {
        console.log(angular.copy($scope.rujukan));
        // var req = {
        //     method: 'POST',
        //     url: base_url + '/api/rujukan/laboratorium/batal',
        //     data: 'data=' + JSON.stringify(rujukan),
        //     headers: {
        //         'Content-type': 'application/x-www-form-urlencoded'
        //     }
        // };
        // $http(req).then(function (result) {
        //     console.log(result);
        //     toastr.success('Berhasil Batalkan Daftar Laboratorium', 'Sukses!');
        //     $timeout(function () {
        //         close(result);
        //     }, 500);
        // }, function (err) {
        //     console.log(err);
        //     toastr.error('Terjadi Kesalahan Sistem', 'Gagal');
        // });
    }

    function save() {
        // marshall
        $scope.data.tindakan = tindakan;

        console.log("data tindakan, ", {
            tindakan: angular.copy($scope.data)
        });

        var req = {
            method: 'POST',
            url: base_url + '/api/rujukan_lab/laboratorium/save',
            data: 'data=' + JSON.stringify($scope.data),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).then(function (result) {
            console.log(result.data.response);
            $scope.rujukan = result.data.response;
            toastr.success('Berhasil Daftar Laboratorium', 'Sukses!');
            $timeout(function () {

                close({
                    uid : result.data.response,
                    data : angular.copy($scope.data.tindakan)
                });
            }, 500);
        }, function (err) {
            console.log(err);
            toastr.error('Terjadi Kesalahan Sistem', 'Gagal');
        });
    }

    function close(data) {

        console.log("data : ", angular.copy(data));
        $uibModalInstance.close(angular.copy(data));
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }

}