app.controller('PemeriksaanDokterFormController', pemeriksaanDokterFormController);
pemeriksaanDokterFormController.$inject = ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$uibModal', '$log', 'EVENT', '$q'];
function pemeriksaanDokterFormController($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $uibModal, $log, EVENT, $q) {

    //rootscope var
    $rootScope.rujukan = [];

    // initial vars
    $scope.data = {};
    $scope.data.rujukan = {
        rujuk_ok: 0,
        rujuk_rawatinap: 0,
        rujuk_audiometri: 0,
        rujuk_catchlab: 0,
        rujuk_ctscan: 0,
        rujuk_diagnostik_fungsional: 0,
        rujuk_fisioterapi: 0,
        rujuk_laboratorium: 0,
        rujuk_ods_odc: 0,
        rujuk_radiologi: 0
    };
    $scope.data.rujuk = [];

    $scope.dataTindakan = [];
    $scope.dataBphp = [];
    $scope.dataObat = [];
    $scope.dataFarmasi = [];
    $scope.dataLaboratorium = [];
    $scope.dataRacikanInduk = [];
    $scope.racikan = {};
    $scope.rawatInap = {};
    $scope.farmasi = {};
    $scope.laboratorium = {};
    $scope.tarifTotalRacikanInduk = 0;

    // boolean vars
    $scope.isTindakanEdit = false;
    $scope.isBphpEdit = false;
    $scope.isObatEdit = false;
    $scope.isOpenAddRacikan = false;


    // function vars
    $scope.addRacikan = addRacikan;
    $scope.openModalAddTindakan = addTindakan;
    $scope.openModalAddBphp = addBphp;
    $scope.openModalAddNonRacikan = addNonRacikan;
    $scope.openModalAddFarmasi = addFarmasi;


    $scope.openModalAddLaboratorium = addLaboratorium;
    $scope.openModalAddRadiologi = addRadiologi;
    $scope.openModalAddCtScan = addCtScan;
    $scope.openModalAddFisioterapi = addFisioterapi;
    $scope.openModalAddCatchLab = addCatchLab;
    $scope.openModalAddAudiometri = addAudiometri;
    $scope.openModalAddDiagnostikFungsional = addDiagnostikFungsional;
    $scope.openModalAddOdsOdc = addOdsOdc;

    $scope.openModalAddRawatInap = addRawatInap;
    $scope.openModalReservasiOK = reservasiOk;
    $scope.deleteTindakan = deleteTindakan;
    $scope.deleteRacikan = deleteRacikan;
    $scope.deleteBphp = deleteBphp;
    $scope.deleteObat = deleteObat;
    $scope.deleteFarmasi = deleteFarmasi;
    $scope.deleteLaboratorium = deleteLaboratorium;
    $scope.racikanQtyChange = racikanQtyChange;
    $scope.racikanDiskonChange = racikanDiskonChange;
    $scope.tindakanQtyChange = tindakanQtyChange;
    $scope.tindakanDiskonChange = tindakanDiskonChange;
    $scope.bphpQtyChange = bphpQtyChange;
    $scope.bphpDiskonChange = bphpDiskonChange;
    $scope.obatQtyChange = obatQtyChange;
    $scope.obatDiskonChange = obatDiskonChange;
    $scope.selected = selected;
    $scope.clear = clear;
    $scope.save = save;

    // init function
    buildTable();
    initData();


    function initData(param) {
        if (typeof param === 'object' || param) {
            $q.all([
                $http.get(base_url + '/api/ods_odc/pemeriksaan_dokter/tindakan_by_pasien/' + param.pasien.id)
            ]).then(function (results) {
                console.log(results);
                $scope.dataTindakan = results[0].data.data;
            })
        }
    }

    function addRacikan(racikan) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRacikan.html',
            controller: 'ModalAddRacikanController',
            size: 'lg',
            resolve: {
                items: function () {
                    return {
                        racikan: racikan.nama,
                        data: $scope.data
                    };
                }
            }
        });

        modalInstance.result.then(then, dismiss);

        function then(selectedItem) {
            // $scope.selected = selectedItem;
            var _tarif = {
                racikan: racikan.nama,
                tarif: [],
                biaya: 0,

            };

            var totalBiaya = 0;

            _tarif.tarif = selectedItem;
            _tarif.biaya += _.sum(_.map(selectedItem, function (data) {
                return parseFloat(data.total);
            }));

            console.log(_tarif);

            $scope.isOpenAddRacikan = false;
            $scope.dataRacikanInduk.push(_tarif);
        }

    }

    function addTindakan(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddTindakan.html',
            controller: 'ModalAddTindakanController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                $scope.dataTindakan.push(data);
            });

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addBphp(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddBphp.html',
            controller: 'ModalAddBphpController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                $scope.dataBphp.push(data);
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addNonRacikan() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddNonRacikan.html',
            controller: 'ModalAddNonRacikanController',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                $scope.dataObat.push(data);
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addFarmasi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddFarmasi.html',
            controller: 'ModalAddFarmasiController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            _.each(selectedItem, function (data) {
                $scope.dataFarmasi.push(data);
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addLaboratorium(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddLaboratorium.html',
            controller: 'ModalAddLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.data.rujukan.rujuk_laboratorium = 1;
            console.log("data tindakan sebelum add lab,  ", $scope.dataTindakan);

            _.each(selectedItem.tindakan, function (tindakan) {
                $scope.dataTindakan.push(tindakan);
            });
            console.log("data tindakan sekarang, ", $scope.dataTindakan);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addRadiologi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRadiologi.html',
            controller: 'ModalAddRadiologiController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_radiologi = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCtScan(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCtScan.html',
            controller: 'ModalAddCtScanController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_ctscan = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addFisioterapi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddFisioterapi.html',
            controller: 'ModalAddFisioterapiController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_fisioterapi = 1;

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCatchLab(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCatchLab.html',
            controller: 'ModalAddCatchLabController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_catchlab = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addAudiometri(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddAudiometri.html',
            controller: 'ModalAddAudiometriController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_audiometri = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }


    function addDiagnostikFungsional(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddDiagnostikFungsional.html',
            controller: 'ModalAddDiagnostikFungsionalController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_diagnostik_fungsional = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addOdsOdc(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddOdsOdc.html',
            controller: 'ModalAddOdsOdcController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_ods_odc = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }


    function addRawatInap(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRawatInap.html',
            controller: 'ModalAddRawatInapController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            // $scope.dataObat.push(selectedItem);
            console.log("data rawat inap, ", selectedItem);
            $scope.data.rawatInap = selectedItem;
            $scope.data.rujukan.rujuk_rawatinap = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function reservasiOk(isReservasi) {
        if (isReservasi) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalReservasiOK.html',
                controller: 'ModalReservasiOKController',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $scope.data;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                // $scope.selected = selectedItem;
                // $scope.dataObat.push(selectedItem);
                console.log("data rawat inap, ", selectedItem);
                $scope.data.reservasiOk = selectedItem;
                $scope.data.rujukan.rujuk_ok = 1;
            }, function () {
                $scope.data.getReservasiOK = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

    }

    function save() {

        $rootScope.$emit('LOADING:EVENT:PROCESSING');

        $scope.data.tindakan = angular.copy($scope.dataTindakan);
        $scope.data.bphp = angular.copy($scope.dataBphp);
        $scope.data.obat = angular.copy($scope.dataObat);
        $scope.data.farmasi = angular.copy($scope.dataFarmasi);
        $scope.data.laboratorium = angular.copy($scope.dataLaboratorium);

        if ($scope.data.getRawatInap) {
            $scope.data.rawatInap = angular.copy($scope.rawatInap);
        }

        if ($scope.data.getFarmasi) {
            $scope.data.farmasi = angular.copy($scope.farmasi);
        }

        if ($scope.data.getLaboratorium) {
            $scope.data.laboratorium = angular.copy($scope.laboratorium);
        }

        console.log(angular.copy($scope.data));

        var req = {
            method: 'POST',
            url: base_url + '/api/ods_odc/pemeriksaan_dokter/save',
            data: 'data=' + JSON.stringify(angular.copy($scope.data)),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).then(function (result) {
            console.log("result save, ", result);
            $rootScope.$emit('LOADING:EVENT:FINISH');
            toastr.success('Simpan Rawat Jalan Berhasil !', 'Sukses');
        }, function (err) {
            console.log("err", err);
            toastr.error(err.toString(), 'Error');
        });

    }

    function selected(item) {

        if (item) {
            var obj = angular.copy(item.originalObject);
            initData({
                pasien: {
                    id: obj.pasien_id
                }
            });
            $scope.data = obj;
            $scope.data.tanggal = new Date($scope.data.tanggal);
            $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
            $scope.data.rujukan = {
                rujuk_ok: 0,
                rujuk_rawatinap: 0,
                rujuk_audiometri: 0,
                rujuk_catchlab: 0,
                rujuk_ctscan: 0,
                rujuk_diagnostik_fungsional: 0,
                rujuk_fisioterapi: 0,
                rujuk_laboratorium: 0,
                rujuk_ods_odc: 0,
                rujuk_radiologi: 0
            };
            console.log(angular.copy($scope.data));

        }
    }

    function deleteTindakan(idx) {
        $scope.dataTindakan.splice(idx, 1);
    }

    function deleteRacikan(idx) {
        $scope.dataRacikanInduk.splice(idx, 1);
    }

    function deleteBphp(idx) {
        $scope.dataBphp.splice(idx, 1);
    }

    function deleteObat(idx) {
        $scope.dataObat.splice(idx, 1);
    }

    function deleteFarmasi(idx) {
        $scope.dataFarmasi.splice(idx, 1);
    }

    function deleteLaboratorium(idx) {
        $scope.dataLaboratorium.splice(idx, 1);
    }

    function racikanQtyChange(idx, qty) {
        $scope.dataRacikanInduk[idx].qty = qty;
        $scope.dataRacikanInduk[idx].total = $scope.dataRacikanInduk[idx].biaya * qty;
        $scope.tarifTotalRacikanInduk = 0;
        $scope.tarifTotalRacikanInduk += parseFloat(_.sum(_.map($scope.dataRacikanInduk, function (data) {
            return parseFloat(data.total);
        })));
    }

    function racikanDiskonChange(idx, diskon) {
        if (diskon) {
            var diskon = diskon / 100;
            var diskonTotal = ($scope.dataRacikanInduk[idx].biaya) * diskon;
            var total = ($scope.dataRacikanInduk[idx].biaya) - diskonTotal;
            $scope.dataRacikanInduk[idx].total = total;
            $scope.tarifTotalRacikanInduk = 0;
            $scope.tarifTotalRacikanInduk += parseFloat(_.sum(_.map($scope.dataRacikanInduk, function (data) {
                return parseFloat(data.total);
            })));
        }
    }


    function tindakanQtyChange(idx, qty) {
        $scope.dataTindakan[idx].qty = qty;
        $scope.dataTindakan[idx].total = $scope.dataTindakan[idx].biaya * qty;
    }

    function tindakanDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) * diskon;
        var total = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) - diskonTotal;
        $scope.dataTindakan[idx].total = total;
    }

    function bphpQtyChange(idx, qty) {
        $scope.dataBphp[idx].qty = qty;
        $scope.dataBphp[idx].total = $scope.dataBphp[idx].biaya * qty;
    }

    function bphpDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataBphp[idx].biaya * $scope.dataBphp[idx].qty) * diskon;
        var total = ($scope.dataBphp[idx].biaya * $scope.dataBphp[idx].qty) - diskonTotal;
        $scope.dataBphp[idx].total = total;
    }

    function obatQtyChange(idx, qty) {
        $scope.dataObat[idx].qty = qty;
        $scope.dataObat[idx].total = $scope.dataObat[idx].biaya * qty;
    }

    function obatDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataObat[idx].biaya * $scope.dataObat[idx].qty) * diskon;
        var total = ($scope.dataObat[idx].biaya * $scope.dataObat[idx].qty) - diskonTotal;
        $scope.dataObat[idx].total = total;
        console.log("data obat");
        console.log($scope.dataObat[idx]);
    }

    function clear() {
        $scope.data = {};
    }

    /**
     * Build Table
     */
    function buildTable() {
        var url = base_url + '/api/ods_odc/pemeriksaan_awal/list_rawat_jalan/1';

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                url: url,
                type: 'POST',
                data: function (data, dtInstance) {

                    // data.columns[data.length - 1].name = "m_pegawai.nama";
                    // Modify the data object properties here before being passed to the server
                }
            })
            // or here
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('order', [[0, 'desc']])
            .withOption('createdRow', function (row) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withPaginationType('full_numbers');

        $scope.dtInstance = {};
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {

                return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s');
            }).withOption('searchable', false),
            DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No MR'),
            DTColumnBuilder.newColumn('no_register').withTitle('No Register'),
            DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
            DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
            DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter')

        ];

        $scope.dtInstance = {};
    }

    /**
     * Dismiss Modal
     */
    function dismiss() {
        $log.info('Modal dismissed at: ' + new Date());
    }


}








