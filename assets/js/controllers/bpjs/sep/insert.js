/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepInsertController', bpjsSepInsertController);

bpjsSepInsertController.$inject = ['$scope', '$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource', '$uibModal'];

function bpjsSepInsertController($scope, $rootScope, $http, CONFIG, EVENT, toastr, $resource, $uibModal) {
    var vm = this;
    vm.sep = {
        ppkPelayanan: '0110R005',
        lokasiLaka: '-',
        sep : ''
    };
    $scope.dataPoli = [];
    $.ajax({
        url: base_url + '/api/bpjs/sep/poli',
        type: 'GET',
        success: function (result) {
            var _result = JSON.parse(result);
            console.log(_result);

            if (_result.response.response) {
                $scope.dataPoli = _result.response.response.list;
            }

        },
        error: function (err) {
            console.log(err);
        }
    });
    $scope.urlDiagnosa = base_url + '/api/bpjs/referensi/diagnosa?diagnosa=';

    $scope.selected = function (item) {
        if (item)
            vm.sep.diagAwal = item.originalObject.kodeDiagnosa;
    };


    $scope.onChangeNoKartu = function(noKartu){
        if(noKartu.length == 13){
            $resource(base_url + '/api/bpjs/peserta/no_kartu/').get({
                no_kartu : noKartu
            }).$promise.then(function (result) {
                var result = JSON.parse(result.data);

                console.log(result);

                if(result.metadata.code == 200){
                    if(result.response.peserta){
                        // vm.peserta = result.response.peserta;
                        vm.sep.klsRawat = result.response.peserta.kelasTanggungan.kdKelas;
                        vm.sep.ppkRujukan = result.response.peserta.provUmum.kdProvider;

                        toastr.success(result.metadata.code, result.metadata.message);
                    }
                }else{
                    toastr.warning(result.metadata.code, result.metadata.message);
                }



            }).catch(function (err) {
                console.log(err);
            });
        }
    };

    vm.insert = insert;

    function insert(isPrint) {

        vm.sep.tglSep = moment(vm.sep.tglSep).format('YYYY-MM-DD hh:mm:ss');
        vm.sep.tglRujukan = moment(vm.sep.tglRujukan).format('YYYY-MM-DD hh:mm:ss');
        vm.sep.user = 'RS';
        vm.sep.poliTujuan = vm.sep.poli.kdPoli;
        
        delete vm.sep.poli;
        var _data = {
            "request": {
                "t_sep": angular.copy(vm.sep)
            }
        };
        console.log(JSON.stringify(_data));
        $rootScope.$emit('ajax:progress');
        $.ajax({
            url: base_url + '/api/bpjs/sep/insert',
            type: 'POST',
            data: 'data=' + JSON.stringify(_data),
            success: function (result) {
                var _result = JSON.parse(result);
                console.log(_result);
                if (!_result) {
                    $rootScope.$emit('ajax:stop');
                    toastr.error('Sistem Error', '');
                    return;
                }

                if (_result.response.metadata.code != 200) {
                    toastr.warning(_result.response.metadata.message, _result.response.metadata.code);
                } else {
                    vm.sep.noSep = _result.response.response;
                    toastr.success(_result.response.metadata.message, _result.response.metadata.code);
                    if (isPrint == true) {
                        openModalPrintSep('lg');
                    }
                }

                $rootScope.$emit('ajax:stop');


            },
            error: function (err) {
                console.log(err);
                $rootScope.$emit('ajax:stop');
            }
        });


    }

    function openModalPrintSep(size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalPrintSep.html',
            controller: 'ModalPrintSep',
            size: size,
            resolve: {
                items: function () {
                    return vm.sep;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {


        }, function () {

        });
    }


//    console.log("controller bpjs peserta");
}