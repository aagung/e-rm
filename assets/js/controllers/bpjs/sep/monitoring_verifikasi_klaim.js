/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsMonitoringController', bpjsMonitoringController);

bpjsMonitoringController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsMonitoringController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.sep = {};

    vm.getMonitoring = function(){
        vm.sep.tglMasuk = moment(vm.sep.tglMasuk).format('YYYY-MM-DD');
        vm.sep.tglKeluar = moment(vm.sep.tglKeluar).format('YYYY-MM-DD');
        var _data = angular.copy(vm.sep);

        $.ajax({
            url: base_url + '/api/bpjs/sep/monitoring',
            type: 'POST',
            data: 'data=' + JSON.stringify(_data),
            success: function (result) {

                var _result = JSON.parse(result);

                if (!_result) {
                    toastr.error('Sistem Error', '');
                    return;
                }

                if (_result.response.metadata.code != 200) {
                    toastr.warning(_result.response.metadata.message, _result.response.metadata.code);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    };

    console.log("controller bpjs peserta");
}