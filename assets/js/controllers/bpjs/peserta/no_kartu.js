angular.module('rsbt').controller('BpjsPesertaController', bpjsPesertaController);

bpjsPesertaController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsPesertaController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.peserta = {};

    vm.cekNoKartu = function () {
        $resource(base_url + '/api/bpjs/peserta/no_kartu/').get({
            no_kartu : vm.peserta.noKartu
        }).$promise.then(function (result) {
            var result = JSON.parse(result.data);
            console.log(result);


            if(result.metadata.code == 200){
                if(result.response.peserta){
                    vm.peserta = result.response.peserta;
                    toastr.success(result.metadata.code, result.metadata.message);
                }
            }else{
                toastr.warning(result.metadata.code, result.metadata.message);
            }



        }).catch(function (err) {
            console.log(err);
        });
    };

    vm.cekNoNik = function () {
        $resource(base_url + '/api/bpjs/peserta/no_nik/').get({
            no_nik : vm.peserta.noNik
        }).$promise.then(function (result) {
            var result = JSON.parse(result.data);
            console.log(result);


            if(result.metadata.code == 200){
                if(result.response.peserta){
                    vm.peserta = result.response.peserta;
                    toastr.success(result.metadata.code, result.metadata.message);
                }
            }else{
                toastr.warning(result.metadata.code, result.metadata.message);
            }

        }).catch(function (err) {
            console.log(err);
        });
    };

    console.log("controller bpjs peserta");
}