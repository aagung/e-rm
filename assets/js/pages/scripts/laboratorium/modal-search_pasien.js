$(() => {
    let initialize = () => {
        let columnsDef = [
            {
                "orderable": true,
                "data": "no_rm",
                "render": (data, type, row, meta) => {
                    return `<a class="select-pasien">${data}</a>`;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "nama",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "alamat",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "no_telepon_1",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "tanggal_lahir",
                "render": (data, type, row, meta) => {
                    return moment(data).format('DD/MM/YYYY');
                },
                "className": "text-center"
            }
        ];

        TABLE_SEARCH_PASIEN_DT = TABLE_SEARCH_PASIEN.DataTable({
            data: [],
            columns: columnsDef,
            "fnDrawCallback": function (oSettings) {
                //
            }
        });

        TABLE_SEARCH_PASIEN.on('click', '.select-pasien', function() {
            let trParent = $(this).parents('tr');
            let data = TABLE_SEARCH_PASIEN_DT.row(trParent).data();

            if (TABLE_SEARCH_PASIEN.onSelect) {
                TABLE_SEARCH_PASIEN.onSelect(data);
            }

            // FORM.find('#status_pasien_id').val(1).change();
            // FORM.find('#pasien_id').val(data.id);
            // FORM.find('#tmp_kabupaten_id').val(data.kabupaten_id);
            // FORM.find('#tmp_kecamatan_id').val(data.kecamatan_id);
            // FORM.find('#tmp_kelurahan_id').val(data.kelurahan_id);                    
            // FORM.find('#title_id').val(data.title_id).change();
            // FORM.find('#disp-no_rm').html(data.no_rm);
            // FORM.find('#nama').val(data.nama);
            // FORM.find(`input:radio[name=jenis_kelamin][value=${data.jenis_kelamin}]`).click();
            // FORM.find(`input:radio[name=kewarganegaraan][value=${data.kewarganegaraan}]`).click();
            // FORM.find('#alamat').val(data.alamat);
            // FORM.find('#provinsi_id').val(data.provinsi_id).change();
            // FORM.find('#kodepos').val(data.kodepos);
            // FORM.find('#jenis_identitas').val(data.jenis_identitas).change();
            // FORM.find('#no_identitas').val(data.no_identitas);
            // FORM.find('#jenis_telepon_1').val(data.jenis_telepon_1).change();
            // FORM.find('#no_telepon_1').val(data.no_telepon_1);
            // FORM.find('#jenis_telepon_2').val(data.jenis_telepon_2).change();
            // FORM.find('#no_telepon_2').val(data.no_telepon_2);
            // FORM.find('#tempat_lahir').val(data.tempat_lahir);
            // FORM.find('#tanggal_lahir').pickadate('picker').set('select', moment(data.tanggal_lahir).format('DD/MM/YYYY'), {format: 'dd/mm/yyyy'});
            // FORM.find('#golongan_darah').val(data.golongan_darah).change();
            // FORM.find('#agama').val(data.agama).change();
            // FORM.find('#status_kawin').val(data.status_kawin).change();
            // FORM.find('#pendidikan_id').val(data.pendidikan_id).change();
            // FORM.find('#pekerjaan_id').val(data.pekerjaan_id).change();
            // $.uniform.update();

            MODAL_SEARCH_PASIEN.modal('hide');
        });

        MODAL_SEARCH_PASIEN.on('search', (e, data) => {
            let no_rekam_medis = data.no_rekam_medis ? data.no_rekam_medis : '';
            let nama = data.nama ? data.nama : '';
            let telepon = data.telepon ? data.telepon : '';
            let tgl_lahir = data.tgl_lahir ? data.tgl_lahir : '';
            // Set Modal Search Value
            MODAL_SEARCH_PASIEN.find('[name=no_rekam_medis]').val(no_rekam_medis);
            MODAL_SEARCH_PASIEN.find('[name=nama]').val(nama);
            MODAL_SEARCH_PASIEN.find('[name=telepon]').val(telepon);
            if (tgl_lahir) MODAL_SEARCH_PASIEN.find('[name=tgl_lahir]').pickadate('picker').set('select', tgl_lahir, {format: 'dd/mm/yyyy'});

            blockElement(MODAL_SEARCH_PASIEN.find('.modal-dialog').selector);
            let modalShown = MODAL_SEARCH_PASIEN.hasClass('in');
            if (! modalShown) {
                MODAL_SEARCH_PASIEN.modal('show');
            }
            $.getJSON(URL.search_pasien.loadData.replace(':NO_REKAM_MEDIS', no_rekam_medis).replace(':NAMA', nama).replace(':TELEPON', telepon).replace(':TGL_LAHIR', tgl_lahir), (res, status) => {
                if (status === 'success') {
                    let data = res.data;

                    TABLE_SEARCH_PASIEN_DT.clear();
                    TABLE_SEARCH_PASIEN_DT.rows.add(data).draw();

                    MODAL_SEARCH_PASIEN.find('.modal-dialog').unblock();
                }
            });
        });

        MODAL_SEARCH_PASIEN.find('[name=tgl_lahir]').pickadate({
            format: 'dd/mm/yyyy',
            selectMonths: true,
            selectYears: 100,
            max: moment().format('dd/mm/yyyy')
        });

        MODAL_SEARCH_PASIEN.find('[data-action="cari"]').click(() => {
            MODAL_SEARCH_PASIEN.trigger('search', [{
                no_rekam_medis: MODAL_SEARCH_PASIEN.find('[name=no_rekam_medis]').val(),
                nama: MODAL_SEARCH_PASIEN.find('[name=nama]').val(),
                telepon: MODAL_SEARCH_PASIEN.find('[name=telepon]').val(),
                tgl_lahir: MODAL_SEARCH_PASIEN.find('[name=tgl_lahir]').pickadate('picker').get(),
            }]);
        });
    }

    initialize();
});