$(() => {
    let fillForm = (evt, uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                $(`[name="disp_jenis_pasien"][value="${data.jenis_pasien}"]`).prop('checked', true).trigger('change').uniform();

                if (data.jenis_pasien == 'rs') {
                    // Data Pasien
                    $("#disp-no_rm").html(data.no_rm);
                    $("#disp-nama_pasien").html(data.pasien);
                    $("#disp-jenis_kelamin").html(parseInt(data.jenis_kelamin) == "1" ? "Laki-Laki" : "Perempuan");
                    $("#disp-tanggal_lahir").html(moment(data.tanggal_lahir).format('DD/MM/YYYY'));
                    $("#disp-alamat").html(data.alamat);
                    $("#disp-telepon").html(data.telepon);
                } else {
                    // Data Pasien
                    $("#disp_input-nama_pasien").val(data.nama).trigger('change');
                    $(`[name="disp_input-jenis_kelamin"][value="${data.jenis_kelamin}"]`).prop('checked', true).trigger('change').uniform();
                    $("#disp_input-tanggal_lahir").pickadate('picker').set('select', moment(data.tanggal_lahir).format('DD/MM/YYYY'), {format: 'dd/mm/yyyy'}).trigger('change');
                    $("#disp_input-alamat").val(data.alamat).trigger('change');
                    $("#disp_input-telepon").val(data.telepon).trigger('change');
                }

                // Data Registrasi
                $("#disp-no_register").html(data.kode);
                $("#disp_input-tanggal").pickadate('picker').set('select', moment(data.tanggal).format('DD/MM/YYYY'), {format: 'dd/mm/yyyy'}).trigger('change');
                $("#disp_input-cara_bayar_id").val(data.cara_bayar_id).trigger('change');
                $("#disp_input-kontraktor_id").val(data.kontraktor_id).trigger('change');
                $("#disp_input-asuransi_id").val(data.asuransi_id).trigger('change');
                $("#disp_input-no_jaminan").val(data.no_jaminan).trigger('change');
                $("#disp_input-dokter_perujuk").val(data.dokter_perujuk).trigger('change');

                // Details
                TABLE_PEMERIKSAAN.trigger('clear');
                for (let detail of data.details) {
                    TABLE_PEMERIKSAAN.trigger('add', [{
                        id: detail.id,
                        rujukan_id: detail.rujukan_id,
                        pemeriksaan_id: detail.pemeriksaan_id,
                        tarif_pelayanan_id: detail.tarif_pelayanan_id,
                        harga: detail.harga,
                        discount_jenis: detail.discount_jenis,
                        discount: detail.discount,
                        quantity: detail.quantity,
                        kode: detail.kode,
                        pemeriksaan: detail.pemeriksaan,
                    }]);
                }


                // INPUTS
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#jenis_pasien").val(data.jenis_pasien);
                $("#pelayanan_id").val(data.pelayanan_id);
                $("#tanggal").val(data.tanggal);
                $("#pasien_id").val(data.pasien_id);
                $("#nama").val(data.nama);
                $("#jenis_kelamin").val(data.jenis_kelamin);
                $("#alamat").val(data.alamat);
                $("#no_identitas").val(data.no_identitas);
                $("#telepon").val(data.telepon);
                $("#tanggal_lahir").val(data.tanggal_lahir);
                $("#dokter_perujuk_id").val(data.dokter_perujuk_id);
                $("#dokter_perujuk").val(data.dokter_perujuk);
                $("#rujukan_dari").val(data.rujukan_dari);
                $("#layanan_id").val(data.layanan_id);
                $("#cara_bayar_id").val(data.cara_bayar_id);
                $("#kontraktor_id").val(data.kontraktor_id);
                $("#asuransi_id").val(data.asuransi_id);
                $("#no_jaminan").val(data.no_jaminan);
                $("#ruang_id").val(data.ruang_id);
                $("#kelas_id").val(data.kelas_id);
                $("#bed_id").val(data.bed_id);
                $("#action").val('verifikasi');

                console.log('fillForm', data);
            }
        });
    }

    let initializeSearchPasien = () => {
        $("#search_tgl_lahir").pickadate({
            format: 'dd/mm/yyyy',
            selectMonths: true,
            selectYears: 100,
            max: moment().format('dd/mm/yyyy')
        });

        BTN_SEARCH_PASIEN.click(() => {
            console.log('searchPasien');
            MODAL_SEARCH_PASIEN.trigger('search', [{
                no_rekam_medis: $("#search_no_rekam_medis").val(),
                nama: $("#search_nama").val(),
                telepon: $("#search_telepon").val(),
                tgl_lahir: $("#search_tgl_lahir").pickadate('picker').get(),
            }]);
        });

        TABLE_SEARCH_PASIEN.onSelect = (data) => {
            console.log('onSelect', data);
            
            // Fill Input
            $("#pasien_id").val(data.id);
            $("#nama").val(data.nama);
            $("#jenis_kelamin").val(data.jenis_kelamin);
            $("#alamat").val(data.alamat);
            $("#no_identitas").val(data.no_identitas);
            $("#telepon").val(data.no_telepon_1);
            $("#tanggal_lahir").val(data.tanggal_lahir);

            // Fill Disp
            $("#disp-no_rm").html(data.no_rm);
            $("#disp-nama_pasien").html(data.nama);
            $("#disp-jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
            $("#disp-tanggal_lahir").html(moment(data.tanggal_lahir).format('DD/MM/YYYY'));
            $("#disp-alamat").html(data.alamat);
            // $("#disp-umur").html(); // TODO
            $("#disp-telepon").html(data.no_telepon_1);
        }
    }

    let initializePemeriksaan = () => {
        TABLE_PEMERIKSAAN.on('clear', (e) => {
            TABLE_PEMERIKSAAN.find('tbody').empty();
            TABLE_PEMERIKSAAN.trigger('update_total');
        });

        TABLE_PEMERIKSAAN.on('add', (e, data) => {
            let tbody = TABLE_PEMERIKSAAN.find('tbody');
            tbody.find('tr.empty').remove();


            let tr = $("<tr/>")
                .appendTo(tbody);
            // INPUTS
            let input_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_id[]')
                .val(data.id)
                .appendTo(tr);
            let input_rujukan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_rujukan_id[]')
                .val(data.rujukan_id)
                .appendTo(tr);
            let input_pemeriksaan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_pemeriksaan_id[]')
                .val(data.pemeriksaan_id)
                .appendTo(tr);
            let input_tarif_pelayanan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_tarif_pelayanan_id[]')
                .val(data.tarif_pelayanan_id)
                .appendTo(tr);
            let input_harga = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_harga[]')
                .val(data.harga)
                .appendTo(tr);
            let input_discount_jenis = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_discount_jenis[]')
                .val(data.discount_jenis)
                .appendTo(tr);
            let input_discount = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_discount[]')
                .val(data.discount)
                .appendTo(tr);
            let input_quantity = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_quantity[]')
                .val(data.quantity)
                .appendTo(tr);

            let tdKode = $("<td/>")
                .html(data.kode)
                .appendTo(tr);


            let tdPemeriksaan = $("<td/>")
                .html(data.pemeriksaan)
                .appendTo(tr);

            let tdHarga = $("<td/>")
                .addClass('text-right')
                .html(numeral(data.harga).format(',.##'))
                .appendTo(tr);

            let tdDiscount = $("<td/>")
                .appendTo(tr);
            let dispInputDiscount = $("<input/>")
                .prop('type', 'text')
                .prop('name', 'detail_disp_input_discount[]')
                .addClass('form-control text-right')
                .appendTo(tdDiscount);
            dispInputDiscount.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputDiscount.autoNumeric('set', data.discount);

            let harga = numeral(data.harga)._value;
            let discount_jenis = data.discount_jenis;
            let discount = numeral(data.discount)._value;
            let total = 0;

            if (discount_jenis == 'persen') {
                total = harga * (1 - discount / 100);
            } else {
                total = harga - discount;
            }

            let tdJumlah = $("<td/>")
                .addClass('text-right disp_jumlah')
                .appendTo(tr);
            tdJumlah.html(numeral(total).format(',.##'));


            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);

            // Handlers
            btnDel.click(() => {
                tr.remove();
                TABLE_PEMERIKSAAN.trigger('update_total');
            });

            dispInputDiscount.on('change keyup blur', function () {
                let discount = numeral($(this).autoNumeric('get'))._value;
                let harga = numeral(input_harga.val())._value;
                let discount_jenis = input_discount_jenis.val();
                let total = 0;

                if (discount_jenis == 'persen') {
                    total = harga * (1 - discount / 100);
                } else {
                    total = harga - discount;
                }

                tdJumlah.html(numeral(total).format(',.##'));
                TABLE_PEMERIKSAAN.trigger('update_total');
            });

            TABLE_PEMERIKSAAN.trigger('update_total');
        });

        TABLE_PEMERIKSAAN.on('update', (e, data) => {
            let pemeriksaan_id = data.id;
            let tr = null;
            
            TABLE_PEMERIKSAAN.find('tbody').find('tr').each((i, el) => {
                if ($(el).find('[name="detail_pemeriksaan_id[]"]').val() == pemeriksaan_id) {
                    tr = $(el);
                }
            });

            if (tr) {
                //
            } else {
                TABLE_PEMERIKSAAN.trigger('add', [{
                    id: 0,
                    rujukan_id: 0,
                    pemeriksaan_id: data.id,
                    tarif_pelayanan_id: data.tarif_pelayanan_id,
                    harga: data.harga,
                    discount_jenis: 'nominal',
                    discount: 0,
                    quantity: 1,
                    kode: data.kode,
                    pemeriksaan: data.nama,
                }]);
            }

            TABLE_PEMERIKSAAN.trigger('update_total');
        });

        TABLE_PEMERIKSAAN.on('update_total', (e) => {
            let tbody = TABLE_PEMERIKSAAN.find('tbody');
            let total = 0;
            tbody.find('tr').each((i, el) => {
                let jumlah = numeral($(el).find('.disp_jumlah').html())._value;
                total += jumlah;
            });
            
            $("#subtotal-table-pemeriksaan").html('Subtotal: ' + numeral(total).format(',.##'));

            // Append Tidak Ada Data
            if (tbody.children().length == 0) {
                tbody.append(`
                    <tr class="empty">
                        <td colspan="6" class="text-center">Tidak Ada Data</td>
                    </tr>
                `);
            }
        });

        BTN_TAMBAH_PEMERIKSAAN.on('click', () => {
            let selected = [];
            // TODO: GET PEMERIKSAAN ID FROM TABLE_PEMERIKSAAN

            MODAL_LAB_SEARCH_PEMERIKSAAN.modal('show');
            blockElement(MODAL_LAB_SEARCH_PEMERIKSAAN.find('.modal-dialog').selector);
            $.getJSON(URL.search_pemeriksaan.loadData, (res, status) => {
                if (status === 'success') {
                    let data = res.data;
                    MODAL_LAB_SEARCH_PEMERIKSAAN.trigger('source', [data, selected]);
                    MODAL_LAB_SEARCH_PEMERIKSAAN.find('.modal-dialog').unblock();
                }
            });
        });

        MODAL_LAB_SEARCH_PEMERIKSAAN.onSave = (data) => {
            console.log('onSave', data);
            for (let d of data) {
                TABLE_PEMERIKSAAN.trigger('update', [d]);
            }

            MODAL_LAB_SEARCH_PEMERIKSAAN.modal('hide');
        };
    }

    let initialize = () => {
        FORM.on('fill', fillForm);
        FORM.validate({
            rules: {
                // TODO: Validasi
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Layanan berhasil disimpan.");

                        $('[type=submit]').prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Layanan.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_VERIFIKASI.click(() => {
            verifikasiDialog({
                url: URL.verifikasiLogin,
                type: 'default',
                data: {
                    pegawai_id: 0, // Sama dengan yang login saat ini
                },
                success: (result) => {
                    FORM.submit();
                }
            });
        });

        $("#disp_input-tanggal").pickadate({
            format: 'dd/mm/yyyy',
            selectMonths: true,
            selectYears: 2,
            min: moment().format('dd/mm/yyyy')
        });
        $("#disp_input-tanggal_lahir").pickadate({
            format: 'dd/mm/yyyy',
            selectMonths: true,
            selectYears: 100,
            max: moment().format('dd/mm/yyyy')
        });

        $('[name=disp_jenis_pasien]').on('change', function () {
            let jenis_pasien = $('[name=disp_jenis_pasien]:checked').val();

            if (jenis_pasien == 'rs') {
                $(".pasien-rs").show();
                $(".pasien-non_rs").hide();
            } else {
                $(".pasien-rs").hide();
                $(".pasien-non_rs").show();
            }
            
            // RESET DISP
            $("#disp-no_rm").html('&mdash;');
            $("#disp-nama_pasien").html('&mdash;');
            $("#disp-jenis_kelamin").html('&mdash;');
            $("#disp-tanggal_lahir").html('&mdash;');
            $("#disp-alamat").html('&mdash;');
            $("#disp-umur").html('&mdash;');
            $("#disp-telepon").html('&mdash;');
            $("#disp-no_register").html('&mdash;');
            $("#disp-tanggal").html('&mdash;');
            $("#disp-rujukan_dari").html('&mdash;');
            $("#disp-cara_bayar").html('&mdash;');
            $("#disp-perusahaan").html('&mdash;');
            $("#disp-no_jaminan").html('&mdash;');
            $("#disp-penjamin_perusahaan").html('&mdash;');
            $("#disp-penjamin_no_jaminan").html('&mdash;');
            $("#disp-layanan").html('&mdash;');
            $("#disp-dokter").html('&mdash;');

            // Reset DISP Input
            $("#disp_input-nama_pasien").val('').trigger('change');
            $('[name="disp_input-jenis_kelamin"]').prop('checked', false).trigger('change').uniform();
            $("#disp_input-tanggal_lahir").pickadate('picker').set('select', null, {format: 'dd/mm/yyyy'});
            $("#disp_input-tanggal_lahir").trigger('change');
            $("#disp_input-alamat").val('').trigger('change');
            $("#disp_input-telepon").val('').trigger('change');

            // Reset Input
            $("#jenis_pasien").val(jenis_pasien);
            $("#pelayanan_id").val(0);
            $("#tanggal").val('');
            $("#pasien_id").val(0);
            $("#nama").val('');
            $("#jenis_kelamin").val(0);
            $("#alamat").val('');
            $("#no_identitas").val('');
            $("#telepon").val('');
            $("#tanggal_lahir").val('');
            $("#dokter_perujuk_id").val(0);
            $("#dokter_perujuk").val('');
            $("#rujukan_dari").val('langsung');
            $("#layanan_id").val(0);
            $("#cara_bayar_id").val(0);
            $("#kontraktor_id").val(0);
            $("#asuransi_id").val(0);
            $("#ruang_id").val(0);
            $("#kelas_id").val(0);
            $("#bed_id").val(0);

            // Reset Search Pasien
            $("#search_no_rekam_medis").val('').trigger('change');
            $("#search_nama").val('').trigger('change');
            $("#search_telepon").val('').trigger('change');
            $("#search_tgl_lahir").pickadate('picker').set('select', null, {format: 'dd/mm/yyyy'});
            $("#search_tgl_lahir").trigger('change');
        });

        /**
         * Disp Input Data Registrasi Change, Keyup, Blur
         */
        $("#disp_input-tanggal").on('change keyup blur', function () {
            let value = $(this).pickadate('picker').get();
            let dt = value.split('/');
            $("#tanggal").val(dt[2] + '-' + dt[1] + '-' + dt[0]);
        });
        $("#disp_input-cara_bayar_id").on('change keyup blur', function () {
            let jenis = $("#disp_input-cara_bayar_id").find('option:selected').data('jenis')
            $("#cara_bayar_id").val($(this).val());

            switch (parseInt(jenis)) {
                case 4: // Asuransi
                    $('.section-kontraktor').hide();
                    $('.section-asuransi').show();
                    $("#disp_input-no_jaminan").val('');
                    $("#disp_input-kontraktor_id").val(0).trigger('change');
                    $("#disp_input-asuransi_id").val(0).trigger('change');
                    break;
                case 5: // Kontraktor
                    $('.section-asuransi').hide();
                    $('.section-kontraktor').show();
                    $("#disp_input-no_jaminan").val('');
                    $("#disp_input-kontraktor_id").val(0).trigger('change');
                    $("#disp_input-asuransi_id").val(0).trigger('change');
                    break;
                default:
                    $('.section-kontraktor').hide();
                    $('.section-asuransi').hide();
                    $("#disp_input-no_jaminan").val('');
                    $("#disp_input-kontraktor_id").val(0).trigger('change');
                    $("#disp_input-asuransi_id").val(0).trigger('change');
                    break;
            }
        });
        $("#disp_input-kontraktor_id").on('change keyup blur', function () {
            $("#kontraktor_id").val($(this).val());
        });
        $("#disp_input-asuransi_id").on('change keyup blur', function () {
            $("#asuransi_id").val($(this).val());
        });
        $("#disp_input-no_jaminan").on('change keyup blur', function () {
            $("#no_jaminan").val($(this).val());
        });
        $("#disp_input-dokter_perujuk").on('change keyup blur', function () {
            $("#dokter_perujuk").val($(this).val());
        });

        /**
         * Disp Input Pasien Non RS Change, keyup, blur
         */
        $("#disp_input-nama_pasien").on('change keyup blur', function () {
            $("#nama").val($(this).val());
        });
        $('[name="disp_input-jenis_kelamin"]').on('change keyup blur', function () {
            let value = $('[name="disp_input-jenis_kelamin"]:checked').val();
            $("#jenis_kelamin").val(value);
        });
        $("#disp_input-tanggal_lahir").on('change keyup blur', function () {
            let value = $(this).pickadate('picker').get();
            let dt = value.split('/');
            $("#tanggal_lahir").val(dt[2] + '-' + dt[1] + '-' + dt[0]);
        });
        $("#disp_input-alamat").on('change keyup blur', function () {
            let value = $(this).val();
            $("#alamat").val(value);
        });
        $("#disp_input-telepon").on('change keyup blur', function () {
            let value = $(this).val();
            $("#telepon").val(value);
        });




        initializeSearchPasien();
        initializePemeriksaan();


        $('[name=disp_jenis_pasien]').trigger('change');
        $("#disp_input-cara_bayar_id").trigger('change');
        $("#disp_input-tanggal").pickadate('picker').set('select', moment().format('DD/MM/YYYY'), {format: 'dd/mm/yyyy'});
        $("#disp_input-tanggal").trigger('change');
    }

    initialize();

    if (UID != "") {
        FORM.trigger('fill', [UID]);
    }
});