$(() => {
    let initializeDaftarKunjungan = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "tanggal_registrasi",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        let url_sampling = URL.form.replace(':UID', row.uid);
                        return `<a href="${url_sampling}">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "nama",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        if (data == 'langsung') {
                            return 'Rujukan Langsung';
                        }
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
                {
                    "data": "tanggal_registrasi",
                    "render": (data, type, row, meta) => {
                        let lama_tunggu = `<span data-popup="tooltip" data-title="Lama Tunggu" class="" data-tanggal="${data}">Lama Tunggu</span>`;

                        return lama_tunggu;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        // let url_sampling = URL.form.replace(':UID', data);
                        // let btnSampling = `<a href="${url_sampling}" data-popup="tooltip" data-title="Sampling" class="btn btn-primary btn-xs"><i class="icon-med-i-laboratory"></i></a>`;
                        let btnBatal = `<button type="button" data-popup="tooltip" data-title="Batal" class="btn btn-danger btn-xs batal" data-uid="${data}"><i class="icon-cross2"></i></button>`;;

                        return `${btnBatal}`;
                    },
                    "className": "text-center"
                }
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                TABLE_BADGE.html(n);
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        // BATAL
        TABLE.on('click', '.batal', function (e) {
            e.preventDefault();
            let uid = $(this).data('uid');

            batalDialog({
                title: 'Anda yakin ingin membatalkan rujukan tersebut?',
                btn_confirm: 'Ya, Batalkan',
                message_input_error: 'Silahkan isi alasan dibatalkannya rujukan.',
                callback: (text_alasan, close) => {
                    let postData = {uid: uid, alasan: text_alasan};
                    $.ajax({
                        url: URL.batal,
                        type: 'POST',
                        dataType: "json",
                        data: postData,
                        success: function (data) {
                            successMessage('Success', 'Rujukan berhasil dibatalkan.');
                            TABLE_DT.fnDraw(false);
                        },
                        error: function (error) {
                            console.log('ERROR', error);
                            errorMessage('Error', 'Terjadi kesalahan saat hendak membatalkan rujukan.');
                        },
                        complete: function () {
                            close();
                        }
                    });
                }
            });
        });

        BTN_REFRESH_TABLE.on('click', function () {
            TABLE_DT.fnDraw(false);
        });

        $('a[href="#tab-register"]').on('click', function () {
            TABLE_DT.fnDraw();
        });

        /**
         * FILTERS
         */
        $("#table_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 0);
        });
        $("#table_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 1);
        });
        $("#table_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 2);
        });
        $("#table_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 3);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_search_ruang_id").closest('.form-group').hide();
                    $("#table_search_poli_id").closest('.form-group').show();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_search_ruang_id").closest('.form-group').show();
                    $("#table_search_poli_id").closest('.form-group').hide();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_search_ruang_id").closest('.form-group').hide();
                    $("#table_search_poli_id").closest('.form-group').hide();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 4);
        });
        $("#table_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 5);
        });
        $("#table_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 6);
        });

        $("#table_search_layanan_id").trigger('change');

        /**
         * EVENT SOURCE
         */
         let listen = () => {
            if (TABLE_EVENT_SOURCE) {
                 TABLE_EVENT_SOURCE.close();
             }
             let eventId = 'laboratorium-daftar_kunjungan_lunas';
             TABLE_EVENT_SOURCE = new EventSource(URL.listen);
             TABLE_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DT.fnDraw(false);
                }
             });
         }

         listen();

         /**
          * LAMA TUNGGU
          */
        setInterval(() => {
            TABLE.find('tbody').find('tr').each((i, el) => {
                let tanggal = moment($(el).find('[data-tanggal]').data('tanggal'));
                let jam = moment().diff(tanggal, 'hours') % 24;
                let menit = moment().diff(tanggal, 'minutes') % 60;
                let detik = moment().diff(tanggal, 'seconds') % 60;

                let contentChunk = [];
                if (jam > 0) {
                    contentChunk.push(`${jam} Jam`);
                }
                if (menit > 0) {
                    contentChunk.push(`${menit} Menit`);
                }
                if (detik > 0) {
                    contentChunk.push(`${detik} Detik`);
                }
                $(el).find('[data-tanggal]').html(contentChunk.join(' '));
            });
        }, 1000);
    }

    let initializeDaftarKunjunganBatal = () => {
        // DATATABLE
        TABLE_BATAL_DT = TABLE_BATAL.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataBatal,
            "columns": [
                {
                    "data": "batal_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "nama",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        if (data == 'langsung') {
                            return 'Rujukan Langsung';
                        }
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
                {
                    "data": "petugas_batal",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "batal_alasan",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                }
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_BATAL_TIMER) clearTimeout(TABLE_BATAL_TIMER);
                TABLE_BATAL_TIMER = setTimeout(function () {
                    // blockElement(TABLE_BATAL.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE_BATAL.unblock();
                TABLE_BATAL.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE_BATAL.on('click', function () {
            TABLE_BATAL_DT.fnDraw(false);
        });

        $('a[href="#tab-batal"]').on('click', function () {
            TABLE_BATAL_DT.fnDraw();
        });

        /**
         * Init Daterangepicker, Autonumeric, dll
         */
         $("#table_batal_search_tanggal").daterangepicker({
             startDate: moment(),
             endDate: moment(),
             applyClass: "bg-slate-600",
             cancelClass: "btn-default",
             opens: "center",
             autoApply: true,
             locale: {
                 format: "DD/MM/YYYY"
             }
         });

        /**
         * FILTERS
         */
        $("#table_batal_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 0);
        });
        $("#table_batal_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 1);
        });
        $("#table_batal_search_tanggal").on('apply.daterangepicker', function (ev, picker) {
            TABLE_BATAL_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 2);
            TABLE_BATAL_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 3);
        });
        $("#table_batal_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 4);
        });
        $("#table_batal_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 5);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_batal_search_ruang_id").closest('.form-group').hide();
                    $("#table_batal_search_poli_id").closest('.form-group').show();
                    $("#table_batal_search_ruang_id").val(0).trigger('change');
                    $("#table_batal_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_batal_search_ruang_id").closest('.form-group').show();
                    $("#table_batal_search_poli_id").closest('.form-group').hide();
                    $("#table_batal_search_ruang_id").val(0).trigger('change');
                    $("#table_batal_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_batal_search_ruang_id").closest('.form-group').hide();
                    $("#table_batal_search_poli_id").closest('.form-group').hide();
                    $("#table_batal_search_ruang_id").val(0).trigger('change');
                    $("#table_batal_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_batal_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 6);
        });
        $("#table_batal_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_BATAL_DT.fnFilter(val, 7);
        });

        $("#table_batal_search_layanan_id").trigger('change');

        /**
         * EVENT SOURCE
         */
         let listen = () => {
            if (TABLE_BATAL_EVENT_SOURCE) {
                 TABLE_BATAL_EVENT_SOURCE.close();
             }
             let eventId = 'laboratorium-daftar_kunjungan_batal';
             TABLE_BATAL_EVENT_SOURCE = new EventSource(URL.listenBatal);
             TABLE_BATAL_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_BATAL_DT.fnDraw(false);
                }
             });
         }

         listen();
    }

    initializeDaftarKunjungan();
    initializeDaftarKunjunganBatal();
});