$(() => {
    /**
     * Set Form Mode
     */
    let setFormMode = (data) => {
        if (data.ruang_id) {
            $("#disp_kelas").closest('.form-group').show();
            $("#disp_ruang").closest('.form-group').show();
            $("#disp_bed").closest('.form-group').show();
        } else {
            $("#disp_kelas").closest('.form-group').hide();
            $("#disp_ruang").closest('.form-group').hide();
            $("#disp_bed").closest('.form-group').hide();
        }
    }

    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                console.log('fillForm', data);

                let tanggal_registrasi = moment(data.tanggal_registrasi);
                let tanggal_lahir = moment(data.tanggal_lahir);
                let umur_tahun = tanggal_registrasi.diff(tanggal_lahir, 'year');
                let umur_bulan = tanggal_registrasi.diff(tanggal_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal_registrasi.diff(tanggal_lahir, 'days') % 31.5);

                // Input
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#pelayanan_id").val(data.pelayanan_id);
                $("#jenis").val(data.jenis);
                $("#jenis_pasien").val(data.jenis_pasien);
                $("#pasien_id").val(data.pasien_id);
                $("#dokter_perujuk_id").val(data.dokter_perujuk_id);
                $("#dokter_perujuk").val(data.dokter_perujuk);
                $("#rujukan_dari").val(data.rujukan_dari);
                $("#layanan_id").val(data.layanan_id);
                $("#cara_bayar_id").val(data.cara_bayar_id);
                $("#kontraktor_id").val(data.kontraktor_id);
                $("#asuransi_id").val(data.asuransi_id);
                $("#ruang_id").val(data.ruang_id);
                $("#kelas_id").val(data.kelas_id);
                $("#bed_id").val(data.bed_id);
                $("#dokter_id").val(data.dokter_id).trigger('change');
                $("#diagnosa").val(data.diagnosa).trigger('change');

                $("#disp_tanggal_registrasi").html(tanggal_registrasi.format('DD/MM/YYYY HH:mm'));
                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_no_register").html(data.no_register);
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);

                // PASIEN
                $("#disp_no_rm").html(data.no_rm);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_alamat").html(data.alamat);
                $("#disp_tanggal_lahir").html(tanggal_lahir.format('DD/MM/YYYY'));
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_telepon").html(data.nomor_hp);

                // RAWAT INAP
                $("#disp_kelas").html(data.kelas);
                $("#disp_ruang").html(data.ruang);
                $("#disp_bed").html(data.bed);

                // Waktu Pengambilan Sample
                $("#disp_tanggal_sampling").data('daterangepicker').setStartDate(moment());
                $("#disp_tanggal_sampling").data('daterangepicker').setEndDate(moment());
                $("#tanggal_sampling").val(moment().format('YYYY-MM-DD'));
                $("#waktu_sampling").pickatime().pickatime('picker').set('select', moment().format('HH:mm'));

                setFormMode(data);

                // getPemeriksaan(uid);
                fetchPemeriksaan(uid);
            }
        });
    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                console.log('getPemeriksaan', data);
                initializeTreePemeriksaan(data);
            }
        });
    }

    let fetchPemeriksaan = (uid) => {
        $.getJSON(URL.fetchPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                for (let d of data) {
                    addPemeriksaan(d);
                }
            }
        });
    }
    let addPemeriksaan = (data) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');

        console.log('addPemeriksaan', data);

        let tr = $("<tr/>")
            .appendTo(tbody);
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_reg_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_reg_id[]')
            .val(data.reg_id)
            .appendTo(tr);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(tr);

        let tdPemeriksaan = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let labelPemeriksaan = $("<label/>")
            .addClass('label-pemeriksaan')
            .html(data.pemeriksaan)
            .appendTo(tdPemeriksaan);

        let tdBmhp = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnBmhp = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-primary btn-labeled btn-bmhp')
            .appendTo(tdBmhp);
        let labelBmhpQuantity = $("<b/>")
            .addClass('bmhp-count')
            .html(`<i>${data.bmhp.length}</i>`)
            .appendTo(btnBmhp);
        btnBmhp.append('Tambah');

        let tdSatuan = $("<td/>")
            .addClass('text-left')
            .html(data.satuan || '-')
            .appendTo(tr);

        let tdQuantity = $("<td/>")
            .addClass('text-left')
            .html(data.quantity || 1)
            .appendTo(tr);

        btnBmhp.on('click', () => {
            FORM_PENGGUNAAN_OBAT.trigger('fillForm', [{
                id: input_id.val(),
                reg_id: input_reg_id.val(),
                pemeriksaan_id: input_pemeriksaan_id.val(),
                bmhp: json_decode(input_bmhp.val()),
            }]);
        });
    }

    let initializeTreePemeriksaan = (data) => {
        console.log('initializeTreePemeriksaan', data);
        // Tree Tindakan
        TREE_TINDAKAN_TAG.fancytree({
            extensions: ["filter"],
            quicksearch: true,
            filter: {
                autoApply: true,   // Re-apply last filter if lazy data is loaded
                autoExpand: true, // Expand all branches that contain matches while filtered
                counter: false,     // Show a badge with number of matching child nodes near parent icons
                fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                highlight: true,   // Highlight matches by wrapping inside <mark> tags
                leavesOnly: false, // Match end nodes only
                nodata: true,      // Display a 'no data' status node if result is empty
                mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
            },
            source: data,
            checkbox: true,
            selectMode: 3,
        });

        TREE_TINDAKAN = TREE_TINDAKAN_TAG.fancytree("getTree");

        setTimeout(() => {
            var rootNode = TREE_TINDAKAN_TAG.fancytree("getRootNode");
            rootNode.sortChildren(function(a, b) {
                var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                return x === y ? 0 : x > y ? 1 : -1;
            }, true);

            // Hide Unselected Node
            // Untuk menghilangkan parent yang tidak dipilih sama sekali
            TREE_TINDAKAN.visit(function (node) {
                node.setExpanded(true);
                if (!node.partsel && !node.selected) {
                    $(node.li).addClass('hide');
                }
            });

            // untuk menghilangkan node dari parent yang dipilih (Partial|semua)
            TREE_TINDAKAN.visit(function (node) {
                if (!node.partsel && !node.selected) {
                    $(node.li).addClass('hide');
                }
            });

            $("<div/>")
                .css('z-index', '1000')
                .css('border', 'none')
                .css('margin', '0px')
                .css('padding', '0px')
                .css('width', '100%')
                .css('height', '100%')
                .css('top', '0px')
                .css('left', '0px')
                .css('position', 'absolute')
                .appendTo(TREE_TINDAKAN_TAG);
        }, 500);
    };

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Sampling berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Sampling.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });

        /**
         * Inputs
         */
        FORM.find('[name=disp_tanggal_sampling]').daterangepicker({
            singleDatePicker: true,
            startDate: moment(),
            endDate: moment(),
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            }
        });
        FORM.find('[name=disp_tanggal_sampling]').on('apply.daterangepicker, change, keyup', (ev, picker) => {
            FORM.find('[name=tanggal_sampling]').val(picker.startDate.format('YYYY-MM-DD'));
        });

        FORM.find('[name=waktu_sampling]').pickatime({
            format: 'HH:i',
            formatLabel: 'HH:i',
            formatSubmit: 'HH:i',
        });
    }

    let initializePenggunaanObat = () => {
        FORM_PENGGUNAAN_OBAT.on('add', (evt, data) => {
            console.log('FORM_PENGGUNAAN_OBAT.add', data);
            let tbody = TABLE_PENGGUNAAN_OBAT.find('tbody');

            let tr = $("<tr/>")
                .appendTo(tbody);

            let input_obat = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat[]')
                .val(data.obat)
                .appendTo(tr);
            let input_obat_alias = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_alias[]')
                .val(data.obat_alias)
                .appendTo(tr);
            let input_obat_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_id[]')
                .val(data.obat_id)
                .appendTo(tr);
            let input_obat_kode = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_kode[]')
                .val(data.obat_kode)
                .appendTo(tr);
            let input_pemeriksaan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_pemeriksaan_id[]')
                .val(data.pemeriksaan_id)
                .appendTo(tr);
            let input_quantity = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_quantity[]')
                .val(data.quantity)
                .appendTo(tr);
            let input_satuan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan[]')
                .val(data.satuan)
                .appendTo(tr);
            let input_satuan_singkatan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan_singkatan[]')
                .val(data.satuan_singkatan)
                .appendTo(tr);

            let tdKode = $("<td/>")
                .addClass('text-center')
                .html(data.obat_kode)
                .appendTo(tr);

            let tdNama = $("<td/>")
                .addClass('text-left')
                .html(data.obat)
                .appendTo(tr);

            let tdQuantity = $("<td/>")
                .addClass('text-left')
                .appendTo(tr);
            let dispInputQuantity = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control')
                .appendTo(tdQuantity);
            dispInputQuantity.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputQuantity.autoNumeric('set', data.quantity);

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let btnDelete = $("<button/>")
                .addClass('btn btn-danger btn-xs btn-delete')
                .html('<i class="icon-cross3"></i>')
                .appendTo(tdAction);

            btnDelete.on('click', () => {
                tr.remove();
            });

            dispInputQuantity.on('change keyup blur', () => {
                input_quantity.val(dispInputQuantity.autoNumeric('get'));
            });
            dispInputQuantity.on('click', () => {
                dispInputQuantity.select();
            });
        });
        FORM_PENGGUNAAN_OBAT.on('clear', (evt) => {
            TABLE_PENGGUNAAN_OBAT.find('tbody').empty();
        })

        FORM_PENGGUNAAN_OBAT.on('fillForm', (evt, data) => {
            // TODO SHOW PENGGUNAAN OBAT
            FORM_PENGGUNAAN_OBAT.find('[name="id"]').val(data.id);
            FORM_PENGGUNAAN_OBAT.find('[name="pemeriksaan_id"]').val(data.pemeriksaan_id);
            FORM_PENGGUNAAN_OBAT.find('[name="reg_id"]').val(data.reg_id);

            FORM_PENGGUNAAN_OBAT.trigger('clear');
            for (let bmhp of data.bmhp) {
                FORM_PENGGUNAAN_OBAT.trigger('add', [bmhp]);
            }

            MODAL_PENGGUNAAN_OBAT.modal('show');
        });

        BTN_TAMBAH_PENGGUNAAN_OBAT.on('click', () => {
            MODAL_PENGGUNAAN_OBAT.modal('hide');
            MODAL_LOOKUP_OBAT.modal('show');
        });

        /**
         * FORM VALIDATION
         */
        FORM_PENGGUNAAN_OBAT.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockElement(MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').selector);
                let postData = $(form).serializeArray();

                let data = {};
                let d, fieldname, isArray;
                for (let i = 0; i < postData.length; i++) {
                    d = postData[i];
                    isArray = false;

                    if (d.name.search(/\[\]/) !== -1) {
                        fieldname = d.name.replace(/\[\]/, '');
                        isArray = true;
                    } else {
                        fieldname = d.name;
                        isArray = false;
                    }

                    if (isArray) {
                        if (! data[fieldname]) {
                            data[fieldname] = [];
                            data[fieldname].push(d.value);
                        } else {
                            data[fieldname].push(d.value)
                        }
                    } else {
                        data[fieldname] = d.value;
                    }
                }

                console.log('DATA', data);
                let obat;
                let list_obat = [];
                if (data.penggunaan_bmhp_obat_id) {
                    for (let i = 0; i < data.penggunaan_bmhp_obat_id.length; i++) {
                        obat = {
                            obat: data['penggunaan_bmhp_obat'][i],
                            obat_alias: data['penggunaan_bmhp_obat_alias'][i],
                            obat_id: data['penggunaan_bmhp_obat_id'][i],
                            obat_kode: data['penggunaan_bmhp_obat_kode'][i],
                            pemeriksaan_id: data['id'],
                            quantity: data['penggunaan_bmhp_quantity'][i],
                            satuan: data['penggunaan_bmhp_satuan'][i],
                            satuan_singkatan: data['penggunaan_bmhp_satuan_singkatan'][i],
                        };
                        list_obat.push(obat);
                    }
                }

                TABLE_PEMERIKSAAN.find('tbody').find('tr').each((i, el) => {
                    if ($(el).find('[name="pemeriksaan_id[]"]').val() == data.id) {
                        $(el).find('[name="pemeriksaan_bmhp[]"]').val(json_encode(list_obat));
                        $(el).find('.bmhp-count').html('<i>' + list_obat.length + '</i>');
                    }
                });

                MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').unblock();
                MODAL_PENGGUNAAN_OBAT.modal('hide');
            }
        });
    }

    let initializeLookupObat = () => {
        /**
         * COLUMNS OPTIONS
         */
        let COLUMNS_OPT_LOOKUP_OBAT = [
            {
                "orderable": false,
                "render": (data, type, row, meta) => {
                    let dataAttr = `data-id="${row.id}" data-uid="${row.uid}" data-kode="${row.kode}" data-nama="${row.nama}" data-alias="${row.alias}" data-satuan="" data-satuan_singkatan=""`;

                    let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

                    return checkbox;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "kode",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "nama",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            // {
            //     "orderable": true,
            //     "data": "jenis",
            //     "render": (data, type, row, meta) => {
            //         return data;
            //     },
            //     "className": "text-left"
            // }
        ];

        TABLE_LOOKUP_OBAT.on('fetch', (evt, cb) => {
            $.getJSON(URL.fetchLookupObat, function (res, status) {
                if (status === 'success') {
                    TABLE_LOOKUP_OBAT_DT.clear().draw();
                    TABLE_LOOKUP_OBAT_DT.rows.add(res.data).draw();

                    if (cb) {
                        cb();
                    }
                } else {
                    //
                }
            });
        });

        TABLE_LOOKUP_OBAT.on('initialize', (evt) => {
            TABLE_LOOKUP_OBAT_DT = TABLE_LOOKUP_OBAT.DataTable({
                data: [],
                columns: COLUMNS_OPT_LOOKUP_OBAT,
                fnDrawCallback: function (oSettings) {
                    TABLE_LOOKUP_OBAT.find('tbody tr input[type=checkbox]').uniform();
                }
            });
        });

        BTN_LOOKUP_OBAT_SIMPAN.on('click', (evt) => {
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    let obat = {
                        obat: tr.find('input[type=checkbox]').data('nama'),
                        obat_alias: tr.find('input[type=checkbox]').data('alias'),
                        obat_id: tr.find('input[type=checkbox]').data('id'),
                        obat_kode: tr.find('input[type=checkbox]').data('kode'),
                        pemeriksaan_id: 0, // TODO PEMERIKSAAN ID
                        quantity: 1,
                        satuan: tr.find('input[type=checkbox]').data('satuan'),
                        satuan_singkatan: tr.find('input[type=checkbox]').data('satuan_singkatan'),
                    }

                    if (tr.find('input[type=checkbox]').is(':checked')) {
                        TABLE_PENGGUNAAN_OBAT.trigger('add', [obat]);
                    }
                });

                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
                MODAL_LOOKUP_OBAT.modal('hide');
            }, 500);
        });

        MODAL_LOOKUP_OBAT.on('shown.bs.modal', () => {
            TABLE_LOOKUP_OBAT.trigger('fetch');
        });
        MODAL_LOOKUP_OBAT.on('hide.bs.modal', () => {
            MODAL_PENGGUNAAN_OBAT.modal('show');
        });


        TABLE_LOOKUP_OBAT.trigger('initialize');
    }

    fillForm(UID);
    initializeForm();
    initializeLookupObat();
    initializePenggunaanObat();
});