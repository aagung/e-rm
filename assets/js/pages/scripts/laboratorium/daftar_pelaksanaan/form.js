$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                
                let tanggal = moment(data.created_at);
                let tanggal_lahir = moment(data.tanggal_lahir);
                let umur_tahun = tanggal.diff(tanggal_lahir, 'year');
                let umur_bulan = tanggal.diff(tanggal_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal.diff(tanggal_lahir, 'days') % 31.5);

                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_tanggal").html(tanggal.format('DD/MM/YYYY'));
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);
                $("#disp_dokter").html(data.dokter);
                $("#disp_diagnosa").html(data.diagnosa);
                $("#disp_no_rm").html(data.no_rm);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_poli_ruang").html(parseInt(data.ruang_id) > 0 ? data.ruang : data.layanan);

                /**
                 * Alasan Penolakan
                 */
                if (parseInt(data.reject_flag) == 1) {
                    $("#alert-reject").show();
                    $("#alert-reject").find('.content').html(data.reject_alasan);
                } else {
                    $("#alert-reject").hide();
                }

                /**
                 * INPUT HIDDEN
                 */
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#register_id").val(data.register_id);
                console.log('fillForm', data);
                getPemeriksaan(uid);
            }
        });
    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                HASIL_CONTAINER.empty();
                for (let d of data) {
                    addPemeriksaan(d);
                }
            }
        });
    }

    let addPemeriksaan = (data) => {
        if (parseInt(data.patologi_flag) == 1) {
            addPemeriksaanPatologi(data);
            return false;
        }

        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_patologi_flag = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_patologi_flag[]')
            .val(data.patologi_flag)
            .appendTo(col);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan ${data.nama}</strong>`);
        fieldsetLegend.append(`&nbsp;&nbsp;`);
        let btnBmhp = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-primary btn-labeled btn-bmhp')
            .appendTo(fieldsetLegend);
        let labelBmhpQuantity = $("<b/>")
            .addClass('bmhp-count')
            .html(`<i>${data.bmhp.length}</i>`)
            .appendTo(btnBmhp);
        btnBmhp.append('BMHP');

        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);

        let tableDiv = $("<div/>")
            .addClass('table-responsive')
            .appendTo(fieldsetCol);
        let table = $("<table/>")
            .addClass('table table-bordered')
            .appendTo(tableDiv);
        let tableHead = $("<thead/>")
            .appendTo(table);
        let tableBody = $("<tbody/>")
            .appendTo(table);
        tableHead.html(`
            <tr class="bg-slate">
                <th class="text-center" style="width: 35%;">
                    Pemeriksaan
                </th>
                <th class="text-center">
                    Hasil
                </th>
                <th class="text-center" style="width: 20%;">
                    Satuan
                </th>
                <th class="text-center" style="width: 20%;">
                    Nilai Rujukan
                </th>
            </tr>
        `);

        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10')
            .val(data.catatan)
            .appendTo(col);

        row.append('<br/><br/><br/>');

        // HASIL
        for (let hasil of data.hasil) {
            addPemeriksaanHasil(hasil, tableBody);
        }

        // Handler
        btnBmhp.on('click', () => {
            FORM_PENGGUNAAN_OBAT.trigger('fillForm', [{
                id: input_id.val(),
                bmhp: json_decode(input_bmhp.val()),
            }]);
        });
    }

    let addPemeriksaanPatologi = (data) => {
        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_patologi_flag = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_patologi_flag[]')
            .val(data.patologi_flag)
            .appendTo(data.patologi_flag);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);
        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10 hide')
            .val(data.catatan)
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan ${data.nama}</strong>`);
        fieldsetLegend.append(`&nbsp;&nbsp;`);
        let btnBmhp = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-primary btn-labeled btn-bmhp')
            .appendTo(fieldsetLegend);
        let labelBmhpQuantity = $("<b/>")
            .addClass('bmhp-count')
            .html(`<i>${data.bmhp.length}</i>`)
            .appendTo(btnBmhp);
        btnBmhp.append('BMHP');

        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);
        let dispCatatan = $("<div/>")
            .addClass('disp_catatan')
            .appendTo(fieldsetCol);
        dispCatatan.summernote();
        dispCatatan.summernote('code', data.catatan);

        row.append('<br/><br/><br/>');

        // Handler
        btnBmhp.on('click', () => {
            FORM_PENGGUNAAN_OBAT.trigger('fillForm', [{
                id: input_id.val(),
                bmhp: json_decode(input_bmhp.val()),
            }]);
        });
    }

    let addPemeriksaanHasil = (data, tbody) => {
        let tr = $("<tr/>")
            .appendTo(tbody);

        let input_id = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_pemeriksaan_id = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_kode = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_kode[]')
            .val(data.kode)
            .appendTo(tr);
        let input_nama = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_nama[]')
            .val(data.nama)
            .appendTo(tr);
        let input_datatype = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_datatype[]')
            .val(data.datatype)
            .appendTo(tr);
        let input_value = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_value[]')
            .val(data.value)
            .appendTo(tr);
        let input_satuan = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_satuan[]')
            .val(data.satuan)
            .appendTo(tr);
        let input_nilai_normal = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_nilai_normal[]')
            .val(data.nilai_normal)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .prop('colspan', 1)
            .html(data.nama)
            .appendTo(tr);

        let tdHasil = $("<td/>")
            .prop('colspan', 1)
            .appendTo(tr);
        let dispInputHasil = getInputHasil(tdHasil, input_value, data);
        // TODO

        let tdSatuan = $("<td/>")
            .prop('colspan', 1)
            .html(data.satuan)
            .appendTo(tr);

        let tdNilaiRujukan = $("<td/>")
            .prop('colspan', 1)
            .html(data.nilai_normal.replace("\r\n", '<br/>').replace("\n", '<br/>'))
            .appendTo(tr);
    }

    let getInputHasil = (td, input, data) => {
        let dispInput;
        switch (data.datatype) {
            /*case 'range':
                let values = data.value.split('-');
                let min_value = numeral(values.shift() || 0)._value;
                let max_value = numeral(values.shift() || 0)._value;
                let row = $("<div/>")
                    .addClass('row')
                    .appendTo(td);
                let colLeft = $("<div/>")
                    .addClass('col-sm-5')
                    .appendTo(row);
                let colSep = $("<div/>")
                    .addClass('col-sm-2')
                    .html('<div class="form-control-static">s/d</div>')
                    .appendTo(row);
                let colRight = $("<div/>")
                    .addClass('col-sm-5')
                    .appendTo(row);

                let dispInputMin = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(colLeft);
                dispInputMin.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p' });
                dispInputMin.autoNumeric('set', min_value);

                let dispInputMax = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(colRight);
                dispInputMax.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p' });
                dispInputMax.autoNumeric('set', max_value);

                let inputHandler = (e) => {
                    let min = dispInputMin.autoNumeric('get');
                    let max = dispInputMax.autoNumeric('get');

                    input.val(min + '-' + max);
                };
                dispInputMin.on('change keyup blur', inputHandler);
                dispInputMax.on('change keyup blur', inputHandler);
                dispInputMin.trigger('change');
                break;
            */
            case 'boolean':
                dispInput = $("<select/>")
                    .addClass('form-control')
                    .appendTo(td);

                dispInput.append($("<option/>").prop('value', 0).html('Negatif'));
                dispInput.append($("<option/>").prop('value', 1).html('Positif'));

                dispInput.on('change', (e) => {
                    input.val(dispInput.val());
                });

                dispInput.val(data.value);
                dispInput.trigger('change');
                break;
            case 'range':
                dispInput = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'lt':
                dispInput = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' < ', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'gt':
                dispInput = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' > ', 'pSign': 's' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'lte':
                dispInput = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' <= ', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'gte':
                dispInput = $("<input/>")
                    .prop('type', 'text')
                    .addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' >= ', 'pSign': 's' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'text':
            default:
                dispInput = $("<textarea/>")
                    .addClass('form-control')
                    .appendTo(td);

                dispInput.on('change keyup blur', (e) => {
                    input.val(dispInput.val());
                });

                dispInput.val(data.value);
                dispInput.trigger('change');
                break;
        }
    }

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                HASIL_CONTAINER.find('.row-pemeriksaan').each((i, el) => {
                    let dispCatatan = $(el).find('.disp_catatan');
                    let inputCatatan = $(el).find('[name="pemeriksaan_catatan[]"]');
                    if (dispCatatan.length > 0) {
                        inputCatatan.val(dispCatatan.summernote('code'));
                    }
                });

                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Hasil berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Hasil.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });
    }

    let initializePenggunaanObat = () => {
        FORM_PENGGUNAAN_OBAT.on('add', (evt, data) => {
            console.log('FORM_PENGGUNAAN_OBAT.add', data);
            let tbody = TABLE_PENGGUNAAN_OBAT.find('tbody');

            let tr = $("<tr/>")
                .appendTo(tbody);

            let input_obat = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat[]')
                .val(data.obat)
                .appendTo(tr);
            let input_obat_alias = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_alias[]')
                .val(data.obat_alias)
                .appendTo(tr);
            let input_obat_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_id[]')
                .val(data.obat_id)
                .appendTo(tr);
            let input_obat_kode = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_kode[]')
                .val(data.obat_kode)
                .appendTo(tr);
            let input_pemeriksaan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_pemeriksaan_id[]')
                .val(data.pemeriksaan_id)
                .appendTo(tr);
            let input_quantity = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_quantity[]')
                .val(data.quantity)
                .appendTo(tr);
            let input_satuan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan[]')
                .val(data.satuan)
                .appendTo(tr);
            let input_satuan_singkatan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan_singkatan[]')
                .val(data.satuan_singkatan)
                .appendTo(tr);

            let tdKode = $("<td/>")
                .addClass('text-center')
                .html(data.obat_kode)
                .appendTo(tr);

            let tdNama = $("<td/>")
                .addClass('text-left')
                .html(data.obat)
                .appendTo(tr);

            let tdQuantity = $("<td/>")
                .addClass('text-left')
                .appendTo(tr);
            let dispInputQuantity = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control')
                .appendTo(tdQuantity);
            dispInputQuantity.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputQuantity.autoNumeric('set', data.quantity);

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let btnDelete = $("<button/>")
                .addClass('btn btn-danger btn-xs btn-delete')
                .html('<i class="icon-cross3"></i>')
                .appendTo(tdAction);

            btnDelete.on('click', () => {
                tr.remove();
            });

            dispInputQuantity.on('change keyup blur', () => {
                input_quantity.val(dispInputQuantity.autoNumeric('get'));
            });
            dispInputQuantity.on('click', () => {
                dispInputQuantity.select();
            });
        });
        FORM_PENGGUNAAN_OBAT.on('clear', (evt) => {
            TABLE_PENGGUNAAN_OBAT.find('tbody').empty();
        })

        FORM_PENGGUNAAN_OBAT.on('fillForm', (evt, data) => {
            // TODO SHOW PENGGUNAAN OBAT
            FORM_PENGGUNAAN_OBAT.find('[name="id"]').val(data.id);

            FORM_PENGGUNAAN_OBAT.trigger('clear');
            for (let bmhp of data.bmhp) {
                FORM_PENGGUNAAN_OBAT.trigger('add', [bmhp]);
            }

            MODAL_PENGGUNAAN_OBAT.modal('show');
        });

        BTN_TAMBAH_PENGGUNAAN_OBAT.on('click', () => {
            MODAL_PENGGUNAAN_OBAT.modal('hide');
            MODAL_LOOKUP_OBAT.modal('show');
        });

        /**
         * FORM VALIDATION
         */
        FORM_PENGGUNAAN_OBAT.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockElement(MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').selector);
                let postData = $(form).serializeArray();

                let data = {};
                let d, fieldname, isArray;
                for (let i = 0; i < postData.length; i++) {
                    d = postData[i];
                    isArray = false;

                    if (d.name.search(/\[\]/) !== -1) {
                        fieldname = d.name.replace(/\[\]/, '');
                        isArray = true;
                    } else {
                        fieldname = d.name;
                        isArray = false;
                    }

                    if (isArray) {
                        if (! data[fieldname]) {
                            data[fieldname] = [];
                            data[fieldname].push(d.value);
                        } else {
                            data[fieldname].push(d.value)
                        }
                    } else {
                        data[fieldname] = d.value;
                    }
                }

                console.log('DATA', data);
                let obat;
                let list_obat = [];
                if (data.penggunaan_bmhp_obat_id) {
                    for (let i = 0; i < data.penggunaan_bmhp_obat_id.length; i++) {
                        obat = {
                            obat: data['penggunaan_bmhp_obat'][i],
                            obat_alias: data['penggunaan_bmhp_obat_alias'][i],
                            obat_id: data['penggunaan_bmhp_obat_id'][i],
                            obat_kode: data['penggunaan_bmhp_obat_kode'][i],
                            pemeriksaan_id: data['id'],
                            quantity: data['penggunaan_bmhp_quantity'][i],
                            satuan: data['penggunaan_bmhp_satuan'][i],
                            satuan_singkatan: data['penggunaan_bmhp_satuan_singkatan'][i],
                        };
                        list_obat.push(obat);
                    }
                }

                HASIL_CONTAINER.find('.row-pemeriksaan').each((i, el) => {
                    if ($(el).find('[name="pemeriksaan_id[]"]').val() == data.id) {
                        $(el).find('[name="pemeriksaan_bmhp[]"]').val(json_encode(list_obat));
                        $(el).find('.bmhp-count').html('<i>' + list_obat.length + '</i>');
                    }
                });

                MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').unblock();
                MODAL_PENGGUNAAN_OBAT.modal('hide');
            }
        });
    }

    let initializeLookupObat = () => {
        /**
         * COLUMNS OPTIONS
         */
        let COLUMNS_OPT_LOOKUP_OBAT = [
            {
                "orderable": false,
                "render": (data, type, row, meta) => {
                    let dataAttr = `data-id="${row.id}" data-uid="${row.uid}" data-kode="${row.kode}" data-nama="${row.nama}" data-alias="${row.alias}" data-satuan="" data-satuan_singkatan=""`;

                    let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

                    return checkbox;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "kode",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "nama",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            // {
            //     "orderable": true,
            //     "data": "jenis",
            //     "render": (data, type, row, meta) => {
            //         return data;
            //     },
            //     "className": "text-left"
            // }
        ];

        TABLE_LOOKUP_OBAT.on('fetch', (evt, cb) => {
            $.getJSON(URL.fetchLookupObat, function (res, status) {
                if (status === 'success') {
                    TABLE_LOOKUP_OBAT_DT.clear().draw();
                    TABLE_LOOKUP_OBAT_DT.rows.add(res.data).draw();

                    if (cb) {
                        cb();
                    }
                } else {
                    //
                }
            });
        });

        TABLE_LOOKUP_OBAT.on('initialize', (evt) => {
            TABLE_LOOKUP_OBAT_DT = TABLE_LOOKUP_OBAT.DataTable({
                data: [],
                columns: COLUMNS_OPT_LOOKUP_OBAT,
                fnDrawCallback: function (oSettings) {
                    TABLE_LOOKUP_OBAT.find('tbody tr input[type=checkbox]').uniform();
                }
            });
        });

        BTN_LOOKUP_OBAT_SIMPAN.on('click', (evt) => {
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    let obat = {
                        obat: tr.find('input[type=checkbox]').data('nama'),
                        obat_alias: tr.find('input[type=checkbox]').data('alias'),
                        obat_id: tr.find('input[type=checkbox]').data('id'),
                        obat_kode: tr.find('input[type=checkbox]').data('kode'),
                        pemeriksaan_id: 0, // TODO PEMERIKSAAN ID
                        quantity: 1,
                        satuan: tr.find('input[type=checkbox]').data('satuan'),
                        satuan_singkatan: tr.find('input[type=checkbox]').data('satuan_singkatan'),
                    }

                    if (tr.find('input[type=checkbox]').is(':checked')) {
                        TABLE_PENGGUNAAN_OBAT.trigger('add', [obat]);
                    }
                });

                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
                MODAL_LOOKUP_OBAT.modal('hide');
            }, 500);
        });

        MODAL_LOOKUP_OBAT.on('shown.bs.modal', () => {
            TABLE_LOOKUP_OBAT.trigger('fetch');
        });
        MODAL_LOOKUP_OBAT.on('hide.bs.modal', () => {
            MODAL_PENGGUNAAN_OBAT.modal('show');
        });


        TABLE_LOOKUP_OBAT.trigger('initialize');
    }

    fillForm(UID);
    initializeForm();
    initializePenggunaanObat();
    initializeLookupObat();
});