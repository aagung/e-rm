$(() => {
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    $(".select-signa").select2({
        placeholder: "- Pilih -",
    });

    function initializeSigna(el, signa_id) {
        $.getJSON(URL.getSigna, function(data, status) {
            if (status === "success") {
                let option = '';
                let arrSignaId = signa_id.split(",");
                for (var i = 0; i < data.data.length; i++) {
                    for(var j = 0; j < arrSignaId.length; j++) {
                        let selected = "";
                        if (arrSignaId[j] === data.data[i].id) selected = "selected='selected'";
                        option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].label}</option>`;
                    }
                }
                el.html(option).trigger("change");
            }
        });
    }
    initializeSigna($("#modal-input_racikan_signa"), "");

    // METODE RACIKAN
    function initializeMetodeRacikan(el, metode) {
        let option = '';
        for (var i = 0; i < metodeRacikan.length; i++) {
            let selected = "";
            if (metode === metodeRacikan[i].id) selected = "selected='selected'";
            option += `<option value="${metodeRacikan[i].id}" ${selected}>${metodeRacikan[i].nama}</option>`;
        }
        el.html(option).trigger("change");
    }

    function initializePegawai(el, pegawai_id) {
        $.getJSON(URL.getPegawai, function(data, status) {
            if (status === "success") {
                let option = '<option value="">- Pilih -</option>';
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (pegawai_id === data.data[i].id) selected = "selected='selected'";
                    option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");
            }
        });
    }

    let fillDataPasien = (data) => {
        $(".div-label_perusahaan").hide();
        $(".div-label_no_jaminan").hide();
        $(".div-label_penjamin_perusahaan").hide();
        $(".div-label_penjamin_no_jaminan").hide();
        $(".section-rawat_inap").hide();
        switch(parseInt(data.asal_pasien)) {
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                $(".section-rawat_inap").show();
                break;
        }

        $(".label-nama_pasien").html(data.pasien.nama);
        $(".label-no_rm").html(data.pasien.no_rm);
        $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
        $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
        $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
        $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
        $(".label-alamat").html(data.pasien.alamat);
        $(".label-no_telepon").html(data.pasien.no_telepon_1);

        $(".label-no_register").html(data.pelayanan.no_register);
        $(".label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
        $(".label-layanan").html(data.pelayanan.layanan);
        $(".label-dokter").html(data.pelayanan.dokter);
        $(".label-ruang").html(data.pelayanan.ruang);
        $(".label-kelas").html(data.pelayanan.kelas);
        $(".label-bed").html(data.pelayanan.bed);

        $(".label-cara_bayar").html(data.pelayanan.cara_bayar);
        $(".label-perusahaan").html(data.pelayanan.perusahaan);
        $(".label-no_jaminan").html(data.pelayanan.no_jaminan);
        $(".label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
        $(".label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

        switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
            case imediscode.CARA_BAYAR_BPJS:
                $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                if(data.pelayanan.penjamin_id) {
                    $(".div-label_penjamin_perusahaan").show();
                    $(".div-label_penjamin_no_jaminan").show();

                    let labelPerusahaan = 'Perusahaan';
                    let labelNoJaminan = 'NIK';
                    if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                        labelPerusahaan = 'Asuransi';
                        labelNoJaminan = 'No. Anggota';
                    }
                    $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                    $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                }
                break;
            case imediscode.CARA_BAYAR_JAMKESDA:
                $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                break;
            case imediscode.CARA_BAYAR_ASURANSI:
            case imediscode.CARA_BAYAR_PERUSAHAAN:
                $(".div-label_perusahaan").show();
                $(".div-label_no_jaminan").show();

                let labelPerusahaan = 'Perusahaan';
                let labelNoJaminan = 'NIK';
                if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                    labelPerusahaan = 'Asuransi';
                    labelNoJaminan = 'No. Anggota';
                }
                $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                break;
            case imediscode.CARA_BAYAR_INTERNAL:
                $(".div-label_no_jaminan").show().children('label').html('NIK');
                break;
        }
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".section-search").hide();
                
                let label;
                switch(BROWSE) {
                    case "buat":
                        label = "Buat Resep";
                        break;
                    case "penyerahan":
                        label = "Penyerahan Resep";
                        break;
                    case "history":
                        $('.btn-save').hide();
                        label = "History";
                        break;
                    default:
                        label = "Resep Masuk";
                        break;
                }
                
                let data = res.data;
                $('.page-title').html(`<h4><i class="icon-copy2"></i>${label} - ${data.nomor_resep}</h4>`);
                $(form).find("#id").val(data.id);
                $(form).find("#uid").val(data.uid);
                $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                
                fillDataPasien(data);

                // NON RACIKAN
                $(tableNonRacikan + " tbody").empty();
                if(data.non_racikan_list.length > 0) {
                    for (var i = 0; i < data.non_racikan_list.length; i++) {
                        addNonRacikan(data.non_racikan_list[i], "view");
                    }
                } else $(tableNonRacikan + " tbody").append(`<tr><td colspan="6" class="text-center">Tidak Ada Data</td></tr>`);

                // RACIKAN
                $(tableRacikan + " tbody").empty();
                if(data.racikan_list.length > 0) {
                    for (var i = 0; i < data.racikan_list.length; i++) {
                        addRacikan(data.racikan_list[i], "view");
                    }
                } else $(tableRacikan + " tbody").append(`<tr><td colspan="8" class="text-center">Tidak Ada Data</td><tr>`);

                $.unblockUI();
                setTimeout(() => {
                    firstLoad = false;
                    if(BROWSE === "history") $('input, select').prop('disabled', true);
                }, 200);
            }
        });
    }

    // NON RACIKAN
    let addNonRacikan = (obj, listMode) => {
        let tbody = $(tableNonRacikan + ' tbody');

        if(tbody.find('input[name="non_racikan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('non_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_id[]')
            .addClass('input-non_racikan_obat_id')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan_id[]')
            .addClass('input-non_racikan_satuan_id')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQtyResep = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQtyResep = $("<span>")
            .html(numeral(obj.quantity_resep).format())
            .appendTo(tdQtyResep);
        let inputQtyResep = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty_resep[]')
            .addClass('input-non_racikan_qty_resep')
            .val(obj.quantity_resep)
            .appendTo(tdQtyResep);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty[]')
            .addClass('input-non_racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdSigna = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let inputTextSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_signa[]')
            .prop('value', obj.signa)
            .appendTo(tdSigna);
        let inputSigna = $("<select/>")
            .addClass('form-control')
            .prop('multiple', 'multiple')
            .appendTo(tdSigna);
        inputSigna.select2({
            placeholder: "- Pilih -",
        });
        
        // Handler
        initializeSigna(inputSigna, obj.signa);

        inputSigna.on('change blur', (e) => {
            inputTextSigna.val(inputSigna.val());
        });

        inputSigna.focus();
    }

    // RACIKAN
    let addRacikan = (obj, listMode) => {
        let tbody = $(tableRacikan + ' tbody');

        if(tbody.find('.input-racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('racikan_uid', obj.uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .appendTo(tr);
        let labelNama = $("<span>")
            .html(obj.nama + obj.label_list_obat)
            .addClass('label-racikan_nama')
            .appendTo(tdNama);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_id[]')
            .addClass('input-racikan_id')
            .val(obj.id ? obj.id : 0)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_nama')
            .prop('name', 'racikan_nama[]')
            .val(obj.nama)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_uid')
            .prop('name', 'racikan_uid[]')
            .val(obj.uid)
            .appendTo(tdNama);
        let inputListObat = $("<textarea/>")
            .addClass('input-racikan_list_obat')
            .prop('name', 'racikan_list_obat[]')
            .val(json_encode(obj.list_obat))
            .html(json_encode(obj.list_obat))
            .appendTo(tdNama);
        inputListObat.hide();

        let tdCaraBuat = $("<td/>")
            .appendTo(tr);
        let labelCaraBuat = $("<span>")
            .html(obj.label_cara_buat)
            .addClass('label-racikan_cara_buat')
            .appendTo(tdCaraBuat);
        let inputCaraBuat = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_cara_buat[]')
            .addClass('input-racikan_cara_buat')
            .val(obj.cara_buat)
            .appendTo(tdCaraBuat);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .addClass('label-racikan_harga')
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_harga[]')
            .addClass('input-racikan_harga')
            .val(obj.harga)
            .appendTo(tdHarga);

        let tdQtyResep = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQtyResep = $("<span>")
            .html(numeral(obj.old_quantity).format())
            .appendTo(tdQtyResep);
        let inputQtyResep = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty_resep[]')
            .addClass('input-racikan_qty_resep')
            .val(obj.old_quantity)
            .appendTo(tdQtyResep);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .addClass('label-racikan_qty')
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty[]')
            .addClass('input-racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdSigna = $("<td/>")
            .addClass('lc')
            .appendTo(tr);
        let inputTextSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_signa[]')
            .prop('value', obj.signa)
            .appendTo(tdSigna);
        let inputSigna = $("<select/>")
            .addClass('form-control')
            .prop('multiple', 'multiple')
            .appendTo(tdSigna);
        inputSigna.select2({
            placeholder: "- Pilih -",
        });

        let tdPeracik = $("<td/>")
            .addClass('lc')
            .appendTo(tr);
        let inputPeracik = $("<select/>")
            .prop('name', 'racikan_peracik[]')
            .addClass('form-control')
            .appendTo(tdPeracik);
        inputPeracik.select2();
        
        // Handler
        initializeSigna(inputSigna, obj.signa);
        initializePegawai(inputPeracik, obj.diracik_by);

        inputSigna.on('change blur', (e) => {
            inputTextSigna.val(inputSigna.val());
        });

        inputPeracik.on('change blur', (e) => {
            if(inputPeracik.val() != "") {
                if(!firstLoad) {
                    typeVerifikasi = "peracik";
                    $('#label-verifikasi_login').html('Verifikasi Peracik Resep');
                    $('#modal-verifikasi_login').modal('show');
                    $('#pegawai-verifikasi_login').val(inputPeracik.val());
                }
            } 
        });

        inputSigna.focus();

        // Handler
        tr.find('td:not(".lc")').on('click', (e) => {
            $('#btn-tambah_racikan_detail').parents('tfoot').hide();
            $('#modal-input_racikan_signa').prop('disabled', true);
            $('#modal-input_racikan_qty').prop('disabled', true);
            $('#modal-input_racikan_uid').val(obj.uid);
            $(formRacikan).find('#modal-input_racikan_nama').val(obj.nama);
            $(formRacikan).find('#modal-input_racikan_cara_buat').val(obj.cara_buat).change();
            $(formRacikan).find('#modal-input_racikan_harga').autoNumeric('set', obj.harga);
            $(formRacikan).find('#modal-input_racikan_qty').autoNumeric('set', obj.quantity);
            initializeSigna($(formRacikan).find('#modal-input_racikan_signa'), inputTextSigna.val());

            $(tableRacikanDetail + ' tbody').empty();
            let listObat = obj.list_obat;
            for(let i = 0; i < listObat.length; i++) 
                addRacikanDetail(listObat[i], "view");

            $(modalRacikan).modal('show');
        });
    }

    let addRacikanDetail = (obj, listMode) => {
        let tbody = $(tableRacikanDetail + ' tbody');

        if(tbody.find('.input-item_racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('item_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-item_racikan_id')
            .prop('name', 'item_racikan_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let inputSatuanDosisId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_dosis_id[]')
            .val(obj.satuan_dosis_id ? obj.satuan_dosis_id : '')
            .appendTo(tdNama);
        let inputSatuanDosis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_dosis[]')
            .val(obj.satuan_dosis ? obj.satuan_dosis : '&mdash;')
            .appendTo(tdNama);
        let inputIsiSatuanDosis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_isi_satuan_dosis[]')
            .val(obj.isi_satuan_dosis ? obj.isi_satuan_dosis : 0)
            .appendTo(tdNama);

        let tdSatuan = $("<td/>")
            .html(obj.satuan)
            .appendTo(tr);

        let tdMetode = $("<td/>")
            .appendTo(tr);
        let labelMetode = $("<span/>")
            .html(obj.label_metode)
            .appendTo(tdMetode);
        let inputTextMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_label_metode[]')
            .val(obj.label_metode)
            .appendTo(tdNama);
        let inputMetode = $("<select/>")
            .prop('name', 'item_metode[]')
            .addClass('form-control')
            .appendTo(tdMetode);
        inputMetode.select2({
            placeholder: "- Pilih -",
        });
        inputMetode.hide().next(".select2-container").hide();

        initializeMetodeRacikan(inputMetode, obj.metode);

        let tdParamMetode = $("<td/>")
            .appendTo(tr);
        let labelParamMetode = $("<span/>")
            .html(obj.param_metode)
            .appendTo(tdParamMetode);
        let inputHiddenParamMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_param_metode[]')
            .val(obj.param_metode)
            .appendTo(tdParamMetode);
        let inputParamMetode = $("<input/>")
            .addClass('form-control')
            .val(obj.param_metode)
            .appendTo(tdParamMetode);
        inputParamMetode.hide();

        let tdSatuanDosis = $("<td/>")
            .html(obj.satuan_dosis ? obj.satuan_dosis : '&mdash;')
            .appendTo(tr);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .html('&mdash;')
            .appendTo(tr);
    }

    // MODAL RACIKAN HANDLER
    $('#modal-input_racikan_cara_buat').change(function() {
        let qtyRacikan = isNaN(parseInt($("#modal-input_racikan_qty").autoNumeric('get'))) ? 0 : parseInt($("#modal-input_racikan_qty").autoNumeric('get'));
        let defaultQty = isNaN(parseFloat($(this).find('option:selected').data('qty_buat'))) ? 0 : parseFloat($(this).find('option:selected').data('qty_buat'));
        if(qtyRacikan <= 0) $('#modal-input_racikan_qty').autoNumeric('set', defaultQty);
    });

    $(formRacikan).validate({
        rules: {
            nama: { required: true },
            cara_buat: { required: true },
            harga: { required: true },
            quantity: { required: true },
            "signa[]": { required: true },
        },
        messages: {
            //
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalRacikan + ' .modal-dialog');
            let postData = $(formRacikan).serializeArray();

            let data = {};
            let d, fieldname, isArray, tmpLabel;
            for (let i = 0; i < postData.length; i++) {
                d = postData[i];
                isArray = false;

                if (d.name.search(/\[\]/) !== -1) {
                    fieldname = d.name.replace(/\[\]/, '');
                    isArray = true;
                } else {
                    fieldname = d.name;
                    isArray = false;
                }

                if (isArray) {
                    if (! data[fieldname]) {
                        data[fieldname] = [];
                        data[fieldname].push(d.value);
                    } else {
                        data[fieldname].push(d.value)
                    }
                } else {
                    data[fieldname] = d.value;
                }
            }

            data['label_cara_buat'] = $('#modal-input_racikan_cara_buat').find('option:selected').html();
            data['harga'] = $('#modal-input_racikan_harga').autoNumeric('get');
            data['quantity'] = $('#modal-input_racikan_qty').autoNumeric('get');

            let labelSignaArr = [];
            $('#modal-input_racikan_signa').find('option:selected').each(function() {
                if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                    labelSignaArr.push($(this).html());
            });

            data['label_signa'] = '&mdash;';
            if(labelSignaArr.length > 0) {
                tmpLabel = `<ul class="text-left">`;
                for(let i = 0; i < labelSignaArr.length; i++) {
                    tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_signa'] = tmpLabel;
            }

            let listObat = [];
            let labelListObat = [];
            for (let i = 0; i < data.item_racikan_id.length; i++) {
                listObat.push({
                    id: data.item_racikan_id[i],
                    stock_id: data.item_stock_id[i],
                    obat_id: data.item_obat_id[i],
                    obat_uid: data.item_obat_uid[i],
                    obat: data.item_obat[i],
                    satuan_id: data.item_satuan_id[i],
                    satuan: data.item_satuan[i],
                    label_metode: data.item_label_metode[i],
                    metode: data.item_metode[i],
                    param_metode: data.item_param_metode[i],
                    satuan_dosis_id: data.item_satuan_dosis_id[i],
                    satuan_dosis: data.item_satuan_dosis[i],
                    isi_satuan_dosis: data.item_isi_satuan_dosis[i],
                    harga: data.item_harga[i],
                    harga_dasar: data.item_harga_dasar[i],
                    old_quantity: data.item_old_qty[i],
                    quantity: data.item_qty[i],
                });
                
                let metode = data.item_label_metode[i];
                if(metode.search(/dosis/i) !== -1) {
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_param_metode[i]} ${data.item_satuan_dosis[i]}`);
                } else if(metode.search(/x\/\y/i) !== -1) {
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_param_metode[i]} ${data.item_satuan[i]}`);
                } else 
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_qty[i]} ${data.item_satuan[i]}`);
            }
            data['list_obat'] = listObat;
            if(data['uid'] === "") data['uid'] = makeid(50);

            if(labelListObat.length > 0) {
                tmpLabel = `<ul class="text-left text-size-mini text-slate-300">`;
                for(let i = 0; i < labelListObat.length; i++) {
                    tmpLabel += `<li>${labelListObat[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_list_obat'] = tmpLabel;
            }

            let isExists = false;
            $(tableRacikan + " tbody").find('tr').each(function() {
                if(data['uid'] === $(this).data('racikan_uid')) 
                    isExists = $(this);
            });

            if(isExists) {
                isExists.find('.input-racikan_list_obat').val(json_encode(data.list_obat)).html(json_encode(data.list_obat));
                isExists.find('.input-racikan_nama').val(data.nama);
                isExists.find('.label-racikan_nama').html(data.nama + data.label_list_obat);
                isExists.find('.input-racikan_cara_buat').val(data.cara_buat);
                isExists.find('.label-racikan_cara_buat').html(data.label_cara_buat);
                isExists.find('.label-racikan_harga').html('Rp.' + numeral(data.harga).format());
                isExists.find('.input-racikan_harga').val(data.harga);
                isExists.find('.label-racikan_qty').html(numeral(data.quantity).format());
                isExists.find('.input-racikan_qty').val(data.quantity);
                isExists.find('.label-racikan_signa').html(data.label_signa);
                isExists.find('.input-racikan_signa').val(data.signa);
            }
            setTimeout(() => {
                $(modalRacikan + ' .modal-dialog').unblock();
            
                $(modalRacikan).modal('hide');
            }, 400);
        }
    });

    // SUBMIT HANDLER
    function processSubmit(formData) {
        blockPage();

        $.ajax({
            url: URL.save,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            dataType: "json",
            success: function (result) {
                $.unblockUI();
                successMessage('Success', "Data Resep berhasil disimpan.");

                UID = result.data;

                $('.btn-save').hide();
                $('.btn-cetak').show();
                /*setTimeout(() => {
                    window.location.assign(URL.index);
                }, 300);*/
            },
            error: function () {
                $.unblockUI();
                errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
            }
        });
    }

    function confirmSubmit(browse, formData, text) {
        swal({
            title: "Konfirmasi",
            type: "warning",
            text: text,
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#2196F3",
            cancelButtonText: "Batal",
            cancelButtonColor: "#FAFAFA",
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function() {
            processSubmit(formData);       
        });
    }

    let printDoc = (url, width, height) => {
        let randomNumber = Math.floor((Math.random()*100)+1); 
        window.open(url, "_blank", `PopUp${randomNumber}, scrollbars=1, height=${height}, width=${width}, left=${(Math.random()*100) + 200}, top=${(Math.random() * 100) + 50}`);
    }

    $(form).validate({
        rules: {
            'non_racikan_signa[]': { required: true },
            'racikan_signa[]': { required: true },
            'racikan_peracik[]': { required: true },
        },
        messages: {
            'non_racikan_signa[]': { required: "Signa diperlukan"},
            'racikan_signa[]': { required: "Signa diperlukan"},
            'racikan_peracik[]': { required: "Peracik diperlukan"},
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            let label = "";
            switch(BROWSE) {
                case "buat":
                    label = "Verifikasi User Pembuat Resep";
                    break;
                case "penyerahan":
                    label = "Verifikasi User Penyerahan Resep";
                    break;
                default:
                    label = "Verifikasi User Penerima Resep";
                    break;
            }
            $('#label-verifikasi_login').html(label);
            $('#modal-verifikasi_login').modal('show');
            typeVerifikasi = "save_form";
            
            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            /*switch(BROWSE) {
                case "buat":
                    confirmSubmit(BROWSE, formData, "Obat telah selesai di siapkan??");
                    break;
                case "penyerahan":
                    confirmSubmit(BROWSE, formData, "Obat telah diserahkan kepada Pasien??");
                    break;
                default:
                    processSubmit(formData);
                    break;
            }*/
            dataForm = formData;
            console.log('Form Data has been temporary stored');
        }
    });

    $('#btn-verifikasi_login').click(function(e) {
        e.preventDefault();
        let username = $('#username-verifikasi_login').val();
        let password = $('#password-verifikasi_login').val();

        if(username == "" || password == "") {
            $('#username-verifikasi_login').focus();
            warningMessage('Peringatan !', 'Username dan Password harus diisi.');
            return;
        }

        blockElement('#modal-verifikasi_login .modal-dialog');
        $.ajax({
            url: URL.verifikasiLogin,
            data: {
                username: username,
                password: btoa(JSON.stringify(password)),
                pegawai_id: $('#pegawai-verifikasi_login').val(),
                type: typeVerifikasi,
            },
            type: 'POST',
            dataType: "json",
            success: function (result) {
                $('#modal-verifikasi_login').modal("hide");
                $('#modal-verifikasi_login .modal-dialog').unblock();
                switch(typeVerifikasi) {
                    case "save_form":
                        switch(BROWSE) {
                            case "buat":
                                confirmSubmit(BROWSE, dataForm, "Obat telah selesai di siapkan??");
                                break;
                            case "penyerahan":
                                confirmSubmit(BROWSE, dataForm, "Obat telah diserahkan kepada Pasien??");
                                break;
                            default:
                                processSubmit(dataForm);
                                break;
                        }
                        break;
                     default:
                        successMessage('Success', 'Verifikasi berhasil dilakukan.');
                        $('#pegawai-verifikasi_login').val("");
                        break;
                }
            },
            error: function (err) {
                $('#modal-verifikasi_login').modal("hide");
                $('#modal-verifikasi_login .modal-dialog').unblock();
                errorMessage('Error', err.responseJSON.message);
            }
        });
    });

    $('#btn-nota').click(function () {
        printDoc(URL.cetakHal.replace(':UID', UID).replace(':BROWSE', 'nota'), 400, 700);
    });

    $('#btn-etiket').click(function () {
        $.getJSON(URL.getData.replace(':UID', UID), function(res, status) {
            if (status === "success") {
                data = res.data;
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.non_racikan_list[i].uid), 400, 400);
                }

                for (var i = 0; i < data.racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.racikan_list[i].uid), 400, 400);
                }
            }
        });
    });

    $('#modal-verifikasi_login').on('hidden.bs.modal', function () {
        $('.login-form')[0].reset();
    });

    if(parseInt(UID) !== 0) 
        fillForm(UID);
    $('.sidebar-control').click();
});