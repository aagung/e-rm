$(() => {
    $('.rangetanggal-form').daterangepicker({
        applyClass: "bg-slate-600",
        cancelClass: "btn-default",
        opens: "center",
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        startDate: moment(),
        endDate: moment(),
    });

    let startReloadData = (s) => {
        if (repeat) {
            sLoadTable = setTimeout(function() {
                let element = "#table-masuk";
                switch(s) {
                    case 2:
                        element = "#table-buat";
                        break;
                    case 3:
                        element = "#table-penyerahan";
                        break;
                }

                $(element).find('tbody').find('tr').each((i, el) => {
                    console.log('browse: ' + s);
                    let startDate = moment($(el).find('[data-tanggal]').data('tanggal'));
                    $(el).find('[data-tanggal]').html(getWaktuTunggu(startDate, ""));
                });
                startReloadData(s);
            }, FREQ);
        }
    }

    let startReload = (s) => {
        repeat = true;
        clearTimeout(sLoadTable);
        startReloadData(s);
    }

    let stopReload = (s) => {
        clearTimeout(sLoadTable);
        repeat = false;
    }

    let subsDate = (range, tipe) => {
        let date = range.substr(0, 10);
        if(tipe === "sampai") date = range.substr(13, 10);
        return getDate(date);
    }

    let listen = (q) => {
        eventSource = new EventSource(URL.listen.replace(':UID', q));
        eventSource.addEventListener('farmasi-' + q, function(e) {
            tableMasuk.draw(false);
            tableBuat.draw(false);
        }, false);
    }

    let fetchDataSelect = (url, element) => {
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let datas = res.data;

                element.empty();
                element.append(`<option value="">${fieldAll}</option/>`);
                for (let data of datas) {
                    $("<option/>")
                        .prop('value', data.id)
                        .html(data.nama)
                        .appendTo(element);
                }
                element.val('').trigger('change');
            }
        });
    }

    let getWaktuTunggu = (start, end) => {
        end = end ? moment(end) : moment();
        let day, hour, minute, second;
        let duration = moment.duration(end.diff(start));

        day = duration.days();
        hour = duration.hours();
        minute = duration.minutes();
        second = duration.seconds();

        if(day > 0) 
            return day + ' Hari ' + hour + ' Jam ' + minute + ' Menit ';

        if(hour > 0) 
            return hour + ' Jam ' + minute + ' Menit';

        return minute + ' Menit';
    }

    let printDoc = (url, width, height) => {
        let randomNumber = Math.floor((Math.random()*100)+1); 
        window.open(url, "_blank", `PopUp${randomNumber}, scrollbars=1, height=${height}, width=${width}, left=${(Math.random()*100) + 200}, top=${(Math.random() * 100) + 50}`);
    }

    let initializeTable = (browse) => {
        let labelButton = "Terima";
        switch(browse) {
            case 2:
                labelButton = "Buat Resep";
                break;
            case 3:
                labelButton = "Serahkan";
                break;
            case 4:
                labelButton = "Detail";
                break;
        }
        let columns = [
                        { 
                            "data": "uid",
                            "orderable": false,
                            "render": (data, type, row, meta) => {
                                let tmp = `<ul class="icons-list mb-10">` +
                                                `<li class="dropdown">` +
                                                    `<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">` +
                                                        `<i class="icon-menu9"></i>` +
                                                    `</a>` +
                                                    `<ul class="dropdown-menu">` +
                                                        `<li><a data-uid="${data}" class="btn-copy_resep">Copy Resep</a></li>` +
                                                        `<li><a data-uid="${data}" class="btn-nota">Nota</a></li>` +
                                                        `<li><a data-uid="${data}" class="btn-signa">Signa</a></li>` +
                                                    '</ul>' +
                                                '</li>' +
                                            '</ul>';

                                if(browse == 3) {
                                    switch(parseInt(row.status_antrian)) {
                                        case 3:
                                            //
                                            break;
                                        case 2:
                                            tmp += `<p><a data-uid="${row.uid}" data-id="${row.id}" data-mode="panggil_ulang" data-popup="tooltip" title="Panggil Ulang Pasien?" class="btn bg-brown btn-xs antrian-row"><i class="glyphicon glyphicon-repeat"></i></a></p>`;
                                            tmp += `<p><a data-uid="${row.uid}" data-id="${row.id}" data-mode="lewati" data-popup="tooltip" title="Lewati Pasien?" class="btn bg-slate btn-xs antrian-row"><i class="glyphicon glyphicon-collapse-down"></i></a</p>`;
                                            break;
                                        default:
                                            tmp += `<p><a data-uid="${row.uid}" data-id="${row.id}" data-mode="panggil" data-popup="tooltip" title="Panggil Pasien?" class="btn btn-info btn-xs antrian-row"><i class="glyphicon glyphicon-bullhorn"></i></a></p>`;
                                            break;
                                    }   
                                }
                                return tmp;
                            },
                            "className": "text-center",
                        },
                        {
                            "data": "no_register",
                            "render": (data, type, row, meta) => {
                                return `${data}<br/><label data-uid="${row.uid}" class="label label-primary form-row" style="cursor: pointer;">${labelButton}</label>`;
                            },
                        },
                        {
                            "data": "tanggal",
                            "render": (data, type, row, meta) => {
                                let tmp = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                                tmp += `<br/><span class="text-size-mini text-info"><b>Nomor:</b> ${row.kode}</span>`;
                                if(browse == 3) {
                                    if(parseInt(row.status_antrian) === 1) 
                                        tmp += `&nbsp;<label class="label label-danger">Pasien Terlewati</label>`;
                                }
                                return tmp;
                            },
                        },
                        { "data": "no_rm" },
                        { "data": "pasien" },
                        { "data": "asal_pasien" },
                        { "data": "layanan" },
                        {
                            "data": "cara_bayar",
                            "render": (data, type, row, meta) => {
                                let tmp = data;
                                if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                    tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                                return tmp;
                            },
                        },
                        { "data": "dokter" },
                    ];

        // DATATABLE
        switch(browse) {
            case 1:
                columns.push({ 
                            "data": "created_at",
                            "render": (data, type, row, meta) => {
                                return `<span data-tanggal="${data}">${getWaktuTunggu(data, "")}</span>`;
                            },
                            "orderable": false,
                            "className": "text-center",
                        });

                tableMasuk = $('#table-masuk').DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,/*
                    "scrollX": true,
                    "scrollCollapse": true,
                    "fixedColumns": {
                      "leftColumns": 4,
                    },*/
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 1),
                      "type": "POST",
                      "data": function(p) {
                          p.farmasi_unit_id = $('#search-masuk_farmasi_unit').val();
                          p.no_register = $('#search-masuk_no_register').val();
                          p.no_rekam_medis = $('#search-masuk_no_rekam_medis').val();
                          p.nama = $('#search-masuk_nama').val();
                          p.asal_pasien = $('#search-masuk_asal_pasien').val();
                          p.poli_id = $('#search-masuk_poli').val();
                          p.ruang_id = $('#search-masuk_ruang').val();
                          p.dokter_id = $('#search-masuk_dokter').val();
                          p.cara_bayar_id = $('#search-masuk_cara_bayar').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [2, "asc"] ],
                    "drawCallback": function (oSettings) {
                        $('#table-masuk').find('[data-popup=tooltip]').tooltip();

                        let n = oSettings._iRecordsTotal;
                        $("#badge-tab1").html(n);
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#search-masuk_cara_bayar'));
                startReload(1);

                var isDTable = $.fn.dataTable.isDataTable($('#table-buat'));
                if(isDTable === false) initializeTable(2);
                break;
            case 2:
                columns.push({ 
                            "data": "created_at",
                            "render": (data, type, row, meta) => {
                                return `<span data-tanggal="${data}">${getWaktuTunggu(data, "")}</span>`;
                            },
                            "orderable": false,
                            "className": "text-center",
                        });

                tableBuat = $('#table-buat').DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,/*
                    "scrollX": true,
                    "scrollCollapse": true,
                    "fixedColumns": {
                      "leftColumns": 4,
                    },*/
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 2),
                      "type": "POST",
                      "data": function(p) {
                          p.farmasi_unit_id = $('#search-buat_farmasi_unit').val();
                          p.no_register = $('#search-buat_no_register').val();
                          p.no_rekam_medis = $('#search-buat_no_rekam_medis').val();
                          p.nama = $('#search-buat_nama').val();
                          p.asal_pasien = $('#search-buat_asal_pasien').val();
                          p.poli_id = $('#search-buat_poli').val();
                          p.ruang_id = $('#search-buat_ruang').val();
                          p.dokter_id = $('#search-buat_dokter').val();
                          p.cara_bayar_id = $('#search-buat_cara_bayar').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [2, "asc"] ],
                    "drawCallback": function (oSettings) {
                        $('#table-buat').find('[data-popup=tooltip]').tooltip();

                        let n = oSettings._iRecordsTotal;
                        $("#badge-tab2").html(n);
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#search-buat_cara_bayar'));

                var isDTable = $.fn.dataTable.isDataTable($('#table-penyerahan'));
                if(isDTable === false) initializeTable(3);
                break;
            case 3:
                columns.push(
                                { 
                                    "data": "dibuat_by",
                                    "orderable": false,
                                },
                                { 
                                    "data": "diracik_by",
                                    "render": (data, type, row, meta) => {
                                        return data ? data : "&mdash;";
                                    },
                                    "orderable": false,
                                },
                                { 
                                    "data": "status",
                                    "render": (data, type, row, meta) => {
                                        return 'Selesai';
                                    },
                                    "orderable": false,
                                    "searchable": false,
                                },
                                { 
                                    "data": "dibuat_at",
                                    "render": (data, type, row, meta) => {
                                        return `<span data-tanggal="${data}">${getWaktuTunggu(data, "")}</span>`;
                                    },
                                    "orderable": false,
                                    "className": "text-center",
                                }
                            );

                tablePenyerahan = $('#table-penyerahan').DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,/*
                    "scrollX": true,
                    "scrollCollapse": true,
                    "fixedColumns": {
                      "leftColumns": 4,
                    },*/
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 3),
                      "type": "POST",
                      "data": function(p) {
                          p.farmasi_unit_id = $('#search-penyerahan_farmasi_unit').val();
                          p.no_register = $('#search-penyerahan_no_register').val();
                          p.no_rekam_medis = $('#search-penyerahan_no_rekam_medis').val();
                          p.nama = $('#search-penyerahan_nama').val();
                          p.asal_pasien = $('#search-penyerahan_asal_pasien').val();
                          p.poli_id = $('#search-penyerahan_poli').val();
                          p.ruang_id = $('#search-penyerahan_ruang').val();
                          p.dokter_id = $('#search-penyerahan_dokter').val();
                          p.cara_bayar_id = $('#search-penyerahan_cara_bayar').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [2, "asc"] ],
                    "drawCallback": function (oSettings) {
                        $('#table-penyerahan').find('[data-popup=tooltip]').tooltip();

                        let n = oSettings._iRecordsTotal;
                        $("#badge-tab3").html(n);
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#search-penyerahan_cara_bayar'));

                var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
                if(isDTable === false) initializeTable(4);
                break;
            case 4:
                columns.push(
                                { 
                                    "data": "dibuat_by",
                                    "orderable": false,
                                },
                                { 
                                    "data": "diracik_by",
                                    "render": (data, type, row, meta) => {
                                        return data ? data : "&mdash;";
                                    },
                                    "orderable": false,
                                },
                                { 
                                    "data": "diserahkan_by",
                                    "render": (data, type, row, meta) => {
                                        return data ? data : "&mdash;";
                                    },
                                    "orderable": false,
                                },
                                { 
                                    "data": "created_at",
                                    "render": (data, type, row, meta) => {
                                        return getWaktuTunggu(data, row.diserahkan_at);
                                    },
                                    "orderable": false,
                                    "className": "text-center",
                                }
                            );

                tableHistory = $('#table-history').DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,/*
                    "scrollX": true,
                    "scrollCollapse": true,
                    "fixedColumns": {
                      "leftColumns": 4,
                    },*/
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 4),
                      "type": "POST",
                      "data": function(p) {
                          p.tanggal_dari = subsDate($('#search-history_tanggal').val(), 'dari');
                          p.tanggal_sampai = subsDate($('#search-history_tanggal').val(), 'sampai');
                          p.no_register = $('#search-history_no_register').val();
                          p.no_rekam_medis = $('#search-history_no_rekam_medis').val();
                          p.nama = $('#search-history_nama').val();
                          p.asal_pasien = $('#search-history_asal_pasien').val();
                          p.poli_id = $('#search-history_poli').val();
                          p.ruang_id = $('#search-history_ruang').val();
                          p.dokter_id = $('#search-history_dokter').val();
                          p.cara_bayar_id = $('#search-history_cara_bayar').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [2, "asc"] ],
                    "drawCallback": function (oSettings) {
                        $('#table-history').find('[data-popup=tooltip]').tooltip();

                        let n = oSettings._iRecordsTotal;
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#search-history_cara_bayar'));
                break;
        }
    }

    // TAB 1
    $('.input-tab1').on('change keyup blur', function () {
        tableMasuk.draw();
    });

    $('#search-masuk_asal_pasien').change(function() {
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(0)), $('#search-masuk_dokter'));
        $('#tab-1').find('.section-rawat_jalan').hide();
        $('#tab-1').find('.section-rawat_inap').hide();
        $('#search-masuk_poli').val("").change();
        $('#search-masuk_ruang').val("").change();

        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('#tab-1').find('.section-rawat_jalan').show('slow');
                break;
            case imediscode.UNIT_LAYANAN_IGD:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-masuk_dokter'));
                break;
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-masuk_dokter'));
                $('#search-masuk_ruang').val("").change();
                $('#tab-1').find('.section-rawat_inap').show('slow');
                break;
        }
    });

    $('#search-masuk_poli').change(function() {
        let val = parseInt($(this).val());
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-masuk_dokter'));
    });

    $('#table-masuk').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        blockPage('Form sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "masuk"));
        }, 1000);
    });

    $('#table-masuk').on("click", ".btn-copy_resep", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'copy_resep'), 600, 700);
    });

    $('#table-masuk').on("click", ".btn-nota", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'nota'), 400, 700);
    });

    $('#table-masuk').on("click", ".btn-signa", function () {
        let uid = $(this).data('uid');
        $.getJSON(URL.getData.replace(':UID', uid), function(res, status) {
            if (status === "success") {
                data = res.data;
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.non_racikan_list[i].uid), 400, 400);
                }

                for (var i = 0; i < data.racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.racikan_list[i].uid), 400, 400);
                }
            }
        });
    });

    // TAB 2
    $('.input-tab2').on('change keyup blur', function () {
        tableBuat.draw();
    });

    $('#search-buat_asal_pasien').change(function() {
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(0)), $('#search-buat_dokter'));
        $('#tab-2').find('.section-rawat_jalan').hide();
        $('#tab-2').find('.section-rawat_inap').hide();
        $('#search-buat_poli').val("").change();
        $('#search-buat_ruang').val("").change();

        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('#tab-2').find('.section-rawat_jalan').show('slow');
                break;
            case imediscode.UNIT_LAYANAN_IGD:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-buat_dokter'));
                break;
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-buat_dokter'));
                $('#search-buat_ruang').val("").change();
                $('#tab-2').find('.section-rawat_inap').show('slow');
                break;
        }
    });

    $('#search-buat_poli').change(function() {
        let val = parseInt($(this).val());
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-buat_dokter'));
    });

    $('#table-buat').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        blockPage('Form sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "buat"));
        }, 1000);
    });

    $('#table-buat').on("click", ".btn-copy_resep", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'copy_resep'), 600, 700);
    });

    $('#table-buat').on("click", ".btn-nota", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'nota'), 400, 700);
    });

    $('#table-buat').on("click", ".btn-signa", function () {
        let uid = $(this).data('uid');
        $.getJSON(URL.getData.replace(':UID', uid), function(res, status) {
            if (status === "success") {
                data = res.data;
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.non_racikan_list[i].uid), 400, 400);
                }

                for (var i = 0; i < data.racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.racikan_list[i].uid), 400, 400);
                }
            }
        });
    });

    // TAB 3
    $('.input-tab3').on('change keyup blur', function () {
        tablePenyerahan.draw();
    });

    $('#search-penyerahan_asal_pasien').change(function() {
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(0)), $('#search-penyerahan_dokter'));
        $('#tab-3').find('.section-rawat_jalan').hide();
        $('#tab-3').find('.section-rawat_inap').hide();
        $('#search-penyerahan_poli').val("").change();
        $('#search-penyerahan_ruang').val("").change();

        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('#tab-3').find('.section-rawat_jalan').show('slow');
                break;
            case imediscode.UNIT_LAYANAN_IGD:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-penyerahan_dokter'));
                break;
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-penyerahan_dokter'));
                $('#search-penyerahan_ruang').val("").change();
                $('#tab-3').find('.section-rawat_inap').show('slow');
                break;
        }
    });

    $('#search-penyerahan_poli').change(function() {
        let val = parseInt($(this).val());
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-penyerahan_dokter'));
    });

    $('#table-penyerahan').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        blockPage('Form sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "penyerahan"));
        }, 1000);
    });

    $('#table-penyerahan').on("click", ".btn-copy_resep", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'copy_resep'), 600, 700);
    });

    $('#table-penyerahan').on("click", ".btn-nota", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'nota'), 400, 700);
    });

    $('#table-penyerahan').on("click", ".btn-signa", function () {
        let uid = $(this).data('uid');
        $.getJSON(URL.getData.replace(':UID', uid), function(res, status) {
            if (status === "success") {
                data = res.data;
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.non_racikan_list[i].uid), 400, 400);
                }

                for (var i = 0; i < data.racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.racikan_list[i].uid), 400, 400);
                }
            }
        });
    });

    $('#table-penyerahan').on("click", ".antrian-row", function () {
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        let mode = $(this).data('mode');
        let tr = $(this).closest('tr');
        let data = tablePenyerahan.row(tr).data();

        $.post(URL.updateAntrian, { id: id, uid: uid, mode: mode, farmasi_unit_id: data.farmasi_unit_id }, function (res, status) {
            if (status === "success") {
                tablePenyerahan.draw();
                successMessage('Berhasil !', `Pasien "${data.no_register}" berhasil diproses.`);
            }
        })
        .fail(function (error) {
            errorMessage('Gagal !', 'Terjadi kesalahan saat memproses antrian.');
        });
    });

    // TAB 4
    $('#search-history_tanggal').on('apply.daterangepicker', (ev, picker) => {
        tableHistory.draw();
    });

    $('#search-history_tanggal').change(function() {
        tableHistory.draw();
    });

    $('.input-tab4').on('change keyup blur', function () {
        tableHistory.draw();
    });

    $('#search-history_asal_pasien').change(function() {
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(0)), $('#search-history_dokter'));
        $('#tab-4').find('.section-rawat_jalan').hide();
        $('#tab-4').find('.section-rawat_inap').hide();
        $('#search-history_poli').val("").change();
        $('#search-history_ruang').val("").change();

        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('#tab-4').find('.section-rawat_jalan').show('slow');
                break;
            case imediscode.UNIT_LAYANAN_IGD:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
                break;
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
                $('#search-history_ruang').val("").change();
                $('#tab-4').find('.section-rawat_inap').show('slow');
                break;
        }
    });

    $('#search-history_poli').change(function() {
        let val = parseInt($(this).val());
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
    });

    $('#table-history').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "history"));
    });

    $('#table-history').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "history"));
    });

    $('#table-history').on("click", ".btn-copy_resep", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'copy_resep'), 600, 700);
    });

    $('#table-history').on("click", ".btn-signa", function () {
        let uid = $(this).data('uid');
        $.getJSON(URL.getData.replace(':UID', uid), function(res, status) {
            if (status === "success") {
                data = res.data;
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.non_racikan_list[i].uid), 400, 400);
                }

                for (var i = 0; i < data.racikan_list.length; i++) {
                    printDoc(URL.cetakSigna.replace(':UID', data.racikan_list[i].uid), 400, 400);
                }
            }
        });
    });

    $('#table-history').on("click", ".btn-nota", function () {
        let uid = $(this).data('uid');
        printDoc(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'nota'), 400, 700);
    });

    $('a[data-toggle="tab"]').click(function (e) {
        stopReload();
        switch($(this).attr('href')) {
            case '#tab-2':
                startReload(2);
                tableBuat.draw(false);
                break;
            case '#tab-3':
                startReload(3);
                tablePenyerahan.draw(false);
                break;
            case '#tab-4':
                tableHistory.draw(false);
                break;
            default:
                startReload(1);
                tableMasuk.draw(false);
                break;
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust()
          .fixedColumns().relayout();
    });

    initializeTable(1);
    listen(tabUid);
});