$(() => {
    $('.rangetanggal-form').daterangepicker({
        applyClass: "bg-slate-600",
        cancelClass: "btn-default",
        opens: "center",
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        startDate: moment(),
        endDate: moment(),
    });

    let subsDate = (range, tipe) => {
        let date = range.substr(0, 10);
        if(tipe === "sampai") date = range.substr(13, 10);
        return getDate(date);
    }

    let listen = (q) => {
        eventSource = new EventSource(URL.listen.replace(':UID', q));
        eventSource.addEventListener('farmasi-' + q, function(e) {
            tableMasuk.draw(false);
            tableBuat.draw(false);
        }, false);
    }

    let fetchDataSelect = (url, element) => {
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let datas = res.data;

                element.empty();
                element.append(`<option value="">${fieldAll}</option/>`);
                for (let data of datas) {
                    $("<option/>")
                        .prop('value', data.id)
                        .html(data.nama)
                        .appendTo(element);
                }
                element.val('').trigger('change');
            }
        });
    }

    let initializeTable = (browse) => {
        let columns = [
                        
                        { 
                            "data": "uid",
                            "orderable": false,
                            "render": (data, type, row, meta) => {
                                let tmp = `<a data-uid="${data}" class="btn-nota"><i class="fa fa-print"></i></a>`
                                return tmp;
                            },
                            "className": "text-center",
                        },
                        /*{
                            "data": "no_penjualan",
                            "render": (data, type, row, meta) => {
                                let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="form-row" title="Lihat Data">${data}</a>`;
                                return tmp;
                            },
                        },*/
                        {
                            "data": "tanggal",
                            "render": (data, type, row, meta) => {
                                let tmp = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                                tmp += `<br/><span class="text-size-mini text-info"><b>Nomor:</b> ${row.kode}</span>`;
                                return tmp;
                            },
                        },
                        { "data": "no_register" },
                        {
                            "data": "pasien",
                            "render": (data, type, row, meta) => {
                                let tmp = data;
                                tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                                return tmp;
                            },
                        },
                        { "data": "asal_pasien" },
                        { "data": "layanan" },
                        {
                            "data": "cara_bayar",
                            "render": (data, type, row, meta) => {
                                let tmp = data;
                                if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                    tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                                return tmp;
                            },
                        },
                        { "data": "dokter" },
                        { 
                            "data": "status",
                            "render": (data, type, row, meta) => {
                                let tmp = 'Selesai';
                                if(parseInt(data) === 0) tmp = `Belum diproses (Kasir)`;
                                return tmp;
                            },
                        },
                    ];

        tableHistory = $('#table-history').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
              "url": URL.loadData.replace(':BROWSE', 4),
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($('#search-history_tanggal').val(), 'dari');
                  p.tanggal_sampai = subsDate($('#search-history_tanggal').val(), 'sampai');
                  p.no_register = $('#search-history_no_register').val();
                  p.no_rekam_medis = $('#search-history_no_rekam_medis').val();
                  p.nama = $('#search-history_nama').val();
                  p.asal_pasien = $('#search-history_asal_pasien').val();
                  p.poli_id = $('#search-history_poli').val();
                  p.ruang_id = $('#search-history_ruang').val();
                  p.dokter_id = $('#search-history_dokter').val();
                  p.cara_bayar_id = $('#search-history_cara_bayar').val();
              }
            },
            "columns": columns,
            "order": [ [1, "asc"] ],
            "drawCallback": function (oSettings) {
                $('#table-history').find('[data-popup=tooltip]').tooltip();
            }
        });

        fetchDataSelect(URL.fetchCaraBayar, $('#search-history_cara_bayar'));
    }

    // TAB 4
    $('#search-history_tanggal').on('apply.daterangepicker', (ev, picker) => {
        tableHistory.draw();
    });

    $('#search-history_tanggal').change(function() {
        tableHistory.draw();
    });

    $('.input-search').on('change keyup blur', function () {
        tableHistory.draw();
    });

    $('#search-history_asal_pasien').change(function() {
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(0)), $('#search-history_dokter'));
        $('.section-rawat_jalan').hide();
        $('.section-rawat_inap').hide();
        $('#search-history_poli').val("").change();
        $('#search-history_ruang').val("").change();
        
        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('.section-rawat_jalan').show('slow');
                break;
            case imediscode.UNIT_LAYANAN_IGD:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
                break;
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                val = "";
                fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
                $('.section-rawat_inap').show('slow');
                break;
        }
    });

    $('#search-history_poli').change(function() {
        let val = parseInt($(this).val());
        fetchDataSelect(URL.fetchDokter.replace(':Q', btoa(val)), $('#search-history_dokter'));
    });

    $('#table-history').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "history"));
    });

    $('#table-history').on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        window.location.assign(URL.form.replace(':UID', uid).replace(':BROWSE', "history"));
    });

    $('#table-history').on("click", ".btn-copy_resep", function () {
        let uid = $(this).data('uid');
        window.open(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'copy_resep'), "COPY RESEP", "scrollbars=1, height=700, width=700");
    });

    $('#table-history').on("click", ".btn-nota", function () {
        let uid = $(this).data('uid');
        window.open(URL.cetakHal.replace(':UID', uid).replace(':BROWSE', 'nota'), "NOTA FARMASI", "scrollbars=1, height=700, width=500");
    });

    initializeTable();
    listen(tabUid);
});