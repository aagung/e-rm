$(() => {
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    $(".wysihtml5-min").wysihtml5({
        parserRules:  wysihtml5ParserRules
    });
    $(".wysihtml5-toolbar").remove();

    let fillDataPasien = (data) => {
        $(".section-data_pasien").show();
        $(".div-label_perusahaan").hide();
        $(".div-label_no_jaminan").hide();
        $(".div-label_penjamin_perusahaan").hide();
        $(".div-label_penjamin_no_jaminan").hide();
        $(".section-rawat_inap").hide();
        switch(parseInt(data.asal_pasien)) {
            case imediscode.UNIT_LAYANAN_RAWAT_INAP:
                $(".section-rawat_inap").show();
                break;
        }

        $(".label-nama_pasien").html(data.pasien.nama);
        $(".label-no_rm").html(data.pasien.no_rm);
        $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
        $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
        $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
        $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
        $(".label-alamat").html(data.pasien.alamat);
        $(".label-no_telepon").html(data.pasien.no_telepon_1);

        $(".label-no_register").html(data.pelayanan.no_register);
        $(".label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
        $(".label-layanan").html(data.pelayanan.layanan);
        $(".label-dokter").html(data.pelayanan.dokter);
        $(".label-ruang").html(data.pelayanan.ruang);
        $(".label-kelas").html(data.pelayanan.kelas);
        $(".label-bed").html(data.pelayanan.bed);

        $(".label-cara_bayar").html(data.pelayanan.cara_bayar);
        $(".label-perusahaan").html(data.pelayanan.perusahaan);
        $(".label-no_jaminan").html(data.pelayanan.no_jaminan);
        $(".label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
        $(".label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

        switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
            case imediscode.CARA_BAYAR_BPJS:
                $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                if(data.pelayanan.penjamin_id) {
                    $(".div-label_penjamin_perusahaan").show();
                    $(".div-label_penjamin_no_jaminan").show();

                    let labelPerusahaan = 'Perusahaan';
                    let labelNoJaminan = 'NIK';
                    if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                        labelPerusahaan = 'Asuransi';
                        labelNoJaminan = 'No. Anggota';
                    }
                    $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                    $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                }
                break;
            case imediscode.CARA_BAYAR_JAMKESDA:
                $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                break;
            case imediscode.CARA_BAYAR_ASURANSI:
            case imediscode.CARA_BAYAR_PERUSAHAAN:
                $(".div-label_perusahaan").show();
                $(".div-label_no_jaminan").show();

                let labelPerusahaan = 'Perusahaan';
                let labelNoJaminan = 'NIK';
                if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                    labelPerusahaan = 'Asuransi';
                    labelNoJaminan = 'No. Anggota';
                }
                $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                break;
            case imediscode.CARA_BAYAR_INTERNAL:
                $(".div-label_no_jaminan").show().children('label').html('NIK');
                break;
        }
    }

    /*let initializePenjualan = () => {
        let fillPenjualan = (uid) => {
            blockElement(modalPenjualan + ' .modal-dialog');
            $.getJSON(URL.getDataPenjualan.replace(':UID', uid), function (res, status) {
                if (status === 'success') {
                    let data = res.data;
                    
                    $(form).find("#penjualan_id").val(data.id);
                    $(form).find("#no_penjualan").val(data.kode);
                    $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                    $(form).find("#asal_pasien").val(data.asal_pasien);
                    $(form).find("#layanan_id").val(data.layanan_id);
                    $(form).find("#dokter_id").val(data.dokter_id);
                    $(form).find("#pelayanan_id").val(data.pelayanan.id);
                    $(form).find("#pelayanan_uid").val(data.pelayanan.uid);
                    $(form).find("#no_register").val(data.pelayanan.no_register);
                    $(form).find("#pasien_id").val(data.pelayanan.pasien_id);
                    $(form).find("#cara_bayar_id").val(data.pelayanan.cara_bayar_id);
                    $(form).find("#perusahaan_id").val(data.pelayanan.perusahaan_id);

                    fillDataPasien(data);

                    $(tableDetail + " tbody").empty();
                    if(data.non_racikan_list.length > 0) {
                        for (var i = 0; i < data.non_racikan_list.length; i++) {
                            let detail = {
                                id: 0,
                                penjualan_detail_id: data.non_racikan_list[i].id,
                                stock_id: data.non_racikan_list[i].stock_id,
                                obat_id: data.non_racikan_list[i].obat_id,
                                obat_uid: data.non_racikan_list[i].obat_uid,
                                obat_kode: data.non_racikan_list[i].obat_kode,
                                obat: data.non_racikan_list[i].obat,
                                satuan_id: data.non_racikan_list[i].satuan_id,
                                satuan: data.non_racikan_list[i].satuan,
                                harga_dasar: data.non_racikan_list[i].harga_dasar,
                                harga: data.non_racikan_list[i].harga,
                                quantity_jual: data.non_racikan_list[i].quantity,
                                quantity: 0,
                                detail_barang: data.non_racikan_list[i].detail_barang,
                            }
                            addDetail(detail, "add", i);
                        }
                    } else $(tableDetail + " tbody").append(`<tr><td colspan="7" class="text-center">Tidak Ada Data</td></tr>`);

                    $(modalPenjualan).modal('hide');
                    $(modalPenjualan + ' .modal-dialog').unblock();
                }
            });
        }

        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_penjualan'));
        if(isDTable === true) $('#table-modal_penjualan').DataTable().destroy();

        tablePenjualan = $('#table-modal_penjualan').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadPenjualan,
                "type": "POST",
                "data": function(p) {
                    p.no_penjualan = $('#modal-penjualan_nomor').val();
                    p.no_register = $('#modal-penjualan_no_register').val();
                }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "kode",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="select-row" title="Pilih Penjualan">${data}</a>`;
                        return tmp;
                    },
                },
                { "data": "no_register" },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "asal_pasien" },
                { "data": "layanan" },
            ],
            "order": [ [1, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_penjualan').find('[data-popup=tooltip]').tooltip();
            },
        });

        $('#modal-penjualan_nomor, #modal-penjualan_no_register').keyup(function() {
            tablePenjualan.draw();
        });

        $('#table-modal_penjualan').on('click', '.select-row', function() {
            let uid = $(this).data('uid');
            fillPenjualan(uid);
        });
    }*/

    let initializePelayanan = () => {
        let fillFormPelayanan = (uid) => {
            blockElement(modalPelayanan + ' .modal-dialog');
            $.getJSON(URL.getDataPelayanan.replace(':UID', uid), function (res, status) {
                if (status === 'success') {
                    let data = res.data;
                    data.pelayanan = data;

                    $(form).find("#no_register").val(data.no_register);
                    $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                    $(form).find("#asal_pasien").val(data.asal_pasien);
                    $(form).find("#layanan_id").val(data.layanan_id);
                    $(form).find("#dokter_id").val(data.dokter_id);
                    $(form).find("#pelayanan_id").val(data.pelayanan.id);
                    $(form).find("#pelayanan_uid").val(data.pelayanan.uid);
                    $(form).find("#pasien_id").val(data.pelayanan.pasien_id);
                    $(form).find("#cara_bayar_id").val(data.pelayanan.cara_bayar_id);
                    $(form).find("#perusahaan_id").val(data.pelayanan.perusahaan_id);

                    fillDataPasien(data);

                    $(modalPelayanan).modal('hide');
                    $(modalPelayanan + ' .modal-dialog').unblock();
                }
            });
        }

        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_pelayanan'));
        if(isDTable === true) $('#table-modal_pelayanan').DataTable().destroy();

        tablePelayanan = $('#table-modal_pelayanan').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadDataPelayanan,
                "type": "POST",
                "data": function(p) {
                    p.no_register = FORM_SEARCH_MODAL_FIELD.no_register.val();
                    p.no_rekam_medis = FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val();
                    p.nama = FORM_SEARCH_MODAL_FIELD.nama.val();
                }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="select-row" title="Pilih Pasien">${data}</a>`;
                        return tmp;
                    },
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "layanan" },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "dokter" },
            ],
            "order": [ [1, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_pelayanan').find('[data-popup=tooltip]').tooltip();
            },
        });

        $('#table-modal_pelayanan').on('click', '.select-row', function() {
            let uid = $(this).data('uid');
            fillFormPelayanan(uid)
        });

        BTN_SEARCH_MODAL.click(function () {
            tablePelayanan.draw(false);
        });

        $("#btn-cari_pasien").click(function() {
            $(modalPelayanan).modal('show');
            tablePelayanan.draw(false);
        });
    }

    let initializePenjualanObat = () => {
        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_obat'));
        if(isDTable === true) tablePenjualanObat.draw();

        tablePenjualanObat = $('#table-modal_obat').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadPenjualanObat,
                "type": "POST",
                "data": function(p) {
                    p.pelayanan_id = $('#pelayanan_id').val();
                }
            },
            "columns": [
                {
                    "data": "obat",
                    "render": (data, type, row, meta) => {
                        return `<a data-popup="tooltip" class="select-row" title="Pilih Obat">${data}</a>`;
                    }
                },
                {
                    "data": "harga",
                    "orderable": false,
                    "searchable": false,
                    "render": (data, type, row, meta) => {
                        return numeral(data).format();
                    },
                },
                {
                    "data": "quantity",
                    "orderable": false,
                    "searchable": false,
                    "render": (data, type, row, meta) => {
                        return numeral(data).format();
                    },
                },
                {
                    "data": "tanggal",
                    "orderable": false,
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "kode",
                },
            ],
            "order": [ [0, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_obat').find('[data-popup=tooltip]').tooltip();
            },
        });

        $('#table-modal_obat').on('click', '.select-row', function() {
            blockElement(modalPenjualanObat + ' .modal-dialog');
            
            let tr = $(this).closest('tr');
            let data = tablePenjualanObat.row(tr).data();
            let detail = {
                id: 0,
                penjualan_detail_id: data.id,
                stock_id: data.stock_id,
                obat_id: data.obat_id,
                obat_uid: data.obat_uid,
                obat_kode: data.obat_kode,
                obat: data.obat,
                satuan_id: data.satuan_id,
                satuan: data.satuan,
                harga_dasar: data.harga_dasar,
                harga: data.harga,
                quantity_jual: data.quantity,
                quantity: 0,
                detail_barang: data.detail_barang,
            }

            addDetail(detail, "add", 0);
            setTimeout(() => {
                $(modalPenjualanObat + ' .modal-dialog').unblock();
                $(modalPenjualanObat).modal('hide');
            }, 400);
        });

        $("#btn-tambah").click(function() {
            let pelayananId = parseInt($('#pelayanan_id').val());
            if(pelayananId === 0) {
                warningMessage('Peringatan !', 'Pilih Pasien terlebih dahulu.');
                $('#btn-cari_pasien').focus();
                return;
            }

            $(modalPenjualanObat).modal('show');
            tablePenjualanObat.draw(false);
        });
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;

                $(".div-btn-cari_pasien").hide();
                $("#btn-cari_pasien").hide();
                
                $(form).find("#id").val(data.id);
                $(form).find("#uid").val(data.uid);
                $(form).find("#penjualan_id").val(data.penjualan_id);
                $(form).find("#no_penjualan").val(data.no_penjualan);
                $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                $(form).find("#asal_pasien").val(data.asal_pasien);
                $(form).find("#dokter_id").val(data.dokter_id);
                $(form).find("#layanan_id").val(data.layanan_id);
                $(form).find("#pelayanan_id").val(data.pelayanan.id);
                $(form).find("#pelayanan_uid").val(data.pelayanan.uid);
                $(form).find("#pasien_id").val(data.pelayanan.pasien_id);
                $(form).find("#cara_bayar_id").val(data.pelayanan.cara_bayar_id);
                $(form).find("#perusahaan_id").val(data.pelayanan.perusahaan_id);
                $(form).find("#no_register").val(data.pelayanan.no_register);
                $(form).find("#alasan").data("wysihtml5").editor.setValue(data.alasan);
                
                fillDataPasien(data);

                $(tableDetail + " tbody").empty();
                if(data.detail_list.length > 0) {
                    for (var i = 0; i < data.detail_list.length; i++) {
                        data.detail_list[i].detail_barang = data.detail_list[i].detail_barang_penjualan;
                        addDetail(data.detail_list[i], "view", i);
                    }
                } else $(tableDetail + " tbody").append(`<tr><td colspan="7" class="text-center">Tidak Ada Data</td></tr>`);

                $.unblockUI();
            }
        });
    }

    let addDetail = (obj, listMode, index) => {
        let tbody = $(tableDetail + ' tbody');

        if(tbody.find('input[name="detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdKode = $("<td/>")
            .html(obj.obat_kode)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputPenjualanDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'penjualan_detail_id[]')
            .val(obj.penjualan_detail_id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_id[]')
            .addClass('input-obat_id')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'satuan_id[]')
            .addClass('input-satuan_id')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);
        let inputDetailBarang = $("<textarea/>")
            .prop('name', 'detail_barang[]')
            .html(obj.detail_barang)
            .appendTo(tdNama);
        inputDetailBarang.hide();

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQtyJual = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQtyJual = $("<span>")
            .html(numeral(obj.quantity_jual).format())
            .appendTo(tdQtyJual);
        let inputQtyJual = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'qty_jual[]')
            .addClass('input-qty_Jual')
            .val(obj.quantity_jual)
            .appendTo(tdQtyJual);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'old_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'qty[]')
            .addClass('input-qty')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .prop('name', 'disp_qty[]')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();

        let btnDel = $('<span/>')
                       .appendTo(tdAction);
        if(parseInt(UID) === 0) {
            btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
        } else btnDel.html("&mdash;");
        
        // Handler
        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQty.hide();
            dispInputQty.show();

            dispInputQty.focus();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            dispInputQty.hide();
        });

        btnDel.on('click', (e) => {
            tr.remove();

            if(tbody.find('input[name="obat_id[]"]').length <= 0) 
                tbody.append(`<tr><td colspan="7" class="text-center">Tidak ada data</td></tr>`);
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            dispInputQty.hide();
        });

        if(listMode === "add") {
            if(index == 0) tr.click();
        }
        
        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }
        
        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.quantity_jual)) {
                warningMessage('Peringatan', `Qty. Retur tidak boleh lebih dari ${obj.quantity_jual}`);
                val = 0;
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });
        dispInputQty.focus();
    }

    $(form).validate({
        rules: {
            'no_penjualan': { required: true },
            'disp_qty[]': { required: true, min: 1 },
        },
        messages: {
            'disp_qty[]': { 
                        required: "Qty. Retur harus diisi",
                        min: "Qty. Retur Tidak boleh 0",
                    },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            let isValid = true;
            $(tableDetail + ' tbody tr').each(function() {
                if($(this).find('td').html().search(/tidak ada data/i) !== -1) isValid = false;
            });

            if(!isValid) {
                warningMessage('Peringatan', `Tidak ada list barang`);
                return;
            }

            $(tableDetail + ' tbody tr').each(function() {
                if($(this).find('input[name="qty[]"]').val() <= 0) {
                    isValid = false;
                }
            });

            if(!isValid) {
                warningMessage('Peringatan', `Qty. Retur tidak boleh 0`);
                return;
            }

            swal({
                title: "Konfirmasi",
                type: "warning",
                text: "Data yang dimasukkan benar??",
                showCancelButton: true,
                confirmButtonText: "Ya",
                confirmButtonColor: "#2196F3",
                cancelButtonText: "Batal",
                cancelButtonColor: "#FAFAFA",
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
            },
            function() {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Data retur berhasil disimpan.");

                        $('.btn-save').hide();
                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 300);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });       
            });
        }
    });

    initializePelayanan();
    initializePenjualanObat();

    if(parseInt(UID) !== 0) 
        fillForm(UID);
});