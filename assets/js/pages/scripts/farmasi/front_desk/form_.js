$(() => {
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    $(".select-signa").select2({
        placeholder: "- Pilih -",
    });

    let initializePelayanan = () => {
        let fillFormPelayanan = (uid) => {
            blockElement(modalPelayanan + ' .modal-dialog');
            $.getJSON(URL.getDataPelayanan.replace(':UID', uid), function (res, status) {
                if (status === 'success') {
                    $(".section-search").hide();
                    $(".div-label_perusahaan").hide();
                    $(".div-label_no_jaminan").hide();
                    $(".div-label_penjamin_perusahaan").hide();
                    $(".div-label_penjamin_no_jaminan").hide();
                    
                    let data = res.data;
                    $(form).find("#asal_pasien").val(data.asal_pasien);
                    $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                    $(form).find("input[name=pelayanan_id]").val(data.id);
                    $(form).find("input[name=pelayanan_uid]").val(data.uid);
                    $(form).find("#no_register").val(data.no_register);
                    $(form).find("#pasien_id").val(data.pasien_id);
                    $(form).find("#cara_bayar_id").val(data.cara_bayar_id);
                    $(form).find("#perusahaan_id").val(data.perusahaan_id);
                    $(form).find("#layanan_id").val(data.layanan_id);
                    $(form).find("#dokter_id").val(data.dokter_id);

                    $("#label-nama_pasien").html(data.pasien.nama);
                    $("#label-no_rm").html(data.pasien.no_rm);
                    $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                    $("#label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                    $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                    $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                    $("#label-alamat").html(data.pasien.alamat);
                    $("#label-no_telepon").html(data.pasien.no_telepon_1);

                    $("#label-no_register").html(data.no_register);
                    $("#label-tanggal").html(data.tanggal);
                    $("#label-asal_pasien").html(data.asal_pasien_desc);
                    $("#label-layanan").html(data.layanan);
                    $("#label-dokter").html(data.dokter);
                    $("#label-cara_bayar").html(data.cara_bayar);
                    $("#label-perusahaan").html(data.perusahaan);
                    $("#label-no_jaminan").html(data.no_jaminan);
                    $("#label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                    $("#label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                    switch (parseInt(data.cara_bayar_jenis)) {
                        case imediscode.CARA_BAYAR_BPJS:
                            $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                            if(data.penjamin_id) {
                                $(".div-label_penjamin_perusahaan").show();
                                $(".div-label_penjamin_no_jaminan").show();

                                let labelPerusahaan = 'Perusahaan';
                                let labelNoJaminan = 'NIK';
                                if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                    labelPerusahaan = 'Asuransi';
                                    labelNoJaminan = 'No. Anggota';
                                }
                                $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                                $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                            }
                            break;
                        case imediscode.CARA_BAYAR_JAMKESDA:
                            $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                            break;
                        case imediscode.CARA_BAYAR_ASURANSI:
                        case imediscode.CARA_BAYAR_PERUSAHAAN:
                            $(".div-label_perusahaan").show();
                            $(".div-label_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                            break;
                        case imediscode.CARA_BAYAR_INTERNAL:
                            $(".div-label_no_jaminan").show().children('label').html('NIK');
                            break;
                    }
                    initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.cara_bayar_id));
                    $(modalPelayanan).modal('hide');
                    $(modalPelayanan + ' .modal-dialog').unblock();

                    data.pelayanan = data;
                    dataResep = data;
                }
            });
        }

        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_pelayanan'));
        if(isDTable === true) $('#table-modal_pelayanan').DataTable().destroy();

        tablePelayanan = $('#table-modal_pelayanan').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadDataPelayanan,
                "type": "POST",
                "data": function(p) {
                    p.no_register = FORM_SEARCH_MODAL_FIELD.no_register.val();
                    p.no_rekam_medis = FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val();
                    p.nama = FORM_SEARCH_MODAL_FIELD.nama.val();
                }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="select-row" title="Pilih Pasien">${data}</a>`;
                        return tmp;
                    },
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "layanan" },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "dokter" },
            ],
            "order": [ [1, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_pelayanan').find('[data-popup=tooltip]').tooltip();
            },
        });

        $('#table-modal_pelayanan').on('click', '.select-row', function() {
            let uid = $(this).data('uid');
            fillFormPelayanan(uid)
        });

        BTN_SEARCH.click(function () {
            FORM_SEARCH_MODAL_FIELD.no_register.val(FORM_SEARCH_FIELD.no_register.val());
            FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val(FORM_SEARCH_FIELD.no_rekam_medis.val());
            FORM_SEARCH_MODAL_FIELD.nama.val(FORM_SEARCH_FIELD.nama.val());

            $(modalPelayanan).modal('show');
            tablePelayanan.draw(false);
        });

        BTN_SEARCH_MODAL.click(function () {
            FORM_SEARCH_FIELD.no_register.val(FORM_SEARCH_MODAL_FIELD.no_register.val());
            FORM_SEARCH_FIELD.no_rekam_medis.val(FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val());
            FORM_SEARCH_FIELD.nama.val(FORM_SEARCH_MODAL_FIELD.nama.val());

            tablePelayanan.draw(false);
        });
    }

    // FARMASI UNIT
    function initializeFarmasiUnit(el, cara_bayar_id) {
        $.getJSON(URL.getFarmasiUnit.replace(':CARA_BAYAR_ID', cara_bayar_id), function(data, status) {
            if (status === "success") {
                let option = `<option value="0" selected="selected">- Pilih -</option>`;
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (data.data.length === 1) selected = "selected='selected'";
                    option += `<option value="${data.data[i].uid}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");

                el.prop('disabled', false);
                if (data.data.length === 1) el.prop('disabled', true);
            }
        });
    }

    // SIGNA
    function initializeSigna(el, signa_id) {
        $.getJSON(URL.getSigna, function(data, status) {
            if (status === "success") {
                let option = '';
                let arrSignaId = signa_id.split(",");
                for (var i = 0; i < data.data.length; i++) {
                    for(var j = 0; j < arrSignaId.length; j++) {
                        let selected = "";
                        if (arrSignaId[j] === data.data[i].id) selected = "selected='selected'";
                        option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].label}</option>`;
                    }
                }
                el.html(option).trigger("change");
            }
        });
    }
    initializeSigna($("#modal-input_racikan_signa"), "");

    let fillForm = (uid) => {
        listResepObat = [];

        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".section-search").hide();
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                
                let data = res.data;
                $('.page-title').html(`<h4><i class="icon-copy2"></i> Resep ${data.nomor}</h4>`);
                $(form).find("#no_resep").val(data.nomor);
                $(form).find("#resep_id").val(data.id);
                $(form).find("#asal_pasien").val(data.asal_pasien);
                $(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
                $(form).find("input[name=pelayanan_id]").val(data.pelayanan.id);
                $(form).find("input[name=pelayanan_uid]").val(data.pelayanan.uid);
                $(form).find("#no_register").val(data.pelayanan.no_register);
                $(form).find("#pasien_id").val(data.pelayanan.pasien_id);
                $(form).find("#cara_bayar_id").val(data.pelayanan.cara_bayar_id);
                $(form).find("#perusahaan_id").val(data.pelayanan.perusahaan_id);
                $(form).find("#layanan_id").val(data.pelayanan.layanan_id);
                $(form).find("#dokter_id").val(data.pelayanan.dokter_id);

                $("#label-nama_pasien").html(data.pasien.nama);
                $("#label-no_rm").html(data.pasien.no_rm);
                $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $("#label-alamat").html(data.pasien.alamat);
                $("#label-no_telepon").html(data.pasien.no_telepon_1);

                $("#label-no_register").html(data.pelayanan.no_register);
                $("#label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
                $("#label-asal_pasien").html(data.asal_pasien_desc);
                $("#label-layanan").html(data.pelayanan.layanan);
                $("#label-dokter").html(data.pelayanan.dokter);
                $("#label-cara_bayar").html(data.pelayanan.cara_bayar);
                $("#label-perusahaan").html(data.pelayanan.perusahaan);
                $("#label-no_jaminan").html(data.pelayanan.no_jaminan);
                $("#label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
                $("#label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

                switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.pelayanan.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }

                // NON RACIKAN
                $(tableNonRacikan + " tbody").empty();
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    addNonRacikan(data.non_racikan_list[i], "view");
                }

                // RACIKAN
                $(tableRacikan + " tbody").empty();
                for (var i = 0; i < data.racikan_list.length; i++) {
                    addRacikan(data.racikan_list[i], "view");
                }

                $.unblockUI();

                dataResep = data;

                initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.pelayanan.cara_bayar_id));
            }
        });
    }

    // NON RACIKAN
    let fetchObat = () => {
        blockElement(modalObat + ' .modal-dialog');
        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_obat'));
        if(isDTable === true) $('#table-modal_obat').DataTable().destroy();

        let farmasi_unit = $('#modal-obat_farmasi_unit').val();
        let cara_bayar_id = btoa($(form).find('#cara_bayar_id').val());

        $.getJSON(URL.fetchObat.replace(':FARMASI_UNIT', farmasi_unit).replace(':CARA_BAYAR_ID', cara_bayar_id), (res, status) => {
            if (status === 'success') {
                tableListObat = $('#table-modal_obat').DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": res.data,
                    "columns": [
                        { 
                            "data": "id",
                            "render": function (data, type, row, meta) {
                                let dataAttr = [
                                                `data-id="${data}"`,
                                                `data-barang_id="${row.barang_id}"`,
                                                `data-barang_uid="${row.barang_uid}"`,
                                                `data-barang="${row.barang}"`,
                                                `data-satuan_id="${row.satuan_id}"`,
                                                `data-satuan="${row.satuan}"`,
                                                `data-harga="${row.harga}"`,
                                                `data-harga_dasar="${row.harga_dasar}"`,
                                                `data-stock="${row.stock_tmp}"`,
                                                ];
                                return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
                            },
                        },
                        { "data": "kode_barang" },
                        { "data": "barang" },
                        { "data": "satuan" },
                        { 
                            "data": "harga",
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        },
                        { "data": "stock_tmp" },
                    ],
                    "drawCallback": function (settings) {
                        $('.check').uniform();
                    },
                });
                setTimeout(() => {
                    $(modalObat + ' .modal-dialog').unblock();
                }, 500);
            };
        });
    }

    let addNonRacikan = (obj, listMode) => {
        obj.browse = 'non_racikan';
        listResepObat.push(obj);
        let tbody = $(tableNonRacikan + ' tbody');

        if(tbody.find('input[name="non_racikan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('non_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_id[]')
            .addClass('input-non_racikan_obat_id')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan_id[]')
            .addClass('input-non_racikan_satuan_id')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQtyResep = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQtyResep = $("<span>")
            .html(numeral(obj.old_quantity).format())
            .appendTo(tdQtyResep);
        let inputQtyResep = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty_resep[]')
            .addClass('input-non_racikan_qty_resep')
            .val(obj.old_quantity)
            .appendTo(tdQtyResep);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty[]')
            .addClass('input-non_racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdSigna = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelSigna = $("<span/>")
            .html(obj.label_signa)
            .appendTo(tdSigna);
        let inputTextSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_signa[]')
            .prop('value', obj.signa)
            .appendTo(tdSigna);
        let inputSigna = $("<select/>")
            .addClass('form-control')
            .prop('multiple', 'multiple')
            .appendTo(tdSigna);
        inputSigna.select2({
            placeholder: "- Pilih -",
        });
        inputSigna.hide().next(".select2-container").hide();

        let tdIter = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelIter = $("<span>")
            .html(numeral(0).format())
            .appendTo(tdIter);
        let inputIter = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_iter[]')
            .addClass('input-non_racikan_iter')
            .val(0)
            .appendTo(tdIter);
        let dispInputIter = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdIter);
        dispInputIter.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputIter.autoNumeric('set', 0).hide();

        let tdAsuransi = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelAsuransi = $("<span/>")
            .html('&mdash;')
            .appendTo(tdAsuransi);
        let divInputAsuransi = $("<div/>")
            .appendTo(tdAsuransi);
        let labelInputAsuransi = $("<label/>")
            .appendTo(divInputAsuransi);
        let inputAsuransi = $("<input/>")
            .prop('type', 'checkbox')
            .addClass('check')
            .appendTo(labelInputAsuransi);
        let inputHiddenAsuransi = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_is_asuransi[]')
            .val(0)
            .appendTo(tdAsuransi);
        inputAsuransi.uniform({radioClass: 'choice'});
        divInputAsuransi.hide();

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);
        
        // Handler
        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQty.hide();
            dispInputQty.show();
            labelSigna.hide();
            inputSigna.show().next(".select2-container").show();
            labelIter.hide();
            dispInputIter.show();
            labelAsuransi.hide();
            divInputAsuransi.show();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            dispInputQty.hide();
            labelSigna.show();
            inputSigna.hide().next(".select2-container").hide();
            labelIter.show();
            dispInputIter.hide();
            labelAsuransi.show();
            divInputAsuransi.hide();
        });

        btnDel.on('click', (e) => {
            tr.remove();
            let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
            if(idxLro !== -1) listResepObat.splice(idxLro, 1);

            if(tbody.find('input[name="non_racikan_obat_id[]"]').length <= 0) 
                tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelSignaArr = [];
            inputSigna.find('option:selected').each(function() {
                if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                    labelSignaArr.push($(this).html());
            });

            labelSigna.html('&mdash;');
            let tmpLabel = '&mdash;'
            if(labelSignaArr.length > 0) {
                tmpLabel = `<ul class="text-left">`;
                for(let i = 0; i < labelSignaArr.length; i++) {
                    tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                labelSigna.html(tmpLabel);
            }

            let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
            if(idxLro !== -1) {
                listResepObat[idxLro].quantity = inputQty.val();
                listResepObat[idxLro].label_signa = tmpLabel;
            }

            labelQty.show();
            dispInputQty.hide();
            labelSigna.show();
            inputSigna.hide().next(".select2-container").hide();
            labelIter.show();
            dispInputIter.hide();
            labelAsuransi.show();
            divInputAsuransi.hide();
        });

        if(listMode === "add") tr.click();
        
        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }
        initializeSigna(inputSigna, obj.signa);

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputIter.on('keyup change blur', (e) => {
            let val = dispInputIter.autoNumeric('get') == "" ? 0 : dispInputIter.autoNumeric('get');
            inputIter.val(val);
            labelIter.html(val);
        });

        inputSigna.on('change blur', (e) => {
            inputTextSigna.val(inputSigna.val());
        });

        inputAsuransi.on('change click', function () {
            labelAsuransi.html($(this).prop('checked') ? '<i class="fa fa-check"></i>' : '&mdash;');
            inputHiddenAsuransi.val($(this).prop('checked') ? 1 : 0);
        });

        dispInputQty.focus();
    }

    $(form).find("#btn-tambah_non_racikan").on('click', () => {
        $('#modal-obat-input_mode').val('non_racikan');
        fetchObat();
        $(modalObat).modal('show');
    });

    // RACIKAN
    let addRacikan = (obj, listMode) => {
        obj.browse = 'racikan';
        listResepObat.push(obj);
        let tbody = $(tableRacikan + ' tbody');

        if(tbody.find('.input-racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('racikan_uid', obj.uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .appendTo(tr);
        let labelNama = $("<span>")
            .html(obj.nama + obj.label_list_obat)
            .addClass('label-racikan_nama')
            .appendTo(tdNama);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_id[]')
            .addClass('input-racikan_id')
            .val(obj.id ? obj.id : 0)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_nama')
            .prop('name', 'racikan_nama[]')
            .val(obj.nama)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_uid')
            .prop('name', 'racikan_uid[]')
            .val(obj.uid)
            .appendTo(tdNama);
        let inputListObat = $("<textarea/>")
            .addClass('input-racikan_list_obat')
            .prop('name', 'racikan_list_obat[]')
            .val(json_encode(obj.list_obat))
            .html(json_encode(obj.list_obat))
            .appendTo(tdNama);
        inputListObat.hide();

        let tdMetode = $("<td/>")
            .appendTo(tr);
        let labelMetode = $("<span>")
            .html(obj.label_metode)
            .addClass('label-racikan_metode')
            .appendTo(tdMetode);
        let inputMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_metode[]')
            .addClass('input-racikan_metode')
            .val(obj.metode)
            .appendTo(tdMetode);

        let tdCaraBuat = $("<td/>")
            .appendTo(tr);
        let labelCaraBuat = $("<span>")
            .html(obj.label_cara_buat)
            .addClass('label-racikan_cara_buat')
            .appendTo(tdCaraBuat);
        let inputCaraBuat = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_cara_buat[]')
            .addClass('input-racikan_cara_buat')
            .val(obj.cara_buat)
            .appendTo(tdCaraBuat);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .addClass('label-racikan_harga')
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_harga[]')
            .addClass('input-racikan_harga')
            .val(obj.harga)
            .appendTo(tdHarga);

        let tdQtyResep = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQtyResep = $("<span>")
            .html(numeral(obj.old_quantity).format())
            .appendTo(tdQtyResep);
        let inputQtyResep = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty_resep[]')
            .addClass('input-racikan_qty_resep')
            .val(obj.old_quantity)
            .appendTo(tdQtyResep);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .addClass('label-racikan_qty')
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty[]')
            .addClass('input-racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdSigna = $("<td/>")
            .appendTo(tr);
        let labelSigna = $("<span>")
            .html(obj.label_signa)
            .addClass('label-racikan_signa')
            .appendTo(tdSigna);
        let inputSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_signa[]')
            .addClass('input-racikan_signa')
            .val(obj.signa)
            .appendTo(tdSigna);

        let tdIter = $("<td/>")
            .addClass('text-center lc')
            .appendTo(tr);
        let inputIter = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_iter[]')
            .addClass('input-racikan_iter')
            .val(0)
            .appendTo(tdIter);
        let dispInputIter = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .val(0)
            .appendTo(tdIter);
        dispInputIter.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});

        let tdAsuransi = $("<td/>")
            .addClass('text-center lc')
            .appendTo(tr);
        let divInputAsuransi = $("<div/>")
            .appendTo(tdAsuransi);
        let labelInputAsuransi = $("<label/>")
            .appendTo(divInputAsuransi);
        let inputAsuransi = $("<input/>")
            .prop('type', 'checkbox')
            .addClass('check')
            .appendTo(labelInputAsuransi);
        let inputHiddenAsuransi = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_is_asuransi[]')
            .val(0)
            .appendTo(tdAsuransi);
        inputAsuransi.uniform({radioClass: 'choice'});
        
        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);
        
        // Handler
        tr.find('td:not(".lc")').on('click', (e) => {
            $('#modal-input_racikan_uid').val(obj.uid);
            $(formRacikan).find('#modal-input_racikan_nama').val(obj.nama);
            $(formRacikan).find('#modal-input_racikan_metode').val(obj.metode).change();
            $(formRacikan).find('#modal-input_racikan_cara_buat').val(obj.metode).change();
            $(formRacikan).find('#modal-input_racikan_harga').autoNumeric('set', obj.harga);
            $(formRacikan).find('#modal-input_racikan_qty').autoNumeric('set', obj.quantity);
            initializeSigna($(formRacikan).find('#modal-input_racikan_signa'), inputSigna.val());

            $(tableRacikanDetail + ' tbody').empty();
            let listObat = obj.list_obat;
            for(let i = 0; i < listObat.length; i++) 
                addRacikanDetail(listObat[i], "view");

            $(modalRacikan).modal('show');
        });

        btnDel.on('click', (e) => {
            tr.remove();
            let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
            if(idxLro !== -1) listResepObat.splice(idxLro, 1);

            if(tbody.find('.input-racikan_id').length <= 0) 
                tbody.append(`<tr><td colspan="7" class="text-center">Tidak ada data</td></tr>`);

        });

        dispInputIter.on('keyup change blur', (e) => {
            let val = dispInputIter.autoNumeric('get') == "" ? 0 : dispInputIter.autoNumeric('get');
            inputIter.val(val);
        });
    }

    let addRacikanDetail = (obj, listMode) => {
        let tbody = $(tableRacikanDetail + ' tbody');

        if(tbody.find('.input-item_racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('item_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-item_racikan_id')
            .prop('name', 'item_racikan_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);
        
        // Handler
        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQty.hide();
            dispInputQty.show();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            dispInputQty.hide();
        });

        btnDel.on('click', (e) => {
            tr.remove();
            updateTotal();
            if(tbody.find('.input-item_racikan_id').length <= 0) 
                tbody.append(`<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>`);

        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            dispInputQty.hide();

            updateTarifRow();
        });

        if(listMode === "add") tr.click();

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());

            updateTotal();
        }

        function updateTotal() {
            let total = 0;
            $(tableRacikanDetail + " tbody").find('tr').each((i, el) => {
                total += numeral($(el).find('td:eq(3)').html())._value;
            });
            $("#modal-input_racikan_harga").autoNumeric('set', total);
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputQty.focus();
    }

    $(form).find("#btn-tambah_racikan").on('click', () => {
        $(tableRacikanDetail + ' tbody').empty();
        $(modalRacikan).find('input, select').val('').trigger('change');
        $(modalRacikan).modal('show');
    });

    $(formRacikan).find("#btn-tambah_racikan_detail").on('click', () => {
        $('#modal-obat-input_mode').val('racikan');
        fetchObat();
        $(modalObat).modal('show');
    });

    // MODAL OBAT Handler
    $('#table-modal_obat').on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListObat.row(tr).data();
        data.checked = $(this).prop('checked');
    });

    $("#btn-tambah_modal_obat").on('click', () => {
        let mode = $('#modal-obat-input_mode').val();
        tableListObat.rows().nodes().each(function (i, dt_index) {
            let trData = tableListObat.row(dt_index).data();
            if(trData.checked) {
                let id = trData.id;
                let barang_id = trData.barang_id;
                let barang_uid = trData.barang_uid;
                let barang = trData.barang;
                let satuan_id = trData.satuan_id;
                let satuan = trData.satuan;
                let harga = trData.harga;
                let harga_dasar = trData.harga_dasar;
                let stock = trData.stock_tmp;

                let isExists = false;
                let param = {
                    table: tableRacikanDetail,
                    label: 'item_racikan_obat_uid'
                };
                if(mode === "non_racikan") {
                    param = {
                        table: tableNonRacikan,
                        label: 'non_racikan_obat_uid'
                    };
                }

                $(param.table + " tbody").find('tr').each((i, el) => {
                    if ($(el).data(param.label) == barang_uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    uid: makeid(50),
                    stock_id: id,
                    stock: stock,
                    obat_id: barang_id,
                    obat_uid: barang_uid,
                    obat: barang,
                    satuan_id: satuan_id,
                    satuan: satuan,
                    harga: harga,
                    harga_dasar: harga_dasar,
                    old_quantity: 0,
                    quantity: 1,
                    signa: '',
                    label_signa: '&mdash;',
                };

                if(mode === "non_racikan") {
                    addNonRacikan(data, "add");
                } else addRacikanDetail(data, "add");
            }
        });

        $(modalObat).modal('hide');
    });

    $(formRacikan).validate({
        rules: {
            nama: { required: true },
            metode: { required: true },
            cara_buat: { required: true },
            harga: { required: true },
            quantity: { required: true },
            "signa[]": { required: true },
        },
        messages: {
            //
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalRacikan + ' .modal-dialog');
            let postData = $(formRacikan).serializeArray();

            let data = {};
            let d, fieldname, isArray, tmpLabel;
            for (let i = 0; i < postData.length; i++) {
                d = postData[i];
                isArray = false;

                if (d.name.search(/\[\]/) !== -1) {
                    fieldname = d.name.replace(/\[\]/, '');
                    isArray = true;
                } else {
                    fieldname = d.name;
                    isArray = false;
                }

                if (isArray) {
                    if (! data[fieldname]) {
                        data[fieldname] = [];
                        data[fieldname].push(d.value);
                    } else {
                        data[fieldname].push(d.value)
                    }
                } else {
                    data[fieldname] = d.value;
                }
            }

            data['label_metode'] = $('#modal-input_racikan_metode').find('option:selected').html();
            data['label_cara_buat'] = $('#modal-input_racikan_cara_buat').find('option:selected').html();
            data['harga'] = $('#modal-input_racikan_harga').autoNumeric('get');
            data['quantity'] = $('#modal-input_racikan_qty').autoNumeric('get');

            let labelSignaArr = [];
            $('#modal-input_racikan_signa').find('option:selected').each(function() {
                if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                    labelSignaArr.push($(this).html());
            });

            data['label_signa'] = '&mdash;';
            if(labelSignaArr.length > 0) {
                tmpLabel = `<ul class="text-left">`;
                for(let i = 0; i < labelSignaArr.length; i++) {
                    tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_signa'] = tmpLabel;
            }

            let listObat = [];
            let labelListObat = [];
            for (let i = 0; i < data.item_racikan_id.length; i++) {
                listObat.push({
                    id: data.item_racikan_id[i],
                    stock_id: data.item_stock_id[i],
                    obat_id: data.item_obat_id[i],
                    obat_uid: data.item_obat_uid[i],
                    obat: data.item_obat[i],
                    satuan_id: data.item_satuan_id[i],
                    satuan: data.item_satuan[i],
                    harga: data.item_harga[i],
                    harga_dasar: data.item_harga_dasar[i],
                    old_quantity: data.item_old_qty[i],
                    quantity: data.item_qty[i],
                });
                labelListObat.push(`${data.item_obat[i]} X ${data.item_qty[i]} ${data.item_satuan[i]}`);
            }
            data['list_obat'] = listObat;
            if(data['uid'] === "") data['uid'] = makeid(50);

            if(labelListObat.length > 0) {
                tmpLabel = `<ul class="text-left text-size-mini text-slate-300">`;
                for(let i = 0; i < labelListObat.length; i++) {
                    tmpLabel += `<li>${labelListObat[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_list_obat'] = tmpLabel;
            }

            let isExists = false;
            $(tableRacikan + " tbody").find('tr').each(function() {
                if(data['uid'] === $(this).data('racikan_uid')) 
                    isExists = $(this);
            });

            if(isExists) {
                isExists.find('.input-racikan_list_obat').val(json_encode(data.list_obat)).html(json_encode(data.list_obat));
                isExists.find('.input-racikan_nama').val(data.nama);
                isExists.find('.label-racikan_nama').html(data.nama + data.label_list_obat);
                isExists.find('.input-racikan_metode').val(data.metode);
                isExists.find('.label-racikan_metode').html(data.label_metode);
                isExists.find('.input-racikan_cara_buat').val(data.cara_buat);
                isExists.find('.label-racikan_cara_buat').html(data.label_cara_buat);
                isExists.find('.label-racikan_harga').html('Rp.' + numeral(data.harga).format());
                isExists.find('.input-racikan_harga').val(data.harga);
                isExists.find('.label-racikan_qty').html(numeral(data.quantity).format());
                isExists.find('.input-racikan_qty').val(data.quantity);
                isExists.find('.label-racikan_signa').html(data.label_signa);
                isExists.find('.input-racikan_signa').val(data.signa);

                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(data.uid);
                if(idxLro !== -1) {
                    listResepObat[idxLro].nama = data.nama;
                    listResepObat[idxLro].label_metode = data.label_metode;
                    listResepObat[idxLro].metode = data.metode;
                    listResepObat[idxLro].label_cara_buat = data.label_cara_buat;
                    listResepObat[idxLro].cara_buat = data.cara_buat;
                    listResepObat[idxLro].harga = data.harga;
                    listResepObat[idxLro].quantity = data.qty;
                    listResepObat[idxLro].label_signa = data.label_signa;
                    listResepObat[idxLro].signa = data.signa;
                }
            } else addRacikan(data, "add");
            setTimeout(() => {
                $(modalRacikan + ' .modal-dialog').unblock();
            
                $(modalRacikan).modal('hide');
            }, 400);
        }
    });

    $('#btn-preview_resep').click(function() {
        if(listResepObat.length <= 0) {
            warningMessage('Peringatan', 'Racikan dan Non Racikan belum terisi');
            return;
        }

        dataResep.list_obat = listResepObat;
        $(modalPreviewResep).find('.label-no_resep').html(dataResep.nomor);
        $(modalPreviewResep).find('.label-dokter').html(dataResep.pelayanan.dokter);
        $(modalPreviewResep).find('.label-layanan').html(dataResep.pelayanan.layanan);
        $(modalPreviewResep).find('.label-regMr').html(`${dataResep.pelayanan.no_register} / ${dataResep.pasien.no_rm}`);
        $(modalPreviewResep).find('.label-tanggal').html(moment(dataResep.tanggal).isValid() ? moment(dataResep.tanggal).format('DD-MM-YYYY HH:mm') : moment().format('DD-MM-YYYY HH:mm'));
        $(modalPreviewResep).find('.label-pro').html(dataResep.pasien.nama);
        $(modalPreviewResep).find('.label-umur').html(`${dataResep.pasien.umur_tahun} Tahun ${dataResep.pasien.umur_bulan} Bulan ${dataResep.pasien.umur_hari} Hari`);

        let tmpLabel = `<ul class="text-left" style="list-style-type: none;">`;
        for(let i = 0; i < listResepObat.length; i++) {
            if(listResepObat[i].browse === "non_racikan") {
                tmpLabel += `<li>${listResepObat[i].obat} X ${listResepObat[i].quantity} ${listResepObat[i].satuan}</li>`;
            } else 
                tmpLabel += `<li>${listResepObat[i].nama} - ${listResepObat[i].label_cara_buat}</li>`;

            if(listResepObat[i].label_signa !== "&mdash;")
                tmpLabel += `<li>${listResepObat[i].label_signa}</li>`;
        }
        tmpLabel += `</ul>`;
        $(modalPreviewResep).find('.label-list_obat').html(tmpLabel);

        $(modalPreviewResep).modal('show');
    });

    $(form).validate({
        rules: {},
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: URL.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    successMessage('Success', "Data Resep berhasil disimpan.");

                    $('.btn-save').hide();
                    $('#btn-tambah_racikan').parents('tfoot').show();
                    $('#btn-tambah_non_racikan').parents('tfoot').show();
                    $('#btn-preview_resep').hide();
                    $('#btn-copy_resep').show();

                    penjualanUID = result.data;
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    $('#btn-copy_resep').click(function() {
        window.open(URL.cetakCopyResep.replace(':UID', penjualanUID).replace(':BROWSE', 'copy_resep'), "COPY RESEP", "scrollbars=1, height=700, width=700");
    });

    initializePelayanan();

    if(parseInt(UID) !== 0) 
        fillForm(UID);
    $('.sidebar-control').click();
});