let checkStock = (type, table, el_stock, el_qty, el_oldQty, blockEl, process) => {
    let tbody = table.find('tbody');
    let length = tbody.children().length;
    if(length) {
        let tr = tbody.find('tr');
        blockElement(blockEl);
        if(type == 'racikan') {
            tr.each(function() {
                let itemList = JSON.parse($(this).find('.input-racikan_list_obat').val());
                for(let i = 0; i < itemList.length; i++) {
                    itemList[i].old_quantity = 0;
                    getCheckStock($(this), itemList[i], blockEl, process);
                }
            });
        } else {
            tr.each(function() {
                let obj = {
                    stock_id: $(this).find(el_stock).val(),
                    quantity: isNaN(parseFloat($(this).find(el_qty).val())) ? parseFloat(0) : parseFloat($(this).find(el_qty).val()),
                    old_quantity: isNaN(parseFloat($(this).find(el_oldQty).val())) ? parseFloat(0) : parseFloat($(this).find(el_oldQty).val()),
                }
                getCheckStock($(this), obj, blockEl, process);
            });
        }
    }
}

let getCheckStock = (tr, obj, blockEl, process) => {
    let showAlert = false;
    let stock_id = obj.stock_id;
    let qty = parseFloat(obj.quantity);
    let old_qty = parseFloat(obj.old_quantity);
    console.log('OLD:'+stock_id+':'+old_qty);
    
    tr.children().removeClass('bg-danger-300');
    $.getJSON(base_url + `/api/farmasi/stock/get_stock_by?mode=cek_stock&stock_id=${btoa(stock_id)}`, function(data, status) {
        if (status === 'success') {
            let stock = 0;
            if(data.data) {
                stock = isNaN(parseFloat(data.data.qty)) ? 0 : parseFloat(data.data.qty);
                stock += old_qty;

                console.log('STOCK:'+stock_id+':'+stock);
            }

            if(qty > stock) {
                showAlert = true;
                tr.children().addClass('bg-danger-300');

                if(process === "") {
                    swal({
                        title: "Peringatan !!",
                        text: "Ada Resep yang barangnya kurang atau tidak tersedia.",
                        html: true,
                        confirmButtonColor: "#2196F3"
                    });
                }
            }

            var cur = tr;
            var next = cur.next().length ? true : false;
            if(!next) {
                $(blockEl).unblock();
            }
        }
    });
}   