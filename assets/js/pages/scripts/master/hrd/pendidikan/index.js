let api_url = base_url + '/api/master/hrd/pendidikan';
let table = {};
let dataSearch = {};

$(function() {
  table = $('#table_pendidikan').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": api_url,
      "type": "POST"
    },
    "columns": [
    { 
      "data": "nama",
      "render": function(data, type, row) {
        // return '<a href="' + pendidikan.url.form + '/' + row.uid + '">' + data + '</a>';
        return '<a href="javascript:void(0)" onclick="pendidikan.editForm(\'' + row.uid + '\')">' + data + '</a>';
      }, 
    },
    { 
      "data": "action",
      "className": "text-center",
      "sortable": false,
      "searchable": false,
      "render": function(data, type, row) {
        return '<a class="text-danger" href="' + pendidikan.url.delete + '/' + row.uid + '" onclick="return pendidikan.delete(this)"><i class="fa fa-trash"></i></a>';
      }, 
    }
    ]
  } );

  $('#modal_default').on('hidden.bs.modal', function (e) {
    app.resetForm()
  })
});

var pendidikan = {
  url : {
    delete : api_url + '/delete',
    form : base_url + '/hrd/pendidikan/form'
  },
  editForm(id) {
    app.editForm(id)
  },
  delete(el) {
    let url = $(el).attr('href');
    swal({
      title: "Apakah anda yakin?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      buttons: ["Tidak", "Ya"]
    })
    .then((confirm) => {
      if (confirm) {
        $.get(url, function(res) {
          swal("Data pendidikan berhasil dihapus.", {
            icon: "success",
          });
          table.ajax.reload();
        }, "json");
      }
    });
    return false;
  }
}

Vue.use(VeeValidate)

var app = new Vue({
  el: '#app-pendidikan',
  data() {
    return {
      form: {
        nama: ''
      },
      uid: '',
      loading: false
    }
  },
  methods: {
    resetForm () {
      this.uid = ''
      this.$nextTick(() => {
        this.form.nama = ''
        this.errors.clear()
      });
    },
    editForm (id) {
      this.uid = id
      this.loading = true
      axios.get(api_url + '/get/' + id).then((response) => {
        res = response.data
        if (res.status == 1) {
          this.form.nama = res.data.nama
          $('#modal_default').modal('show')
        }
        this.loading = false
      })
    },
    validateBeforeSubmit () {
      this.$validator.validateAll().then((result) => {
        if (result) {
          // eslint-disable-next-line
          this.loading = true
          const data = new URLSearchParams()
          data.append('nama', this.form.nama)
          axios.post(api_url + '/store/' + this.uid, data, {
            headers: {
              "Content-Type": 'application/x-www-form-urlencoded'
            }
          }).then((response) => {
            res = response.data
            if (res.status == 1) {
              icon = "success"
              title = "Berhasil"
            } else {
              icon = "error"
              title = "Gagal!"
            }
            swal({
              title: title,
              text: res.message,
              icon: icon,
            }).then((e) => {
              $('#modal_default').modal('hide')
              table.ajax.reload()
            })
            this.loading = false
          })
          return;
        }
      });
    }
  }
});