$(function () {
    /**
     * HELPERS
     */
     //

    let disableForm = () => {
        FORM.find('button').prop('disabled', true);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }
    let enableForm = () => {
        FORM.find('button').prop('disabled', false);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }

    let fetchLookupObat = (cb) => {
        $.getJSON(URL.fetchLookupObat, function (res, status) {
            if (status === 'success') {
                TABLE_LOOKUP_OBAT_DT.clear().draw();
                TABLE_LOOKUP_OBAT_DT.rows.add(res.data).draw();

                if (cb) {
                    cb();
                }
            } else {
                //
            }
        });
    }

    let fetchUkuranFoto = (selected_ukuran) => {
        $.getJSON(URL.fetchUkuranFoto, (res, status) => {
            if (status === 'success') {
                let data = res.data;
                
                TABLE_UKURAN_FOTO.find('tbody').empty();
                let id;
                for (let d of data) {
                    id = parseInt(d.id);
                    addUkuranFoto(d, selected_ukuran.indexOf(id) !== -1);
                }
            }
        });
    }

    let addUkuranFoto = (data, isSelected) => {
        let tbody = TABLE_UKURAN_FOTO.find('tbody');

        let tr = $("<tr/>")
            .appendTo(tbody);
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'ukuran_foto_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_selected = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'ukuran_foto_selected[]')
            .val(isSelected ? 1 : 0)
            .appendTo(tr);

        let tdSelect = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let inputSelect = $("<input/>")
            .prop('type', 'checkbox')
            .prop('checked', isSelected)
            .appendTo(tdSelect);

        let tdKode = $("<td/>")
            .addClass('text-center')
            .html(data.kode)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .addClass('text-left')
            .html(data.nama)
            .appendTo(tr);

        // CHANGE
        inputSelect.on('change', function () {
            if ($(this).prop('checked')) {
                input_selected.val(1);
            } else {
                input_selected.val(0);
            }
        });
    }

    let fetchParent = (parent_id) => {
        $.getJSON(URL.fetchParent, (res, status) => {
            if (status === 'success') {
                let data = res.data;

                FORM.find('[name=parent_id]').empty();
                let temp;
                for (let i = 0; i < data.length; i++) {
                    temp = $("<option/>")
                        .prop('value', data[i].id)
                        .html('&mdash;'.repeat(data[i].lvl) + ' ' + (data[i].kode ? `${data[i].kode} - ` : '') + data[i].nama)
                        .appendTo(FORM.find('[name=parent_id]'));
                }

                FORM.find('[name=parent_id]').val(parent_id).trigger('change');

                if (data.length <= 0) {
                    $("<option/>")
                        .prop('value', 0)
                        .html('Root')
                        .appendTo(FORM.find('[name=parent_id]'));
                    FORM.find('[name=parent_id]').val(1).trigger('change');
                }
            }
        });
    }

    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#kode").val(data.kode);
                $("#nama").val(data.nama);
                $(`[name=jenis][value=${data.jenis}]`).prop('checked', true).trigger('change');
                $("#old_parent_id").val(data.parent_id);
                $("#parent_id").val(data.parent_id);
                $("#deskripsi").val(data.deskripsi);
                $("#disp_deskripsi").summernote('code', data.deskripsi);
                fetchParent(data.parent_id);

                let ukuran_foto = json_decode(data.ukuran_foto);
                for (let i = 0; i < ukuran_foto.length; i++) {
                    ukuran_foto[i] = parseInt(ukuran_foto[i]);
                }
                fetchUkuranFoto(ukuran_foto);

                // Obat
                TABLE_OBAT.find('tbody').empty();
                for (let obat of data.obats) {
                    console.log('OBAT', obat);
                    addObat(obat);
                }

                if (data.deleted_flag == 1) {
                    BTN_DELETE.hide();
                    BTN_RESTORE.show();
                    LABEL_DELETED.show();
                    disableForm();
                } else {
                    BTN_RESTORE.hide();
                    BTN_DELETE.show();
                    LABEL_DELETED.hide();
                    enableForm();
                }
            }
        });
    }

    let addObat = (data, mode) => {
        let tbody = TABLE_OBAT.find('tbody');

        let tr = $("<tr/>")
            .data('uid', makeid(10))
            .data('obat_id', data.obat_id)
            .appendTo(tbody);
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_obat_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_obat_id[]')
            .val(data.obat_id)
            .appendTo(tr);
        let input_quantity = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_quantity[]')
            .val(data.quantity)
            .appendTo(tr);
        let input_status = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_status[]')
            .val(data.status)
            .appendTo(tr);

        let tdKode = $("<td/>")
            .appendTo(tr);
        tdKode.html(data.obat_kode);

        let tdNama = $("<td/>")
            .appendTo(tr);
        tdNama.html(data.obat);

        let tdQuantity = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQuantity = $("<span/>")
            .addClass('text-right')
            .appendTo(tdQuantity)
            .html(numeral(data.quantity).format(',.##'));
        let dispInputQuantity = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right disp_input_quantity')
            .appendTo(tdQuantity);
        dispInputQuantity.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQuantity.autoNumeric('set', data.quantity);

        let tdStatus = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnStatus = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-sm text-center')
            .appendTo(tdStatus);
        if (parseInt(data.status) == 1) {
            btnStatus
                .removeClass('bg-slate')
                .addClass('bg-success')
                .html(LABEL_STATUS_ACTIVE);
        } else {
            btnStatus
                .removeClass('bg-success')
                .addClass('bg-slate')
                .html(LABEL_STATUS_INACTIVE);
        }

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handler
        btnDel.on('click', (e) => {
            tr.remove();
        });

        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQuantity.hide()
            dispInputQuantity.show();

            dispInputQuantity.focus();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        });

        dispInputQuantity.on('keyup change blur', (e) => {
            input_quantity.val(dispInputQuantity.autoNumeric('get'));
            labelQuantity.html(dispInputQuantity.autoNumeric('get'));

            if (dispInputQuantity.autoNumeric('get') == "") {
                dispInputQuantity.autoNumeric('set', 0);
                dispInputQuantity.trigger('change');
            }
        });

        btnStatus.on('click', (e) => {
            if (parseInt(input_status.val()) == 1) {
                input_status.val(0);
                btnStatus
                    .removeClass('bg-success')
                    .addClass('bg-slate')
                    .html(LABEL_STATUS_INACTIVE);
            } else {
                input_status.val(1);
                btnStatus
                    .removeClass('bg-slate')
                    .addClass('bg-success')
                    .html(LABEL_STATUS_ACTIVE);
            }
            return false;
        });

        dispInputQuantity.trigger('change');

        if (mode == 'view') {
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        } else {
            btnDone.show();
            btnDel.hide();

            labelQuantity.hide();
            dispInputQuantity.show();

            dispInputQuantity.focus();
        }
    }

    let initialize = (uid) => {
        // INITIALIZE DATATABLE
        TABLE_LOOKUP_OBAT_DT = TABLE_LOOKUP_OBAT.DataTable({
            data: [],
            columns: COLUMNS_OPT_LOOKUP_OBAT,
            fnDrawCallback: function (oSettings) {
                TABLE_LOOKUP_OBAT.find('tbody tr input[type=checkbox]').uniform();
            }
        });

        FORM.find('[name=jenis]').on('change', function () {
            let val = $(this).val();
            if (val == 'kelompok') {
                $("#section-ukuran_foto").hide('slow');
                $("#section-obat").hide('slow');
            } else {
                $("#section-ukuran_foto").show('slow');
                $("#section-obat").show('slow');
            }
        });

        BTN_TAMBAH_OBAT.on('click', (e) => {
            MODAL_LOOKUP_OBAT.modal('show');
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    tr.find('input[type=checkbox]').prop('checked', false);
                    // tr.find('input[type=checkbox]').uniform();
                });
                TABLE_LOOKUP_OBAT_DT.draw(false);
                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
            }, 500);
        });

        BTN_TAMBAH_LOOKUP_OBAT_SIMPAN.on('click', (e) => {
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    let obat = {
                        id: 0,
                        pemeriksaan_id: 0,
                        obat_id: tr.find('input[type=checkbox]').data('id'),
                        obat_kode: tr.find('input[type=checkbox]').data('kode'),
                        obat: tr.find('input[type=checkbox]').data('nama'),
                        quantity: 1,
                        status: 1,
                    };

                    if (tr.find('input[type=checkbox]').is(':checked')) {
                        let existsTr = false;
                        TABLE_OBAT.find('tbody').find('tr').each((i, el) => {
                            console.log($(el), $(el).data('obat_id'));
                            if ($(el).data('obat_id') == obat.obat_id) {
                                existsTr = $(el);
                            }
                        });

                        if (existsTr) {
                            let dispInputQuantity = existsTr.find('.disp_input_quantity');
                            let quantity = numeral(dispInputQuantity.autoNumeric('get'))._value;
                            dispInputQuantity.autoNumeric('set', quantity+1);
                            dispInputQuantity.trigger('change');

                            let bg = existsTr.find('td').css('background-color');
                            let highlightBg = 'rgba(96,125,139,0.5)';
                            existsTr.find('td')
                                .animate({backgroundColor: highlightBg}, 400)
                                .delay(400)
                                .animate({backgroundColor: bg}, 400);
                        } else {
                            addObat(obat);
                        }
                    }
                });

                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
                MODAL_LOOKUP_OBAT.modal('hide');
            }, 500);
        });

        /**
         * FORM VALIDATION
         */
        (() => {
            /**
             * FORM
             */
            FORM.validate({
                rules: {
                    kode: { required: true },
                    nama: { required: true },
                    jenis: { required: true },
                    parent_id: { required: true },
                },
                messages: {
                    kode: "Kode Diperlukan.",
                    nama: "Nama Diperlukan.",
                    jenis: "Jenis Diperlukan.",
                    parent_id: "Parent Diperlukan.",
                },
                focusInvalid: true,
                errorPlacement: function(error, element) {
                    var inputGroup = $(element).closest('.input-group');
                    var checkbox = $(element).closest('.checkbox-inline');

                    if (inputGroup.length) {
                        error.insertAfter(inputGroup);
                    } else if (checkbox.length) {
                        checkbox.append(error);
                    } else {
                        $(element).closest("div").append(error);
                    }
                },
                submitHandler: function (form) {
                    blockPage();
                    $("#deskripsi").val($("#disp_deskripsi").summernote('code'));
                    var postData = $(form).serializeArray();
                    var formData = new FormData($(form)[0]);

                    for (var i = 0; i < postData.length; i++) {
                        if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                            formData.delete(postData[i].name);
                            formData.append(postData[i].name, postData[i].value);
                        }
                    }

                    $.ajax({
                        url: URL.save,
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (result) {
                            result = JSON.parse(result);
                            var data = result.data;
                            console.log(data);
                            successMessage('Success', "Pemeriksaan berhasil disimpan.");

                            $("#btn-simpan").hide();

                            setTimeout(() => {
                                window.location.assign(URL.index);
                            }, 3000);
                        },
                        error: function () {
                            $.unblockUI();
                            errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan pemeriksaan.");
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                }
            });
        })();

        /**
         * BUTTON
         */
        BTN_DELETE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Hapus Data Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: URL.delete,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Dihapus.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Dihapus.');
                },
            });
        });

        BTN_RESTORE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Restore Data Tersebut?',
                text: '',
                btn_confirm: 'Restore',
                url: URL.restore,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Direstore.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Direstore.');
                },
            });
        });

        BTN_CANCEL.on('click', (e) => {
            window.location.assign(URL.index);
        });

        if (uid) {
            fillForm(uid);
        } else {
            fetchParent(0);
            fetchUkuranFoto([]);
            BTN_DELETE.hide();
            BTN_RESTORE.hide();
            LABEL_DELETED.hide();
        }

        fetchLookupObat();
    }


    initialize(UID);
});