$(() => {
    const COLUMN_DEF = [
        {
            "orderable": true,
            "data": "type",
            "render": (data, type, row, meta) => {
                return data;
            },
            "className": "text-left"
        },
        {
            "orderable": true,
            "data": 'nama',
            "render": (data, type, row, meta) => {
                // TODO: MORE SPECIFIC
                let disp = data;
                if (data.length > 30) {
                    disp = data.substring(0, 30).trim() + "...";
                }

                return `
                    <span class="small text-bold text-muted">Kode: ${row.kode}</span>
                    <br/>
                    <u data-popup="tooltip" title="${data}">${disp}</u>
                `;
            },
            "className": "text-left"
        },
        {
            "orderable": false,
            "data": "uid",
            "render": (data, type, row, meta) => {
                let btnDelete = `
                    <button type="button" class="btn btn-danger btn-xs" data-action="delete" data-uid="${data}">
                        <i class="icon-trash"></i>
                    </button>
                `;

                return btnDelete;
            }
        }
    ];

    const COLUMN_DEF_LOOKUP = [
        {
            "orderable": false,
            "data": "",
            "render": (data, type, row, meta) => {
                let dataAttr = `data-id="${row.id}"`;
                let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

                return checkbox;
            },
            "className": "text-center"
        },
        {
            "orderable": true,
            "data": "kode",
            "render": (data, type, row, meta) => {
                return data;
            },
            "className": "text-left"
        },
        {
            "orderable": true,
            "data": "nama",
            "render": (data, type, row, meta) => {
                return data;
            },
            "className": "text-left"
        },
    ];

    let COLS = [];

    let fetchGrouper = (cb) => {
        $.getJSON(URL.fetchGrouper, (res, status) => {
            console.log('fetchGrouper', res);
            if (cb) {
                cb(res);
            }
        });
    }

    let fetchData = (grouper, cb) => {
        $.getJSON(URL.fetchData.replace(':GROUPER', grouper), (res, status) => {
            console.log('fetchData', res);
            if (cb) {
                cb(res);
            }
        })
    }

    let addGrouper = (data) => {
        let col = $("<div/>")
            .data('grouper', data.grouper)
            .addClass('col-md-6 mb-20 grouper');

        let panel = $("<div/>")
            .addClass('panel panel-white')
            .appendTo(col);
        let panelHeading = $("<div/>")
            .addClass('panel-heading')
            .appendTo(panel);
        let panelBody = $("<div/>")
            .addClass('panel-body')
            .appendTo(panel);

        // Panel Heading
        panelHeading.html(`
            <h5 class="panel-title">Grouper: ${data.grouper_title}</h5>
            <div class="heading-elements" style="right: 20px;">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        `);

        // Panel Body
        let divTable = $("<div/>")
            .addClass('table-responsive')
            .appendTo(panelBody);
        let table = $("<table/>")
            .addClass('table table-bordered table-striped table-hover')
            .appendTo(divTable);
        let tableHead = $("<thead/>")
            .html(`
                <tr>
                    <th class="text-center">Type</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">&nbsp;</th>
                </tr>
            `)
            .appendTo(table);
        let tableBody = $("<tbody/>")
            .appendTo(table);
        let tableFoot = $("<tfoot/>")
            .appendTo(table);


        // TFOOT
        let trFoot1 = $("<tr/>")
            .appendTo(tableFoot);
        let tdFoot1 = $("<td/>")
            .prop('colspan', 3)
            .appendTo(trFoot1);
        let btnTambahTarif = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-primary btn-labeled btn-xs btn-block')
            .html('<b><i class="icon-plus-circle2"></i></b> Tarif Tindakan')
            .appendTo(tdFoot1);

        let trFoot2 = $("<tr/>")
            .appendTo(tableFoot);
        let tdFoot2 = $("<td/>")
            .prop('colspan', 3)
            .appendTo(trFoot2);
        let btnTambahKategori = $("<button/>")
            .prop('type', 'button')
            .addClass('btn bg-purple btn-labeled btn-xs btn-block')
            .html('<b><i class="icon-plus-circle2"></i></b> Kategori Obat')
            .appendTo(tdFoot2);


        let tableDt = table.DataTable({
            data: [],
            columns: COLUMN_DEF,
            "fnDrawCallback": function (oSettings) {
                table.find('tbody tr input[type=checkbox]').uniform();
            }
        });

        table.data('tableDt', tableDt);

        col.on('fetch', () => {
            let grouper = col.data('grouper');
            fetchData(grouper, (res) => {
                let data = res.data;

                tableDt.clear().draw();
                tableDt.rows.add(data).draw();
            });
        });

        col.on('click', '[data-action="reload"]', () => {
            col.trigger('fetch');
        });

        btnTambahTarif.on('click', () => {
            let grouper = col.data('grouper');

            blockElement(MODAL_LOOKUP_TARIF.find('.modal-dialog').selector);
            MODAL_LOOKUP_TARIF.find('[name=grouper]').val(grouper);
            MODAL_LOOKUP_TARIF.modal('show');

            TABLE_LOOKUP_TARIF_DT.rows().nodes().each((i, rowIdx) => {
                let tr = $(TABLE_LOOKUP_TARIF_DT.row(rowIdx).node());
                tr.find('input[type=checkbox]').prop('checked', false);
            });
            TABLE_LOOKUP_TARIF_DT.draw();

            MODAL_LOOKUP_TARIF.find('.modal-dialog').unblock();
        });

        btnTambahKategori.on('click', () => {
            let grouper = col.data('grouper');

            blockElement(MODAL_LOOKUP_TARIF.find('.modal-dialog').selector);
            MODAL_LOOKUP_KATEGORI.find('[name=grouper]').val(grouper);
            MODAL_LOOKUP_KATEGORI.modal('show');

            TABLE_LOOKUP_KATEGORI_DT.rows().nodes().each((i, rowIdx) => {
                let tr = $(TABLE_LOOKUP_KATEGORI_DT.row(rowIdx).node());
                tr.find('input[type=checkbox]').prop('checked', false);
            });
            TABLE_LOOKUP_KATEGORI_DT.draw();

            MODAL_LOOKUP_KATEGORI.find('.modal-dialog').unblock();
        });


        col.trigger('fetch');

        COLS.push(col);
    }

    let initializeLookup = () => {
        TABLE_LOOKUP_TARIF_DT = TABLE_LOOKUP_TARIF.DataTable({
            data: [],
            columns: COLUMN_DEF_LOOKUP,
            "fnDrawCallback": function (oSettings) {
                TABLE_LOOKUP_TARIF.find('tbody tr input[type=checkbox]').uniform();
            }
        });

        TABLE_LOOKUP_KATEGORI_DT = TABLE_LOOKUP_KATEGORI.DataTable({
            data: [],
            columns: COLUMN_DEF_LOOKUP,
            "fnDrawCallback": function (oSettings) {
                TABLE_LOOKUP_KATEGORI.find('tbody tr input[type=checkbox]').uniform();
            }
        });

        TABLE_LOOKUP_TARIF.on('fetch', () => {
            $.getJSON(URL.fetchTarif, (res, status) => {
                if (status === 'success') {
                    let data = res.data;

                    TABLE_LOOKUP_TARIF_DT.clear().draw();
                    TABLE_LOOKUP_TARIF_DT.rows.add(data).draw();
                }
            });
        });
        TABLE_LOOKUP_KATEGORI.on('fetch', () => {
            $.getJSON(URL.fetchKategori, (res, status) => {
                if (status === 'success') {
                    let data = res.data;

                    TABLE_LOOKUP_KATEGORI_DT.clear().draw();
                    TABLE_LOOKUP_KATEGORI_DT.rows.add(data).draw();
                }
            });
        });

        // BUTTON HANDLER
        BTN_TAMBAH_LOOKUP_TARIF.on('click', () => {
            blockElement(MODAL_LOOKUP_TARIF.find('.modal-dialog').selector);
            let grouper = MODAL_LOOKUP_TARIF.find('[name="grouper"]').val();
            let formData = new FormData();
            TABLE_LOOKUP_TARIF_DT.rows().nodes().each((i, rowIdx) => {
                let tr = $(TABLE_LOOKUP_TARIF_DT.row(rowIdx).node());
                
                if (tr.find('input[type=checkbox]').is(':checked')) {
                    formData.append('grouper[]', grouper);
                    formData.append('tarif_pelayanan_id[]', tr.find('input[type=checkbox]').data('id'));
                }
            });
            
            $.ajax({
                url: URL.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    MODAL_LOOKUP_TARIF.find('.modal-dialog').unblock();
                    successMessage('Success', "Tarif Pelayanan Berhasil Ditambahkan.");

                    CONTENT.find('.grouper').each((i, el) => {
                        if ($(el).data('grouper') == grouper) {
                            $(el).trigger('fetch');
                            MODAL_LOOKUP_TARIF.modal('hide');
                        }
                    });
                },
                error: function (error) {
                    MODAL_LOOKUP_TARIF.find('.modal-dialog').unblock();
                    console.log(error);

                    errorMessage('Error', "Tarif Pelayanan Gagal Ditambahkan.");
                },
                complete: function () {
                    MODAL_LOOKUP_TARIF.find('.modal-dialog').unblock();
                }
            });
        });

        BTN_TAMBAH_LOOKUP_KATEGORI.on('click', () => {
            blockElement(MODAL_LOOKUP_KATEGORI.find('.modal-dialog').selector);
            let grouper = MODAL_LOOKUP_KATEGORI.find('[name="grouper"]').val();
            let formData = new FormData();
            TABLE_LOOKUP_KATEGORI_DT.rows().nodes().each((i, rowIdx) => {
                let tr = $(TABLE_LOOKUP_KATEGORI_DT.row(rowIdx).node());

                if (tr.find('input[type=checkbox]').is(':checked')) {
                    formData.append('grouper[]', grouper);
                    formData.append('kategori_id[]', tr.find('input[type=checkbox]').data('id'));
                }
            });
            
            $.ajax({
                url: URL.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    MODAL_LOOKUP_KATEGORI.find('.modal-dialog').unblock();
                    successMessage('Success', "Kategori Barang Berhasil Ditambahkan.");

                    CONTENT.find('.grouper').each((i, el) => {
                        if ($(el).data('grouper') == grouper) {
                            $(el).trigger('fetch');
                            MODAL_LOOKUP_KATEGORI.modal('hide');
                        }
                    });
                },
                error: function (error) {
                    MODAL_LOOKUP_KATEGORI.find('.modal-dialog').unblock();
                    console.log(error);

                    errorMessage('Error', "Kategori Barang Gagal Ditambahkan.");
                },
                complete: function () {
                    MODAL_LOOKUP_KATEGORI.find('.modal-dialog').unblock();
                }
            });
        });

        TABLE_LOOKUP_TARIF.trigger('fetch');
        TABLE_LOOKUP_KATEGORI.trigger('fetch');
    }

    let initialize = () => {
        fetchGrouper((res) => {
            CONTENT.empty();
            let groupers = res.data;

            for (let grouper of groupers) {
                addGrouper(grouper);
            }

            let row = $("<div/>")
                .addClass('row')
                .appendTo(CONTENT);
            console.log(COLS);
            for (let col of COLS) {
                if (row.children().length == 2) {
                    row = $("<div/>")
                        .addClass('row')
                        .appendTo(CONTENT);
                }

                row.append(col);
            }
        });

        initializeLookup();
    }

    initialize();
});