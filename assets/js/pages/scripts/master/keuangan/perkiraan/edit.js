var Edit = function () {
	
	var lastLineNo = 0;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillJenisPerkiraan = function(jenisPerkiraan, elementId) {
        $.getJSON(url_get_jenis_perkiraan, function(data, status) {
            if (status === 'success') {
                var optionJenisPerkiraan = '';
                var selected = parseInt(jenisPerkiraan) == 0 ? ' selected="selected"' : '';
                optionJenisPerkiraan += '<option value="0"' + selected + '>[ Pilih Jenis Perkiraan: ]</option>';
				var keysJenisPerkiraan = Object.keys(data.jenis_perkiraan_list);
				keysJenisPerkiraan.forEach(function(keyJenisPerkiraan) {
					selected = parseInt(jenisPerkiraan) == parseInt(keyJenisPerkiraan) ? ' selected="selected"' : '';
					optionJenisPerkiraan += '<option value="' + keyJenisPerkiraan + '"' + selected + '>' + data.jenis_perkiraan_list[keyJenisPerkiraan] + '</option>';
				});
                $(elementId).html(optionJenisPerkiraan);
            }
        });
    };
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };
	
	var formHandle = function() {
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$(".switch").bootstrapSwitch();
		
		lastLineNo = $('#sub_rincian_footer').prevAll().length;
		
		$('#kode').on('focus', function() {
			$(this).select();
		});
		
		$('#nama').on('focus', function() {
			$(this).select();
		});
		
		$('#jenis_perkiraan').on('change', function() {
			switch (parseInt($(this).val())) {
				case imediscode.JENIS_PERKIRAAN_AKTIVA:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_LANCAR:
				case imediscode.JENIS_PERKIRAAN_KAS_KECIL:
				case imediscode.JENIS_PERKIRAAN_KAS:
				case imediscode.JENIS_PERKIRAAN_BANK:
				case imediscode.JENIS_PERKIRAAN_PIUTANG_DAGANG:
				case imediscode.JENIS_PERKIRAAN_PERSEDIAAN:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_TETAP:
				case imediscode.JENIS_PERKIRAAN_PEROLEHAN:
				case imediscode.JENIS_PERKIRAAN_PENYUSUTAN:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_LAIN_LAIN:
					$('#kategori').val(imediscode.KATEGORI_PERKIRAAN_AKTIVA);
					break;
				case imediscode.JENIS_PERKIRAAN_UTANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_DAGANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_LANCAR:
				case imediscode.JENIS_PERKIRAAN_UTANG_JANGKA_PANJANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_LAIN_LAIN:
					$('#kategori').val(imediscode.KATEGORI_PERKIRAAN_UTANG);
					break;
				case imediscode.JENIS_PERKIRAAN_MODAL:
					$('#kategori').val(KATEGORI_PERKIRAAN_MODAL);
					break;
				case imediscode.JENIS_PERKIRAAN_PENDAPATAN:
					$('#kategori').val(imediscode.KATEGORI_PERKIRAAN_PENDAPATAN);
					break;
				case imediscode.JENIS_PERKIRAAN_BIAYA:
				case imediscode.JENIS_PERKIRAAN_HARGA_POKOK_PENJUALAN:
				case imediscode.JENIS_PERKIRAAN_BIAYA_ADMINISTRASI_DAN_UMUM:
				case imediscode.JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN:
				case imediscode.JENIS_PERKIRAAN_BIAYA_LAIN_LAIN:
				case imediscode.JENIS_PERKIRAAN_PAJAK:
					$('#kategori').val(imediscode.KATEGORI_PERKIRAAN_BIAYA);
					break;
				case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
				case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
					$('#kategori').val(imediscode.KATEGORI_PERKIRAAN_SEMENTARA);
					break;
			}
			
			switch (parseInt($('#kategori').val())) {
				case imediscode.KATEGORI_PERKIRAAN_AKTIVA:
				case imediscode.KATEGORI_PERKIRAAN_UTANG:
				case imediscode.KATEGORI_PERKIRAAN_MODAL:
					$('#jenis_laporan').val(imediscode.JENIS_LAPORAN_NERACA);
					break;
				case imediscode.KATEGORI_PERKIRAAN_PENDAPATAN:
				case imediscode.KATEGORI_PERKIRAAN_BIAYA:
					$('#jenis_laporan').val(imediscode.JENIS_LAPORAN_LABA_RUGI);
					break;
				case imediscode.KATEGORI_PERKIRAAN_SEMENTARA:
					switch (parseInt($(this).val())) {
						case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
							$('#jenis_laporan').val(imediscode.JENIS_LAPORAN_NERACA);
							break;
						case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
							$('#jenis_laporan').val(imediscode.JENIS_LAPORAN_LABA_RUGI);
							break;
					}
					break;
			}
			
			switch (parseInt($('#kategori').val())) {
				case imediscode.KATEGORI_PERKIRAAN_AKTIVA:
				case imediscode.KATEGORI_PERKIRAAN_BIAYA:
					$('#saldo_normal').val(imediscode.SALDO_NORMAL_DEBIT);
					break;
				case imediscode.KATEGORI_PERKIRAAN_UTANG:
				case imediscode.KATEGORI_PERKIRAAN_MODAL:
				case imediscode.KATEGORI_PERKIRAAN_PENDAPATAN:
					$('#saldo_normal').val(imediscode.SALDO_NORMAL_KREDIT);
					break;
				case imediscode.KATEGORI_PERKIRAAN_SEMENTARA:
					switch (parseInt($(this).val())) {
						case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
						case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
							$('#saldo_normal').val(imediscode.SALDO_NORMAL_KREDIT);
							break;
					}
					break;
			}
		});
		
		$('input[type=radio][name=jenis]').on('change', function() {
			if (parseInt($(this).val()) === 1) {
				$('#sub_rincian_section').hide();
			}
			else if (parseInt($(this).val()) === 2) {
				$('#sub_rincian_section').show();
			}
		});
		
		$('#parent_id').on('change', function() {
			var path = $(this).find('option:selected').data('path');
			var depth = $(this).find('option:selected').data('depth');
			$('#parent_path').val(path);
			$('#parent_depth').val(depth);
		});
		
		$('#rekening_kontrol').on('switchChange.bootstrapSwitch', function (event, state) {
			if (state) {
				$('#select_rekening_kontrol_section').show();
			}
			else {
				$('#select_rekening_kontrol_section').hide();
			}
		}); 
		
		$('#tambah_button').on('click', function() {
			lastLineNo++;
			var lineNo = lastLineNo;

            var templateString = $('#sub-rincian-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#sub_rincian_footer').before(newString);
            var $tr = $('#sub_rincian_footer').prev('tr');
			$tr.data('line_no', lineNo);
			
			fillJenisPerkiraan(0, '#sub_rincian_jenis_perkiraan_' + lineNo);
		});
		
		$('#sub_rincian_table').on('click', '.hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
		});
		
		$('#sub_rincian_table').on('click', '.sub-rincian-jenis-perkiraan-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			switch (parseInt($(this).val())) {
				case imediscode.JENIS_PERKIRAAN_AKTIVA:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_LANCAR:
				case imediscode.JENIS_PERKIRAAN_KAS_KECIL:
				case imediscode.JENIS_PERKIRAAN_KAS:
				case imediscode.JENIS_PERKIRAAN_BANK:
				case imediscode.JENIS_PERKIRAAN_PIUTANG_DAGANG:
				case imediscode.JENIS_PERKIRAAN_PERSEDIAAN:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_TETAP:
				case imediscode.JENIS_PERKIRAAN_PEROLEHAN:
				case imediscode.JENIS_PERKIRAAN_PENYUSUTAN:
				case imediscode.JENIS_PERKIRAAN_AKTIVA_LAIN_LAIN:
					$('#sub_rincian_kategori_' + lineNo).val(imediscode.KATEGORI_PERKIRAAN_AKTIVA);
					break;
				case imediscode.JENIS_PERKIRAAN_UTANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_DAGANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_LANCAR:
				case imediscode.JENIS_PERKIRAAN_UTANG_JANGKA_PANJANG:
				case imediscode.JENIS_PERKIRAAN_UTANG_LAIN_LAIN:
					$('#sub_rincian_kategori_' + lineNo).val(imediscode.KATEGORI_PERKIRAAN_UTANG);
					break;
				case imediscode.JENIS_PERKIRAAN_MODAL:
					$('#sub_rincian_kategori_' + lineNo).val(KATEGORI_PERKIRAAN_MODAL);
					break;
				case imediscode.JENIS_PERKIRAAN_PENDAPATAN:
					$('#sub_rincian_kategori_' + lineNo).val(imediscode.KATEGORI_PERKIRAAN_PENDAPATAN);
					break;
				case imediscode.JENIS_PERKIRAAN_BIAYA:
				case imediscode.JENIS_PERKIRAAN_HARGA_POKOK_PENJUALAN:
				case imediscode.JENIS_PERKIRAAN_BIAYA_ADMINISTRASI_DAN_UMUM:
				case imediscode.JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN:
				case imediscode.JENIS_PERKIRAAN_BIAYA_LAIN_LAIN:
				case imediscode.JENIS_PERKIRAAN_PAJAK:
					$('#sub_rincian_kategori_' + lineNo).val(imediscode.KATEGORI_PERKIRAAN_BIAYA);
					break;
				case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
				case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
					$('#sub_rincian_kategori_' + lineNo).val(imediscode.KATEGORI_PERKIRAAN_SEMENTARA);
					break;
			}
			
			switch (parseInt($('#sub_rincian_kategori_' + lineNo).val())) {
				case imediscode.KATEGORI_PERKIRAAN_AKTIVA:
				case imediscode.KATEGORI_PERKIRAAN_UTANG:
				case imediscode.KATEGORI_PERKIRAAN_MODAL:
					$('#sub_rincian_jenis_laporan_' + lineNo).val(imediscode.JENIS_LAPORAN_NERACA);
					break;
				case imediscode.KATEGORI_PERKIRAAN_PENDAPATAN:
				case imediscode.KATEGORI_PERKIRAAN_BIAYA:
					$('#sub_rincian_jenis_laporan_' + lineNo).val(imediscode.JENIS_LAPORAN_LABA_RUGI);
					break;
				case imediscode.KATEGORI_PERKIRAAN_SEMENTARA:
					switch (parseInt($(this).val())) {
						case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
							$('#sub_rincian_jenis_laporan_' + lineNo).val(imediscode.JENIS_LAPORAN_NERACA);
							break;
						case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
							$('#sub_rincian_jenis_laporan_' + lineNo).val(imediscode.JENIS_LAPORAN_LABA_RUGI);
							break;
					}
					break;
			}
			
			switch (parseInt($('#sub_rincian_kategori_' + lineNo).val())) {
				case imediscode.KATEGORI_PERKIRAAN_AKTIVA:
				case imediscode.KATEGORI_PERKIRAAN_BIAYA:
					$('#sub_rincian_saldo_normal_' + lineNo).val(imediscode.SALDO_NORMAL_DEBIT);
					break;
				case imediscode.KATEGORI_PERKIRAAN_UTANG:
				case imediscode.KATEGORI_PERKIRAAN_MODAL:
				case imediscode.KATEGORI_PERKIRAAN_PENDAPATAN:
					$('#sub_rincian_saldo_normal_' + lineNo).val(imediscode.SALDO_NORMAL_KREDIT);
					break;
				case imediscode.KATEGORI_PERKIRAAN_SEMENTARA:
					switch (parseInt($(this).val())) {
						case imediscode.JENIS_PERKIRAAN_LABA_RUGI_BERJALAN:
						case imediscode.JENIS_PERKIRAAN_IKHTISAR_LABA_RUGI:
							$('#sub_rincian_saldo_normal_' + lineNo).val(imediscode.SALDO_NORMAL_KREDIT);
							break;
					}
					break;
			}
		});
		
		var perkiraanApp = {
			initPerkiraanForm: function () {
				$("#perkiraan_form").validate({
					rules: {
						'kode': {'required': true, 'minlength': 1},
						'nama': {'required': true, 'minlength': 1},
						'jenis_perkiraan': {'required': true, 'min': 1}
					},
					messages: {
						'kode': "Kode diperlukan",
						'nama': "Nama diperlukan",
						'jenis_perkiraan': "Jenis Perkiraan diperlukan!"
					},
					submitHandler: function(form) {
						perkiraanApp.addPerkiraan($(form));
					}
				});
			},
			addPerkiraan: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		perkiraanApp.initPerkiraanForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index + '?start=' + start + '&length=' + length + '&rekap_rincian=' + rekapRincian + '&search=' + search;
		});
		
		$('#kode').focus();
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index + '?start=' + start + '&length=' + length + '&rekap_rincian=' + rekapRincian + '&search=' + search;
							break;
					};
				}
            });
			
        }

    };

}();