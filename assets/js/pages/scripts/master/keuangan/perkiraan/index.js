var Index = function () {
	
	var oDaftarPerkiraanTable = null;
	var oTableInfo;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPerkiraanTable = function() {

		oDaftarPerkiraanTable = $('#perkiraan_table').DataTable({
			"displayStart": start,
            "pageLength": length,
            "search": {
                "search": search
            },
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data": function (d) {
					d.rekap_rincian = $('#filter_rekap_rincian').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "kode", "name": "kode"},
				{"data": "nama", "name": "nama"},
				{"data": "status", "name": "status", "orderable": false, "searchable": false, "render": function (data, type, row, meta) {
					if (data == 0) 
						return '<span class="label label-success">Aktif</span>';
					else
						return '<span class="label label-danger">Tidak Aktif</span>';
				}, "className": "text-center"},
				{"data": "uid",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<ul class="icons-list">';
						btnList += '	<li><a class="edit-row text-primary" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Edit"><i class="fa fa-edit"></i></a></li>';
						if (row.deleted) {
							btnList += '	<li><a class="delete-row text-danger" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Hapus"><i class="fa fa-trash"></i></a></li>';
						}
						btnList += '	<li><a class="text-primary" data-popup="popover" title="Popover title" data-trigger="hover" data-content="And here some amazing content. It very engaging. Right?"><i class="fa fa-info-circle"></i></a></li>';
						btnList += '</ul>';
						return btnList;
					},
					"className": "text-center"
				}
			],
			"createdRow": function( row, data, dataIndex ) {
				var indent = '';
				var lvl = parseInt(data.lvl) - 1;
				var nama = '';
				for (var i = 0; i < lvl; i++) {
					indent += '<span class="gi">|&mdash;</span>';
				}
				nama = indent + data.nama;
				
				if (parseInt(data.jenis) === 1) {
					$(row).find('td:eq(0)').html('<strong>' + data.kode + '</strong>');
					$(row).find('td:eq(1)').html('<strong>' + nama + '</strong>');
				}
				else {
					$(row).find('td:eq(1)').html(nama);
				}
			}
		});
			
    };
	
	var reloadDaftarPerkiraan = function() {
        if (oDaftarPerkiraanTable == null) {
			handleDaftarPerkiraanTable();
        }
		else {
			oDaftarPerkiraanTable.ajax.reload(null, false);
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarPerkiraan();
			$('[data-popup=popover]').popover();
			
			$('#tambah_button').on('click', function(event) {
				event.preventDefault();
				window.location = url_edit + '?uid=&start=' + oTableInfo.start + '&length=' + oTableInfo.length + '&rekap_rincian=' + $('#filter_rekap_rincian').val() + '&search=' + oDaftarPerkiraanTable.search();
			});
			
			$('#filter_button').on('click', function() {
				reloadDaftarPerkiraan();
			});
			
			$('#perkiraan_table').on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid + '&start=' + oTableInfo.start + '&length=' + oTableInfo.length + '&rekap_rincian=' + $('#filter_rekap_rincian').val() + '&search=' + oDaftarPerkiraanTable.search();
			});
			
			$("#perkiraan_table").on("click", ".delete-row", function () {
				var uid = $(this).data('uid');
				var hapus = false;
				swal({
					title: "Anda yakin untuk menghapus data tersebut?",
					type: "warning",
					showCancelButton: true,
					closeOnConfirm: false,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "OK",
					cancelButtonText: "Batal",
					showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.getJSON(url_delete + '?uid=' + uid, function(data, status) {
							if (status === 'success') {
								reloadDaftarPerkiraan();
								swal({
									title: "Data telah dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
							else {
								swal({
									title: "Data gagal dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
						});
					}
				});
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarPerkiraan();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data':
							oTableInfo = oDaftarPerkiraanTable.page.info();
							break;
					}
				}
            });
			
        }

    };

}();