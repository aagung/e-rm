var Edit = function () {
	var lastLineNo = 1;
	var aBedList = {};

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillKelas = function(kelasId, elementId) {
		//$('#kabupaten_spinner').show();
        $.getJSON(url_get_kelas_all, function(data, status) {
            if (status === 'success') {
                var optionKelas = '';
                var selected = parseInt(kelasId) == 0 ? ' selected="selected"' : '';
                optionKelas += '<option value="0"' + selected + '>[ Pilih Kelas: ]</option>';
                for (var i = 0; i < data.kelas_list.length; i++) {
                    selected = parseInt(kelasId) == parseInt(data.kelas_list[i].id) ? ' selected="selected"' : '';
                    optionKelas += '<option value="' + data.kelas_list[i].id + '"' + selected + '>' + data.kelas_list[i].nama + '</option>';
                }
                $(elementId).html(optionKelas);
                //$('#kabupaten_id').val(kabupatenId).trigger('change.select2');
				//$('#kabupaten_spinner').hide();
            }
        });
    };
	
	var fillStatusBed = function(statusBed, elementId) {
		//$('#kabupaten_spinner').show();
        $.getJSON(url_get_status_bed, function(data, status) {
            if (status === 'success') {
                var optionStatusBed = '';
                var selected = parseInt(statusBed) == 0 ? ' selected="selected"' : '';
                optionStatusBed += '<option value="0"' + selected + '>[ Pilih Status Bed: ]</option>';
				Object.keys(data.status_bed_list).forEach(function(key) {
					selected = parseInt(statusBed) == parseInt(key) ? ' selected="selected"' : '';
					optionStatusBed += '<option value="' + key + '"' + selected + '>' + data.status_bed_list[key] + '</option>';
				});
				
                $(elementId).html(optionStatusBed);
                //$('#kabupaten_id').val(kabupatenId).trigger('change.select2');
				//$('#kabupaten_spinner').hide();
            }
        });
    };
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };
	
	var formHandle = function() {
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$('#jenis').on('change', function() {
			switch (parseInt($(this).val())) {
				case imediscode.JENIS_RUANG_RAWAT_INAP:
					$('#bed_table_section').show();
					break;
				case imediscode.JENIS_RUANG_OPERASI:
					$('#bed_table_section').hide();
					break;
			}
		});
		
		lastLineNo = $('#ruang_footer_section').prevAll().length;
		
		$('#tambah_bed_button').on('click', function() {
			lastLineNo++;
			var lineNo = lastLineNo;

            var templateString = $('#ruang-bed-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#ruang_footer_section').before(newString);
            var $tr = $('#ruang_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#ruang_status_' + lineNo).uniform();
			
			fillKelas(0, '#ruang_kelas_id_' + lineNo);
			fillStatusBed(0, '#ruang_status_bed_' + lineNo);
			
			var rowCount = $('#bed_table tbody tr').length;
		});
		
		$('#bed_table').on('click', '.ruang-bed-hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
			
			var rowCount = $('#bed_table tbody tr').length;
		});
		
		var ruangApp = {
			initRuangForm: function () {
				$("#ruang_form").validate({
					rules: {
						'kode': {'required': true, 'minlength': 1},
						'nama': {'required': true, 'minlength': 1},
						'spesialisasi': {'required': true, 'minlength': 1}
					},
					messages: {
						'kode': "Kode Diperlukan",
						'nama': "Nama Diperlukan",
						'spesialisasi': "Spesialisasi Diperlukan"
					},
					submitHandler: function(form) {
						ruangApp.addRuang($(form));
					}
				});
			},
			addRuang: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		ruangApp.initRuangForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index;
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							//window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();