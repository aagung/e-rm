var Index = function () {
	
	var oDaftarRuangTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarRuangTable = function() {

		oDaftarRuangTable = $('#ruang_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data": function (d) {
					d.jenis_ruang = $('#filter_jenis_ruang').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "kode", "name": "kode", "width": "20%"},
				{"data": "nama", "name": "nama"},
		      	{ 
			        "data": "status",
			        "orderable": false,
			        "searchable": false,
			        "render": function (data, type, row, meta) {
			          if (data == 0) 
			            return '<button type="button" class="toggle-status-row btn bg-success btn-xs" data-status="1" data-uid="' + row.uid + '" title="Aktifkan Data">Aktif</button>';
			          return '<button type="button" class="toggle-status-row btn bg-slate-400 btn-xs" data-status="0" data-uid="' + row.uid + '" title="Non Aktifkan Data">Non Aktif</button>';
		        }, "className": "text-center"},
				{"data": "uid",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<ul class="icons-list">';
						btnList += '	<li><a class="edit-row text-primary" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Edit"><i class="fa fa-edit"></i></a></li>';
						if (row.deleted) {
							btnList += '	<li><a class="delete-row text-danger" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Hapus"><i class="fa fa-trash"></i></a></li>';
						}
						btnList += '</ul>';
						return btnList;
					},
					"className": "text-center"
				}
			]
		});
			
    };
	
	var reloadDaftarRuang = function() {
        if (oDaftarRuangTable == null) {
			handleDaftarRuangTable();
        }
		else {
			oDaftarRuangTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarRuang();
			
			$('#filter_jenis_ruang').on('change', function() {
				reloadDaftarRuang();
			});
			
			$('#ruang_table').on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});

			$('#ruang_table').on("click", ".delete-row", function () {
				var uid = $(this).data('uid');
				var hapus = false;
				swal({
					title: "Anda yakin untuk menghapus data tersebut?",
					type: "warning",
					showCancelButton: true,
					closeOnConfirm: false,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "OK",
					cancelButtonText: "Batal",
					showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.getJSON(url_delete + '?uid=' + uid, function(data, status) {
							if (status === 'success') {
								reloadDaftarRuang();
								swal({
									title: "Data telah dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
							else {
								swal({
									title: "Data gagal dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
						});
					}
				});
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarRuang();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();