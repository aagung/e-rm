var Edit = function () {
	
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillKabupaten = function(provinsiId, kabupatenId) {
		$('#kabupaten_spinner').show();
        $.getJSON(url_get_kabupaten_by_provinsi + '?provinsi_id=' + provinsiId, function(data, status) {
            if (status === 'success') {
                var optionKabupaten = '';
                var selected = parseInt(kabupatenId) == 0 ? ' selected="selected"' : '';
                optionKabupaten += '<option value="0"' + selected + '>[ Kabupaten/Kota: ]</option>';
                for (var i = 0; i < data.kabupaten_list.length; i++) {
                    selected = parseInt(kabupatenId) == parseInt(data.kabupaten_list[i].id) ? ' selected="selected"' : '';
                    optionKabupaten += '<option value="' + data.kabupaten_list[i].id + '"' + selected + '>' + data.kabupaten_list[i].nama + '</option>';
                }
                $('#kabupaten_id').html(optionKabupaten);
                $('#kabupaten_id').val(kabupatenId).trigger('change.select2');
				$('#kabupaten_spinner').hide();
            }
        });
    };

    var fillKecamatan = function(kabupatenId, kecamatanId) {
		$('#kecamatan_spinner').show();
        $.getJSON(url_get_kecamatan_by_kabupaten + '?kabupaten_id=' + kabupatenId, function(data, status) {
            if (status === 'success') {
                var optionKecamatan = '';
                var selected = parseInt(kecamatanId) == 0 ? ' selected="selected"' : '';
                optionKecamatan += '<option value="0"' + selected + '>[ Kecamatan: ]</option>';
                for (var i = 0; i < data.kecamatan_list.length; i++) {
                    selected = parseInt(kecamatanId) == parseInt(data.kecamatan_list[i].id) ? ' selected="selected"' : '';
                    optionKecamatan += '<option value="' + data.kecamatan_list[i].id + '"' + selected + '>' + data.kecamatan_list[i].nama + '</option>';
                }
                $('#kecamatan_id').html(optionKecamatan);
                $('#kecamatan_id').val(kecamatanId).trigger('change.select2');
				$('#kecamatan_spinner').hide();
            }
        });
    };

    var fillKelurahan = function(kecamatanId, kelurahanId) {
		$('#kelurahan_spinner').show();
        $.getJSON(url_get_kelurahan_by_kecamatan + '?kecamatan_id=' + kecamatanId, function(data, status) {
            if (status === 'success') {
                var optionKelurahan = '';
                var selected = parseInt(kelurahanId) == 0 ? ' selected="selected"' : '';
                optionKelurahan += '<option value="0" data-kodepos=""' + selected + '>[ Kelurahan/Desa: ]</option>';
                for (var i = 0; i < data.kelurahan_list.length; i++) {
                    selected = parseInt(kelurahanId) == parseInt(data.kelurahan_list[i].id) ? ' selected="selected"' : '';
                    optionKelurahan += '<option value="' + data.kelurahan_list[i].id + '" data-kodepos="' + data.kelurahan_list[i].kodepos + '"' + selected + '>' + data.kelurahan_list[i].nama + '</option>';
                }
                $('#kelurahan_id').html(optionKelurahan);
                $('#kelurahan_id').val(kelurahanId).trigger('change.select2');
				$('#kelurahan_spinner').hide();
            }
        });
    };
	
	var calculateAge = function() {
		if ($('#tanggal_lahir').val() === '') {
            $('#tanggal_lahir').val(0);
            $("#umur_tahun").val(0);
            $("#umur_bulan").val(0);
            $("#umur_hari").val(0);
        }
        else {
			var today = moment();
			var dob = moment($('#tanggal_lahir').val());
			
			var years = today.diff(dob, 'year');
			dob.add(years, 'years');
			
			var months = today.diff(dob, 'months');
			dob.add(months, 'months');
			
			var days = today.diff(dob, 'days');

            $('#umur_tahun').val(years);
            $('#umur_bulan').val(months);
            $('#umur_hari').val(days);
        }
	};
	
	var formHandle = function() {
		
		$('.komponen-tarif').autoNumeric('init', numericOptions);
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$('#disp_tanggal_lahir').daterangepicker({
			autoUpdateInput: false,
			autoapply: true,
			singleDatePicker: true, 
			showDropdowns: true,
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal_lahir').val(chosen_date.format('YYYY-MM-DD'));
			calculateAge();
		});
		
		$('#disp_tanggal_lahir').on('apply.daterangepicker', function (ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY'));
		});

		$('#disp_tanggal_lahir').on('cancel.daterangepicker', function (ev, picker) {
			$(this).val('');
		});
		
		$('#title_id').on('change', function() {
			var jenisKelamin = $('#title_id option:selected').attr('data-jenis_kelamin');
			$('#jenis_kelamin_1').prop('checked', false);
			$('#jenis_kelamin_1').parent().removeClass('checked');
			$('#jenis_kelamin_2').prop('checked', false);
			$('#jenis_kelamin_2').parent().removeClass('checked');
			switch (parseInt(jenisKelamin)) {
				case 1:
					$('#jenis_kelamin_1').prop('checked', true);
					$('#jenis_kelamin_1').parent().addClass('checked');
					break;
				case 2:
					$('#jenis_kelamin_2').prop('checked', true);
					$('#jenis_kelamin_2').parent().addClass('checked');
					break;
			}
		});
		
		$('#kewarganegaraan_1').click(function() {
			$('#provinsi_section').show();
			$('#kabupaten_section').show();
			$('#kecamatan_section').show();
			$('#kelurahan_section').show();
			$('#kodepos_section').show();
		});
		
		$('#kewarganegaraan_2').click(function() {
			$('#provinsi_section').hide();
			$('#kabupaten_section').hide();
			$('#kecamatan_section').hide();
			$('#kelurahan_section').hide();
			$('#kodepos_section').hide();
		});
		
		$('#provinsi_id').on('change', function() {
            var provinsiId = $(this).val();
            fillKabupaten(provinsiId, 0);
            fillKecamatan(0, 0);
            fillKelurahan(0, 0);
			$('#kodepos').val('');
        });
		
		$('#kabupaten_id').on('change', function() {
            var kabupatenId = $(this).val();
            fillKecamatan(kabupatenId, 0);
            fillKelurahan(0, 0);
			$('#kodepos').val('');
        });

        $('#kecamatan_id').on('change', function() {
            var kecamatanId = $(this).val();
            fillKelurahan(kecamatanId, 0);
			$('#kodepos').val('');
        });

        $('#kelurahan_id').on('change', function() {
			var kodepos = $('#kelurahan_id option:selected').attr('data-kodepos');
            $('#kodepos').val(kodepos.trim());
        });
		
		var pasienApp = {
			initPasienForm: function () {
				$('#pasien_form').validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						pasienApp.addPasien($(form));
					}
				});
			},
			addPasien: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		pasienApp.initPasienForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index;
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();