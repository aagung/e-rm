var Index = function () {
	
	var oDaftarPasienTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPasienTable = function() {

		oDaftarPasienTable = $('#pasien_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "jenis_kelamin", "name": "jenis_kelamin", "render": function (data, type, row, meta) {
					switch (parseInt(row.jenis_kelamin)) {
						case imediscode.JENIS_KELAMIN_LAKI_LAKI:
							jenisKelamin = 'Laki-laki';
							break;
						case imediscode.JENIS_KELAMIN_PEREMPUAN:
							jenisKelamin = 'Perempuan';
							break;
						default:
							jenisKelamin = '';
					}
					return jenisKelamin;
				}},
				{"data": "alamat", "name": "alamat"},
		      	{ 
			        "data": "status",
			        "orderable": false,
			        "searchable": false,
			        "render": function (data, type, row, meta) {
			          if (data == 0) 
			            return '<button type="button" class="toggle-status-row btn bg-success btn-xs" data-status="1" data-uid="' + row.uid + '" title="Aktifkan Data">Aktif</button>';
			          return '<button type="button" class="toggle-status-row btn bg-slate-400 btn-xs" data-status="0" data-uid="' + row.uid + '" title="Non Aktifkan Data">Non Aktif</button>';
		        }, "className": "text-center"},
				{"data": "uid",
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						var btnList = '<ul class="icons-list">';
						btnList += '	<li><a class="edit-row text-primary" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Edit"><i class="fa fa-edit"></i></a></li>';
						if (row.deleted) {
							btnList += '	<li><a class="delete-row text-danger" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Hapus"><i class="fa fa-trash"></i></a></li>';
						}
						btnList += '</ul>';
						return btnList;
					},
					"className": "text-center"
				}
			]
		});
			
    };
	
	var reloadDaftarPasien = function() {
        if (oDaftarPasienTable == null) {
			handleDaftarPasienTable();
        }
		else {
			oDaftarPasienTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarPasien();
			
			$("#pasien_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});
			
			$("#pasien_table").on("click", ".toggle-status-row", function () {
			    var btn = $(this);
			    btn.prop("disabled", true);
			    var uid = $(this).data('uid');
			    var statusData = $(this).data('status');

			    var title;
			    if (parseInt(statusData) === 1) title = "Mengaktifkan Pasien";
			    else title = "Menonaktifkan Pasien";

			    // Progress loader
			    var cur_value = 1;
			    var update = false;
			    var progress;
			    var timer;

			    // Make a loader.
			    var loader = new PNotify({
			      title: title,
			      text: '<div class="progress progress-striped active" style="margin:0">\
			      <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
			      <span class="sr-only">0%</span>\
			      </div>\
			      </div>',
			      addclass: 'bg-slate',
			      icon: 'icon-spinner4 spinner',
			      hide: false,
			      buttons: {
			        closer: true,
			        sticker: false
			      },
			      history: {
			        history: false
			      },
			      before_open: function(PNotify) {
			        progress = PNotify.get().find("div.progress-bar");
			        progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");

			        // Pretend to do something.
			        timer = setInterval(function() {
			          if (cur_value >= 100) {
			            // Remove the interval.
			            window.clearInterval(timer);
			            
			            update = true;
			            loader.remove();

			            return;
			          }
			          cur_value += 5;
			          progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
			        }, 65);
			      },
			      after_close: function(PNotify, timer_hide) {
			        btn.prop("disabled", false);
			        clearInterval(timer);
			        
			        if (update) {
			          $.post(url_edit_status, {uid: uid, status: statusData}, function (data, status) {
			            if (status === "success") {

			              if (parseInt(statusData) === 0) {
			                btn.removeClass('bg-success');
			                btn.addClass('bg-slate-400');
			                btn.data('status', 0);
			                btn.html("Aktif");
			                successMessage('Berhasil', 'Pasien berhasil dinonaktifkan.');
			              } else {
			                btn.removeClass('bg-slate-400');
			                btn.addClass('bg-success');
			                btn.data('status', 1);
			                btn.html("Active");
			                successMessage('Berhasil', 'Pasien berhasil diaktifkan.');
			              }
			              oDaftarPasienTable.draw();
			            }
			          }).fail(function (error) {
			            if (parseInt(statusData) === 0) 
			              errorMessage('Peringatan', 'Terjadi kesalahan saat menonaktifkan Pasien.');
			            else 
			              errorMessage('Peringatan', 'Terjadi kesalahan saat mengaktifkan Pasien.');

			          });
			        }

			        update = false;
			        
			      }
			    });
			});

			$("#pasien_table").on("click", ".delete-row", function () {
				var uid = $(this).data('uid');
				var hapus = false;
				swal({
					title: "Anda yakin untuk menghapus data tersebut?",
					type: "warning",
					showCancelButton: true,
					closeOnConfirm: false,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "OK",
					cancelButtonText: "Batal",
					showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.getJSON(url_delete + '?uid=' + uid, function(data, status) {
							if (status === 'success') {
								reloadDaftarPasien();
								swal({
									title: "Data telah dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
							else {
								swal({
									title: "Data gagal dihapus!",
									confirmButtonColor: "#2196F3"
								});
							}
						});
					}
				});
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarPasien();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();