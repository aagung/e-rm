var Edit = function () {
	
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}
	var aTarif = [];

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillPerusahaan = function(perusahaanId, jenis, elementId, elementSpinner) {
		$(elementSpinner).show();
        $.getJSON(url_get_perusahaan_by_jenis + '?jenis=' + jenis, function(data, status) {
            if (status === 'success') {
				var title = '';
				switch (parseInt(jenis)) {
					case imediscode.CARA_BAYAR_ASURANSI:
						title = 'Asuransi';
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						title = 'Perusahaan';
						break;
				}
                var optionPerusahaan = '';
                var selected = parseInt(perusahaanId) == 0 ? ' selected="selected"' : '';
                optionPerusahaan += '<option value="0"' + selected + '>[ ' + title + ': ]</option>';
                for (var i = 0; i < data.perusahaan_list.length; i++) {
                    selected = parseInt(perusahaanId) == parseInt(data.perusahaan_list[i].id) ? ' selected="selected"' : '';
                    optionPerusahaan += '<option value="' + data.perusahaan_list[i].id + '"' + selected + '>' + data.perusahaan_list[i].nama + '</option>';
                }
                $(elementId).html(optionPerusahaan);
                //$(elementId).val(perusahaanId).trigger('change.select2');
				$(elementSpinner).hide();
            }
        });
    };
	
	var displayKelas = function() {
		$('#disp_kelas_id').html('');
		var optionKelas = '';
		var selected = '';
		var keysKelas = Object.keys(aKelasList);
		keysKelas.forEach(function(keyKelas, index) {
			if (parseInt(aKelasList[keyKelas].check) === 1) {
				selected = index === 0 ? ' selected="selected"' : '';
				optionKelas += '<option value="' + aKelasList[keyKelas].id + '"' + selected + '>' + aKelasList[keyKelas].nama + '</option>';
			}
		});
		$('#disp_kelas_id').html(optionKelas);
	};
	
	var displayGolonganOperasi = function() {
		$('#disp_golongan_operasi').html('');
		var optionOperasi = '';
		var selected = '';
		var keysOperasi = Object.keys(aGolonganOperasiList);
		keysOperasi.forEach(function(keyOperasi, index) {
			if (parseInt(aGolonganOperasiList[keyOperasi].check) === 1) {
				selected = index === 0 ? ' selected="selected"' : '';
				optionOperasi += '<option value="' + aGolonganOperasiList[keyOperasi].id + '"' + selected + '>' + aGolonganOperasiList[keyOperasi].nama + '</option>';
			}
		});
		$('#disp_golongan_operasi').html(optionOperasi);
	};
	
	var displayKomponenTarif = function() {
		$('#komponen_tarif_table tbody').html('');
		var caraBayarId = parseInt($('#cara_bayar_id').val());
		var jenis = parseInt($('#cara_bayar_id option:selected').data('jenis'));
		var perusahaanId = $('#perusahaan_id').val();
		var kelasId = $('#disp_kelas').is(':checked') ? $('#disp_kelas_id').val() : 0;
		var golonganOperasi = $('#disp_bedah').is(':checked') ? $('#disp_golongan_operasi').val() : 0;
		var komponenId = 0;
		var nama = '';
		var tarif = 0;
		var jumlahTarif = 0;
		var rows = '';
		var keysKomponen = Object.keys(aKomponenTarifList);
		keysKomponen.forEach(function(keyKomponen) {
			if (parseInt(aKomponenTarifList[keyKomponen].check) === 1) {
				komponenId = aKomponenTarifList[keyKomponen].id;
				nama = aKomponenTarifList[keyKomponen].nama;
				switch (jenis) {
					case imediscode.CARA_BAYAR_UMUM:
					case imediscode.CARA_BAYAR_BPJS:
					case imediscode.CARA_BAYAR_JAMKESDA:
					case imediscode.CARA_BAYAR_INTERNAL:
						tarif = aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[parseInt(keyKomponen)].tarif;
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
						tarif = aTarifPelayananDetail[caraBayarId].asuransi_list[perusahaanId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[keyKomponen].tarif;
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						tarif = aTarifPelayananDetail[caraBayarId].perusahaan_list[perusahaanId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[keyKomponen].tarif;
						break;
				}
				rows = '<tr data-save="0">';
				rows += '	<td style="padding:8px;">' + nama + '</td>';
				rows += '	<td style="padding:2px;"><input class="form-control komponen-tarif" type="text" id="disp_komponen_tarif_' + komponenId + '" value="' + tarif + '" data-komponen_id="' + komponenId + '" maxlength="21" autocomplete="off" style="text-align:right;" /></td>';
				rows += '</tr>';
				$('#komponen_tarif_table tbody').append(rows);
				$('#disp_komponen_tarif_' + komponenId).autoNumeric('init', numericOptions);
				jumlahTarif += parseFloat(tarif);
			}
		});
		$('#jumlah_tarif').val(jumlahTarif);
		$('#label_jumlah_tarif').autoNumeric('set', jumlahTarif);
	};
	
	var displayTarif = function() {
		var caraBayarId = parseInt($('#cara_bayar_id').val());
		var jenis = parseInt($('#cara_bayar_id option:selected').data('jenis'));
		var perusahaanId = parseInt($('#perusahaan_id').val());
		var kelasId = $('#disp_kelas').is(':checked') ? $('#disp_kelas_id').val() : 0;
		var golonganOperasi = $('#disp_bedah').is(':checked') ? $('#disp_golongan_operasi').val() : 0;
		var komponenId = 0;
		var tarif = 0;
		var jumlahTarif = 0;
		var keysKomponen = Object.keys(aKomponenTarifList);
		keysKomponen.forEach(function(keyKomponen) {
			if (parseInt(aKomponenTarifList[keyKomponen].check) === 1) {
				switch (jenis) {
					case imediscode.CARA_BAYAR_UMUM:
					case imediscode.CARA_BAYAR_BPJS:
					case imediscode.CARA_BAYAR_JAMKESDA:
					case imediscode.CARA_BAYAR_INTERNAL:
						tarif = aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[keyKomponen].tarif;
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
						tarif = aTarifPelayananDetail[caraBayarId].asuransi_list[perusahaanId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[keyKomponen].tarif;
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						tarif = aTarifPelayananDetail[caraBayarId].perusahaan_list[perusahaanId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[keyKomponen].tarif;
						break;
				}
				$('#disp_komponen_tarif_' + keyKomponen).autoNumeric('set', tarif);
				jumlahTarif += parseFloat(tarif);
			}
		});
		$('#jumlah_tarif').val(jumlahTarif);
		$('#label_jumlah_tarif').autoNumeric('set', jumlahTarif);
	};
	
	var formHandle = function() {
		
		$('#label_jumlah_tarif').autoNumeric('init', numericOptions);
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		displayKomponenTarif();
		
		$('#disp_kelas').on('change', function() {
			if ($(this).is(':checked')) {
				$('#kelas_section').show();
				$('#kelas_button_section').show();
				$('#kelas').val(1);
				
				$('#bedah_section').show();
				if ($('#disp_bedah').is(':checked')) {
					$('#bedah_select_section').show();
					$('#bedah_button_section').show();
				}
				else {
					$('#bedah_select_section').hide();
					$('#bedah_button_section').hide();
				}
				
				var keysCaraBayar = Object.keys(aTarifPelayananDetail);
				keysCaraBayar.forEach(function(keyCaraBayar) {
					switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
						case imediscode.CARA_BAYAR_UMUM:
						case imediscode.CARA_BAYAR_BPJS:
						case imediscode.CARA_BAYAR_JAMKESDA:
						case imediscode.CARA_BAYAR_INTERNAL:
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								if (typeof aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas] === 'undefined') {
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
									
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].id = keyKelas;
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].tarif_pelayanan_detail_id = 'new_tarif_pelayanan_detail_id_' + keyCaraBayar + '_' + keyKelas;
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].jumlah_tarif = 0;
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].check = 1;
									
									var keysKomponen = Object.keys(aKomponenTarifList);
									keysKomponen.forEach(function(keyKomponen) {
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {};
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = 0;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
									});
								}
							});
							break;
						case imediscode.CARA_BAYAR_ASURANSI:
							var keysAsuransi = Object.keys(aAsuransiList);
							keysAsuransi.forEach(function(keyAsuransi) {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									if (typeof aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas] === 'undefined') {
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].id = keyKelas;
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].tarif_pelayanan_detail_id = 'new_tarif_pelayanan_detail_id_' + keyCaraBayar + '_asuransi_' + keyAsuransi + '_' + keyKelas;
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].jumlah_tarif = 0;
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].check = 1;
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {};
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = 0;
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									}
								});
							});
							break;
						case imediscode.CARA_BAYAR_PERUSAHAAN:
							var keysPerusahaan = Object.keys(aPerusahaanList);
							keysPerusahaan.forEach(function(keyPerusahaan) {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									if (typeof aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas] === 'undefined') {
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].id = keyKelas;
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].tarif_pelayanan_detail_id = 'new_tarif_pelayanan_detail_id_' + keyCaraBayar + '_perusahaan_' + keyPerusahaan + '_' + keyKelas;
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].jumlah_tarif = 0;
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].check = 1;
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {};
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = 0;
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									}
								});
							});
							break;
					}
				});
			}
			else {
				$('#kelas_section').hide();
				$('#kelas_button_section').hide();
				$('#kelas').val(0);
				
				$('#bedah_section').hide();
			}
		});

		$('#disp_kelas_id').on('change', function() {
			displayTarif();
		});

		$('#kelas_button').on('click', function() {
			var rows = '<label class="display-block text-semibold">Kelas :</label>';
			
			var keysKelas = Object.keys(aKelasList);
			keysKelas.forEach(function(keyKelas) {
				rows += '<div class="checkbox">';
				rows += '	<label>';
				rows += '		<input class="styled kelas-selection" type="checkbox" data-kelas_id="' + aKelasList[keyKelas].id + '" ' + (parseInt(aKelasList[keyKelas].check) === 1 ? 'checked="checked"' : '') + '> ' + aKelasList[keyKelas].nama;
				rows += '	</label>';
				rows += '</div>';
			});
			$('#mdl_kelas_section').html(rows);
			$(".styled").uniform({
				radioClass: 'choice'
			});
			
			$('#kelas_modal').modal({backdrop: 'static'});
			$('#kelas_modal').modal('show');
		});
		
		$('#mdl_kelas_ok_button').on('click', function() {
			$('.kelas-selection').each(function () {
				var kelasId = $(this).data('kelas_id');
				var isCheck = $(this).is(':checked') ? '1' : '0';
				aKelasList[kelasId].check = isCheck;
			});
			
			var keysCaraBayar = Object.keys(aTarifPelayananDetail);
			keysCaraBayar.forEach(function(keyCaraBayar) {
				switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
					case imediscode.CARA_BAYAR_UMUM:
					case imediscode.CARA_BAYAR_BPJS:
					case imediscode.CARA_BAYAR_JAMKESDA:
					case imediscode.CARA_BAYAR_INTERNAL:
						var keysKelas = Object.keys(aKelasList);
						keysKelas.forEach(function(keyKelas) {
							if (typeof aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].check !== 'undefined') {
								aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].check = aKelasList[keyKelas].check;
							}
						});
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
						var keysAsuransi = Object.keys(aAsuransiList);
						keysAsuransi.forEach(function(keyAsuransi) {
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								if (typeof aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].check !== 'undefined') {
									aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].check = aKelasList[keyKelas].check;
								}
							});
						});
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						var keysPerusahaan = Object.keys(aPerusahaanList);
						keysPerusahaan.forEach(function(keyPerusahaan) {
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								if (typeof aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].check !== 'undefined') {
									aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].check = aKelasList[keyKelas].check;
								}
							});
						});
						break;
				}
			});
			
			displayKelas();
			
			$('#kelas_modal').modal('hide');
		});

		$('#disp_bedah').on('change', function() {
			if ($(this).is(':checked')) {
				$('#bedah_select_section').show();
				$('#bedah_button_section').show();
				$('#bedah').val(1);
				
				var keysCaraBayar = Object.keys(aTarifPelayananDetail);
				keysCaraBayar.forEach(function(keyCaraBayar) {
					switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
						case imediscode.CARA_BAYAR_UMUM:
						case imediscode.CARA_BAYAR_BPJS:
						case imediscode.CARA_BAYAR_JAMKESDA:
						case imediscode.CARA_BAYAR_INTERNAL:
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								var keysOperasi = Object.keys(aGolonganOperasiList);
								keysOperasi.forEach(function(keyOperasi) {
									if (typeof aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi] === 'undefined') {
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi] = {'komponen_list': {}};
										
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].id = keyOperasi;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id = '';
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif = 0;
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].check = 1;
										
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = 0;
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									}
								});
							});
							break;
						case imediscode.CARA_BAYAR_ASURANSI:
							var keysAsuransi = Object.keys(aAsuransiList);
							keysAsuransi.forEach(function(keyAsuransi) {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									var keysOperasi = Object.keys(aGolonganOperasiList);
									keysOperasi.forEach(function(keyOperasi) {
										if (typeof aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi] === 'undefined') {
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi] = {'komponen_list': {}};
											var keysKomponen = Object.keys(aKomponenTarifList);
											keysKomponen.forEach(function(keyKomponen) {
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = 0;
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
											});
										}
									});
								});
							});
							break;
						case imediscode.CARA_BAYAR_PERUSAHAAN:
							var keysPerusahaan = Object.keys(aPerusahaanList);
							keysPerusahaan.forEach(function(keyPerusahaan) {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									var keysOperasi = Object.keys(aGolonganOperasiList);
									keysOperasi.forEach(function(keyOperasi) {
										if (typeof aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi] === 'undefined') {
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi] = {'komponen_list': {}};
											var keysKomponen = Object.keys(aKomponenTarifList);
											keysKomponen.forEach(function(keyKomponen) {
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aKomponenTarifList[keyKomponen].id;
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].nama = aKomponenTarifList[keyKomponen].nama;
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].jenis = aKomponenTarifList[keyKomponen].jenis;
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = "new_komponen_detail_id_" + keyCaraBayar + "_" + keyKelas + "_" + keyKomponen;
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = 0;
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
											});
										}
									});
								});
							});
							break;
					}
				});
			}
			else {
				$('#bedah_select_section').hide();
				$('#bedah_button_section').hide();
				$('#bedah').val(0);
			}
		});
		
		$('#disp_golongan_operasi').on('change', function() {
			displayTarif();
		});
		
		$('#golongan_operasi_button').on('click', function() {
			var rows = '<label class="display-block text-semibold">Golongan Operasi :</label>';
			
			var keysOperasi = Object.keys(aGolonganOperasiList);
			keysOperasi.forEach(function(keyOperasi) {
				rows += '<div class="checkbox">';
				rows += '	<label>';
				rows += '		<input class="styled golongan-operasi-selection" type="checkbox" data-golongan_operasi_id="' + aGolonganOperasiList[keyOperasi].id + '" ' + (parseInt(aGolonganOperasiList[keyOperasi].check) === 1 ? 'checked="checked"' : '') + '> ' + aGolonganOperasiList[keyOperasi].nama;
				rows += '	</label>';
				rows += '</div>';
			});
			$('#mdl_golongan_operasi_section').html(rows);
			$(".styled").uniform({
				radioClass: 'choice'
			});
			$('#golongan_operasi_modal').modal({backdrop: 'static'});
			$('#golongan_operasi_modal').modal('show');
		});
		
		$('#mdl_golongan_operasi_button').on('click', function() {
			$('.golongan-operasi-selection').each(function () {
				var golonganOperasiId = $(this).data('golongan_operasi_id');
				var isCheck = $(this).is(':checked') ? '1' : '0';
				aGolonganOperasiList[golonganOperasiId].check = isCheck;
			});
			
			var keysCaraBayar = Object.keys(aTarifPelayananDetail);
			keysCaraBayar.forEach(function(keyCaraBayar) {
				switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
					case imediscode.CARA_BAYAR_UMUM:
					case imediscode.CARA_BAYAR_BPJS:
					case imediscode.CARA_BAYAR_JAMKESDA:
					case imediscode.CARA_BAYAR_INTERNAL:
						var keysKelas = Object.keys(aKelasList);
						keysKelas.forEach(function(keyKelas) {
							var keysOperasi = Object.keys(aGolonganOperasiList);
							keysOperasi.forEach(function(keyOperasi) {
								if (typeof aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].check !== 'undefined') {
									aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].check = aGolonganOperasiList[keyOperasi].check;
								}
							});
						});
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
						var keysAsuransi = Object.keys(aAsuransiList);
						keysAsuransi.forEach(function(keyAsuransi) {
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								var keysOperasi = Object.keys(aGolonganOperasiList);
								keysOperasi.forEach(function(keyOperasi) {
									if (typeof aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].check !== 'undefined') {
										aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].check = aGolonganOperasiList[keyOperasi].check;
									}
								});
							});
						});
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						var keysPerusahaan = Object.keys(aPerusahaanList);
						keysPerusahaan.forEach(function(keyPerusahaan) {
							var keysKelas = Object.keys(aKelasList);
							keysKelas.forEach(function(keyKelas) {
								var keysOperasi = Object.keys(aGolonganOperasiList);
								keysOperasi.forEach(function(keyOperasi) {
									if (typeof aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].check !== 'undefined') {
										aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].check = aGolonganOperasiList[keyOperasi].check;
									}
								});
							});
						});
						break;
				}
			});
			
			displayGolonganOperasi();
			
			$('#golongan_operasi_modal').modal('hide');
		});
		
		$('#cara_bayar_id').on('change', function() {
			var jenis = parseInt($(this).find('option:selected').data('jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_UMUM:
				case imediscode.CARA_BAYAR_BPJS:
				case imediscode.CARA_BAYAR_JAMKESDA:
				case imediscode.CARA_BAYAR_INTERNAL:
					$('#asuransi_perusahaan_section').hide();
					$('#perusahaan_id').html('<option value="0" selected="selected">[ Asuransi/Perusahaan: ]</option>');
					displayTarif();
					break;
				case imediscode.CARA_BAYAR_ASURANSI:
					$('#perusahaan_title').text('Asuransi');
					$('#asuransi_perusahaan_section').show();
					fillPerusahaan(0, imediscode.CARA_BAYAR_ASURANSI, '#perusahaan_id', '#perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					$('#perusahaan_title').text('Perusahaan');
					$('#asuransi_perusahaan_section').show();
					fillPerusahaan(0, imediscode.CARA_BAYAR_PERUSAHAAN, '#perusahaan_id', '#perusahaan_spinner');
					break;
			}
		});
		
		$('#komponen_tarif_button').on('click', function() {
			var rows = '<label class="display-block text-semibold">Komponen Tarif :</label>';
			var keysKomponen = Object.keys(aKomponenTarifList);
			keysKomponen.forEach(function(keyKomponen) {
				rows += '<div class="checkbox">';
				rows += '	<label>';
				rows += '		<input class="styled komponen-tarif-selection" type="checkbox" data-komponen_id="' + aKomponenTarifList[keyKomponen].id + '" ' + (parseInt(aKomponenTarifList[keyKomponen].check) === 1 ? 'checked="checked"' : '') + '> ' + aKomponenTarifList[keyKomponen].nama;
				rows += '	</label>';
				rows += '</div>';
			});
			$('#komponen_tarif_section').html(rows);
			$(".styled").uniform({
				radioClass: 'choice'
			});
			
			$('#komponen_tarif_modal').modal({backdrop: 'static'});
			$('#komponen_tarif_modal').modal('show');
		});
		
		$('#komponen_tarif_ok_button').on('click', function() {
			
			$('.komponen-tarif-selection').each(function () {
				var komponenId = $(this).data('komponen_id');
				var check = $(this).is(':checked') ? '1' : '0';
				aKomponenTarifList[komponenId].check = check;
			});
			
			var keysCaraBayar = Object.keys(aTarifPelayananDetail);
			keysCaraBayar.forEach(function(keyCaraBayar) {
				switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
					case imediscode.CARA_BAYAR_UMUM:
					case imediscode.CARA_BAYAR_BPJS:
					case imediscode.CARA_BAYAR_JAMKESDA:
					case imediscode.CARA_BAYAR_INTERNAL:
						if (!$('#disp_kelas').is(':checked')) {
							var keysKomponen = Object.keys(aKomponenTarifList);
							keysKomponen.forEach(function(keyKomponen) {
								aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
							});
						}
						else {
							if (!$('#disp_bedah').is(':checked')) {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									var keysKomponen = Object.keys(aKomponenTarifList);
									keysKomponen.forEach(function(keyKomponen) {
										aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
									});
								});
							}
							else {
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									var keysOperasi = Object.keys(aGolonganOperasiList);
									keysOperasi.forEach(function(keyOperasi) {
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									});
								});
							}
						}
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
						var keysAsuransi = Object.keys(aAsuransiList);
						keysAsuransi.forEach(function(keyAsuransi) {
							if (!$('#disp_kelas').is(':checked')) {
								var keysKomponen = Object.keys(aKomponenTarifList);
								keysKomponen.forEach(function(keyKomponen) {
									aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
								});
							}
							else {
								if (!$('#disp_bedah').is(':checked')) {
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									});
								}
								else {
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										var keysOperasi = Object.keys(aGolonganOperasiList);
										keysOperasi.forEach(function(keyOperasi) {
											var keysKomponen = Object.keys(aKomponenTarifList);
											keysKomponen.forEach(function(keyKomponen) {
												aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
											});
										});
									});
								}
							}
						});
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						var keysPerusahaan = Object.keys(aPerusahaanList);
						keysPerusahaan.forEach(function(keyPerusahaan) {
							if (!$('#disp_kelas').is(':checked')) {
								var keysKomponen = Object.keys(aKomponenTarifList);
								keysKomponen.forEach(function(keyKomponen) {
									aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
								});
							}
							else {
								if (!$('#disp_bedah').is(':checked')) {
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										var keysKomponen = Object.keys(aKomponenTarifList);
										keysKomponen.forEach(function(keyKomponen) {
											aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
										});
									});
								}
								else {
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										var keysOperasi = Object.keys(aGolonganOperasiList);
										keysOperasi.forEach(function(keyOperasi) {
											var keysKomponen = Object.keys(aKomponenTarifList);
											keysKomponen.forEach(function(keyKomponen) {
												aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check = aKomponenTarifList[keyKomponen].check;
											});
										});
									});
								}
							}
						});
						break;
				}
			});
			
			displayKomponenTarif();
			
			$('#komponen_tarif_modal').modal('hide');
		});
		
		$('#komponen_tarif_table').on('focus', '.komponen-tarif', function() {
			$(this).select();
		});
		
		$('#komponen_tarif_table').on('change input keyup', '.komponen-tarif', function() {
			var caraBayarId = parseInt($('#cara_bayar_id').val());
			var perusahaanId = parseInt($('#perusahaan_id').val());
			var kelasId = $('#disp_kelas').is(':checked') ? $('#disp_kelas_id').val() : 0;
			var golonganOperasi = $('#disp_bedah').is(':checked') ? $('#disp_golongan_operasi').val() : 0;
			var komponenId = parseInt($(this).data('komponen_id'));
			var oldTarif = aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[komponenId].tarif;
			var newTarif = parseFloat($(this).autoNumeric('get'));
			
			aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].operasi_list[golonganOperasi].komponen_list[komponenId].tarif = newTarif;
			
			var oldJumlahTarif = parseFloat($('#jumlah_tarif').val());
			var newJumlahTarif = (oldJumlahTarif - oldTarif) + newTarif;
			
			var jenis = parseInt($('#cara_bayar_id option:selected').data('jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_UMUM:
				case imediscode.CARA_BAYAR_BPJS:
				case imediscode.CARA_BAYAR_JAMKESDA:
				case imediscode.CARA_BAYAR_INTERNAL:
					if (!$('#disp_kelas').is(':checked')) {
						aTarifPelayananDetail[caraBayarId].jumlah_tarif = newJumlahTarif;
					}
					else {
						if (!$('#disp_bedah').is(':checked')) {
							aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].jumlah_tarif = newJumlahTarif;
						}
						else {
							aTarifPelayananDetail[caraBayarId].kelas_list[kelasId].operasi_list[golonganOperasi].jumlah_tarif = newJumlahTarif;
						}
					}
					break;
				case imediscode.CARA_BAYAR_ASURANSI:
					if (!$('#disp_kelas').is(':checked')) {
						aTarifPelayananDetail[caraBayarId].asuransi_list[perusahaan_id].jumlah_tarif = newJumlahTarif;
					}
					else {
						if (!$('#disp_bedah').is(':checked')) {
							aTarifPelayananDetail[caraBayarId].asuransi_list[perusahaan_id].kelas_list[kelasId].jumlah_tarif = newJumlahTarif;
						}
						else {
							aTarifPelayananDetail[caraBayarId].asuransi_list[perusahaan_id].kelas_list[kelasId].operasi_list[golonganOperasi].jumlah_tarif = newJumlahTarif;
						}
					}
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					if (!$('#disp_kelas').is(':checked')) {
						aTarifPelayananDetail[caraBayarId].perusahaan_list[perusahaan_id].jumlah_tarif = newJumlahTarif;
					}
					else {
						if (!$('#disp_bedah').is(':checked')) {
							aTarifPelayananDetail[caraBayarId].perusahaan_list[perusahaan_id].kelas_list[kelasId].jumlah_tarif = newJumlahTarif;
						}
						else {
							aTarifPelayananDetail[caraBayarId].perusahaan_list[perusahaan_id].kelas_list[kelasId].operasi_list[golonganOperasi].jumlah_tarif = newJumlahTarif;
						}
					}
					break;
			}
			
			$('#jumlah_tarif').val(newJumlahTarif);
			$('#label_jumlah_tarif').autoNumeric('set', newJumlahTarif);
		});
		
		var tarifPelayananApp = {
			initTarifPelayananForm: function () {
				$("#tarif_pelayanan_form").validate({
					rules: {
						'kode': {'required': true, 'minlength': 1},
						'nama': {'required': true, 'minlength': 1}
					},
					messages: {
						'kode': "Kode Diperlukan",
						'nama': "Nama Diperlukan"
					},
					submitHandler: function(form) {
						tarifPelayananApp.addTarifPelayanan($(form));
					}
				});
			},
			addTarifPelayanan: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var oTarifPelayanan = {};
				oTarifPelayanan.id = $('#id').val();
				oTarifPelayanan.uid = $('#uid').val();
				oTarifPelayanan.kode = $('#kode').val();
				oTarifPelayanan.nama = $('#nama').val();
				oTarifPelayanan.kelas = $('#disp_kelas').is(':checked') ? 1 : 0;
				oTarifPelayanan.bedah = $('#disp_bedah').is(':checked') ? 1 : 0;
				oTarifPelayanan.layanan_list = $('#unit').val() !== null ? $('#unit').val() : [$('#layanan_lain_lain').val()];
				oTarifPelayanan.status = $('#status').is(':checked') ? 1 : 0;
				oTarifPelayanan.detail_list = {}
				
				var keysCaraBayar = Object.keys(aTarifPelayananDetail);
				keysCaraBayar.forEach(function(keyCaraBayar) {
					oTarifPelayanan.detail_list[keyCaraBayar] = {};
					oTarifPelayanan.detail_list[keyCaraBayar].id = keyCaraBayar;
					oTarifPelayanan.detail_list[keyCaraBayar].jenis = aTarifPelayananDetail[keyCaraBayar].jenis;
					switch (parseInt(aTarifPelayananDetail[keyCaraBayar].jenis)) {
						case imediscode.CARA_BAYAR_UMUM:
						case imediscode.CARA_BAYAR_BPJS:
						case imediscode.CARA_BAYAR_JAMKESDA:
						case imediscode.CARA_BAYAR_INTERNAL:
							if (oTarifPelayanan.kelas === 0) {
								oTarifPelayanan.detail_list[keyCaraBayar].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].tarif_pelayanan_detail_id;
								oTarifPelayanan.detail_list[keyCaraBayar].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].jumlah_tarif;
								oTarifPelayanan.detail_list[keyCaraBayar].kelas_list = {0: {'operasi_list': {0: {'komponen_list': {}}}}};
								var keysKomponen = Object.keys(aKomponenTarifList);
								keysKomponen.forEach(function(keyKomponen) {
									if (parseInt(aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen] = {};
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id;
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif;
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis = aTarifPelayananDetail[keyCaraBayar].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis;
									}
								});
							}
							else {
								oTarifPelayanan.detail_list[keyCaraBayar].kelas_list = {};
								var keysKelas = Object.keys(aKelasList);
								keysKelas.forEach(function(keyKelas) {
									if (oTarifPelayanan.bedah === 0) {
										if (parseInt(aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].check) === 1) {
											oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
											
											oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].tarif_pelayanan_detail_id;
											oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].jumlah_tarif;
											
											var keysKomponen = Object.keys(aKomponenTarifList);
											keysKomponen.forEach(function(keyKomponen) {
												if (parseInt(aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
													oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {}; 
													oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id;
													oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
													oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif;
												}
											});
										}
									}
									else {
										oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas] = {'operasi_list': {}};
										var keysOperasi = Object.keys(aGolonganOperasiList);
										keysOperasi.forEach(function(keyOperasi, index) {
											if (parseInt(aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].check) === 1) {
												oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi] = {};
											
												oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id;
												oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif;
											
												oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list = {};
												
												var keysKomponen = Object.keys(aKomponenTarifList);
												keysKomponen.forEach(function(keyKomponen) {
													if (parseInt(aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check) === 1) {
														oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
														oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id;
														oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id;
														oTarifPelayanan.detail_list[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif;
													}
												});
											}
										});
									}
								});
							}
							break;
						case imediscode.CARA_BAYAR_ASURANSI:
							oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list = {};
							var keysAsuransi = Object.keys(aAsuransiList);
							keysAsuransi.forEach(function(keyAsuransi) {
								if (oTarifPelayanan.kelas === 0) {
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi] = {};
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].tarif_pelayanan_detail_id;
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].jumlah_tarif;
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list = {0: {'operasi_list': {0: {'komponen_list': {}}}}};
									var keysKomponen = Object.keys(aKomponenTarifList);
									keysKomponen.forEach(function(keyKomponen) {
										if (parseInt(aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen] = {};
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id;
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif;
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis;
										}
									});
								}
								else {
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi] = {};
									oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list = {};
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										if (oTarifPelayanan.bedah === 0) {
											if (parseInt(aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].check) === 1) {
												oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
												
												oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].tarif_pelayanan_detail_id;
												oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].jumlah_tarif;
												
												var keysKomponen = Object.keys(aKomponenTarifList);
												keysKomponen.forEach(function(keyKomponen) {
													if (parseInt(aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
														oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {}; 
														oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id;
														oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
														oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif;
													}
												});
											}
										}
										else {
											oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas] = {'operasi_list': {}};
											var keysOperasi = Object.keys(aGolonganOperasiList);
											keysOperasi.forEach(function(keyOperasi, index) {
												if (parseInt(aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].check) === 1) {
													oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi] = {};
												
													oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id;
													oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif;
												
													oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list = {};
													
													var keysKomponen = Object.keys(aKomponenTarifList);
													keysKomponen.forEach(function(keyKomponen) {
														if (parseInt(aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check) === 1) {
															oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
															oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id;
															oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id;
															oTarifPelayanan.detail_list[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].asuransi_list[keyAsuransi].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif;
														}
													});
												}
											});
										}
									});
								}
							});
							break;
						case imediscode.CARA_BAYAR_PERUSAHAAN:
							oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list = {};
							var keysAsuransi = Object.keys(aPerusahaanList);
							keysAsuransi.forEach(function(keyPerusahaan) {
								if (oTarifPelayanan.kelas === 0) {
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan] = {};
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].tarif_pelayanan_detail_id;
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].jumlah_tarif;
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list = {0: {'operasi_list': {0: {'komponen_list': {}}}}};
									var keysKomponen = Object.keys(aKomponenTarifList);
									keysKomponen.forEach(function(keyKomponen) {
										if (parseInt(aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen] = {};
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].id;
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].tarif;
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[0].operasi_list[0].komponen_list[keyKomponen].jenis;
										}
									});
								}
								else {
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan] = {};
									oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list = {};
									var keysKelas = Object.keys(aKelasList);
									keysKelas.forEach(function(keyKelas) {
										if (oTarifPelayanan.bedah === 0) {
											if (parseInt(aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].check) === 1) {
												oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas] = {'operasi_list': {0: {'komponen_list': {}}}};
												
												oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].tarif_pelayanan_detail_id;
												oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].jumlah_tarif;
												
												var keysKomponen = Object.keys(aKomponenTarifList);
												keysKomponen.forEach(function(keyKomponen) {
													if (parseInt(aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].check) === 1) {
														oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen] = {}; 
														oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].id;
														oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].komponen_detail_id;
														oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[0].komponen_list[keyKomponen].tarif;
													}
												});
											}
										}
										else {
											oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas] = {'operasi_list': {}};
											var keysOperasi = Object.keys(aGolonganOperasiList);
											keysOperasi.forEach(function(keyOperasi, index) {
												if (parseInt(aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].check) === 1) {
													oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi] = {};
												
													oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].tarif_pelayanan_detail_id;
													oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].jumlah_tarif;
												
													oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list = {};
													
													var keysKomponen = Object.keys(aKomponenTarifList);
													keysKomponen.forEach(function(keyKomponen) {
														if (parseInt(aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].check) === 1) {
															oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen] = {};
															oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].id;
															oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].komponen_detail_id;
															oTarifPelayanan.detail_list[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif = aTarifPelayananDetail[keyCaraBayar].perusahaan_list[keyPerusahaan].kelas_list[keyKelas].operasi_list[keyOperasi].komponen_list[keyKomponen].tarif;
														}
													});
												}
											});
										}
									});
								}
							});
							break;
					}
				});
				
				oTarifPelayanan.detail_list = JSON.stringify(oTarifPelayanan.detail_list);
				
				$.post(url, oTarifPelayanan, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		tarifPelayananApp.initTarifPelayananForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index + '?start=' + start + '&length=' + length + '&layanan_id=' + layananId + '&search=' + search;
		});
		
		$('#kode').focus();
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index + '?start=' + start + '&length=' + length + '&layanan_id=' + layananId + '&search=' + search;
							break;
					};
				}
            });
			
        }

    };

}();