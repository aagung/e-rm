var Edit = function () {

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var formHandle = function() {
		
		$('#disp_tgl_habis').daterangepicker({
			autoUpdateInput: false,
			autoapply: true,
			singleDatePicker: true, 
			showDropdowns: true,
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal_lahir').val(chosen_date.format('YYYY-MM-DD'));
		});
		
		$('#disp_tgl_habis').on('apply.daterangepicker', function (ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY'));
		});

		$('#disp_tgl_habis').on('cancel.daterangepicker', function (ev, picker) {
			$(this).val('');
		});
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		var asuransiApp = {
			initAsuransiForm: function () {
				$("#perusahaan_form").validate({
					rules: {
						'kode': {'required': true, 'minlength': 1},
						'nama': {'required': true, 'minlength': 1}
					},
					messages: {
						'kode': "Kode Diperlukan",
						'nama': "Nama Diperlukan"
					},
					submitHandler: function(form) {
						asuransiApp.addAsuransi($(form));
					}
				});
			},
			addAsuransi: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		asuransiApp.initAsuransiForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index;
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();