var Edit = function () {

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var formHandle = function() {
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$('#jenis_unit').on('change', function() {
			switch (parseInt($(this).val())) {
				case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
					$('#ods_odc_section').show();
					if ($('#ods_odc').is(":checked")) {
						$('#jenis_ods_odc_section').show();
					}
					else {
						$('#jenis_ods_odc_section').hide();
					}
					$('#kelas').val(0);
					$('#bedah').val(0);
					break;
				case imediscode.UNIT_LAYANAN_IGD:
                    $('#ods_odc_section').hide();
                    $('#jenis_ods_odc_section').hide();
                    $('#kelas').val(0);
					$('#bedah').val(0);
					break;
				case imediscode.UNIT_LAYANAN_RAWAT_INAP:
					$('#ods_odc_section').hide();
					$('#bedah_section').show();
                    $('#kelas').val(1);
					$('#bedah').val(0);
					break;
				case imediscode.UNIT_LAYANAN_PENUNJANG_MEDIS:
					$('#ods_odc_section').hide();
					$('#jenis_ods_odc_section').hide();
					$('#kelas').val(1);
					$('#bedah').val(0);
					$('#jenis_penunjang_medis_section').show();
					break;
				case imediscode.UNIT_LAYANAN_AMBULANCE:
					$('#ods_odc_section').hide();
					$('#jenis_ods_odc_section').hide();
					$('#kelas').val(0);
					$('#bedah').val(0);
					break;
				case imediscode.UNIT_LAYANAN_LAIN_LAIN:
					$('#ods_odc_section').hide();
					$('#jenis_ods_odc_section').hide();
					$('#kelas').val(0);
					$('#bedah').val(0);
					break;
			}
		});
		
		$('#ods_odc').on('change', function() {
			if ($(this).is(":checked")) {
				$('#jenis_ods_odc_section').show();
			}
			else {
				$('#jenis_ods_odc_section').hide();
			}
		});
		
		$('#jenis_penunjang_medis').on('change', function() {
			var jenis = parseInt($(this).val());
			switch (jenis) {
				case imediscode.PENUNJANG_MEDIS_LABORATORIUM:
				case imediscode.PENUNJANG_MEDIS_RADIOLOGI:
					$('#bedah').val(0);
					break;
				case imediscode.PENUNJANG_MEDIS_OK:
					$('#bedah').val(1);
					break;
			}
		});
		
		var layananApp = {
			initLayananForm: function () {
				$("#layanan_form").validate({
					rules: {
						'nama': {'required': true, 'minlength': 1}
					},
					messages: {
						'nama': "Nama Diperlukan"
					},
					submitHandler: function(form) {
						layananApp.addLayanan($(form));
					}
				});
			},
			addLayanan: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		layananApp.initLayananForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index;
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();