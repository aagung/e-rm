var Index = function () {
	
	var oDaftarBookingTable = null;
	var oDaftarBatalTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarBookingTable = function() {

		oDaftarBookingTable = $('#booking_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_booking,
                "type": "POST"
			},
            "order": [[ 1, "asc" ]],
            "columns": [
				{"data": "no_register", "name": "no_register", "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + data + '</a>';
				}},
				{"data": "tanggal", "name": "tanggal"},
				{"data": "waktu", "name": "waktu"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "ruang_ok", "name": "ruang_ok"},
				{"data": "asal_pasien", "name": "asal_pasien"},
				{"data": "ruang", "name": "ruang"},
				{"data": "jaminan", "name": "jaminan"},
				{"data": "klasifikasi", "name": "klasifikasi"},
				{"data": "action", "name": "action", "render": function() {
					return '<button class="btn bg-success btn-xs batal-row" type="button" data-id="1" data-uid="98b238fb-043d-11e9-830c-68f7280ee3e0" title="Batal">Batal</button>';
				}}
			]
		});
			
    };
	
	var reloadDaftarBooking = function() {
        if (oDaftarBookingTable == null) {
			handleDaftarBookingTable();
        }
		else {
			oDaftarBookingTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarBooking();
			
			$("#booking_table").on("click", ".edit-row", function () {
				var id = $(this).data('id');
				var uid = $(this).data('uid');
				window.location = url_edit+ '?id=0&uid=';
			});
			
			$("#booking_table").on("click", ".batal-row", function () {
				alert('test');
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarBooking();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();