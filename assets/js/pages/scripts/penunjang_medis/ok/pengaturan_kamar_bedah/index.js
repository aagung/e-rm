var Index = function () {
	
	var oDaftarJadwalTable = null;
	var oDaftarBatalTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillJenisOperasi = function(jenisOperasiId, layananId, caraBayarId, perusahaanId, kelasId, golonganOperasi) {
		//$(elementSpinner).show();
        $.getJSON(url_get_tarif + '?layanan_id=' + layananId + '&cara_bayar_id=' + caraBayarId + '&perusahaan_id=' + perusahaanId + '&kelas_id=' + kelasId + '&golongan_operasi=' + golonganOperasi, function(data, status) {
            if (status === 'success') {
                var optionJenisOperasi = '';
                var selected = parseInt(jenisOperasiId) == 0 ? ' selected="selected"' : '';
                optionJenisOperasi += '<option value="0"' + selected + '>[ Pilih Jenis Operasi: ]</option>';
                for (var i = 0; i < data.tarif_pelayanan_list.length; i++) {
                    selected = parseInt(jenisOperasiId) == parseInt(data.tarif_pelayanan_list[i].id) ? ' selected="selected"' : '';
                    optionJenisOperasi += '<option value="' + data.tarif_pelayanan_list[i].id + '"' + selected + '>' + data.tarif_pelayanan_list[i].nama + '</option>';
                }
                $('#mdl_jenis_operasi_id').html(optionJenisOperasi);
                //$(elementId).val(perusahaanId).trigger('change.select2');
				//$(elementSpinner).hide();
            }
        });
    };
	
	var handleDaftarJadwalTable = function() {

		oDaftarJadwalTable = $('#jadwal_operasi_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_jadwal,
                "type": "POST",
				"data": function (d) {
					d.tanggal = $('#filter_tanggal').val();
					d.ruang_operasi_id = $('#filter_ruang_operasi_id').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "jam_mulai", "name": "jam_mulai"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "asal_pasien", "name": "asal_pasien", "render": function(data, type, row, meta) {
					var asalPasien = '';
					switch (parseInt(data)) {
						case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
							asalPasien = "Rawat Jalan";
							break;
						case imediscode.UNIT_LAYANAN_IGD:
							asalPasien = "IGD";
							break;
						case imediscode.UNIT_LAYANAN_RAWAT_INAP:
							asalPasien = "Rawat Inap";
							break;
					}
					return asalPasien;
				}},
				{"data": "layanan", "name": "layanan", "render": function(data, type, row, meta) {
					var layananRuang = '';
					switch (parseInt(row.asal_pasien)) {
						case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
						case imediscode.UNIT_LAYANAN_IGD:
							layananRuang = row.layanan;
							break;
						case imediscode.UNIT_LAYANAN_RAWAT_INAP:
							layananRuang = row.ruang;
							break;
					}
					return layananRuang;
				}},
				{"data": "cara_bayar", "name": "cara_bayar"},
				{"data": "dokter", "name": "dokter"},
				{"data": "diagnosa_utama", "name": "diagnosa_utama"},
				{"data": "nama_operasi", "name": "nama_operasi"},
				{"data": "klasifikasi", "name": "klasifikasi"},
				{"data": "uid", "name": "uid", "width": "20%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-id="' + row.id + '" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>' + 
						   '<a class="text-danger batal-row" data-id="' + row.id + '" data-uid="' + row.uid + '" data-nama="' + row.nama + '" title="Batal"><i class="fa fa-ban"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarJadwal = function() {
        if (oDaftarJadwalTable == null) {
			handleDaftarJadwalTable();
        }
		else {
			oDaftarJadwalTable.ajax.reload();
		}
    };
	
	var handleDaftarBatalTable = function() {

		oDaftarBatalTable = $('#batal_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_batal,
                "type": "POST",
				"data": function (d) {
					d.tanggal_dari = $('#filter_batal_tanggal_dari').val();
					d.tanggal_sampai = $('#filter_batal_tanggal_sampai').val();
					d.no_rm = $('#filter_batal_no_rm').val();
					d.nama = $('#filter_batal_nama').val();
					d.ruang_id = $('#filter_batal_ruang_id').val();
					d.asal_pasien = $('#filter_batal_unit').val();
					d.cara_bayar_id = $('#filter_batal_cara_bayar_id').val();
					d.operator_1 = $('#filter_batal_operator_1').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal"},
				{"data": "jam_mulai", "name": "jam_mulai"},
				{"data": "ruang", "name": "ruang"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "asal_pasien", "name": "asal_pasien", "render": function(data, type, row, meta) {
					var asalPasien = '';
					switch (parseInt(data)) {
						case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
							asalPasien = "Rawat Jalan";
							break;
						case imediscode.UNIT_LAYANAN_IGD:
							asalPasien = "IGD";
							break;
						case imediscode.UNIT_LAYANAN_RAWAT_INAP:
							asalPasien = "Rawat Inap";
							break;
					}
					return asalPasien;
				}},
				{"data": "cara_bayar", "name": "cara_bayar"},
				{"data": "alasan_batal", "name": "alasan_batal"}
			]
		});
			
    };
	
	var reloadBatalTable = function() {
        if (oDaftarBatalTable == null) {
			handleDaftarBatalTable();
        }
		else {
			oDaftarBatalTable.ajax.reload();
		}
    };
	
	var refresh = function(browse) {
		var eventId = browse;

        evenSource = new EventSource(url_refresh + '?event=' + eventId);
        evenSource.addEventListener(eventId, function(e) {
            if (e.data.data) {
                switch (browse) {
					case 'jadwal_operasi':
						reloadDaftarJadwal();
						break;
					case 'jadwal_operasi_batal':
						reloadBatalTable();
						break;
                }
            }
        }, false);
    }
	
    return {

        init: function() {
			
			$('#filter_ruang_operasi_id').val(aRuangOperasiList[0].id);
			
			reloadDaftarJadwal();
			reloadBatalTable();
			refresh('jadwal_operasi');
			refresh('jadwal_operasi_batal');
			
			$('#filter_kamar_operasi').bootpag({
				total: countRuangOperasi,
				maxVisible: 1,
				leaps: false
			}).on("page", function(event, num){
				$.getJSON(url_get_ruang_operasi + '?start=' + num, function(data, status) {
					if (status === 'success') {
						$(".pagination > .active > a").html(data.ruang.nama);
						$('#filter_ruang_operasi_id').val(data.ruang.id);
						reloadDaftarJadwal();
					}
				});
			}).children('.pagination').addClass('pagination-sm');
			$(".pagination > .active > a").html(aRuangOperasiList[0].nama);
			
			$('#disp_filter_tanggal').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
				reloadDaftarJadwal();
			});
			
			$('#disp_mdl_tanggal').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#mdl_tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
			});
			
			$('#disp_filter_batal_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_batal_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
				reloadBatalTable();
			});
			
			$('#disp_filter_batal_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_batal_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
				reloadBatalTable();
			});
			
			var optionBedah = '';
			optionBedah += '<option value="0">[ Pilih Nama Operasi: ]</option>';
			for (var i = 0; i < aBedahList.length; i++) {
				optionBedah += '<option value="' + aBedahList[i].id + '">' + aBedahList[i].nama + '</option>';
			}
			$('#mdl_operasi_id').html(optionBedah);
			
			$('#mdl_operasi_id').on('change', function() {
				fillJenisOperasi(0, $(this).val(), $('#view_mdl_cara_bayar_id').val(), $('#view_mdl_perusahaan_id').val(), $('#view_mdl_kelas_id').val(), $('#view_mdl_golongan_operasi').val());
			});
			
			var optionRuangOperasi = '';
			optionRuangOperasi += '<option value="0">[ Pilih Ruang Operasi: ]</option>';
			for (var i = 0; i < aRuangOperasiList.length; i++) {
				optionRuangOperasi += '<option value="' + aRuangOperasiList[i].id + '">' + aRuangOperasiList[i].nama + '</option>';
			}
			$('#mdl_ruang_id').html(optionRuangOperasi);
			$('#filter_batal_ruang_id').html(optionRuangOperasi);
			
			$('#jadwal_operasi_table').on('click', '.edit-row', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
				$.getJSON(url_get_data_ok + '?id=' + id, function(data, status) {
					if (status === 'success') {
						$('#mdl_id').val(data.ok.id);
						$('#mdl_uid').val(data.ok.uid);
						$('#view_mdl_no_rm').text(data.ok.no_rm);
						$('#view_mdl_nama').text(data.ok.pasien);
						$('#view_mdl_umur').text(data.ok.umur);
						$('#view_mdl_jaminan').text(data.ok.cara_bayar);
						$('#view_mdl_cara_bayar_id').val(data.ok.cara_bayar_id);
						$('#view_mdl_perusahaan_id').val(data.ok.perusahaan_id !== null ? data.ok.perusahaan_id : 0);
						$('#view_mdl_diagnosa').text(data.ok.diagnosa_utama);
						$('#view_mdl_asal_pasien').text(data.ok.asal_pasien);
						$('#view_mdl_poli_ruang').text(data.ok.layanan);
						$('#view_mdl_kelas_id').val(5);
						$('#view_mdl_dokter_dpjp').text(data.ok.dokter);
						$('#view_mdl_golongan_operasi').val(1);
						$('#mdl_tanggal').val(data.ok.tanggal);
						$('#disp_mdl_tanggal').val(moment(data.ok.tanggal).format('DD/MM/YYYY'));
						$('#mdl_jam_mulai').val(data.ok.jam_mulai);
						$('#mdl_operasi_id').val(data.ok.operasi_id);
						$('#mdl_ruang_id').val(data.ok.ruang_id);
						$('#mdl_waktu_operasi').val(data.ok.jam_selesai);
						$('#mdl_klasifikasi').val(data.ok.klasifikasi);
						
						fillJenisOperasi(data.ok.tindakan_operasi_id, data.ok.operasi_id, $('#view_mdl_cara_bayar_id').val(), $('#view_mdl_perusahaan_id').val(), $('#view_mdl_kelas_id').val(), $('#view_mdl_golongan_operasi').val());
					}
				});
				$('#jadwal_operasi_modal').modal({backdrop: 'static'});
				$('#jadwal_operasi_modal').modal('show');
			});
			
			$('#jadwal_operasi_table').on('click', '.batal-row', function(event) {
				event.preventDefault();
				
				var nama = $(this).data('nama');
				$('#mdl_batal_message').text('Jadwal untuk Nama: ' + nama + ' akan dibatalkan.');
				
				$('#mdl_batal_id').val($(this).data('id'));
				$('#mdl_batal_uid').val($(this).data('uid'));
				
				$('#batal_modal').modal({backdrop: 'static'});
				$('#batal_modal').modal('show');
			});
			
			$('#batal_ya_button').on('click', function() {
				var uid = $('#mdl_batal_uid').val();
				var alasan = $('#mdl_batal_alasan').val();
				$.getJSON(url_batal + '?uid=' + uid + '&alasan=' + alasan, function(data, status) {
					
				});
				$('#batal_modal').modal('hide');
				reloadDaftarJadwal();
			});
			
			$('#filter_batal_no_rm').on('change', function() {
				reloadBatalTable();
			});
			
			$('#filter_batal_nama').on('change', function() {
				reloadBatalTable();
			});
			
			$('#filter_batal_ruang_id').on('change', function() {
				reloadBatalTable();
			});
			
			$('#filter_batal_unit').on('change', function() {
				reloadBatalTable();
			});
			
			$('#filter_batal_cara_bayar_id').on('change', function() {
				reloadBatalTable();
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarJadwal();
			});
			
			var okApp = {
				initOkForm: function () {
					$('#ok_form').validate({
						rules: {
						},
						messages: {
						},
						submitHandler: function(form) {
							okApp.addOk($(form));
						}
					});
				},
				addOk: function(form) {
					$.blockUI({ 
						message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: '10px 15px',
							color: '#fff',
							width: 'auto',
							'-webkit-border-radius': 2,
							'-moz-border-radius': 2,
							backgroundColor: '#333'
						}
					});
					var url = url_simpan;
					var postData = form.serialize();
					$.post(url, postData, function(data, status) {
						if (status === "success") {
							showMessage('Simpan', 'Record telah di simpan!', 'success');
							return;
						}
						showMessage('Simpan', 'Record gagal di simpan!', 'danger');
					}, 'json');
				}
			};
			okApp.initOkForm();
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							$('#jadwal_operasi_modal').modal('hide');
							break;
					};
				}
            });
			
        }

    };

}();