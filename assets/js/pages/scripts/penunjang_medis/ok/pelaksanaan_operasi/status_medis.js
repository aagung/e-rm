var StatusMedis = function () {
	
	var oDaftarHistoryTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarHistoryTable = function() {

		oDaftarHistoryTable = $('#riwayat_pengobatan_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_history,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal"},
				{"data": "no_register", "name": "no_register"},
				{"data": "asal_pasien", "name": "asal_pasien", "render": function(data, type, row, meta) {
					var asalPasien = '';
					switch (parseInt(data)) {
						case 1:
							asalPasien = "Rawat Jalan";
							break;
						case 2:
							asalPasien = "IGD";
							break;
						case 3:
							asalPasien = "Rawat Inap";
							break;
					}
					return asalPasien;
				}},
				{"data": "cara_bayar", "name": "cara_bayar"},
				{"data": "perawat", "name": "perawat"},
				{"data": "dokter", "name": "dokter"},
				{"data": "diagnosa_utama", "name": "diagnosa_utama"}
			]
		});
			
    };
	
	var reloadDaftarHistory = function() {
        if (oDaftarHistoryTable == null) {
			handleDaftarHistoryTable();
        }
		else {
			oDaftarHistoryTable.ajax.reload();
		}
    };
	
	var formHandle = function() {
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$('#pengobatan_table').on('click', '.edit-row', function() {
			var id = $(this).data('id');
			var uid = $(this).data('uid');
			window.location = url_status_medis_2 + '?id=' + id + '&uid=' + uid;
		});
		
		var titleApp = {
			initTitleForm: function () {
				$("#title_form").validate({
					rules: {
						'nama': {'required': true, 'minlength': 1},
						'singkatan': {'required': true, 'minlength': 1}
					},
					messages: {
						'nama': "Nama Diperlukan",
						'singkatan': "Singkatan Diperlukan"
					},
					submitHandler: function(form) {
						titleApp.addTitle($(form));
					}
				});
			},
			addTitle: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		titleApp.initTitleForm();
		
		$('#kembali_1_button, #kembali_2_button').on('click', function() {
			window.location = url_index;
		});
		
	}
	
    return {

        init: function() {
			
			reloadDaftarHistory();
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();