var LaporanOK = function () {
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillDokter = function(dokterId, elementId) {
		//$(elementSpinner).show();
        $.getJSON(url_get_dokter, function(data, status) {
            if (status === 'success') {
                var optionDokter = '';
                var selected = parseInt(dokterId) == 0 ? ' selected="selected"' : '';
                optionDokter += '<option value="0"' + selected + '>[ Pilih Dokter: ]</option>';
                for (var i = 0; i < data.dokter_list.length; i++) {
                    selected = parseInt(dokterId) == parseInt(data.dokter_list[i].id) ? ' selected="selected"' : '';
                    optionDokter += '<option value="' + data.dokter_list[i].id + '"' + selected + '>' + data.dokter_list[i].nama + '</option>';
                }
                $(elementId).html(optionDokter);
                //$(elementId).val(perusahaanId).trigger('change.select2');
				//$(elementSpinner).hide();
            }
        });
    };
	
	var formHandle = function() {
		
		$('#label_tindakan_sub_total').autoNumeric('init', numericOptions);
		$('#label_obat_sub_total').autoNumeric('init', numericOptions);
		$('#label_bmhp_sub_total').autoNumeric('init', numericOptions);
		
		$('#uraian_operasi').wysihtml5({
			parserRules:  wysihtml5ParserRules,
			"font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
			"emphasis": true, // Italics, bold, etc. Default true
			"lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
			"html": false, // Button which allows you to edit the generated HTML. Default false
			"link": true, // Button to insert a link. Default true
			"image": false, // Button to insert an image. Default true,
			"action": false, // Undo / Redo buttons,
			"color": true // Button to change color of font
		});
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		var laporanOkApp = {
			initLaporanOkForm: function () {
				$("#laporan_ok_form").validate({
					rules: {

					},
					messages: {
						
					},
					submitHandler: function(form) {
						laporanOkApp.addLaporanOk($(form));
					}
				});
			},
			addLaporanOk: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		laporanOkApp.initLaporanOkForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = $('#prev_url').val();
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = $('#prev_url').val();
							break;
					};
				}
            });
			
        }

    };

}();