var Index = function () {
	
	var oDaftarPelaksanaanOperasiTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPelaksanaanOperasiTable = function() {

		oDaftarPelaksanaanOperasiTable = $('#pelaksanaan_operasi_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data": function (d) {
					d.tanggal_dari = $('#filter_tanggal_dari').val();
					d.tanggal_sampai = $('#filter_tanggal_sampai').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal"},
				{"data": "jam_mulai", "name": "jam_mulai"},
				{"data": "ruang_ok", "name": "ruang_ok"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "ruang", "name": "ruang"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-id="' + row.id + '" data-uid="' + row.uid + '" data-uid_pelayanan="' + row.uid_pelayanan + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarPelaksanaanOperasi = function() {
        if (oDaftarPelaksanaanOperasiTable == null) {
			handleDaftarPelaksanaanOperasiTable();
        }
		else {
			oDaftarPelaksanaanOperasiTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarPelaksanaanOperasi();
			
			$('#disp_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
				reloadDaftarPelaksanaanOperasi();
			});
			
			$('#disp_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
				reloadDaftarPelaksanaanOperasi();
			});
			
			$("#pelaksanaan_operasi_table").on("click", ".edit-row", function () {
				var id = $(this).data('id');
				var uid = $(this).data('uid');
				var uidPelayanan = $(this).data('uid_pelayanan');
				window.location = url_edit + '/' + uidPelayanan + '?browse=ok&uid_ok=' + uid + '&id_ok=' + id;
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarPelaksanaanOperasi();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();