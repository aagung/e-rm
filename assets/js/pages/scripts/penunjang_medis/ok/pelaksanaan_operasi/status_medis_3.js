var StatusMedis3 = function () {

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var formHandle = function() {
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		$('#tambah_cppt_button').on('click', function() {
			$('#buat_cppt_modal').modal({backdrop: 'static'});
			$('#buat_cppt_modal').modal('show');
		});
		
		$('#mdl_cppt_buat_baru_button').on('click', function() {
			$('#buat_cppt_modal').modal('hide');
			
			var id = $('#id').val();
			var uid = $('#uid').val();
			window.location = url_laporan_ok + '?id=' + id + '&uid=' + uid;
		});
		
		var titleApp = {
			initTitleForm: function () {
				$("#title_form").validate({
					rules: {
						'nama': {'required': true, 'minlength': 1},
						'singkatan': {'required': true, 'minlength': 1}
					},
					messages: {
						'nama': "Nama Diperlukan",
						'singkatan': "Singkatan Diperlukan"
					},
					submitHandler: function(form) {
						titleApp.addTitle($(form));
					}
				});
			},
			addTitle: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		titleApp.initTitleForm();
		
		$('#kembali_1_button, #kembali_2_button').on('click', function() {
			window.location = url_index + '?id=' + $('#id').val() + '&uid=' + $('#uid').val();
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();