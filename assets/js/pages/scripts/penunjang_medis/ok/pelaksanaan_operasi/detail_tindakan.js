var DetailTindakan = function () {
	var oTableLookupTarifPelayanan = null;
	var oTableLookupObat = null;
	var oTableLookupBmhp = null;
	var lastTindakanLineNo = 1;
	var lastObatLineNo = 1;
	var lastBmhpLineNo = 1;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var fillDokter = function(dokterId, elementId) {
		//$(elementSpinner).show();
        $.getJSON(url_get_dokter, function(data, status) {
            if (status === 'success') {
                var optionDokter = '';
                var selected = parseInt(dokterId) == 0 ? ' selected="selected"' : '';
                optionDokter += '<option value="0"' + selected + '>[ Pilih Dokter: ]</option>';
                for (var i = 0; i < data.dokter_list.length; i++) {
                    selected = parseInt(dokterId) == parseInt(data.dokter_list[i].id) ? ' selected="selected"' : '';
                    optionDokter += '<option value="' + data.dokter_list[i].id + '"' + selected + '>' + data.dokter_list[i].nama + '</option>';
                }
                $(elementId).html(optionDokter);
                //$(elementId).val(perusahaanId).trigger('change.select2');
				//$(elementSpinner).hide();
            }
        });
    };
	
	var handleLookupTarifPelayanan = function() {
        
        oTableLookupTarifPelayanan = $('#lookup_tarif_pelayanan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_tarif_pelayanan,
                "type": "POST",
				"data"    : function(d) {
								d.layanan_id = $('#layanan_id').val();
								d.cara_bayar_id = $('#cara_bayar_id').val();
								d.perusahaan_id = $('#perusahaan_id').val();
								d.kelas_id = $('#kelas_id').val();
								d.golongan_operasi = $('#golongan_operasi').val();
							},
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode", "name": "kode", 
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.id + '"';
						dataTag += ' data-uid="' + row.uid + '"';
						dataTag += ' data-kode="' + row.kode + '"';
						dataTag += ' data-nama="' + row.nama + '"';
						dataTag += ' data-tarif="' + row.tarif + '"';
						return '<a class="select-row"' + dataTag + ' href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "nama", "name": "nama"},
				{ "data": "tarif", "name": "tarif"}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_tarif_pelayanan_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupTarifPelayanan = function() {
		if (oTableLookupTarifPelayanan == null) {
			handleLookupTarifPelayanan();
		}
		else {
			oTableLookupTarifPelayanan.ajax.reload();
		}
	}
	
	var handleLookupObat = function() {
        
        oTableLookupObat = $('#lookup_obat_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_obat,
                "type": "POST",
				"data": function(p) {
					p.ruang_id = $('#ruang_ok_id').val();
				}
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode_barang", "name": "kode_barang", 
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.barang_id + '"';
						dataTag += ' data-uid="' + row.barang_uid + '"';
						dataTag += ' data-kode="' + row.kode_barang + '"';
						dataTag += ' data-nama="' + row.barang + '"';
						dataTag += ' data-harga_dasar="' + row.harga_dasar + '"';
						dataTag += ' data-harga_jual="' + row.harga + '"';
						return '<a class="select-row"' + dataTag + ' href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "barang", "name": "barang"},
				{ "data": "stock", "name": "stock"}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_obat_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupObat = function() {
		if (oTableLookupObat == null) {
			handleLookupObat();
		}
		else {
			oTableLookupObat.ajax.reload();
		}
	}
	
	var handleLookupBmhp = function() {
        
        oTableLookupBmhp = $('#lookup_bmhp_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_bmhp,
                "type": "POST",
				"data": function(p) {
					p.ruang_id = $('#ruang_ok_id').val();
				}
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode_barang", "name": "kode_barang", 
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.barang_id + '"';
						dataTag += ' data-uid="' + row.barang_uid + '"';
						dataTag += ' data-kode="' + row.kode_barang + '"';
						dataTag += ' data-nama="' + row.barang + '"';
						dataTag += ' data-harga_dasar="' + row.harga_dasar + '"';
						dataTag += ' data-harga_jual="' + row.harga + '"';
						return '<a class="select-row"' + dataTag + ' href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "barang", "name": "barang"},
				{ "data": "stock", "name": "stock"}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_obat_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupBmhp = function() {
		if (oTableLookupBmhp == null) {
			handleLookupBmhp();
		}
		else {
			oTableLookupBmhp.ajax.reload();
		}
	}
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };
	
	var formHandle = function() {
		
		$('#label_tindakan_sub_total').autoNumeric('init', numericOptions);
		$('#label_obat_sub_total').autoNumeric('init', numericOptions);
		$('#label_bmhp_sub_total').autoNumeric('init', numericOptions);
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		lastTindakanLineNo = $('#tindakan_footer_section').prevAll().length;
		
		$('#tambah_tindakan_button').on('click', function() {
			lastTindakanLineNo++;
			lineNo = lastTindakanLineNo;

            var templateString = $('#tindakan-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#tindakan_footer_section').before(newString);
            var $tr = $('#tindakan_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#disp_tindakan_tarif_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_tindakan_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			fillDokter(0, '#tindakan_dokter_' + lineNo);
		});
		
		$('#tindakan_medis_table').on('click', '.tindakan-hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
		});
		
		$('#tindakan_medis_table').on('click', '.tarif-pelayanan-button', function() {
			var $tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_tarif_pelayanan_modal').data('line_no', lineNo);
			
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
		});
		
		$('#lookup_tarif_pelayanan_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
			$('#lookup_tarif_pelayanan_modal').modal('hide');
		});
		
		$('#tindakan_medis_table').on('focus', '.tindakan-quantity-row', function() {
			$(this).select();
		});
		
		$('#tindakan_medis_table').on('change', '.tindakan-quantity-row', function() {
			
		});
		
		$('#tindakan_medis_table').on('focus', '.tindakan-tarif-row', function() {
			$(this).select();
		});
		
		lastObatLineNo = $('#obat_footer_section').prevAll().length;
		
		$('#tambah_obat_button').on('click', function() {
			lastObatLineNo++;
			lineNo = lastObatLineNo;

            var templateString = $('#obat-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#obat_footer_section').before(newString);
            var $tr = $('#obat_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#disp_obat_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_obat_tarif_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_obat_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			fillDokter(0, '#obat_dokter_' + lineNo);
		});
		
		$('#obat_table').on('click', '.obat-hapus-button', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			
			var oldJumlah = parseFloat($('#obat_jumlah_' + lineNo).val());
			var oldSubTotal = parseFloat($('#obat_sub_total').val());
			var newSubTotal = oldSubTotal - oldJumlah;
			
			$('#obat_sub_total').val(newSubTotal);
			$('#label_obat_sub_total').autoNumeric('set', newSubTotal);
			
			$tr.remove();
		});
		
		$('#obat_table').on('click', '.obat-button', function() {
			var $tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_obat_modal').data('line_no', lineNo);
			
			reloadLookupObat();
			
			$('#lookup_obat_modal').modal({backdrop: 'static'});
			$('#lookup_obat_modal').modal('show');
		});
		
		$('#lookup_obat_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
			var id = $(this).data('id');
			var kode = $(this).data('kode');
			var nama = $(this).data('nama');
			var hargaDasar = parseFloat($(this).data('harga_dasar'));
			var hargaJual = parseFloat($(this).data('harga_jual'));
			var lineNo = $('#lookup_obat_modal').data('line_no');
			
			$('#obat_obat_id_' + lineNo).val(id);
			$('#disp_kode_' + lineNo).val(kode);
			$('#label_nama_obat_' + lineNo).text(nama);
			$('#obat_tarif_' + lineNo).val(hargaJual);
			$('#disp_obat_tarif_' + lineNo).autoNumeric('set', hargaJual);
			
			var oldJumlah = parseFloat($('#obat_jumlah_' + lineNo).val());
			var quantity = parseFloat($('#disp_obat_quantity_' + lineNo).autoNumeric('get'));
			var newJumlah = hargaJual * quantity;
			
			$('#obat_jumlah_' + lineNo).val(newJumlah);
			$('#label_obat_jumlah_' + lineNo).autoNumeric('set', newJumlah);
			
			var oldSubTotal = parseFloat($('#obat_sub_total').val());
			var newSubTotal = (oldSubTotal - oldJumlah) + newJumlah;
			
			$('#obat_sub_total').val(newSubTotal);
			$('#label_obat_sub_total').autoNumeric('set', newSubTotal);
			
			$('#lookup_obat_modal').modal('hide');
		});
		
		$('#obat_table').on('focus', '.obat-quantity-row', function() {
			$(this).select();
		});
		
		$('#obat_table').on('change', '.obat-quantity-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			var hargaSatuan = parseFloat($('#obat_tarif_' + lineNo).val());
			var quantity = parseFloat($(this).autoNumeric('get'));
			var oldJumlah = parseFloat($('#obat_jumlah_' + lineNo).val());
			var newJumlah = quantity * hargaSatuan;
			
			$('#obat_quantity_' + lineNo).val(quantity);
			
			$('#obat_jumlah_' + lineNo).val(newJumlah);
			$('#label_obat_jumlah_' + lineNo).autoNumeric('set', newJumlah);
			
			var oldSubTotal = parseFloat($('#obat_sub_total').val());
			var newSubTotal = (oldSubTotal - oldJumlah) + newJumlah;
			
			$('#obat_sub_total').val(newSubTotal);
			$('#label_obat_sub_total').autoNumeric('set', newSubTotal);
		});
		
		$('#obat_table').on('focus', '.obat-tarif-row', function() {
			$(this).select();
		});
		
		$('#obat_table').on('change', '.obat-tarif-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			var hargaSatuan = parseFloat($(this).autoNumeric('get'));
			var quantity = parseFloat($('#obat_quantity_' + lineNo).val());
			var newJumlah = quantity * hargaSatuan;
			
			$('#obat_tarif_' + lineNo).val(hargaSatuan);
			
			$('#obat_jumlah_' + lineNo).val(newJumlah);
			$('#label_obat_jumlah_' + lineNo).autoNumeric('set', newJumlah);
		});
		
		lastBmhpLineNo = $('#bmhp_footer_section').prevAll().length;
		
		$('#tambah_bmhp_button').on('click', function() {
			lastObatLineNo++;
			lineNo = lastObatLineNo;

            var templateString = $('#bmhp-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#bmhp_footer_section').before(newString);
            var $tr = $('#bmhp_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#disp_bmhp_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_bmhp_tarif_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_bmhp_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			fillDokter(0, '#bmhp_dokter_' + lineNo);
		});
		
		$('#bmhp_table').on('click', '.bmhp-hapus-button', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			
			var oldJumlah = parseFloat($('#bmhp_jumlah_' + lineNo).val());
			var oldSubTotal = parseFloat($('#bmhp_sub_total').val());
			var newSubTotal = oldSubTotal - oldJumlah;
			
			$('#bmhp_sub_total').val(newSubTotal);
			$('#label_bmhp_sub_total').autoNumeric('set', newSubTotal);
			
			$tr.remove();
		});
		
		$('#bmhp_table').on('click', '.bmhp-button', function() {
			var $tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_bmhp_modal').data('line_no', lineNo);
			
			reloadLookupBmhp();
			
			$('#lookup_bmhp_modal').modal({backdrop: 'static'});
			$('#lookup_bmhp_modal').modal('show');
		});
		
		$('#lookup_bmhp_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
			var id = $(this).data('id');
			var kode = $(this).data('kode');
			var nama = $(this).data('nama');
			var hargaDasar = parseFloat($(this).data('harga_dasar'));
			var hargaJual = parseFloat($(this).data('harga_jual'));
			var lineNo = $('#lookup_bmhp_modal').data('line_no');
			
			$('#bmhp_bmhp_id_' + lineNo).val(id);
			$('#disp_bmhp_kode_' + lineNo).val(kode);
			$('#label_nama_bmhp_' + lineNo).text(nama);
			$('#bmhp_tarif_' + lineNo).val(hargaJual);
			$('#disp_bmhp_tarif_' + lineNo).autoNumeric('set', hargaJual);
			
			var oldJumlah = parseFloat($('#bmhp_jumlah_' + lineNo).val());
			var quantity = parseFloat($('#disp_bmhp_quantity_' + lineNo).autoNumeric('get'));
			var newJumlah = hargaJual * quantity;
			
			$('#bmhp_jumlah_' + lineNo).val(newJumlah);
			$('#label_bmhp_jumlah_' + lineNo).autoNumeric('set', newJumlah);
			
			var oldSubTotal = parseFloat($('#bmhp_sub_total').val());
			var newSubTotal = (oldSubTotal - oldJumlah) + newJumlah;
			
			$('#bmhp_sub_total').val(newSubTotal);
			$('#label_bmhp_sub_total').autoNumeric('set', newSubTotal);
			
			$('#lookup_bmhp_modal').modal('hide');
		});
		
		$('#bmhp_table').on('focus', '.bmhp-quantity-row', function() {
			$(this).select();
		});
		
		$('#bmhp_table').on('change', '.bmhp-quantity-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			var hargaSatuan = parseFloat($('#bmhp_tarif_' + lineNo).val());
			var quantity = parseFloat($(this).autoNumeric('get'));
			var oldJumlah = parseFloat($('#bmhp_jumlah_' + lineNo).val());
			var newJumlah = quantity * hargaSatuan;
			
			$('#bmhp_quantity_' + lineNo).val(quantity);
			
			$('#bmhp_jumlah_' + lineNo).val(newJumlah);
			$('#label_bmhp_jumlah_' + lineNo).autoNumeric('set', newJumlah);
			
			var oldSubTotal = parseFloat($('#bmhp_sub_total').val());
			var newSubTotal = (oldSubTotal - oldJumlah) + newJumlah;
			
			$('#bmhp_sub_total').val(newSubTotal);
			$('#label_bmhp_sub_total').autoNumeric('set', newSubTotal);
		});
		
		$('#bmhp_table').on('focus', '.bmhp-tarif-row', function() {
			$(this).select();
		});
		
		var okApp = {
			initOkForm: function () {
				$("#ok_form").validate({
					rules: {

					},
					messages: {
						
					},
					submitHandler: function(form) {
						okApp.addOk($(form));
					}
				});
			},
			addOk: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		okApp.initOkForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = $('#prev_url').val();
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = $('#prev_url').val();
							break;
					};
				}
            });
			
        }

    };

}();