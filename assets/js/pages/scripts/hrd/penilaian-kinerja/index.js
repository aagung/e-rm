let api_url = base_url + '/api/hrd/penilaian_kinerja_rs';
let table = {};
let dataSearch = {};

$(function() {
  $periode = $('#periode');

  $periode.on('change', function() {
    let periode = $(this).val();
    dataSearch.periode = periode;
    table.ajax.reload(); 
  });

  table = $('#table_penilaian').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": api_url,
      "type": "POST",
      "data": (d) => {
        d.periode = dataSearch.periode;
      }
    },
    "columns": [
    { 
      "data": "periode_penilaian",
      "render": function(data, type, row) {
        return '<a href="' + penilaian.url.form + '/' + row.uid + '">' + data + '</a>';
      }, 
    },
    { "data": "total_nilai" },
    { "data": "nilai_mutu" },
    { 
      "data": "action",
      "className": "text-center",
      "sortable": false,
      "searchable": false,
      "render": function(data, type, row) {
        return '<a class="text-danger" href="' + penilaian.url.delete + '/' + row.uid + '" onclick="return penilaian.delete(this)"><i class="fa fa-trash"></i></a>';
      }, 
    }
    ]
  } );
});

var penilaian = {
  url : {
    delete : api_url + '/delete',
    form : base_url + '/hrd/penilaian_kinerja/form'
  },
  delete(el) {
    let url = $(el).attr('href');
    swal({
      title: "Apakah anda yakin?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      buttons: ["Tidak", "Ya"]
    })
    .then((confirm) => {
      if (confirm) {
        $.get(url, function(res) {
          swal("Data penilaian berhasil dihapus.", {
            icon: "success",
          });
          table.ajax.reload();
        }, "json");
      }
    });
    return false;
  }
}