let api_url = base_url + '/api/hrd/penilaian_kinerja_rs';

Vue.component('inputHasil', {
  props: { 
    indi: {
      type: Object,
      required: true
    }
  },
  watch: {
    indi: {
      deep: true,
      handler () {
        let indikatorKinerja = this.indi
        let hasil = indikatorKinerja.hasil
        let bobot = indikatorKinerja.bobot
        if (indikatorKinerja.bobot == null) {
          bobot = 0
        }

        let tipe_target = indikatorKinerja.tipe_target
        let capaian = 0
        if (tipe_target != 'string') {
          let target = parseFloat(indikatorKinerja.target)
          if (target > 0) {
            if (parseFloat(hasil) > target) {
              hasil = target
              indikatorKinerja.hasil = hasil
            }
            capaian = hasil / target * bobot
          }
        } else {
          capaian = hasil * bobot / 100
        }

        indikatorKinerja.capaian = capaian.toFixed(2)
      }
    }
  },
  template: `<input type="number" v-model="indi.hasil" :max="indi.target" min="0" class="form-control">`
})

Vue.component('inputHasilFormula', {
  props: { 
    row: {
      type: Object,
      required: true
    },
    indi: {
      type: Array,
      required: true
    }
  },
  data () {
    return {
      fieldId: 'id'
    }
  },
  watch: {
    row: {
      deep: true,
      handler () {
        if (typeof this.row.indikator_id !== 'undefined') {
          this.fieldId = 'indikator_id'
        }

        let hasilFormula = this.indi.find(obj => (obj.tipe === 'hasil' && obj.header_id === this.row.header_id))
        let myArray = this.indi
        if (hasilFormula) {
          let rumus = hasilFormula.rumus.split(" ")
          let hasNull = false
          for (let x = 0; x < rumus.length; x++) {
            if (rumus[x] == parseInt(rumus[x], 10)) {
              let obj = myArray.find(obj => obj[this.fieldId] === rumus[x])
              rumus[x] = obj.hasil
              if (!obj.hasil) {
                hasNull = true
              }
            }
          }

          if (!hasNull) {
            let formula = rumus.join(' ')
            let hasil = eval(formula)
            hasilFormula.hasil = hasil.toFixed(2)
          }
        }

        let hasil = hasilFormula.hasil
        let bobot = hasilFormula.bobot
        if (hasilFormula.bobot == null) {
          bobot = 0
        }

        let tipe_target = hasilFormula.tipe_target
        let capaian = 0
        if (tipe_target != 'string') {
          let target = parseFloat(hasilFormula.target)
          if (target > 0) {
            if (parseFloat(hasil) > target) {
              hasil = target
            }
            capaian = hasil / target * bobot
          }
        } else {
          capaian = hasil * bobot / 100
        }

        hasilFormula.capaian = capaian.toFixed(2)
      }
    }
  },
  template: `<input type="number" v-model="row.hasil" :max="row.target" min="0" class="form-control">`
})

var app = new Vue({
  el: '#display-form',
  data () {
  	return {
	    url: {
	    	perspektif_kinerja: api_url + '/perspektif_kinerja',
        penilaian_kinerja_kinerja: api_url + '/get',
        storePenilaian: api_url + '/store',
        index: base_url + '/hrd/penilaian_kinerja'
	    },
	    perspektifKinerja: [],
	    count: 1,
      tahun: 2019,
      uid: ''
	  }
  },
  mounted () {
    $("#display-form").loading()
    this.uid = $('#uid').val();
    if (this.uid == '') {
      this.getPerspektifKinerja()
    } else {
      this.getPenilaianKinerja()
    }
  },
  computed: {
    nilaiAkhir () {
      let total = 0
      let count = 0
      for (var i = this.capaianKinerja.length - 1; i >= 0; i--) { 
        let capaianKinerja = this.capaianKinerja[i]
        total += parseFloat(capaianKinerja.total_capaian)
        count++
      }
      return (total / count).toFixed(2)
    },
    nilaiMutu () {
      let nilai = this.nilaiAkhir
      if (nilai >= 90 ) {
        return "BAIK SEKALI"
      } else if (nilai >= 70) {
        return "B A I K"
      } else if (nilai >= 60) {
        return "SEDANG"
      } else if (nilai <= 60) {
        return "KURANG"
      }
    },
    capaianKinerja () {
      let capaianKinerja = this.perspektifKinerja
      for (var i = capaianKinerja.length - 1; i >= 0; i--) {
      	let total_capaian = 0
        let row = capaianKinerja[i]
      	let result = this.calculatePenilaian(row.indikator_kinerja, total_capaian)
        if (result.total_capaian !== '')
    		  row.total_capaian = result.total_capaian.toFixed(2)
      }
      return capaianKinerja
    }
  },
  methods: {
    store () {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          $("#display-form").loading()
          const data = new URLSearchParams()
          data.append('penilaian', JSON.stringify(this.perspektifKinerja)) 
          data.append('total_nilai', this.nilaiAkhir) 
          data.append('nilai_mutu', this.nilaiMutu) 
          data.append('periode', this.tahun) 
          
          let url = this.url.storePenilaian
          if (this.uid !== '') {
            url = this.url.storePenilaian + '/' + this.uid
          }

          axios.post(url, data, {
            headers: {
              "Content-Type": 'application/x-www-form-urlencoded'
            }
          }).then((response) => {
            console.log('response', response)
            res = response.data
            if (res.status == 1) {
              icon = "success"
              title = "Berhasil"  
            } else {
              icon = "error"
              title = "Gagal!"
            }
            swal({
              title: title,
              text: res.message,
              icon: icon,
            }).then((e) => {
              window.location = this.url.index;
            })

            $("#display-form").loading('stop')
          })
        }
      })
    },
  	getPerspektifKinerja () {
  		// Make a request for a user with a given ID
			axios.get(this.url.perspektif_kinerja)
			  .then((response) => {
			    // handle success
			    if (response.status == 200) {
			    	let result = response.data
			    	if (result.status == 1) {
			    		this.perspektifKinerja = result.data
              $("#display-form").loading('stop')
			    	}
			    }
			  })
  	},
    getPenilaianKinerja () {
      console.log('this.uid', this.uid)
      axios.get(this.url.penilaian_kinerja_kinerja + '/' + this.uid)
        .then((response) => {
          // handle success
          if (response.status == 200) {
            let result = response.data
            if (result.status == 1) {
              this.perspektifKinerja = result.data
              $("#display-form").loading('stop')
            }
          }
        })
    },
  	calculatePenilaian (listIndikatorKinerja, total_capaian = 0) {
      let nomor = 1
  		for (var j = 0; j < listIndikatorKinerja.length; j++) {
    		let indikatorKinerja = listIndikatorKinerja[j]
        if (indikatorKinerja.capaian !== '') {
    		  total_capaian += parseFloat(indikatorKinerja.capaian)
        }
        if (indikatorKinerja.tipe == 'header') {
          indikatorKinerja.nomor = nomor
          nomor++
        } else {
          indikatorKinerja.nomor = ''
        }
    		if (typeof indikatorKinerja.child !== 'undefined') {
    			let calc = this.calculatePenilaian(indikatorKinerja.child, total_capaian)
    			total_capaian = calc.total_capaian
    		}
    	}
  		return {
  			total_capaian: total_capaian
  		}
  	}
  }
})