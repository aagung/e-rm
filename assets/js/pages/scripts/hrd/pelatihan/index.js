$(function(){
    var the_table;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitkerja  = $('#unitkerja');
    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        /*startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }*/
    });

    $tgl.val('');

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        console.log('jalan');
        tgl_mulai = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir = picker.endDate.format('YYYY-MM-DD');
        unitkerja = $unitkerja.val();
        get_data(tgl_mulai,tgl_akhir,unitkerja);
    });

    $unitkerja.change(function(){
        unitkerja = $unitkerja.val();
        var tgl_mulai = '';
        var tgl_akhir = '';
        if($tgl.val()) {
            tgl_mulai = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        console.log('mulai ',tgl_mulai);
        console.log('akhir ',tgl_akhir);
        get_data(tgl_mulai,tgl_akhir,unitkerja);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(tgl_mulai,tgl_akhir,unitkerja){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_pelatihan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/pelatihan',
            "type": "POST",
            "dataType":"json",
            "data": {tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitkerja:unitkerja}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "tanggal_mulai",
                "render": function(data){
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                "data": "nama_acara",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/pelatihan/view/' + row.uid + '" onClick="view_detail(this)">' + data.toUpperCase() + '</span>';
                }
            },
            {
                "data": "penyelenggara",
                "render": function(data){
                    return data.toUpperCase();
                }
            },
            {
                "data": "durasi",
            },
            {
                "data": "tempat",
                "render": function(data){
                    return data.toUpperCase();
                }
            },
            {
                "data": "jumlah_peserta",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/pelatihan/peserta/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
