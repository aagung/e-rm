let api_url = base_url + '/api/hrd/indikator_penilaian_kinerja_rs';

Vue.component('inputHasil', {
  props: { 
    indi: {
      type: Object,
      required: true
    }
  },
  watch: {
    indi: {
      deep: true,
      handler () {
        let indikatorKinerja = this.indi
        let hasil = indikatorKinerja.hasil
        let bobot = indikatorKinerja.bobot
        if (indikatorKinerja.bobot == null) {
          bobot = 0
        }

        let tipe_target = indikatorKinerja.tipe_target
        let capaian = 0
        if (tipe_target != 'string') {
          let target = parseFloat(indikatorKinerja.target)
          if (target > 0) {
            if (parseFloat(hasil) > target) {
              hasil = target
              indikatorKinerja.hasil = hasil
            }
            capaian = hasil / target * bobot
          }
        } else {
          capaian = hasil * bobot / 100
        }

        indikatorKinerja.capaian = capaian.toFixed(2)
      }
    }
  },
  template: `<input type="number" v-model="indi.hasil" :max="indi.target" min="0" class="form-control">`
})

Vue.component('inputHasilFormula', {
  props: { 
    row: {
      type: Object,
      required: true
    },
    indi: {
      type: Array,
      required: true
    }
  },
  data () {
    return {
      fieldId: 'id'
    }
  },
  watch: {
    row: {
      deep: true,
      handler () {
        if (typeof this.row.indikator_id !== 'undefined') {
          this.fieldId = 'indikator_id'
        }

        let hasilFormula = this.indi.find(obj => (obj.tipe === 'hasil' && obj.header_id === this.row.header_id))
        let myArray = this.indi
        if (hasilFormula) {
          let rumus = hasilFormula.rumus.split(" ")
          let hasNull = false
          for (let x = 0; x < rumus.length; x++) {
            if (rumus[x] == parseInt(rumus[x], 10)) {
              let obj = myArray.find(obj => obj[this.fieldId] === rumus[x])
              rumus[x] = obj.hasil
              if (!obj.hasil) {
                hasNull = true
              }
            }
          }

          if (!hasNull) {
            let formula = rumus.join(' ')
            let hasil = eval(formula)
            hasilFormula.hasil = hasil.toFixed(2)
          }
        }

        let hasil = hasilFormula.hasil
        let bobot = hasilFormula.bobot
        if (hasilFormula.bobot == null) {
          bobot = 0
        }

        let tipe_target = hasilFormula.tipe_target
        let capaian = 0
        if (tipe_target != 'string') {
          let target = parseFloat(hasilFormula.target)
          if (target > 0) {
            if (parseFloat(hasil) > target) {
              hasil = target
            }
            capaian = hasil / target * bobot
          }
        } else {
          capaian = hasil * bobot / 100
        }

        hasilFormula.capaian = capaian.toFixed(2)
      }
    }
  },
  template: `<input type="number" v-model="row.hasil" :max="row.target" min="0" class="form-control">`
})

var app = new Vue({
  el: '#display-form',
  data () {
  	return {
	    url: {
	    	perspektif_kinerja: api_url + '/perspektif_kinerja',
        perspektifStore: api_url + '/perspektif_kinerja_store',
        perspektifDelete: api_url + '/perspektif_kinerja_delete',
        indikator_kinerja: api_url + '/get',
        indikatorStore: api_url + '/store',
        indikatorDelete: api_url + '/delete',
        index: base_url + '/hrd/penilaian_kinerja'
      },
      loading: {
        perspektif: false,
        indikator: false
      },
      form: {
        perspektif: {
          nama: '',
          id: ''
        },
        indikator: {
          id: '',
          tipe: '',
          nama: '',
          tipe_target: 'persentase',
          target: '',
          bobot: '',
          rumus: '',
          header_id: null,
          parent_id: null,
          perspektif_kinerja_id: 0
        }
      },
      tipe: [
        { id: 'input', nama: 'Input' },
        { id: 'header', nama: 'Header' },
        { id: 'hasil', nama: 'Hasil' }
      ],
      tipeTarget: [
        { id: 'persentase', nama: 'Persentase' },
        { id: 'numeric', nama: 'Angka' },
        { id: 'string', nama: 'Teks' }
      ],
      indikatorKinerja: [],
      perspektifKinerja: [],
      perspektif: '',
      count: 1,
      loaded: false
	  }
  },
  mounted () {
    $("#display-form").loading()
    this.getPerspektifKinerja()
  },
  computed: {
    total_bobot () {
      let total_bobot = 0
      for (let i = 0; i < this.indikatorKinerja.length; i++) {
        let indi = this.indikatorKinerja[i]
        if (indi.bobot) {
          total_bobot += parseInt(indi.bobot)
        }
        if (indi.child) {
          for (let j = 0; j < indi.child.length; j++) {
            let indiC = indi.child[j]
            if (indiC.bobot) {
              total_bobot += parseInt(indiC.bobot)
            }
          }
        }
      }
      return total_bobot
    }
  },
  methods: {
    perspektifEdit () {
      if (!this.perspektif) {
        alert('Perspektif belum dipilih')
        return
      }
      let perspektif = this.perspektifKinerja.find((val) => val.id == this.perspektif)
      this.form.perspektif.id = perspektif.id
      this.form.perspektif.nama = perspektif.nama
      $('#modal_mini').modal('show')
    },
    perspektifStore () {
      if (this.form.perspektif.nama == '') {
        alert('Nama harus diisi!')
        return
      }
      this.loading.perspektif = true
      const data = new URLSearchParams()
      data.append('nama', this.form.perspektif.nama)

      let url = this.url.perspektifStore
      if (this.form.perspektif.id !== '') {
        url = this.url.perspektifStore + '/' + this.form.perspektif.id
      }

      axios.post(url, data, {
        headers: {
          "Content-Type": 'application/x-www-form-urlencoded'
        }
      }).then((response) => {
        res = response.data
        if (res.status == 1) {
          icon = "success"
          title = "Berhasil"
        } else {
          icon = "error"
          title = "Gagal!"
        }
        swal({
          title: title,
          text: res.message,
          icon: icon,
        }).then((e) => {
          $('#modal_mini').modal('hide')
          this.getPerspektifKinerja()
        })
        this.perspektifResetForm()
        this.loading.perspektif = false
      })
    },
    perspektifDelete() {
      if (!this.perspektif) {
        alert('Perspektif belum dipilih')
        return
      }
      swal({
        title: "Apakah anda yakin?",
        text: "Data yang dihapus tidak bisa dikembalikan.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      }).then((confirm) => {
        if (confirm) { 
          this.loading.perspektif = true
          let url = this.url.perspektifDelete + '/' + this.perspektif
          axios.get(url, {
            headers: {
              "Content-Type": 'application/x-www-form-urlencoded'
            }
          }).then((response) => {
            res = response.data
            if (res.status == 1) {
              icon = "success"
              title = "Berhasil"
            } else {
              icon = "error"
              title = "Gagal!"
            }
            swal({
              title: title,
              text: res.message,
              icon: icon,
            }).then((e) => {
              $('#modal_mini').modal('hide')
              this.getPerspektifKinerja()
            })
            this.perspektifResetForm()
            this.perspektif = ''
            this.loading.perspektif = false
          })
        }
      })
    },
    perspektifResetForm () {
      this.form.perspektif.nama = ''
      this.form.perspektif.id = ''
    },
  	getPerspektifKinerja () {
  		// Make a request for a user with a given ID
			axios.get(this.url.perspektif_kinerja)
			  .then((response) => {
			    // handle success
			    if (response.status == 200) {
			    	let result = response.data
			    	if (result.status == 1) {
              this.perspektifKinerja = result.data
              this.loaded = true
              $("#display-form").loading('stop')
			    	}
			    }
			  })
    },
    indikatorEdit(indikator) {
      console.log('indikator', indikator);

      this.form.indikator.id = indikator.id
      this.form.indikator.nama = indikator.nama
      this.form.indikator.tipe = indikator.tipe
      this.form.indikator.tipe_target = indikator.tipe_target
      this.form.indikator.target = indikator.target
      this.form.indikator.bobot = indikator.bobot
      this.form.indikator.rumus = indikator.rumus
      $('#modal_indikator').modal('show')
    },
    indikatorAdd(parent_id = null, header_id = null) {
      if (!this.perspektif) {
        alert('Perspektif belum dipilih')
        return
      }
      let perspektif = this.perspektifKinerja.find((val) => val.id == this.perspektif)
      this.form.indikator.perspektif_kinerja_id = perspektif.id
      if (parent_id) {
        this.form.indikator.parent_id = parent_id
      }
      
      if (header_id) {
        this.form.indikator.header_id = header_id
      }

      $('#modal_indikator').modal('show')
    },
    indikatorStore() {
      if (this.form.indikator.nama == '') {
        alert('Nama harus diisi!')
        return
      }
      if (this.form.indikator.tipe == '') {
        alert('Tipe harus diisi!')
        return
      }
      this.loading.indikator = true
      const data = new URLSearchParams()
      data.append('nama', this.form.indikator.nama)
      data.append('tipe', this.form.indikator.tipe)
      data.append('tipe_target', this.form.indikator.tipe_target)
      if (this.form.indikator.target) {
        data.append('target', this.form.indikator.target)
      }
      if (this.form.indikator.bobot) {
        data.append('bobot', this.form.indikator.bobot)
      }
      if (this.form.indikator.rumus) {
        data.append('rumus', this.form.indikator.rumus)
      }
      if (this.form.indikator.parent_id) {
        data.append('parent_id', this.form.indikator.parent_id)
      }
      if (this.form.indikator.header_id) {
        data.append('header_id', this.form.indikator.header_id)
      }
      if (this.form.indikator.perspektif_kinerja_id) {
        data.append('perspektif_kinerja_id', this.form.indikator.perspektif_kinerja_id)
      }

      let url = this.url.indikatorStore
      if (this.form.indikator.id !== '') {
        url = this.url.indikatorStore + '/' + this.form.indikator.id
      }

      axios.post(url, data, {
        headers: {
          "Content-Type": 'application/x-www-form-urlencoded'
        }
      }).then((response) => {
        res = response.data
        if (res.status == 1) {
          icon = "success"
          title = "Berhasil"
        } else {
          icon = "error"
          title = "Gagal!"
        }
        swal({
          title: title,
          text: res.message,
          icon: icon,
        }).then((e) => {
          $('#modal_indikator').modal('hide')
          this.getIndikatorKinerja(this.perspektif)
        })
        this.indikatorResetForm()
        this.loading.indikator = false
      })
    },
    indikatorDelete(id) {
      if (!id) {
        alert('Indikator belum dipilih')
        return
      }
      swal({
        title: "Apakah anda yakin?",
        text: "Data yang dihapus tidak bisa dikembalikan.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      }).then((confirm) => {
        if (confirm) {
          this.loading.indikator = true
          let url = this.url.indikatorDelete + '/' + id
          axios.get(url, {
            headers: {
              "Content-Type": 'application/x-www-form-urlencoded'
            }
          }).then((response) => {
            res = response.data
            if (res.status == 1) {
              icon = "success"
              title = "Berhasil"
            } else {
              icon = "error"
              title = "Gagal!"
            }
            swal({
              title: title,
              text: res.message,
              icon: icon,
            }).then((e) => {
              this.getIndikatorKinerja(this.perspektif)
            })
            this.indikatorResetForm()
            this.loading.indikator = false
          })
        }
      })
    },
    indikatorResetForm() {
      this.form.indikator.nama = ''
      this.form.indikator.id = ''
      this.form.indikator.tipe = ''
      this.form.indikator.nama = ''
      this.form.indikator.tipe_target = 'persentase'
      this.form.indikator.target = ''
      this.form.indikator.bobot = ''
      this.form.indikator.rumus = ''
      this.form.indikator.header_id = null
      this.form.indikator.parent_id = null
      this.form.indikator.perspektif_kinerja_id = 0
    },
    getIndikatorKinerja (id) {
      axios.get(this.url.indikator_kinerja + '/' + id)
        .then((response) => {
          // handle success
          if (response.status == 200) {
            let result = response.data
            if (result.status == 1) {
              this.indikatorKinerja = result.data
              this.setPenomoran(this.indikatorKinerja)
              $("#display-form").loading('stop')
            }
          }
        })
    },
    setPenomoran(listIndikatorKinerja) {
      let nomor = 1
      for (var j = 0; j < listIndikatorKinerja.length; j++) {
        let indikatorKinerja = listIndikatorKinerja[j]
        if (indikatorKinerja.tipe == 'header') {
          indikatorKinerja.nomor = nomor
          nomor++
        } else {
          indikatorKinerja.nomor = ''
        }
        if (typeof indikatorKinerja.child !== 'undefined') {
          let calc = this.setPenomoran(indikatorKinerja.child)
        }
      }
    }
  },
  watch: {
    perspektif (newVal) {
      console.log('newVal: ', newVal);
      this.getIndikatorKinerja(newVal)
    }
  }
})