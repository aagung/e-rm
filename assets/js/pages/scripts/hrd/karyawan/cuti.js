let tableCuti = {};
let currentDate = moment().format('DD/MM/YYYY');
$(function() {
  $unitKerja = $('#jenis_cuti');
  $lamaCuti = $('#lama_cuti');
  $sisaCuti = $('#sisa_cuti');
  
  let uid = $('input[name="uid"]').val();
  $tanggalMulai = $('#cuti_tanggal_mulai');
  $tanggalMulai.daterangepicker({
    singleDatePicker: true,
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });
  
  $tanggalMasuk = $('#cuti_tanggal_masuk');
  $tanggalMasuk.daterangepicker({
    singleDatePicker: true,
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });

  tableCuti = $('#table_cuti').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": cuti.url.index + '/' + uid,
      "type": "POST",
    },
    "columns": [
      { 
        "data": "created_at",
        "render": function(data, type, row) {
          return '<a href="#" onclick="return cuti.showDetail(' + row.id + ')">' + moment(data).format('DD/MM/YYYY') + '</a>';
        }, 
      },
      { "data": "jenis_cuti" },
      { 
        "data": "tanggal_mulai",
        "render": function(data, type, row) {
          return moment(data).format('DD/MM/YYYY');
        },
      },
      { "data": "lama_cuti" },
      { 
        "data": "tanggal_masuk",
        "render": function(data, type, row) {
          return moment(data).format('DD/MM/YYYY');
        },
      },
      { 
        "data": "status",
        "render": function(data, type, row) {
          return 'Cuti';
        },
      },
      { 
        "data": "action",
        "className": "text-center",
        "sortable": false,
        "searchable": false,
        "render": function(data, type, row) {
          return '<i class="icon icon-checkmark-circle"></i>';
        }, 
      }
    ]
  });

  $(".form-pengajuan-cuti").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Ajukan cuti untuk" + $('#nama').val() + " (" + $('#nik').val() + "), Selama " + $('#lama_cuti').val() + " Hari",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          let tanggalMulai = $("#cuti_tanggal_mulai").val();
          let tanggalMasuk = $("#cuti_tanggal_masuk").val();
          tanggalMulai = moment(tanggalMulai, 'DD/MM/YYYY').format('YYYY-MM-DD');
          tanggalMasuk = moment(tanggalMasuk, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#cuti_tanggal_mulai").val(tanggalMulai);
          $("#cuti_tanggal_masuk").val(tanggalMasuk);
          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                tanggalMulai = moment(tanggalMulai).format('DD/MM/YYYY');
                tanggalMasuk = moment(tanggalMasuk).format('DD/MM/YYYY');
                $("#cuti_tanggal_mulai").val(tanggalMulai);
                $("#cuti_tanggal_masuk").val(tanggalMasuk);
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('#jenis_cuti_id').val('').trigger('change');
                  $('#lama_cuti').val('');
                  $('#cuti_tanggal_mulai').val(currentDate);
                  $('#cuti_tanggal_masuk').val(currentDate);
                  tableCuti.ajax.reload();
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var cuti = {
  url : {
    index: base_url + '/api/hrd/cuti/index',
    delete : base_url + '/api/hrd/cuti/delete',
    form : base_url + '/hrd/cuti/form'
  },
  showDetail(id, hasil = false) {    
    $("#modal_cuti .dialog-loading").loading();
    $.ajax({
      url: base_url + '/api/hrd/cuti/get/' + id,
      type: 'GET',
      dataType: 'JSON',
      success: function (res) {
        let data = res.data;

        $("#modal_cuti .jenis_cuti_id").val(data.jenis_cuti_id).trigger('change');
        $("#modal_cuti .lama_cuti").val(data.lama_cuti);
        $("#modal_cuti .sisa_cuti").val(data.sisa_cuti);
        $("#modal_cuti .tanggal_masuk").val(moment(data.tanggal_masuk).format('DD/MM/YYYY'));
        $("#modal_cuti .tanggal_mulai").val(moment(data.tanggal_mulai).format('DD/MM/YYYY'));

        $("#modal_cuti .dialog-loading").loading('stop');
        $('#modal_cuti').modal('show');
      }
    });
    return false;
  },
}