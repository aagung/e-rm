let tableRewardPunishment = {};
$(function() {
  $jenis        = $('input.jenis-reward');
  $totalJam  = $('#total_jam');

  let uid = $('input[name="uid"]').val();
  $tanggalBerlaku = $('#tanggal_berlaku');
  $tanggalBerlaku.daterangepicker({
    singleDatePicker: true,
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });

  $tanggalBerakhir = $('#tanggal_berakhir');
  $tanggalBerakhir.daterangepicker({
    singleDatePicker: true,
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });
  
  $jenis.on('click', function (el) {
    let jenis = $(this).val();
    $('input[name=nominal]').attr('disabled',true)
    $('#input-lain-lain').attr('disabled',true)
    console.log('jenis', jenis)
    switch (jenis) {
      case 'Bonus' :
        $('#nominal-bonus').removeAttr('disabled').focus()
      break;
      case 'Kenaikan Gaji' :
        $('#nominal-kenaikan-gaji').removeAttr('disabled').focus()
      break;
      case 'Lain-lain' :
        $('#input-lain-lain').removeAttr('disabled').focus()
      break;
    }
  })

  tableRewardPunishment = $('#table_reward_punishment').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": reward_punishment.url.index + '/' + uid,
      "type": "POST",
    },
    "columns": [
      { 
        "data": "created_at",
        "render": function(data, type, row) {
          return '<a href="#" onclick="return reward_punishment.showDetail(' + row.id + ')">' + moment(data).format('DD/MM/YYYY') + '</a>';
        }, 
      },
      { "data": "jenis" },
      { "data": "keterangan" }
    ]
  });

  $(".form-reward").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('input[name=nominal]').val('');
                  $('#input-lain-lain').val('');
                  $('#keterangan-reward').val('');
                  tableRewardPunishment.ajax.reload();
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });

  $(".form-punishment").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          let tanggalBerlaku = $("#tanggal_berlaku").val();
          tanggalBerlaku = moment(tanggalBerlaku, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#tanggal_berlaku").val(tanggalBerlaku);
          let tanggalBerakhir = $("#tanggal_berakhir").val();
          tanggalBerakhir = moment(tanggalBerakhir, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#tanggal_berakhir").val(tanggalBerakhir);

          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                tanggalBerlaku = moment(tanggalBerlaku).format('DD/MM/YYYY');
                $("#tanggal_berlaku").val(tanggalBerlaku);
                tanggalBerakhir = moment(tanggalBerakhir).format('DD/MM/YYYY');
                $("#tanggal_berakhir").val(tanggalBerakhir);

                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('#punishment-jenis').val('').trigger('change');
                  $('#sk_surat_peringatan').val('');
                  $('#punishment-keterangan').val('');
                  $('#tanggal_berlaku').val(currentDate);
                  $('#tanggal_akhir').val(currentDate);
                  tableRewardPunishment.ajax.reload();
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var reward_punishment = {
  url : {
    index: base_url + '/api/hrd/reward_punishment/index',
    delete : base_url + '/api/hrd/reward_punishment/delete',
    form : base_url + '/hrd/reward_punishment/form'
  },
  showDetail(id, hasil = false) {    
    $(".dialog-loading").loading();
    $.ajax({
      url: base_url + '/api/hrd/reward_punishment/get/' + id,
      type: 'GET',
      dataType: 'JSON',
      success: function (res) {
        let data = res.data;
        if (data.kategori == 'reward') {
          $('#modal_reward_punishment .modal-title').html('REWARD');
          $('#modal_reward_punishment .reward-jenis').val(data.jenis);
          $('#modal_reward_punishment .reward-nominal').val(data.nominal);
          $('#modal_reward_punishment .reward-keterangan').val(data.keterangan);
          $('.detail-punishment').hide();
          $('.detail-reward').show();
        } else {
          $('#modal_reward_punishment .modal-title').html('PUNISHMENT');
          $('#modal_reward_punishment .punishment-jenis').val(data.jenis);
          $('#modal_reward_punishment .punishment-tanggal-berlaku').val(moment(data.tanggal_berlaku).format('DD/MM/YYYY'));
          $('#modal_reward_punishment .punishment-tanggal-berakhir').val(moment(data.tanggal_berakhir).format('DD/MM/YYYY'));
          $('#modal_reward_punishment .punishment-sk-surat-peringatan').val(data.sk_surat_peringatan);
          $('#modal_reward_punishment .punishment-keterangan').val(data.keterangan);
          $('.berkas-sk-peringatan').html('');
          if (data.berkas_sk_peringatan.length > 0) {
            $('.div-berkas-sk-peringatan').show();
          } else {
            $('.div-berkas-sk-peringatan').hide();
          }
          for (let i = 0; i < data.berkas_sk_peringatan.length; i++) {
            let row = data.berkas_sk_peringatan;
            let item = '<div class="col-md-3">' +
              '<div class="thumbnail">' +
                '<div class="thumb">' +
                  '<img src="' + row[i].berkas + '" alt="">' +
                  '<div class="caption-overflow">' +
                    '<span>' +
                      '<a href="' + row[i].berkas + '" class="btn btn-flat border-white text-white btn-rounded btn-icon" data-popup="lightbox"><i class="icon-zoomin3"></i></a>' +
                    '</span>' +
                  '</div>' +
                '</div>' +
                '<div class="">' +
                  '<h6 class="text-semibold no-margin-top text-center">' + row[i].nama + '</h6>' +
                '</div>' +
              '</div>' +
            '</div>';
            $('.berkas-sk-peringatan').append(item);
          }

          $('.detail-reward').hide();
          $('.detail-punishment').show();
        }
        $(".dialog-loading").loading('stop');
        $('#modal_reward_punishment').modal('show');
      }
    });
    return false;
  },
  hideDetail() {
    $('#modal_reward_punishment').modal('hide');
  },
  storeHasil() {
    $(".form-hasil-reward_punishment").submit();
  }
}

function diff(start, end) {
	if (start && end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    
    return parseFloat(hours + minutes/60).toFixed(2);
	} else {
		return '';
	}
}