var page = -1;
var infinit = false;
$(window).scroll(function() {
    let heightScroll = $(window).scrollTop() + $(window).height();
    let heigtDocument = $(document).height();
    if( heightScroll + 2 >= heigtDocument) {
        console.log("page", page);
        console.log("infinit", infinit);
        if (infinit) {
            page++;
            loadMoreData(page);
        }
        
    }
});

$(function(){
    $('.btn-history').on('addClass', function() {
        infinit = true;
        page = -1;
        $(".history").html('');
    });

    $('.btn-history').on('removeClass', function() {
        infinit = false;
    });
});

(function ($) {
  $.each(['addClass', 'removeClass'], function (i, ev) {
    var el = $.fn[ev];
    $.fn[ev] = function () {
      this.trigger(ev);
      return el.apply(this, arguments);
  };
});
})(jQuery);

function loadMoreData(page = ''){
    var uid = $('input[name="uid"]').val();
    $.ajax(
    {
        url: base_url + '/api/hrd/riwayat_kepegawaian/index/' + uid + '?page=' + page,
        type: "get",
        beforeSend: function()
        {
            $('.ajax-load').show();
        }
    })
    .done(function(data)
    {
        console.log("data", data);
        if(data == ""){
            infinit = false;
            $('.ajax-load').html("Tidak ditemukan riwayat yang lain.");
            return;
        } else {
            $('.ajax-load').html("Scroll untuk memuat riwayat yang lain.");
            $(".history").append(data);
        }
    })
    .fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('server not responding...');
    });
}