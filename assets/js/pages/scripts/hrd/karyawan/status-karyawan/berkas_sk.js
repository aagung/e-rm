let BerkasSK = {
	b64_sk : '',
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	formBerkas : '',
	table 	   : '',
	resetForm() {
		BerkasSK.formBerkas.find('.btn-upload').html('<i class="fa fa-upload"></i>');
		BerkasSK.b64_sk     = '';
		BerkasSK.stateEdit  = false;
		BerkasSK.rowEdit    = '';
		BerkasSK.formBerkas.find('#nama_berkas').val('');
		BerkasSK.formBerkas.find('#berkas_sk').val('');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('input[name="berkas_sk_deleted"]').val('');
	},

	simpan() {
		let data = {
			id            : '',
			created_at    : moment().format('DD/MM/YYYY'),
			nama_berkas   : BerkasSK.formBerkas.find('#nama_berkas').val(),
			berkas_sk     : BerkasSK.b64_sk
		};

		if (!BerkasSK.stateEdit) {
			BerkasSK.createRow(data);
		} else {
			$(BerkasSK.rowEdit).find('#nama_berkas').val(data.nama_berkas);

			if(data.berkas_sk) {
				$(BerkasSK.rowEdit).find('.prev_zoom').attr('href', data.berkas_sk).show();
			}
			
			$(BerkasSK.rowEdit).find('.berkas_sk').val(data.berkas_sk);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					BerkasSK.deleted.push(id);
					$('input[name="berkas_sk_deleted"]').val(JSON.stringify(BerkasSK.deleted));
				}

				$row.remove();
				swal("Data pendidikan berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},
	createRow(data) {
		let maxRow = $("#table_berkas_sk_pengangkatan tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		// + '<a onclick="return BerkasSK.edit(this)">'
		+ '<span class="tanggal">' + data.created_at + '</span>'
		// + '</a>'
		+ '<input type="hidden" name="berkas_sk[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="nama_berkas">' + data.nama_berkas + '</span>'
		+ '<input type="hidden" name="berkas_sk[' + indexRow + '][nama]" class="nama_berkas" value="' + data.nama_berkas + '">'
		+ '</td>'
		+ '<td class="text-center">';
		if (data.berkas_sk == '') {
			dispImage = 'display:none';
		} 
		row += '<a href="' + data.berkas_sk + '" data-popup="lightbox" class="prev_zoom" style="' + dispImage + '">'
		row += '<i class="fa fa-image"></i>'
		row += '</a>';
		if (data.id) {
			berkasValue = '';
		} else {
			berkasValue = data.berkas_sk;
		}
		row += '<input type="hidden" name="berkas_sk[' + indexRow + '][berkas]" class="berkas_sk" value="' + berkasValue + '">'
		+ '&nbsp;'
		+ '<a onclick="return BerkasSK.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		BerkasSK.table.append(row);
	},
	showForm() {
		BerkasSK.formBerkas.find('.tanggal').text(moment().format('DD/MM/YYYY'));
		$('.form-berkas').show();
		$('#berkas_sk_pengangkatan .btn-tambah').hide();
		$('#berkas_sk_pengangkatan .btn-simpan').show();
	},
	cancel() {
		BerkasSK.resetForm();
		$('.form-berkas').hide();
		$('#berkas_sk_pengangkatan .btn-simpan').hide();
		$('#berkas_sk_pengangkatan .btn-tambah').show();
	},
	resetTableBerkas() {
		$("#table_berkas_sk_pengangkatan tbody").html('');
		BerkasSK.resetDataDeleted();
	},
	showFormBerkas() {
		$('#modal_kelola_jabatan').modal('hide');
		$('#berkas_sk_pengangkatan').modal('show');
	},
	hideFormBerkas() {
		$('#berkas_sk_pengangkatan').modal('hide');
		$('#modal_kelola_jabatan').modal('show');
	}
};


$(function() {
	BerkasSK.formBerkas = $(".form-berkas");
	BerkasSK.table      = $("#table_berkas_sk_pengangkatan tbody");

	$('#berkas_sk_pengangkatan .btn-simpan').on('click', function() {
		let nama   = BerkasSK.formBerkas.find('#nama_berkas').val();

		if (nama == '') {
			swal({
		      title: 'Error!',
		      text: "Maaf, nama berkas harus diisi",
		      icon: 'error',
		    });
			return false;
		}

		if (BerkasSK.b64_sk == '') {
			swal({
		      title: 'Error!',
		      text: "Maaf, berkas SK harus diisi",
		      icon: 'error',
		    });
			return false;
		}

		BerkasSK.simpan();
		BerkasSK.resetForm();
		$('.form-berkas').hide();
		$('#berkas_sk_pengangkatan .btn-simpan').hide();
		$('#berkas_sk_pengangkatan .btn-tambah').show();
		return false;
	});

	$('#berkas_sk_pengangkatan btn-tutup').on('click', function() {
		BerkasSK.resetForm(form);
	});

	$("#berkas_sk").on('change', function(){
		BerkasSK.formBerkas.find('.btn-upload').html('<i class="fa fa-image"></i>');
		readURL(this, function(img) {
			console.log("img", img);
			if (img) {
				BerkasSK.b64_sk = img;
			}
		})
	});

	$("#btn_penfor_batal").click(function(){
		let form = BerkasSK.formBerkas;
		BerkasSK.resetForm(form);
	});
})

