$(function() {
	$('#keljab_tanggal_berlaku').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'DD/MM/YYYY'
    },
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

  $('#phk_tanggal_berlaku').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'DD/MM/YYYY'
    },
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

	$(".form-kelola-jabatan").validate({
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		submitHandler: function(form) {
			swal({
				title: "Apakah anda yakin?",
				text: "Sistem akan menyimpan data " + KelolaJabatan.jenis + ".",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				buttons: ["Tidak", "Ya"]
			})
			.then((confirm) => {
				if (confirm) {
					if (KelolaJabatan.jenis == 'PHK') { 
						let tglGabung = $("#phk_tanggal_berlaku").val();
						tglGabung = moment(tglGabung, 'DD/MM/YYYY').format('YYYY-MM-DD');
						$("#phk_tanggal_berlaku").val(tglGabung);
					} else {
						let tglGabung = $("#keljab_tanggal_berlaku").val();
						tglGabung = moment(tglGabung, 'DD/MM/YYYY').format('YYYY-MM-DD');
						$("#keljab_tanggal_berlaku").val(tglGabung);
					}
					$(".form-kelola-jabatan").loading();
					$(form).ajaxSubmit({
						dataType:  'json',
						success: function(res){
							if (res.status == 1) {
								icon = "success";
								title = "Berhasil"  
							} else {
								icon = "error";
								title = "Gagal!"
							}

							swal({
								title: title,
								text: res.message,
								icon: icon,
							}).then(function(e) {
								$('#modal_kelola_jabatan').modal('hide');
								KelolaJabatan.resetForm();
								karyawan.getKaryawan();
							});

							$(".form-kelola-jabatan").loading('stop');
						}
					});
				}
			});

			return false;
		},
		rules: {
			nik: {
				digits: true,
				maxlength: 16,
				minlength: 5,
			},
		},
		messages: {
			custom: {
				required: "This is a custom error message",
			},
			agree: "Please accept our policy"
		}
	});
});

let KelolaJabatan = {
	jenis: 'mutasi',
	title: 'Mutasi Karyawan',
	showModal(jenis) {
		KelolaJabatan.jenis = jenis;
		KelolaJabatan.getDataKaryawan();
		var str = KelolaJabatan.jenis;
		if (jenis == 'PHK') {
			$('#mutasi_unit_kerja_id').attr('required',false)
			$('#mutasi_jabatan_id').attr('required',false)
			$('#no_sk').attr('required',false).attr('disabled', true)
			$('#keljab_tanggal_berlaku').attr('required',false).attr('disabled', true)
			$('.kelola-jabatan').hide()

			$('#phk_no_sk').attr('required',true).attr('disabled', false)
			$('#phk_tanggal_berlaku').attr('required',true).attr('disabled', false)
			$('#phk_alasan').attr('required',true).attr('disabled', false)
			$('.phk').show()
		} else {
			str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
			  return letter.toUpperCase();
			});
			$('#phk_no_sk').attr('required',false).attr('disabled', true)
			$('#phk_tanggal_berlaku').attr('required',false).attr('disabled', true)
			$('#phk_alasan').attr('required',false).attr('disabled', true)
			$('.phk').hide()

			$('#mutasi_unit_kerja_id').attr('required',true)
			$('#mutasi_jabatan_id').attr('required',true)
			$('#no_sk').attr('required',true).attr('disabled', false)
			$('#keljab_tanggal_berlaku').attr('required',true).attr('disabled', false)
			$('.kelola-jabatan').show()
		}

		KelolaJabatan.title = str + ' Karyawan';
		$('#modal_kelola_jabatan .modal-title').text(KelolaJabatan.title);
		$('#modal_kelola_jabatan').find('input[name=jenis]').val(KelolaJabatan.jenis);
		
		$('#modal_kelola_jabatan').modal('show');
	},
	hideModal() {
		swal({
			title: "Apakah anda yakin ingin membatalkan?",
			text: KelolaJabatan.title,
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				KelolaJabatan.resetForm();
				$('#modal_kelola_jabatan').modal('hide');
			}
		});
	},
	resetForm() {
		$("#mutasi_unit_kerja_id").val('').trigger('change');
		$("#mutasi_jabatan_id").val('').trigger('change');
		$("#no_sk").val('');
		$("#keljab_tanggal_berlaku").val('');
		$("#phk_no_sk").val('');
		$("#phk_tanggal_berlaku").val('');
		$("#phk_alasan").val('');
		BerkasSK.resetTableBerkas();
	},
	getDataKaryawan() {
		let id  = $("input[name=id]").val();
		let nik  = $("#nik").val();
		let nama = $("#nama").val();
		let lama_unit_kerja_id = $("#unit_kerja_id").val();
        let lama_jabatan_id = $("#jabatan_id").val();
		let lama_unit_kerja = $("#unit_kerja_id option:selected").text();
        let lama_jabatan = $("#jabatan_id option:selected").text();

        $formKelolaJabatan = $(".form-kelola-jabatan");
        $formKelolaJabatan.find('input[name=karyawan_id]').val(id);
        $formKelolaJabatan.find('input[name=nama]').val(nama);
        $formKelolaJabatan.find('input[name=nik]').val(nik);
        $formKelolaJabatan.find('input[name=unit_kerja]').val(lama_unit_kerja);
        $formKelolaJabatan.find('input[name=jabatan]').val(lama_jabatan);
	},
}