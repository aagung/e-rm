$(function() {
  $('#tanggal_gabung').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'DD/MM/YYYY'
    },
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

  $("#berkas_pengangkatan").change(function() {
    readURL(this, function(imageUrl){
      $("#prev_sk").attr('src', imageUrl);
      $("#prev_sk_zoom").attr('href', imageUrl);
    });
  });

  $(".form-status-kepegawaian").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          let tglGabung = $("#tanggal_gabung").val();
          tglGabung = moment(tglGabung, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#tanggal_gabung").val(tglGabung);
          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                tglGabung = moment(tglGabung).format('DD/MM/YYYY');
                $("#tanggal_gabung").val(tglGabung);
                $("input[name=id]").val(res.data.id);
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  let uid = $("input[name=uid]").val();
                  if (uid == '') {
                    window.location = StatusKepegawaian.url.page;
                  } 
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var StatusKepegawaian = {
  url : {
    page : base_url + '/hrd/karyawan',
    store : api_url + '/store_status_kepegawaian'
  },
  getKaryawan() {

  }
}