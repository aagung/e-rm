$(function() {
	$('#resign_tanggal_berlaku').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'DD/MM/YYYY'
    },
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

	$(".form-resign").validate({
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		submitHandler: function(form) {
			swal({
				title: "Apakah anda yakin?",
				text: "Sistem akan menyimpan data " + Resign.jenis + ".",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				buttons: ["Tidak", "Ya"]
			})
			.then((confirm) => {
				if (confirm) {
					let tglBerlaku = $("#resign_tanggal_berlaku").val();
					tglBerlaku = moment(tglBerlaku, 'DD/MM/YYYY').format('YYYY-MM-DD');
					$("#resign_tanggal_berlaku").val(tglBerlaku);
					$(".form-resign").loading();
					$(form).ajaxSubmit({
						dataType:  'json',
						success: function(res){
							if (res.status == 1) {
								icon = "success";
								title = "Berhasil"  
							} else {
								icon = "error";
								title = "Gagal!"
							}

							swal({
								title: title,
								text: res.message,
								icon: icon,
							}).then(function(e) {
								$('#modal_resign').modal('hide');
								Resign.resetForm();
							});

							$(".form-resign").loading('stop');
						}
					});
				}
			});

			return false;
		},
		rules: {
			nik: {
				digits: true,
				maxlength: 16,
				minlength: 5,
			},
		},
		messages: {
			custom: {
				required: "This is a custom error message",
			},
			agree: "Please accept our policy"
		}
	});
});

let Resign = {
	jenis: 'resign',
	title: 'Resign Karyawan',
	showModal(jenis) {
		Resign.jenis = jenis;
		Resign.getDataKaryawan();
		var str = Resign.jenis;
		str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
		  return letter.toUpperCase();
		});

		Resign.title = str + ' Karyawan';
		$('#modal_resign .modal-title').text(Resign.title);
		$('#modal_resign').find('input[name=jenis]').val(Resign.jenis);
		
		$('#modal_resign').modal('show');
	},
	hideModal() {
		swal({
			title: "Apakah anda yakin ingin membatalkan?",
			text: Resign.title,
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				Resign.resetForm();
				$('#modal_resign').modal('hide');
			}
		});
	},
	resetForm() {
		$("#resign_tanggal_berlaku").val('');
		BerkasSK.resetTableBerkas();
	},
	getDataKaryawan() {
		let id  = $("input[name=id]").val();
		let nik  = $("#nik").val();
		let nama = $("#nama").val();
		let lama_unit_kerja_id = $("#unit_kerja_id").val();
        let lama_jabatan_id = $("#jabatan_id").val();
		let lama_unit_kerja = $("#unit_kerja_id option:selected").text();
        let lama_jabatan = $("#jabatan_id option:selected").text();

        $formResign = $(".form-resign");
        $formResign.find('input[name=karyawan_id]').val(id);
        $formResign.find('input[name=nama]').val(nama);
        $formResign.find('input[name=nik]').val(nik);
        $formResign.find('input[name=unit_kerja]').val(lama_unit_kerja);
        $formResign.find('input[name=jabatan]').val(lama_jabatan);
	},
}