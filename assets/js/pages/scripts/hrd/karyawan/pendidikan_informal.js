let PendidikanInformal = {
	b64_ijazah : '',
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	api_url    : base_url + '/api/hrd/pendidikan_informal',
	formPenFor : '',
	table 	   : '',
	resetForm(form) {
		PendidikanInformal.formPenFor.find('.btn-upload').text('Pilih File');
		PendidikanInformal.b64_ijazah = '';
		PendidikanInformal.stateEdit  = false;
		PendidikanInformal.rowEdit    = '';
		$('.configreset', form).click();
		$('select', form).trigger('change');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('input[name="pendidikan_informal_deleted"]').val('');
	},

	simpan(form) {
		let data = {
			id            : '',
			pendidikan_id : $(form).find('#non_pendidikan_id').val(),
			pendidikan    : $(form).find('#non_pendidikan_id option:selected').text(),
			institusi_id  : $(form).find('#non_institusi_id').val(),
			institusi     : $(form).find('#non_institusi_id option:selected').text(),
			jurusan_id    : $(form).find('#non_jurusan_id').val(),
			jurusan       : $(form).find('#non_jurusan_id option:selected').text(),
			no_ijazah     : $(form).find('#non_no_ijazah').val(),
			tahun_lulus   : $(form).find('#non_tahun_lulus').val(),
			foto_ijazah   : PendidikanInformal.b64_ijazah
		};

		if (data.jurusan_id == '') {
			data.jurusan = '---';
		}

		if (!PendidikanInformal.stateEdit) {
			PendidikanInformal.createRow(data);
		} else {
			$(PendidikanInformal.rowEdit).find('.pendidikan_id').val(data.pendidikan_id);
			$(PendidikanInformal.rowEdit).find('.pendidikan').text(data.pendidikan);
			$(PendidikanInformal.rowEdit).find('.institusi_id').val(data.institusi_id);
			$(PendidikanInformal.rowEdit).find('.institusi').text(data.institusi);
			$(PendidikanInformal.rowEdit).find('.jurusan_id').val(data.jurusan_id);
			$(PendidikanInformal.rowEdit).find('.jurusan').text(data.jurusan);
			$(PendidikanInformal.rowEdit).find('span.no_ijazah').text(data.no_ijazah);
			$(PendidikanInformal.rowEdit).find('span.tahun_lulus').text(data.tahun_lulus);
			$(PendidikanInformal.rowEdit).find('input.no_ijazah').val(data.no_ijazah);
			$(PendidikanInformal.rowEdit).find('input.tahun_lulus').val(data.tahun_lulus);

			if(data.foto_ijazah) {
				$(PendidikanInformal.rowEdit).find('.prev_zoom').attr('href', data.foto_ijazah).show();
				$(PendidikanInformal.rowEdit).find('.prev_image').attr('src', data.foto_ijazah).show();
			}
			
			$(PendidikanInformal.rowEdit).find('.foto_ijazah').val(data.foto_ijazah);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					PendidikanInformal.deleted.push(id);
					$('input[name="pendidikan_informal_deleted"]').val(JSON.stringify(PendidikanInformal.deleted));
				}

				$row.remove();
				swal("Data pendidikan berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},

	edit(el) {
		PendidikanInformal.stateEdit = true;
		PendidikanInformal.rowEdit = $(el).closest('tr');

		PendidikanInformal.formPenFor.find('#non_pendidikan_id').val($(PendidikanInformal.rowEdit).find('.pendidikan_id').val()).trigger('change');
		PendidikanInformal.formPenFor.find('#non_institusi_id').val($(PendidikanInformal.rowEdit).find('.institusi_id').val()).trigger('change');
		PendidikanInformal.formPenFor.find('#non_jurusan_id').val($(PendidikanInformal.rowEdit).find('.jurusan_id').val()).trigger('change');
		PendidikanInformal.formPenFor.find('#non_no_ijazah').val($(PendidikanInformal.rowEdit).find('input.no_ijazah').val());
		PendidikanInformal.formPenFor.find('#non_tahun_lulus').val($(PendidikanInformal.rowEdit).find('input.tahun_lulus').val());

		let foto_ijazah = $(PendidikanInformal.rowEdit).find('.foto_ijazah').val();
		if(foto_ijazah) {
			PendidikanInformal.b64_ijazah = foto_ijazah;
			PendidikanInformal.formPenFor.find('.btn-upload').text('Ubah File');
		}
		PendidikanInformal.formPenFor.modal('show');
	},

	get() {
		let uid = $('input[name="uid"]').val();
		$("#table_pendidikan_informal tbody").html('');
		$.get(PendidikanInformal.api_url + '/index/' + uid, function(res) {
			if (res.status == 1) {
				let data = res.data;
				for (var i = 0; i < data.length; i++) {
					PendidikanInformal.createRow(data[i]);
				}
			}
		}, "JSON");
	},

	createRow(data) {
		let maxRow = $("#table_pendidikan_informal tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		+ '<a onclick="return PendidikanInformal.edit(this)">'
		+ '<span class="pendidikan">' + data.pendidikan + '</span>'
		+ '</a>'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][pendidikan_id]" class="pendidikan_id" value="' + data.pendidikan_id + '">'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="institusi">' + data.institusi + '</span>'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][institusi_id]" class="institusi_id" value="' + data.institusi_id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="jurusan">' + data.jurusan + '</span>'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][jurusan_id]" class="jurusan_id" value="' + data.jurusan_id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="tahun_lulus">' + data.tahun_lulus + '</span>'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][tahun_lulus]" class="tahun_lulus" value="' + data.tahun_lulus + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="no_ijazah">' + data.no_ijazah + '</span>'
		+ '<input type="hidden" name="pendidikan_informal[' + indexRow + '][no_ijazah]" class="no_ijazah" value="' + data.no_ijazah + '">'
		+ '</td>'
		+ '<td>';
		if (data.foto_ijazah == '') {
			dispImage = 'display:none';
		} 
		row += '<a href="' + data.foto_ijazah + '" data-popup="lightbox" class="prev_zoom" style="' + dispImage + '">'
		row += '<img src="' + data.foto_ijazah + '" style="width:100%;' + dispImage + '" class="prev_image" >'
		row += '</a>';
		if (data.id) {
			fotoIjazahValue = '';
		} else {
			fotoIjazahValue = data.foto_ijazah;
		}
		row += '<input type="hidden" name="pendidikan_informal[' + indexRow + '][foto_ijazah]" class="foto_ijazah" value="' + fotoIjazahValue + '">'
		+ '</td>'
		+ '<td>'
		+ '<a onclick="return PendidikanInformal.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		PendidikanInformal.table.append(row);
	}
};


$(function() {
	PendidikanInformal.formPenFor = $("#modal_pendidikan_informal");
	PendidikanInformal.table      = $("#table_pendidikan_informal tbody");

	PendidikanInformal.get();
	$(".form-pendidikan-informal").validate({
		submitHandler: function(form) {
			PendidikanInformal.simpan(form);
			PendidikanInformal.resetForm(form);
			PendidikanInformal.formPenFor.modal('hide');

			return false;
		},
		rules: {
			tahun_lulus: {
        		digits: true,
        	},
        	no_ijazah: {
        		digits: true,
        	}
		}

	});

	$("#non_foto_ijazah").on('change', function(){
		PendidikanInformal.formPenFor.find('.btn-upload').text('Ubah File');
		readURL(this, function(img) {
			PendidikanInformal.b64_ijazah = img;
		})
	});

	$("#btn_peninfor_batal").click(function(){
		let form = PendidikanInformal.formPenFor;
		PendidikanInformal.resetForm(form);
	});
})

