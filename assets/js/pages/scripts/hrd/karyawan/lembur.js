let tableLembur = {};
$(function() {
  $petugas      = $('#petugas_id');
  $waktuMulai   = $('#waktu_mulai');
  $waktuSelesai = $('#waktu_selesai');
  $totalJam  = $('#total_jam');

  $waktuMulai.AnyTime_picker({
    format: "%H:%i"
  });

  $waktuSelesai.AnyTime_picker({
    format: "%H:%i"
  });

  $waktuSelesai.on('change', function () {
  	let mulai = $waktuMulai.val();
  	let selesai = $waktuSelesai.val();
  	let totalJam = diff(mulai, selesai);
  	$totalJam.val(totalJam)
  });

  let uid = $('input[name="uid"]').val();
  $tanggalLembur = $('#lembur_tanggal');
  $tanggalLembur.daterangepicker({
    singleDatePicker: true,
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });
  
  $petugas.on('change', function (el) {
    let karyawanUid = $(this).val();
    if (karyawanUid) {
	    $.ajax({
	      url: base_url + '/api/hrd/karyawan/get/' + karyawanUid,
	      type: 'GET',
	      dataType: 'JSON',
	      success: function (res) {
	        let data = res.data;
	        $('#penugas_level').val(data.level);
	        $('#penugas_unit_kerja').val(data.unit_kerja);
	      }
	    });
    } else {
    	$('#penugas_level').val('');
	    $('#penugas_unit_kerja').val('');
    }
  })

  tableLembur = $('#table_lembur').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": lembur.url.index + '/' + uid,
      "type": "POST",
    },
    "columns": [
      { 
        "data": "tanggal",
        "render": function(data, type, row) {
          return '<a href="#" onclick="return lembur.showDetail(' + row.id + ')">' + moment(data).format('DD/MM/YYYY') + '</a>';
        }, 
      },
      { 
        "data": "waktu_mulai",
        "render": function(data, type, row) {
          return (row.waktu_mulai + ' s/d ' + row.waktu_selesai);
        },
      },
      { "data": "penugas_lembur" },
      { "data": "uraian" },
      { 
        "data": "action",
        "className": "text-center",
        "sortable": false,
        "searchable": false,
        "render": function(data, type, row) {
          if (row.hasil_lembur == null) {
            return '<a href="#" onclick="return lembur.showDetail(' + row.id + ', true)">' + '<i class="icon icon-file-text2"></i>' + '</a>';
          } else {
            return '';
          }
        }, 
      }
    ]
  });

  $(".form-pengajuan-lembur").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          let tanggalLembur = $("#lembur_tanggal").val();
          tanggalLembur = moment(tanggalLembur, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#lembur_tanggal").val(tanggalLembur);
          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                tanggalLembur = moment(tanggalLembur).format('DD/MM/YYYY');
                $("#lembur_tanggal").val(tanggalLembur);
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('#petugas_id').val('').trigger('change');
                  $('#total_jam').val('');
                  $('#uraian').val('');
                  $('#lembur_tanggal').val(currentDate);
                  $('#waktu_mulai').val('');
                  $('#waktu_selesai').val('');
                  tableLembur.ajax.reload();
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });

  $(".form-hasil-lembur").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          $(".dialog-loading").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('#modal_lembur').modal('hide');
                  tableLembur.ajax.reload();
                }
              });

              $(".dialog-loading").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var lembur = {
  url : {
    index: base_url + '/api/hrd/lembur/index',
    delete : base_url + '/api/hrd/lembur/delete',
    form : base_url + '/hrd/lembur/form'
  },
  showDetail(id, hasil = false) {
    $('.penugas_nama').val('');
    $('.penugas_level').val('');
    $('.penugas_unit_kerja').val('');
    $('.lembur_tanggal').val('');
    $('.waktu_mulai').val('');
    $('.waktu_selesai').val('');
    $('.total_jam').val('');
    $('.uraian').val('');
    $('.hasil_total_jam').val('');
    $('.hasil_lembur').val('');
    
    $('.div-hasil-lembur').hide();
    $('.btn-submit-hasil').hide();
    if (hasil) {
      $('.btn-submit-hasil').show();
      $('.div-hasil-lembur').show();
      $('.div-hasil-lembur input, .div-hasil-lembur textarea').removeAttr('readonly')
      $('.div-hasil-lembur .hasil_total_jam').focus();
    } else {
      $('.div-hasil-lembur input, .div-hasil-lembur textarea').attr('readonly', true)
    }


    $(".dialog-loading").loading();
    $.ajax({
      url: base_url + '/api/hrd/lembur/get/' + id,
      type: 'GET',
      dataType: 'JSON',
      success: function (res) {
        let data = res.data;
        $('#lembur_id').val(data.id);
        $('.penugas_nama').val(data.penugas_nama);
        $('.penugas_level').val(data.penugas_level);
        $('.penugas_unit_kerja').val(data.penugas_unit_kerja);
        $('.lembur_tanggal').val(moment(data.lembur_tanggal).format('DD/MM/YYYY'));
        $('.waktu_mulai').val(data.waktu_mulai);
        $('.waktu_selesai').val(data.waktu_selesai);
        $('.total_jam').val(data.total_jam);
        $('.uraian').val(data.uraian);
        $('.hasil_total_jam').val(data.hasil_total_jam);
        $('.hasil_lembur').val(data.hasil_lembur);
        
        if (data.hasil_lembur != null) {
          $('.div-hasil-lembur').show()
        }

        $(".dialog-loading").loading('stop');
      }
    });
    $('#modal_lembur').modal('show');
    return false;
  },
  hideDetail() {
    $('#modal_lembur').modal('hide');
  },
  storeHasil() {
    $(".form-hasil-lembur").submit();
  }
}

function diff(start, end) {
	if (start && end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    
    return parseFloat(hours + minutes/60).toFixed(2);
	} else {
		return '';
	}
}