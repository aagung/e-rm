let Keluarga = {
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	api_url    : base_url + '/api/hrd/keluarga',
	formKeluarga : '',
	table 	   : '',
	resetForm(form) {
		Keluarga.stateEdit  = false;
		Keluarga.rowEdit    = '';
		$('.configreset', form).click();
		$('select', form).trigger('change');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('input[name="keluarga_deleted"]').val('');
	},

	dateSistem(tgl) {
		if (tgl) {
			tgl = moment(tgl, 'DD/MM/YYYY').format('YYYY-MM-DD');
			return tgl;
		} else {
			return '';
		}
	},

	dateIndo(tgl) {
		if (tgl) {
			tgl = moment(tgl, 'YYYY-MM-DD').format('DD/MM/YYYY');
			return tgl;
		} else {
			return '';
		}
	},

	simpan(form) {
		let tglLahir = $(form).find('#tanggal_lahir').val();
		let data = {
			id                 : '',
			nama               : $(form).find('#nama').val(),
			status_keluarga_id : $(form).find('#status_keluarga_id').val(),
			status_keluarga    : $(form).find('#status_keluarga_id option:selected').text(),
			tanggal_lahir      : this.dateSistem(tglLahir),
			jenis_kelamin_id   : $(form).find('#jenis_kelamin_id').val(),
			jenis_kelamin      : $(form).find('#jenis_kelamin_id option:selected').text(),
			pekerjaan_id       : $(form).find('#pekerjaan_id').val(),
			pekerjaan          : $(form).find('#pekerjaan_id option:selected').text(),
		};

		console.log("data", data);

		if (!Keluarga.stateEdit) {
			Keluarga.createRow(data);
		} else {
			$(Keluarga.rowEdit).find('input.nama').val(data.nama);
			$(Keluarga.rowEdit).find('span.nama').text(data.nama);

			$(Keluarga.rowEdit).find('input.status_keluarga_id').val(data.status_keluarga_id);
			$(Keluarga.rowEdit).find('span.status_keluarga_id').text(data.status_keluarga);
			
			$(Keluarga.rowEdit).find('input.tanggal_lahir').val(data.tanggal_lahir);
			$(Keluarga.rowEdit).find('span.tanggal_lahir').text(this.dateIndo(data.tanggal_lahir));
			
			$(Keluarga.rowEdit).find('input.jenis_kelamin_id').val(data.jenis_kelamin_id);
			$(Keluarga.rowEdit).find('span.jenis_kelamin_id').text(data.jenis_kelamin);
			
			$(Keluarga.rowEdit).find('input.pekerjaan_id').val(data.pekerjaan_id);
			$(Keluarga.rowEdit).find('span.pekerjaan_id').text(data.pekerjaan);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					Keluarga.deleted.push(id);
					$('input[name="keluarga_deleted"]').val(JSON.stringify(Keluarga.deleted));
				}

				$row.remove();
				swal("Data keluarga berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},

	edit(el) {
		Keluarga.stateEdit = true;
		Keluarga.rowEdit = $(el).closest('tr');

		Keluarga.formKeluarga.find('#nama').val($(Keluarga.rowEdit).find('input.nama').val());
		Keluarga.formKeluarga.find('#status_keluarga_id').val($(Keluarga.rowEdit).find('input.status_keluarga_id').val()).trigger('change');
		Keluarga.formKeluarga.find('#tanggal_lahir').val(this.dateIndo($(Keluarga.rowEdit).find('input.tanggal_lahir').val()));
		Keluarga.formKeluarga.find('#jenis_kelamin_id').val($(Keluarga.rowEdit).find('input.jenis_kelamin_id').val()).trigger('change');
		Keluarga.formKeluarga.find('#pekerjaan_id').val($(Keluarga.rowEdit).find('input.pekerjaan_id').val()).trigger('change');

		Keluarga.formKeluarga.modal('show');
	},

	get() {
		let uid = $('input[name="uid"]').val();
		$("#table_keluarga tbody").html('');
		$.get(Keluarga.api_url + '/index/' + uid, function(res) {
			if (res.status == 1) {
				let data = res.data;
				for (var i = 0; i < data.length; i++) {
					Keluarga.createRow(data[i]);
				}
			}
		}, "JSON");
	},

	createRow(data) {
		let maxRow = $("#table_keluarga tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		+ '<a onclick="return Keluarga.edit(this)">'
		+ '<span class="nama">' + data.nama + '</span>'
		+ '</a>'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][nama]" class="nama" value="' + data.nama + '">'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'

		+ '<td>'
		+ '<span class="status_keluarga_id">' + data.status_keluarga + '</span>'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][status_keluarga_id]" class="status_keluarga_id" value="' + data.status_keluarga_id + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="tanggal_lahir">' + this.dateIndo(data.tanggal_lahir) + '</span>'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][tanggal_lahir]" class="tanggal_lahir" value="' + data.tanggal_lahir + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="jenis_kelamin_id">' + data.jenis_kelamin + '</span>'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][jenis_kelamin_id]" class="jenis_kelamin_id" value="' + data.jenis_kelamin_id + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="pekerjaan_id">' + data.pekerjaan + '</span>'
		+ '<input type="hidden" name="keluarga[' + indexRow + '][pekerjaan_id]" class="pekerjaan_id" value="' + data.pekerjaan_id + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<a onclick="return Keluarga.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		Keluarga.table.append(row);
	}
};


$(function() {
	Keluarga.formKeluarga = $("#modal_keluarga");
	Keluarga.table         = $("#table_keluarga tbody");

	$('.tanggal-form').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		locale: {
			format: 'DD/MM/YYYY'
		},
		minYear: 1901,
		maxYear: parseInt(moment().format('YYYY'),10)
	});

	Keluarga.get();
	$(".form-keluarga").validate({
		submitHandler: function(form) {
			Keluarga.simpan(form);
			Keluarga.resetForm(form);
			Keluarga.formKeluarga.modal('hide');

			return false;
		},
		rules: {
			/*pekerjaan_id: {
        		digits: true,
        	},
        	jenis_kelamin_id: {
        		digits: true,
        	}*/
		}

	});

	$("#btn_keluarga_batal").click(function(){
		let form = Keluarga.formKeluarga;
		Keluarga.resetForm(form);
	});
})

