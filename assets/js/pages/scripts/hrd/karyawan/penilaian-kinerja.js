let tablePenilaianKinerja = {};
$(function() {
  let uid = $('input[name="uid"]').val();
  tablePenilaianKinerja = $('#table_penilaian_kinerja').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": PenilaianKinerja.url.index + '/' + uid,
      "type": "POST",
    },
    "columns": [
      { 
        "data": "created_at",
        "render": function(data, type, row) {
          return '<a href="#" onclick="return PenilaianKinerja.showDetail(' + row.id + ')">' + moment(data).format('DD/MM/YYYY') + '</a>';
        }, 
      },
      { "data": "periode_penilaian" },
      { "data": "nilai_mutu" },
      { "data": "angka_mutu" }
    ]
  });

  $(".kuantitatif input.nilai").on('change', function() {
    let nilai = $(this).val();
    $row      = $(this).closest('tr');
    let bobot = $row.find('.bobot').val();

    let nilai_bobot = parseFloat(nilai) * parseFloat(bobot / 100);
    /*Hasil*/
    $row.find('.nilai_bobot .value').val(nilai_bobot);
    $row.find('.nilai_bobot .disp').html(nilai_bobot);

    let penilaian = $(".kuantitatif table tbody tr");
    let subtotal = parseFloat(0);
    for (var i = 0; i < penilaian.length; i++) {
      let nilai_bobot = $(penilaian[i]).find('.nilai_bobot .value').val();
      if (nilai_bobot) {
        subtotal += parseFloat(nilai_bobot);
      }
    }
    $(".kuantitatif .subtotal .value").val(subtotal);
    $(".kuantitatif .subtotal .disp").html(subtotal);

    let bobot_kuantitatif = $(".total_nilai_bobot .bobot").val();
    let total_nilai_bobot = parseFloat(subtotal) * parseFloat(bobot_kuantitatif / 100);
    $(".kuantitatif .total_nilai_bobot .value").val(total_nilai_bobot);
    $(".kuantitatif .total_nilai_bobot .disp").html(total_nilai_bobot);

    let nilai_bobot_kualitatif = $(".kualitatif .total_nilai_bobot .value").val();
    let total_nilai = total_nilai_bobot + parseFloat(nilai_bobot_kualitatif);
    $("#total_nilai").val(total_nilai);
  });

  $(".kualitatif input.nilai").on('change', function() {
    let nilai = $(this).val();
    $row      = $(this).closest('tr');
    let bobot = $row.find('.bobot').val();

    let nilai_bobot = parseFloat(nilai) * parseFloat(bobot / 100);
    /*Hasil*/
    $row.find('.nilai_bobot .value').val(nilai_bobot);
    $row.find('.nilai_bobot .disp').html(nilai_bobot);

    let penilaian = $(".kualitatif table tbody tr");
    let subtotal = parseFloat(0);
    for (var i = 0; i < penilaian.length; i++) {
      let nilai_bobot = $(penilaian[i]).find('.nilai_bobot .value').val();
      if (nilai_bobot) {
        subtotal += parseFloat(nilai_bobot);
      }
    }
    $(".kualitatif .subtotal .value").val(subtotal);
    $(".kualitatif .subtotal .disp").html(subtotal);

    let bobot_kualitatif = $(".total_nilai_bobot .bobot").val();
    let total_nilai_bobot = parseFloat(subtotal) * parseFloat(bobot_kualitatif / 100);
    $(".kualitatif .total_nilai_bobot .value").val(total_nilai_bobot);
    $(".kualitatif .total_nilai_bobot .disp").html(total_nilai_bobot);

    let nilai_bobot_kuantitatif = $(".kuantitatif .total_nilai_bobot .value").val();
    let total_nilai = total_nilai_bobot + parseFloat(nilai_bobot_kuantitatif);
    $("#total_nilai").val(total_nilai);
  });

  $(".form-penilaian-kinerja").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          $(".dialog-loading").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }

              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  $('.nilai').val(0);
                  $('.catatan').val(0);
                  $('.nilai_bobot .value').val(0);
                  $('.nilai_bobot .disp').html(0);
                  $(".subtotal .value").val(0);
                  $(".subtotal .disp").html(0);

                  $(".total_nilai_bobot .value").val(0);
                  $(".total_nilai_bobot .disp").html(0);

                  $("#total_nilai").val(0);

                  $("#penilaian_terakhir").val('');
                  $("#periode_penilaian").val('');
                  $("#nilai_mutu").val('');
                  $("#angka_mutu").val('');
                  
                  $("#modal_penilaian_kinerja").modal('hide');
                  tablePenilaianKinerja.ajax.reload();
                }
              });

              $(".dialog-loading").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var PenilaianKinerja = {
  url : {
    index: base_url + '/api/hrd/penilaian_kinerja/index',
    delete : base_url + '/api/hrd/penilaian_kinerja/delete',
    form : base_url + '/hrd/penilaian_kinerja/form'
  },
  showDetail(id, hasil = false) {    
    $("#detail_penilaian_kinerja .dialog-loading").loading();
    $.ajax({
      url: base_url + '/api/hrd/penilaian_kinerja/get/' + id,
      type: 'GET',
      dataType: 'JSON',
      success: function (res) {
        let data = res.data;
        let penilaianKuantitatif = data.kuantitatif;
        let penilaianKualitatif = data.kualitatif;

        $tableKuantitatif = $("#detail_penilaian_kinerja .kuantitatif table tbody");
        $tableKuantitatif.html('');
        let totalBobotKuantitatif = 0;
        for (var i = 0; i < penilaianKuantitatif.length; i++) {
          let trData = penilaianKuantitatif[i];
          console.log('trData', trData)
          let row = '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + trData.faktor + '</td>' +
            '<td>' + trData.target + '</td>' +
            '<td>' + trData.nilai + '</td>' +
            '<td>' + trData.tase + '%</td>' +
            '<td>' + trData.bobot + '%</td>' +
            '<td>' + trData.nilai_bobot + '</td>' +
          '</tr>';
          totalBobotKuantitatif += parseFloat(trData.bobot);

          $tableKuantitatif.append(row);
        }

        $tableKualitatif = $("#detail_penilaian_kinerja .kualitatif table tbody");
        $tableKualitatif.html('');
        let totalBobotKualitatif = 0;
        for (var i = 0; i < penilaianKualitatif.length; i++) {
          let trData = penilaianKualitatif[i];
          console.log('trData', trData)
          let row = '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + trData.faktor + '</td>' +
            '<td>' + trData.nilai + '</td>' +
            '<td>' + trData.bobot + '%</td>' +
            '<td>' + trData.nilai_bobot + '</td>' +
            '<td>' + trData.catatan + '</td>' +
          '</tr>';
          totalBobotKualitatif += parseFloat(trData.bobot);

          $tableKualitatif.append(row);
        }

        $("#detail_penilaian_kinerja .kuantitatif .subtotal .disp").html(data.nilai_kuantitatif);
        $("#detail_penilaian_kinerja .kuantitatif .subtotal .bobot").html(totalBobotKuantitatif);
        $("#detail_penilaian_kinerja .kuantitatif .total_nilai_bobot .disp").html(data.nilai_bobot_kuantitatif);
        $("#detail_penilaian_kinerja .kuantitatif .total_nilai_bobot .bobot").html(data.bobot_kuantitatif);

        $("#detail_penilaian_kinerja .kualitatif .subtotal .disp").html(data.nilai_kualitatif);
        $("#detail_penilaian_kinerja .kualitatif .subtotal .bobot").html(100);
        $("#detail_penilaian_kinerja .kualitatif .total_nilai_bobot .disp").html(data.nilai_bobot_kualitatif);
        $("#detail_penilaian_kinerja .kualitatif .total_nilai_bobot .bobot").html(data.bobot_kualitatif);
        
        $("#detail_penilaian_kinerja .total_nilai").val(data.total_nilai);
        $("#detail_penilaian_kinerja .penilaian_terakhir").val(data.penilaian_terakhir);
        $("#detail_penilaian_kinerja .periode_penilaian").val(data.periode_penilaian);
        $("#detail_penilaian_kinerja .nilai_mutu").val(data.nilai_mutu);
        $("#detail_penilaian_kinerja .angka_mutu").val(data.angka_mutu);

        $("#detail_penilaian_kinerja .dialog-loading").loading('stop');
        $('#detail_penilaian_kinerja').modal('show');
      }
    });
    return false;
  },
  hideDetail() {
    $('#detail_penilaian_kinerja').modal('hide');
  },
  showModal() {
    $("#modal_penilaian_kinerja").modal('show');
  },
  storeHasil() {
    $(".form-penilaian-kinerja").submit();
  },
  batal() {
    $("#modal_penilaian_kinerja").modal('hide');
  }
}