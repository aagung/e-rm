let PendidikanFormal = {
	b64_ijazah : '',
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	api_url    : base_url + '/api/hrd/pendidikan_formal',
	formPenFor : '',
	table 	   : '',
	resetForm(form) {
		PendidikanFormal.formPenFor.find('.btn-upload').text('Pilih File');
		PendidikanFormal.b64_ijazah = '';
		PendidikanFormal.stateEdit  = false;
		PendidikanFormal.rowEdit    = '';
		$('.configreset', form).click();
		$('select', form).trigger('change');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('input[name="pendidikan_formal_deleted"]').val('');
	},

	simpan(form) {
		let data = {
			id            : '',
			pendidikan_id : $(form).find('#pendidikan_id').val(),
			pendidikan    : $(form).find('#pendidikan_id option:selected').text(),
			institusi_id  : $(form).find('#institusi_id').val(),
			institusi     : $(form).find('#institusi_id option:selected').text(),
			jurusan_id    : $(form).find('#jurusan_id').val(),
			jurusan       : $(form).find('#jurusan_id option:selected').text(),
			no_ijazah     : $(form).find('#no_ijazah').val(),
			tahun_lulus   : $(form).find('#tahun_lulus').val(),
			foto_ijazah   : PendidikanFormal.b64_ijazah
		};

		if (data.jurusan_id == '') {
			data.jurusan = '---';
		}

		if (!PendidikanFormal.stateEdit) {
			PendidikanFormal.createRow(data);
		} else {
			$(PendidikanFormal.rowEdit).find('.pendidikan_id').val(data.pendidikan_id);
			$(PendidikanFormal.rowEdit).find('.pendidikan').text(data.pendidikan);
			$(PendidikanFormal.rowEdit).find('.institusi_id').val(data.institusi_id);
			$(PendidikanFormal.rowEdit).find('.institusi').text(data.institusi);
			$(PendidikanFormal.rowEdit).find('.jurusan_id').val(data.jurusan_id);
			$(PendidikanFormal.rowEdit).find('.jurusan').text(data.jurusan);
			$(PendidikanFormal.rowEdit).find('span.no_ijazah').text(data.no_ijazah);
			$(PendidikanFormal.rowEdit).find('span.tahun_lulus').text(data.tahun_lulus);
			$(PendidikanFormal.rowEdit).find('input.no_ijazah').val(data.no_ijazah);
			$(PendidikanFormal.rowEdit).find('input.tahun_lulus').val(data.tahun_lulus);

			if(data.foto_ijazah) {
				$(PendidikanFormal.rowEdit).find('.prev_zoom').attr('href', data.foto_ijazah).show();
				$(PendidikanFormal.rowEdit).find('.prev_image').attr('src', data.foto_ijazah).show();
			}
			
			$(PendidikanFormal.rowEdit).find('.foto_ijazah').val(data.foto_ijazah);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					PendidikanFormal.deleted.push(id);
					$('input[name="pendidikan_formal_deleted"]').val(JSON.stringify(PendidikanFormal.deleted));
				}

				$row.remove();
				swal("Data pendidikan berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},

	edit(el) {
		PendidikanFormal.stateEdit = true;
		PendidikanFormal.rowEdit = $(el).closest('tr');

		PendidikanFormal.formPenFor.find('#pendidikan_id').val($(PendidikanFormal.rowEdit).find('.pendidikan_id').val()).trigger('change');
		PendidikanFormal.formPenFor.find('#institusi_id').val($(PendidikanFormal.rowEdit).find('.institusi_id').val()).trigger('change');
		PendidikanFormal.formPenFor.find('#jurusan_id').val($(PendidikanFormal.rowEdit).find('.jurusan_id').val()).trigger('change');
		PendidikanFormal.formPenFor.find('#no_ijazah').val($(PendidikanFormal.rowEdit).find('input.no_ijazah').val());
		PendidikanFormal.formPenFor.find('#tahun_lulus').val($(PendidikanFormal.rowEdit).find('input.tahun_lulus').val());

		let foto_ijazah = $(PendidikanFormal.rowEdit).find('.foto_ijazah').val();
		if(foto_ijazah) {
			PendidikanFormal.b64_ijazah = foto_ijazah;
			PendidikanFormal.formPenFor.find('.btn-upload').text('Ubah File');
		}
		PendidikanFormal.formPenFor.modal('show');
	},

	get() {
		let uid = $('input[name="uid"]').val();
		$("#table_pendidikan_formal tbody").html('');
		$.get(PendidikanFormal.api_url + '/index/' + uid, function(res) {
			if (res.status == 1) {
				let data = res.data;
				for (var i = 0; i < data.length; i++) {
					PendidikanFormal.createRow(data[i]);
				}
			}
		}, "JSON");
	},

	createRow(data) {
		let maxRow = $("#table_pendidikan_formal tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		+ '<a onclick="return PendidikanFormal.edit(this)">'
		+ '<span class="pendidikan">' + data.pendidikan + '</span>'
		+ '</a>'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][pendidikan_id]" class="pendidikan_id" value="' + data.pendidikan_id + '">'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="institusi">' + data.institusi + '</span>'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][institusi_id]" class="institusi_id" value="' + data.institusi_id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="jurusan">' + data.jurusan + '</span>'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][jurusan_id]" class="jurusan_id" value="' + data.jurusan_id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="tahun_lulus">' + data.tahun_lulus + '</span>'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][tahun_lulus]" class="tahun_lulus" value="' + data.tahun_lulus + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="no_ijazah">' + data.no_ijazah + '</span>'
		+ '<input type="hidden" name="pendidikan_formal[' + indexRow + '][no_ijazah]" class="no_ijazah" value="' + data.no_ijazah + '">'
		+ '</td>'
		+ '<td>';
		if (data.foto_ijazah == '') {
			dispImage = 'display:none';
		} 
		row += '<a href="' + data.foto_ijazah + '" data-popup="lightbox" class="prev_zoom" style="' + dispImage + '">'
		row += '<img src="' + data.foto_ijazah + '" style="width:100%;' + dispImage + '" class="prev_image" >'
		row += '</a>';
		if (data.id) {
			fotoIjazahValue = '';
		} else {
			fotoIjazahValue = data.foto_ijazah;
		}
		row += '<input type="hidden" name="pendidikan_formal[' + indexRow + '][foto_ijazah]" class="foto_ijazah" value="' + fotoIjazahValue + '">'
		+ '</td>'
		+ '<td>'
		+ '<a onclick="return PendidikanFormal.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		PendidikanFormal.table.append(row);
	}
};


$(function() {
	PendidikanFormal.formPenFor = $("#modal_pendidikan_formal");
	PendidikanFormal.table      = $("#table_pendidikan_formal tbody");

	PendidikanFormal.get();
	$(".form-pendidikan-formal").validate({
		submitHandler: function(form) {
			PendidikanFormal.simpan(form);
			PendidikanFormal.resetForm(form);
			PendidikanFormal.formPenFor.modal('hide');

			return false;
		},
		rules: {
			tahun_lulus: {
        		digits: true,
        	},
        	no_ijazah: {
        		digits: true,
        	}
		}

	});

	$("#foto_ijazah").on('change', function(){
		PendidikanFormal.formPenFor.find('.btn-upload').text('Ubah File');
		readURL(this, function(img) {
			PendidikanFormal.b64_ijazah = img;
		})
	});

	$("#btn_penfor_batal").click(function(){
		let form = PendidikanFormal.formPenFor;
		PendidikanFormal.resetForm(form);
	});
})

