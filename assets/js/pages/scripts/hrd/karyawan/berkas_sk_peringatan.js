let BerkasSKPeringatan = {
	b64_sk : '',
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	formBerkas : '',
	table 	   : '',
	resetForm() {
		BerkasSKPeringatan.formBerkas.find('.btn-upload').html('<i class="fa fa-upload"></i>');
		BerkasSKPeringatan.b64_sk     = '';
		BerkasSKPeringatan.stateEdit  = false;
		BerkasSKPeringatan.rowEdit    = '';
		BerkasSKPeringatan.formBerkas.find('#nama_berkas_peringatan').val('');
		BerkasSKPeringatan.formBerkas.find('#input_berkas_sk_peringatan').val('');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('#berkas_sk_peringatan input[name="berkas_sk_deleted"]').val('');
	},

	simpan() {
		let data = {
			id            : '',
			created_at    : moment().format('DD/MM/YYYY'),
			nama_berkas   : BerkasSKPeringatan.formBerkas.find('#nama_berkas_peringatan').val(),
			berkas_sk     : BerkasSKPeringatan.b64_sk
		};

		if (!BerkasSKPeringatan.stateEdit) {
			BerkasSKPeringatan.createRow(data);
		} else {
			$(BerkasSKPeringatan.rowEdit).find('#nama_berkas_peringatan').val(data.nama_berkas);

			if(data.berkas_sk) {
				$(BerkasSKPeringatan.rowEdit).find('.prev_zoom').attr('href', data.berkas_sk).show();
			}
			
			$(BerkasSKPeringatan.rowEdit).find('.berkas_sk').val(data.berkas_sk);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					BerkasSKPeringatan.deleted.push(id);
					$('input[name="berkas_sk_deleted"]').val(JSON.stringify(BerkasSKPeringatan.deleted));
				}

				$row.remove();
				swal("Data pendidikan berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},
	createRow(data) {
		let maxRow = $("#table_berkas_sk_peringatan tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		// + '<a onclick="return BerkasSKPeringatan.edit(this)">'
		+ '<span class="tanggal">' + data.created_at + '</span>'
		// + '</a>'
		+ '<input type="hidden" name="berkas_sk[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'
		+ '<td>'
		+ '<span class="nama_berkas">' + data.nama_berkas + '</span>'
		+ '<input type="hidden" name="berkas_sk[' + indexRow + '][nama]" class="nama_berkas" value="' + data.nama_berkas + '">'
		+ '</td>'
		+ '<td class="text-center">';
		if (data.berkas_sk == '') {
			dispImage = 'display:none';
		} 
		row += '<a href="' + data.berkas_sk + '" data-popup="lightbox" class="prev_zoom" style="' + dispImage + '">'
		row += '<i class="fa fa-image"></i>'
		row += '</a>';
		if (data.id) {
			berkasValue = '';
		} else {
			berkasValue = data.berkas_sk;
		}
		row += '<input type="hidden" name="berkas_sk[' + indexRow + '][berkas]" class="berkas_sk" value="' + berkasValue + '">'
		+ '&nbsp;'
		+ '<a onclick="return BerkasSKPeringatan.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		BerkasSKPeringatan.table.append(row);
	},
	showForm() {
		BerkasSKPeringatan.formBerkas.find('.tanggal').text(moment().format('DD/MM/YYYY'));
		$('.form-berkas-peringatan').show();
		$('#berkas_sk_peringatan .btn-tambah').hide();
		$('#berkas_sk_peringatan .btn-simpan').show();
	},
	cancel() {
		BerkasSKPeringatan.resetForm();
		$('.form-berkas-peringatan').hide();
		$('#berkas_sk_peringatan .btn-simpan').hide();
		$('#berkas_sk_peringatan .btn-tambah').show();
	},
	resetTableBerkas() {
		$("#table_berkas_sk_peringatan tbody").html('');
		BerkasSKPeringatan.resetDataDeleted();
	},
	showFormBerkas() {
		$('#berkas_sk_peringatan').modal('show');
	},
	hideFormBerkas() {
		$('#berkas_sk_peringatan').modal('hide');
	}
};


$(function() {
	BerkasSKPeringatan.formBerkas = $(".form-berkas-peringatan");
	BerkasSKPeringatan.table      = $("#table_berkas_sk_peringatan tbody");

	$('#berkas_sk_peringatan .btn-simpan').on('click', function() {
		let nama   = BerkasSKPeringatan.formBerkas.find('#nama_berkas_peringatan').val();

		if (nama == '') {
			swal({
		      title: 'Error!',
		      text: "Maaf, nama berkas harus diisi",
		      icon: 'error',
		    });
			return false;
		}

		if (BerkasSKPeringatan.b64_sk == '') {
			swal({
		      title: 'Error!',
		      text: "Maaf, berkas SK harus diisi",
		      icon: 'error',
		    });
			return false;
		}

		BerkasSKPeringatan.simpan();
		BerkasSKPeringatan.resetForm();
		$('.form-berkas-peringatan').hide();
		$('#berkas_sk_peringatan .btn-simpan').hide();
		$('#berkas_sk_peringatan .btn-tambah').show();
		return false;
	});

	$('#berkas_sk_peringatan btn-tutup').on('click', function() {
		BerkasSKPeringatan.resetForm(form);
	});

	$("#input_berkas_sk_peringatan").on('change', function(){
		BerkasSKPeringatan.formBerkas.find('.btn-upload').html('<i class="fa fa-image"></i>');
		readURL(this, function(img) {
			console.log("img", img);
			if (img) {
				BerkasSKPeringatan.b64_sk = img;
			}
		})
	});

	$("#btn_penfor_batal").click(function(){
		let form = BerkasSKPeringatan.formBerkas;
		BerkasSKPeringatan.resetForm(form);
	});
})

