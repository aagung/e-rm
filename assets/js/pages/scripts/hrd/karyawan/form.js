let api_url = base_url + '/api/hrd/karyawan';
let table = {};
let dataSearch = {};

$(function() {
  karyawan.getKaryawan();

  $('#provinsi_id').on('change', function() {
    let prov_id = $(this).val();
    karyawan.getKabupaten(prov_id);
  });

  $('select').on('change', function() {
    $(".form-validate-jquery").valid();
  });

  $('#nama').on('keyup', function() {
    $('#profile_nama').text($(this).val());
  });

  $('#nik').on('keyup', function() {
    $('#profile_nik').text($(this).val());
  });

  $('#tanggal_lahir').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'DD/MM/YYYY'
    },
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

  $("#foto_ktp").change(function() {
    readURL(this, function(imageUrl){
      $("#prev_ktp").attr('src', imageUrl);
      $("#prev_ktp_zoom").attr('href', imageUrl);
    });
  });

  $("#foto_profil").change(function() {
    readURL(this, function(imageUrl){
      $("#prev_profil").attr('src', imageUrl);
      $("#prev_profil_zoom").attr('href', imageUrl);
    });
  });

  $('[data-popup="lightbox"]').fancybox({
    padding: 3
  });

  $(".form-validate-jquery").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {
          let tglLahir = $("#tanggal_lahir").val();
          tglLahir = moment(tglLahir, 'DD/MM/YYYY').format('YYYY-MM-DD');
          $("#tanggal_lahir").val(tglLahir);
          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                tglLahir = moment(tglLahir).format('DD/MM/YYYY');
                $("#tanggal_lahir").val(tglLahir);
                $("input[name=id]").val(res.data.id);
                icon = "success";
                title = "Berhasil"  
              } else {
                icon = "error";
                title = "Gagal!"
              }
              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  let uid = $("input[name=uid]").val();
                  if (uid == '') {
                    window.location = karyawan.url.page;
                  } 
                }
              });

              PendidikanFormal.resetDataDeleted();
              PendidikanFormal.get();

              PendidikanInformal.resetDataDeleted();
              PendidikanInformal.get();

              Pekerjaan.resetDataDeleted();
              Pekerjaan.get();

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      nik: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
      no_ktp: {
        digits: true,
        maxlength: 16,
        minlength: 5,
      },
      no_hp: {
        digits: true,
        maxlength: 15,
        minlength: 10,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });

  var $sigdiv;
  $('.btn-simpan-ttd').click(function(){
    let data = $sigdiv.jSignature('getData');
    $('#prev_tandatangan').attr('src', data);
    $('#tanda_tangan').val(data);
    $('#modal_tandatangan').modal('hide');
    $('.btn-input-ttd').html('Ubah');
  })

  $('.btn-input-ttd').click(function(){
    $('#modal_tandatangan').modal('show');
  })

  $('#modal_tandatangan').on('shown.bs.modal', function (e) {
    if (typeof $sigdiv === "undefined") {
      $sigdiv = $("#signature").jSignature({color:"#333",lineWidth:2,width:'100%',height:200,'background-color':"#ddd"})
    }
    $sigdiv.jSignature('reset');
  })
});

var karyawan = {
  url : {
    page : base_url + '/hrd/karyawan',
    kabupaten : api_url + '/kabupaten',
    store : api_url + '/store',
    get : api_url + '/get'
  },
  getKaryawan() {
    let uid = $("input[name=uid]").val();
    if(uid) {
      $("#display-form").loading();
      $.get(this.url.get + '/' + uid, function(res) {
        if (res.status) {
          let row = res.data;
          karyawan.getKabupaten(row.provinsi_id,function(){
            $("#kabupaten_id").val(row.kabupaten_id).trigger('change');
          });
          $('#sisa_cuti').val(row.cuti);
          $("input[name=id]").val(row.id);
          $("input[name=cuti]").val(row.cuti);
          $("#nik").val(row.nik);
          $("#nama").val(row.nama);
          $("#no_ktp").val(row.no_ktp);
          $("#tempat_lahir").val(row.tempat_lahir);
          $("#tanggal_lahir").val(moment(row.tanggal_lahir).format('DD/MM/YYYY'));
          $("#jenis_kelamin_id").val(row.jenis_kelamin_id).trigger('change');
          $("#agama_id").val(row.agama_id).trigger('change');
          $("#nama_ayah").val(row.nama_ayah);
          $("#nama_ibu").val(row.nama_ibu);
          $("#status_kawin_id").val(row.status_kawin_id).trigger('change');
          $("#alamat_sekarang").val(row.alamat_sekarang);
          $("#alamat_tetap").val(row.alamat_tetap);
          $("#provinsi_id").val(row.provinsi_id).trigger('change');
          $("#no_hp").val(row.no_hp);
          if (row.foto_ktp) {
            $("#prev_ktp").attr('src', row.foto_ktp);
            $("#prev_ktp_zoom").attr('href', row.foto_ktp);
          }

          if (row.foto_profil) {
            $("#prev_profil").attr('src', row.foto_profil);
            $("#prev_profil_zoom").attr('href', row.foto_profil);
          }

          if (row.tanda_tangan) {
            console.log('row.tanda_tangan', row.tanda_tangan)
            $("#prev_tandatangan").attr('src', row.tanda_tangan);
            $('.btn-input-ttd').html('Ubah');
          }

          /*Form Status Kepegawaian*/
          $("#nik_status").val(row.nik);
          $("#unit_kerja_id").val(row.unit_kerja_id).trigger('change');
          $("#jabatan_id").val(row.jabatan_id).trigger('change');
          $("#status_karyawan").val(row.status_karyawan).trigger('change');
          if(row.tanggal_gabung) {
            $("#tanggal_gabung").val(moment(row.tanggal_gabung).format('DD/MM/YYYY'));
          }
          $("#sk_pengangkatan").val(row.sk_pengangkatan);
          $("#no_bpjs_kesehatan").val(row.no_bpjs_kesehatan);
          $("#no_bpjs_ketenagakerjaan").val(row.no_bpjs_ketenagakerjaan);
          $("#status_ptkp_pph21").val(row.status_ptkp_pph21).trigger('change');
          if (row.berkas_pengangkatan) {
            $("#prev_sk").attr('src', row.berkas_pengangkatan);
            $("#prev_sk_zoom").attr('href', row.berkas_pengangkatan);
          }
          
          $("#profile_nik").text(row.nik);
          $("#profile_nama").text(row.nama);

          $('.form-penilaian-kinerja input[name=karyawan_id]').val(row.id);
          $('.form-penilaian-kinerja .nama').val(row.nama);
          $('.form-penilaian-kinerja .unit_kerja').val(row.unit_kerja);
          $('.form-penilaian-kinerja .level').val(row.level);

        }
        $("#display-form").loading('stop'); 
      }, "json");
    }
  },
  getKabupaten(prov_id, cb) {
    $.get(this.url.kabupaten + '/' + prov_id, function(res) {
      if (res.status) {
        let htmlOpt = '';
        $('#kabupaten_id').attr("disabled", false);
        for (var i = 0; i < res.data.length; i++) {
          htmlOpt += '<option value="' + res.data[i].id + '">' + res.data[i].nama + '</option>';
        }
        $('#kabupaten_id').html(htmlOpt);

        if (cb) {
          return cb();
        }
      } 
    }, "json");
  }
}