let Pekerjaan = {
	rowEdit    : '',
	stateEdit  : false,
	deleted    : [],
	api_url    : base_url + '/api/hrd/pekerjaan',
	formPekerjaan : '',
	table 	   : '',
	resetForm(form) {
		Pekerjaan.stateEdit  = false;
		Pekerjaan.rowEdit    = '';
		$('.configreset', form).click();
		$('select', form).trigger('change');
	},

	resetDataDeleted() {
		this.deleted = [];
		$('input[name="pekerjaan_deleted"]').val('');
	},

	dateSistem(tgl) {
		if (tgl) {
			tgl = moment(tgl, 'DD/MM/YYYY').format('YYYY-MM-DD');
			return tgl;
		} else {
			return '';
		}
	},

	dateIndo(tgl) {
		if (tgl) {
			tgl = moment(tgl, 'YYYY-MM-DD').format('DD/MM/YYYY');
			return tgl;
		} else {
			return '';
		}
	},

	simpan(form) {
		let tglMasuk = $(form).find('#tanggal_masuk').val();
		console.log("tglMasuk", tglMasuk);
		let tglKeluar = $(form).find('#tanggal_keluar').val()
		console.log("tglKeluar", tglKeluar);
		let data = {
			id              : '',
			nama_perusahaan : $(form).find('#nama_perusahaan').val(),
			jabatan         : $(form).find('#jabatan').val(),
			tanggal_masuk   : this.dateSistem(tglMasuk),
			tanggal_keluar  : this.dateSistem(tglKeluar),
			alasan_keluar   : $(form).find('#alasan_keluar').val(),
		};

		console.log("data", data);

		if (!Pekerjaan.stateEdit) {
			Pekerjaan.createRow(data);
		} else {
			$(Pekerjaan.rowEdit).find('input.nama_perusahaan').val(data.nama_perusahaan);
			$(Pekerjaan.rowEdit).find('span.nama_perusahaan').text(data.nama_perusahaan);

			$(Pekerjaan.rowEdit).find('input.jabatan').val(data.jabatan);
			$(Pekerjaan.rowEdit).find('span.jabatan').text(data.jabatan);
			
			$(Pekerjaan.rowEdit).find('input.tanggal_masuk').val(data.tanggal_masuk);
			$(Pekerjaan.rowEdit).find('span.tanggal_masuk').text(this.dateIndo(data.tanggal_masuk));
			
			$(Pekerjaan.rowEdit).find('input.tanggal_keluar').val(data.tanggal_keluar);
			$(Pekerjaan.rowEdit).find('span.tanggal_keluar').text(this.dateIndo(data.tanggal_keluar));
			
			$(Pekerjaan.rowEdit).find('input.alasan_keluar').val(data.alasan_keluar);
			$(Pekerjaan.rowEdit).find('span.alasan_keluar').text(data.alasan_keluar);
		}
	},

	hapus(el) {
		swal({
			title: "Apakah anda yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Tidak", "Ya"]
		})
		.then((confirm) => {
			if (confirm) {
				$row = $(el).closest('tr');
				let id = $row.find('.id').val();
				if (id != '') {
					Pekerjaan.deleted.push(id);
					$('input[name="pekerjaan_deleted"]').val(JSON.stringify(Pekerjaan.deleted));
				}

				$row.remove();
				swal("Data pekerjaan berhasil dihapus.", {
					icon: "success",
				});
			}
		});
	},

	edit(el) {
		Pekerjaan.stateEdit = true;
		Pekerjaan.rowEdit = $(el).closest('tr');

		Pekerjaan.formPekerjaan.find('#nama_perusahaan').val($(Pekerjaan.rowEdit).find('input.nama_perusahaan').val());
		Pekerjaan.formPekerjaan.find('#jabatan').val($(Pekerjaan.rowEdit).find('input.jabatan').val());
		Pekerjaan.formPekerjaan.find('#tanggal_masuk').val(this.dateIndo($(Pekerjaan.rowEdit).find('input.tanggal_masuk').val()));
		Pekerjaan.formPekerjaan.find('#tanggal_keluar').val(this.dateIndo($(Pekerjaan.rowEdit).find('input.tanggal_keluar').val()));
		Pekerjaan.formPekerjaan.find('#alasan_keluar').val($(Pekerjaan.rowEdit).find('input.alasan_keluar').val());

		Pekerjaan.formPekerjaan.modal('show');
	},

	get() {
		let uid = $('input[name="uid"]').val();
		$("#table_pekerjaan tbody").html('');
		$.get(Pekerjaan.api_url + '/index/' + uid, function(res) {
			if (res.status == 1) {
				let data = res.data;
				for (var i = 0; i < data.length; i++) {
					Pekerjaan.createRow(data[i]);
				}
			}
		}, "JSON");
	},

	createRow(data) {
		let maxRow = $("#table_pekerjaan tbody").find('tr').length;
		let indexRow = maxRow + 1;
		let dispImage = '';
		let row = '<tr>'
		+ '<td>'
		+ '<a onclick="return Pekerjaan.edit(this)">'
		+ '<span class="nama_perusahaan">' + data.nama_perusahaan + '</span>'
		+ '</a>'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][nama_perusahaan]" class="nama_perusahaan" value="' + data.nama_perusahaan + '">'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][id]" class="id" value="' + data.id + '">'
		+ '</td>'

		+ '<td>'
		+ '<span class="jabatan">' + data.jabatan + '</span>'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][jabatan]" class="jabatan" value="' + data.jabatan + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="tanggal_masuk">' + this.dateIndo(data.tanggal_masuk) + '</span>'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][tanggal_masuk]" class="tanggal_masuk" value="' + data.tanggal_masuk + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="tanggal_keluar">' + this.dateIndo(data.tanggal_keluar) + '</span>'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][tanggal_keluar]" class="tanggal_keluar" value="' + data.tanggal_keluar + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<span class="alasan_keluar">' + data.alasan_keluar + '</span>'
		+ '<input type="hidden" name="pekerjaan[' + indexRow + '][alasan_keluar]" class="alasan_keluar" value="' + data.alasan_keluar + '">'
		+ '</td>'
		
		+ '<td>'
		+ '<a onclick="return Pekerjaan.hapus(this)"><i class="fa fa-trash text-danger"></i></a>'
		+ '</td>'
		+ '</tr>';

		Pekerjaan.table.append(row);
	}
};


$(function() {
	Pekerjaan.formPekerjaan = $("#modal_pekerjaan");
	Pekerjaan.table         = $("#table_pekerjaan tbody");

	$('.tanggal-form').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		locale: {
			format: 'DD/MM/YYYY'
		},
		minYear: 1901,
		maxYear: parseInt(moment().format('YYYY'),10)
	});

	Pekerjaan.get();
	$(".form-pekerjaan").validate({
		submitHandler: function(form) {
			Pekerjaan.simpan(form);
			Pekerjaan.resetForm(form);
			Pekerjaan.formPekerjaan.modal('hide');

			return false;
		},
		rules: {
			/*alasan_keluar: {
        		digits: true,
        	},
        	tanggal_keluar: {
        		digits: true,
        	}*/
		}

	});

	$("#btn_pekerjaan_batal").click(function(){
		let form = Pekerjaan.formPekerjaan;
		Pekerjaan.resetForm(form);
	});
})

