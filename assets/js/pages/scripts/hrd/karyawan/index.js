let api_url = base_url + '/api/hrd/karyawan';
let table = {};
let dataSearch = {};

$(function() {
  $tgl = $('#tgl_gabung');
  $unitKerja = $('#unit_kerja');

  $tgl.daterangepicker({
    "applyClass": 'bg-slate-600',
    "cancelClass": 'btn-default',
    "locale": {
      "format": 'DD/MM/YYYY'
    },
  });

  $tgl.on('apply.daterangepicker', function(ev, picker) {
    dataSearch.tanggal_awal  = picker.startDate.format('YYYY-MM-DD');
    dataSearch.tanggal_akhir = picker.endDate.format('YYYY-MM-DD');
    table.ajax.reload();
  });

  $unitKerja.on('change', function() {
    let unit_kerja = $(this).val();
    dataSearch.unit_kerja_id = unit_kerja;
    table.ajax.reload();
  });

  table = $('#table_karyawan').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": api_url,
      "type": "POST",
      "data": (d) => {
        d.tanggal_awal  = dataSearch.tanggal_awal;
        d.tanggal_akhir = dataSearch.tanggal_akhir;
        d.unit_kerja_id = dataSearch.unit_kerja_id;
      }
    },
    "columns": [
    {
      "data": "nik",
      "render": function(data, type, row) {
        return '<a href="' + karyawan.url.form + '/' + row.uid + '">' + data + '</a>';
      },
    },
    { "data": "nama" },
    {
      "data": "tanggal_gabung",
      "render": function(data, type, row) {
        return moment(data).format('DD/MM/YYYY');
      },
    },
    { "data": "unit_kerja" },
    { "data": "jabatan" },
    {
      "data": "action",
      "className": "text-center",
      "sortable": false,
      "searchable": false,
      "render": function(data, type, row) {
        return '<a class="text-danger" href="' + karyawan.url.delete + '/' + row.uid + '" onclick="return karyawan.delete(this)"><i class="fa fa-trash"></i></a>';
      },
    }
    ]
  } );
});

var karyawan = {
  url : {
    delete : api_url + '/delete',
    form : base_url + '/hrd/karyawan/form'
  },
  delete(el) {
    let url = $(el).attr('href');
    swal({
      title: "Apakah anda yakin?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      buttons: ["Tidak", "Ya"]
    })
    .then((confirm) => {
      if (confirm) {
        $.get(url, function(res) {
          swal("Data karyawan berhasil dihapus.", {
            icon: "success",
          });
          table.ajax.reload();
        }, "json");
      }
    });
    return false;
  }
}