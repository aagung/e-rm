let api_url = base_url + '/api/asset/kategori';
let table = {};
let dataSearch = {};

$(function() {
    groups.getGroup();

    $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        submitHandler: function(form) {
            swal({
                    title: "Apakah anda yakin?",
                    text: "Sistem akan menyimpan data ini.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    buttons: ["Tidak", "Ya"]
                })
                .then((confirm) => {
                    if (confirm) {

                        $("#display-form").loading();
                        $(form).ajaxSubmit({
                            dataType: 'json',
                            success: function(res) {
                                if (res.status == 1) {
                                    $("input[name=id]").val(res.data.id);
                                    icon = "success";
                                    title = "Berhasil"
                                } else {
                                    icon = "error";
                                    title = "Gagal!"
                                }
                                swal({
                                    title: title,
                                    text: res.message,
                                    icon: icon,
                                }).then(function(e) {
                                    if (title == "Berhasil") {
                                        let uid = $("input[name=uid]").val();
                                        if (uid == '') {
                                            window.location = groups.url.page;
                                        }
                                    }
                                });

                                $("#display-form").loading('stop');
                            }
                        });
                    }
                });

            return false;
        },
        rules: {
            group_asset: {
                digits: false,
                maxlength: 5,
                minlength: 1,
            },
            kode: {
                digits: false,
                maxlength: 255,
                minlength: 2,
            },
            nama: {
                digits: false,
                maxlength: 50,
                minlength: 3,
            },
            umur_ekonomis: {
                digits: true,
                maxlength: 2,
                minlength: 1,
            },
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });

    $('#group_asset').on('change', function() {

        let group_id = $(this).val();

        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?group=' + group_id;
            window.history.pushState({ path: newurl }, '', newurl);
        }

        window.location.reload(newurl);

    });

});

var groups = {
    url: {
        page: base_url + '/asset/kategori',
        get: api_url + '/get'
    },
    getGroup() {
        let uid = $("input[name=uid]").val();
        if (uid) {
            $("#display-form").loading();
            $.get(this.url.get + '/' + uid, function(res) {
                if (res.status) {
                    let row = res.data;

                    $("#group_label").text(row.group_nama);
                    $("input[name=id]").val(row.id);
                    $("input[name=group_id]").val(row.group_id);
                    $("#group_asset").val(row.group_uid);
                    $("#kode").val(row.kode);
                    $("#nama").val(row.nama);
                    $("#umur_ekonomis").val(row.umur_ekonomis);
                    $("#notes").val(row.notes);


                }
                $("#display-form").loading('stop');
            }, "json");
        }
    }
}