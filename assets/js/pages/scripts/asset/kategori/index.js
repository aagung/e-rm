let api_url = base_url + '/api/asset/kategori';
let dataSearch = {};

$(function() {
    table = $('#table_kategori').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": api_url,
            "type": "POST",
            "data": (d) => {
                d.group_uid = dataSearch.group_uid;
            }
        },
        "columns": [{
                "data": "kode",
                "render": function(data, type, row) {
                    return '<a href="' + kategori.url.form + '/' + row.uid + '">' + data + '</a>';
                },
            },
            { "data": "group" },
            { "data": "nama" },
            { "data": "umur_ekonomis" },
            { "data": "notes" },
            {
                "data": "uid",
                "className": "text-center",
                "sortable": false,
                "searchable": false,
                "render": function(data, type, row) {
                    return '<a class="text-danger" href="' + kategori.url.delete + '/' + row.uid + '" onclick="return kategori.delete(this)"><i class="fa fa-trash"></i></a>';
                },
            }
        ]
    });

    $('#group_asset').on('change', function() {

        let group_id = $(this).val();

        dataSearch.group_uid = group_id;

        table.ajax.reload();

        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?group=' + group_id;
            var formUrls = kategori.url.form + '?group=' + group_id;
            window.history.pushState({ path: newurl }, '', newurl);
        }

        $('#add_kategori').attr('href', formUrls);

    }).trigger('change');

});

var kategori = {
    url: {
        delete: api_url + '/delete',
        form: base_url + '/asset/kategori/form'
    },
    delete(el) {
        let url = $(el).attr('href');
        swal({
                title: "Apakah anda yakin?",
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Tidak", "Ya"]
            })
            .then((confirm) => {
                if (confirm) {
                    $.get(url, function(res) {
                        swal("Data kategori berhasil dihapus.", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    }, "json");
                }
            });
        return false;
    }
}