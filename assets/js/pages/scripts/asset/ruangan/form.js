let api_url = base_url + '/api/asset/ruangan';
let table = {};
let dataSearch = {};

$(function() {
  ruangan.getRuangan();

  $(".form-validate-jquery").validate({
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    submitHandler: function(form) {
      swal({
        title: "Apakah anda yakin?",
        text: "Sistem akan menyimpan data ini.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Tidak", "Ya"]
      })
      .then((confirm) => {
        if (confirm) {

          $("#display-form").loading();
          $(form).ajaxSubmit({
            dataType:  'json',
            success: function(res){
              if (res.status == 1) {
                $("input[name=id]").val(res.data.id);
                icon = "success";
                title = "Berhasil"
              } else {
                icon = "error";
                title = "Gagal!"
              }
              swal({
                title: title,
                text: res.message,
                icon: icon,
              }).then(function(e) {
                if (title == "Berhasil") {
                  let uid = $("input[name=uid]").val();
                  if (uid == '') {
                    window.location = ruangan.url.page;
                  }
                }
              });

              $("#display-form").loading('stop');
            }
          });
        }
      });

      return false;
    },
    rules: {
      kode: {
        digits: false,
        maxlength: 255,
        minlength: 2,
      },
      nama: {
        digits: false,
        maxlength: 16,
        minlength: 3,
      },
    },
    messages: {
      custom: {
        required: "This is a custom error message",
      },
      agree: "Please accept our policy"
    }
  });
});

var ruangan = {
  url : {
    page: base_url + '/asset/ruangan',
    get : api_url + '/get'
  },
  getRuangan() {
    let uid = $("input[name=uid]").val();
    if(uid) {
      $("#display-form").loading();
      $.get(this.url.get + '/' + uid, function(res) {
        if (res.status) {
          let row = res.data;

          $("input[name=id]").val(row.id);
          $("#kode").val(row.kode);
          $("#nama").val(row.nama);
          $("#notes").val(row.notes);


        }
        $("#display-form").loading('stop');
      }, "json");
    }
  }
}