let api_url = base_url + '/api/asset/status';


$(function() {
  table = $('#table_status').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": api_url,
      "type": "POST"
    },
    "columns": [
    {
      "data": "kode",
    },
    { "data": "nama" },
    { "data": "notes" },
    {
      "data": "uid",
      "className": "text-center",
      "sortable": false,
      "searchable": false,
      "render": function(data, type, row) {
        return '<a class="text-primary" href="' + group.url.form + '/' + row.uid + '"><i class="fa fa-edit"></i></a >&nbsp;<a class="text-danger" href="' + group.url.delete + '/' + row.uid + '" onclick="return group.delete(this)"><i class="fa fa-trash"></i></a>';
      },
    }
    ]
  } );
});

var group = {
  url : {
    delete : api_url + '/delete',
    form : base_url + '/asset/status/form',
  },
  delete(el) {
    let url = $(el).attr('href');
    swal({
      title: "Apakah anda yakin?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      buttons: ["Tidak", "Ya"]
    })
    .then((confirm) => {
      if (confirm) {
        $.get(url, function(res) {
          swal("Data status berhasil dihapus.", {
            icon: "success",
          });
          table.ajax.reload();
        }, "json");
      }
    });
    return false;
  }
}