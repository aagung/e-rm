let api_url = base_url + '/api/asset/group';


$(function() {
    table = $('#table_group').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": api_url,
            "type": "POST"
        },
        "columns": [{
                "data": "kode",
                "render": function(data, type, row) {
                    return '<a href="' + group.url.kategori_list + '/?group=' + row.uid + '">' + data + '</a>';
                },
            },
            { "data": "nama" },
            { "data": "notes" },
            {
                "data": "uid",
                "className": "text-center",
                "sortable": false,
                "searchable": false,
                "render": function(data, type, row) {
                    return '<a class="text-primary" href="' + group.url.form + '/' + row.uid + '"><i class="fa fa-edit"></i></a >&nbsp;<a class="text-danger" href="' + group.url.delete + '/' + row.uid + '" onclick="return group.delete(this)"><i class="fa fa-trash"></i></a>';
                },
            }
        ]
    });
});

var group = {
    url: {
        delete: api_url + '/delete',
        form: base_url + '/asset/group/form',
        kategori_list: base_url + '/asset/kategori/index'
    },
    delete(el) {
        let url = $(el).attr('href');
        swal({
                title: "Apakah anda yakin?",
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Tidak", "Ya"]
            })
            .then((confirm) => {
                if (confirm) {
                    $.get(url, function(res) {
                        swal("Data group berhasil dihapus.", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    }, "json");
                }
            });
        return false;
    }
}