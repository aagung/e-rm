function readURL(input, cb) {
  let valid = validateFileType(input);
  if (valid) { 
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        cb(e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }
}

function validateFileType(input){
  var fileName = $(input).val();
  if (fileName) {
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        return true;
    }else{
      swal({
        title: 'Error!',
        text: "Maaf, Hanya untuk gambar dengan tipe jpg/jpeg dan png!",
        icon: 'error',
      });
      return false;
    }   
  } else {
    return true;
  }
}

function tabActive(el, contentId) {
  let uid = $("input[name=uid]").val();
  if (uid) {
    $('.tab-content').hide();
    $('.' + contentId).show();

    $('.nav-form .list-group-item').removeClass('active');
    $(el).addClass('active');
  } else {
    swal({
      title: 'Peringatan',
      text: 'Maaf anda harus mengisi biodata terlebih dahulu!',
      icon: 'error',
    });
  }
  return false;
}