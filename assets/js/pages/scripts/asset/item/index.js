let api_url = base_url + '/api/asset/item';
let dataSearch = {};

$(function() {

    $('.disp_tanggal').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        max: moment().format('dd/mm/yyyy')
    });

    table = $('#table_barang').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": api_url,
            "type": "POST",
            "data": (d) => {
                d.kode_asset = dataSearch.kode_asset;
                d.nama_asset = dataSearch.nama_asset;
                d.group_asset = dataSearch.group_asset;
                d.tgl_perolehan_asset = dataSearch.tgl_perolehan_asset;
                d.status_asset = dataSearch.status_asset;
            }

        },
        "columns": [{
                "data": "kode",
                "render": function(data, type, row) {
                    return '<a class="text-primary" href="' + barang.url.view + '/' + row.uid + '">' + data + '</a >';
                }
            },
            {
                "data": "nama"
            },
            { "data": "qty" },
            { "data": "group" },
            { "data": "kategori" },
            { "data": "tgl_perolehan" },
            { "data": "status_asset" },
            { "data": "nilai" },
            {
                "data": "uid",
                "className": "text-center",
                "sortable": false,
                "searchable": false,
                "render": function(data, type, row) {
                    return '<a class="text-primary" href="' + barang.url.form + '/null/' + row.uid + '"><i class="fa fa-edit"></i></a >&nbsp;<a class="text-danger" href="' + barang.url.delete + '/' + row.uid + '" onclick="return barang.delete(this)"><i class="fa fa-trash"></i></a>';
                },
            }
        ]
    });

    $('#button_filter').on('click', function(e) {
        do_filter();
        e.preventDefault;
    }).trigger('click');

    $('#button_reset').on('click', function(e) {
        $('#kode_asset').val('');
        $('#nama_asset').val('');
        $('#group_asset').val('');
        $('#tgl_perolehan_asset').val('');
        $('#status_asset').val('');

        do_filter();

        e.preventDefault;
    }).trigger('click');
});

function do_filter() {
    dataSearch.kode_asset = $('#kode_asset').val();
    dataSearch.nama_asset = $('#nama_asset').val();
    dataSearch.group_asset = $('#group_asset').val();
    dataSearch.tgl_perolehan_asset = $('#tgl_perolehan_asset').val();
    dataSearch.status_asset = $('#status_asset').val();

    console.log(dataSearch);
    table.ajax.reload();
}

var barang = {
    url: {
        delete: api_url + '/delete',
        form: base_url + '/asset/item/form',
        view: base_url + '/asset/item/view'
    },
    delete(el) {
        let url = $(el).attr('href');
        swal({
                title: "Apakah anda yakin?",
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Tidak", "Ya"]
            })
            .then((confirm) => {
                if (confirm) {
                    $.get(url, function(res) {
                        swal("Data barang berhasil dihapus.", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    }, "json");
                }
            });
        return false;
    }
}