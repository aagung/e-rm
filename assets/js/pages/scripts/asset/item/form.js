let api_url = base_url + '/api/asset/item';
let table = {};
let dataSearch = {};

$(function() {
    item.getItem();
    item.getPermintaan();

    $("input[type=file]").on("change", function() {
        $("[for=file]").html(this.files[0].name);
        $("#myImage").attr("src", URL.createObjectURL(this.files[0]));
    })

    $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        submitHandler: function(form) {
            swal({
                    title: "Apakah anda yakin?",
                    text: "Sistem akan menyimpan data ini.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    buttons: ["Tidak", "Ya"]
                })
                .then((confirm) => {
                    if (confirm) {

                        $("#display-form").loading();
                        $(form).ajaxSubmit({
                            dataType: 'json',
                            success: function(res) {
                                if (res.status == 1) {
                                    $("input[name=id]").val(res.data.id);
                                    icon = "success";
                                    title = "Berhasil"
                                } else {
                                    icon = "error";
                                    title = "Gagal!"
                                }
                                swal({
                                    title: title,
                                    text: res.message,
                                    icon: icon,
                                }).then(function(e) {
                                    if (title == "Berhasil") {
                                        let uid = $("input[name=uid]").val();
                                        if (uid == '') {
                                            window.location = item.url.page;
                                        }
                                    }
                                });

                                $("#display-form").loading('stop');
                            }
                        });
                    }
                });

            return false;
        },
        rules: {
            kode: {
                digits: false,
                maxlength: 100,
                minlength: 2,
            },
            nama: {
                digits: false,
                maxlength: 100,
                minlength: 3,
            },
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });
});

var item = {
    url: {
        page: base_url + '/asset/item',
        get: api_url + '/get',
        permintaan: base_url + '/api/asset/permintaan',
    },
    getItem() {
        let uid = $("input[name=uid]").val();
        if (uid) {
            $("#display-form").loading();
            $.get(this.url.get + '/' + uid, function(res) {
                if (res.status) {
                    $('.disp_num').autoNumeric('init', { aSep: '.', aDec: ',', mDec: '2' });



                    let row = res.data;
                    let tgl_perolehan = moment(row.tgl_perolehan, 'Y-m-d').format('DD/MM/YYYY');
                    $("input[name=id]").val(row.id);
                    $("#kode").val(row.kode);
                    $("#kode").text(row.kode);
                    $("#nama").val(row.nama);
                    $("#nama").text(row.nama);
                    $("#kategori_id").val(row.kategori_id);
                    $("#merk").val(row.merk);
                    $("#no_seri").val(row.no_seri);
                    $("#notes").val(row.notes);
                    // $("#userfile").val(row.image);
                    $('#myImage').attr('src', row.image);
                    $("#distributor_id").val(row.distributor_id);
                    $("#ruangan_id").val(row.ruangan_id);

                    $("#status_barang").val(row.status_barang);
                    $("#qty").val(row.qty);
                    $("#tgl_perolehan").val(tgl_perolehan);
                    $("#harga_perolehan").val(row.harga_perolehan);
                    $(".input_num").val(row.harga_perolehan);
                    $("#tahun_umur_ekonomis").val(row.tahun_umur_ekonomis);

                    $("#nilai_barang").val(row.nilai_barang);

                    $('.disp_num').each(function() {
                        var no = numeral($(this).val()).format('0,0');
                        $(this).val(no)
                        console.log(no);
                    });

                    $('.aksi-btn').attr("data-raw", JSON.stringify(row));
                }
                $("#display-form").loading('stop');
            }, "json");
        }
    },
    getPermintaan() {
        let permintaan_uid = $("input[name=permintaan_uid]").val();
        if (permintaan_uid) {
            $("#display-form").loading();
            $.get(this.url.permintaan + '/get/' + permintaan_uid, function(res) {
                if (res.status) {
                    let row = res.data;
                    $("#nama").val(row.nama);
                    var timeMinta = moment(row.tgl_permintaan).format('DD/MM/YYYY')
                    $("#tgl_permintaan").val(timeMinta);
                    $("#qty").val(row.unit);
                    $("#kategori_id").val(row.kategori_id);
                    $("#merk").val(row.merk);
                    $("#no_seri").val(row.no_seri);
                    $("#deskripsi").val(row.deskripsi);
                    $("#notes").val(row.notes);
                }
                $("#display-form").loading('stop');
            }, "json");
        }
    }
}