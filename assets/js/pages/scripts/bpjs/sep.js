(($) => {
    $.fn.createSep = function (opts) {
        let options = $.extend({}, $.fn.createSep.defaultOpts, opts);

        let instance = this.data('create.sep');

        if (instance != null) {
            throw Error("can't reinitialize create SEP.");
        }

        // Check Input
        if (options.inputSep.length == 0) {
            throw Error(`${options.inputSep.selector} can't be null`);
        }
        if (options.inputNoKartu.length == 0) {
            throw Error(`${options.inputNoKartu.selector} can't be null`);
        }

        // Check URL
        if (options.url.getPeserta == null || options.url.getPeserta == "") {
            throw Error(`Create Sep URL Get Peserta Cannot Be '${options.url.getPeserta}'`);
        }
        if (options.url.insert == null || options.url.insert == "") {
            throw Error(`Create Sep URL Insert SEP Cannot Be '${options.url.insert}'`);
        }
        if (options.url.update == null || options.url.update == "") {
            throw Error(`Create Sep URL Update SEP Cannot Be '${options.url.update}'`);
        }
        if (options.url.delete == null || options.url.delete == "") {
            throw Error(`Create Sep URL Delete SEP Cannot Be '${options.url.delete}'`);
        }
        if (options.url.cari == null || options.url.cari == "") {
            throw Error(`Create Sep URL Cari SEP Cannot Be '${options.url.cari}'`);
        }

        this.data('create.sep', options);

        let $this = this;
        this.on('click', function (e) {
            clickEvent($this);
        });

        console.log('initialize create.sep', this, options);
    }

    let clickEvent = function ($this) {
        blockJqElement($this.closest('.input-group'));
        let opt = $this.data('create.sep');
        let inputSep = opt.inputSep;
        let inputNoKartu = opt.inputNoKartu;

        // Check No. Kartu Apakah Sudah Terisi / Belum
        if (inputNoKartu.val().trim() == "") {
            swalHtml('Perhatian', '<p class="text-center">Silahkan Isi No. Kartu Terlebih Dahulu.</p>', 'warning');
            return;
        }

        // TODO: Check Layanan Yang Dipilih

        // TODO: CHECK TAGIHAN PASIEN

        // GET Data Peserta Berdasarkan No. Kartu
        let no_kartu = inputNoKartu.val().trim();
        $.ajax({
            type: "GET",
            url: opt.url.getPeserta.replace(':NO_KARTU', no_kartu).replace(':TGL', moment().format('YYYY-MM-DD')),
            success: (res) => {
                $this.closest('.input-group').unblock();
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Peserta BPJS Dengan No. Kartu '${no_kartu}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                showForm($this, response);
            },
            error: (err) => {
                $this.closest('.input-group').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                $this.closest('.input-group').unblock();
            }
        });
    }

    let showForm = function ($this, response) {
        let opt = $this.data('create.sep');
        let inputSep = opt.inputSep;
        let inputNoKartu = opt.inputNoKartu;

        let noKartu = inputNoKartu.val();

        let modal = createForm($this);

        modal.find('[name=search_no_kartu]').val(response.peserta.noKartu);
        modal.find('.btn_search_no_kartu').click();
        modal.modal('show');
    }

    $.fn.createSep.defaultOnSuccess = function (res) {
        console.log('$.fn.createSep.defaultOnSuccess', res);
    }

    $.fn.createSep.defaultOnError = function (err) {
        console.log('$.fn.createSep.defaultOnError', err);
    }

    $.fn.createSep.defaultOpts = {
        inputSep: $("#no_jaminan"),
        inputNoKartu: $("#no_kartu"),
        onSuccess: $.fn.createSep.defaultOnSuccess,
        onError: $.fn.createSep.defaultOnError,
    }

    /**
     * FETCH Data Referensi Dan Lain-Lain
     */
    let fetchFaskes = function ($this, kode) {
        let opt = $this.data('create.sep');
        let modal = createForm($this);
        $.ajax({
            type: "GET",
            url: opt.url.fetchFaskes,
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Peserta BPJS Dengan No. Kartu '${no_kartu}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                showForm($this, response);
            },
            error: (err) => {
                console.log(err);
            }
        });
    };

    /**
     * Buat Form Insert SEP
     */
    let createForm = function ($this) {
        let modal = $("#modal-create_sep");

        if (modal.length > 0) {
            return modal;
        }

        // TODO: Create Form


        // Handler
        modal.find('.btn_search_no_kartu').on('click', function (e) {
            let noKartu = modal.find('[name=search_no_kartu]').val();

            blockJqElement($(this).closest('.input-group'));

            $.ajax({
                type: "GET",
                url: opt.url.getPeserta.replace(':NO_KARTU', no_kartu).replace(':TGL', moment().format('YYYY-MM-DD')),
                success: (res) => {
                    $(this).closest('.input-group').unblock();
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    if (response == null) {
                        let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Peserta BPJS Dengan No. Kartu '${no_kartu}' Tidak Ditemukan.</p>`;
                        swalHtml('Perhatian', msg, 'warning');
                        return;
                    }

                    console.log('get_peserta_by_no_kartu', response);

                    // FILL FORM
                    modal.find('[name=no_kartu]').val(response.peserta.noKartu);
                    modal.find('[name=tgl_sep]').val(moment().format('YYYY-MM-DD'));
                    modal.find('[name=ppk_pelayanan]').val();
                    modal.find('[name=jns_pelayanan]').val();
                    modal.find('[name=kls_rawat]').val();
                    modal.find('[name=no_mr]').val();
                    modal.find('[name=asal_rujukan]').val();
                    modal.find('[name=tgl_rujukan]').val();
                    modal.find('[name=no_rujukan]').val();
                    modal.find('[name=ppk_rujukan]').val();
                    modal.find('[name=catatan]').val();
                    modal.find('[name=diag_awal]').val();
                    modal.find('[name=poli_tujuan]').val();
                    modal.find('[name=poli_eksekutif]').val();
                    modal.find('[name=cob]').val();
                    modal.find('[name=katarak]').val();
                    modal.find('[name=laka_lantas]').val();
                    modal.find('[name=penjamin]').val();
                    modal.find('[name=tgl_kejadian]').val();
                    modal.find('[name=keterangan]').val();
                    modal.find('[name=suplesi]').val();
                    modal.find('[name=no_sep_suplesi]').val();
                    modal.find('[name=laka_kode_propinsi]').val();
                    modal.find('[name=laka_kode_kabupaten]').val();
                    modal.find('[name=laka_kode_kecamatan]').val();
                    modal.find('[name=skdp_no_surat]').val();
                    modal.find('[name=skdp_kode_dpjp]').val();
                    modal.find('[name=no_telp]').val();
                },
                error: (err) => {
                    $(this).closest('.input-group').unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    $(this).closest('.input-group').unblock();
                }
            });
        });


        return modal;
    }
})( jQuery );