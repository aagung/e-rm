$(() => {
    let fillForm = (noSep) => {
        noSep = noSep || "0";
        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        $.ajax({
            type: "GET",
            url: URL.sep.cari.replace(':NO_SEP', noSep),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Rujukan Dengan No. Rujukan '${noRujukan}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                // HIDE CARI
                MODAL_SEP.find('[name="search_no_rujukan"]').closest('.form-horizontal').hide();

                // FILL FORM
                MODAL_SEP.find('[name=no_sep]').val(response.noSep);
                MODAL_SEP.find('[name=no_kartu]').val(response.peserta.noKartu);
                MODAL_SEP.find('[name=tgl_sep]').val(moment(response.tglSep).format('YYYY-MM-DD'));
                MODAL_SEP.find('[name=no_mr]').val(response.peserta.noMr);
                MODAL_SEP.find('[name=no_rujukan]').val(response.noRujukan);


                MODAL_SEP.find('[name="disp-nama"]').val(response.peserta.nama);
                MODAL_SEP.find('[name="disp-no_mr"]').val(response.peserta.noMr || "-");
                MODAL_SEP.find('[name="disp-no_kartu"]').val(response.peserta.noKartu);
                MODAL_SEP.find('[name="disp-no_rujukan"]').val(response.noRujukan);
                MODAL_SEP.find('[name="disp-tgl_sep"]').val(moment(response.tglSep).format('DD/MM/YYYY'));

                console.log('fillForm', response);
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let fillFormByNoRujukan = (jenis, noRujukan) => {
        jenis = jenis || "pcare";
        noRujukan = noRujukan || "0";

        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        $.ajax({
            type: "GET",
            url: URL.sep.getRujukan.replace(':JENIS', jenis).replace(':NO_RUJUKAN', noRujukan),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Rujukan Dengan No. Rujukan '${noRujukan}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                // Show Cari
                MODAL_SEP.find('[name="search_no_rujukan"]').closest('.form-horizontal').show();

                // FILL FORM
                MODAL_SEP.find('[name=no_sep]').val("");
                MODAL_SEP.find('[name=no_kartu]').val(response.rujukan.peserta.noKartu);
                MODAL_SEP.find('[name=tgl_sep]').val(moment().format('YYYY-MM-DD'));
                MODAL_SEP.find('[name=ppk_pelayanan]').val(""); // TODO: DIAMBIL DARI MASTER LAYANANAN
                MODAL_SEP.find('[name=jns_pelayanan]').val(2); // 1. RANAP, 2. RAJAL
                MODAL_SEP.find('[name=kls_rawat]').val(3);
                MODAL_SEP.find('[name=no_mr]').val(response.rujukan.peserta.mr.noMr);
                MODAL_SEP.find('[name=asal_rujukan]').val(response.asalFaskes);
                MODAL_SEP.find('[name=tgl_rujukan]').val(response.rujukan.tglKunjungan);
                MODAL_SEP.find('[name=no_rujukan]').val(response.rujukan.noKunjungan);
                MODAL_SEP.find('[name=ppk_rujukan]').val(response.rujukan.provPerujuk.kode);
                MODAL_SEP.find('[name=catatan]').val("");
                MODAL_SEP.find('[name=diag_awal]').val(response.rujukan.diagnosa.kode);
                MODAL_SEP.find('[name=poli_tujuan]').val(response.rujukan.poliRujukan.kode);
                MODAL_SEP.find('[name=poli_eksekutif]').val(0);
                MODAL_SEP.find('[name=cob]').val(0);
                MODAL_SEP.find('[name=katarak]').val(0);
                MODAL_SEP.find('[name=laka_lantas]').val(0);
                MODAL_SEP.find('[name=penjamin]').val(2);
                MODAL_SEP.find('[name=tgl_kejadian]').val("");
                MODAL_SEP.find('[name=keterangan]').val("");
                MODAL_SEP.find('[name=suplesi]').val(0);
                MODAL_SEP.find('[name=no_sep_suplesi]').val(0);
                MODAL_SEP.find('[name=laka_kode_propinsi]').val("");
                MODAL_SEP.find('[name=laka_kode_kabupaten]').val("");
                MODAL_SEP.find('[name=laka_kode_kecamatan]').val("");
                MODAL_SEP.find('[name=skdp_no_surat]').val("");
                MODAL_SEP.find('[name=skdp_kode_dpjp]').val("");
                MODAL_SEP.find('[name=no_telp]').val(response.rujukan.peserta.mr.noTelepon);

                // DISP
                let disp_dokter = ($("#dokter_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#dokter_id").find('option:selected').html() : "-";
                let disp_pekerjaan = ($("#pekerjaan_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#pekerjaan_id").find('option:selected').html() : "-";
                let disp_layanan = ($("#layanan_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#layanan_id").find('option:selected').html() : "-";

                MODAL_SEP.find('[name="disp-nama"]').val(response.rujukan.peserta.nama);
                MODAL_SEP.find('[name="disp-no_mr"]').val(response.rujukan.peserta.mr.noMr || "-");
                MODAL_SEP.find('[name="disp-no_kartu"]').val(response.rujukan.peserta.noKartu);
                MODAL_SEP.find('[name="disp-tanggal_lahir"]').val(moment(response.rujukan.peserta.tglLahir).format('DD/MM/YYYY'));
                MODAL_SEP.find('[name="disp-jenis_peserta"]').val(response.rujukan.peserta.jenisPeserta.keterangan);
                MODAL_SEP.find('[name="disp-kelas_tanggungan"]').val(response.rujukan.peserta.hakKelas.keterangan);
                MODAL_SEP.find('[name="disp-dokter"]').val(disp_dokter);
                MODAL_SEP.find('[name="disp-pekerjaan"]').val(disp_pekerjaan);
                MODAL_SEP.find('[name="disp-spesialis"]').val(response.rujukan.poliRujukan.kode + ' - ' + response.rujukan.poliRujukan.nama);
                MODAL_SEP.find('[name="disp-ppk_rujukan"]').val(response.rujukan.provPerujuk.kode + ' - ' + response.rujukan.provPerujuk.nama);
                MODAL_SEP.find('[name="disp-tgl_rujukan"]').val(moment(response.rujukan.tglKunjungan).format('DD/MM/YYYY'));
                MODAL_SEP.find('[name="disp-no_rujukan"]').val(response.rujukan.noKunjungan);
                MODAL_SEP.find('[name="disp-skdp_no_surat"]').val("");
                MODAL_SEP.find('[name="disp-skdp_kode_dpjp"]').val("");
                MODAL_SEP.find('[name="disp-tgl_sep"]').val(moment().format('DD/MM/YYYY'));
                MODAL_SEP.find('[name="disp-tgl_kejadian"]').val('');
                MODAL_SEP.find('[name="disp-laka_lantas"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-diag_awal"]').val(response.rujukan.diagnosa.kode + ' - ' + response.rujukan.diagnosa.nama);
                MODAL_SEP.find('[name="disp-no_telp"]').val(response.rujukan.peserta.mr.noTelepon);
                MODAL_SEP.find('[name="disp-catatan"]').val("");
                MODAL_SEP.find('[name="disp-no_mr"]').val(response.rujukan.peserta.mr.noMr || "");
                MODAL_SEP.find('[name="disp-cob"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-katarak"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_propinsi"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_kecamatan"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_kabupaten"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-keterangan"]').val("");

                fetchPropinsi("");

                console.log('fillFormByNoRujukan', response);
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let fillFormByNoKartu = (noKartu) => {
        noKartu = noKartu || "0";
        let tgl = moment().format('YYYY-MM-DD');

        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        $.ajax({
            type: "GET",
            url: URL.sep.getPeserta.replace(':NO_KARTU', noKartu).replace(':TGL', tgl),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Peserta BPJS Dengan No. Kartu '${no_kartu}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                // Show Cari
                MODAL_SEP.find('[name="search_no_rujukan"]').closest('.form-horizontal').show();

                // FILL FORM
                MODAL_SEP.find('[name=no_sep]').val("");
                MODAL_SEP.find('[name=no_kartu]').val(response.peserta.noKartu);
                MODAL_SEP.find('[name=tgl_sep]').val(moment().format('YYYY-MM-DD'));
                MODAL_SEP.find('[name=ppk_pelayanan]').val(""); // TODO: DIAMBIL DARI MASTER LAYANANAN
                MODAL_SEP.find('[name=jns_pelayanan]').val(2); // 1. RANAP, 2. RAJAL
                MODAL_SEP.find('[name=kls_rawat]').val(3);
                MODAL_SEP.find('[name=no_mr]').val(response.peserta.mr.noMr);
                MODAL_SEP.find('[name=asal_rujukan]').val("");
                MODAL_SEP.find('[name=tgl_rujukan]').val("");
                MODAL_SEP.find('[name=no_rujukan]').val("");
                MODAL_SEP.find('[name=ppk_rujukan]').val("");
                MODAL_SEP.find('[name=catatan]').val("");
                MODAL_SEP.find('[name=diag_awal]').val("");
                MODAL_SEP.find('[name=poli_tujuan]').val($("#layanan_kode_bpjs").val());
                MODAL_SEP.find('[name=poli_eksekutif]').val(0);
                MODAL_SEP.find('[name=cob]').val(0);
                MODAL_SEP.find('[name=katarak]').val(0);
                MODAL_SEP.find('[name=laka_lantas]').val(0);
                MODAL_SEP.find('[name=penjamin]').val(2);
                MODAL_SEP.find('[name=tgl_kejadian]').val("");
                MODAL_SEP.find('[name=keterangan]').val("");
                MODAL_SEP.find('[name=suplesi]').val(0);
                MODAL_SEP.find('[name=no_sep_suplesi]').val(0);
                MODAL_SEP.find('[name=laka_kode_propinsi]').val("");
                MODAL_SEP.find('[name=laka_kode_kabupaten]').val("");
                MODAL_SEP.find('[name=laka_kode_kecamatan]').val("");
                MODAL_SEP.find('[name=skdp_no_surat]').val("");
                MODAL_SEP.find('[name=skdp_kode_dpjp]').val("");
                MODAL_SEP.find('[name=no_telp]').val(response.peserta.mr.noTelepon);

                // DISP
                let disp_dokter = ($("#dokter_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#dokter_id").find('option:selected').html() : "-";
                let disp_pekerjaan = ($("#pekerjaan_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#pekerjaan_id").find('option:selected').html() : "-";
                let disp_layanan = ($("#layanan_id").find('option:selected').html() || "").toLowerCase() != "- pilih -" ? $("#layanan_id").find('option:selected').html() : "-";

                MODAL_SEP.find('[name="disp-nama"]').val(response.peserta.nama);
                MODAL_SEP.find('[name="disp-no_mr"]').val(response.peserta.mr.noMr || "-");
                MODAL_SEP.find('[name="disp-no_kartu"]').val(response.peserta.noKartu);
                MODAL_SEP.find('[name="disp-tanggal_lahir"]').val(response.peserta.tglLahir);
                MODAL_SEP.find('[name="disp-jenis_peserta"]').val(response.peserta.jenisPeserta.keterangan);
                MODAL_SEP.find('[name="disp-kelas_tanggungan"]').val(response.peserta.hakKelas.keterangan);
                MODAL_SEP.find('[name="disp-dokter"]').val(disp_dokter);
                MODAL_SEP.find('[name="disp-pekerjaan"]').val(disp_pekerjaan);
                MODAL_SEP.find('[name="disp-spesialis"]').val(disp_layanan);
                MODAL_SEP.find('[name="disp-ppk_rujukan"]').val("");
                MODAL_SEP.find('[name="disp-tgl_rujukan"]').val("");
                MODAL_SEP.find('[name="disp-no_rujukan"]').val("");
                MODAL_SEP.find('[name="disp-skdp_no_surat"]').val("");
                MODAL_SEP.find('[name="disp-skdp_kode_dpjp"]').val("");
                MODAL_SEP.find('[name="disp-tgl_sep"]').val(moment().format('DD/MM/YYYY'));
                MODAL_SEP.find('[name="disp-tgl_kejadian"]').val('');
                MODAL_SEP.find('[name="disp-laka_lantas"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-diag_awal"]').val("");
                MODAL_SEP.find('[name="disp-no_telp"]').val(response.peserta.mr.noTelepon);
                MODAL_SEP.find('[name="disp-catatan"]').val("");
                MODAL_SEP.find('[name="disp-no_mr"]').val(response.peserta.mr.noMr || "");
                MODAL_SEP.find('[name="disp-cob"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-katarak"]').prop('checked', false).trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_propinsi"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_kecamatan"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-laka_kode_kabupaten"]').val("").trigger('change');
                MODAL_SEP.find('[name="disp-keterangan"]').val("");

                fetchPropinsi("");

                console.log('fillFormByNoKartu', response);
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    /**
     * SEP
     */
    let insertSEP = (formData, cb) => {
        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        
        $.ajax({
            url: URL.sep.insert,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {
                result = JSON.parse(result);
                let metaData = result.metaData;
                let response = result.response;
                console.log(result);

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Tidak Dapat Membuat SEP Harap Hubungi Admin.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                // TODO:
                if (cb) {
                    cb(result);
                }
            },
            error: function (err) {
                MODAL_SEP.find('.modal-dialog').unblock();

                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: function () {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let updateSEP = (formData, cb) => {
        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        
        $.ajax({
            url: URL.sep.update,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {
                result = JSON.parse(result);
                let metaData = result.metaData;
                let response = result.response;
                console.log(result);

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Tidak Dapat Mengedit SEP Harap Hubungi Admin.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                // TODO:
                if (cb) {
                    cb(result);
                }
            },
            error: function (err) {
                MODAL_SEP.find('.modal-dialog').unblock();

                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: function () {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let deleteSEP = (noSep) => {
        blockJqElement(MODAL_SEP.find('.modal-dialog'));
        // TODO:
    }

    /**
     * FETCH PROPINSI, KABUPATEN, KECAMATAN
     */
    let fetchPropinsi = (value) => {
        $.ajax({
            type: "GET",
            url: URL.sep.referensi.propinsi,
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                let INPUT = MODAL_SEP.find('[name="disp-laka_kode_propinsi"]');

                INPUT.empty();
                $("<option/>")
                    .val("")
                    .html('- Pilih -')
                    .appendTo(INPUT);
                for (let i = 0; i < response.list.length; i++) {
                    $("<option/>")
                        .val(response.list[i].kode)
                        .html(response.list[i].nama)
                        .appendTo(INPUT);
                }

                value = value || "";
                INPUT.val(value).trigger('change');
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let fetchKabupaten = (kd_propinsi, value) => {
        $.ajax({
            type: "GET",
            url: URL.sep.referensi.kabupaten.replace(':KD_PROPINSI', kd_propinsi),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                let INPUT = MODAL_SEP.find('[name="disp-laka_kode_kabupaten"]');

                INPUT.empty();
                $("<option/>")
                    .val("")
                    .html('- Pilih -')
                    .appendTo(INPUT);
                for (let i = 0; i < response.list.length; i++) {
                    $("<option/>")
                        .val(response.list[i].kode)
                        .html(response.list[i].nama)
                        .appendTo(INPUT);
                }

                value = value || "";
                INPUT.val(value).trigger('change');
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let fetchKecamatan = (kd_kabupaten, value) => {
        $.ajax({
            type: "GET",
            url: URL.sep.referensi.kecamatan.replace(':KD_KABUPATEN', kd_kabupaten),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                let INPUT = MODAL_SEP.find('[name="disp-laka_kode_kecamatan"]');

                INPUT.empty();
                $("<option/>")
                    .val("")
                    .html('- Pilih -')
                    .appendTo(INPUT);
                for (let i = 0; i < response.list.length; i++) {
                    $("<option/>")
                        .val(response.list[i].kode)
                        .html(response.list[i].nama)
                        .appendTo(INPUT);
                }

                value = value || "";
                INPUT.val(value).trigger('change');
            },
            error: (err) => {
                MODAL_SEP.find('.modal-dialog').unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                MODAL_SEP.find('.modal-dialog').unblock();
            }
        });
    }

    let initialize = () => {
        // FORM VALIDATION
        FORM_SEP.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                let noKartu = MODAL_SEP.find('[name="no_kartu"]').val();
                let noSep = MODAL_SEP.find('[name="no_sep"]').val();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                if (noKartu.trim() == "") {
                    swalHtml('Peringatan', 'Silahkan lengkapi form terlebih dahulu.', 'warning');
                    return;
                }

                if (noSep.trim() == "" || parseInt(noSep.trim()) == 0) {
                    insertSEP(formData, (result) => {
                        let metaData = result.metaData;
                        let response = result.response;

                        if (response != null) {
                            INPUT_SEP.val(response.sep.noSep);
                            INPUT_NO_KARTU.val(response.sep.peserta.noKartu);

                            MODAL_SEP.modal('hide');

                            BTN_CREATE_SEP.hide();
                            BTN_EDIT_SEP.show();
                            BTN_CETAK_SEP.show();
                        }
                    });
                } else {
                    updateSEP(formData, (result) => {
                        let metaData = result.metaData;
                        let response = result.response;

                        if (response != null) {
                            MODAL_SEP.modal('hide');
                        }
                    });
                }
            }
        });

        BTN_CREATE_SEP.click(function () {
            let noSep = INPUT_SEP.val();

            // if ($("#layanan_kode_bpjs").val() == "" || $("#layanan_kode_bpjs").val() == null) {
            //     let msg = `
            //         <p class="text-center">Silahkan pilih layanan terlebih dahulu.</p>
            //     `;
            //     swalHtml('Perhatian', msg, 'warning');
            //     return;
            // }

            MODAL_SEP.modal('show');
            // fillForm(0);
        });

        BTN_EDIT_SEP.click(function () {
            let noSep = INPUT_SEP.val();

            MODAL_SEP.modal('show');
            fillForm(noSep);
        });

        BTN_CETAK_SEP.click(function () {
            let noSep = INPUT_SEP.val();
            let link = URL.sep.cetak.replace(':NO_SEP', noSep);

            window.open(link);
        })

        // INPUT
        MODAL_SEP.find('[name="disp-tgl_rujukan"]').formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        MODAL_SEP.find('[name="disp-tgl_rujukan"]').on('change keyup blur', function (e) {
            if (moment($(this).val(), 'DD/MM/YYYY').isValid()) {
                MODAL_SEP.find('[name="tgl_rujukan"]').val(moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
            } else {
                MODAL_SEP.find('[name="tgl_rujukan"]').val("");
            }
        });

        MODAL_SEP.find('[name="disp-tgl_kejadian"]').formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        MODAL_SEP.find('[name="disp-tgl_kejadian"]').on('change keyup blur', function (e) {
            if (moment($(this).val(), 'DD/MM/YYYY').isValid()) {
                MODAL_SEP.find('[name="tgl_kejadian"]').val(moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
            } else {
                MODAL_SEP.find('[name="tgl_kejadian"]').val("");
            }
        });

        MODAL_SEP.find('[name="disp-skdp_no_surat"]').on('change keyup blur', function (e) {
            MODAL_SEP.find('[name="skdp_no_surat"]').val($(this).val());
        });

        MODAL_SEP.find('[name="disp-tgl_sep"]').on('change keyup blur', function (e) {
            MODAL_SEP.find('[name="tgl_sep"]').val(moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
        });

        MODAL_SEP.find('[name="disp-no_telp"]').on('change keyup blur', function (e) {
            MODAL_SEP.find('[name="no_telp"]').val($(this).val());
        });

        MODAL_SEP.find('[name="disp-catatan"]').on('change keyup blur', function (e) {
            MODAL_SEP.find('[name="catatan"]').val($(this).val());
        });

        MODAL_SEP.find('[name="disp-poli_eksekutif"]').on('change', function (e) {
            MODAL_SEP.find('[name="poli_eksekutif"]').val($(this).prop('checked') ? 1 : 0);
        });

        MODAL_SEP.find('[name="disp-laka_lantas"]').on('change', function (e) {
            MODAL_SEP.find('[name="laka_lantas"]').val($(this).prop('checked') ? 1 : 0);
        });

        MODAL_SEP.find('[name="disp-cob"]').on('change', function (e) {
            MODAL_SEP.find('[name="cob"]').val($(this).prop('checked') ? 1 : 0);
        });
        
        MODAL_SEP.find('[name="disp-katarak"]').on('change', function (e) {
            MODAL_SEP.find('[name="katarak"]').val($(this).prop('checked') ? 1 : 0);
        });

        MODAL_SEP.find('[name="disp-laka_kode_propinsi"]').on('change', function () {
            let val = $(this).val();
            MODAL_SEP.find('[name="laka_kode_propinsi"]').val(val);
            
            val = val || 0;
            fetchKabupaten(val, "");
        });
        MODAL_SEP.find('[name="disp-laka_kode_kabupaten"]').on('change', function () {
            let val = $(this).val();
            MODAL_SEP.find('[name="laka_kode_kabupaten"]').val(val);
            
            val = val || 0;
            fetchKecamatan(val, "");
        });
        MODAL_SEP.find('[name="disp-laka_kode_kecamatan"]').on('change', function () {
            let val = $(this).val();
            MODAL_SEP.find('[name="laka_kode_kecamatan"]').val(val);
        });
        MODAL_SEP.find('[name="disp-keterangan"]').on('change', function (e) {
            MODAL_SEP.find('[name="keterangan"]').val($(this).val());
        });


        // BUTTONS
        MODAL_SEP.find('.btn_search_no_rujukan').click(function () {
            let jenis = MODAL_SEP.find('[name="search_no_rujukan_jenis"]').val();
            let noRujukan = MODAL_SEP.find('[name="search_no_rujukan"]').val();

            fillFormByNoRujukan(jenis, noRujukan);
        });
        MODAL_SEP.find('.btn_search_no_kartu').click(function () {
            let noKartu = MODAL_SEP.find('[name="search_no_kartu"]').val();
            fillFormByNoKartu(noKartu);
        });

        MODAL_SEP.find('[name="search_no_rujukan_jenis"]').select2();
        MODAL_SEP.find('[name="disp-laka_kode_propinsi"]').select2();
        MODAL_SEP.find('[name="disp-laka_kode_kecamatan"]').select2();
        MODAL_SEP.find('[name="disp-laka_kode_kabupaten"]').select2();


        MODAL_SEP.find('.btn_search_ppk_asal_rujukan').click(function (e) {
            MODAL_SEP.modal('hide');
            MODAL_SEARCH_PPK_RUJUKAN.modal('show');
        });
        MODAL_SEP.find('.btn_search_diag_awal').click(function (e) {
            MODAL_SEP.modal('hide');
            MODAL_SEARCH_DIAGNOSA.modal('show');
        });
        MODAL_SEP.find('.btn_search_skdp_kode_dpjp').click(function (e) {
            MODAL_SEP.modal('hide');
            MODAL_SEARCH_DOKTER_DPJP.modal('show');
        });

        // Initialize Modals
        initializeSearchPPKRujukan();
        initializeSearchDiagnosa();
        initializeSearchDokterDPJP();

        // SELECT2 Container
        MODAL_SEP.find('.select2-container').css('margin-bottom', '5px');
    }

    let initializeSearchPPKRujukan = () => {
        MODAL_SEARCH_PPK_RUJUKAN.find('[data-action="cari"]').click(function () {
            let jenis = MODAL_SEARCH_PPK_RUJUKAN.find('[name="jenis"]:checked').val();
            let nama = MODAL_SEARCH_PPK_RUJUKAN.find('[name="nama"]').val();

            if (! (parseInt(jenis) >= 1)) {
                jenis = 1;
            }

            if (nama.trim() == "") {
                nama = "a";
            }

            blockJqElement(TABLE_SEARCH_PPK_RUJUKAN);
            $.ajax({
                type: "GET",
                url: URL.sep.referensi.faskes.replace(':NAMA', nama).replace(':JENIS', jenis),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_PPK_RUJUKAN.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_PPK_RUJUKAN.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let faskes;
                    for (let i = 0; i < response.faskes.length; i++) {
                        faskes = response.faskes[i];
                        TABLE_SEARCH_PPK_RUJUKAN.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${faskes.kode}" data-nama="${faskes.nama}" data-action="click">
                                        ${faskes.kode}
                                    </a>
                                </td>
                                <td>${faskes.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_PPK_RUJUKAN.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_PPK_RUJUKAN.unblock();
                }
            });
        });

        MODAL_SEARCH_PPK_RUJUKAN.on('hidden.bs.modal', function () {
            MODAL_SEP.modal('show');
        });

        TABLE_SEARCH_PPK_RUJUKAN.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            let jenis = MODAL_SEARCH_PPK_RUJUKAN.find('[name="jenis"]:checked').val();
            if (! (parseInt(jenis) >= 1)) {
                jenis = 1;
            }

            console.log(kode, nama);

            MODAL_SEARCH_PPK_RUJUKAN.modal('hide');
            MODAL_SEP.modal('show');

            MODAL_SEP.find('[name=asal_rujukan]').val(jenis);
            MODAL_SEP.find('[name="ppk_rujukan"]').val(kode);
            MODAL_SEP.find('[name="disp-ppk_rujukan"]').val(`${kode} - ${nama}`);

        });
    }

    let initializeSearchDiagnosa = () => {
        MODAL_SEARCH_DIAGNOSA.find('[data-action="cari"]').click(function () {
            let nama = MODAL_SEARCH_DIAGNOSA.find('[name="nama"]').val();

            if (nama.trim() == "") {
                nama = "a";
            }

            blockJqElement(TABLE_SEARCH_DIAGNOSA);
            $.ajax({
                type: "GET",
                url: URL.sep.referensi.diagnosa.replace(':NAMA', nama),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_DIAGNOSA.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_DIAGNOSA.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let row;
                    for (let i = 0; i < response.diagnosa.length; i++) {
                        row = response.diagnosa[i];
                        TABLE_SEARCH_DIAGNOSA.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${row.kode}" data-nama="${row.nama}" data-action="click">
                                        ${row.kode}
                                    </a>
                                </td>
                                <td>${row.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_DIAGNOSA.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_DIAGNOSA.unblock();
                }
            });
        });

        MODAL_SEARCH_DIAGNOSA.on('hidden.bs.modal', function () {
            MODAL_SEP.modal('show');
        });

        TABLE_SEARCH_DIAGNOSA.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            console.log(kode, nama);

            MODAL_SEARCH_DIAGNOSA.modal('hide');
            MODAL_SEP.modal('show');

            MODAL_SEP.find('[name="diag_awal"]').val(kode);
            MODAL_SEP.find('[name="disp-diag_awal"]').val(`${nama}`);
        });
    }

    let initializeSearchDokterDPJP = () => {
        MODAL_SEARCH_DOKTER_DPJP.find('[data-action="cari"]').click(function () {
            let jenis = MODAL_SEARCH_DOKTER_DPJP.find('[name="jenis"]:checked').val();
            let tanggal = moment(MODAL_SEARCH_DOKTER_DPJP.find('[name="tanggal"]').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
            let kode = MODAL_SEARCH_DOKTER_DPJP.find('[name="kode"]').val();

            if (! (parseInt(jenis) >= 1)) {
                jenis = 1;
            }

            if (tanggal.trim() == "") {
                tanggal = moment().format('YYYY-MM-DD');
            }

            if (kode.trim() == "") {
                kode = "a";
            }

            blockJqElement(TABLE_SEARCH_DOKTER_DPJP);
            $.ajax({
                type: "GET",
                url: URL.sep.referensi.dokter_dpjp.replace(':JENIS', jenis).replace(':TANGGAL', tanggal).replace(':KODE', kode),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_DOKTER_DPJP.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_DOKTER_DPJP.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let row;
                    for (let i = 0; i < response.list.length; i++) {
                        row = response.list[i];
                        TABLE_SEARCH_DOKTER_DPJP.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${row.kode}" data-nama="${row.nama}" data-action="click">
                                        ${row.kode}
                                    </a>
                                </td>
                                <td>${row.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_DOKTER_DPJP.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_DOKTER_DPJP.unblock();
                }
            });
        });

        MODAL_SEARCH_DOKTER_DPJP.on('hidden.bs.modal', function () {
            MODAL_SEP.modal('show');
        });

        TABLE_SEARCH_DOKTER_DPJP.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            console.log(kode, nama);

            MODAL_SEARCH_DOKTER_DPJP.modal('hide');
            MODAL_SEP.modal('show');

            MODAL_SEP.find('[name="skdp_kode_dpjp"]').val(kode);
            MODAL_SEP.find('[name="disp-skdp_kode_dpjp"]').val(`${kode} - ${nama}`);
        });

        MODAL_SEARCH_DOKTER_DPJP.find('[name="tanggal"]').formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
    }

    initialize();
});