$(() => {
    let enableLoad = false;
    let EL = {
        tanggal: '#modal-input_reservasi_ranap_tanggal',
        ruang_id: '#modal-input_reservasi_ranap_ruang_id',
        kelas_id: '#modal-input_reservasi_ranap_kelas_id',
        bed_id: '#modal-input_reservasi_ranap_bed_id',
        dokter_id: '#modal-input_reservasi_ranap_dokter_id',
        diagnosa_utama: '#modal-input_reservasi_ranap_diagnosa_utama',
        submit: '#btn-save_reservasi_ranap',
    }

    $('.reservasi-date').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        min: moment()
    });

    let fillForm = (uid) => {
        let dokter_id = $("[name=dokter_id]").length > 0 ? $("[name=dokter_id]").val() : "";
        initializeDiagnosa($(EL.diagnosa_utama));
        $.getJSON(`${base_url}/api/pelayanan_medis/rujukan/rujukan_ranap/get_data/${uid}`, function (res, status) {
            if (status === 'success') {
                let data = res.data;
                $(modalReservasiRanap).find(`input[name=uid]`).val(uid);
                $(EL.tanggal).val(moment(data.tanggal).format('DD/MM/YYYY'));

                /*fillSelect({
                    url: `${base_url}/api/master/ruang/get_by_jenis?jenis=rawat_inap`,
                    value: data.ruang_id,
                }, EL.ruang_id);

                fillSelect({
                    url: `${base_url}/api/master/kelas/get_all`,
                    value: data.kelas_id,
                }, EL.kelas_id);

                fillSelect({
                    url: `${base_url}/api/master/bed/get_all?ruang_id=${btoa(data.ruang_id)}&kelas_id=${btoa(data.kelas_id)}`,
                    value: data.bed_id,
                }, EL.bed_id);*/

                fillSelect({
                    url: `${base_url}/api/master/dokter/fetch_all`,
                    value: data.dokter_id ? data.dokter_id : dokter_id,
                }, EL.dokter_id);
                
                if(data.diagnosa_utama) {
                    let optDiagnosaUtama = new Option(data.diagnosa_utama.code, data.diagnosa_utama.id, true, true);
                    $(EL.diagnosa_utama).append(optDiagnosaUtama).trigger('change');

                    $(EL.diagnosa_utama).trigger({
                        type: 'select2:select',
                        params: {
                            data: {
                                    id: data.diagnosa_utama.id,
                                    code: data.diagnosa_utama.title,
                                    text: data.diagnosa_utama.code,
                                    description: data.diagnosa_utama.description,
                                }
                        }
                    });
                }

                $(EL.submit).show();
                $(modalReservasiRanap).find('input, textarea, select').prop('disabled', false);  
                if(data.status == 1) {
                   $(EL.submit).hide();
                   $(modalReservasiRanap).find('input, textarea, select').prop('disabled', true);  
                } 
                setTimeout(() => {
                    enableLoad = true;
                    $(modalReservasiRanap).modal('show');
                }, 300);
            }
        });
    }

    // DIAGNOSA
    let initializeDiagnosa = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.code}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.description}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.code || data.text || '- Pilih -';
        }

        el.select2({
            placeholder: "- Pilih -",
            ajax: {
                url: `${base_url}/api/master/icd_10/search`,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.id, description: obj.description, code: obj.code };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        el.on('select2:select', function (e) {
            var data = e.params.data;
            if(typeof data !== "undefined") {
                //
            }
        });
    }

    let fillSelect = (obj, element) => {
        let parent = $(element).parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            let list;
            if(element === EL.dokter_id) {
                list = data.data;
            } else list = data.list;

            if (status === 'success') {
                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${list[i].id}" ${selected}>${list[i].nama}</option>`;
                }
                $(element).html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    /*$(`${EL.ruang_id}, ${EL.kelas_id}`).change(function() {
        if(enableLoad) {
            let ruang_id = $(EL.ruang_id).val() ? btoa($(EL.ruang_id).val()) : '';
            let kelas_id = $(EL.kelas_id).val() ? btoa($(EL.kelas_id).val()) : '';
            let bed_id = $(EL.bed_id).val() ? $(EL.bed_id).val() : '';
            fillSelect({
                url:  `${base_url}/api/master/bed/get_all?ruang_id=${ruang_id}&kelas_id=${kelas_id}`,
                value: bed_id,
            }, EL.bed_id);
        }
    });*/

    $(btnReservasiRanap).click(function() {
        let uid = $(this).data('uid') === undefined ? "" : $(this).data('uid');
        fillForm(uid);
    });

    $(formReservasiRanap).validate({
        rules: {
            tanggal: { required: true },
            dokter_id: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalReservasiRanap + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: `${base_url}/api/pelayanan_medis/rujukan/rujukan_ranap/save`,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    console.log(result);
                    RUJUKAN.ranap_uid = result;
                    successMessage('Success', "Reservasi Rawat Inap berhasil disimpan.");
                    setTimeout(() => {
                        $(modalReservasiRanap + ' .modal-dialog').unblock();
                        $(modalReservasiRanap).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalReservasiRanap + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });
});