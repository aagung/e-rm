$(() => {
	let EL = {
		farmasiUnit: '#modal-obat_farmasi_unit',
		table: '#table-modal_obat',
	};

	let fetchObat = () => {
        blockElement(modalObat + ' .modal-dialog');
        let isDTable = $.fn.dataTable.isDataTable($(EL.table));
        if(isDTable === false) {
        	tableListObat = $(EL.table).DataTable({
	            "processing": true,
				"serverSide": true,
				"ordering": false,
				"ajax": {
					"url": urlfetchObat.replace(':FARMASI_UNIT', $(EL.farmasiUnit).val()),
					"type": "POST",
				},
	            "columns": [
	                { 
	                    "data": "id",
	                    "render": function (data, type, row, meta) {
	                        let dataAttr = [
	                                        `data-id="${data}"`,
	                                        `data-barang_id="${row.barang_id}"`,
	                                        `data-barang_uid="${row.barang_uid}"`,
	                                        `data-barang="${row.barang}"`,
	                                        `data-satuan_id="${row.satuan_id}"`,
	                                        `data-satuan="${row.satuan}"`,
	                                        `data-harga="${row.harga}"`,
	                                        `data-harga_dasar="${row.harga_dasar}"`,
	                                        `data-stock="${row.stock}"`,
	                                    ];
	                        return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
	                    },
	                },
	                { "data": "kode_barang" },
	                { "data": "barang" },
	                { "data": "satuan" },
	                { 
	                    "data": "harga",
	                    "render": function (data, type, row, meta) {
	                        return numeral(data).format('0.0,');
	                    },
	                    "className": "text-right"
	                },
	                { 
	                	"data": "stock",
	                	"render": function (data, type, row, meta) {
	                        return numeral(data).format('0.0,');
	                    },
	                },
	            ],
	            "drawCallback": function (settings) {
	                let tr = $(EL.table).find("tbody tr");
					tr.each(function(el) {
						let data = tableListObat.row(el).data();
						if(data) {
							for (var val in selectedObat) {
								if(data.barang_uid === val) {
									let cbEl = $(this).find('.check');
									cbEl.prop('checked', true);
								}
							}
						}
					});

	                $('.check').uniform();
	            },
	        });
        } else tableListObat.draw();
        	
        setTimeout(() => {
            $(modalObat + ' .modal-dialog').unblock();
        }, 300);
    }

    $(EL.farmasiUnit).on('change', '', function() {
        fetchObat();
    });

    $(EL.table).on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListObat.row(tr).data();
        
        if($(this).prop('checked') === true) {
        	selectedObat[data.barang_uid] = data;
        } else {
        	delete selectedObat[data.barang_uid];
        }
    });

    $(modalObat).on('show.bs.modal', function () {
    	console.log('Empty selectedObat');
    	$(EL.farmasiUnit).change();
        selectedObat = [];
    });
});