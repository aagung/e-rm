/*
*    modalPindahBed, modalCheckout => didapat dari file modal 'pelayanan_medis/rawat_inap/pindah-bed-modal.php'
*/

$(() => {
    $('.rangetanggal-form').daterangepicker({
        applyClass: "bg-slate-600",
        cancelClass: "btn-default",
        opens: "center",
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        startDate: moment(),
        endDate: moment(),
    });

    let subsDate = (range, tipe) => {
        let date = range.substr(0, 10);
        if(tipe === "sampai") date = range.substr(13, 10);
        return getDate(date);
    }

    let listen = (browse) => {
        let eventId = 'ranap-daftar_kunjungan';
        if(browse === 2) eventId = 'ranap-daftar_kunjungan_checkout';

        evenSource = new EventSource(URL.listen.replace(':EVENT', eventId));
        evenSource.addEventListener(eventId, (e) => {
            let data = $.parseJSON(e.data);
            if (data.data) {
                if(browse === 3) {
                    TABLE_CHECKOUT_DT.draw(false);
                } else TABLE_DT.draw(false);
            }
        }, false);
    }

    let fetchDataSelect = (url, element) => {
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let datas = res.data ? res.data : res.list;

                element.empty();
                element.append(`<option value="">${fieldAll}</option/>`);
                for (let data of datas) {
                    $("<option/>")
                        .prop('value', data.id)
                        .html(data.nama)
                        .appendTo(element);
                }
                element.val('').trigger('change');
            }
        });
    }

    let initializeTable = (tab) => {
        let columns = [];
        if(tab === 1) {
            columns.push({
                        "data": "uid",
                        "searchable": false,
                        "orderable": false,
                        "render": (data, type, row, meta) => {
                            let tmp = `<p><a data-uid="${data}" data-id="${row.id}" data-popup="tooltip" title="Pindah Bed?" class="btn btn-warning btn-xs move-row">Pindah Bed</a></p>`;
                            tmp += `<p><a data-uid="${data}" data-id="${row.id}" data-popup="popover" title="Tutup Asuhan?" data-trigger="hover" data-content="Proses ini dilakukan agar kasir dapat memproses transaksi rawat inap dan perawat tidak dapat menambah tindakan lagi." class="btn btn-info btn-xs close-row">Tutup Asuhan</a></p>`;
                            if(row.close == 1) {
                                tmp = `<p><a data-uid="${data}" data-id="${row.id}" data-popup="popover" title="Buka Kembali Asuhan?" data-trigger="hover" data-content="Proses ini dilakukan agar perawat dapat memasukkan kembali data asuhan." class="btn bg-brown btn-xs open-row">Buka Asuhan</a></p>`;
                                tmp += `<p><a data-uid="${data}" data-id="${row.id}" data-popup="popover" title="Bersihkan Bed?" data-trigger="hover" data-content="Proses ini dilakukan setelah pasien membayar tagihan di kasir." class="btn btn-danger btn-xs clean-row">Bersihkan Bed</a></p>`;
                            }
                            return tmp;
                        },
                        "className": "text-center",
                    });
        }

        columns.push({
                        "data": "tanggal",
                        "searchable": false,
                        "render": (data, type, row, meta) => {
                            let tmp;
                            switch(tab) {
                                case 3:
                                    tmp  = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '&mdash;';
                                    break;
                                default:
                                    tmp  = moment(row.checkout_at).isValid() ? moment(row.checkout_at).format('DD/MM/YYYY HH:mm') : '&mdash;';
                                    if(tab === 1) tmp = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '&mdash;';
                            }
                            return tmp;
                        },
                    },
                    {
                        "data": "no_register",
                        "render": (data, type, row, meta) => {
                            let tmp = `<a href="${URL.statusPemeriksaan.replace(':UID', row.uid)}" data-popup="tooltip" title="Lihat Status Pemeriksaan">${data}</a>`;
                            switch(tab) {
                                case 3:
                                    tmp = `<a class="detail-row" data-popup="tooltip" title="Lihat Detail Rujukan">${data}</a>`;
                                    break;
                                case 2:
                                    tmp = data;
                                    break;
                            }
                            return tmp;
                        },
                    },
                    {
                        "data": "ruang",
                        "searchable": false,
                    },
                    {
                        "data": "kelas",
                        "searchable": false,
                    },
                    {
                        "data": "pasien",
                        "render": (data, type, row, meta) => {
                            let tmp = data;
                            tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                            return tmp;
                        },
                    },
                    {
                        "data": "cara_bayar",
                        "searchable": false,
                        "render": (data, type, row, meta) => {
                            let tmp = data;
                            if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                            return tmp;
                        },
                    },
                    { 
                        "data": "dokter",
                        "searchable": false,
                    });

        if(tab === 1) {
            columns.push({ 
                        "data": "titip",
                        "searchable": false,
                        "render": (data, type, row, meta) => {
                            let tmp = '&mdash;';
                            if(data == 1) tmp = `Titip <br/><span class="text-size-mini text-info">${row.titip_kelas}</span>`;
                            return tmp;
                        },
                    },
                    { 
                        "data": "uid",
                        "visible": false,
                        "searchable": false,
                        "orderable": false,
                    });
        }

        // DATATABLE
        switch(tab) {
            case 1:
                TABLE_DT = TABLE.DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 3),
                      "type": "POST",
                      "data": function(p) {
                          p.ruang_id = $('#table-search_ruang_id').val();
                          p.kelas_id = $('#table-search_kelas_id').val();
                          p.cara_bayar_id = $('#table-search_cara_bayar_id').val();
                          p.dokter_id = $('#table-search_dokter_id').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [1, "asc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE.find('[data-popup=popover]').popover();
                        TABLE.find('[data-popup=tooltip]').tooltip();
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#table-search_cara_bayar_id'));
                fetchDataSelect(URL.fetchDokter, $('#table-search_dokter_id'));
                fetchDataSelect(URL.fetchRuang, $('#table-search_ruang_id'));
                fetchDataSelect(URL.fetchKelas, $('#table-search_kelas_id'));

                var isDTable = $.fn.dataTable.isDataTable(TABLE_CHECKOUT);
                if(isDTable === false) initializeTable(2);
                break;
            case 2:
                TABLE_CHECKOUT_DT = TABLE_CHECKOUT.DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 4),
                      "type": "POST",
                      "data": function(p) {
                          p.tanggal_dari = subsDate($('#table-checkout_search_tanggal').val(), 'dari');
                          p.tanggal_sampai = subsDate($('#table-checkout_search_tanggal').val(), 'sampai');
                          p.ruang_id = $('#table-checkout_search_ruang_id').val();
                          p.kelas_id = $('#table-checkout_search_kelas_id').val();
                          p.cara_bayar_id = $('#table-checkout_search_cara_bayar_id').val();
                          p.dokter_id = $('#table-checkout_search_dokter_id').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [0, "asc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE_CHECKOUT.find('[data-popup=tooltip]').tooltip();
                    }
                });

                fetchDataSelect(URL.fetchDokter, $('#table-checkout_search_dokter_id'));
                fetchDataSelect(URL.fetchCaraBayar, $('#table-checkout_search_cara_bayar_id'));
                fetchDataSelect(URL.fetchRuang, $('#table-checkout_search_ruang_id'));
                fetchDataSelect(URL.fetchKelas, $('#table-checkout_search_kelas_id'));

                var isDTable = $.fn.dataTable.isDataTable(TABLE_RUJUK_OK);
                if(isDTable === false) initializeTable(3);
                break;
            case 3:
                TABLE_RUJUK_OK_DT = TABLE_RUJUK_OK.DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadRujukOk,
                      "type": "POST",
                      "data": function(p) {}
                    },
                    "columns": columns,
                    "order": [ [0, "asc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE_RUJUK_OK.find('[data-popup=tooltip]').tooltip();
                    }
                });
                break;
        }
    }

    $('#btn-reset').on('click', () => {
        $('#table-search_ruang_id').val('').trigger('change');
        $('#table-search_kelas_id').val('').trigger('change');
        $('#table-search_cara_bayar_id').val('').trigger('change');
        $('#table-search_dokter_id').val('').trigger('change');
    });

    $('.select-table_tab1').on('change', function () {
        TABLE_DT.draw();
    });

    TABLE.on("click", ".move-row", function () {
        let uid = $(this).data('uid');
        $(modalPindahBed).find(`input[name=uid]`).val(uid);
        $(modalPindahBed).modal('show');
    });

    TABLE.on("click", ".clean-row", function () {
        let uid = $(this).data('uid');
        $(modalCheckout).find(`input[name=uid]`).val(uid);
        $(modalCheckout).modal('show');
    });

    TABLE.on("click", ".close-row", function () {
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        let tr = $(this).closest('tr');
        let data = TABLE_DT.row(tr).data();
        swal({
            title: "Yakin akan menutup perawatan pasien tersebut?",
            text: `<div class="row form-horizontal text-left">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. RM</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold">${data.no_rm}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Nama</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-nama_pasien">${data.pasien}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Jenis Kelamin</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.jenis_kelamin}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Alamat</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.alamat}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Tanggal Lahir</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-tanggal_lahir">${data.tanggal_lahir}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Umur</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-umur">${data.umur_tahun} Tahun</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. Telepon</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-no_telepon">${data.no_telepon}</div>
                                </div>
                            </div>
                        </div>
                    </div>`,
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#F44336",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top",
        },
        function() {        
            $.post(URL.closeKeperawatan, { id: id, uid: uid}, function (data, status) {
                if (status === "success") {
                    swal.close();
                    successMessage('Berhasil !', 'Perawatan berhasil ditutup.');
                }
            })
            .fail(function (error) {
                swal.close();
                errorMessage('Gagal !', 'Terjadi kesalahan saat menutup perawatan.');
            });
        });
    });

    TABLE.on("click", ".open-row", function () {
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        let tr = $(this).closest('tr');
        let data = TABLE_DT.row(tr).data();
        swal({
            title: "Yakin akan membuka kembali perawatan pasien tersebut?",
            text: `<div class="row form-horizontal text-left">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. RM</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold">${data.no_rm}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Nama</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-nama_pasien">${data.pasien}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Jenis Kelamin</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.jenis_kelamin}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Alamat</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.alamat}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Tanggal Lahir</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-tanggal_lahir">${data.tanggal_lahir}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Umur</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-umur">${data.umur_tahun} Tahun</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. Telepon</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-no_telepon">${data.no_telepon}</div>
                                </div>
                            </div>
                        </div>
                    </div>`,
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#F44336",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top",
        },
        function() {        
            $.post(URL.openKeperawatan, { id: id, uid: uid}, function (data, status) {
                if (status === "success") {
                    swal.close();
                    successMessage('Berhasil !', 'Perawatan berhasil dibuka kembali.');
                }
            })
            .fail(function (error) {
                swal.close();
                errorMessage('Gagal !', 'Terjadi kesalahan saat membuka perawatan.');
            });
        });
    });
    
    // TAB 2
    $('#btn-reset_checkout').on('click', () => {
        $('#table-checkout_search_cara_bayar_id').val('').trigger('change');
        $('#table-checkout_search_dokter_id').val('').trigger('change');
        $('#table-checkout_search_ruang_id').val('').trigger('change');
        $('#table-checkout_search_kelas_id').val('').trigger('change');
        $("#table-checkout_search_tanggal").data("daterangepicker").setStartDate(moment())
        $("#table-checkout_search_tanggal").data("daterangepicker").setEndDate(moment());
    });

    $('#table-checkout_search_tanggal').on('apply.daterangepicker', (ev, picker) => {
        TABLE_CHECKOUT_DT.draw();
    });

    $('.select-table_tab2').on('change', function () {
        TABLE_CHECKOUT_DT.draw();
    });

    $('#table-checkout_search_tanggal').change(function() {
        TABLE_CHECKOUT_DT.draw();
    });

    // TAB 3
    let detailOperasi = (uid) => {
        $.getJSON(URL.getDataOk.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".section-rawat_inap").show();
                
                let data = res.data;
                $(".label-ruang_operasi").html(data.ruang_operasi ? data.ruang_operasi : '&mdash;');
                $(".label-tanggal_operasi").html(moment(data.tanggal).format('DD-MM-YYYY'));
                $(".label-nama_operasi").html(data.operasi ? data.operasi : '&mdash;');
                $(".label-spesialisasi_operasi").html(data.spesialisasi ? data.spesialisasi : '&mdash;');
                $(".label-golongan_operasi").html(data.golongan_operasi ? data.golongan_operasi : '&mdash;');
                $(".label-tindakan_operasi").html(data.tindakan_operasi ? data.tindakan_operasi : '&mdash;');
                $(".label-diagnosa_operasi").html(data.diagnosa_utama ? data.diagnosa_utama.description : '&mdash;');
                $(".label-klasifikasi").html(data.klasifikasi ? ucwords(data.klasifikasi) : '&mdash;');

                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.pelayanan.no_register);
                $(".label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
                $(".label-layanan").html(data.pelayanan.layanan);
                $(".label-dokter").html(data.pelayanan.dokter);
                $(".label-ruang").html(data.pelayanan.ruang);
                $(".label-kelas").html(data.pelayanan.kelas);
                $(".label-bed").html(data.pelayanan.bed);

                $(".label-cara_bayar").html(data.pelayanan.cara_bayar);
                $(".label-perusahaan").html(data.pelayanan.perusahaan);
                $(".label-no_jaminan").html(data.pelayanan.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

                switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.pelayanan.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }

            }
        }).then(() => {
            $(modalDetailOk).modal('show');
        });
    }

    TABLE_RUJUK_OK.on("click", ".detail-row", function () {
        let tr = $(this).closest('tr');
        let data = TABLE_RUJUK_OK_DT.row(tr).data();
        detailOperasi(data.uid);
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-3':
            TABLE_RUJUK_OK_DT.draw(false);
            break;
          case '#tab-2':
            TABLE_CHECKOUT_DT.draw(false);
            break;
          default:
            TABLE_DT.draw(false);
            break;
        }
    });

    initializeTable(1);
    listen(1);
    listen(2);
});