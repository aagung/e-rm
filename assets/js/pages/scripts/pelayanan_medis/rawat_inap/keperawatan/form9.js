$(() => {
    let table = "#table";
    /*$(".styled").uniform({
        radioClass: 'choice'
    });*/

    let getTotalSkor = (param) => {
        let skor = 0;
        $(table + ' tbody').find(`.${param}`).each(function() {
            if($(this).prop('checked') === true) {
                skor = parseInt($(this).data('skor'));
                $(`[name=skor_${param}]`).val(skor);
            }
        });

        let total_skor = 0;
        $(table + ' tbody').find('.skor').each(function() {
            total_skor += parseInt($(this).val());
        });
        $(`[name=total_skor_branden]`).val(total_skor);
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid).replace(':KEPERAWATAN_UID', keperawatanUid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".section-rawat_inap").show();

                let data = res.data;
                if(data.keperawatan) {
                    $("#id").val(data.keperawatan.id);
                    $("#uid").val(data.keperawatan.uid);
                    if(data.keperawatan.data_pemeriksaan) {
                        let pemeriksaan = data.keperawatan.data_pemeriksaan;
                        let label = Object.keys(pemeriksaan);
                        for (var i = 0; i < label.length; i++) {
                            switch(label[i]) {
                                default: 
                                    if($(`input[name="${label[i]}"]`).is(':text') || $(`[name="${label[i]}"]`).is('textarea') || $(`[name="${label[i]}"]`).is('select')) {
                                        if(pemeriksaan[label[i]]) $(`[name="${label[i]}"]`).val(pemeriksaan[label[i]]).change();
                                    } else {   
                                        if(pemeriksaan[label[i]]) $(`input[name="${label[i]}"][value="${pemeriksaan[label[i]]}"]`).click();
                                    }
                                    break;
                            }
                        }
                    }
                }

                $("#pelayanan_id").val(data.id);
                $("#pelayanan_uid").val(data.uid);

                FORM.find(".label-nama_pasien").html(data.pasien.nama);
                FORM.find(".label-no_rm").html(data.pasien.no_rm);
                FORM.find(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                FORM.find(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                FORM.find(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                FORM.find(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                FORM.find(".label-alamat").html(data.pasien.alamat);
                FORM.find(".label-no_telepon").html(data.pasien.no_telepon_1);

                FORM.find(".label-no_register").html(data.no_register);
                FORM.find(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                FORM.find(".label-cara_bayar").html(data.cara_bayar);
                FORM.find(".label-layanan").html(data.layanan);
                FORM.find(".label-dokter").html(data.dokter);
                FORM.find(".label-ruang").html(data.ruang);
                FORM.find(".label-kelas").html(data.kelas);
                FORM.find(".label-bed").html(data.bed);

                FORM.find(".label-no_register").html(data.no_register);
                FORM.find(".label-layanan").html(data.layanan);
                FORM.find(".label-dokter").html(data.dokter);
                FORM.find(".label-cara_bayar").html(data.cara_bayar);
                FORM.find(".label-perusahaan").html(data.perusahaan);
                FORM.find(".label-no_jaminan").html(data.no_jaminan);
                FORM.find(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                FORM.find(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            FORM.find(".div-label_penjamin_perusahaan").show();
                            FORM.find(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            FORM.find(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            FORM.find(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        FORM.find(".div-label_perusahaan").show();
                        FORM.find(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        FORM.find(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        FORM.find(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $.unblockUI();
            }
        });
    }

    let initializeForm = () => {
        $(table).on('click', '.check-row', function() {
            let param = $(this).data('param');
            getTotalSkor(param);
        });

        $('.btn-kembali').click(function() {
            window.location.assign(URL.index);
        });

        FORM.validate({
            rules: {},
            messages: {},
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 2000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        fillForm(UID);
    }

    initializeForm();
});