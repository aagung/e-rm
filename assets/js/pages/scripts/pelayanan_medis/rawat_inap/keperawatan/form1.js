$(() => {
    /*$(".styled").uniform({
        radioClass: 'choice'
    });*/

    $("#nilai_skrining_nyeri").ionRangeSlider({
        type: "single",
        min: 0,
        max: 10,
        step: 1,
        grid: true,
        grid_snap: true,
        onChange: function(data) {
            console.log(data);
        }
    });

    let getTotalSkor = (table) => {
        let total_skor = 0;
        if(table === "#table-skrining_gizi") {
            $(table + ' tbody').find('tr').each(function() {
                if($(this).find('.check-row').prop('checked') === true) {
                    let skor = parseInt($(this).find('.check-row').data('skor'));
                    total_skor += skor;
                }    
            });
            $('[name=skrining_gizi_total_skor]').val(total_skor);
        } else {
            $(table + ' tbody').find('tr').each(function(trIndex, trEl) {
                $(trEl).find('.check-row').each(function(i, el) {
                    if($(el).prop('checked') === true) {
                        let skor = parseInt($(el).data('skor'));
                        $(trEl).find('label').html(numeral(skor).format());
                        $(trEl).find('input[type=hidden]').val(skor);
                        total_skor += skor;
                    };
                });    
            });
            $('.label-newss_pasien_dewasa_total_skor').html(numeral(total_skor).format());
            $('[name=newss_pasien_dewasa_total_skor]').val(total_skor);

            let color = "bg-success";
            if(total_skor >= 6) {
                color = "bg-danger";
            } else if(total_skor >= 4) {
                color = "bg-warning";
            } else if(total_skor >= 2) {
                color = "bg-yellow";
            }
            $('.label-newss_pasien_dewasa_total_skor').parent().removeClass().addClass(color);
        }
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid).replace(':KEPERAWATAN_UID', keperawatanUid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();
                $(".section-rawat_inap").show();

                let data = res.data;
                if(data.keperawatan) {
                    $("#id").val(data.keperawatan.id);
                    $("#uid").val(data.keperawatan.uid);
                    if(data.keperawatan.data_pemeriksaan) {
                        let pemeriksaan = data.keperawatan.data_pemeriksaan;
                        let label = Object.keys(pemeriksaan);
                        for (var i = 0; i < label.length; i++) {
                            switch(label[i]) {
                                case "skrining_nyeri_content":
                                    // NONE
                                    break;
                                case "skrining_nyeri_lokalis":
                                    var geometry = pemeriksaan[label[i]];
                                    skrining_nyeri.geometry = geometry;
                                    skrining_nyeri.redraw();
                                    break;
                                case "nilai_skrining_nyeri":
                                    let inputInstance = $(`#${label[i]}`).data("ionRangeSlider");
                                    inputInstance.update({
                                        from: pemeriksaan[label[i]]
                                    });
                                    break;
                                default: 
                                    if($(`input[name="${label[i]}"]`).is(':text') || $(`[name="${label[i]}"]`).is('textarea') || $(`[name="${label[i]}"]`).is('select')) {
                                        if(pemeriksaan[label[i]]) $(`[name="${label[i]}"]`).val(pemeriksaan[label[i]]).change();
                                    } else {   
                                        if(pemeriksaan[label[i]]) $(`input[name="${label[i]}"][value="${pemeriksaan[label[i]]}"]`).click();
                                    }
                                    break;
                            }
                        }
                        //$.uniform.update();
                    }
                }

                getTotalSkor('#table-skrining_gizi');
                getTotalSkor('#table-newss_pasien_dewasa');

                $("#pelayanan_id").val(data.id);
                $("#pelayanan_uid").val(data.uid);

                FORM.find(".label-nama_pasien").html(data.pasien.nama);
                FORM.find(".label-no_rm").html(data.pasien.no_rm);
                FORM.find(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                FORM.find(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                FORM.find(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                FORM.find(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                FORM.find(".label-alamat").html(data.pasien.alamat);
                FORM.find(".label-no_telepon").html(data.pasien.no_telepon_1);

                FORM.find(".label-no_register").html(data.no_register);
                FORM.find(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                FORM.find(".label-cara_bayar").html(data.cara_bayar);
                FORM.find(".label-layanan").html(data.layanan);
                FORM.find(".label-dokter").html(data.dokter);
                FORM.find(".label-ruang").html(data.ruang);
                FORM.find(".label-kelas").html(data.kelas);
                FORM.find(".label-bed").html(data.bed);

                FORM.find(".label-no_register").html(data.no_register);
                FORM.find(".label-layanan").html(data.layanan);
                FORM.find(".label-dokter").html(data.dokter);
                FORM.find(".label-cara_bayar").html(data.cara_bayar);
                FORM.find(".label-perusahaan").html(data.perusahaan);
                FORM.find(".label-no_jaminan").html(data.no_jaminan);
                FORM.find(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                FORM.find(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            FORM.find(".div-label_penjamin_perusahaan").show();
                            FORM.find(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            FORM.find(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            FORM.find(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        FORM.find(".div-label_perusahaan").show();
                        FORM.find(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        FORM.find(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        FORM.find(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $.unblockUI();
            }
        });

        /*if(MODE === "view") {
            $("input[type=text], textarea, select").prop("disabled", true);
            $(".btn-save").hide();
        }*/
    }

    let initializeForm = () => {
        $('input[name="pernah_dirawat"]').click(function() {
            $(".div-detail_pernah_dirawat").hide();
            $("#kapan").val('');
            $("#dimana").val('');
            $("#diagnosis").val('');
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-detail_pernah_dirawat").show('slow');
        });

        $('input[name="alergi"]').click(function() {
            $(".div-alergi_ke").hide();
            $(".div-alergi_ke_lainnya").hide();
            $('input:radio[name=alergi_ke]').prop('checked', false);
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-alergi_ke").show('slow');
        });

        $('input[name="alergi_ke"]').click(function() {
            $("#alergi_ke_lainnya").val('');
            $(".div-alergi_ke_lainnya").hide();

            let val = $(this).val();
            if(val === "lainnya") $(".div-alergi_ke_lainnya").show('slow');
        });

        $('#table-skrining_gizi').on('click', '.check-row', function() {
            getTotalSkor('#table-skrining_gizi');
        });

        $('#table-newss_pasien_dewasa').on('click', '.check-row', function() {
            let total_skor = 0;
            getTotalSkor('#table-newss_pasien_dewasa');
        });

        $('.btn-kembali').click(function() {
            window.location.assign(URL.index);
        });

        FORM.validate({
            rules: {
                alasan_masuk_rs: { required: true },
            },
            messages: {},
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 2000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        fillForm(UID);
    }

    initializeForm();
});