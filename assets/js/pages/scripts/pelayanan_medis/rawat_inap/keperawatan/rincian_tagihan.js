$(() => {
    let initTable = (el, data) => {
        $(el).DataTable({
            "info": false,
            "paginate": false,
            "scrollY": 250,
            "scrollCollapse": true,
            "data": data,
            "columns": [
                { 
                    "data": "tanggal",
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return data ? moment(data).format('DD-MM-YYYY HH:mm') : '&mdash;'
                    },
                },
                { 
                    "data": "uraian",
                    "render": function (data, type, row, meta) {
                        return data ? data : "&mdash;";
                    },
                },
                { 
                    "data": "tarif",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return numeral(data).format();
                    },
                },
                { 
                    "data": "quantity",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return numeral(data).format();
                    },
                },
                { 
                    "data": "quantity", // discount
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return numeral(0).format();
                    },
                },
                { 
                    "data": "id",
                    "searchable": false,
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        let tarif = isNaN(parseFloat(row.tarif)) ? 0 : parseFloat(row.tarif);
                        let quantity = isNaN(parseFloat(row.quantity)) ? 0 : parseFloat(row.quantity);
                        let discount = 0;
                        let jumlah = (tarif * quantity) + discount;
                        return `<label class="jumlah-row">${numeral(jumlah).format('0.0,')}</label>`;
                    },
                },
            ],
            "drawCallback": function (settings) {
                // DELETE THEAD OVER on SCROLL TBODY
                let trScrollBody = $('.dataTables_scrollBody').find('table thead tr');
                trScrollBody.each(function() {
                    $(this).css('height', 0).css('border', 0);
                    $(this).find('td').removeClass('sorting_asc').removeClass('sorting_desc').removeClass('sorting');
                });


                let subTotal = 0;
                let tr = $(el).find("tbody tr");
                tr.each(function() {
                    let jumlah = parseFloat(numeral($(this).find('.jumlah-row').html())._value);
                    subTotal += jumlah;
                });
                
                let parent = $(el).parents('.table-responsive');
                parent.find('.td-sub_total').html('Rp. ' + numeral(subTotal).format('0.0,'));
            }
        });
    }

    let calcTagihan = () => {
        let total = 0, sisa = 0;
        let deposit = parseFloat(numeral($(labelDeposit).html())._value);
        $('.td-sub_total').each(function() {
            total += parseFloat(numeral($(this).html())._value);
        });
        $(labelTotalTagihan).html('Rp. ' + numeral(total).format('0.0,'));

        sisa = total - deposit;
        sisa = (sisa < 0) ? 0 : sisa;
        $(labelSisaTagihan).html('Rp. ' + numeral(sisa).format('0.0,'));
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".section-rawat_inap").show();
    
                let data = res.data;
                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-ruang").html(data.ruang);
                $(".label-kelas").html(data.kelas);
                $(".label-bed").html(data.bed);

                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }

                $(labelDeposit).html('Rp. ' + numeral(data.deposit).format('0.0,'));
                initTable(tableRajal, data.rajal);
                initTable(tableIgd, data.igd);
                initTable(tableBiayaSewa, data.biaya_sewa);
                initTable(tableVisiteDokter, data.visite_dokter);
                initTable(tableTindakan, data.tindakan);
                initTable(tableObat, data.obat);
                initTable(tableBmhp, data.bmhp);
                initTable(tableLab, data.lab);
                initTable(tableRad, data.rad);
                calcTagihan();

                $.unblockUI();
            }
        });
    }

    $('.btn-kembali').click(function() {
        window.location.assign(URL.index);
    });

    fillForm(UID);
});