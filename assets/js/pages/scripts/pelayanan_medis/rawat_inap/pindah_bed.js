$(() => {
    let EL = {
        tanggal: '#modal-input_pindah_bed_tanggal',
        jam: '#modal-input_pindah_bed_jam',
        ruang_id: '#modal-input_pindah_bed_ruang_id',
        kelas_id: '#modal-input_pindah_bed_kelas_id',
        bed_id: '#modal-input_pindah_bed_bed_id',
        titip: '#modal-input_pindah_bed_titip',
        titip_kelas_id: '#modal-input_pindah_bed_titip_kelas_id',
        submit: '#btn-save_pindah_bed',
    }

    /*$(modalPindahBed).find('.input-date').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
    });*/

    $(modalPindahBed).find(".styled").uniform({
        radioClass: 'choice'
    });

    let setValidateForm = (obj, mode) => {
        $(obj.element).rules("remove");
        if(mode === "add") $(obj.element).rules('add', obj.rules);
    }

    let fillSelect = (obj, element) => {
        let parent = $(element).parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            let list = data.list;

            if (status === 'success') {
                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${list[i].id}" ${selected}>${list[i].nama}</option>`;
                }
                $(element).html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    let fillForm = (uid) => {
        $.getJSON(urlPindahBed.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                let labelBed = data.bed;
                labelBed += data.titip == 1 ? `<br/><span class="text-size-mini text-info">(${data.titip_kelas})</span>` : "";

                $(modalPindahBed).find(`input[name=id]`).val(data.id);
                $(modalPindahBed).find(`input[name=uid]`).val(uid);
                $(modalPindahBed).find(`input[name=dokter_id]`).val(data.dokter_id);
                $(modalPindahBed).find(`input[name=old_ruang_id]`).val(data.ruang_id);
                $(modalPindahBed).find(`input[name=old_kelas_id]`).val(data.kelas_id);
                $(modalPindahBed).find(`input[name=old_bed_id]`).val(data.bed_id);
                //$(EL.tanggal).pickadate('picker').set('select', moment().format('DD/MM/YYYY'));
                $(EL.jam).val(moment().format('HH:mm'));

                $(EL.titip_kelas_id).val("").change();
                $(EL.titip).attr("checked", false);
                $('.section-titip').hide();

                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();

                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(data.tanggal);
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $(".section-rawat_inap").show();
                $(".label-ruang").html(data.ruang);
                $(".label-kelas").html(data.kelas);
                $(".label-bed").html(labelBed);
                $(".label-ruang_lama").html(data.ruang);
                $(".label-kelas_lama").html(data.kelas);
                $(".label-bed_lama").html(labelBed);

                fillSelect({
                    url: `${base_url}/api/master/ruang/get_by_jenis?jenis=rawat_inap`,
                    value: '',
                }, EL.ruang_id);

                fillSelect({
                    url: `${base_url}/api/master/kelas/get_all`,
                    value: '',
                }, EL.kelas_id);

                fillSelect({
                    url: `${base_url}/api/master/bed/get_all?ruang_id=&kelas_id=`,
                    value: '',
                }, EL.bed_id);

                fillSelect({
                    url: `${base_url}/api/master/kelas/get_all`,
                    value: '',
                }, EL.titip_kelas_id);

                $.uniform.update();
            }
        });
    }

    $(`${EL.ruang_id}, ${EL.kelas_id}`).change(function() {
        let ruang_id = $(EL.ruang_id).val() ? btoa($(EL.ruang_id).val()) : '';
        let kelas_id = $(EL.kelas_id).val() ? btoa($(EL.kelas_id).val()) : '';
        fillSelect({
            url:  `${base_url}/api/master/bed/get_all?ruang_id=${ruang_id}&kelas_id=${kelas_id}`,
            value: '',
        }, EL.bed_id);
    });

    $(EL.titip).click(function() {
        if($(EL.ruang_id).val() == "") {
            warningMessage('Peringatan !', 'Ruangan pilih terlebih dahulu.');
            $(this).attr("checked", false);
            $.uniform.update();
            return;
        }

        $(EL.titip_kelas_id).val("").change();
        $('.section-titip').hide();
        setValidateForm({element: EL.titip_kelas_id}, "remove");
        if($(this).prop('checked') === true) {
            $('.section-titip').show('slow');
            setValidateForm({
                element: EL.titip_kelas_id,
                rules: { required: true }   
            }, "add");
        }
    });

    $(EL.titip_kelas_id).change(function() {
        let titip_kelas_id = $(EL.titip_kelas_id).val();
        let kelas_id = $(EL.kelas_id).val();

        if(parseInt(titip_kelas_id) === parseInt(kelas_id)) {
            $(EL.titip_kelas_id).val("").change();
            warningMessage('Peringatan !', 'Silahkan pilih Kelas yang berbeda.');
            return;
        }
    });

    $(formPindahBed).validate({
        rules: {
            tanggal: { required: true },
            jam: { required: true },
            bed_id: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalPindahBed + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: urlPindahBed.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    successMessage('Success', "Pindah bed berhasil dilakukan.");
                    setTimeout(() => {
                        $(modalPindahBed + ' .modal-dialog').unblock();
                        $(modalPindahBed).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalPindahBed + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    $(modalPindahBed).on('show.bs.modal', function () {
        let uid = $(modalPindahBed).find(`input[name=uid]`).val();
        fillForm(uid);
    });
});