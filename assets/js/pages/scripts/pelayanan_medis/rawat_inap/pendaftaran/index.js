$(() => {
    let listen = (event) => {
        eventSource = new EventSource(URL.listen.replace(':EVENT', event));
        eventSource.addEventListener(event, function(e) {
            TABLE_DT.draw(false);
        }, false);
    }

    let fetchDataSelect = (url, element) => {
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let datas = res.data;

                element.empty();
                element.append(`<option value="">${fieldAll}</option/>`);
                for (let data of datas) {
                    $("<option/>")
                        .prop('value', data.id)
                        .html(data.nama)
                        .appendTo(element);
                }
                element.val('').trigger('change');
            }
        });
    }

    let initializeTable = (browse) => {
        let columns = [];
        columns.push(
                    {
                        "data": "no_register",
                        "render": (data, type, row, meta) => {
                            return `<a class="form-row" data-uid="${row.uid}" data-popup="tooltip" title="Proses Rujukan Ranap">${data}</a>`;
                        },
                    },
                    {
                        "data": "tanggal",
                        "render": (data, type, row, meta) => {
                            return moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';
                        },
                    },
                    { "data": "no_rm" },
                    { "data": "pasien" },
                    { "data": "asal_pasien" },
                    { "data": "layanan" },
                    {
                        "data": "cara_bayar",
                        "render": (data, type, row, meta) => {
                            let tmp = data;
                            if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                            return tmp;
                        },
                    },
                    { "data": "dokter" },
                    /*{
                        "data": "uid",
                        "orderable": false,
                        "render": (data, type, row, meta) => {
                            let tmp = `<a data-uid="${row.pelayanan_uid}" data-id="${row.pelayanan_id}" data-popup="tooltip" title="Batalkan?" class="btn btn-warning btn-xs batal-row"><i class="fa fa-times"></i></a>`;
                            if(parseInt(browse) === 2) {
                                tmp = `<a href="${URL.statusPemeriksaan.replace(':UID', row.pelayanan_uid)}" data-popup="tooltip" title="Lihat Status Pemeriksaan" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>`;
                                if(parseInt(row.batal) === 1)
                                    tmp = `<label class="label label-warning" data-popup="tooltip" title="${row.batal_alasan}">Batal</label>`;
                            }
                            return tmp;
                        },
                        "className": "text-center",
                    },*/
                );

        // DATATABLE
        switch(browse) {
            case 1:
                TABLE_DT = TABLE.DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 1),
                      "type": "POST",
                      "data": function(p) {
                          p.no_register = $('#table_search_no_register').val();
                          p.no_rekam_medis = $('#table_search_no_rekam_medis').val();
                          p.nama = $('#table_search_nama').val();
                          p.asal_pasien = $('#table_search_asal_pasien').val();
                          p.poli_id = $('#table_search_poli').val();
                          p.cara_bayar_id = $('#table_search_cara_bayar_id').val();
                          p.dokter_id = $('#table_search_dokter_id').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [0, "asc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE.find('[data-popup=tooltip]').tooltip();
                    }
                });

                fetchDataSelect(URL.fetchCaraBayar, $('#table_search_cara_bayar_id'));
                fetchDataSelect(URL.fetchDokter, $('#table_search_dokter_id'));
                break;
        }
    }

    $('#btn-reset').on('click', () => {
        $('.input-table_tab1').val('').trigger('change');
    });

    $('.input-table_tab1').on('change keyup blur', function () {
        TABLE_DT.draw();
    });

    TABLE.on("click", ".form-row", function () {
        let uid = $(this).data('uid');
        blockPage('Form Pendaftaran sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.form.replace(':UID', uid) + "?mode=rujukan");
        }, 1000);
    });

    $('#table_search_asal_pasien').change(function() {
        $('#tab-1').find('.section-rawat_jalan').hide();
        $('#tab-1').find('.section-rawat_inap').hide();
        let val = parseInt($(this).val());
        switch(val) {
            case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
                $('#search-buat_poli').val("").change();
                $('#tab-1').find('.section-rawat_jalan').show('slow');
                break;
        }
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          default:
            TABLE_DT.draw(false);
            break;
        }
    });

    initializeTable(1);
    listen("daftar-rujukan_ranap");
});