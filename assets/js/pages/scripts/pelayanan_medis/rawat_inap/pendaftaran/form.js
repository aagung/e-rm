$(() => {
    $('.section-rawat_jalan').hide();
    $('.section-rawat_inap').show();

    $(".styled").uniform({
        radioClass: 'choice'
    });

    let fetchDataSelect = (id, url, element) => {
        id = id || "";
        let parent = $(element).parent();
        parent.find('.loading-select').show();
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let options = '';
                let datas = res.data ? res.data : res.list;

                $(element).empty();
                $(element).append('<option value="" selected="selected">- Pilih -</option/>');
                for (let data of datas) {
                    let selected = "";
                    if(data.id === id) selected = `selected="selected"`;

                    let value = `<option value="${data.id}" ${selected}>${data.nama}</option>`;                    
                    switch(element) {
                        case "#cara_bayar_id":
                            value = `<option value="${data.id}" ${selected} data-jenis="${data.jenis}">${data.nama}</option>`;
                            break;
                    }
                    options += value;
                }
                $(element).append(options);
                $(element).trigger('change');
            }
            parent.find('.loading-select').hide();
        });
    }

    let setValidateForm = (obj, mode) => {
        FORM.find(obj.element).rules("remove");
        if(mode === "add") FORM.find(obj.element).rules('add', obj.rules);
    }

    let addDokter = (dokter_id) => {
        let SECTION_DOKTER = $('#section-dokter');
        let length = SECTION_DOKTER.find('.input-group').length;

        let inputGroup = $('<div/>')
            .addClass('input-group mb-5')
            .appendTo(SECTION_DOKTER);
            let spanLoading = $('<span/>')
                .addClass('input-group-addon loading-select')
                .html('<i class="icon-spinner2 spinner"></i>')
                .appendTo(inputGroup);
            spanLoading.hide();
            let selectDokter = $('<select/>')
                .addClass('form-control select') 
                .prop('name', 'dokter_id[]')
                .appendTo(inputGroup);
            selectDokter.select2();

            if(parseInt(length) > 0) {
                let spanRemove = $('<span/>')
                    .addClass('input-group-addon bg-danger')
                    .prop('style', 'cursor: pointer')
                    .html('<i class="fa fa-times"></i>')
                    .appendTo(inputGroup);
                spanRemove.click(function() {
                    inputGroup.remove();
                });
            }

        fetchDataSelect(dokter_id, URL.fetchDokter, selectDokter);
    }

    let fillFormPelayanan = (uid) => {
        blockElement(modalPelayanan + ' .modal-dialog');
        $.getJSON(URL.getDataPelayanan.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-alamat").html(data.pasien.alamat);

                FORM.find('#tmp_rumah_sakit_id').val(data.rumah_sakit_id);
                FORM.find('#tmp_perusahaan_id').val(data.perusahaan_id);
                FORM.find('#tmp_no_jaminan').val(data.no_jaminan);
                FORM.find('#tmp_no_kartu').val(data.no_kartu);
                FORM.find('#tmp_penjamin_id').val(data.penjamin_id);
                FORM.find('#tmp_penjamin_perusahaan_id').val(data.penjamin_perusahaan_id);
                FORM.find('#tmp_penjamin_no_jaminan').val(data.penjamin_no_jaminan);
                FORM.find('#asal_pelayanan_id').val(data.id);                    
                FORM.find('#pasien_id').val(data.pasien_id);                    
                FORM.find('#asal_pasien').val(data.asal_pasien);                    

                FORM.find('#disp-no_register').val(data.no_register);
                FORM.find('#rujukan_dari').val(data.rujukan_dari).change();                    
                FORM.find('#disp-rujukan_dari').html(data.asal_pasien_desc);                    
                FORM.find('#nama_perujuk').val(data.nama_perujuk);                    
                FORM.find('#no_jaminan').val(data.no_jaminan);                    
                FORM.find('#penjamin_id').val(data.penjamin_id).change();                    
                FORM.find('#penjamin_no_jaminan').val(data.penjamin_no_jaminan);                    
                FORM.find('#disp-dokter_perujuk').html(data.dokter);                    
                fetchDataSelect(data.cara_bayar_id, URL.fetchCaraBayar, '#cara_bayar_id');
                addDokter(data.dokter_id);

                $(modalPelayanan + ' .modal-dialog').unblock();
            }
        });
    }

    let initializeTablePelayanan = () => {
        tablePelayanan = $('#table-modal_pelayanan').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadDataPelayanan,
                "type": "POST",
                "data": function(p) {
                    p.no_register = FORM_SEARCH_MODAL_FIELD.no_register.val();
                    p.no_rekam_medis = FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val();
                    p.nama = FORM_SEARCH_MODAL_FIELD.nama.val();
                }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="select-row" title="Pilih Pasien">${data}</a>`;
                        return tmp;
                    },
                },
                {
                    "data": "pasien_id",
                    "render": (data, type, row, meta) => {
                        let tmp = row.pasien;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { 
                    "data": "layanan_id",
                    "render": (data, type, row, meta) => {
                        return row.layanan;
                    },
                },
                {
                    "data": "cara_bayar_id",
                    "render": (data, type, row, meta) => {
                        let tmp = row.cara_bayar;
                        if(row.cara_bayar.search(/asuransi/i) !== -1 || row.cara_bayar.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { 
                    "data": "dokter_id",
                    "render": (data, type, row, meta) => {
                        return row.dokter;
                    },
                },
            ],
            "order": [ [1, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_pelayanan').find('[data-popup=tooltip]').tooltip();
            },
        });
    }

    let fillFormRujukan = (uid) => {
        $.getJSON(URL.getDataRujukan.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                BTN_CARI_PASIEN.hide();

                let data = res.data;
                fillFormPelayanan(data.pelayanan.uid);
                $('#rujukan_id').val(data.id);
                $('#ruang_id').val(data.ruang_id);
                $('#kelas_id').val(data.kelas_id);
                $('#bed_id').val(data.bed_id);

                $('.label-ruang').html(data.ruang ? data.ruang : "&mdash;");
                $('.label-kelas').html(data.kelas ? data.kelas : "&mdash;");
                $('.label-bed').html(data.bed ? data.bed : "&mdash;");

                fetchDataSelect("", URL.fetchKelas, '#titip_kelas_id');
            }
        });
    }

    let fillForm = (uid) => {
        if(UID == 0) {
            fetchDataSelect("", URL.fetchKelas, '#titip_kelas_id');
            addDokter("");
        }
    }

    let initializeForm = () => {
        BTN_PILIH_RUANGAN.click(function() {
            //selectedRuangan = {};
            $(modalKatalog).modal('show');
        });

        $(modalKatalog).on('hidden.bs.modal', function () {
            if(selectedRuangan) {
                $('#ruang_id').val(selectedRuangan.ruang_id);
                $('#kelas_id').val(selectedRuangan.kelas_id);
                $('#bed_id').val(selectedRuangan.id);

                $('.label-ruang').html(selectedRuangan.ruang);
                $('.label-kelas').html(selectedRuangan.kelas);
                $('.label-bed').html(selectedRuangan.bed);
            }
        });

        FORM.find('#rujukan_dari').change(function() {
            FORM.find('.section-rujukan_fktp').hide('slow');
            FORM.find('.section-rujukan_fkrtl').hide('slow');
            setValidateForm({element: '#nama_perujuk'}, "remove");
            setValidateForm({element: '#rumah_sakit_id'}, "remove");

            let el = {};
            let optionText = $(this).find('option:selected').text();
            if(optionText.search(/fktp/i) !== -1) {
                el['sectionInput'] = '.section-rujukan_fktp';
                el['element'] = '#nama_perujuk';
            } else if(optionText.search(/fkrtl/i) !== -1) {
                el['sectionInput'] = '.section-rujukan_fkrtl';
                el['element'] = '#rumah_sakit_id';
                fetchDataSelect(FORM.find('#tmp_rumah_sakit_id').val(), URL.fetchRumahSakit, '#rumah_sakit_id');
            }

            FORM.find(el.sectionInput).show('slow');
            setValidateForm({
                element: el.element,
                rules: { required: true }   
            }, "add");
        });

        FORM.find('#cara_bayar_id').change(function() {
            let val = parseInt($(this).val());
            let jenis = $(this).find('option:selected').data('jenis');
            let optionText = $(this).find('option:selected').text();

            FORM.find('.section-perusahaan').hide('slow');
            FORM.find('.section-no_jaminan').hide('slow');
            FORM.find('.section-no_kartu').hide('slow');
            FORM.find('.section-penjamin').hide('slow');
            FORM.find('.section-penjamin_perusahaan').hide('slow');
            FORM.find('.section-penjamin_no_jaminan').hide('slow');
            FORM.find('.input-cara_bayar').val('').change();
            
            // SEP BPJS
            FORM.find('#btn-sep').hide('slow');
            FORM.find('.section-no_jaminan input').attr('readonly', false);

            setValidateForm({element: '#perusahaan_id'}, "remove");
            setValidateForm({element: '#no_jaminan'}, "remove");
            setValidateForm({element: '#no_kartu'}, "remove");
            setValidateForm({element: '#penjamin_perusahaan_id'}, "remove");
            setValidateForm({element: '#penjamin_no_jaminan'}, "remove");

            FORM.find('#no_jaminan').val($('#tmp_no_jaminan').val());
            FORM.find('#no_kartu').val($('#tmp_no_kartu').val());
            FORM.find('#penjamin_id').val($('#tmp_penjamin_id').val()).change();
            switch (jenis) {
                case imediscode.CARA_BAYAR_BPJS:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('No. SEP');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-no_kartu').show('slow');
                    setValidateForm({
                        element: '#no_kartu',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-penjamin').show('slow');
                    FORM.find('#btn-sep').show('slow');
                    FORM.find('.section-no_jaminan input').attr('readonly', true);
                    break;
                case imediscode.CARA_BAYAR_JAMKESDA:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('No. Jamkesda');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
                case imediscode.CARA_BAYAR_ASURANSI:
                case imediscode.CARA_BAYAR_PERUSAHAAN:
                    let labelPerusahaan = 'Perusahaan';
                    let labelNoJaminan = 'NIK';
                    if(parseInt(jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                        labelPerusahaan = 'Asuransi';
                        labelNoJaminan = 'No. Anggota';

                        let q = 0;
                        $('#penjamin_id option').each(function() {
                            if("perusahaan" == $(this).text().toLowerCase()) 
                                q = $(this).val();
                        });
                        fetchDataSelect(FORM.find('#tmp_penjamin_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(q)), '#penjamin_perusahaan_id');
                        FORM.find('.section-penjamin_perusahaan').show('slow').children('label').html('Perusahaan');
                    }

                    fetchDataSelect(FORM.find('#tmp_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(jenis)), '#perusahaan_id');
                    FORM.find('.section-perusahaan').show('slow').children('label').html(labelPerusahaan);
                    setValidateForm({
                        element: '#perusahaan_id',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-no_jaminan').show('slow').children('label').html(labelNoJaminan);
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
                case imediscode.CARA_BAYAR_INTERNAL:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('NIK');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
            }
        });

        FORM.find('#penjamin_id').change(function() {
            FORM.find('.section-penjamin_perusahaan').hide('slow');
            FORM.find('.section-penjamin_no_jaminan').hide('slow');

            let val = isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
            if(val !== 0) {
                FORM.find('.input-penjamin').val('').change();
                setValidateForm({element: '#penjamin_perusahaan_id'}, "remove");
                setValidateForm({element: '#penjamin_no_jaminan'}, "remove");

                let labelPerusahaan = 'Perusahaan';
                let labelNoPenjamin = 'NIK';
                if(val === imediscode.CARA_BAYAR_ASURANSI) {
                    labelPerusahaan = 'Asuransi';
                    labelNoPenjamin = 'No. Anggota';
                }

                fetchDataSelect(FORM.find('#tmp_penjamin_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(val)), '#penjamin_perusahaan_id');
                FORM.find('.section-penjamin_perusahaan').show('slow').children('label').html(labelPerusahaan);
                setValidateForm({
                    element: '#penjamin_perusahaan_id',
                    rules: { required: true }   
                }, "add");

                FORM.find('.section-penjamin_no_jaminan').show('slow').children('label').html(labelNoPenjamin);
                FORM.find('#penjamin_no_jaminan').val($('#tmp_penjamin_no_jaminan').val());
                setValidateForm({
                    element: '#penjamin_no_jaminan',
                    rules: { required: true }   
                }, "add");
            }
        });

        FORM.validate({
            rules: {
                no_register: { required: true },
                tanggal: { required: true },
                rujukan_dari: { required: true },
                cara_bayar_id: { required: true },
                'dokter_id[]': { required: true },
            },
            focusInvalid: true,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;

                $('html, body').animate({
                    scrollTop: FORM.offset().top
                }, 1000);
            },
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                if($("#ruang_id").val() == "") {
                    warningMessage('Peringatan !', 'Ruangan pilih terlebih dahulu.');
                    return;
                }

                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Pendaftaran berhasil dilakukan.");

                        UID = result.data;
                        $('.btn-form').hide('slow');
                        $('.section-cetak').show('slow');
                        $('.btn-tambah_baru').show('slow');
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        $('[name="titip"]').click(function() {
            if($("#ruang_id").val() == "") {
                warningMessage('Peringatan !', 'Ruangan pilih terlebih dahulu.');
                $(this).attr("checked", false);
                $.uniform.update();
                return;
            }

            $('#titip_kelas_id').val("").change();
            $('.section-titip').hide();
            setValidateForm({element: '#titip_kelas_id'}, "remove");
            if($(this).prop('checked') === true) {
                $('.section-titip').show('slow');
                setValidateForm({
                    element: '#titip_kelas_id',
                    rules: { required: true }   
                }, "add");
            }
        });

        $('#titip_kelas_id').change(function() {
            let titip_kelas_id = $('#titip_kelas_id').val();
            let kelas_id = $('#kelas_id').val();

            if(parseInt(titip_kelas_id) === parseInt(kelas_id)) {
                $('#titip_kelas_id').val("").change();
                warningMessage('Peringatan !', 'Silahkan pilih Kelas yang berbeda.');
                return;
            }
        });

        BTN_CARI_PASIEN.click(function () {
            let isDTable = $.fn.dataTable.isDataTable($('#table-modal_pelayanan'));
            if(isDTable === false) {
                initializeTablePelayanan();
            } else tablePelayanan.draw();
            $(modalPelayanan).modal('show');
        });

        BTN_SEARCH_MODAL.click(function () {
            tablePelayanan.draw();
        });

        $('#table-modal_pelayanan').on('click', '.select-row', function() {
            SECTION_DOKTER.find(".input-group").remove();

            let uid = $(this).data('uid');
            fillFormPelayanan(uid);
            $(modalPelayanan).modal('hide');
        });

        BTN_CANCEL.click(function() {
            window.location.assign(URL.index);
        });

        BTN_SAVE.click(function() {
            FORM.find("[type=submit]").click();
        });

        BTN_ADD_DOKTER.click(function() {
            addDokter("");
        });

        $('.btn-tambah_baru').click(function() {
            window.location.assign(`${URL.index}/form`);
        });

        // Button Cetak
        $(".btn-cetak_nota").click(function () {
            window.open(URL.cetakNota.replace(':UID', UID));
        });

        $(".btn-cetak_tracer").click(function () {
            window.open(URL.cetakTracer.replace(':UID', UID));
        });

        $(".btn-cetak_id_card").click(function () {
            window.open(URL.cetakIdCard.replace(':UID', UID));
        });
    }

    initializeForm();

    if(MODE === "rujukan") {
        fillFormRujukan(UID);
    } else fillForm(UID);
});