$(() => {
    let EL = {
        submit: '#btn-save_pindah_bed',
    }

    $(modalCheckout).find('.input-date').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
    });

    $(modalCheckout).find(".styled").uniform({
        radioClass: 'choice'
    });

    let fillForm = (uid) => {
        $.getJSON(urlCheckout.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                let labelBed = data.bed;
                labelBed += data.titip == 1 ? `<br/><span class="text-size-mini text-info">(${data.titip_kelas})</span>` : "";

                $(modalCheckout).find(`input[name=id]`).val(data.id);
                $(modalCheckout).find(`input[name=uid]`).val(uid);
                $(modalCheckout).find(`input[name=bed_id]`).val(data.bed_id);
                $(modalCheckout).find(`[name=tanggal_pulang]`).pickadate('picker').set('select', moment().format('DD/MM/YYYY'));
                $(modalCheckout).find(`[name=jam_pulang]`).val(moment().format('HH:mm'));

                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(data.tanggal);
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $(".section-rawat_inap").show();
                $(".label-ruang").html(data.ruang);
                $(".label-kelas").html(data.kelas);
                $(".label-bed").html(labelBed);

                $.uniform.update();
            }
        });
    }

    $(formCheckout).validate({
        rules: {
            tanggal_pulang: { required: true },
            jam_pulang: { required: true },
            tranpotasi_pulang: { required: true },
            yang_mendampingi: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalCheckout + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: urlCheckout.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    successMessage('Success', "Checkout berhasil dilakukan.");
                    setTimeout(() => {
                        $(modalCheckout + ' .modal-dialog').unblock();
                        $(modalCheckout).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalCheckout + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    $(modalCheckout).on('show.bs.modal', function () {
        let uid = $(modalCheckout).find(`input[name=uid]`).val();
        fillForm(uid);
    });
});