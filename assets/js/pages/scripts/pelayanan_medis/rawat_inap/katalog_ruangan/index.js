$(() => {
    let TABLE_DT;
    let EL = {
        table: '#katalog-table',
        ruang_id: '#katalog-search_ruang_id',
        kelas_id: '#katalog-search_kelas_id',
        status: '#katalog-search_status',
        modal: '#modal-katalog_ruangan',
    }

    let fillSelect = (obj, element) => {
        let parent = $(element).parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            let list = data.list
            if (status === 'success') {
                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${list[i].id}" ${selected}>${list[i].nama}</option>`;
                }
                $(element).html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    let initializeTable = (browse) => {
        TABLE_DT = $(EL.table).DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
              "url": `${base_url}/api/pelayanan_medis/rawat_inap/katalog_ruangan/load_data`,
              "type": "POST",
              "data": function(p) {
                  p.ruang_id = btoa($(EL.ruang_id).val());
                  p.kelas_id = btoa($(EL.kelas_id).val());
                  p.status = $(EL.status).val();
              }
            },
            "columns": [
                {
                    "data": "ruang",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-popup="tooltip" href="${base_url}/pelayanan_medis/rawat_inap/katalog_ruangan/form/${row.ruang_uid}" class="detail-row" title="Lihat Ruangan">${data}</a>`;
                        if(browse === "rawat_inap") {
                            tmp = data;
                            if(row.status_bed.match(/kosong/i))
                                tmp = `<a data-popup="tooltip" class="select-row" title="Pilih Ruangan">${data}</a>`;
                        }
                        return tmp;
                    },
                },
                { "data": "kelas" },
                { "data": "bed" },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = "&mdash;";
                        if(data) {
                            tmp = data;
                            tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        }
                        return tmp;
                    },
                    "orderable": false,
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        let tmp = data ? data : "&mdash;";
                        if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                    "orderable": false,
                },
                { 
                    "data": "dokter",
                    "render": (data, type, row, meta) => {
                        return data ? data : "&mdash;";
                    },
                    "orderable": false,
                },
                { "data": "status_bed" },
            ],
            "order": [ [0, "asc"] ],
            "drawCallback": function (oSettings) {
                $(EL.table).find('[data-popup=tooltip]').tooltip();
            }
        });

        fillSelect({
            url:  `${base_url}/api/master/ruang/get_by_jenis?jenis=rawat_inap`,
            value: 0,
        }, $(EL.ruang_id));

        fillSelect({
            url:  `${base_url}/api/master/kelas/get_all`,
            value: 0,
        }, $(EL.kelas_id));
    }

    $(`${EL.ruang_id}, ${EL.kelas_id}, ${EL.status}`).change(function() {
        TABLE_DT.draw();
    });
    initializeTable();

    // KATALOG RUANGAN IN RAWAT INAP
    $(EL.modal).on('show.bs.modal', function () {
        let isDTable = $.fn.dataTable.isDataTable($(EL.table));
        if(isDTable === true) $(EL.table).DataTable().destroy();
        initializeTable("rawat_inap");
    });

    $(EL.table).on('click', '.select-row', function() {
        let tr = $(this).closest('tr');
        selectedRuangan = TABLE_DT.row(tr).data();

        $(EL.modal).modal("hide");
    });

});