$(() => {
	let slider = null,
		TEMPLATES = {},
		bedKelas = {},
		fasilitasTmp = [],
		formUpload = $("#form-upload"),
		modalUpload = $("#modal-upload"),
		formChangeFoto = $("#form-change_foto"),
    	modalChangeFoto = $("#modal-change_foto");

	let EL = {
        listBed: '#content-beds',
        panelKelas: '#panel-kelas',
        namaKelas: '#span-nama_kelas',
        displayFotos: '#content-fotos',
        jumlahBed: '#span-jumlah_bed',
        jumlahBedKosong: '#span-jumlah_bed_tersedia',
        jumlahBedIsi: '#span-jumlah_bed_terisi',
        jumlahBayi: '#span-jumlah_bayi',
        jumlahAnak: '#span-jumlah_anak',
        jumlahWanita: '#span-jumlah_wanita',
        jumlahPria: '#span-jumlah_pria',
        btnAddFasilitas: '#btn-add_fasilitas',
        listFasilitas: '#content-fasilitas',
        btnAddFoto: '#btn-add_foto',
        listFoto: '#div-list_foto',
    }

    TEMPLATES.BED = 
		'<div class="col-sm-4">' + 
		    '<div class="panel panel-body bed-panel" id="bed-{{UID}}">' + 
		        '<div class="media">' +
		            '<li class="dropdown" style="list-style-type: none; float: left;">' +
		                '<a href="#" class="media-left media-middle dropdown-toggle" data-toggle="dropdown">' + 
		                    '<i class="icon-bed2 bed-indicator {{BED_STATUS}}"></i>' +
		                '</a>' +
		                /*'<ul class="dropdown-menu dropdown-menu-left">' +
		                    '<li class="{{CLASS_DISABLED}}"><a href="#" class="bed-status-toggle" data-status="1" data-id="{{ID}}" data-uid="{{UID}}"><i class="icon-bed2 bed-available" style="font-size: 16px;"></i> Tersedia</a></li>' +
		                    '<li class="{{CLASS_DISABLED}}"><a href="#" class="bed-status-toggle" data-status="3" data-id="{{ID}}" data-uid="{{UID}}"><i class="icon-bed2 bed-prepared" style="font-size: 16px;"></i> Disiapkan</a></li>' +
		                    '<li class="{{CLASS_DISABLED}}"><a href="#" class="bed-status-toggle" data-status="4" data-id="{{ID}}" data-uid="{{UID}}"><i class="icon-bed2 bed-renovation" style="font-size: 16px;"></i> Renovasi</a></li>' +
		                '</ul>' +*/
		            '</li>' +
		            '<div class="media-body">' +
		                '<h6 class="media-heading">{{BED}}</h6>' +
		                '<span class="text-muted">{{PASIEN_CARA_BAYAR}}</span>' +
		            '</div>' +
		            '<div class="media-right media-middle">' +
		                '<ul class="icons-list icons-list-vertical {{ADA_PASIEN}}">' +
		                    '<li class="dropdown">' +
		                        '<span class="dropdown-toggle cursor-pointer" data-trigger="hover" data-popup="popover" title="# {{PASIEN_NO_REKAM_MEDIS}}" data-html="true" data-content="<div class=\'form-group\'><div class=\'col-xs-3\'><i class=\'icon-user\'></i></div><div class=\'col-xs-9\'>{{PASIEN_NAMA}}</div></div><div class=\'form-group\'><div class=\'col-xs-3\'><i class=\'fa fa-venus-mars\'></i></div><div class=\'col-xs-9\'>{{PASIEN_JENIS_KELAMIN}}</div></div><div class=\'form-group\'><div class=\'col-xs-3\'><i class=\'icon-shield-check\'></i></div><div class=\'col-xs-9\'>{{PASIEN_CARA_BAYAR}}</div></div><div class=\'form-group\'><div class=\'col-xs-3\'><i class=\'icon-calendar2\'></i></div><div class=\'col-xs-9\'>{{PASIEN_TANGGAL_DIRAWAT}}</div></div><div class=\'form-group\'><div class=\'col-xs-3\'><i class=\'icon-med-i-surgery\'></i></div><div class=\'col-xs-9\'>{{PASIEN_LAMA_DIRAWAT}}</div></div><div>&nbsp;</div>"><i class="icon-bed2 fa fa-eye"></i></span>' +
		                    '</li>' +
		                '</ul>' +
		            '</div>' +
		        '</div>' +
		    '</div>' +
		'</div>';

	let renderBed = (data) => {
	    // Check if kelas row !exists
	    if (! $('#' + bedKelas[data.kelas]).length) {
	        let id = 'beds-' + makeid(10);
	        bedKelas[data.kelas] = id;
	        let row = $('<div/>').addClass('row pl-10 kelas-toggle')
	            .prop('id', id)
	            .data('kelas_id', data.kelas_id)
	            .data('kelas_uid', data.kelas_uid);
	        row.append('<div class="text-size-small text-uppercase text-semibold text-muted mb-10">' + data.kelas + '</div>');
	        
	        $(EL.listBed).append(row);
	    }

	    let bedStatus = data.status_bed;
	    let isDisabled = data.is_disabled;

	    let bed = TEMPLATES.BED;
	    bed = bed.replace(/\{\{CLASS_DISABLED\}\}/g, isDisabled);
	    bed = bed.replace(/\{\{ID\}\}/g, data.id);
	    bed = bed.replace(/\{\{UID\}\}/g, data.uid);
	    bed = bed.replace(/\{\{BED_STATUS\}\}/g, bedStatus);
	    bed = bed.replace(/\{\{BED\}\}/g, data.nama);
	    bed = bed.replace(/\{\{ADA_PASIEN\}\}/g, bedStatus == 'bed-full' ? '' : 'hide');
	    if(data.pelayanan) {
	    	let tanggalMasuk = moment(data.pelayanan.tanggal_masuk).isValid() ? moment(data.pelayanan.tanggal_masuk).format('DD/MM/YYYY') : '';
		    let tanggalCheckout = moment(data.pelayanan.tanggal_checkout).isValid() ? moment(data.pelayanan.tanggal_checkout).format('DD/MM/YYYY') : 'Sekarang';
		    let tanggalDirawat = tanggalMasuk + ' s/d ' + tanggalCheckout;

		    bed = bed.replace(/\{\{PASIEN_NO_REKAM_MEDIS\}\}/g, data.pelayanan.no_rm);
		    bed = bed.replace(/\{\{PASIEN_NAMA\}\}/g, data.pelayanan.pasien);
		    bed = bed.replace(/\{\{PASIEN_JENIS_KELAMIN\}\}/g, parseInt(data.pelayanan.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
		    bed = bed.replace(/\{\{PASIEN_CARA_BAYAR\}\}/g, data.pelayanan.cara_bayar);
		    bed = bed.replace(/\{\{PASIEN_TANGGAL_DIRAWAT\}\}/g, tanggalDirawat);
		    bed = bed.replace(/\{\{PASIEN_LAMA_DIRAWAT\}\}/g, data.pelayanan.lama_dirawat ? data.pelayanan.lama_dirawat + ' Hari' : '&mdash;');
	    } else {
		    bed = bed.replace(/\{\{PASIEN_CARA_BAYAR\}\}/g, '');
	    }
	    $('#' + bedKelas[data.kelas]).append(bed);
	}

    TEMPLATES.FASILITAS = 
		'<p class="fasilitas-row" id="fasilitas-row-{{UID}}">' +
		    '<span class="fasilitas-input" style="display:none;">' +
		        '<input type="text" class="form-control input-fasilitas" id="input-fasilitas-{{UID}}" value="{{FASILITAS}}" />' +
		        '<button class="btn btn-xs btn-link pull-right btn-fasilitas_batal" data-id="{{ID}}" data-uid="{{UID}}"><i class="fa fa-remove"></i></button>' +
		        '<button class="btn btn-xs btn-link pull-right btn-fasilitas_save" data-id="{{ID}}" data-uid="{{UID}}"><i class="fa fa-save"></i></button>' +
		        '<span class="clearfix"></span>' +
		    '</span>' +
		    '<span class="fasilitas-label">' +
		        '<i class=" icon-checkmark4"></i> <span class="fasilitas">{{FASILITAS}}</span>' +
		        '<button class="btn btn-xs btn-link pull-right btn-fasilitas_hapus" data-id="{{ID}}" data-uid="{{UID}}"><i class="fa fa-trash"></i></button>' +
		        '<button class="btn btn-xs btn-link pull-right btn-fasilitas_edit" data-id="{{ID}}" data-uid="{{UID}}"><i class="fa fa-edit"></i></button>' +
		        '<span class="clearfix"></span>' +
		    '</span>' +
		'</p>';

	let renderFasilitas = (data, jqEl) => {
	    var fasilitas = TEMPLATES.FASILITAS.replace(/\{\{FASILITAS\}\}/g, data.nama)
	        .replace(/\{\{ID\}\}/g, data.id)
	        .replace(/\{\{UID\}\}/g, data.uid);

	    jqEl.append(fasilitas);
	}	

	TEMPLATES.FOTO = 
		'<div class="col-lg-6 col-sm-12 foto-kelas-container" id="foto-kelas-{{UID}}">' +
		    '<div class="thumb mb-10">' +
		        '<img src="{{THUMB_URL}}" class="img-responsive img-rounded" alt="">' +
		        '<div class="caption-overflow">' +
		            '<span>' +
		                '<a href="{{PHOTO_URL}}" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded btn-preview-foto-kelas"><i class="icon-eye"></i></a>' + 
		                '<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-upload-foto-kelas" data-id="{{ID}}" data-uid="{{UID}}" data-photourl="{{PHOTO_URL}}"><i class="icon-link2"></i></a>' +
		                '<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-remove-foto-kelas" data-id="{{ID}}" data-uid="{{UID}}" data-photourl="{{PHOTO_URL}}"><i class="icon-cross2"></i></a>' +
		            '</span>' +
		        '</div>' +
		    '</div>' +
		'</div>';
	
	let renderFoto = (data, jqEl) => {
	    var foto = TEMPLATES.FOTO
	        .replace(/\{\{THUMB_URL\}\}/g, data.thumburl)
	        .replace(/\{\{PHOTO_URL\}\}/g, data.oriurl)
	        .replace(/\{\{ID\}\}/g, data.id)
	        .replace(/\{\{UID\}\}/g, data.uid);

	    jqEl.append(foto);
	}

	TEMPLATES.SLIDER_FOTO = 
		`<li data-thumb="{{THUMB_URL}}" data-src="{{THUMB_URL}}">` +
	        '<img src="{{PHOTO_URL}}" style="width: 100%; height: 300px;">' +
		'</li>';

	let renderSliderFoto = (data, jqEl) => {
	    var foto = TEMPLATES.SLIDER_FOTO
	        .replace(/\{\{THUMB_URL\}\}/g, data.thumburl)
	        .replace(/\{\{PHOTO_URL\}\}/g, data.oriurl);

	    jqEl.append(foto);
	}

	let listen = (event) => {
        eventSource = new EventSource(URL.listen.replace(':EVENT', event));
        eventSource.addEventListener(event, function(e) {
			fillContentBed(UID, function () {
		        $('[data-popup="popover"]').popover();
		    });
        }, false);
    }

	let fillContentBed = (uid, callback) => {
	    $.getJSON(URL.getListBed, function (res, status) {
	        if (status === 'success') {
	            data = res.data;

	            $(EL.listBed).empty();
	            for (var i = 0; i < data.length; i++) {
	                renderBed(data[i]);
	            }

	            if (data[0]) {
	                fillPanelKelas(data[0].kelas_uid, data[0].ruang_id, function () {
	                    $('[data-popup="lightbox"]').fancybox({
	                        padding: 3
	                    });
	                });
	            }

	            $(EL.listBed).closest('div').addClass('col-lg-8');
	            $(EL.panelKelas).parent().show();
	        }

	        if (callback) callback();
	    });
	}

	let fillPanelKelas = (uid, ruangUid, callback) => {
		blockElement(EL.panelKelas);
	    $.getJSON(URL.getFasilitas.replace(':UID', uid).replace(':RUANG_UID', btoa(ruangUid)), function (data, status) {
	        if (status === 'success') {
	            data = data.data;

	            $("#kelas_id").val(data.id);
	            $("#kelas_uid").val(data.uid);
	            $(EL.namaKelas).html(data.nama.match(/kelas/i) ? data.nama : 'Kelas ' + data.nama);

	            // Jumlah Bed
	            $(EL.jumlahBed).html(numeral(data.jumlah_bed).format('0,0'));
	            $(EL.jumlahBedKosong).html(numeral(data.jumlah_bed_kosong).format('0,0'));
	            $(EL.jumlahBedIsi).html(numeral(data.jumlah_bed_isi).format('0,0'));

	            // Jumlah Kategori Umur
	            $(EL.jumlahBayi).html(numeral(data.jumlah_bayi).format('0,0'));
	            $(EL.jumlahAnak).html(numeral(data.jumlah_anak).format('0,0'));
	            $(EL.jumlahWanita).html(numeral(data.jumlah_perempuan).format('0,0'));
	            $(EL.jumlahPria).html(numeral(data.jumlah_tuan).format('0,0'));

	            // Fasilitas
	            $(EL.listFasilitas).empty();
	            for (var i = 0; i < data.fasilitas_list.length; i++) {
	                renderFasilitas(data.fasilitas_list[i], $(EL.listFasilitas));
	            }

	            // Foto Kelas
	            $(EL.listFoto).empty();
	            $(EL.displayFotos).empty();
	            for (var i = 0; i < data.foto_list.length; i++) {
	                data.foto_list[i].showPreview = true;
	                data.foto_list[i].showUpload = true;
	                data.foto_list[i].showChange = true;
	                data.foto_list[i].showDelete = true;
	                renderFoto(data.foto_list[i], $(EL.listFoto));
	            	renderSliderFoto(data.foto_list[i], $(EL.displayFotos));
	            }

	            if(slider) slider.destroy();
	            slider = $(EL.displayFotos).lightSlider({
	            	gallery: true,
			        item: 1,
			        loop: true,
			        auto: true,
			        pauseOnHover: true,
	            });
	        }
	        if (callback) callback();
	        $(EL.panelKelas).unblock();
	    });
	}

	$('body').on('click', '.kelas-toggle', function() {
		let kelas_uid = $(this).data('kelas_uid');
		fillPanelKelas(kelas_uid, ID, function () {
            $('[data-popup="lightbox"]').fancybox({
                padding: 3
            });
        });
	});

	// FASILITAS HANDLER
	$(EL.btnAddFasilitas).click(function (e) {
        e.preventDefault();
        let tempuid = makeid(10);
        let fasilitas = TEMPLATES.FASILITAS.replace(/\{\{FASILITAS\}\}/g, '')
            .replace(/\{\{ID\}\}/g, 0)
            .replace(/\{\{UID\}\}/g, tempuid);

        $(EL.listFasilitas).append(fasilitas);

        let row = $('#fasilitas-row-' + tempuid);
        row.find('.fasilitas-input').show();
        row.find('.fasilitas-label').hide();
        row.find('.input-fasilitas').focus();
        row.data('add', '1');
    });

    $('body').on('click', '.btn-fasilitas_edit', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        
        let row = $('#fasilitas-row-' + uid);
        row.find('.fasilitas-input').show();
        row.find('.fasilitas-label').hide();
        row.find('.input-fasilitas').focus();

        fasilitasTmp[uid] = row.find('.input-fasilitas').val();
    });

    $('body').on('click', '.btn-fasilitas_batal', function (e) {
    	e.preventDefault();
        let id = $(this).data('id');
        let uid = $(this).data('uid');

        let row = $('#fasilitas-row-' + uid);
        if (row.data('add') == '1') {
            row.remove();
            return;
        }
        
        row.find('.fasilitas-input').hide();
        row.find('.fasilitas-label').show();
        row.find('.input-fasilitas').val(fasilitasTmp[uid]);
    });

    $('body').on('click', '.btn-fasilitas_hapus', function () {
        var id = $(this).data('id');
        var uid = $(this).data('uid');
        $.post(URL.deleteFasilitas, {uid: uid}, function (data, status) {
            if (status === 'success') {
                $('#fasilitas-row-' + uid).addClass('animated lightSpeedOut');
                setTimeout(function () {
                    $('#fasilitas-row-' + uid).remove();
                }, 500);
            }
        });
    });

    $('body').on('click', '.btn-fasilitas_save', function() {
    	let id = $(this).data('id');
        let uid = $(this).data('uid');
        let kelasId = $("#kelas_id").val();

        let row = $('#fasilitas-row-' + uid);
        let text = row.find('.input-fasilitas').val();
        if (text.trim() == "") {
            row.tooltip({
                title: 'Silahkan isi data dibawah.',
                trigger: 'manual'
            });
            row.tooltip('show');
            return;
        }
        row.tooltip('destroy');
        
        blockElement('#fasilitas-row-' + uid);
        data = {
            id: id,
            uid: uid,
            ruang_id: ID,
            kelas_id: kelasId,
            nama: row.find('.input-fasilitas').val()
        };

        if (row.data('add') == '1') {
            delete data['id'];
        }

        $.ajax({
            url: URL.saveFasilitas,
            type: 'POST',
            dataType: "json",
            data: data,
            success: function (data) {
            	data = data.data;
                row.find('.fasilitas-input').hide();
                row.find('.fasilitas-label').show();
                row.find('.fasilitas').html(row.find('.input-fasilitas').val());

                row.data('add', 0);
                row.prop('id', 'fasilitas-row-' + data.uid);
                row.find('.input-fasilitas').prop('id', 'input-fasilitas-' + data.uid);
                row.find('.btn-fasilitas_batal').data('id', data.id).data('uid', data.uid);
                row.find('.btn-fasilitas_save').data('id', data.id).data('uid', data.uid);
                row.find('.btn-fasilitas_hapus').data('id', data.id).data('uid', data.uid);
                row.find('.btn-fasilitas_edit').data('id', data.id).data('uid', data.uid);
            },
            error: function () {
                errorMessage('Peringatan !', 'Terjadi kesalahan ketika hendak menyimpan data.');
            },
            complete: function () {
                row.unblock();
            }
        });	
    });

    $(document).on('keypress', '.input-fasilitas', function (e) {
    	let row = $(this).closest('.fasilitas-row');
        if (e.keyCode == 13) row.find('.btn-fasilitas_save').click();
    });

    // FOTO HANDLER
    var maxImageWidth = 1280;
    var maxImageHeight = 720;
    formUpload.dropzone({
        url: URL.uploadFoto,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 1,
        dictDefaultMessage: 'Drop file to upload <span>or CLICK</span>',
        autoProcessQueue: true,
        acceptedFiles: 'image/*',
        init: function() {
            this.on('addedfile', function(file){
                if (this.fileTracker) {
                    this.removeFile(this.fileTracker);
                }
                this.fileTracker = file;
            });

            this.on('thumbnail', function (file) {
                if (file.width > maxImageWidth || file.height > maxImageHeight) {
                    file.rejectDimensions()
                } else {
                    file.acceptDimensions();
                }
            });
        },
        accept: function(file, done) {
            file.acceptDimensions = done;
            file.rejectDimensions = function() { 
                errorMessage('Peringatan !', 'Resolusi foto terlalu besar.');
                done("Invalid dimension."); 
            };
        },
        success: function (file, response) {
            var obj = $.parseJSON(response);
            /*if ( $("#foto-kelas-" + obj.uid).length ) {
                var element = $("#foto-kelas-" + obj.uid);
                element.find('img').prop('src', obj.thumburl);
                element.find('.btn-preview-foto-kelas').prop('href', obj.oriurl);
                element.find('.btn-upload-foto-kelas').data('id', obj.id).data('uid', obj.uid);
                element.find('.btn-remove-foto-kelas').data('id', obj.id).data('uid', obj.uid);
            } else {
                renderFoto(obj, $(EL.listFoto));
            }*/
            fillPanelKelas($('#kelas_uid').val(), ID, function () {
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            });
            modalUpload.modal('hide'); 
        }
    });

    $(document).on('click', '.btn-upload-foto-kelas', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        let ruangId = ID;
        let kelasId = $("#kelas_id").val();

        $('#upload_id').val(id);
        $('#upload_uid').val(uid);
        $('#upload_ruang_id').val(ruangId);
        $('#upload_kelas_id').val(kelasId);

        modalUpload.modal('show');
    });

    $(EL.btnAddFoto).click(function (e) {
        e.preventDefault();
        let id = 0;
        let uid = 0;
        let ruangId = ID;
        let kelasId = $("#kelas_id").val();

        $('#upload_id').val(id);
        $('#upload_uid').val(uid);
        $('#upload_ruang_id').val(ruangId);
        $('#upload_kelas_id').val(kelasId);

        modalUpload.modal('show');
    });

    $(document).on('click', '.btn-remove-foto-kelas', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let uid = $(this).data('uid');

        if (id == '0') return;

        var container = $(this).closest('.foto-kelas-container');
        blockElement(container.selector);

        var data = {
            id: id,
            uid: uid,
        };

        $.ajax({
            url: URL.deleteFoto,
            type: 'POST',
            dataType: "json",
            data: data,
            success: function (data) {
                container.addClass('animated lightSpeedOut');
                setTimeout(function () {
                	fillPanelKelas($('#kelas_uid').val(), ID, function () {
		                $('[data-popup="lightbox"]').fancybox({
		                    padding: 3
		                });
		            });
                }, 2000);
            },
            error: function () {
                errorMessage('Peringatan !', 'Terjadi kesalahan saat menyimpan menghapus foto.');
            },
            complete: function () {
                container.unblock();
            }
        });
    });

    listen('katalog-ruangan');
})