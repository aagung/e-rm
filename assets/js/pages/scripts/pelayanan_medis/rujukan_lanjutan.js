$(() => {
    let enableLoad = false;
    let EL = {
        layanan_id: '#modal-input_rujukan_lanjutan_layanan_id',
        dokter_id: '#modal-input_rujukan_lanjutan_dokter_id',
        tanggal: '#modal-input_rujukan_lanjutan_tanggal',
        diagnosa: '#modal-input_rujukan_lanjutan_diagnosa',
        terapi: '#modal-input_rujukan_lanjutan_terapi',
        pemeriksaan_penunjang: '#modal-input_rujukan_lanjutan_pemeriksaan_penunjang',
        tujuan_merujuk: '#modal-input_rujukan_lanjutan_tujuan_merujuk',
        alasan_merujuk: '#modal-input_rujukan_lanjutan_alasan_merujuk',
        submit: '#btn-save_rujukan_lanjutan',
        showCppt: '#btn-show_rujukan_cppt',

        // RAWAT INAP
        bantuan_konsultasi: '#modal-input_rujukan_lanjutan_bantuan_konsultasi',
        keterangan: '#modal-input_rujukan_lanjutan_keterangan',
    }

    $(".styled").uniform({
        radioClass: 'choice'
    });

    $(EL.tanggal).pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        min: moment()
    });

    let setValidateForm = (obj, mode) => {
        $(modalRujukanLanjutan).find(obj.element).rules("remove");
        if(mode === "add") $(modalRujukanLanjutan).find(obj.element).rules('add', obj.rules);
    }

    let fillForm = (uid) => {
        enableLoad = false;
        $.getJSON(`${base_url}/api/pelayanan_medis/rujukan/rujukan_lanjutan/get_data/${uid}`, function (res, status) {
            if (status === 'success') {
                let statusTitle = "";
                let data = res.data;
                if($(modalRujukanLanjutan).find(`input[name=asal_pasien]`).val() == imediscode.UNIT_LAYANAN_RAWAT_INAP) {
                    $(modalRujukanLanjutan).find('.section-rawat_inap').removeClass('hide');    
                }

                $(modalRujukanLanjutan).find(`input[name=uid]`).val(uid);
                $(modalRujukanLanjutan).find(`input[name=booking_id]`).val(data.booking_id);
                $(modalRujukanLanjutan).find(`input[name=jenis][value="${data.jenis}"]`).click();
                $(modalRujukanLanjutan).find(`input[name=jenis_konsultasi][value="${data.jenis_konsultasi}"]`).click();
                $(EL.tanggal).val(moment(data.tanggal).format('DD/MM/YYYY'));
                $(EL.diagnosa).val(data.diagnosa);
                $(EL.terapi).val(data.terapi);
                $(EL.pemeriksaan_penunjang).val(data.pemeriksaan_penunjang);
                $(EL.tujuan_merujuk).val(data.tujuan_merujuk);
                $(EL.alasan_merujuk).val(data.alasan_merujuk);
                $(EL.bantuan_konsultasi).val(data.bantuan_konsultasi).change();
                $(EL.keterangan).val(data.keterangan);

                fillSelect({
                    url: `${base_url}/api/pelayanan_medis/rawat_jalan/rawat_jalan/fetch_layanan`,
                    value: data.layanan_id,
                }, $(EL.layanan_id));

                fillSelect({
                    url: `${base_url}/api/master/dokter/fetch_all?q=${btoa(parseInt(data.layanan_id))}`,
                    value: data.dokter_id,
                }, $(EL.dokter_id));
                
                $(EL.submit).show();
                $(EL.showCppt).prop('data-pelayanan_uid', "").hide();
                $(modalRujukanLanjutan).find('input, textarea, select').prop('disabled', false);  
                if(data.status != 0) {
                   $(EL.submit).hide();
                   $(modalRujukanLanjutan).find('input, textarea, select').prop('disabled', true);  

                   switch(parseInt(data.status)) {
                       case 1:
                           if(data.rujukan_pelayanan_uid) 
                               $(EL.showCppt).prop('data-pelayanan_uid', data.rujukan_pelayanan_uid).show();
                           statusTitle = ` <span class="label label-success text-center">Diproses</span>`;
                           break;
                       case 2:
                           statusTitle = ` <span class="label label-warning text-center">Dibatalkan</span>`;
                           break;
                   }
                } 
                $(modalRujukanLanjutan).find('.modal-title').html('Rujukan Lanjutan' + statusTitle);
                $.uniform.update();
                
                setTimeout(() => {
                    enableLoad = true;
                    $(modalRujukanLanjutan).modal('show');
                }, 300);
            }
        });
    }

    let fillSelect = (obj, element) => {
        let parent = element.parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            if (status === 'success') {
                let list = data.data; 

                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${list[i].id}" ${selected}>${list[i].nama}</option>`;
                }
                element.html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    $(btnRujukanLanjutan).click(function() {
        let uid = $(this).data('uid') === undefined ? "" : $(this).data('uid');
        fillForm(uid);
    });

    $(modalRujukanLanjutan).find('input[name="jenis"]').click(function() {
        $(modalRujukanLanjutan).find('.input-rujukan_lanjutan').val('').change();

        setValidateForm({element: EL.tanggal}, "remove");
        setValidateForm({element: EL.layanan_id}, "remove");
        setValidateForm({element: EL.dokter_id}, "remove");
        setValidateForm({element: EL.diagnosa}, "remove");
        setValidateForm({element: EL.tujuan_merujuk}, "remove");
        setValidateForm({element: EL.alasan_merujuk}, "remove");
        setValidateForm({element: EL.bantuan_konsultasi}, "remove");
        $('.section-spesialis_lain').hide();
        $('.section-luar').hide();

        let val = $(this).val();
        switch(val) {
            case "luar":
                setValidateForm({
                    element: EL.diagnosa,
                    rules: { required: true }   
                }, "add");

                setValidateForm({
                    element: EL.alasan_merujuk,
                    rules: { required: true }   
                }, "add");

                setValidateForm({
                    element: EL.tujuan_merujuk,
                    rules: { required: true }   
                }, "add");

                $('.section-luar').show('slow');
                break;
            default:
                setValidateForm({
                    element: EL.tanggal,
                    rules: { required: true }   
                }, "add");

                setValidateForm({
                    element: EL.layanan_id,
                    rules: { required: true }   
                }, "add");

                setValidateForm({
                    element: EL.dokter_id,
                    rules: { required: true }   
                }, "add");

                if($(modalRujukanLanjutan).find(`input[name=asal_pasien]`).val() == imediscode.UNIT_LAYANAN_RAWAT_INAP) {
                    setValidateForm({
                        element: EL.bantuan_konsultasi,
                        rules: { required: true }   
                    }, "add");                    
                }

                $('.section-spesialis_lain').show('slow');
                break;
        }
    });

    $(EL.layanan_id).change(function() {
        let layanan_id = parseInt($(EL.layanan_id).val());
        if(enableLoad) {
            let dokter_id = $(EL.dokter_id).val() ? $(EL.dokter_id).val() : '';
            fillSelect({
                url: `${base_url}/api/master/dokter/fetch_all?q=${btoa(layanan_id)}`,
                value: dokter_id,
            }, $(EL.dokter_id));
        }
    });

    $(formRujukanLanjutan).validate({
        rules: {
            jenis: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalRujukanLanjutan + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: `${base_url}/api/pelayanan_medis/rujukan/rujukan_lanjutan/save`,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    RUJUKAN.lanjutan_uid = result;
                    successMessage('Success', "Rujukan lanjutan berhasil disimpan.");
                    setTimeout(() => {
                        $(modalRujukanLanjutan + ' .modal-dialog').unblock();
                        $(modalRujukanLanjutan).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalRujukanLanjutan + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });
});