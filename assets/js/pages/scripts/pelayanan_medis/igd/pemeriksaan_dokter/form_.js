/*
*    modalTindakan, modalTindakanLayananId, selectedTindakan, modalBtnTambahTindakan => didapat dari file modal 'pelayanan_medis/search-tarif-pelayanan-modal.php'
*    modalBmhp, modalBmhpLayananId, selectedBmhp, modalBtnTambahBmhp => didapat dari file modal 'pelayanan_medis/search-bmhp-modal.php'
*    modalObat, modalObatMode, selectedObat, modalBtnTambahObat => didapat dari file modal 'pelayanan_medis/search-obat-modal.php'
*/

$(() => {
    let aDiagnosaSekunder = [];
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    $(".styled").uniform({
        radioClass: 'choice'
    });

    $("#nilai_skrining_nyeri").ionRangeSlider({
        type: "single",
        min: 0,
        max: 10,
        step: 1,
        grid: true,
        grid_snap: true,
        onChange: function(data) {
            console.log(data);
        }
    });

    $(".select-signa").select2({
        placeholder: "- Pilih -",
    });

    let fillTabPemeriksaan = (uid) => {
        blockElement(formPemeriksaan);
        $(formPemeriksaan).find("#id").val(0);
        $(formPemeriksaan).find("#uid").val('');

        $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'pemeriksaan'), function (res, status) {
            if (status === 'success') {
                $(".div-table_triage").html("");
                $(".div-ada_nyari").hide();
                $(".div-catatan_kehamilan").hide();
                $(".div-nyeri_hilang_lainnya").hide();
                $(".input-status_lokalis").val("").html("");
                enablePlanning = res.tab_planning;

                let data = res.data;
                if(data) {
                    $(formPemeriksaan).find("#id").val(data.id);
                    $(formPemeriksaan).find("#uid").val(data.uid);

                    if(data.data_pemeriksaan) {
                        let pemeriksaan = data.data_pemeriksaan;
                        let label = Object.keys(pemeriksaan);
                        for (var i = 0; i < label.length; i++) {
                            switch(label[i]) {
                                case "pediatrik_content":
                                case "adult_content":
                                    // NONE
                                    break;
                                case "pediatrik_lokalis":
                                case "adult_lokalis":
                                    var geometry = pemeriksaan[label[i]];
                                    if(label[i] == "pediatrik_lokalis") {
                                        pediatrik.geometry = geometry;
                                        pediatrik.redraw();
                                    } else {
                                        adult.geometry = geometry;
                                        adult.redraw();
                                    }
                                    break;
                                case "nilai_skrining_nyeri":
                                    let inputInstance = $(`#${label[i]}`).data("ionRangeSlider");
                                    inputInstance.update({
                                        from: pemeriksaan[label[i]]
                                    });
                                    break;
                                default: 
                                    if($(`input[name="${label[i]}"]`).is(':text') || $(`[name="${label[i]}"]`).is('textarea') || $(`[name="${label[i]}"]`).is('select')) {
                                        if(pemeriksaan[label[i]]) $(`[name="${label[i]}"]`).val(pemeriksaan[label[i]]).change();
                                    } else {   
                                        if(pemeriksaan[label[i]]) $(`input[name="${label[i]}"][value="${pemeriksaan[label[i]]}"]`).click();
                                    }
                                    break;
                            }
                        }
                        $.uniform.update();
                    }
                }
                $(formPemeriksaan).unblock();
            }
        });
    }

    let fillTabPlanning = (uid) => {
        listResepObat = [];

        blockElement(formPlanning);
        $(formPlanning).find("#id").val(0);
        $(formPlanning).find("#uid").val('');

        $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'planning'), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                if(data) {
                    $(formPlanning).find("#id").val(data.id);
                    $(formPlanning).find("#uid").val(data.uid);
                    $(formPlanning).find("#resep_id").val(data.resep.id);
                    $("[name=pemeriksaan_dokter_id]").val(data.id);

                    $('#modal-racikan_data_pasien').show();
                    $('#modal-label_racikan_berat_badan').html(data.pasien.berat_badan + 'Kg');
                    $(modalPreviewResep).find('.label-berat_badan').html(data.pasien.berat_badan + 'Kg');

                    // RESEP
                    dataRegistrasi.resep = data.resep;

                    // DIAGNOSA UTAMA
                    if(data.diagnosa_utama) {
                        let optDiagnosaUtama = new Option(data.diagnosa_utama.code, data.diagnosa_utama.uid, true, true);
                        $("#diagnosa_utama").append(optDiagnosaUtama).trigger('change');

                        $("#diagnosa_utama").trigger({
                            type: 'select2:select',
                            params: {
                                data: {
                                        id: data.diagnosa_utama.uid,
                                        code: data.diagnosa_utama.title,
                                        text: data.diagnosa_utama.code,
                                        description: data.diagnosa_utama.description,
                                    }
                            }
                        });
                    }

                    // DIAGNOSA SEKUNDER
                    $("#diagnosa_sekunder").html('');
                    if(data.diagnosa_sekunder) {
                        let optDiagnosaSekunder;
                        for (var i = 0; i < data.diagnosa_sekunder.length; i++) {
                            optDiagnosaSekunder = new Option(data.diagnosa_sekunder[i].code, data.diagnosa_sekunder[i].uid, true, true);
                            $("#diagnosa_sekunder").append(optDiagnosaSekunder).trigger('change');

                            $("#diagnosa_sekunder").trigger({
                                type: 'select2:select',
                                params: {
                                    data:  {
                                            id: data.diagnosa_sekunder[i].uid,
                                            code: data.diagnosa_sekunder[i].title,
                                            text: data.diagnosa_sekunder[i].code,
                                            description: data.diagnosa_sekunder[i].description,
                                        }
                                }
                            });
                        }
                    }

                    // KONDISI PASIEN 
                    if(data.kondisi_pasien) {
                        $(formPlanning).find("#kondisi_pasien").val(data.kondisi_pasien).change();
                        if(data.kondisi_pasien === "hidup") $(formPlanning).find(".div-tindakan_lebih_lanjut").show();
                    } else $(formPlanning).find(".div-tindakan_lebih_lanjut").show();
                    $(formPlanning).find("#tindakan_lebih_lanjut").val(data.tindakan_lebih_lanjut).change();
                    $(formPlanning).find("#ringkasan_kondisi_pasien").val(data.ringkasan_kondisi_pasien);

                    // TINDAKAN
                    $(tableTindakan + " tbody").empty();
                    for (var i = 0; i < data.tindakan_list.length; i++) {
                        addTindakan(data.tindakan_list[i]);
                    }

                    // BMHP
                    $(tableBmhp + " tbody").empty();
                    for (var i = 0; i < data.bmhp_list.length; i++) {
                        data.bmhp_list[i].old_quantity = data.bmhp_list[i].quantity;
                        addBmhp(data.bmhp_list[i]);
                    }


                    let listMode = "view";
                    $('#btn-tambah_racikan').parents('tfoot').show();
                    $('#btn-tambah_non_racikan').parents('tfoot').show();
                    if(parseInt(data.resep.status) !== 0) {
                        listMode = "view_noedit";
                        $('#btn-tambah_racikan').parents('tfoot').hide();
                        $('#btn-tambah_non_racikan').parents('tfoot').hide();
                    }
                    // NON RACIKAN
                    $(tableNonRacikan + " tbody").empty();
                    for (var i = 0; i < data.non_racikan_list.length; i++) {
                        addNonRacikan(data.non_racikan_list[i], listMode);
                    }

                    // RACIKAN
                    $(tableRacikan + " tbody").empty();
                    for (var i = 0; i < data.racikan_list.length; i++) {
                        addRacikan(data.racikan_list[i], listMode);
                    }

                    // RUJUKAN
                    RUJUKAN.ok_uid = "";
                    RUJUKAN.ranap_uid = "";
                    RUJUKAN.lab_uid = "";
                    RUJUKAN.rad_uid = "";
                    RUJUKAN.kunlang_uid = "";
                    RUJUKAN.lanjutan_uid = "";
                    if(data.rujukan_ok_uid) RUJUKAN.ok_uid = data.rujukan_ok_uid;
                    if(data.rujukan_ranap_uid) RUJUKAN.ranap_uid = data.rujukan_ranap_uid;
                    if(data.rujukan_lab_uid) RUJUKAN.lab_uid = data.rujukan_lab_uid;
                    if(data.rujukan_rad_uid) RUJUKAN.rad_uid = data.rujukan_rad_uid;
                    if(data.rujukan_kunjungan_ulang_uid) RUJUKAN.kunlang_uid = data.rujukan_kunjungan_ulang_uid;
                    if(data.rujukan_lanjutan_uid) RUJUKAN.lanjutan_uid = data.rujukan_lanjutan_uid;

                    setDisplayRujukan(RUJUKAN.ok_uid, "ok");
                    setDisplayRujukan(RUJUKAN.ranap_uid, "ranap");
                    setDisplayRujukan(RUJUKAN.lab_uid, "lab");
                    setDisplayRujukan(RUJUKAN.rad_uid, "rad");
                    setDisplayRujukan(RUJUKAN.kunlang_uid, "kunlang");
                    setDisplayRujukan(RUJUKAN.lanjutan_uid, "lanjutan");

                    initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.pelayanan.cara_bayar_id));

                    $('.btn-rujukan').attr('disabled', false);
                    $('.btn-table_tindakan').parents('tfoot').show();
                    if(parseInt(data.pelayanan.close) !== 0) {
                        $('.btn-rujukan').attr('disabled', true);
                        $('.btn-table_tindakan').parents('tfoot').hide();
                        $('.btn-b').parents('td').html('&mdash;');
                    }
                }
                $(formPlanning).unblock();
                $('a[href="#tab-planning"]').focus();
            }
        });
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getDataRegistrasi.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();
    
                let data = res.data;
                fillTabPemeriksaan(uid);
                $(formPemeriksaan).find("input[name=pelayanan_id]").val(data.id);
                $(formPemeriksaan).find("input[name=pelayanan_uid]").val(data.uid);
                $(formPemeriksaan).find("#dokter_id").val(data.dokter_id);
                $(formPlanning).find("input[name=pelayanan_id]").val(data.id);
                $(formPlanning).find("input[name=pelayanan_uid]").val(data.uid);
                $(formPlanning).find("#no_register").val(data.no_register);
                $(formPlanning).find("#pasien_id").val(data.pasien_id);
                $(formPlanning).find("#cara_bayar_id").val(data.cara_bayar_id);
                $(formPlanning).find("#perusahaan_id").val(data.perusahaan_id);
                $(formPlanning).find("#layanan_id").val(data.layanan_id);
                $(formPlanning).find("#dokter_id").val(data.dokter_id);

                // RUJUKAN
                $(formReservasiOk).find("input[name=pelayanan_id]").val(data.id);
                $(formReservasiOk).find("input[name=layanan_id]").val(data.layanan_id);

                $(formReservasiRanap).find("input[name=pelayanan_id]").val(data.id);
                $(formReservasiRanap).find("input[name=layanan_id]").val(data.layanan_id);

                $(formReservasiLab).find("input[name=pelayanan_id]").val(data.id);
                $(formReservasiLab).find("input[name=pasien_id]").val(data.pasien_id);
                $(formReservasiLab).find("input[name=cara_bayar_id]").val(data.cara_bayar_id);
                $(formReservasiLab).find("input[name=perusahaan_id]").val(data.perusahaan_id);
                $(formReservasiLab).find("input[name=kontraktor_id]").val(data.perusahaan_id);
                $(formReservasiLab).find("input[name=no_jaminan]").val(data.no_jaminan);
                $(formReservasiLab).find("input[name=layanan_id]").val(data.layanan_id);
                $(formReservasiLab).find("input[name=dokter_perujuk_id]").val(data.dokter_id);
                $(formReservasiLab).find("input[name=dokter_perujuk]").val(data.dokter);
                $(formReservasiLab).find("input[name=rujukan_dari]").val(data.layanan);
                $(formReservasiLab).find("input[name=nama]").val(data.pasien.nama);
                $(formReservasiLab).find("input[name=alamat]").val(data.pasien.alamat);
                $(formReservasiLab).find("input[name=no_identitas]").val(data.pasien.no_identitas);
                $(formReservasiLab).find("input[name=no_telepon]").val(data.pasien.no_telepon_1);
                $(formReservasiLab).find("input[name=jenis_kelamin]").val(data.pasien.jenis_kelamin);
                $(formReservasiLab).find("input[name=tanggal_lahir]").val(data.pasien.real_tanggal_lahir);

                $(formReservasiRad).find("input[name=pelayanan_id]").val(data.id);
                $(formReservasiRad).find("input[name=pasien_id]").val(data.pasien_id);
                $(formReservasiRad).find("input[name=cara_bayar_id]").val(data.cara_bayar_id);
                $(formReservasiRad).find("input[name=perusahaan_id]").val(data.perusahaan_id);
                $(formReservasiRad).find("input[name=kontraktor_id]").val(data.perusahaan_id);
                $(formReservasiRad).find("input[name=no_jaminan]").val(data.no_jaminan);
                $(formReservasiRad).find("input[name=layanan_id]").val(data.layanan_id);
                $(formReservasiRad).find("input[name=dokter_perujuk_id]").val(data.dokter_id);
                $(formReservasiRad).find("input[name=dokter_perujuk]").val(data.dokter);
                $(formReservasiRad).find("input[name=rujukan_dari]").val(data.layanan);
                $(formReservasiRad).find("input[name=nama]").val(data.pasien.nama);
                $(formReservasiRad).find("input[name=alamat]").val(data.pasien.alamat);
                $(formReservasiRad).find("input[name=no_identitas]").val(data.pasien.no_identitas);
                $(formReservasiRad).find("input[name=no_telepon]").val(data.pasien.no_telepon_1);
                $(formReservasiRad).find("input[name=jenis_kelamin]").val(data.pasien.jenis_kelamin);
                $(formReservasiRad).find("input[name=tanggal_lahir]").val(data.pasien.real_tanggal_lahir);

                $(formKunjunganUlang).find("input[name=pelayanan_id]").val(data.id);
                $(formKunjunganUlang).find("input[name=pasien_id]").val(data.pasien_id);
                $(formKunjunganUlang).find("input[name=cara_bayar_id]").val(data.cara_bayar_id);
                $(formKunjunganUlang).find("input[name=perusahaan_id]").val(data.perusahaan_id);
                $(formKunjunganUlang).find("input[name=layanan_id]").val(data.layanan_id);
                $(formKunjunganUlang).find("input[name=dokter_id]").val(data.dokter_id);
                $(formKunjunganUlang).find("input[name=no_rm]").val(data.pasien.no_rm);
                $(formKunjunganUlang).find("input[name=nama_pasien]").val(data.pasien.nama);
                $(formKunjunganUlang).find("input[name=no_telepon]").val(data.pasien.no_telepon_1);
                let data_pasien = JSON.stringify({
                    title_id: data.pasien.title_id,
                    tanggal_lahir: data.pasien.real_tanggal_lahir,
                    jenis_kelamin: data.pasien.jenis_kelamin,
                });                
                $(formKunjunganUlang).find("textarea[name=data_pasien]").val(data_pasien).html(data_pasien);

                $(formRujukanLanjutan).find("input[name=pelayanan_id]").val(data.id);
                $(formRujukanLanjutan).find("input[name=pasien_id]").val(data.pasien_id);
                $(formRujukanLanjutan).find("input[name=cara_bayar_id]").val(data.cara_bayar_id);
                $(formRujukanLanjutan).find("input[name=perusahaan_id]").val(data.perusahaan_id);
                $(formRujukanLanjutan).find("input[name=no_rm]").val(data.pasien.no_rm);
                $(formRujukanLanjutan).find("input[name=nama_pasien]").val(data.pasien.nama);
                $(formRujukanLanjutan).find("input[name=no_telepon]").val(data.pasien.no_telepon_1);
                data_pasien = JSON.stringify({
                    title_id: data.pasien.title_id,
                    tanggal_lahir: data.pasien.real_tanggal_lahir,
                    jenis_kelamin: data.pasien.jenis_kelamin,
                });                
                $(formRujukanLanjutan).find("textarea[name=data_pasien]").val(data_pasien).html(data_pasien);

                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $.unblockUI();

                dataRegistrasi = data;
            }
        });

        if(MODE === "view") {
            $("input[type=text], textarea, select").prop("disabled", true);
            $(".btn-save").hide();
        }

        $.uniform.update();
    }

    let setDisplayRujukan = (uid, browse) => {
        let btn;
        switch(browse) {
            case "ok":
                btn = btnReservasiOk;
                break;
            case "ranap":
                btn = btnReservasiRanap;
                break;
            case "lab":
                btn = btnReservasiLab;
                break;
            case "rad":
                btn = btnReservasiRad;
                break;
            case "kunlang":
                btn = btnKunjunganUlang;
                break;
            case "lanjutan":
                btn = btnRujukanLanjutan;
                break;
        }

        $(btn).data('uid', uid);
        $(btn).removeClass('btn-success');
        if(uid) $(btn).addClass('btn-success');
    }

    // TAB PEMERIKSAAN
    let calcNilaiKesadaran = (el) => {
        let bukaMata = isNaN(parseFloat($("#nilai_buka_mata").val())) ? 0 : parseFloat($("#nilai_buka_mata").val());
        let verbal = isNaN(parseFloat($("#nilai_verbal").val())) ? 0 : parseFloat($("#nilai_verbal").val());
        let reaksiMotorik = isNaN(parseFloat($("#nilai_reaksi_motorik").val())) ? 0 : parseFloat($("#nilai_reaksi_motorik").val());
        $(el).val(bukaMata + verbal + reaksiMotorik); 
    }

    $("#triage").change(function() {
        $(".div-table_triage").html("");
        let val = $(this).val();
        if(val) {
            let tmpTable = `<table class="table table-bordered">`;
            tmpTable += `      <thead>`;
            tmpTable += `          <tr>`;
            tmpTable += `              <th class="text-bold">Pemeriksaan (Examination)</th>`;
            tmpTable += `              <th class="text-bold">${val}</th>`;
            tmpTable += `              <th class="text-bold">&nbsp;</th>`;
            tmpTable += `          </tr>`;
            tmpTable += `      </thead>`;
            tmpTable += `      <tbody>`;
            switch(val) {
                case "RESUSITASI (Triage I)":
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>JALAN NAFAS (Airway)</td>`;
                    tmpTable += `       <td>Sumbatan (Obstruction)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="jalan_nafas_sumbatan" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td rowspan="3">PERNAFASAN (Breathing)</td>`;
                    tmpTable += `       <td>Henti Nafas (Breathing Arrest)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="henti_nafas" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>Frekuensi_nafas (RR) < 10x /mnt</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nafas_kurang_dari_10x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>Sianosis</td>`;
                    tmpTable += `       <td><input type="checkbox" name="sianosis" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td rowspan="4">SIRKULASI (CIRCULATION)</td>`;
                    tmpTable += `       <td>Henti Jantung (Cardiac Arrest)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="henti_jantung" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>Nadi Tidak Teraba (Pulseners)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="nadi_tidak_teraba" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>Pucat (Pale)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="pucat" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>Akral Dingin (Clammy)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="akral_dingin" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-danger">`;
                    tmpTable += `       <td>KESADARAN (Disability)</td>`;
                    tmpTable += `       <td>GCS < 9</td>`;
                    tmpTable += `       <td><input type="checkbox" name="gcs_kurang_dari_9" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    break;
                case "EMERGENCY (Triage II)":
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>JALAN NAFAS (Airway)</td>`;
                    tmpTable += `       <td>Bebas (Patent)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="jalan_nafas_bebas" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td rowspan="2">PERNAFASAN (Breathing)</td>`;
                    tmpTable += `       <td>Frekuensi_nafas (RR) < 32x /mnt</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nafas_kurang_dari_32x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>Wheezing</td>`;
                    tmpTable += `       <td><input type="checkbox" name="wheezing" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td rowspan="6">SIRKULASI (CIRCULATION)</td>`;
                    tmpTable += `       <td>Nadi Terasa Lemah (Weakness Pulse)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="nadi_terasa_lemah" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>Frek Nadi (HR) < 50x /mnt</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nadi_kurang_dari_50x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>Frek Nadi (HR) < 150x /mnt (DWS)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nadi_kurang_dari_150x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>Pucat (Pale)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="pucat" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>Akral Dingin (Clammy)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="akral_dingin" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>CRT < 2detik</td>`;
                    tmpTable += `       <td><input type="checkbox" name="crt_kurang_dari_2dtk" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-warning-400">`;
                    tmpTable += `       <td>KESADARAN (Disability)</td>`;
                    tmpTable += `       <td>GCS 9 - 12</td>`;
                    tmpTable += `       <td><input type="checkbox" name="gcs_9_sampai_12" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    break;
                case "URGENT (Triage III)":
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td>JALAN NAFAS (Airway)</td>`;
                    tmpTable += `       <td>Bebas (Patent)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="jalan_nafas_bebas" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td rowspan="2">PERNAFASAN (Breathing)</td>`;
                    tmpTable += `       <td>Frekuensi_nafas (RR) 24 - 32x /mnt</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nafas_24_sampai_32x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td>Wheezing</td>`;
                    tmpTable += `       <td><input type="checkbox" name="wheezing" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td rowspan="3">SIRKULASI (CIRCULATION)</td>`;
                    tmpTable += `       <td>Frek Nadi (HR) 120 - 150x /mnt</td>`;
                    tmpTable += `       <td><input type="checkbox" name="frekuensi_nadi_120_sampai_150x" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td>TD Sist > 160 mmHg</td>`;
                    tmpTable += `       <td><input type="checkbox" name="td_sist_lebih_dari_160_mmhg" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td>TD Diast > 100 mmHg</td>`;
                    tmpTable += `       <td><input type="checkbox" name="td_diast_lebih_dari_100_mmhg" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="bg-orange-400">`;
                    tmpTable += `       <td>KESADARAN (Disability)</td>`;
                    tmpTable += `       <td>GCS > 12</td>`;
                    tmpTable += `       <td><input type="checkbox" name="gcs_lebih_dari_12" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    break;
                case "NON URGENT (Triage IV)":
                case "FALSE EMERGENCY":
                    let trClass = "";
                    if(val === "NON URGENT (Triage IV)") trClass = "bg-teal-800";
                    tmpTable += `   <tr class="${trClass}">`;
                    tmpTable += `       <td>JALAN NAFAS (Airway)</td>`;
                    tmpTable += `       <td>Bebas (Patent)</td>`;
                    tmpTable += `       <td><input type="checkbox" name="jalan_nafas_bebas" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="${trClass}">`;
                    tmpTable += `       <td>PERNAFASAN (Breathing)</td>`;
                    tmpTable += `       <td>Normal</td>`;
                    tmpTable += `       <td><input type="checkbox" name="pernafasan_normal" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="${trClass}">`;
                    tmpTable += `       <td>SIRKULASI (CIRCULATION)</td>`;
                    tmpTable += `       <td>Normal</td>`;
                    tmpTable += `       <td><input type="checkbox" name="sirkulasi_normal" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    tmpTable += `   <tr class="${trClass}">`;
                    tmpTable += `       <td>KESADARAN (Disability)</td>`;
                    tmpTable += `       <td>GCS 15</td>`;
                    tmpTable += `       <td><input type="checkbox" name="gcs_15" value="ya" /></td>`;
                    tmpTable += `   </tr>`;
                    break;
            }
            tmpTable += `      </tbody>`;
            tmpTable += `   </table>`;

            $(".div-table_triage").html(tmpTable);
        }
    })

    $("#buka_mata").change(function() {
        let val = $(this).val();
        $("#nilai_buka_mata").val("");

        if(val) {
            let nilai = $(this).find('option:selected').data('nilai');
            $("#nilai_buka_mata").val(nilai);
        }
        calcNilaiKesadaran("#nilai_kesadaran");
    });

    $("#verbal").change(function() {
        let val = $(this).val();
        $("#nilai_verbal").val("");

        if(val) {
            let nilai = $(this).find('option:selected').data('nilai');
            $("#nilai_verbal").val(nilai);
        }
        calcNilaiKesadaran("#nilai_kesadaran");
    });

    $("#reaksi_motorik").change(function() {
        let val = $(this).val();
        $("#nilai_reaksi_motorik").val("");

        if(val) {
            let nilai = $(this).find('option:selected').data('nilai');
            $("#nilai_reaksi_motorik").val(nilai);
        }
        calcNilaiKesadaran("#nilai_kesadaran");
    });

    $('input[name="kehamilan"]').click(function() {
        $("#catatan_kehamilan").val('');
        $(".div-catatan_kehamilan").hide();

        if($(this).val() === "ya") $(".div-catatan_kehamilan").show('slow');
    });

    $('input[name="adakah_nyeri"]').click(function() {
        $(".div-ada_nyeri").find("input[type=text], textarea").val("");
        $(".div-ada_nyeri").find("input[type=checkbox], input[type=radio]").removeAttr("checked");
        $(".div-ada_nyeri").hide();

        if($(this).val() === "ya") $(".div-ada_nyeri").show('slow');
        $.uniform.update();
    });

    $('input[name="nyeri_hilang_bila_lainnya"]').click(function() {
        $("#nyeri_hilang_deskripisi_lainnya").val('');
        $(".div-nyeri_hilang_lainnya").hide();

        if($(this).prop('checked') === true) $(".div-nyeri_hilang_lainnya").show('slow');
    });

    $(formPemeriksaan).validate({
        rules: {
            triage: { required: true },
        },
        messages: {},
        focusInvalid: true,
        /*invalidHandler: function(form, validator) {
            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(formPemeriksaan).offset().top
            }, 1000);
        },*/
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: URL.savePemeriksaan,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                    setTimeout(() => {
                        enablePlanning = true;
                        $('a[href="#tab-planning"]').click();
                    }, 200);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    // TAB PLANNING
    $(formPlanning).on("change", "#kondisi_pasien", function() {
        $(formPlanning).find(".div-tindakan_lebih_lanjut").hide();
        if($(this).val() === "hidup") {
            $(formPlanning).find(".div-tindakan_lebih_lanjut").show('slow');
        }
    });
    
    // DIAGNOSA
    let initializeDiagnosa = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.code}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.description}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.code || data.text || '- Pilih -';
        }

        $(`#${el}`).select2({
            placeholder: "- Pilih -",
            ajax: {
                url: URL.getIcd10,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.uid, description: obj.description, code: obj.code };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        $(`#${el}`).on('select2:select', function (e) {
            var data = e.params.data;
            if(typeof data !== "undefined") {
                let label = "";
                if(el === "diagnosa_sekunder") {
                    aDiagnosaSekunder[data.id] = data;
                    label = generateLabelDiagnosaSekunder();
                } else label = data.description;
                $(`#div-label_${el}`).html(label);
            }
        });
    }
    
    $(`#diagnosa_sekunder`).on('select2:unselect', function (e) {
        var data = e.params.data;
        delete aDiagnosaSekunder[data.id];
        label = generateLabelDiagnosaSekunder();
        $(`#div-label_diagnosa_sekunder`).html(label);
    });

    initializeDiagnosa("diagnosa_utama");
    initializeDiagnosa("diagnosa_sekunder");

    let generateLabelDiagnosaSekunder = () => {
        let tmp = `<ul>`;
        for (let i in aDiagnosaSekunder) {
            tmp += `<li>${aDiagnosaSekunder[i].description}</li>`;
        }
        tmp += `</ul>`;
        return tmp;
    }

    // SIGNA
    function initializeSigna(el, signa_id) {
        $.getJSON(URL.getSigna, function(data, status) {
            if (status === "success") {
                let option = '';
                let arrSignaId = signa_id.split(",");
                for (var i = 0; i < data.data.length; i++) {
                    for(var j = 0; j < arrSignaId.length; j++) {
                        let selected = "";
                        if (arrSignaId[j] === data.data[i].id) selected = "selected='selected'";
                        option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].label}</option>`;
                    }
                }
                el.html(option).trigger("change");
            }
        });
    }
    initializeSigna($("#modal-input_racikan_signa"), "");

    // FARMASI UNIT
    function initializeFarmasiUnit(el, cara_bayar_id) {
        $.getJSON(URL.getFarmasiUnit.replace(':CARA_BAYAR_ID', cara_bayar_id), function(data, status) {
            if (status === "success") {
                let option = `<option value="0" selected="selected">- Pilih -</option>`;
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (data.data.length === 1) selected = "selected='selected'";
                    option += `<option value="${data.data[i].uid}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");

                el.prop('disabled', false);
                if (data.data.length === 1) el.prop('disabled', true);
            }
        });
    }

    // METODE RACIKAN
    function initializeMetodeRacikan(el, metode) {
        let option = '';
        for (var i = 0; i < metodeRacikan.length; i++) {
            let selected = "";
            if (metode === metodeRacikan[i].id) selected = "selected='selected'";
            option += `<option value="${metodeRacikan[i].id}" ${selected}>${metodeRacikan[i].nama}</option>`;
        }
        el.html(option).trigger("change");
    }

    // TINDAKAN
    let addTindakan = (obj, listMode) => {
        let tbody = $(tableTindakan + ' tbody');

        if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('tarif_pelayanan_uid', obj.tarif_pelayanan_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.tarif_pelayanan)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputTindakanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_tindakan_id[]')
            .val(obj.tindakan_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_id[]')
            .val(obj.tarif_pelayanan_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_uid[]')
            .val(obj.tarif_pelayanan_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan[]')
            .val(obj.tarif_pelayanan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.tarif).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_tarif[]')
            .val(obj.tarif)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.tarif * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        
        if(MODE !== "view" && parseInt(obj.close) !== 1) {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);

            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputQty.focus();
    }

    $(formPlanning).find("#btn-tambah_tindakan").on('click', () => {
        $(modalTindakanLayananId).val($('#layanan_id').val());
        $(modalTindakanCaraBayarId).val($('#cara_bayar_id').val());
        $(modalTindakanPerusahaanId).val(($('#perusahaan_id').val() === null ? "" : $('#perusahaan_id').val()));
        $(modalTindakanKelasId).val(imediscode.DEF_KELAS);
        $(modalTindakan).modal('show');
    });

    // MODAL Handler
    $(modalBtnTambahTindakan).on('click', () => {
        for (var dt_index in selectedTindakan) {
            let trData = selectedTindakan[dt_index];
                
            let isExists = false;
            $(tableTindakan + " tbody").find('tr').each((i, el) => {
                if ($(el).data('tarif_pelayanan_uid') == trData.uid) 
                    isExists = $(el);
            });

            if (isExists) {
                errorMessage('Error', 'Anda telah memilih tindakan ini sebelumnya. Silahkan pilih kembali !');

                let bg = isExists.find('td').css('background-color');
                let highlightBg = 'rgba(255, 0, 0, 0.2)';
                isExists.find('td').css('background-color', highlightBg);
                setTimeout(() => {
                    isExists.find('td').css('background-color', bg);
                }, 1500);
                return;
            }

            let data = {
                id: 0,
                tindakan_id: 0,
                tarif_pelayanan_id: trData.id,
                tarif_pelayanan_uid: trData.uid,
                kode: trData.kode,
                tarif_pelayanan: trData.nama,
                tarif: trData.tarif,
                quantity: 1,
                close: 0,
            };
            addTindakan(data, "add");
        }

        $(modalTindakan).modal('hide');
    });

    // BMHP
    let addBmhp = (obj, listMode) => {
        let tbody = $(tableBmhp + ' tbody');

        if(tbody.find('input[name="bmhp_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('bmhp_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputTindakanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_tindakan_id[]')
            .val(obj.tindakan_id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdPaket = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelPaket = $("<span/>")
            .html(obj.paket == 1 ? '<i class="fa fa-check"></i>' : '&mdash;')
            .appendTo(tdPaket);
        let divInputPaket = $("<div/>")
            .appendTo(tdPaket);
        let labelInputPaket = $("<label/>")
            .appendTo(divInputPaket);
        let inputPaket = $("<input/>")
            .prop('type', 'checkbox')
            .prop('checked', obj.paket == 1 ? true : false)
            .addClass('check')
            .appendTo(labelInputPaket);
        let inputHiddenPaket = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_paket[]')
            .val(obj.paket)
            .appendTo(tdPaket);
        inputPaket.uniform({radioClass: 'choice'});
        divInputPaket.hide();

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view" && parseInt(obj.close) !== 1) {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
                labelPaket.hide();
                divInputPaket.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelPaket.show();
                divInputPaket.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="bmhp_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelPaket.show();
                divInputPaket.hide();
                tr.children().removeClass('bg-danger-300');
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');


        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            /*if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 0;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }*/
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        inputPaket.on('change click', function () {
            labelPaket.html($(this).prop('checked') ? '<i class="fa fa-check"></i>' : '&mdash;');
            inputHiddenPaket.val($(this).prop('checked') ? 1 : 0);
        });

        dispInputQty.focus();
    }

    $(formPlanning).find("#btn-tambah_bmhp").on('click', () => {
        $(modalBmhpLayananId).val(btoa($(formPlanning).find('#layanan_id').val()));
        $(modalBmhp).modal('show');
    });

    // MODAL BMHP Handler
    $(modalBtnTambahBmhp).on('click', () => {
        for (var dt_index in selectedBmhp) {
            let trData = selectedBmhp[dt_index];
                
            let isExists = false;
            $(tableBmhp + " tbody").find('tr').each((i, el) => {
                if ($(el).data('bmhp_uid') == trData.barang_uid) 
                    isExists = $(el);
            });

            if (isExists) {
                errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');

                let bg = isExists.find('td').css('background-color');
                let highlightBg = 'rgba(255, 0, 0, 0.2)';
                isExists.find('td').css('background-color', highlightBg);
                setTimeout(() => {
                    isExists.find('td').css('background-color', bg);
                }, 1500);
                return;
            }

            let data = {
                id: 0,
                tindakan_id: 0,
                stock_id: trData.id,
                stock: trData.stock,
                obat_id: trData.barang_id,
                obat_uid: trData.barang_uid,
                obat: trData.barang,
                satuan_id: trData.satuan_id,
                satuan: trData.satuan,
                harga: trData.harga,
                harga_dasar: trData.harga_dasar,
                old_quantity: 0,
                quantity: 1,
                paket: 0,
                close: 0,
            };
            addBmhp(data, "add");
        }

        $(modalBmhp).modal('hide');
    });

    // NON RACIKAN
    let addNonRacikan = (obj, listMode) => {
        obj.browse = 'non_racikan';
        listResepObat.push(obj);
        let tbody = $(tableNonRacikan + ' tbody');

        if(tbody.find('input[name="non_racikan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('non_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_id[]')
            .addClass('input-non_racikan_obat_id')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan_id[]')
            .addClass('input-non_racikan_satuan_id')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty[]')
            .addClass('input-non_racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdSigna = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelSigna = $("<span/>")
            .html(obj.label_signa)
            .appendTo(tdSigna);
        let inputTextSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_signa[]')
            .prop('value', obj.signa)
            .appendTo(tdSigna);
        let inputSigna = $("<select/>")
            .addClass('form-control')
            .prop('multiple', 'multiple')
            .appendTo(tdSigna);
        inputSigna.select2({
            placeholder: "- Pilih -",
        });
        inputSigna.hide().next(".select2-container").hide();

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
                labelSigna.hide();
                inputSigna.show().next(".select2-container").show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelSigna.show();
                inputSigna.hide().next(".select2-container").hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) listResepObat.splice(idxLro, 1);

                if(tbody.find('input[name="non_racikan_obat_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelSignaArr = [];
                inputSigna.find('option:selected').each(function() {
                    if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                        labelSignaArr.push($(this).html());
                });

                labelSigna.html('&mdash;');
                let tmpLabel = '&mdash;'
                if(labelSignaArr.length > 0) {
                    tmpLabel = `<ul class="text-left">`;
                    for(let i = 0; i < labelSignaArr.length; i++) {
                        tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                    }
                    tmpLabel += `</ul>`;
                    labelSigna.html(tmpLabel);
                }

                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) {
                    listResepObat[idxLro].quantity = inputQty.val();
                    listResepObat[idxLro].label_signa = tmpLabel;
                }

                labelQty.show();
                dispInputQty.hide();
                labelSigna.show();
                inputSigna.hide().next(".select2-container").hide();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        if(listMode === "view_noedit") tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }
        initializeSigna(inputSigna, obj.signa);

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            /*if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }*/
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        inputSigna.on('change blur', (e) => {
            inputTextSigna.val(inputSigna.val());
        });

        dispInputQty.focus();

    }

    $(formPlanning).find("#btn-tambah_non_racikan").on('click', () => {
        $(modalObatMode).val('non_racikan');
        $(modalObat).modal('show');
    });

    // RACIKAN
    let addRacikan = (obj, listMode) => {
        obj.browse = 'racikan';
        listResepObat.push(obj);
        let tbody = $(tableRacikan + ' tbody');

        if(tbody.find('.input-racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('racikan_uid', obj.uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .appendTo(tr);
        let labelNama = $("<span>")
            .html(obj.nama + obj.label_list_obat)
            .addClass('label-racikan_nama')
            .appendTo(tdNama);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_id[]')
            .addClass('input-racikan_id')
            .val(obj.id ? obj.id : 0)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_nama')
            .prop('name', 'racikan_nama[]')
            .val(obj.nama)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_uid')
            .prop('name', 'racikan_uid[]')
            .val(obj.uid)
            .appendTo(tdNama);
        let inputListObat = $("<textarea/>")
            .addClass('input-racikan_list_obat')
            .prop('name', 'racikan_list_obat[]')
            .val(json_encode(obj.list_obat))
            .html(json_encode(obj.list_obat))
            .appendTo(tdNama);
        inputListObat.hide();

        let tdCaraBuat = $("<td/>")
            .appendTo(tr);
        let labelCaraBuat = $("<span>")
            .html(obj.label_cara_buat)
            .addClass('label-racikan_cara_buat')
            .appendTo(tdCaraBuat);
        let inputCaraBuat = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_cara_buat[]')
            .addClass('input-racikan_cara_buat')
            .val(obj.cara_buat)
            .appendTo(tdCaraBuat);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .addClass('label-racikan_harga')
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_harga[]')
            .addClass('input-racikan_harga')
            .val(obj.harga)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .addClass('label-racikan_qty')
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty[]')
            .addClass('input-racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdSigna = $("<td/>")
            .appendTo(tr);
        let labelSigna = $("<span>")
            .html(obj.label_signa)
            .addClass('label-racikan_signa')
            .appendTo(tdSigna);
        let inputSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_signa[]')
            .addClass('input-racikan_signa')
            .val(obj.signa)
            .appendTo(tdSigna);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                $('#modal-input_racikan_uid').val(obj.uid);
                $(formRacikan).find('#modal-input_racikan_nama').val(obj.nama);
                $(formRacikan).find('#modal-input_racikan_cara_buat').val(obj.cara_buat).change();
                $(formRacikan).find('#modal-input_racikan_harga').autoNumeric('set', obj.harga);
                $(formRacikan).find('#modal-input_racikan_qty').autoNumeric('set', obj.quantity);
                initializeSigna($(formRacikan).find('#modal-input_racikan_signa'), inputSigna.val());

                $(tableRacikanDetail + ' tbody').empty();
                let listObat = obj.list_obat;
                for(let i = 0; i < listObat.length; i++) 
                    addRacikanDetail(listObat[i], "view");

                $(modalRacikan).modal('show');
            });

            btnDel.on('click', (e) => {
                tr.remove();
                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) listResepObat.splice(idxLro, 1);

                if(tbody.find('.input-racikan_id').length <= 0) 
                    tbody.append(`<tr><td colspan="7" class="text-center">Tidak ada data</td></tr>`);

            });
        } else tdAction.html('&mdash;');

        if(listMode === "view_noedit") tdAction.html('&mdash;');
    }

    let genNamaRacikan = (table) => {
        let name = "";
        let length = table.find('.input-item_racikan_id').length;
        if(length > 0) name = `R${length}`;
        $(formRacikan).find('#modal-input_racikan_nama').val(name);
    }

    let addRacikanDetail = (obj, listMode) => {
        let tbody = $(tableRacikanDetail + ' tbody');

        if(tbody.find('.input-item_racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('item_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-item_racikan_id')
            .prop('name', 'item_racikan_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);
        let inputSatuanDosisId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_dosis_id[]')
            .val(obj.satuan_dosis_id ? obj.satuan_dosis_id : '')
            .appendTo(tdNama);
        let inputSatuanDosis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_dosis[]')
            .val(obj.satuan_dosis ? obj.satuan_dosis : '&mdash;')
            .appendTo(tdNama);
        let inputIsiSatuanDosis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_isi_satuan_dosis[]')
            .val(obj.isi_satuan_dosis != 0 ? obj.isi_satuan_dosis : 1)
            .appendTo(tdNama);

        let tdSatuan = $("<td/>")
            .html(obj.satuan)
            .appendTo(tr);

        let tdMetode = $("<td/>")
            .appendTo(tr);
        let labelMetode = $("<span/>")
            .html(obj.label_metode)
            .appendTo(tdMetode);
        let inputTextMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_label_metode[]')
            .val(obj.label_metode)
            .appendTo(tdNama);
        let inputMetode = $("<select/>")
            .prop('name', 'item_metode[]')
            .addClass('form-control')
            .appendTo(tdMetode);
        inputMetode.select2({
            placeholder: "- Pilih -",
        });
        inputMetode.hide().next(".select2-container").hide();

        let tdParamMetode = $("<td/>")
            .appendTo(tr);
        let labelParamMetode = $("<span/>")
            .html(obj.param_metode)
            .appendTo(tdParamMetode);
        let inputHiddenParamMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_param_metode[]')
            .val(obj.param_metode)
            .appendTo(tdParamMetode);
        let inputParamMetode = $("<input/>")
            .addClass('form-control')
            .val(obj.param_metode)
            .appendTo(tdParamMetode);
        inputParamMetode.hide();

        let tdSatuanDosis = $("<td/>")
            .appendTo(tr);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdHarga = $("<td/>")
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdTotal = $("<td/>")
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelMetode.hide();
                inputMetode.show().next(".select2-container").show();
                labelParamMetode.hide();
                inputParamMetode.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelMetode.show();
                inputMetode.hide().next(".select2-container").hide();
                labelParamMetode.show();
                inputParamMetode.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                updateTotal();
                genNamaRacikan(tbody);
                if(tbody.find('.input-item_racikan_id').length <= 0) 
                    tbody.append(`<tr><td colspan="9" class="text-center">Tidak ada data</td></tr>`);

            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelMetode.show();
                inputMetode.hide().next(".select2-container").hide();
                labelParamMetode.show();
                inputParamMetode.hide();

                updateTarifRow();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());

            updateTotal();
        }

        function updateTotal() {
            let total = 0;
            $(tableRacikanDetail + " tbody").find('tr').each((i, el) => {
                total += numeral($(el).find('td:eq(7)').html())._value;
            });
            $("#modal-input_racikan_harga").autoNumeric('set', total);
        }

        initializeMetodeRacikan(inputMetode, obj.metode);

        inputMetode.on('change', (e) => {
            let metode = inputMetode.find('option:selected').text();
            inputTextMetode.val(metode);
            labelMetode.html(metode);
            
            inputParamMetode.val(inputHiddenParamMetode.val()).inputmask({ alias : "numeric", prefix: '', placeholder: '0.00' });
            labelParamMetode.val('&mdash;');
            tdSatuanDosis.html('&mdash;');

            if(metode.search(/dosis/i) !== -1) {
                tdSatuanDosis.html(inputSatuanDosis.val());
                console.log('dosis');
            } else if(metode.search(/x\/\y/i) !== -1) {
                inputParamMetode.inputmask("#/#", {"placeholder": "0/0"});
                console.log('x / y');
            } else console.log('n qty obat');

            inputParamMetode.focus();
            updateTarifRow();
        });

        inputParamMetode.on('change keyup', (e) => {
            let qtyRacikan = isNaN(parseInt($("#modal-input_racikan_qty").autoNumeric('get'))) ? 0 : parseInt($("#modal-input_racikan_qty").autoNumeric('get'));
            let metode = inputMetode.find('option:selected').text();
            if(metode.search(/dosis/i) !== -1) {
                var dosis = isNaN(parseFloat(inputParamMetode.val())) ? 0 : parseFloat(inputParamMetode.val());
                var label_dosis = dosis;
                let isiSatuanDosis = parseFloat(inputIsiSatuanDosis.val());
                inputQty.val(Math.ceil((qtyRacikan * dosis) / isiSatuanDosis));
                labelQty.html(Math.ceil((qtyRacikan * dosis) / isiSatuanDosis));
                console.log('dosis');
            } else if(metode.search(/x\/\y/i) !== -1) {
                var dosis = inputParamMetode.val() != "" ? inputParamMetode.val() : "0/0";
                var label_dosis = dosis;
                dosis = dosis.split('/');
                let x = parseFloat(dosis[0]);
                let y = parseFloat(dosis[1]);
                inputQty.val(Math.ceil((x / y) * qtyRacikan));
                labelQty.html(Math.ceil((x / y) * qtyRacikan));
                console.log('x / y');
            } else {
                var dosis = isNaN(parseFloat(inputParamMetode.val())) ? 0 : parseFloat(inputParamMetode.val());
                var label_dosis = dosis;
                inputQty.val(dosis);
                labelQty.html(dosis);
                console.log('n qty obat');
            }
            inputHiddenParamMetode.val(label_dosis);
            labelParamMetode.html(label_dosis);
            updateTarifRow();
        });

        inputMetode.change();
        inputParamMetode.focus();
        genNamaRacikan(tbody);
    }

    $(formPlanning).find("#btn-tambah_racikan").on('click', () => {
        $(tableRacikanDetail + ' tbody').empty();
        $(modalRacikan).find('input, select').val('').trigger('change');
        $(modalRacikan).modal('show');
    });

    // MODAL OBAT Handler
    $(modalBtnTambahObat).on('click', () => {
        let mode = $(modalObatMode).val();
        for (var dt_index in selectedObat) {
            let trData = selectedObat[dt_index];
            
            let isExists = false;
            let param = {
                table: tableRacikanDetail,
                label: 'item_racikan_obat_uid'
            };
            if(mode === "non_racikan") {
                param = {
                    table: tableNonRacikan,
                    label: 'non_racikan_obat_uid'
                };
            }

            $(param.table + " tbody").find('tr').each((i, el) => {
                if ($(el).data(param.label) == trData.barang_uid) 
                    isExists = $(el);
            });

            if (isExists) {
                errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');
                $(modalObat).modal('hide');

                let bg = isExists.find('td').css('background-color');
                let highlightBg = 'rgba(255, 0, 0, 0.2)';
                isExists.find('td').css('background-color', highlightBg);
                setTimeout(() => {
                    isExists.find('td').css('background-color', bg);
                }, 1500);
                return;
            }

            let data = {
                id: 0,
                uid: makeid(50),
                stock_id: trData.id,
                stock: trData.stock_tmp,
                obat_id: trData.barang_id,
                obat_uid: trData.barang_uid,
                obat: trData.barang,
                satuan_id: trData.satuan_id,
                satuan: trData.satuan,
                harga: trData.harga,
                harga_dasar: trData.harga_dasar,
                old_quantity: 0,
                quantity: 1,
                signa: '',
                label_signa: '&mdash;',
            };

            if(mode === "non_racikan") {
                addNonRacikan(data, "add");
            } else {
                data.metode = '';
                data.label_metode = '&mdash;';
                data.param_metode = '';
                data.satuan_dosis_id = trData.satuan_dosis_id;
                data.satuan_dosis = trData.satuan_dosis;
                data.isi_satuan_dosis = trData.isi_satuan_dosis;
                addRacikanDetail(data, "add");
            }
        }

        $(modalObat).modal('hide');
    });

    // MODAL RACIKAN HANDLER
    $(formRacikan).find("#btn-tambah_racikan_detail").on('click', () => {
        let caraBuatRacikan = $("#modal-input_racikan_cara_buat").val();
        if(caraBuatRacikan === ""){
            warningMessage('Peringatan !', 'Cara Buat harus diisi terlebih dahulu.');
            $('#modal-input_racikan_cara_buat').focus();
            return;
        } 
        
        let qtyRacikan = isNaN(parseInt($("#modal-input_racikan_qty").autoNumeric('get'))) ? 0 : parseInt($("#modal-input_racikan_qty").autoNumeric('get'));
        if(qtyRacikan <= 0){
            warningMessage('Peringatan !', 'Qty. Racikan harus diisi terlebih dahulu.');
            $('#modal-input_racikan_qty').focus();
            return;
        } 

        $(modalObatMode).val('racikan');
        $(modalObat).modal('show');
    });

    $('#modal-input_racikan_cara_buat').change(function() {
        let qtyRacikan = isNaN(parseInt($("#modal-input_racikan_qty").autoNumeric('get'))) ? 0 : parseInt($("#modal-input_racikan_qty").autoNumeric('get'));
        let defaultQty = isNaN(parseFloat($(this).find('option:selected').data('qty_buat'))) ? 0 : parseFloat($(this).find('option:selected').data('qty_buat'));
        if(qtyRacikan <= 0) $('#modal-input_racikan_qty').autoNumeric('set', defaultQty);
    });

    $(formRacikan).validate({
        rules: {
            nama: { required: true },
            cara_buat: { required: true },
            harga: { required: true },
            quantity: { required: true },
            "signa[]": { required: true },
        },
        messages: {
            //
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalRacikan + ' .modal-dialog');
            let postData = $(formRacikan).serializeArray();

            let data = {};
            let d, fieldname, isArray, tmpLabel;
            for (let i = 0; i < postData.length; i++) {
                d = postData[i];
                isArray = false;

                if (d.name.search(/\[\]/) !== -1) {
                    fieldname = d.name.replace(/\[\]/, '');
                    isArray = true;
                } else {
                    fieldname = d.name;
                    isArray = false;
                }

                if (isArray) {
                    if (! data[fieldname]) {
                        data[fieldname] = [];
                        data[fieldname].push(d.value);
                    } else {
                        data[fieldname].push(d.value)
                    }
                } else {
                    data[fieldname] = d.value;
                }
            }

            data['label_cara_buat'] = $('#modal-input_racikan_cara_buat').find('option:selected').html();
            data['harga'] = $('#modal-input_racikan_harga').autoNumeric('get');
            data['quantity'] = $('#modal-input_racikan_qty').autoNumeric('get');

            let labelSignaArr = [];
            $('#modal-input_racikan_signa').find('option:selected').each(function() {
                if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                    labelSignaArr.push($(this).html());
            });

            data['label_signa'] = '&mdash;';
            if(labelSignaArr.length > 0) {
                tmpLabel = `<ul class="text-left">`;
                for(let i = 0; i < labelSignaArr.length; i++) {
                    tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_signa'] = tmpLabel;
            }

            let listObat = [];
            let labelListObat = [];
            for (let i = 0; i < data.item_racikan_id.length; i++) {
                listObat.push({
                    id: data.item_racikan_id[i],
                    stock_id: data.item_stock_id[i],
                    obat_id: data.item_obat_id[i],
                    obat_uid: data.item_obat_uid[i],
                    obat: data.item_obat[i],
                    satuan_id: data.item_satuan_id[i],
                    satuan: data.item_satuan[i],
                    label_metode: data.item_label_metode[i],
                    metode: data.item_metode[i],
                    param_metode: data.item_param_metode[i],
                    satuan_dosis_id: data.item_satuan_dosis_id[i],
                    satuan_dosis: data.item_satuan_dosis[i],
                    isi_satuan_dosis: data.item_isi_satuan_dosis[i],
                    harga: data.item_harga[i],
                    harga_dasar: data.item_harga_dasar[i],
                    old_quantity: data.item_old_qty[i],
                    quantity: data.item_qty[i],
                });

                let metode = data.item_label_metode[i];
                if(metode.search(/dosis/i) !== -1) {
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_param_metode[i]} ${data.item_satuan_dosis[i]}`);
                } else if(metode.search(/x\/\y/i) !== -1) {
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_param_metode[i]} ${data.item_satuan[i]}`);
                } else 
                    labelListObat.push(`${data.item_obat[i]} &nbsp;&nbsp; ${data.item_qty[i]} ${data.item_satuan[i]}`);
            }
            data['list_obat'] = listObat;
            if(data['uid'] === "") data['uid'] = makeid(50);

            if(labelListObat.length > 0) {
                tmpLabel = `<ul class="text-left text-size-mini text-slate-300">`;
                for(let i = 0; i < labelListObat.length; i++) {
                    tmpLabel += `<li>${labelListObat[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_list_obat'] = tmpLabel;
            }

            let isExists = false;
            $(tableRacikan + " tbody").find('tr').each(function() {
                if(data['uid'] === $(this).data('racikan_uid')) 
                    isExists = $(this);
            });

            if(isExists) {
                isExists.find('.input-racikan_list_obat').val(json_encode(data.list_obat)).html(json_encode(data.list_obat));
                isExists.find('.input-racikan_nama').val(data.nama);
                isExists.find('.label-racikan_nama').html(data.nama + data.label_list_obat);
                isExists.find('.input-racikan_cara_buat').val(data.cara_buat);
                isExists.find('.label-racikan_cara_buat').html(data.label_cara_buat);
                isExists.find('.label-racikan_harga').html('Rp.' + numeral(data.harga).format());
                isExists.find('.input-racikan_harga').val(data.harga);
                isExists.find('.label-racikan_qty').html(numeral(data.quantity).format());
                isExists.find('.input-racikan_qty').val(data.quantity);
                isExists.find('.label-racikan_signa').html(data.label_signa);
                isExists.find('.input-racikan_signa').val(data.signa);

                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(data.uid);
                if(idxLro !== -1) {
                    listResepObat[idxLro].list_obat = data.list_obat;
                    listResepObat[idxLro].label_list_obat = data.label_list_obat;
                    listResepObat[idxLro].nama = data.nama;
                    listResepObat[idxLro].label_cara_buat = data.label_cara_buat;
                    listResepObat[idxLro].cara_buat = data.cara_buat;
                    listResepObat[idxLro].harga = data.harga;
                    listResepObat[idxLro].quantity = data.qty;
                    listResepObat[idxLro].label_signa = data.label_signa;
                    listResepObat[idxLro].signa = data.signa;
                }
            } else addRacikan(data, "add");
            setTimeout(() => {
                $(modalRacikan + ' .modal-dialog').unblock();
            
                $(modalRacikan).modal('hide');
            }, 400);
        }
    });

    $('#btn-preview_resep').click(function() {
        if(listResepObat.length <= 0) {
            warningMessage('Peringatan', 'Racikan dan Non Racikan belum terisi');
            return;
        }

        dataRegistrasi.resep.list_obat = listResepObat;
        $(modalPreviewResep).find('.label-no_resep').html(dataRegistrasi.resep.nomor);
        $(modalPreviewResep).find('.label-dokter').html(dataRegistrasi.dokter);
        $(modalPreviewResep).find('.label-layanan').html(dataRegistrasi.layanan);
        $(modalPreviewResep).find('.label-regMr').html(`${dataRegistrasi.no_register} / ${dataRegistrasi.pasien.no_rm}`);
        $(modalPreviewResep).find('.label-tanggal').html(moment(dataRegistrasi.resep.tanggal).format('DD-MM-YYYY HH:mm'));
        $(modalPreviewResep).find('.label-pro').html(dataRegistrasi.pasien.nama);
        $(modalPreviewResep).find('.label-umur').html(`${dataRegistrasi.pasien.umur_tahun} Tahun ${dataRegistrasi.pasien.umur_bulan} Bulan ${dataRegistrasi.pasien.umur_hari} Hari`);

        let tmpLabel = `<ul class="text-left no-padding-left" style="list-style-type: none;">`;
        for(let i = 0; i < listResepObat.length; i++) {
            tmpLabel += `<li class="text-bold">R/</li>`;
            if(listResepObat[i].browse === "non_racikan") {
                tmpLabel += `<li class="pl-20">${listResepObat[i].obat} X ${listResepObat[i].quantity} ${listResepObat[i].satuan}</li>`;
            } else {
                tmpLabel += `<li class="pl-20">${listResepObat[i].nama} - ${listResepObat[i].label_cara_buat}</li>`;
                tmpLabel += `<li class="pl-20">${listResepObat[i].label_list_obat}</li>`;
            }

            if(listResepObat[i].label_signa !== "&mdash;")
                tmpLabel += `<li>${listResepObat[i].label_signa}</li>`;
        }
        tmpLabel += `</ul>`;
        $(modalPreviewResep).find('.label-list_obat').html(tmpLabel);

        $(modalPreviewResep).modal('show');
    });

    $(formPlanning).validate({
        rules: {
            diagnosa_utama: { required: true }
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            // Check tr in table non racikan and racikan which means have stock under qty request
            let underStock = false;
            $(tableBmhp).find('tbody tr').each(function() {
                if($(this).children().hasClass('bg-danger-300')) underStock = true;
            });

            if(underStock) {
                swal({
                    type: "warning",
                    title: "Ada barang yang dibawah qty request !",
                    text: "Cek stock barang yang memiliki kolom berwarna merah.",
                    html: true,
                    confirmButtonColor: "#2196F3"
                });
                return;
            }

            $.ajax({
                url: URL.savePlanning,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    successMessage('Success', "Assesment dan Planning berhasil disimpan.");

                    fillTabPlanning(UID);
                },
                error: function (err) {
                    $.unblockUI();
                    if(err.responseJSON) {
                        checkStock('bmhp', $(tableBmhp), '[name="bmhp_stock_id[]"]', '[name="bmhp_qty[]"]', '[name="bmhp_old_qty[]"]', tableBmhp, 'save');

                        err = err.responseJSON;
                        swal({
                            type: "warning",
                            title: err.title,
                            text: err.message,
                            html: true,
                            confirmButtonColor: "#2196F3"
                        });
                    } else {
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                }
            });
        }
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
            case '#tab-pemeriksaan':
                fillTabPemeriksaan(UID);
                break;
            default:
                if(!enablePlanning) {
                    e.preventDefault();
                    errorMessage('Error', "Silahkan isi terlebih dahulu tab pemeriksaan.");   
                    return false;
                }
                fillTabPlanning(UID);
                break;
        }
    });

    $('.btn-kembali').click(function() {
        window.location.assign(URL.index);
    });
    
    fillForm(UID);

    // RUJUKAN MODAL
    $(modalReservasiOk).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.ok_uid, "ok");
    });

    $(modalReservasiRanap).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.ranap_uid, "ranap");
    });

    $(modalReservasiLab).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.lab_uid, "lab");
    });

    $(modalReservasiRad).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.rad_uid, "rad");
    });

    $(modalKunjunganUlang).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.kunlang_uid, "kunlang");
    });

    $(modalRujukanLanjutan).on('hidden.bs.modal', function () {
        setDisplayRujukan(RUJUKAN.lanjutan_uid, "lanjutan");
    });
});