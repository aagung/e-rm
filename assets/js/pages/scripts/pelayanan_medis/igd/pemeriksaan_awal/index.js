$(() => {
    $('.rangetanggal-form').daterangepicker({
        applyClass: "bg-slate-600",
        cancelClass: "btn-default",
        opens: "center",
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        startDate: moment(),
        endDate: moment(),
    });

    let subsDate = (range, tipe) => {
        let date = range.substr(0, 10);
        if(tipe === "sampai") date = range.substr(13, 10);
        return getDate(date);
    }

    let listen = (browse, url) => {
        let eventId = 'igd-daftar_pasien';
        if(browse === 2) eventId = 'igd-daftar_history';

        evenSource = new EventSource(url.replace(':EVENT', eventId));
        evenSource.addEventListener(eventId, (e) => {
            let data = $.parseJSON(e.data);
            if (data.data) {
                if(browse === 2) {
                    TABLE_HISTORY_DT.draw(false);
                } else TABLE_DT.draw(false);
            }
        }, false);
    }

    let fetchDataSelect = (url, element) => {
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let datas = res.data;

                element.empty();
                element.append(`<option value="">${fieldAll}</option/>`);
                for (let data of datas) {
                    $("<option/>")
                        .prop('value', data.id)
                        .html(data.nama)
                        .appendTo(element);
                }
                element.val('').trigger('change');
            }
        });
    }

    let showDetail = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                $("#div-batal_alasan").hide();
                $(".div-detail_perusahaan").hide();
                $(".div-detail_no_jaminan").hide();
                $(".div-detail_penjamin_perusahaan").hide();
                $(".div-detail_penjamin_no_jaminan").hide();

                $("#detail-nama_pasien").html(data.pasien.nama);
                $("#detail-no_rm").html(data.pasien.no_rm);
                $("#detail-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#detail-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#detail-pj_nama").html(data.pj_nama);
                $("#detail-pj_no_telepon").html(data.pj_telepon);

                $("#detail-no_register").html(data.no_register);
                $("#detail-layanan").html(data.layanan);
                $("#detail-dokter").html(data.dokter);
                $("#detail-cara_bayar").html(data.cara_bayar);
                $("#detail-perusahaan").html(data.perusahaan);
                $("#detail-no_jaminan").html(data.no_jaminan);
                $("#detail-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $("#detail-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-detail_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-detail_penjamin_perusahaan").show();
                            $(".div-detail_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-detail_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-detail_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-detail_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-detail_perusahaan").show();
                        $(".div-detail_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-detail_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-detail_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-detail_no_jaminan").show().children('label').html('NIK');
                        break;
                }

                if(parseInt(data.batal) === 1) { 
                    $("#div-batal_alasan").show();

                    let batal_alasan = data.batal_alasan ? data.batal_alasan : "&mdash;";
                    $("#detail-batal_alasan").html(`${batal_alasan}<br/><span class="text-size-mini text-info">Dibatalkan oleh: ${data.batal_by}</span><br/><span class="text-size-mini text-danger">Pada: ${moment(data.batal_at).format('DD/MM/YYYY')}</span>`);
                }

                MODAL_DETAIL.modal('show');
            }
        });
    }

    let initializeTable = (browse) => {
        let columns = [];
        columns.push(
                    {
                        "data": "tanggal",
                        "render": (data, type, row, meta) => {
                            return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                        },
                    },
                    {
                        "data": "no_register",
                        "render": (data, type, row, meta) => {
                            if(parseInt(browse) === 2) {
                                return `<a data-uid="${row.pelayanan_uid}" data-popup="tooltip" class="detail-row" title="Lihat Data">${data}</a>`;
                            } else return `<a href="${URL.statusPemeriksaan.replace(':UID', row.pelayanan_uid)}" data-popup="tooltip" title="Lihat Status Pemeriksaan">${data}</a>`;
                        },
                    },
                    {
                        "data": "pasien",
                        "render": (data, type, row, meta) => {
                            let tmp = data;
                            tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                            return tmp;
                        },
                    },
                    { "data": "layanan" },
                    {
                        "data": "cara_bayar",
                        "render": (data, type, row, meta) => {
                            let tmp = data;
                            if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                            return tmp;
                        },
                    },
                    { "data": "dokter" },
                    {
                        "data": "uid",
                        "orderable": false,
                        "render": (data, type, row, meta) => {
                            let tmp = `<a data-uid="${row.pelayanan_uid}" data-id="${row.pelayanan_id}" data-popup="tooltip" title="Batalkan?" class="btn btn-warning btn-xs batal-row"><i class="fa fa-times"></i></a>`;
                            if(parseInt(browse) === 2) {
                                tmp = `<a href="${URL.statusPemeriksaan.replace(':UID', row.pelayanan_uid)}" data-popup="tooltip" title="Lihat Status Pemeriksaan" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>`;
                                if(parseInt(row.batal) === 1)
                                    tmp = `<label class="label label-warning" data-popup="tooltip" title="${row.batal_alasan}">Batal</label>`;
                            }
                            return tmp;
                        },
                        "className": "text-center",
                    },
                );

        // DATATABLE
        switch(browse) {
            case 1:
                TABLE_DT = TABLE.DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 1),
                      "type": "POST",
                      "data": function(p) {
                          /*p.tanggal_dari = subsDate($('#table_search_tanggal').val(), 'dari');
                          p.tanggal_sampai = subsDate($('#table_search_tanggal').val(), 'sampai');*/
                          p.no_register = $('#table_search_no_register').val();
                          p.no_rekam_medis = $('#table_search_no_rekam_medis').val();
                          p.nama = $('#table_search_nama').val();
                          p.cara_bayar_id = $('#table_search_cara_bayar_id').val();
                          p.dokter_id = $('#table_search_dokter_id').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [0, "asc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE.find('[data-popup=tooltip]').tooltip();
                    }
                });

                fetchDataSelect(URL.fetchDokter, $('#table_search_dokter_id'));
                fetchDataSelect(URL.fetchCaraBayar, $('#table_search_cara_bayar_id'));

                let isDTable = $.fn.dataTable.isDataTable(TABLE_HISTORY);
                if(isDTable === false) initializeTable(2);
                break;
            case 2:
                TABLE_HISTORY_DT = TABLE_HISTORY.DataTable({
                    "searching": false,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                      "url": URL.loadData.replace(':BROWSE', 2),
                      "type": "POST",
                      "data": function(p) {
                          p.tanggal_dari = subsDate($('#table-history_search_tanggal').val(), 'dari');
                          p.tanggal_sampai = subsDate($('#table-history_search_tanggal').val(), 'sampai');
                          p.no_register = $('#table-history_search_no_register').val();
                          p.no_rekam_medis = $('#table-history_search_no_rekam_medis').val();
                          p.nama = $('#table-history_search_nama').val();
                          p.cara_bayar_id = $('#table-history_search_cara_bayar_id').val();
                          p.dokter_id = $('#table-history_search_dokter_id').val();
                          p.status = $('#table-history_search_status').val();
                      }
                    },
                    "columns": columns,
                    "order": [ [0, "desc"] ],
                    "drawCallback": function (oSettings) {
                        TABLE_HISTORY.find('[data-popup=tooltip]').tooltip();
                    }
                });

                fetchDataSelect(URL.fetchDokter, $('#table-history_search_dokter_id'));
                fetchDataSelect(URL.fetchCaraBayar, $('#table-history_search_cara_bayar_id'));
                break;
        }
    }

    $('#btn-reset').on('click', () => {
        $('#table_search_no_register').val('');
        $('#table_search_no_rekam_medis').val('');
        $('#table_search_nama').val('');
        $('#table_search_cara_bayar_id').val('').trigger('change');
        $('#table_search_dokter_id').val('').trigger('change');
        /*$("#table_search_tanggal").data("daterangepicker").setStartDate(moment())
        $("#table_search_tanggal").data("daterangepicker").setEndDate(moment());*/
    });

    /*$('#table_search_tanggal').on('apply.daterangepicker', (ev, picker) => {
        TABLE_DT.draw();
    });*/

    $('.input-table_tab1').on('keyup', function () {
        TABLE_DT.draw();
    });

    $('.select-table_tab1').on('change', function () {
        TABLE_DT.draw();
    });

    /*$('#table_search_tanggal').change(function() {
        TABLE_DT.draw();
    });*/

    TABLE.on("click", ".detail-row", function () {
        let uid = $(this).data('uid');
        showDetail(uid);
    });

    TABLE.on("click", ".batal-row", function () {
        let id = $(this).data('id');
        let uid = $(this).data('uid');
        let tr = $(this).closest('tr');
        let data = TABLE_DT.row(tr).data();
        swal({
            title: "Yakin akan membatalkan kunjungan tersebut?",
            text: `<div class="row form-horizontal text-left">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. RM</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold">${data.no_rm}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Nama</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-nama_pasien">${data.pasien}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Jenis Kelamin</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.jenis_kelamin}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Alamat</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.alamat}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Tanggal Lahir</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-tanggal_lahir">${data.tanggal_lahir}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Umur</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-umur">${data.umur_tahun} Tahun</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. Telepon</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-no_telepon">${data.no_telepon}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    Alasan pembatalan<br/><textarea class='form-control' id='swal-text' style='resize: vertical;'></textarea>`,
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#F44336",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top",
        },
        function() {        
            let alasan = $("#swal-text").val();
            if (alasan === "") {
              swal.showInputError("Silahkan isi alasan pembatalan kunjungan.");
              $("#swal-text").focus();
              return;
            }

            $.post(URL.batal, { id: id, uid: uid, alasan: alasan}, function (data, status) {
                if (status === "success") {
                    swal.close();
                    TABLE_DT.draw();
                    successMessage('Berhasil !', 'Pembatalan kunjungan berhasil dilakukan.');
                }
            })
            .fail(function (error) {
                swal.close();
                errorMessage('Gagal !', 'Terjadi kesalahan saat pembatalan kunjungan.');
            });
        });
    });

    $('#btn-reset_batal').on('click', () => {
        $('#table-history_search_no_register').val('').trigger('change');
        $('#table-history_search_no_rekam_medis').val('').trigger('change');
        $('#table-history_search_nama').val('').trigger('change');
        $('#table-history_search_cara_bayar_id').val('').trigger('change');
        $('#table-history_search_dokter_id').val('').trigger('change');
        $("#table-history_search_tanggal").data("daterangepicker").setStartDate(moment())
        $("#table-history_search_tanggal").data("daterangepicker").setEndDate(moment());
    });

    $('#table-history_search_tanggal').on('apply.daterangepicker', (ev, picker) => {
        TABLE_HISTORY_DT.draw();
    });

    $('.input-table_tab2').on('keyup', function () {
        TABLE_HISTORY_DT.draw();
    });

    $('.select-table_tab2').on('change', function () {
        TABLE_HISTORY_DT.draw();
    });

    $('#table-history_search_tanggal').change(function() {
        TABLE_HISTORY_DT.draw();
    });

    TABLE_HISTORY.on("click", ".detail-row", function () {
        let uid = $(this).data('uid');
        showDetail(uid);
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-2':
            TABLE_HISTORY_DT.draw(false);
            break;
          default:
            TABLE_DT.draw(false);
            break;
        }
    });

    initializeTable(1);
    listen(1, URL.listenDaftarPasien);
    listen(2, URL.listenHistory);
});