/*
*    modalTindakan, modalTindakanLayananId, selectedTindakan, modalBtnTambahTindakan => didapat dari file modal 'pelayanan_medis/search-tarif-pelayanan-modal.php'
*    modalBmhp, modalBmhpLayananId, selectedBmhp, modalBtnTambahBmhp => didapat dari file modal 'pelayanan_medis/search-bmhp-modal.php'
*/

$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    $("#metode_numeric_rating_scale").ionRangeSlider({
        type: "single",
        min: 0,
        max: 10,
        step: 1,
        grid: true,
        grid_snap: true,
        onChange: function(data) {
            console.log(data);
        }
    });

    let initializeForm = () => {
        let fillTabPemeriksaan = (uid) => {
            blockElement(formPemeriksaan);
            $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'pemeriksaan'), function (res, status) {
                if (status === 'success') {
                    $(".div-label_perusahaan").hide();
                    $(".div-label_no_jaminan").hide();
                    $(".div-label_penjamin_perusahaan").hide();
                    $(".div-label_penjamin_no_jaminan").hide();
                    $(".div-detail_pernah_dirawat").hide();
                    $(".div-alergi_ke").hide();
                    $(".div-alergi_ke_lainnya").hide();
                    $(".div-status_ekonomi_lainnya").hide();

                    let data = res.data;
                    if(data.data_pemeriksaan) {
                        let pemeriksaan = data.data_pemeriksaan;

                        let label = Object.keys(pemeriksaan);
                        for (var i = 0; i < label.length; i++) {
                            switch(label[i]) {
                                case "metode_numeric_rating_scale":
                                    let inputInstance = $(`#${label[i]}`).data("ionRangeSlider");
                                    inputInstance.update({
                                        from: pemeriksaan[label[i]]
                                    });
                                    break;
                                default:
                                    label[i] = label[i].replace("'", "");
                                    if($(`input[name="${label[i]}"]`).is(':text') || $(`[name="${label[i]}"]`).is('textarea') || $(`[name="${label[i]}"]`).is('select')) {
                                        if(pemeriksaan[label[i]]) $(`[name="${label[i]}"]`).val(pemeriksaan[label[i]]).change();
                                    } else {   
                                        if(pemeriksaan[label[i]]) $(`input[name="${label[i]}"][value="${pemeriksaan[label[i]]}"]`).click();
                                    }
                                    break;
                            }
                        }
                        $.uniform.update();
                    }

                    $("input[name=id]").val(data.id);
                    $("input[name=uid]").val(data.uid);
                    $("input[name=pelayanan_id]").val(data.pelayanan_id);
                    $("input[name=pelayanan_uid]").val(data.pelayanan_uid);
                    $("input[name=no_register]").val(data.no_register);
                    $("input[name=pasien_id]").val(data.pasien_id);
                    $("input[name=dokter_id]").val(data.dokter_id);
                    $("input[name=layanan_id]").val(data.pelayanan.layanan_id);
                    $(formPlanning).find('#cara_bayar_id').val(data.pelayanan.cara_bayar_id);
                    $(formPlanning).find('#perusahaan_id').val(data.pelayanan.perusahaan_id);

                    $(".label-nama_pasien").html(data.pasien.nama);
                    $(".label-no_rm").html(data.pasien.no_rm);
                    $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                    $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                    $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                    $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                    $(".label-alamat").html(data.pasien.alamat);
                    $(".label-no_telepon").html(data.pasien.no_telepon_1);

                    $(".label-no_register").html(data.pelayanan.no_register);
                    $(".label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
                    $(".label-cara_bayar").html(data.pelayanan.cara_bayar);
                    $(".label-layanan").html(data.pelayanan.layanan);
                    $(".label-dokter").html(data.pelayanan.dokter);

                    $(".label-no_register").html(data.pelayanan.no_register);
                    $(".label-layanan").html(data.pelayanan.layanan);
                    $(".label-dokter").html(data.pelayanan.dokter);
                    $(".label-cara_bayar").html(data.pelayanan.cara_bayar);
                    $(".label-perusahaan").html(data.pelayanan.perusahaan);
                    $(".label-no_jaminan").html(data.pelayanan.no_jaminan);
                    $(".label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
                    $(".label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

                    switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
                        case imediscode.CARA_BAYAR_BPJS:
                            $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                            if(data.pelayanan.penjamin_id) {
                                $(".div-label_penjamin_perusahaan").show();
                                $(".div-label_penjamin_no_jaminan").show();

                                let labelPerusahaan = 'Perusahaan';
                                let labelNoJaminan = 'NIK';
                                if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                    labelPerusahaan = 'Asuransi';
                                    labelNoJaminan = 'No. Anggota';
                                }
                                $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                                $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                            }
                            break;
                        case imediscode.CARA_BAYAR_JAMKESDA:
                            $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                            break;
                        case imediscode.CARA_BAYAR_ASURANSI:
                        case imediscode.CARA_BAYAR_PERUSAHAAN:
                            $(".div-label_perusahaan").show();
                            $(".div-label_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                            break;
                        case imediscode.CARA_BAYAR_INTERNAL:
                            $(".div-label_no_jaminan").show().children('label').html('NIK');
                            break;
                    }
                    $(formPemeriksaan).unblock();
                }
            });

            if(MODE === "view") {
                $("input[type=text], textarea, select").prop("disabled", true);
                $(".btn-save").hide();
            }
        }

        let fillTabPlanning = (uid) => {
            blockElement(formPlanning);
            $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'planning'), function (res, status) {
                if (status === 'success') {
                    let data = res.data;
                    if(data) {
                        // TINDAKAN
                        $(tableTindakan + " tbody").empty();
                        for (var i = 0; i < data.tindakan_list.length; i++) {
                            addTindakan(data.tindakan_list[i]);
                        }

                        // BMHP
                        $(tableBmhp + " tbody").empty();
                        for (var i = 0; i < data.bmhp_list.length; i++) {
                            data.bmhp_list[i].old_quantity = data.bmhp_list[i].quantity;
                            addBmhp(data.bmhp_list[i]);
                        }

                        $('.btn-table_tindakan').parents('tfoot').show();
                        if(parseInt(data.pelayanan.close) !== 0) {
                            $('.btn-table_tindakan').parents('tfoot').hide();
                            $('.btn-b').parents('td').html('&mdash;');
                        }
                    }
                    $(formPlanning).unblock();
                    $('a[href="#tab-planning"]').focus();
                }
            });
        }
        // TAB PEMERIKSAAN
        $('input[name="pernah_dirawat"]').click(function() {
            $(".div-detail_pernah_dirawat").hide();
            $("#kapan").val('');
            $("#dimana").val('');
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-detail_pernah_dirawat").show('slow');
        });

        $('input[name="alergi"]').click(function() {
            $(".div-alergi_ke").hide();
            $(".div-alergi_ke_lainnya").hide();
            $('input:radio[name=alergi_ke]').prop('checked', false);
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-alergi_ke").show('slow');
        });

        $('input[name="alergi_ke"]').click(function() {
            $("#alergi_ke_lainnya").val('');
            $(".div-alergi_ke_lainnya").hide();

            let val = $(this).val();
            if(val === "lainnya") $(".div-alergi_ke_lainnya").show('slow');
        });

        $('input[name="status_ekonomi"]').click(function() {
            $("#status_ekonomi_lainnya").val('');
            $(".div-status_ekonomi_lainnya").hide();

            let val = $(this).val();
            if(val === "lainnya") $(".div-status_ekonomi_lainnya").show('slow');
        });

        $('.btn-kembali').click(function() {
            window.location.assign(URL.index);
        });

        $(formPemeriksaan).validate({
            rules: {
                alasan_masuk_rs: { required: true },
                td: { required: true },
                frek_nadi: { required: true },
                frek_nafas: { required: true },
                suhu: { required: true },
                tinggi_badan: { required: true },
                berat_badan: { required: true },
            },
            messages: {},
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.savePemeriksaan,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                        setTimeout(() => {
                            //window.location.assign(URL.index);
                            $('a[href="#tab-planning"]').click();
                        }, 200);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        // TAB PLANNING
        // TINDAKAN
        let addTindakan = (obj, listMode) => {
            let tbody = $(tableTindakan + ' tbody');

            if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
                tbody.empty();

            let tr = $("<tr/>")
                .data('tarif_pelayanan_uid', obj.tarif_pelayanan_uid)
                .appendTo(tbody);

            let tdNama = $("<td/>")
                .html(obj.tarif_pelayanan)
                .appendTo(tr);
            let inputDetailId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_detail_id[]')
                .val(obj.id)
                .appendTo(tdNama);
            let inputTindakanId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_tindakan_id[]')
                .val(obj.tindakan_id)
                .appendTo(tdNama);
            let inputId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_id[]')
                .val(obj.tarif_pelayanan_id)
                .appendTo(tdNama);
            let inputUid = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_uid[]')
                .val(obj.tarif_pelayanan_uid)
                .appendTo(tdNama);
            let inputNama = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan[]')
                .val(obj.tarif_pelayanan)
                .appendTo(tdNama);

            let tdHarga = $("<td/>")
                .addClass('text-right')
                .appendTo(tr);
            let labelHarga = $("<span>")
                .html('Rp.' + numeral(obj.tarif).format())
                .appendTo(tdHarga);
            let inputHarga = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_tarif[]')
                .val(obj.tarif)
                .appendTo(tdHarga);

            let tdQty = $("<td/>")
                .addClass('text-right')
                .appendTo(tr);
            let labelQty = $("<span>")
                .html(numeral(obj.quantity).format())
                .appendTo(tdQty);
            let inputQty = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_qty[]')
                .val(obj.quantity)
                .appendTo(tdQty);
            let dispInputQty = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control text-right')
                .appendTo(tdQty);
            dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputQty.autoNumeric('set', obj.quantity).hide();

            let tdTotal = $("<td/>")
                .addClass('text-right')
                .html('Rp.' + numeral(obj.tarif * obj.quantity).format())
                .appendTo(tr);

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            
            if(MODE !== "view" && parseInt(obj.close) !== 1) {
                let btnDone = $("<button/>")
                    .prop('type', 'button')
                    .addClass('btn btn-success btn-xs')
                    .html('<i class="fa fa-check"></i>')
                    .appendTo(tdAction);
                btnDone.hide();
                let btnDel = $("<button/>")
                    .prop('type', 'button')
                    .addClass('btn btn-danger btn-xs btn-b')
                    .html('<i class="fa fa-trash-o"></i>')
                    .appendTo(tdAction);

                // Handler
                tr.on('click', (e) => {
                    if (tr.data('done') == 1) {
                        tr.data('done', 0);
                        return;
                    }
                    tbody.find('tr').each((i, el) => {
                        if ($(el).data('uid') != tr.data('uid')) {
                            $(el).trigger('input_close');
                        }
                    });

                    btnDone.show();
                    btnDel.hide();

                    labelQty.hide();
                    dispInputQty.show();
                }).on('input_close', () => {
                    btnDone.hide();
                    btnDel.show();

                    labelQty.show();
                    dispInputQty.hide();
                });

                btnDel.on('click', (e) => {
                    tr.remove();
                    if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
                        tbody.append(`<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>`);
                });

                btnDone.on('click', (e) => {
                    tr.data('done', 1);
                    btnDone.hide();
                    btnDel.show();

                    labelQty.show();
                    dispInputQty.hide();
                });

                if(listMode === "add") tr.click();
            } else tdAction.html('&mdash;');

            function updateTarifRow() {
                let quantity = parseFloat(inputQty.val());
                let tarif = parseFloat(inputHarga.val());
                let total = tarif * quantity;
                tdTotal.html('Rp.' + numeral(total).format());
            }

            dispInputQty.on('keyup change blur', (e) => {
                let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
                inputQty.val(val);
                labelQty.html(val);

                updateTarifRow();
            });

            dispInputQty.focus();
        }

        $(formPlanning).find("#btn-tambah_tindakan").on('click', () => {
            $(modalTindakanLayananId).val($('#layanan_id').val());
            $(modalTindakanCaraBayarId).val($('#cara_bayar_id').val());
            $(modalTindakanPerusahaanId).val(($('#perusahaan_id').val() === null ? "" : $('#perusahaan_id').val()));
            $(modalTindakanKelasId).val(imediscode.DEF_KELAS);
            $(modalTindakan).modal('show');
        });

        // MODAL Handler
        $(modalBtnTambahTindakan).on('click', () => {
            for (var dt_index in selectedTindakan) {
                let trData = selectedTindakan[dt_index];
                    
                let isExists = false;
                $(tableTindakan + " tbody").find('tr').each((i, el) => {
                    if ($(el).data('tarif_pelayanan_uid') == trData.uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih tindakan ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    tindakan_id: 0,
                    tarif_pelayanan_id: trData.id,
                    tarif_pelayanan_uid: trData.uid,
                    kode: trData.kode,
                    tarif_pelayanan: trData.nama,
                    tarif: trData.tarif,
                    quantity: 1,
                    close: 0,
                };
                addTindakan(data, "add");
            }

            $(modalTindakan).modal('hide');
        });

        // BMHP
        let addBmhp = (obj, listMode) => {
            let tbody = $(tableBmhp + ' tbody');

            if(tbody.find('input[name="bmhp_detail_id[]"]').length <= 0) 
                tbody.empty();

            let tr = $("<tr/>")
                .data('bmhp_uid', obj.obat_uid)
                .appendTo(tbody);

            let tdNama = $("<td/>")
                .html(obj.obat)
                .appendTo(tr);
            let inputDetailId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_detail_id[]')
                .val(obj.id)
                .appendTo(tdNama);
            let inputTindakanId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_tindakan_id[]')
                .val(obj.tindakan_id)
                .appendTo(tdNama);
            let inputStockId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_stock_id[]')
                .val(obj.stock_id)
                .appendTo(tdNama);
            let inputId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_id[]')
                .val(obj.obat_id)
                .appendTo(tdNama);
            let inputUid = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_uid[]')
                .val(obj.obat_uid)
                .appendTo(tdNama);
            let inputNama = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp[]')
                .val(obj.obat)
                .appendTo(tdNama);
            let inputSatuanId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_satuan_id[]')
                .val(obj.satuan_id)
                .appendTo(tdNama);
            let inputSatuan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_satuan[]')
                .val(obj.satuan)
                .appendTo(tdNama);

            let tdHarga = $("<td/>")
                .addClass('text-right')
                .appendTo(tr);
            let labelHarga = $("<span>")
                .html('Rp.' + numeral(obj.harga).format())
                .appendTo(tdHarga);
            let inputHarga = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_harga[]')
                .val(obj.harga)
                .appendTo(tdHarga);
            let inputHargaDasar = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_harga_dasar[]')
                .val(obj.harga_dasar)
                .appendTo(tdHarga);

            let tdQty = $("<td/>")
                .addClass('text-right')
                .appendTo(tr);
            let labelQty = $("<span>")
                .html(numeral(obj.quantity).format())
                .appendTo(tdQty);
            let inputOldQty = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_old_qty[]')
                .val(obj.old_quantity)
                .appendTo(tdQty);
            let inputQty = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_qty[]')
                .val(obj.quantity)
                .appendTo(tdQty);
            let dispInputQty = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control text-right')
                .appendTo(tdQty);
            dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputQty.autoNumeric('set', obj.quantity).hide();

            let tdTotal = $("<td/>")
                .addClass('text-right')
                .html('Rp.' + numeral(obj.harga * obj.quantity).format())
                .appendTo(tr);

            let tdPaket = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let labelPaket = $("<span/>")
                .html(obj.paket == 1 ? '<i class="fa fa-check"></i>' : '&mdash;')
                .appendTo(tdPaket);
            let divInputPaket = $("<div/>")
                .appendTo(tdPaket);
            let labelInputPaket = $("<label/>")
                .appendTo(divInputPaket);
            let inputPaket = $("<input/>")
                .prop('type', 'checkbox')
                .prop('checked', obj.paket == 1 ? true : false)
                .addClass('check')
                .appendTo(labelInputPaket);
            let inputHiddenPaket = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'bmhp_paket[]')
                .val(obj.paket)
                .appendTo(tdPaket);
            inputPaket.uniform({radioClass: 'choice'});
            divInputPaket.hide();

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);

            if(MODE !== "view" && parseInt(obj.close) !== 1) {
                let btnDone = $("<button/>")
                    .prop('type', 'button')
                    .addClass('btn btn-success btn-xs')
                    .html('<i class="fa fa-check"></i>')
                    .appendTo(tdAction);
                btnDone.hide();
                let btnDel = $("<button/>")
                    .prop('type', 'button')
                    .addClass('btn btn-danger btn-xs btn-b')
                    .html('<i class="fa fa-trash-o"></i>')
                    .appendTo(tdAction);
                
                // Handler
                tr.on('click', (e) => {
                    if (tr.data('done') == 1) {
                        tr.data('done', 0);
                        return;
                    }
                    tbody.find('tr').each((i, el) => {
                        if ($(el).data('uid') != tr.data('uid')) {
                            $(el).trigger('input_close');
                        }
                    });

                    btnDone.show();
                    btnDel.hide();

                    labelQty.hide();
                    dispInputQty.show();
                    labelPaket.hide();
                    divInputPaket.show();
                }).on('input_close', () => {
                    btnDone.hide();
                    btnDel.show();

                    labelQty.show();
                    dispInputQty.hide();
                    labelPaket.show();
                    divInputPaket.hide();
                });

                btnDel.on('click', (e) => {
                    tr.remove();
                    if(tbody.find('input[name="bmhp_id[]"]').length <= 0) 
                        tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
                });

                btnDone.on('click', (e) => {
                    tr.data('done', 1);
                    btnDone.hide();
                    btnDel.show();

                    labelQty.show();
                    dispInputQty.hide();
                    labelPaket.show();
                    divInputPaket.hide();
                    tr.children().removeClass('bg-danger-300');
                });

                if(listMode === "add") tr.click();
            } else tdAction.html('&mdash;');


            function updateTarifRow() {
                let quantity = parseFloat(inputQty.val());
                let tarif = parseFloat(inputHarga.val());
                let total = tarif * quantity;
                tdTotal.html('Rp.' + numeral(total).format());
            }

            dispInputQty.on('keyup change blur', (e) => {
                let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
                /*if(parseFloat(val) > parseFloat(obj.stock)) {
                    val = 0;
                    warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
                }*/
                dispInputQty.autoNumeric('set', val);
                inputQty.val(val);
                labelQty.html(val);

                updateTarifRow();
            });

            inputPaket.on('change click', function () {
                labelPaket.html($(this).prop('checked') ? '<i class="fa fa-check"></i>' : '&mdash;');
                inputHiddenPaket.val($(this).prop('checked') ? 1 : 0);
            });

            dispInputQty.focus();
        }

        $(formPlanning).find("#btn-tambah_bmhp").on('click', () => {
            $(modalBmhpLayananId).val(btoa($(formPlanning).find('#layanan_id').val()));
            $(modalBmhp).modal('show');
        });

        // MODAL BMHP Handler
        $(modalBtnTambahBmhp).on('click', () => {
            for (var dt_index in selectedBmhp) {
                let trData = selectedBmhp[dt_index];
                    
                let isExists = false;
                $(tableBmhp + " tbody").find('tr').each((i, el) => {
                    if ($(el).data('bmhp_uid') == trData.barang_uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    tindakan_id: 0,
                    stock_id: trData.id,
                    stock: trData.stock,
                    obat_id: trData.barang_id,
                    obat_uid: trData.barang_uid,
                    obat: trData.barang,
                    satuan_id: trData.satuan_id,
                    satuan: trData.satuan,
                    harga: trData.harga,
                    harga_dasar: trData.harga_dasar,
                    old_quantity: 0,
                    quantity: 1,
                    paket: 0,
                    close: 0,
                };
                addBmhp(data, "add");
            }

            $(modalBmhp).modal('hide');
        });

        $(formPlanning).validate({
            rules: {},
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                // Check tr in table non racikan and racikan which means have stock under qty request
                let underStock = false;
                $(tableBmhp).find('tbody tr').each(function() {
                    if($(this).children().hasClass('bg-danger-300')) underStock = true;
                });

                if(underStock) {
                    swal({
                        type: "warning",
                        title: "Ada barang yang dibawah qty request !",
                        text: "Cek stock barang yang memiliki kolom berwarna merah.",
                        html: true,
                        confirmButtonColor: "#2196F3"
                    });
                    return;
                }

                $.ajax({
                    url: URL.savePlanning,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Tindakan dan BMHP berhasil disimpan.");
                        fillTabPlanning(UID);
                    },
                    error: function (err) {
                        $.unblockUI();
                        if(err.responseJSON) {
                            checkStock('bmhp', $(tableBmhp), '[name="bmhp_stock_id[]"]', '[name="bmhp_qty[]"]', '[name="bmhp_old_qty[]"]', tableBmhp, 'save');

                            err = err.responseJSON;
                            swal({
                                type: "warning",
                                title: err.title,
                                text: err.message,
                                html: true,
                                confirmButtonColor: "#2196F3"
                            });
                        } else {
                            errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                        }
                    }
                });
            }
        });

        $('a[data-toggle="tab"]').click(function (e) {
            switch($(this).attr('href')) {
                case '#tab-pemeriksaan':
                    fillTabPemeriksaan(UID);
                    break;
                default:
                    fillTabPlanning(UID);
                    break;
            }
        });

        fillTabPemeriksaan(UID);
    }

    initializeForm();
});