$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getDataRegistrasi.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();
    
                let data = res.data;

                $("input[name=pelayanan_id]").val(data.id);
                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }


                $.unblockUI();

                fillDetail(uid);
            }
        });

        $.uniform.update();
    }

    let getPemeriksaanDokter = (pelayanan_uid) => {
        $.getJSON(URL.getDataPemeriksaan.replace(':UID', pelayanan_uid), function (res, status) {
            if(status === 'success'){
                data = res.data;

                let textSubjective = "";
                let textObjective = "";
                let textAssesment = "";
                let textPlanning = "";

                if(data.data_pemeriksaan) {
                    let pemeriksaan = data.data_pemeriksaan;
                    let label = Object.keys(pemeriksaan);
                    for (var i = 0; i < label.length; i++) {
                        let arrLabel = label[i].split('_');
                        let val = pemeriksaan[label[i]] ? pemeriksaan[label[i]] : "";
                        if(val) {
                            if(arrLabel[0] === "subjective") {
                                // Subjective
                                arrLabel.shift();
                                let key = arrLabel.join(" ");
                                textSubjective += `<b>${ucwords(key)}</b>: ${val}<br/>`;
                            } else {
                                // Objective
                                switch(label[i]) {
                                    case "pediatrik_content":
                                    case "adult_content":
                                    case "pediatrik_lokalis":
                                    case "adult_lokalis":
                                        // NONE
                                        break;
                                    default:
                                        let key = arrLabel.join(" ");
                                        textObjective += `<b>${ucwords(key)}</b>: ${val}<br/>`;
                                        break;
                                }
                            }
                        }
                    }
                }

                // Assesment
                let diagnosa_utama = data.diagnosa_utama ? data.diagnosa_utama.description : "&mdash;";
                let diagnosa_sekunder = "&mdash;";
                let aDiagnosaSekunder = [];
                for (var i = 0; i < data.diagnosa_sekunder.length; i++) {
                    aDiagnosaSekunder.push(data.diagnosa_sekunder[i].description);    
                }
                if(aDiagnosaSekunder.length > 0)
                    diagnosa_sekunder = aDiagnosaSekunder.join(", ");
                
                textAssesment = `<b>Diagnosa Utama</b>: ${diagnosa_utama}<br/>`;
                textAssesment += `<b>Diagnosa Sekunder</b>: ${diagnosa_sekunder}<br/>`;

                /*
                * Planning
                */
                // Tindakan
                let textTindakan = "";
                let aTindakan = [];
                for (var i = 0; i < data.tindakan_list.length; i++) {
                    if(data.tindakan_list[i].tarif_pelayanan)
                        aTindakan.push(data.tindakan_list[i].tarif_pelayanan);
                }
                if(aTindakan.length > 0)
                    textTindakan = aTindakan.join(", ");

                // BMHP
                let textBmhp = "";
                let aBmhp = [];
                for (var i = 0; i < data.bmhp_list.length; i++) {
                    if(data.bmhp_list[i].obat)
                        aBmhp.push(`${data.bmhp_list[i].obat} <b>(${numeral(data.bmhp_list[i].quantity).format()} ${data.bmhp_list[i].satuan})</b>`);
                }
                if(aBmhp.length > 0)
                    textBmhp = aBmhp.join("<br/>");

                // Non Racikan
                let textNonRacikan = "";
                let aNonRacikan = [];
                for (var i = 0; i < data.non_racikan_list.length; i++) {
                    if(data.non_racikan_list[i].obat)
                        aNonRacikan.push(`${data.non_racikan_list[i].obat} <b>(${numeral(data.non_racikan_list[i].quantity).format()} ${data.non_racikan_list[i].satuan})</b>`);
                }
                if(aNonRacikan.length > 0)
                    textNonRacikan = aNonRacikan.join("<br/>");

                // Racikan
                let textRacikan = "";
                let aRacikan = [];
                for (var i = 0; i < data.racikan_list.length; i++) {
                    if(data.racikan_list[i].nama)
                        aRacikan.push(`${data.racikan_list[i].nama} <b>(${numeral(data.racikan_list[i].quantity).format()})</b> ${data.racikan_list[i].label_list_obat}`);
                }
                if(aRacikan.length > 0)
                    textRacikan = aRacikan.join("<br/>");

                /*
                * Rujukan
                */
                // OK
                let textRujukanOk = "";
                if(data.rujukan_ok) {
                    let tanggal = moment(data.rujukan_ok.tanggal).format('DD/MM/YYYY');
                    let operasi = data.rujukan_ok.operasi ? data.rujukan_ok.operasi : "&mdash;";
                    let klasifikasi = data.rujukan_ok.klasifikasi;

                    textRujukanOk += "<b>Rujukan OK</b>:<br/>";
                    textRujukanOk += "<ul class='text-left no-padding-bottom'>";
                    textRujukanOk +=     `<li><b>Tanggal:</b> ${tanggal}</li>`;
                    textRujukanOk +=     `<li><b>Operasi:</b> ${operasi}</li>`;
                    textRujukanOk +=     `<li><b>Klasifikasi:</b> ${ucwords(klasifikasi)}</li>`;
                    textRujukanOk += "</ul>";
                }

                // Ranap
                let textRujukanRanap = "";
                if(data.rujukan_ranap) {
                    let tanggal = moment(data.rujukan_ranap.tanggal).format('DD/MM/YYYY');

                    textRujukanRanap += "<b>Rujukan Rawat Inap</b>:<br/>";
                    textRujukanRanap += "<ul class='text-left no-padding-bottom'>";
                    textRujukanRanap +=     `<li><b>Tanggal:</b> ${tanggal}</li>`;
                    textRujukanRanap += "</ul>";
                }

                // Kunjungan Ulang
                let textRujukanKunlang = "";
                if(data.rujukan_kunjungan_ulang) {
                    let tanggal = moment(data.rujukan_kunjungan_ulang.tanggal).format('DD/MM/YYYY');

                    textRujukanKunlang += "<b>Kunjungan Ulang</b>:<br/>";
                    textRujukanKunlang += "<ul class='text-left no-padding-bottom'>";
                    textRujukanKunlang +=     `<li><b>Tanggal:</b> ${tanggal}</li>`;
                    textRujukanKunlang += "</ul>";
                }

                // Lab
                let textRujukanLab = "";
                if(data.rujukan_lab) {
                    textRujukanLab += "<b>Laboratorium</b>:<br/>";
                    textRujukanLab += "<ul class='text-left no-padding-bottom'>";
                    for (var i = 0; i < data.rujukan_lab.details.length; i++) {
                        if(data.rujukan_lab.details[i].pemeriksaan)
                            textRujukanLab += `<li>${data.rujukan_lab.details[i].pemeriksaan}</li>`;
                    }
                    textRujukanLab += "</ul>";
                }

                // Rad
                let textRujukanRad = "";
                if(data.rujukan_rad) {
                    textRujukanRad += "<b>Radiologi</b>:<br/>";
                    textRujukanRad += "<ul class='text-left no-padding-bottom'>";
                    for (var i = 0; i < data.rujukan_rad.details.length; i++) {
                        if(data.rujukan_rad.details[i].pemeriksaan)
                            textRujukanRad += `<li>${data.rujukan_rad.details[i].pemeriksaan}</li>`;
                    }
                    textRujukanRad += "</ul>";
                }

                // Lanjutan
                let textLanjutan = "";
                if(data.rujukan_lanjutan) {
                    textLanjutan += "<b>Rujukan Lanjutan</b>:<br/>";
                    textLanjutan += "<ul class='text-left no-padding-bottom'>";
                    if(data.rujukan_lanjutan.jenis === "spesialis_lain") {
                        let tanggal = moment(data.rujukan_lanjutan.tanggal).format('DD/MM/YYYY');
                        let layanan = data.rujukan_lanjutan.layanan;
                        let dokter = data.rujukan_lanjutan.dokter;

                        textLanjutan += `<li><b>Tanggal:</b> ${tanggal}</li>`;
                        textLanjutan += `<li><b>Layanan:</b> ${layanan}</li>`;
                        textLanjutan += `<li><b>Dokter:</b> ${dokter}</li>`;
                    } else {
                        let diagnosa = data.rujukan_lanjutan.diagnosa ? data.rujukan_lanjutan.diagnosa : "&mdash;";
                        let terapi = data.rujukan_lanjutan.terapi ? data.rujukan_lanjutan.terapi : "&mdash;";
                        let pemeriksaan_penunjang = data.rujukan_lanjutan.pemeriksaan_penunjang ? data.rujukan_lanjutan.pemeriksaan_penunjang : "&mdash;";
                        let tujuan_merujuk = data.rujukan_lanjutan.tujuan_merujuk ? data.rujukan_lanjutan.tujuan_merujuk : "&mdash;";
                        let alasan_merujuk = data.rujukan_lanjutan.alasan_merujuk ? data.rujukan_lanjutan.alasan_merujuk : "&mdash;";

                        textLanjutan += `<li><b>Diagnosa:</b><br/> ${diagnosa}</li>`;
                        textLanjutan += `<li><b>Terapi:</b><br/> ${terapi}</li>`;
                        textLanjutan += `<li><b>Pemeriksaan Penunjang:</b><br/> ${pemeriksaan_penunjang}</li>`;
                        textLanjutan += `<li><b>Tujuan Merujuk:</b><br/> ${tujuan_merujuk}</li>`;
                        textLanjutan += `<li><b>Alasan Merujuk:</b><br/> ${alasan_merujuk}</li>`;
                    }
                    textLanjutan += "</ul>";
                }

                let aPlanning = [];
                if(textTindakan)
                    aPlanning.push(`<b>Tindakan</b>:<br/> ${textTindakan}<br/>`);
                if(textBmhp)
                    aPlanning.push(`<b>BMHP</b>:<br/> ${textBmhp}<br/>`);
                if(textNonRacikan)
                    aPlanning.push(`<b>Resep - Non Racikan</b>:<br/> ${textNonRacikan}<br/>`);
                if(textRacikan)
                    aPlanning.push(`<b>Resep - Racikan</b>:<br/> ${textRacikan}`);
                if(textRujukanOk)
                    aPlanning.push(textRujukanOk);
                if(textRujukanRanap)
                    aPlanning.push(textRujukanRanap);
                if(textRujukanLab)
                    aPlanning.push(textRujukanLab);
                if(textRujukanRad)
                    aPlanning.push(textRujukanRad);
                if(textRujukanKunlang)
                    aPlanning.push(textRujukanKunlang);
                if(textLanjutan)
                    aPlanning.push(textLanjutan);
                if(aPlanning.length > 0)
                    textPlanning = aPlanning.join(" ");


                addDetail({
                    id: 0,
                    tanggal: moment(data.pemeriksaan_at).format('YYYY-MM-DD'),
                    jam: moment(data.pemeriksaan_at).format('HH:mm'),
                    dokter_id: data.dokter_id,
                    ppa: data.disp_pemeriksaan_by,
                    ppa_id: data.pemeriksaan_by,
                    s: textSubjective ? textSubjective : "&mdash;",
                    o: textObjective ? textObjective : "&mdash;",
                    a: textAssesment ? textAssesment : "&mdash;",
                    p: textPlanning ? textPlanning : "&mdash;",
                    data_uid: data.uid,
                    verifikasi_dpjp: 0,
                });
            }
        });       
    }

    let fillDetail = (pelayanan_uid) => {
        blockElement(tableDetail);
        $.getJSON(URL.getDataCppt.replace(':PELAYANAN_UID', pelayanan_uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                if(data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        addDetail(data[i]);
                    }

                    if(MODE === "view") {
                        $(".btn-save").hide();
                        $(btnTambahDetail).parents('tr').hide();
                        $("input[type=text], textarea, select").prop("disabled", true);
                    }
                } else {
                    getPemeriksaanDokter(pelayanan_uid);
                }
                $(tableDetail).unblock();
            }
        });
    }

    let addDetail = (obj) => {
        let disp_tanggal = moment(obj.tanggal).isValid() ? moment(obj.tanggal).format('DD/MM/YYYY') : moment().format('DD/MM/YYYY');
        let jam = moment(obj.tanggal + ' ' + obj.jam).isValid() ? moment(obj.tanggal + ' ' + obj.jam).format('HH:mm') : moment().format('HH:mm');;

        let tbody = $(tableDetail + ' tbody');

        if(tbody.find('input[name="detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('detail_id', obj.id)
            .appendTo(tbody);

        let tdTanggal = $("<td/>")
            .appendTo(tr);
            let inputDetailId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_id[]')
                .val(obj.id)
                .appendTo(tdTanggal);
            let inputDataId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_data_uid[]')
                .val(obj.data_uid)
                .appendTo(tdTanggal);
            let inputDokterId = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_dokter_id[]')
                .val(obj.dokter_id)
                .appendTo(tdTanggal);
            let labelTanggal = $("<div/>")
                .html(disp_tanggal)
                .appendTo(tdTanggal)
            let inputTanggal = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_tanggal[]')
                .val(disp_tanggal)
                .appendTo(tdTanggal);

        let tdJam = $("<td/>")
            .appendTo(tr);
            let labelJam = $("<div/>")
                .html(jam)
                .appendTo(tdJam);
            let inputJam = $("<input/>")
                .addClass('form-control')
                .prop('type', 'hidden')
                .prop('name', 'detail_jam[]')
                .val(jam)
                .appendTo(tdJam);

        let tdPPA = $("<td/>")
            .appendTo(tr);
            let labelPPA = $("<div/>")
                    .html(obj.ppa)
                    .appendTo(tdPPA);
            let inputPPA = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_ppa_id[]')
                .val(obj.ppa_id)
                .appendTo(tdPPA);

        let tdHasilPemeriksaan = $("<td/>")
            .appendTo(tr);
            let divS = $("<div/>")
                .addClass('row col-lg-12 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelS = $("<div/>")
                    .addClass('col-xs-1')
                    .html('S')
                    .appendTo(divS);
                let divInputS = $("<div/>")
                    .addClass('col-xs-11')
                    .html(obj.s)
                    .appendTo(divS);
                    let inputS = $("<textarea/>")
                        .prop('style', 'display: none')
                        .prop('type', 'hidden')
                        .prop('name', 'detail_s[]')
                        .val(obj.s)
                        .html(obj.s)
                        .appendTo(divInputS);
            let divO = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelO = $("<div/>")
                    .addClass('col-xs-1')
                    .html('O')
                    .appendTo(divO);
                let divInputO = $("<div/>")
                    .addClass('col-xs-11')
                    .html(obj.o)
                    .appendTo(divO);
                    let inputO = $("<textarea/>")
                        .prop('style', 'display: none')
                        .prop('type', 'hidden')
                        .prop('name', 'detail_o[]')
                        .val(obj.o)
                        .html(obj.o)
                        .appendTo(divInputO);

            let divA = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelA = $("<div/>")
                    .addClass('col-xs-1')
                    .html('A')
                    .appendTo(divA);
                let divInputA = $("<div/>")
                    .addClass('col-xs-11')
                    .html(obj.a)
                    .appendTo(divA);
                    let inputA = $("<textarea/>")
                        .prop('style', 'display: none')
                        .prop('type', 'hidden')
                        .prop('name', 'detail_a[]')
                        .val(obj.a)
                        .html(obj.a)
                        .appendTo(divInputA);

            let divP = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelP = $("<div/>")
                    .addClass('col-xs-1')
                    .html('P')
                    .appendTo(divP);
                let divInputP = $("<div/>")
                    .addClass('col-xs-11')
                    .html(obj.p)
                    .appendTo(divP);
                    let inputP = $("<textarea/>")
                        .prop('style', 'display: none')
                        .prop('type', 'hidden')
                        .prop('name', 'detail_p[]')
                        .val(obj.p)
                        .html(obj.p)
                        .appendTo(divInputP);

        let tdVerifikasi = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
            let divInputVerifikasi = $("<div/>")
                .appendTo(tdVerifikasi);
                let labelInputVerifikasi = $("<label/>")
                    .appendTo(divInputVerifikasi);
                    let inputVerifikasi = $("<input/>")
                        .prop('type', 'checkbox')
                        .prop('checked', obj.verifikasi_dpjp == 1 ? true : false)
                        .addClass('check')
                        .appendTo(labelInputVerifikasi);
            let inputHiddenVerifikasi = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_verifikasi_dpjp[]')
                .val(obj.verifikasi_dpjp)
                .appendTo(tdVerifikasi);
        inputVerifikasi.uniform({radioClass: 'choice'});

        /*let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="detail_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });
        } else tdAction.html('&mdash;');*/

        inputVerifikasi.on('change click', function () {
            inputHiddenVerifikasi.val($(this).prop('checked') ? 1 : 0);
        });

        inputTanggal.focus();
    }

    /*$(btnTambahDetail).click(function (e) {
        addDetail({
            id: 0,
            tanggal: moment(),
            ppa: "",
            ppa_id: "",
            s: "&mdash;",
            o: "&mdash;",
            a: "&mdash;",
            p: "&mdash;",
            verifikasi_dpjp: 0,
        })
    });*/

    $(form).validate({
        rules: {
            'detail_tanggal[]': { required: true },
            'detail_jam[]': { required: true },
            'detail_ppa_id[]': { required: true },
        },
        messages: {
            'detail_tanggal[]': { required: 'Tanggal Diperlukan' },
            'detail_jam[]': { required: 'Jam Diperlukan' },
            'detail_ppa_id[]': { required: 'PPA Diperlukan' },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            $(element).parent().append("<br/>").append(error);
        },
        submitHandler: function (form) {

            if($('input[name="detail_id[]"]').length <= 0) {
                $(btnTambahDetail).focus();
                warningMessage('Peringatan!', "Detail CPPT harus diisi");
                return;
            }

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            blockPage();
            $.ajax({
                url: URL.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    $('.btn-save').hide();
                    successMessage('Success', "CPPT berhasil disimpan.");

                    setTimeout(() => {
                        window.location.assign(URL.index);
                    }, 1000);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    $('.btn-kembali').click(function() {
        window.location.assign(URL.index);
    });

    fillForm(UID);
});