$(() => {
    let view = (uid) => {
        MODAL_HISTORY.modal('show');
        blockJqElement(MODAL_HISTORY.find('.modal-dialog'));
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                MODAL_HISTORY.find(".disp-no_kartu").html(data.no_kartu);
                MODAL_HISTORY.find(".disp-nama").html(data.nama);

                let history = [];
                history.push({
                    status: 'Pengajuan',
                    no_sep: '&mdash;',
                    tgl: moment(data.created_at).format('DD/MM/YYYY'),
                    user: data.created_user.first_name + ' ' + data.created_user.last_name,
                    keterangan: data.keterangan,
                });
                switch (parseInt(data.status)) {
                    case 1:
                        break;
                    case 2:
                        history.push({
                            status: 'Disetujui',
                            no_sep: '&mdash;',
                            tgl: moment(data.setujui_at).format('DD/MM/YYYY'),
                            user: data.setujui_user.first_name + ' ' + data.setujui_user.last_name,
                            keterangan: data.keterangan,
                        });
                        break;
                }

                TABLE_HISTORY.find('tbody').empty();
                let no = 1;
                for (let h of history) {
                    TABLE_HISTORY.find('tbody').append(`
                        <tr>
                            <td class="text-center">${numeral(no).format()}.</td>
                            <td class="text-left">${h.status}</td>
                            <td class="text-left">${h.no_sep}</td>
                            <td class="text-center">${h.tgl}</td>
                            <td class="text-left">${h.user}</td>
                            <td class="text-left">${h.keterangan}</td>
                        </tr>
                    `);
                }

                MODAL_HISTORY.find('.modal-dialog').unblock();
            }
        });
    }

    let initialize = () => {
        initializeInput();
        initializeDatatable();
    }

    let initializeInput = () => {
        $('.rangetanggal-form').daterangepicker({
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            },
            startDate: moment(),
            endDate: moment(),
        });
    }

    let initializeDatatable = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "no",
                    "render": (data, type, row, meta) => {
                        return numeral(data).format() + '.';
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_kartu",
                    "render": (data, type, row, meta) => {
                        return `<a href="#" data-uid="${row.uid}" data-toggle="view">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "nama",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "tgl_sep",
                    "render": (data, type, row, meta) => {
                        return moment(data).format('DD/MM/YYYY');
                    },
                    "className": "text-left"
                },
                {
                    "data": "jenis_pelayanan",
                    "render": (data, type, row, meta) => {
                        if (parseInt(data) == 1) {
                            return 'RI';
                        }

                        return 'RJ'
                    },
                    "className": "text-left"
                },
                {
                    "data": "status",
                    "render": (data, type, row, meta) => {
                        let status = '';
                        switch (parseInt(data)) {
                            case 1:
                                status = `<a href="#" data-uid="${row.uid}" data-toggle="setujui">Pengajuan</a>`;
                                break;
                            case 2:
                                status = `Disetujui`;
                                break;
                        }

                        return status;
                    },
                    "className": "text-left"
                },
            ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        $("#table_search_tanggal").on('apply.daterangepicker', function (ev, picker) {
            TABLE_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 0);
            TABLE_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 1);
        });

        $("#table_search_tanggal").on('change keyup', function () {
            TABLE_DT.fnFilter($(this).data('daterangepicker').startDate.format('YYYY-MM-DD'), 0);
            TABLE_DT.fnFilter($(this).data('daterangepicker').endDate.format('YYYY-MM-DD'), 1);
        });

        TABLE_DT.fnFilter(moment().format('YYYY-MM-DD'), 0);
        TABLE_DT.fnFilter(moment().format('YYYY-MM-DD'), 1);


        // HANDLERS
        TABLE.on('click', '[data-toggle="view"]', function (e) {
            e.preventDefault();
            let uid = $(this).data('uid');

            view(uid);
        });
        TABLE.on('click', '[data-toggle="setujui"]', function (e) {
            e.preventDefault();
            let uid = $(this).data('uid');

            swal({
                title: 'Setujui Pengajuan SEP Tersebut?',
                text: '',
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Setujui',
                confirmButtonColor: "#48A74C",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url: URL.setujui,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        uid: uid,
                    },
                    success: function (res) {
                        successMessage('Success', "Pengajuan SEP Berhasil Disetujui.");
                        TABLE_DT.fnDraw(false);
                    },
                    error: function (err) {
                        console.log(err);

                        let response = json_decode(err.responseText);

                        errorMessage('Error', response.message || "Terjadi kesalahan saat hendak menyimpan Rujukan.");

                        swal.close();
                    },
                    complete: function () {
                        swal.close();
                    }
                });
            });
        });
    }

    initialize();
});