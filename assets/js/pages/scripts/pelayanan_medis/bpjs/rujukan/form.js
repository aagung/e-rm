$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                console.log(data);

                // Input Hiddens
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#tgl_rujukan").val(moment(data.tgl_rujukan).format('YYYY-MM-DD'));
                $("#no_rujukan").val(data.no_rujukan);
                $("#jenis_pelayanan").val(data.jenis_pelayanan);
                $("#no_sep").val(data.no_sep);
                $("#no_kartu").val(data.no_kartu);
                $("#nama").val(data.nama);
                $("#ppk_dirujuk").val(data.ppk_dirujuk);
                $("#catatan").val(data.catatan);
                $("#diag_rujukan").val(data.diag_rujukan);
                $("#tipe_rujukan").val(data.tipe_rujukan);
                $("#poli_rujukan").val(data.poli_rujukan);

                // Disp Input
                // $("#disp-nama").html(response.peserta.nama);
                // $("#disp-jenis_kelamin").html(response.peserta.kelamin == "L" ? "Laki-Laki" : "Perempuan");
                // $("#disp-jaminan").html(response.penjamin ? response.penjamin : "&mdash;");
                $("#disp-no_sep").html(data.no_sep);
                // $("#disp-jenis_rawat").html(response.jnsPelayanan);
                // $("#disp-tanggal_lahir").html(moment(response.tglLahir).format('DD/MM/YYYY'));
                // $("#disp-nik").html('&mdash;');
                $("#disp-no_peserta").html(data.no_kartu);
                // $("#disp-kelas_hak").val(response.kelasRawat);
                $("#disp-tgl_rujukan").val(moment(data.tgl_rujukan).format('DD/MM/YYYY')).trigger('change');
                $("#disp-jenis_pelayanan").val(data.jenis_pelayanan).trigger('change');
                $("#disp-diag_rujukan").val(data.diag_rujukan).trigger('change');
                $("#disp-ppk_dirujuk").val(data.ppk_dirujuk).trigger('change');
                $("#disp-poli_rujukan").val(data.poli_rujukan).trigger('change');
                $("#disp-catatan").val(data.catatan).trigger('change');
                $(`[name="disp-tipe_rujukan"][value="${data.tipe_rujukan}"]`).prop('checked', true);
                $(`[name="disp-tipe_rujukan"]`).trigger('change');


                $("#search_value").prop('readonly', true);
                $("#search_value").val(data.no_sep);
                $("#btn-search_sep").hide('slow');
                fillByNoRujukan(data.no_rujukan);
            }
        });
    }

    let fillByNoSep = (no_sep) => {
        blockJqElement(FORM);
        $.ajax({
            type: "GET",
            url: URL.getSep.replace(':NO_SEP', no_sep),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (! response) {
                    let message = '';

                    if (metaData.message.toLowerCase() == 'ok') {
                        message = metaData.code;
                    } else {
                        message = metaData.message;
                    }

                    let msg = `
                        <p class="text-center">${message}</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                let tanggal = moment();

                let rujukan = {};
                if (response.rujukan) {
                    rujukan = response.rujukan;
                }

                // Input Hiddens
                $("#id").val(0);
                $("#uid").val(0);
                $("#tgl_rujukan").val(tanggal.format('YYYY-MM-DD'));
                $("#no_rujukan").val('');
                $("#jenis_pelayanan").val('');
                $("#no_sep").val(response.noSep);
                $("#no_kartu").val(response.peserta.noKartu);
                $("#nama").val(response.peserta.nama);
                $("#ppk_dirujuk").val('');
                $("#catatan").val('');
                $("#diag_rujukan").val('');
                $("#tipe_rujukan").val('');
                $("#poli_rujukan").val('');

                // Disp Input
                $("#disp-nama").html(response.peserta.nama);
                $("#disp-jenis_kelamin").html(response.peserta.kelamin == "L" ? "Laki-Laki" : "Perempuan");
                $("#disp-jaminan").html(response.penjamin ? response.penjamin : "&mdash;");
                $("#disp-no_sep").html(response.noSep);
                $("#disp-jenis_rawat").html(response.jnsPelayanan);
                $("#disp-tanggal_lahir").html(moment(response.tglLahir).format('DD/MM/YYYY'));
                $("#disp-nik").html(rujukan.rujukan.peserta.nik);
                $("#disp-no_peserta").html(response.peserta.noKartu);
                $("#disp-kelas_hak").html(response.peserta.hakKelas);
                $("#disp-tgl_rujukan").val(tanggal.format('DD/MM/YYYY')).trigger('change');
                $("#disp-jenis_pelayanan").val(0).trigger('change');
                $("#disp-diag_rujukan").val('').trigger('change');
                $("#disp-ppk_dirujuk").val('').trigger('change');
                $("#disp-poli_rujukan").val('').trigger('change');
                $("#disp-catatan").val('').trigger('change');

                $("#search_value").prop('readonly', true);
                $("#btn-search_sep").hide('slow');
                $("#btn-simpan").prop('disabled', false);
            },
            error: (err) => {
                FORM.unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                FORM.unblock();
            }
        });
    }

    let fillByNoRujukan = (no_rujukan) => {
        // TODO: FILL BY NO. RUJUKAN
    }

    let initialize = () => {
        // FORM VALIDATION
        FORM.validate({
            rules: {
                "search_value": { required: true },
                "disp-tgl_rujukan": { required: true },
                "disp-jenis_pelayanan": { required: true, min: 1 },
                "disp-diag_rujukan": { required: true },
                "disp-ppk_dirujuk": { required: true },
                "disp-poli_rujukan": { required: true },
                "disp-catatan": { required: false },
            },
            messages: {
                "search_value": "No. SEP Diperlukan.",
                "disp-tgl_rujukan": "Tgl. Rujukan Diperlukan.",
                "disp-jenis_pelayanan": "Layanan Diperlukan.",
                "disp-diag_rujukan": "Diagnosa Rujukan Diperlukan.",
                "disp-ppk_dirujuk": "PPK Dirujuk Diperlukan.",
                "disp-poli_rujukan": "Spesialis / Subspesialis Diperlukan.",
                "disp-catatan": "Catatan Diperlukan.",
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockJqElement(FORM);
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        // TODO:. ....
                        // result = JSON.parse(result);
                        // var data = result.data;
                        // console.log(data);
                        successMessage('Success', "Data Hasil berhasil disimpan.");

                        $("#btn-simpan").prop('disabled', true);

                        setTimeout(() => {
                            // window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function (err) {
                        FORM.unblock();
                        console.log(err);

                        let response = json_decode(err.responseText);

                        errorMessage('Error', response.message || "Terjadi kesalahan saat hendak menyimpan Rujukan.");
                    },
                    complete: function () {
                        FORM.unblock();
                    }
                });
            }
        });

        $("#btn-simpan").click(function () {
            FORM.submit();
        });

        $("#btn-cetak").click(function () {
            let no_sep = $("#no_sep").val();
            let link = URL.cetak.replace(':NO_SEP', no_sep);

            window.open(link);
        });
        
        // INPUTS
        $("#disp-tgl_rujukan").formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        $("#disp-tgl_rujukan").on('change keyup blur', function (e) {
            if (moment($(this).val(), 'DD/MM/YYYY').isValid()) {
                FORM.find('[name="tgl_rujukan"]').val(moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
            } else {
                FORM.find('[name="tgl_rujukan"]').val("");
            }
        });

        $("#disp-tgl_rujukan").val(moment().format('DD/MM/YYYY')).trigger('change');

        // no_rujukan
        // no_sep
        // no_kartu
        // nama

        $("#disp-jenis_pelayanan").on('change', function () {
            $("#jenis_pelayanan").val($(this).val());
        });
        
        $("#disp-catatan").on('change', function () {
            $("#catatan").val($(this).val());
        });

        $('[name="disp-tipe_rujukan"]').on('change', function () {
            $("#tipe_rujukan").val($('[name="disp-tipe_rujukan"]:checked').val());
        });


        $("#btn-search_sep").click(function () {
            let search_by = $("#search_by").val().trim();
            let search_value = $("#search_value").val().trim();

            if (search_by == "") {
                swalHtml('Perhatian', `<p class="text-center">Silahkan Pilih Kategori Pencarian</p>`, 'warning');
            }

            if (search_value == "") {
                swalHtml('Perhatian', `<p class="text-center">Silahkan isi inputan pencarian</p>`, 'warning');
            }

            if (search_by == "sep") {
                fillByNoSep(search_value);
            }
        });

        // Initializes
        initializeSearchDiagnosa();
        initializeSearchPPKRujukan();
        initializeSearchPoli();

        $("#btn-simpan").prop('disabled', true);

        if (UID != "") {
            fillForm(UID);
        }
    }

    let initializeSearchDiagnosa = () => {
        $("#btn-search_diag_rujukan").click(function () {
            MODAL_SEARCH_DIAGNOSA.modal('show');
        });

        MODAL_SEARCH_DIAGNOSA.find('[data-action="cari"]').click(function () {
            let nama = MODAL_SEARCH_DIAGNOSA.find('[name="nama"]').val();

            if (nama.trim() == "") {
                nama = "a";
            }

            blockJqElement(TABLE_SEARCH_DIAGNOSA);
            $.ajax({
                type: "GET",
                url: URL.referensi.diagnosa.replace(':NAMA', nama),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_DIAGNOSA.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_DIAGNOSA.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let row;
                    for (let i = 0; i < response.diagnosa.length; i++) {
                        row = response.diagnosa[i];
                        TABLE_SEARCH_DIAGNOSA.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${row.kode}" data-nama="${row.nama}" data-action="click">
                                        ${row.kode}
                                    </a>
                                </td>
                                <td>${row.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_DIAGNOSA.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_DIAGNOSA.unblock();
                }
            });
        });

        TABLE_SEARCH_DIAGNOSA.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            console.log(kode, nama);

            MODAL_SEARCH_DIAGNOSA.modal('hide');

            $("#diag_rujukan").val(kode);
            $("#disp-diag_rujukan").val(nama);
        });
    }

    let initializeSearchPPKRujukan = () => {
        $("#btn-search_ppk_dirujuk").click(function () {
            MODAL_SEARCH_PPK_RUJUKAN.modal('show');
        })

        MODAL_SEARCH_PPK_RUJUKAN.find('[data-action="cari"]').click(function () {
            let jenis = MODAL_SEARCH_PPK_RUJUKAN.find('[name="jenis"]:checked').val();
            let nama = MODAL_SEARCH_PPK_RUJUKAN.find('[name="nama"]').val();

            if (! (parseInt(jenis) >= 1)) {
                jenis = 1;
            }

            if (nama.trim() == "") {
                nama = "a";
            }

            blockJqElement(TABLE_SEARCH_PPK_RUJUKAN);
            $.ajax({
                type: "GET",
                url: URL.referensi.faskes.replace(':NAMA', nama).replace(':JENIS', jenis),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_PPK_RUJUKAN.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_PPK_RUJUKAN.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let faskes;
                    for (let i = 0; i < response.faskes.length; i++) {
                        faskes = response.faskes[i];
                        TABLE_SEARCH_PPK_RUJUKAN.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${faskes.kode}" data-nama="${faskes.nama}" data-action="click">
                                        ${faskes.kode}
                                    </a>
                                </td>
                                <td>${faskes.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_PPK_RUJUKAN.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_PPK_RUJUKAN.unblock();
                }
            });
        });

        TABLE_SEARCH_PPK_RUJUKAN.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            let jenis = MODAL_SEARCH_PPK_RUJUKAN.find('[name="jenis"]:checked').val();
            if (! (parseInt(jenis) >= 1)) {
                jenis = 1;
            }

            console.log(kode, nama);

            MODAL_SEARCH_PPK_RUJUKAN.modal('hide');

            $("#ppk_dirujuk").val(kode);
            $("#disp-ppk_dirujuk").val(`${kode} - ${nama}`);

        });
    }

    let initializeSearchPoli = () => {
        $("#btn-search_poli_rujukan").click(function () {
            MODAL_SEARCH_POLI.modal('show');
        });

        MODAL_SEARCH_POLI.find('[data-action="cari"]').click(function () {
            let nama = MODAL_SEARCH_POLI.find('[name="nama"]').val();

            if (nama.trim() == "") {
                nama = "a";
            }

            blockJqElement(TABLE_SEARCH_POLI);
            $.ajax({
                type: "GET",
                url: URL.referensi.poli.replace(':NAMA', nama),
                success: (res) => {
                    res = json_decode(res);
                    console.log(res);
                    let metaData = res.metaData;
                    let response = res.response;

                    TABLE_SEARCH_POLI.find('tbody').empty();

                    if (! response) {
                        TABLE_SEARCH_POLI.find('tbody').html(`
                            <tr>
                                <td class="text-center" colspan="2">Tidak ada hasil yang dapat ditampilkan.</td>
                            </tr>
                        `);
                        return;
                    }

                    let row;
                    for (let i = 0; i < response.poli.length; i++) {
                        row = response.poli[i];
                        TABLE_SEARCH_POLI.find('tbody').append(`
                            <tr>
                                <td class="text-center">
                                    <a href="#" data-kode="${row.kode}" data-nama="${row.nama}" data-action="click">
                                        ${row.kode}
                                    </a>
                                </td>
                                <td>${row.nama}</td>
                            </tr>
                        `);
                    }
                },
                error: (err) => {
                    TABLE_SEARCH_POLI.unblock();
                    console.log(err);
                    let msg = `
                        <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                        <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                        <br/>
                        <p class="text-center">Terima Kasih</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                },
                complete: () => {
                    TABLE_SEARCH_POLI.unblock();
                }
            });
        });

        TABLE_SEARCH_POLI.on('click', '[data-action="click"]', function (e) {
            e.preventDefault();

            let kode = $(this).data('kode');
            let nama = $(this).data('nama');

            console.log(kode, nama);

            MODAL_SEARCH_POLI.modal('hide');

            $("#poli_rujukan").val(kode);
            $("#disp-poli_rujukan").val(nama);
        });
    }

    initialize();
})