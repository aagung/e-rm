$(() => {
    let subsDate = (range, tipe) => {
        let date = range.substr(0, 10);
        if(tipe === "sampai") date = range.substr(13, 10);
        return getDate(date);
    }

    let initialize = () => {
        initializeInputs();
        initializeDatatable();
    }

    let initializeInputs = () => {
        $('.rangetanggal-form').daterangepicker({
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            },
            startDate: moment(),
            endDate: moment(),
        });
    }

    let initializeDatatable = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "no",
                    "render": (data, type, row, meta) => {
                        return numeral(data).format() + '.';
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rujukan",
                    "render": (data, type, row, meta) => {
                        let link = URL.form.replace(':UID', row.uid);
                        return `<a href="${link}">${data}</a>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "jenis_pelayanan",
                    "render": (data, type, row, meta) => {
                        if (parseInt(data) == 1) {
                            return 'RI';
                        }

                        return 'RJ'
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_sep",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_kartu",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "nama",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "ppk_dirujuk",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
            ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        $("#table_search_tanggal").on('apply.daterangepicker', function (ev, picker) {
            TABLE_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 0);
            TABLE_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 1);
        });

        $("#table_search_tanggal").on('change keyup', function () {
            TABLE_DT.fnFilter($(this).data('daterangepicker').startDate.format('YYYY-MM-DD'), 0);
            TABLE_DT.fnFilter($(this).data('daterangepicker').endDate.format('YYYY-MM-DD'), 1);
        });

        $('#btn-reset').on('click', () => {
            $("#table_search_tanggal").data("daterangepicker").setStartDate(moment())
            $("#table_search_tanggal").data("daterangepicker").setEndDate(moment());

            $("#table_search_tanggal").trigger('change');
        });
    }

    initialize();
});