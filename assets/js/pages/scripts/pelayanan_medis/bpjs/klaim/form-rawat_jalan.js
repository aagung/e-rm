/**
 * CARA PULANG: (E-Klaim)
 * 1 = Atas persetujuan dokter
 * 2 = Dirujuk
 * 3 = Atas permintaan sendiri
 * 4 = Meninggal
 * 5 = Lain-lain
 */

$(() => {
    let aDiagnosaSekunder = [];
    // DIAGNOSA
    let initializeDiagnosa = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.code}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.description}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.code || data.text || '- Pilih -';
        }

        $(`#${el}`).select2({
            placeholder: "- Pilih -",
            ajax: {
                url: URL.getIcd10,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.uid, description: obj.description, code: obj.code };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        $(`#${el}`).on('select2:select', function (e) {
            var data = e.params.data;
            if(typeof data !== "undefined") {
                if(el === "disp-diagnosa_sekunder") {
                    aDiagnosaSekunder[data.id] = data;
                } else {
                    $(`#${el}`).data('diagnosa', data);
                }

                generateDiagnosaRequest();
            }
        });
    }
    let generateDiagnosaRequest = () => {
        let codes = [];

        if ($("#disp-diagnosa_utama").data('diagnosa')) {
            codes.push($("#disp-diagnosa_utama").data('diagnosa').code);
        }

        for (let i in aDiagnosaSekunder) {
            codes.push(aDiagnosaSekunder[i].code);
        }

        $("#diagnosa").val(codes.length > 0 ? codes.join('#') : '#');
    }

    let initializeProcedure = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.kd_ina_cbgs}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.nm_cbgs}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.kd_ina_cbgs || data.text || '- Pilih -';
        }

        $(`#${el}`).select2({
            placeholder: "- Pilih -",
            ajax: {
                url: URL.getIcd9,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.uid, nm_cbgs: obj.nm_cbgs, kd_ina_cbgs: obj.kd_ina_cbgs };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        $(`#${el}`).on('select2:select', function (e) {
            var data = e.params.data;
            if(typeof data !== "undefined") {
                $(`#${el}`).data('procedure', data);

                generateProcedureRequest();
            }
        });
    }
    let generateProcedureRequest = () => {
        let codes = [];

        if ($("#disp-procedure").data('procedure')) {
            codes.push($("#disp-procedure").data('procedure').kd_ina_cbgs);
        }

        $("#procedure").val(codes.length > 0 ? codes.join('#') : '#');
    }

    let getData = (uid, cb) => {
        $.getJSON(URL.getData.data.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                SEP = data;

                if (cb) {
                    cb(res);
                }
            }
        });
    }

    let getSep = (no_sep, cb) => {
        blockElement('.sep-section');
        $.ajax({
            type: "GET",
            url: URL.getData.sep.replace(':NO_SEP', no_sep),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (response == null) {
                    let msg = metaData.message ? `<p class="text-center">${metaData.message}</p>` : `<p class="text-center">Data Dengan No. SEP '${no_sep}' Tidak Ditemukan.</p>`;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                if (cb) {
                    cb(res);
                }
            },
            error: (err) => {
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                $('.sep-section').unblock();
            }
        });
    }

    let getPelayanan = (uid, cb) => {
        $.getJSON(URL.getData.pelayanan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                if (cb) {
                    cb(res);
                }
            }
        });
    }

    let getGrouper = (uid, cb) => {
        blockElement('.grouper-section');
        $.getJSON(URL.getData.grouper.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                let total = numeral(res.total)._value;
                console.log('getGrouper', data);

                $("#disp-total").html(numeral(total).format());
                $("#disp-prosedur_non_bedah").autoNumeric('set', data.prosedur_non_bedah);
                $("#disp-tenaga_ahli").autoNumeric('set', data.tenaga_ahli);
                $("#disp-radiologi").autoNumeric('set', data.radiologi);
                $("#disp-rehabilitasi").autoNumeric('set', data.rehabilitasi);
                $("#disp-obat").autoNumeric('set', data.obat);
                $("#disp-alkes").autoNumeric('set', data.alkes);
                $("#disp-prosedur_bedah").autoNumeric('set', data.prosedur_bedah);
                $("#disp-keperawatan").autoNumeric('set', data.keperawatan);
                $("#disp-laboratorium").autoNumeric('set', data.laboratorium);
                $("#disp-kamar").autoNumeric('set', data.kamar);
                $("#disp-obat_kronis").autoNumeric('set', data.obat_kronis);
                $("#disp-bmhp").autoNumeric('set', data.bmhp);
                $("#disp-konsultasi").autoNumeric('set', data.konsultasi);
                $("#disp-penunjang").autoNumeric('set', data.penunjang);
                $("#disp-pelayanan_darah").autoNumeric('set', data.pelayanan_darah);
                $("#disp-rawat_intensif").autoNumeric('set', data.rawat_intensif);
                $("#disp-obat_kemoterapi").autoNumeric('set', data.obat_kemoterapi);
                $("#disp-sewa_alat").autoNumeric('set', data.sewa_alat);

                // Input Hidden
                $("#tarif_prosedur_non_bedah").val(data.prosedur_non_bedah);
                $("#tarif_prosedur_bedah").val(data.prosedur_bedah);
                $("#tarif_konsultasi").val(data.konsultasi);
                $("#tarif_tenaga_ahli").val(data.tenaga_ahli);
                $("#tarif_keperawatan").val(data.keperawatan);
                $("#tarif_penunjang").val(data.penunjang);
                $("#tarif_radiologi").val(data.radiologi);
                $("#tarif_laboratorium").val(data.laboratorium);
                $("#tarif_pelayanan_darah").val(data.pelayanan_darah);
                $("#tarif_rehabilitasi").val(data.rehabilitasi);
                $("#tarif_kamar").val(data.kamar);
                $("#tarif_rawat_intensif").val(data.rawat_intensif);
                $("#tarif_obat").val(data.obat);
                $("#tarif_obat_kronis").val(data.obat_kronis);
                $("#tarif_obat_kemoterapi").val(data.obat_kemoterapi);
                $("#tarif_alkes").val(data.alkes);
                $("#tarif_bmhp").val(data.bmhp);
                $("#tarif_sewa_alat").val(data.sewa_alat);

                if (cb) {
                    cb(res);
                }

                $('.grouper-section').unblock();
            }
        });
    }

    let fillForm = (uid) => {
        getData(uid, (response) => {
            let obj = response.data;

            if (obj.klaim) {
                let klaim = obj.klaim;
                $("#id").val(klaim.id);
                $("#uid").val(klaim.uid);
                $("#diagnosa").val(klaim.diagnosa);
                $("#procedure").val(klaim.procedure);

                // DISP Diagnosa Utama
                $("#disp-diagnosa_utama").val('').trigger('change');
                if(klaim.diagnosa_utama_obj) {
                    let optDiagnosaUtama = new Option(klaim.diagnosa_utama_obj.code, klaim.diagnosa_utama_obj.uid, true, true);
                    $("#disp-diagnosa_utama").append(optDiagnosaUtama).trigger('change');

                    $("#disp-diagnosa_utama").trigger({
                        type: 'select2:select',
                        params: {
                            data: {
                                    id: klaim.diagnosa_utama_obj.uid,
                                    code: klaim.diagnosa_utama_obj.title,
                                    text: klaim.diagnosa_utama_obj.code,
                                    description: klaim.diagnosa_utama_obj.description,
                                }
                        }
                    });

                    $("#disp-diagnosa_utama").data('diagnosa', {
                        id: klaim.diagnosa_utama_obj.uid,
                        code: klaim.diagnosa_utama_obj.title,
                        text: klaim.diagnosa_utama_obj.code,
                        description: klaim.diagnosa_utama_obj.description,
                    });
                }

                // DISP Diagnosa Sekunder
                aDiagnosaSekunder = [];
                $("#disp-diagnosa_sekunder").val('').trigger('change');
                if(klaim.diagnosa_sekunder_arr) {
                    let optDiagnosaSekunder;
                    for (var i = 0; i < klaim.diagnosa_sekunder_arr.length; i++) {
                        optDiagnosaSekunder = new Option(klaim.diagnosa_sekunder_arr[i].code, klaim.diagnosa_sekunder_arr[i].uid, true, true);
                        $("#disp-diagnosa_sekunder").append(optDiagnosaSekunder).trigger('change');

                        $("#disp-diagnosa_sekunder").trigger({
                            type: 'select2:select',
                            params: {
                                data:  {
                                        id: klaim.diagnosa_sekunder_arr[i].uid,
                                        code: klaim.diagnosa_sekunder_arr[i].title,
                                        text: klaim.diagnosa_sekunder_arr[i].code,
                                        description: klaim.diagnosa_sekunder_arr[i].description,
                                    }
                            }
                        });

                        aDiagnosaSekunder[klaim.diagnosa_sekunder_arr[i].id] = {
                            id: klaim.diagnosa_sekunder_arr[i].uid,
                            code: klaim.diagnosa_sekunder_arr[i].title,
                            text: klaim.diagnosa_sekunder_arr[i].code,
                            description: klaim.diagnosa_sekunder_arr[i].description,
                        };
                    }
                }

                // Disp Procedure
                $("#disp-procedure").val('').trigger('change');
                if(klaim.procedure_obj) {
                    let optProcedure = new Option(klaim.procedure_obj.kd_ina_cbgs, klaim.procedure_obj.uid, true, true);
                    $("#disp-procedure").append(optProcedure).trigger('change');

                    $("#disp-procedure").trigger({
                        type: 'select2:select',
                        params: {
                            data: {
                                    id: klaim.procedure_obj.uid,
                                    kd_ina_cbgs: klaim.procedure_obj.kd_ina_cbgs,
                                    text: klaim.procedure_obj.kd_ina_cbgs,
                                    nm_cbgs: klaim.procedure_obj.nm_cbgs,
                                }
                        }
                    });
                }

                // TODO: BUGS DIAGNOSA
                // generateDiagnosaRequest();
            }

            $("#sep_id").val(obj.id);
            $("#jenis_rawat").val(obj.jenis_pelayanan);
            $("#kelas_rawat").val(2); // Rawat Jalan Default 2

            // TODO: INPUT HIDDEN
            $("#tarif_poli_eks").val(0);

            getSep(obj.no_sep, (res) => {
                let data = res.response;
                let peserta = data.peserta;
                let rujukan = data.rujukan.rujukan;
                console.log('getSep', data);

                // Fill Form
                if (data) {
                    // ROW 1
                    $("#disp-nama").val(data.peserta.nama);
                    $("#disp-jenis_kelamin").val(data.peserta.kelamin);
                    $("#disp-penjamin").val(data.penjamin ? data.penjamin : '-');
                    $("#disp-no_sep").val(data.noSep);
                    $("#disp-tanggal_lahir").val(moment(data.peserta.tglLahir).format('DD/MM/YYYY'));
                    $("#disp-no_peserta").val(data.peserta.noKartu);
                    $("#disp-cob").val(''); // Rawat Jalan tidak bisa COB

                    if (rujukan) {
                        $("#disp-nik").val(rujukan.peserta.nik);
                    } else {
                        $("#disp-nik").val('-');
                    }


                    // ROW 2
                    $("#disp-jenis_pelayanan").val(data.jnsPelayanan);
                    $("#disp-eksekutif").prop('checked', parseInt(data.poliEksekutif) == 1);
                    $("#disp-kelas_rawat").val('Kelas 2'); // Rawat jalan Default Kelas 2
                    $("#disp-umur").val(rujukan.peserta.umur.umurSekarang);
                    $("#disp-birth_weight").val('-');
                    $("#disp-discharge_status").val('Atas permintaan sendiri'); // Default Untuk Rawat Jalan, TODO: AMBIL DARI DATA PELAYANAN

                    // INPUT HIDDEN
                    $("#nomor_sep").val(data.noSep);
                    $("#nomor_kartu").val(data.peserta.noKartu);
                    $("#adl_sub_acute").val(''); // TODO: Ngambil Dari Mana?
                    $("#adl_chronic").val(''); // TODO: Ngambil Dari Mana?
                    $("#icu_indikator").val(0); // 0 karena Rawat Jalan
                    $("#icu_los").val(0); // 0 karena Rawat Jalan
                    $("#ventilator_hour").val(0); // 0 Karena Rawat Jalan
                    $("#upgrade_class_ind").val(0); // 0 Karena Rawat Jalan
                    $("#upgrade_class_class").val(0); // 0 Karena Rawat Jalan
                    $("#upgrade_class_los").val(0); // 0 Karena Rawat Jalan
                    $("#add_payment_pct").val(0); // 0 Karena Rawat Jalan
                    $("#payor_id").val(); // TODO: Ambil Data Di Applikasi E-Klaim
                    $("#payor_cd").val(); // TODO: Ambil Data Di Applikasi E-Klaim
                    $("#cob_cd").val('#'); // TODO: Ambil Dari Mana?
                    $("#coder_nik").val(''); // Katanya Optional

                    // TODO:
                    $("#disp-los").html('-');
                    $("#disp-adl_sub_acute").html('-');
                    $("#disp-adl_chronic").html('-');
                }
            });
            getPelayanan(uid, (res) => {
                let data = res.data;
                console.log('getPelayanan', data);

                $("#disp-tanggal_masuk").val(moment(data.tanggal).format('DD/MM/YYYY'));
                $("#disp-tanggal_keluar").val(moment(data.tanggal).format('DD/MM/YYYY'));
                $("#disp-dpjp").val(data.dokter);
                $("#disp-kode_tarif").val(data.cara_bayar);

                // INPUT HIDDEN
                $("#tgl_masuk").val(data.tanggal);
                $("#tgl_pulang").val(data.tanggal);
                $("#birth_weight").val(0);
                $("#discharge_status").val(3); // Rawat Jalan Default Atas Permintaan Sendiri
                $("#nama_dokter").val(data.dokter);
                $("#kode_tarif").val('AP'); // TODO: Sementara Pakai AP
            });
            getGrouper(uid, (res) => {
                // CALLBACK
            });
        });
    }

    /**
     * Initialize Form
     */
    let initializeForm = () => {
        // Initialize Form
        FORM.validate({
            rules: {
                "disp-diagnosa_utama": { required: true },
                "disp-diagnosa_sekunder[]": { required: false },
                "disp-procedure": { required: true },
            },
            messages: {
                "disp-diagnosa_utama": "Diagnosa Utama Diperlukan.",
                "disp-diagnosa_sekunder[]": "Diagnosa Sekunder Diperlukan.",
                "disp-procedure": "Procedure Diperlukan.",
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockJqElement(FORM);
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        let data = result.data;

                        // Set ID & UID
                        $("#id").val(data.id);
                        $("#uid").val(data.uid);

                        successMessage('Success', "Data Klaim Hasil berhasil disimpan.");
                    },
                    error: function (err) {
                        FORM.unblock();
                        console.log(err);

                        let response = json_decode(err.responseText);

                        errorMessage('Error', response.message || "Terjadi kesalahan saat hendak menyimpan Rujukan.");
                    },
                    complete: function () {
                        FORM.unblock();
                    }
                });
            }
        });
    }

    let initializeInputs = () => {
        // initializeInputs
        $('.grouper-autonumeric').autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'Rp. ', 'pSign': 'p'});

        initializeDiagnosa("disp-diagnosa_utama");
        initializeDiagnosa("disp-diagnosa_sekunder");
        initializeProcedure("disp-procedure");

        $(`#disp-diagnosa_sekunder`).on('select2:unselect', function (e) {
            var data = e.params.data;
            delete aDiagnosaSekunder[data.id];
            generateDiagnosaRequest();
        });

        BTN_RELOAD_TARIF.click((e) => {
            e.preventDefault();
            getGrouper(SEP.uid, (res) => {
                console.log('BTN_RELOAD_TARIF.click', res);
            });
        })
    }

    let initialize = () => {
        initializeForm();
        initializeInputs();
    }

    initialize();

    if (UID && UID != "") {
        fillForm(UID);
    }
});