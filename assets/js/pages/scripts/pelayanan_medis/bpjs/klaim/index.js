$(() => {
    let initialize = () => {
        initializeInput();
        initializeDatatable();
    }

    let initializeInput = () => {
        // $('.rangetanggal-form').daterangepicker({
        //     applyClass: "bg-slate-600",
        //     cancelClass: "btn-default",
        //     opens: "center",
        //     autoApply: true,
        //     locale: {
        //         format: "DD/MM/YYYY"
        //     },
        //     startDate: moment(),
        //     endDate: moment(),
        // });
        // $('.singletanggal-form').daterangepicker({
        //     singleDatePicker: true,
        //     applyClass: "bg-slate-600",
        //     cancelClass: "btn-default",
        //     opens: "center",
        //     autoApply: true,
        //     locale: {
        //         format: "DD/MM/YYYY"
        //     },
        //     startDate: moment(),
        //     endDate: moment(),
        // });

        $("select").select2();
    }

    let initializeDatatable = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "tanggal_masuk",
                    "render": (data, type, row, meta) => {
                        let linkRwj = URL.rawat_jalan.replace(':UID', row.uid);
                        let linkRanap = URL.rawat_inap.replace(':UID', row.uid);
                        let tgl = moment(data).format('DD/MM/YYYY');

                        if (parseInt(row.jenis_pelayanan) == 1) {
                            return `<a href="${linkRanap}">${tgl}</a>`;
                        }

                        return `<a href="${linkRwj}">${tgl}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "tanggal_keluar",
                    "render": (data, type, row, meta) => {
                        return moment(data).format('DD/MM/YYYY');
                    },
                    "className": "text-center"
                },
                {
                    "data": "penjamin",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_sep",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "jenis_pelayanan",
                    "render": (data, type, row, meta) => {
                        if (parseInt(data) == 1) {
                            return 'RI';
                        }

                        return 'RJ'
                    },
                    "className": "text-left"
                },
                {
                    "data": "cbg",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "status",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "create_user",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
            ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        // FILTER
        $("#table_search_value").on('change keyup blur', function () {
            let value = $(this).val();

            // TABLE_DT.fnFilter(value, 0);
        });
        $("#btn-cari").click(function () {
            let value = $("#table_search_value").val();
            
            TABLE_DT.fnFilter(value, 0);
        });

        $("#table_search_nama").on('change keyup blur', function () {
            let value = $(this).val();

            TABLE_DT.fnFilter(value, 1);
        });

        $("#table_search_jenis_kelamin").on('change keyup blur', function () {
            let value = $(this).val();

            TABLE_DT.fnFilter(value, 2);
        });

        $("#table_search_tanggal_lahir").formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        $("#table_search_tanggal_lahir").on('change keyup blur', function () {
            let value = moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

            if (moment(value).isValid()) {
                TABLE_DT.fnFilter(value, 3);
            } else {
                TABLE_DT.fnFilter('', 3);
            }
        });
        // $("#table_search_tanggal_lahir").on('apply.daterangepicker', function (ev, picker) {
        //     TABLE_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 3);
        // });
        // $("#table_search_tanggal_lahir").on('change keyup blur', function () {
        //     let value = $(this).data('daterangepicker').startDate.format('YYYY-MM-DD');

        //     TABLE_DT.fnFilter(value, 3);
        // });

        $("#table_search_nik").on('change keyup blur', function () {
            let value = $(this).val();

            TABLE_DT.fnFilter(value, 4);
        });


        // HANDLERS
        TABLE.on('click', '[data-toggle="view"]', function (e) {
            e.preventDefault();
            let uid = $(this).data('uid');

            view(uid);
        });
    }

    initialize();
});