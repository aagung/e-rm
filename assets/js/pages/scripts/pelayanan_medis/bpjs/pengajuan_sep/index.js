$(() => {
    let fillForm = (noKartu) => {
        blockJqElement(FORM);
        $.ajax({
            type: "GET",
            url: URL.getPeserta.replace(':NO_KARTU', noKartu).replace(':TGL', moment().format('YYYY-MM-DD')),
            success: (res) => {
                res = json_decode(res);
                console.log(res);
                let metaData = res.metaData;
                let response = res.response;

                if (! response) {
                    let message = '';

                    if (metaData.message.toLowerCase() == 'ok') {
                        message = metaData.code;
                    } else {
                        message = metaData.message;
                    }

                    let msg = `
                        <p class="text-center">${message}</p>
                    `;
                    swalHtml('Perhatian', msg, 'warning');
                    return;
                }

                let tanggal = moment();

                console.log(response);

                // Input Hidden
                $("#id").val(0);
                $("#uid").val(0);
                $("#no_kartu").val(response.peserta.noKartu);
                $("#nama").val(response.peserta.nama);
                $("#tgl_sep").val(moment().format('YYYY-MM-DD'));
                $("#jenis_pelayanan").val(0);
                $("#keterangan").val("");

                // Disp Input
                $("#disp-nama").html(response.peserta.nama);
                $("#disp-no_kartu").html(response.peserta.noKartu);
                $("#disp-tgl_sep").val(moment().format('DD/MM/YYYY')).trigger('change');
                $("#disp-jenis_pelayanan").val(0).trigger('change');
                $("#disp-keterangan").val("").trigger('change');

                $("#search_no_kartu").prop('readonly', true);
                $("#btn-search").hide('slow');
                $("#btn-simpan").prop('disabled', false);
                $("#btn-batal").show('slow');
            },
            error: (err) => {
                FORM.unblock();
                console.log(err);
                let msg = `
                    <p class="text-center">Koneksi dengan server BPJS sedang mengalami gangguan</p>
                    <p class="text-center">Silahkan menunggu beberapa saat lagi.</p>
                    <br/>
                    <p class="text-center">Terima Kasih</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            },
            complete: () => {
                FORM.unblock();
            }
        });
    }

    let clearForm = () => {
        // Input Hidden
        $("#id").val(0);
        $("#uid").val(0);
        $("#no_kartu").val("");
        $("#nama").val("");
        $("#tgl_sep").val(moment().format('YYYY-MM-DD'));
        $("#jenis_pelayanan").val(0);
        $("#keterangan").val("");

        // Disp Input
        $("#disp-nama").html("&mdash;");
        $("#disp-no_kartu").html("&mdash;");
        $("#disp-tgl_sep").val(moment().format('DD/MM/YYYY')).trigger('change');
        $("#disp-jenis_pelayanan").val(0).trigger('change');
        $("#disp-keterangan").val("").trigger('change');

        $("#search_no_kartu").val('');

        $("#search_no_kartu").prop('readonly', false);
        $("#btn-search").show('slow');
        $("#btn-simpan").prop('disabled', true);
        $("#btn-batal").hide('slow');
    }

    let initialize = () => {
        // FORM VALIDATION
        FORM.validate({
            rules: {
                "search_no_kartu": { required: true },
                "disp-tgl_sep": { required: true },
                "disp-jenis_pelayanan": { required: true, min: 1 },
                "disp-keterangan": { required: true },
            },
            messages: {
                "search_no_kartu": "No. Kartu Diperlukan.",
                "disp-tgl_sep": "Tgl. SEP Diperlukan.",
                "disp-jenis_pelayanan": "Jenis Pelayanan Diperlukan.",
                "disp-keterangan": "Keterangan Diperlukan.",
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockJqElement(FORM);
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        // TODO:. ....
                        // result = JSON.parse(result);
                        // var data = result.data;
                        // console.log(data);
                        successMessage('Success', "Pengajuan SEP Berhasil Dibuat.");

                        $("#btn-simpan").prop('disabled', true);

                        setTimeout(() => {
                            clearForm();
                        }, 3000);
                    },
                    error: function (err) {
                        FORM.unblock();
                        console.log(err);

                        let response = json_decode(err.responseText);

                        errorMessage('Error', response.message || "Terjadi kesalahan saat hendak mengajukan SEP.");
                    },
                    complete: function () {
                        FORM.unblock();
                    }
                });
            }
        });

        // Input Change
        $("#disp-tgl_sep").formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        $("#disp-tgl_sep").on('change keyup blur', function (e) {
            if (moment($(this).val(), 'DD/MM/YYYY').isValid()) {
                FORM.find('[name="tgl_sep"]').val(moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
            } else {
                FORM.find('[name="tgl_sep"]').val("");
            }
        });

        $("#disp-jenis_pelayanan").on('change', function () {
            $("#jenis_pelayanan").val($(this).val());
        });

        $("#disp-keterangan").on('change', function () {
            $("#keterangan").val($(this).val());
        });

        $("#btn-search").click(function () {
            let noKartu = $("#search_no_kartu").val();

            if (noKartu) {
                fillForm(noKartu);
            } else {
                let msg = `
                    <p class="text-center">Silahkan Isi No. Kartu</p>
                `;
                swalHtml('Perhatian', msg, 'warning');
            }
        });

        $("#btn-batal").click(function () {
            clearForm();
        });

        // Init
        $("#btn-simpan").prop('disabled', true);
    }

    initialize();
});