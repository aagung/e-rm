$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                $("#label-nama_pasien").html(data.pasien.nama);
                $("#label-no_rm").html(data.pasien.no_rm);
                $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#label-ttl").html(`${data.pasien.tempat_lahir}, ${data.pasien.tanggal_lahir}`);
                $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $("#label-alamat").html(data.pasien.alamat);
                $("#label-no_telepon").html(data.pasien.no_telepon_1);

                if(!data.data_pemeriksaan) data.data_pemeriksaan = {};
                // Isi Table Keperawatan Saat Ini
                let tr = $('<tr/>');
                tr.append($('<td/>').html(moment(data.tanggal).isValid() ? moment(data.tanggal).format('DD/MM/YYYY HH:mm') : '-'));
                tr.append($('<td/>').html(`<a class="show-row" data-popup="tooltip" title="Lihat Status Assesment" data-uid="${data.uid}">${data.no_register}</a>`));
                tr.append($('<td/>').html(data.layanan));
                tr.append($('<td/>').html(data.cara_bayar + (data.cara_bayar.search(/asuransi/i) !== -1 || data.cara_bayar.search(/perusahaan/i) !== -1 ? `<br/><span class="text-size-mini text-info">${data.perusahaan}</span>` : "")));
                tr.append($('<td/>').html(data.dokter));
                tr.append($('<td/>').html(data.data_pemeriksaan.diagnosis !== undefined ? data.data_pemeriksaan.diagnosis : "&mdash;"));
                tableKeperawatan.find('tbody').append(tr);
                tableKeperawatan.find('[data-popup=tooltip]').tooltip();

                ID = data.id;
                PASIEN_ID = data.pasien_id;
                fillTab("riwayat_keperawatan");
            }
        });
    }

    let fillListHistoryLab = (obj, no, elID) => {
        let tbody = $(`${elID} tbody`);

        if(tbody.children('.tr-data').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .addClass('tr-data')
            .appendTo(tbody);

        let tdNo = $("<td/>")
            .addClass('text-center')
            .html(no)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .html(obj.nama)
            .appendTo(tr);

        let tdHasil = $("<td/>")
            .html(obj.value ? obj.value : "&mdash;")
            .appendTo(tr);
        
        let tdSatuan = $("<td/>")
            .html(obj.satuan ? obj.satuan : "&mdash;")
            .appendTo(tr);

        let tdNilaiNormal = $("<td/>")
            .html(obj.nilai_normal ? obj.nilai_normal : "&mdash;")
            .appendTo(tr);
    }

    let fillListHistoryRad = (obj, no, elID) => {
        let tbody = $(`${elID} tbody`);

        if(tbody.children('.tr-data').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .addClass('tr-data')
            .appendTo(tbody);

        let tdNo = $("<td/>")
            .addClass('text-center')
            .html(no)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .html(obj.nama)
            .appendTo(tr);

        let tdNoFoto = $("<td/>")
            .addClass('text-center')
            .html(obj.no_foto ? obj.no_foto : "&mdash;")
            .appendTo(tr);

        let tdUkuranFoto = $("<td/>")
            .addClass('text-center')
            .html(obj.ukuran_foto ? obj.ukuran_foto : "&mdash;")
            .appendTo(tr);
        
        let tdFE = $("<td/>")
            .addClass('text-center')
            .html(obj.faktor_eksposisi ? obj.faktor_eksposisi : "&mdash;")
            .appendTo(tr);
    }

    let contentFotoRad = (obj, elID) => {
        let div = $('<div/>')
            .addClass(`col-sm-3`)
            .appendTo(elID);
            let panel = $('<div/>')
                .addClass(`panel panel-default`)
                .appendTo(div);
            let panelBody = $('<div/>')
                .addClass(`panel-body no-padding`)
                .appendTo(panel);
                let img = $('<div/>')
                    .appendTo(panelBody);
                img.html(`
                    <a href="${obj.link}" target="_blank" data-popup="lightbox">
                        <img src="${obj.link}" style="width: 100%;">
                    </a>
                `);

        if($(elID).html() !== "") {
            $(tabRad).find('.no-preview-image').hide();
        } else $(tabRad).find('.no-preview-image').show();
    }

    let fillListHistoryObat = (obj, no, elID) => {
        let tbody = $(`${elID} tbody`);

        if(tbody.children('.tr-data').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .addClass('tr-data')
            .appendTo(tbody);

        let tdNo = $("<td/>")
            .addClass('text-center')
            .html(no)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .html(obj.nama + obj.label_list_obat)
            .appendTo(tr);

        let tdSatuan = $("<td/>")
            .addClass('text-center')
            .html(obj.satuan ? obj.satuan : "&mdash;")
            .appendTo(tr);
        
        let tdCaraBuat = $("<td/>")
            .html(obj.label_cara_buat ? obj.label_cara_buat : "&mdash;")
            .appendTo(tr);

        let tdMetode = $("<td/>")
            .html(obj.label_metode ? obj.label_metode : "&mdash;")
            .appendTo(tr);
        
        let tdQty = $("<td/>")
            .addClass('text-right')
            .html(numeral(obj.quantity).format())
            .appendTo(tr);

        let tdSigna = $("<td/>")
            .html(obj.label_signa)
            .appendTo(tr);

        let tdPeracik = $("<td/>")
            .html(obj.peracik ? obj.peracik : "&mdash;")
            .appendTo(tr);
    }

    let fillTab = (browse) => {
        switch(browse) {
            case "riwayat_keperawatan":
                blockElement(tabKeperawatan);
                let isDTable = $.fn.dataTable.isDataTable(tableHistoryKeperawatan);
                if(!isDTable) {
                    tableHistoryKeperawatanDt = tableHistoryKeperawatan.DataTable({
                        "searching": false,
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                            "url": URL.loadHistoryKeperawatan,
                            "type": "POST",
                            "data": function(p) {
                                p.pelayanan_id = btoa(ID);
                                p.pasien_id = btoa(PASIEN_ID);
                            }
                        },
                        "columns": [
                            {
                                "data": "tanggal",
                                "render": (data, type, row, meta) => {
                                    return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                                },
                            },
                            {
                                "data": "no_register",
                                "orderable": false,
                                "render": (data, type, row, meta) => {
                                    return `<a href="${URL.statusAssesment.replace(':UID', row.uid).replace(':BROWSE', row.unit)}" target="_blank" data-popup="tooltip" title="Lihat Status Assesment">${data}</a>`;
                                },
                            },
                            { 
                                "data": "layanan",
                                "orderable": false,
                            },
                            {
                                "data": "cara_bayar",
                                "orderable": false,
                                "render": (data, type, row, meta) => {
                                    let tmp = data;
                                    if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                                        tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                                    return tmp;
                                },
                            },
                            { 
                                "data": "dokter",
                                "orderable": false,
                            },
                            { 
                                "data": "diagnosa",
                                "orderable": false,
                            }
                        ],
                        "order": [ [0, "desc"] ],
                        "drawCallback": function (oSettings) {
                            tableKeperawatan.find('[data-popup=tooltip]').tooltip();
                        }
                    });
                } else tableHistoryKeperawatanDt.draw(false);

                setTimeout(function() { 
                    $(tabKeperawatan).unblock();
                }, 700);                
                break;
            case "riwayat_lab":
                blockElement(tabLab);
                $(tabLab).find('.div-data').html("").hide();
                $(tabLab).find('.div-no_data').hide();
                $.getJSON(URL.loadHistoryLab.replace(':PELAYANAN_ID', btoa(ID)), function(res, status) {
                    if(status === 'success') {
                        let data = res.data;
                        if(data.length > 0) {
                            $(tabLab).find('.div-data').append('<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accrodion-history_lab"></div>');
                            for (var i = 0; i < data.length; i++) {
                                var tanggal_sampling = moment(data[i].tanggal_sampling).format('DD-MM-YYYY');
                                var jam_sampling = moment(data[i].tanggal_sampling + ' ' + data[i].jam_sampling).format('HH:mm');
                                var catatan = data[i].catatan_dokter ? data[i].catatan_dokter : "&mdash;";
                                var rows = '<div class="panel panel-white">';
                                rows += ' <div class="panel-heading">';
                                rows += '   <h6 class="panel-title">';
                                rows += `     <a class="${(data.length == 1 ? "" : "collapsed")}" data-toggle="collapse" data-parent="#accrodion-history_lab" href="#history_lab_${( i + 1 )}" aria-expanded="${(data.length == 1 ? "true" : "false")}">`;
                                rows += `       <b>Tanggal</b> ${tanggal_sampling}&nbsp;&nbsp; <b>Jam</b> ${jam_sampling}`;
                                rows += '     </a>';
                                rows += '   </h6>';
                                rows += ' </div>';
                                rows += ` <div id="history_lab_${( i + 1 )}" class="panel-collapse collapse ${(data.length == 1 ? "in" : "")}" aria-expanded="false">`;
                                rows += '   <div class="table-responsive">';
                                rows += '     <table id="table-history_lab_' + ( i + 1 ) + '" class="table table-bordered table-striped">';
                                rows += '       <thead>';
                                rows += '         <tr class="bg-slate">';
                                rows += '           <th class="text-center">No.</th>';
                                rows += '           <th class="text-center">Pemeriksaan</th>';
                                rows += '           <th class="text-center">Hasil</th>';
                                rows += '           <th class="text-center">Satuan</th>';
                                rows += '           <th class="text-center">Nilai Normal</th>';
                                rows += '         </tr>';
                                rows += '       </thead>';
                                rows += '       <tbody>';
                                rows += '           <tr><td colspan="5">Tidak Ada Data</td><tr>';
                                rows += '       </tbody>';
                                rows += '      <table>';
                                rows += '     <div class="col-sm-12 mt-10">';
                                rows += '       <div class="col-md-8">';
                                rows += '         <label>Catatan Dokter</label>';
                                rows += `         <div class="form-control-static">${catatan}</div>`;
                                rows += '       </div>';
                                rows += '     </div>';
                                rows += '   </div>';
                                rows += ' </div>';
                                rows += '</div>';
                                $("#accrodion-history_lab").append(rows);

                                for (var j = 0; j < data[i].list.length; j++) {
                                    fillListHistoryLab(data[i].list[j], (j+1), `#table-history_lab_${(i + 1)}`);
                                }
                                $(tabLab).find('.div-data').show();
                            }
                        } else $(tabLab).find('.div-no_data').show();
                    }
                });

                setTimeout(function() { 
                    $(tabLab).unblock();
                }, 700);
                break;
            case "riwayat_rad":
                blockElement(tabRad);
                $(tabRad).find('.div-data').html("").hide();
                $(tabRad).find('.div-no_data').hide();
                $.getJSON(URL.loadHistoryRad.replace(':PELAYANAN_ID', btoa(ID)), function(res, status) {
                    if(status === 'success') {
                        let data = res.data;
                        if(data.length > 0) {
                            $(tabRad).find('.div-data').append('<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accordion-history_rad"></div>');
                            for (var i = 0; i < data.length; i++) {
                                var tanggal_sampling = moment(data[i].tanggal_sampling).format('DD-MM-YYYY');
                                var jam_sampling = moment(data[i].tanggal_sampling + ' ' + data[i].jam_sampling).format('HH:mm');
                                var catatan = data[i].catatan_dokter ? data[i].catatan_dokter : "&mdash;";
                                var rows = '<div class="panel panel-white">';
                                rows += ' <div class="panel-heading">';
                                rows += '   <h6 class="panel-title">';
                                rows += `     <a class="${(data.length == 1 ? "" : "collapsed")}" data-toggle="collapse" data-parent="#accordion-history_rad" href="#history_rad_${( i + 1 )}" aria-expanded="${(data.length == 1 ? "true" : "false")}">`;
                                rows += `       <b>Tanggal</b> ${tanggal_sampling}&nbsp;&nbsp; <b>Jam</b> ${jam_sampling}`;
                                rows += '     </a>';
                                rows += '   </h6>';
                                rows += ' </div>';
                                rows += ` <div id="history_rad_${( i + 1 )}" class="panel-collapse collapse ${(data.length == 1 ? "in" : "")}" aria-expanded="false">`;
                                rows += '   <div class="table-responsive">';
                                rows += '     <table id="table-history_rad_' + ( i + 1 ) + '" class="table table-bordered table-striped">';
                                rows += '       <thead>';
                                rows += '         <tr class="bg-slate">';
                                rows += '           <th class="text-center">No.</th>';
                                rows += '           <th class="text-center">Pemeriksaan</th>';
                                rows += '           <th class="text-center">No. Foto</th>';
                                rows += '           <th class="text-center">Ukuran</th>';
                                rows += '           <th class="text-center">Faktor Eksposisi</th>';
                                rows += '         </tr>';
                                rows += '       </thead>';
                                rows += '       <tbody>';
                                rows += '           <tr><td colspan="5">Tidak Ada Data</td><tr>';
                                rows += '       </tbody>';
                                rows += '      <table>';
                                rows += '     <div class="col-sm-12 mt-10">';
                                rows += '         <label>Catatan Dokter</label>';
                                rows += `         <div class="form-control-static">${catatan}</div>`;
                                rows += '     </div>';
                                rows += '     <div class="col-sm-12 mt-10">';
                                rows += '       <div class="panel panel-default">';
                                rows += '          <div class="panel-body">';
                                rows += `            <h2 class="text-slate-300 no-preview-image text-center">No Image Available</h2>`;
                                rows += `            <div id="content-foto_${(i+1)}"></div>`;
                                rows += '          </div>';
                                rows += '       </div>';
                                rows += '     </div>';
                                rows += '   </div>';
                                rows += ' </div>';
                                rows += '</div>';
                                $("#accordion-history_rad").append(rows);

                                for (var j = 0; j < data[i].list.length; j++) {
                                    fillListHistoryRad(data[i].list[j], (j+1), `#table-history_rad_${(i + 1)}`);
                                }

                                for (var j = 0; j < data[i].list_foto.length; j++) {
                                    contentFotoRad(data[i].list_foto[j], `#content-foto_${(i+1)}`);
                                }
                                $(tabRad).find('.div-data').show();
                            }
                        } else $(tabRad).find('.div-no_data').show();
                    }
                });
                setTimeout(function() { 
                    $(tabRad).unblock();
                }, 700);
                break;
            case "riwayat_obat":
                blockElement('#tab-riwayat_obat');
                $(tabObat).find('.div-data').hide();
                $(tabObat).find('.div-no_data').hide();
                $.getJSON(URL.loadHistoryObat.replace(':PELAYANAN_ID', btoa(ID)), function(res, status) {
                    if(status === 'success') {
                        let data = res.data;
                        if(data.length > 0) {
                            $(tabObat).find('.div-data').append('<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accordion-history_obat"></div>');
                            for (var i = 0; i < data.length; i++) {
                                var tanggal = moment(data[i].tanggal).format('DD-MM-YYYY');
                                var jam = moment(data[i].tanggal).format('HH:mm');
                                var nomor = data[i].nomor ? data[i].nomor : "&mdash;";
                                var rows = '<div class="panel panel-white">';
                                rows += ' <div class="panel-heading">';
                                rows += '   <h6 class="panel-title">';
                                rows += `     <a class="${(data.length == 1 ? "" : "collapsed")}" data-toggle="collapse" data-parent="#accordion-history_obat" href="#history_obat_${( i + 1 )}" aria-expanded="${(data.length == 1 ? "true" : "false")}">`;
                                rows += `       <b>Tanggal</b> ${tanggal}&nbsp;&nbsp; <b>Jam</b> ${jam}`;
                                rows += '     </a>';
                                rows += '   </h6>';
                                rows += ' </div>';
                                rows += ` <div id="history_obat_${( i + 1 )}" class="panel-collapse collapse ${(data.length == 1 ? "in" : "")}" aria-expanded="false">`;
                                rows += '   <div class="panel-body form-inline" style="padding-left: 30px;">';
                                rows += '     <div class="form-group mr-10">';
                                rows += '       <label>No. Resep</label>';
                                rows += `       <p class="form-control-static text-bold">${nomor}</p>`;
                                rows += '     </div>';
                                rows += '   </div>';
                                rows += '   <div class="table-responsive">';
                                rows += '     <table id="table-history_obat_' + ( i + 1 ) + '" class="table table-bordered table-striped">';
                                rows += '       <thead>';
                                rows += '         <tr class="bg-slate">';
                                rows += '           <th class="text-center">No.</th>';
                                rows += '           <th class="text-center">Obat</th>';
                                rows += '           <th class="text-center">Satuan</th>';
                                rows += '           <th class="text-center">Cara Buat</th>';
                                rows += '           <th class="text-center">Metode</th>';
                                rows += '           <th class="text-center">Qty</th>';
                                rows += '           <th class="text-center">Signa</th>';
                                rows += '           <th class="text-center">Peracik</th>';
                                rows += '         </tr>';
                                rows += '       </thead>';
                                rows += '       <tbody>';
                                rows += '           <tr><td colspan="8">Tidak Ada Data</td><tr>';
                                rows += '       </tbody>';
                                rows += '      <table>';
                                rows += '     </div>';
                                rows += '   </div>';
                                rows += ' </div>';
                                rows += '</div>';
                                $("#accordion-history_obat").append(rows);

                                for (var j = 0; j < data[i].list.length; j++) {
                                    fillListHistoryObat(data[i].list[j], (j+1), `#table-history_obat_${(i + 1)}`);
                                }
                                $(tabObat).find('.div-data').show();
                            }
                        } else $(tabObat).find('.div-no_data').show();
                    }
                });

                setTimeout(function() { 
                    $(tabObat).unblock();
                }, 700);
                break;
        }
    }

    tableKeperawatan.on("click", ".show-row", function () {
        let uid = $(this).data('uid');
        let url = URL.statusAssesment.replace(':UID', uid).replace(':BROWSE', BROWSE);
        blockPage('Form status assesment sedang diproses ...');
        setTimeout(function() {
            if(BROWSE === "ok") url = url + '&uid_ok=' + uidOk + '&id_ok=' + idOk; 
            window.location.assign(url);
        }, 700);
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
            case tabLab:
                fillTab("riwayat_lab");
                break;
            case tabRad:
                fillTab("riwayat_rad");
                break;
            case tabObat:
                fillTab("riwayat_obat");
                break;
            default:
                fillTab("riwayat_keperawatan");
            break;
        }
    });

    fillForm(UID);
});