$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                if(!data.data_pemeriksaan) data.data_pemeriksaan = {};

                $("#label-nama_pasien").html(data.pasien.nama);
                $("#label-no_rm").html(data.pasien.no_rm);
                $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#label-ttl").html(`${data.pasien.tempat_lahir}, ${data.pasien.tanggal_lahir}`);
                $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $("#label-alamat").html(data.pasien.alamat);
                $("#label-no_telepon").html(data.pasien.no_telepon_1);

                $("#label-no_register").html(data.no_register);
                $("#label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $("#label-cara_bayar").html(data.cara_bayar);
                $("#label-layanan").html(data.layanan);
                $("#label-dokter").html(data.dokter);

                tableAssesment.DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": data.assesment_list,
                    "columns": [
                        { "data": "lembar" },
                        { 
                            "data": "status",
                            "render": function (data, type, row, meta) {
                                let tmp = 'Kosong';
                                if(parseInt(data) !== 0) tmp = 'Terisi';
                                return tmp;
                            },
                        },
                        { 
                            "data": "ppa",
                            "render": function (data, type, row, meta) {
                                return data ? data : "&mdash;";
                            },
                        },
                        { 
                            "data": "tanggal_pengisian",
                            "render": function (data, type, row, meta) {
                                return data ? moment(data).format('DD-MM-YYYY HH:mm') : '&mdash;'
                            },
                        },
                        { 
                            "data": "open",
                            "render": function (data, type, row, meta) {
                                let tmp = '&mdash;';
                                let mode = 'add';
                                let url = URL.formPemeriksaanAwal;
                                if(row.lembar.search(/cppt/i) !== -1) { 
                                    url = URL.formCppt;
                                    tmp = `<a data-url="${url.replace(':UID', UID).replace(':MODE', mode)}" class="btn btn-primary btn-xs edit-row" data-popup="tooltip" title="Pilih">Pilih</a>`;
                                } else {
                                    if(BROWSE === "ok") {
                                        tmp = '&mdash;';
                                    } else {                                        
                                        if(parseInt(row.status) !== 0) {
                                            mode = 'edit';
                                            tmp = `<a data-url="${url.replace(':UID', UID).replace(':MODE', 'view')}" class="btn btn-info btn-xs view-row" data-popup="tooltip" title="View"><i class="fa fa-eye"></i></a>&nbsp;`;
                                        }
                                        tmp += `<a data-url="${url.replace(':UID', UID).replace(':MODE', mode)}" class="btn btn-primary btn-xs edit-row" data-popup="tooltip" title="Edit"><i class="fa fa-edit"></i></a>`;                                            
                                    }
                                }
                                
                                return tmp;
                            },
                            "className": "text-center"
                        },
                    ],
                    "drawCallback": function (settings) {
                        tableAssesment.find('[data-popup=tooltip]').tooltip();
                    }
                });
            }
        });
    }

    tableAssesment.on("click", ".view-row, .edit-row", function () {
        let url = $(this).data('url');
        blockPage('Form pemeriksaan sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(url);
        }, 400);
    });

    fillForm(UID);
});