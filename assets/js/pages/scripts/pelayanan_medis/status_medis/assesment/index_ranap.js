/*
*    modalFormKeperawatan, modalInputFormKeperawatan, tableFormKeperawatanHistory => didapat dari file modal 'pelayanan_medis/status_medis/assesment/modal/form-keperawatan.php'
*/

$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;

                $('.div-pilih_form_keperawatan').show();
                if(parseInt(data.close) === 1) $('.div-pilih_form_keperawatan').hide();

                let labelBed = data.bed;
                labelBed += data.titip == 1 ? `<br/><span class="text-size-mini text-info">(${data.titip_kelas})</span>` : "";
                if(!data.data_pemeriksaan) data.data_pemeriksaan = {};

                $("#label-nama_pasien").html(data.pasien.nama);
                $("#label-no_rm").html(data.pasien.no_rm);
                $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#label-ttl").html(`${data.pasien.tempat_lahir}, ${data.pasien.tanggal_lahir}`);
                $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $("#label-alamat").html(data.pasien.alamat);
                $("#label-no_telepon").html(data.pasien.no_telepon_1);

                $("#label-no_register").html(data.no_register);
                $("#label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $("#label-cara_bayar").html(data.cara_bayar);
                $("#label-layanan").html(data.layanan);
                $("#label-dokter").html(data.dokter);
                $("#label-ruang").html(data.ruang);
                $("#label-kelas").html(data.kelas);
                $("#label-bed").html(labelBed);

                $('.section-rawat_inap').show();

                tableAssesment.DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": data.assesment_list,
                    "columns": [
                        { "data": "lembar" },
                        { 
                            "data": "status",
                            "render": function (data, type, row, meta) {
                                let tmp = 'Kosong';
                                if(parseInt(data) !== 0) tmp = 'Terisi';
                                return tmp;
                            },
                        },
                        { 
                            "data": "ppa",
                            "render": function (data, type, row, meta) {
                                return data ? data : "&mdash;";
                            },
                        },
                        { 
                            "data": "tanggal_pengisian",
                            "render": function (data, type, row, meta) {
                                return data ? moment(data).format('DD-MM-YYYY HH:mm') : '&mdash;'
                            },
                        },
                        { 
                            "data": "uid",
                            "render": function (data, type, row, meta) {
                                let btnClass = 'pemeriksaan-row';
                                if(row.lembar.search(/cppt/i) !== -1) btnClass = 'cppt-row';

                                let tmp = `<a class="btn btn-primary btn-xs ${btnClass}" data-popup="tooltip" title="Pilih">Pilih</a>`;
                                if(BROWSE === "ok") {
                                    if(row.lembar.search(/keperawatan/i) !== -1) tmp = '&mdash;';
                                }
                                return tmp;
                            },
                            "className": "text-center"
                        },
                    ],
                    "drawCallback": function (settings) {
                        tableAssesment.find('[data-popup=tooltip]').tooltip();
                    }
                });

                $("[name=pelayanan_id]").val(data.id);
                $("[name=pelayanan_uid]").val(data.uid);
            }
        });
    }

    tableAssesment.on("click", ".pemeriksaan-row", function () {
        $(modalFormKeperawatan).modal('show');
    });

    tableAssesment.on("click", ".cppt-row", function () {
        blockPage('Form CPPT sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.formCppt.replace(':UID', UID).replace(':MODE', 'view'));
        }, 300);
    });

    fillForm(UID);
});