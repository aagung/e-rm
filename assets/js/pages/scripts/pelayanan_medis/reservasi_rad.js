$(() => {
    let dataPemeriksaan = [], TREE, FT; // Fancy Tree

    let EL = {
        btnCari: '.modal-btn_cari',
        submit: '#btn-save_reservasi_rad',
    }

    let fillForm = (uid) => {
        uid = uid ? uid : "0";
        if(uid === "0") {
            $(modalReservasiRad).find(`input[name=id]`).val(0);
            $(modalReservasiRad).find(`input[name=uid]`).val(uid);
            $(modalReservasiRad).find(`input[name=tanggal]`).val(moment().format('YYYY-MM-DD HH:mm:ss'));
            fetchPemeriksaan([]);
            return;
        }

        $.getJSON(`${base_url}/api/radiologi/front_desk/get_data/${uid}`, function (res, status) {
            if (status === 'success') {
                let statusTitle = "";
                let data = res.data;
                $(modalReservasiRad).find(`input[name=id]`).val(data.id);
                $(modalReservasiRad).find(`input[name=uid]`).val(data.uid);
                $(modalReservasiRad).find(`input[name=tanggal]`).val(moment(data.tanggal).format('YYYY-MM-DD HH:mm:ss'));

                fetchPemeriksaan(data.details);

                $(EL.submit).show();
                $(modalReservasiRad).find('input, textarea, select').prop('disabled', false);  
                if(data.status != 1) {
                   $(EL.submit).hide();
                   $(modalReservasiRad).find('input, textarea, select').prop('disabled', true);  

                   switch(parseInt(data.status)) {
                       case 2:
                           statusTitle = ` <span class="label label-success text-center">Diproses</span>`;
                           break;
                       case 3:
                           statusTitle = ` <span class="label label-warning text-center">Dibatalkan</span>`;
                           break;
                   }
                } 
                $(modalReservasiRad).find('.modal-title').html('Reservasi Radiologi' + statusTitle);
            }
        });
    }

    /*let fillSelect = (obj, element) => {
        let parent = element.parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            if (status === 'success') {
                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < data.list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${data.list[i].id}" ${selected}>${data.list[i].nama}</option>`;
                }
                element.html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }*/

    let fetchPemeriksaan = (selected) => {
        blockElement(modalReservasiRad + ' .tree-container');
        $.getJSON(`${base_url}/api/master/radiologi/pemeriksaan/fetch_tarif`, (res, status) => {
            if (status === 'success') {
                let data = res.data;
                data = data || [];
                selected = selected || [];

                FT = null;

                if (TREE) TREE.remove();
                TREE = $("<div/>")
                            .addClass('tree-rad_pemeriksaan tree-checkbox-hierarchical well');
                $(modalReservasiRad).find('.tree-container').html(TREE);

                TREE.fancytree({
                    extensions: ["filter"],
                    quicksearch: true,
                    filter: {
                        autoApply: true,   // Re-apply last filter if lazy data is loaded
                        autoExpand: true, // Expand all branches that contain matches while filtered
                        counter: false,     // Show a badge with number of matching child nodes near parent icons
                        fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                        hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                        hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                        highlight: true,   // Highlight matches by wrapping inside <mark> tags
                        leavesOnly: false, // Match end nodes only
                        nodata: true,      // Display a 'no data' status node if result is empty
                        mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                    },
                    source: data,
                    checkbox: true,
                    selectMode: 3,
                });

                FT = TREE.fancytree("getTree");

                setTimeout(() => {
                    var rootNode = TREE.fancytree("getRootNode");
                    rootNode.sortChildren(function(a, b) {
                        var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                        y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                        return x === y ? 0 : x > y ? 1 : -1;
                    }, true);

                    // Hide Unselected Node
                    // Untuk menghilangkan parent yang tidak dipilih sama sekali
                    // FT.visit(function (node) {
                    //     node.setExpanded(true);
                    //     if (!node.partsel && !node.selected) {
                    //         $(node.li).addClass('hide');
                    //     }
                    // });

                    // untuk menghilangkan node dari parent yang dipilih (Partial|semua)
                    // FT.visit(function (node) {
                    //     if (!node.partsel && !node.selected) {
                    //         $(node.li).addClass('hide');
                    //     }
                    // });

                    $(modalReservasiRad + ' .tree-container').unblock();
                }, 500);

                // SET SELECTED
                let node;
                for (let sel_id of selected) {
                    dataPemeriksaan[sel_id.pemeriksaan_uid] = sel_id;
                    node = FT.getNodeByKey(sel_id.pemeriksaan_id);
                    if (! node) node = FT.getNodeByKey(sel_id.pemeriksaan_id + '');

                    if (node) {
                        node.setSelected(true);
                        node.setActive(true);
                        node.setExpanded(true);
                    }
                }
            }
        });
        setTimeout(() => {
            $(modalReservasiRad).modal('show');
        }, 300);
    }

    $(btnReservasiRad).click(function() {
        let uid = $(this).data('uid') === undefined ? "" : $(this).data('uid');
        fillForm(uid);
    });

    $(modalReservasiRad).on('click', EL.btnCari, function(e) {
        e.preventDefault();

        if (FT) {
            let query = $(modalReservasiRad).find('[name="nama_pemeriksaan"]').val();
            var filterFunc = FT.filterNodes;
            var opts = { autoExpand: true, highlight: true };

            // if (query != "") 
                var n = filterFunc.call(FT, query, opts);
        }
    });

    $(formReservasiRad).validate({
        rules: {},
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            let selectedNodes = FT.getSelectedNodes();
            if (selectedNodes.length <= 0) {
                warningMessage('Peringatan !', 'Pemeriksaan belum terpilih.');
                return;
            }

            let formData = new FormData($(form)[0]);

            // Data Pemeriksaan
            let tmpPemeriksaan = [];
            for (let node of selectedNodes) {
                if (! node.isFolder()) {
                    let isExists = node.data.uid in dataPemeriksaan ? true : false;
                    if(!isExists) {
                        tmpPemeriksaan[node.data.uid] = {
                            id: 0,
                            rujukan_id: 0,
                            pemeriksaan_id: node.data.id,
                            pemeriksaan_uid: node.data.uid,
                            tarif_pelayanan_id: node.data.tarif_pelayanan_id,
                            harga: node.data.harga,
                            discount_jenis: 'nominal',
                            discount: 0,
                            quantity: 1,
                        };
                    } else tmpPemeriksaan[node.data.uid] = dataPemeriksaan[node.data.uid];
                }
            }
            dataPemeriksaan = tmpPemeriksaan;

            for(let key in dataPemeriksaan) {
                formData.append('detail_id[]', dataPemeriksaan[key].id);
                formData.append('detail_rujukan_id[]', dataPemeriksaan[key].rujukan_id);
                formData.append('detail_pemeriksaan_id[]', dataPemeriksaan[key].pemeriksaan_id);
                formData.append('detail_tarif_pelayanan_id[]', dataPemeriksaan[key].tarif_pelayanan_id);
                formData.append('detail_harga[]', dataPemeriksaan[key].harga);
                formData.append('detail_discount_jenis[]', dataPemeriksaan[key].discount_jenis);
                formData.append('detail_discount[]', dataPemeriksaan[key].discount);
                formData.append('detail_quantity[]', dataPemeriksaan[key].quantity);
                formData.append('detail_action[]', dataPemeriksaan[key].action);
            }

            blockElement(modalReservasiRad + ' .modal-dialog');
            $.ajax({
                url: `${base_url}/api/radiologi/front_desk/save`,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    RUJUKAN.rad_uid = result.data.uid;
                    successMessage('Success', "Reservasi Radiologi berhasil disimpan.");
                    setTimeout(() => {
                        $(modalReservasiRad + ' .modal-dialog').unblock();
                        $(modalReservasiRad).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalReservasiRad + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });
});