$(() => {
    let enableLoad = false;            
    let EL = {
        tanggal: '#modal-input_kunjungan_ulang_tanggal',
        layanan_id: '#modal-input_kunjungan_ulang_layanan_id',
        dokter_id: '#modal-input_kunjungan_ulang_dokter_id',
        submit: '#btn-save_kunjungan_ulang',
    }

    $(EL.tanggal).pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        min: moment(moment(), "dd/mm/yyyy").add(1, 'days').format('dd/mm/yyyy')
    });

    let setValidateForm = (obj, mode) => {
        $(modalKunjunganUlang).find(obj.element).rules("remove");
        if(mode === "add") $(modalKunjunganUlang).find(obj.element).rules('add', obj.rules);
    }

    let fillSelect = (obj, element) => {
        let parent = element.parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            if (status === 'success') {
                let list = data.data; 

                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${list[i].id}" ${selected}>${list[i].nama}</option>`;
                }
                element.html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    let fillForm = (uid) => {
        enableLoad = false;
        $.getJSON(`${base_url}/api/pelayanan_medis/rujukan/rujukan_kunjungan_ulang/get_data/${uid}`, function (res, status) {
            if (status === 'success') {
                let statusTitle = "";
                let data = res.data;

                if($(modalKunjunganUlang).find(`input[name=asal_pasien]`).val() == imediscode.UNIT_LAYANAN_RAWAT_INAP) {
                    $(modalKunjunganUlang).find('.div-rawat_inap').show();    

                    setValidateForm({
                        element: EL.layanan_id,
                        rules: { required: true }   
                    }, "add");

                    setValidateForm({
                        element: EL.dokter_id,
                        rules: { required: true }   
                    }, "add");

                    fillSelect({
                        url: `${base_url}/api/pelayanan_medis/rawat_jalan/rawat_jalan/fetch_layanan`,
                        value: data.layanan_id,
                    }, $(EL.layanan_id));

                    fillSelect({
                        url: `${base_url}/api/master/dokter/fetch_all?q=${btoa(parseInt(data.layanan_id))}`,
                        value: data.dokter_id,
                    }, $(EL.dokter_id));
                }

                $(modalKunjunganUlang).find(`input[name=uid]`).val(uid);
                $(modalKunjunganUlang).find(`input[name=booking_id]`).val(data.booking_id);
                $(EL.tanggal).val(moment(data.tanggal).format('DD/MM/YYYY'));
                
                $(EL.submit).show();
                $(modalKunjunganUlang).find('input, textarea, select').prop('disabled', false);  
                if(data.status != 0) {
                   $(EL.submit).hide();
                   $(modalKunjunganUlang).find('input, textarea, select').prop('disabled', true);  

                   switch(parseInt(data.status)) {
                       case 1:
                           statusTitle = ` <span class="label label-success text-center">Diproses</span>`;
                           break;
                       case 2:
                           statusTitle = ` <span class="label label-warning text-center">Dibatalkan</span>`;
                           break;
                   }
                } 
                $(modalKunjunganUlang).find('.modal-title').html('Reservasi Kunjungan Ulang' + statusTitle);

                setTimeout(() => {
                    enableLoad = true;
                    $(modalKunjunganUlang).modal('show');
                }, 300);
            }
        });
    }

    $(EL.layanan_id).change(function() {
        let layanan_id = parseInt($(EL.layanan_id).val());
        if(enableLoad) {
            let dokter_id = $(EL.dokter_id).val() ? $(EL.dokter_id).val() : '';
            fillSelect({
                url: `${base_url}/api/master/dokter/fetch_all?q=${btoa(layanan_id)}`,
                value: dokter_id,
            }, $(EL.dokter_id));
        }
        $(modalKunjunganUlang).find(`input[name=layanan_id]`).val(layanan_id);
    });

    $(EL.dokter_id).change(function() {
        let dokter_id = parseInt($(EL.dokter_id).val());
        $(modalKunjunganUlang).find(`input[name=dokter_id]`).val(dokter_id);
    });

    $(btnKunjunganUlang).click(function() {
        let uid = $(this).data('uid') === undefined ? "" : $(this).data('uid');
        fillForm(uid);
    });

    $(formKunjunganUlang).validate({
        rules: {
            tanggal: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalKunjunganUlang + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: `${base_url}/api/pelayanan_medis/rujukan/rujukan_kunjungan_ulang/save`,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    RUJUKAN.kunlang_uid = result;
                    successMessage('Success', "Kunjungan Ulang berhasil disimpan.");
                    setTimeout(() => {
                        $(modalKunjunganUlang + ' .modal-dialog').unblock();
                        $(modalKunjunganUlang).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalKunjunganUlang + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });
});