$(() => {
    let EL = {
        tanggal: '#modal-input_reservasi_ok_tanggal',
        jam_mulai: '#modal-input_reservasi_ok_jam_mulai',
        jam_selesai: '#modal-input_reservasi_ok_jam_selesai',
        ruang_id: '#modal-input_reservasi_ok_ruang_id',
        diagnosa_utama: '#modal-input_reservasi_ok_diagnosa_utama',
        operasi_id: '#modal-input_reservasi_ok_operasi_id',
        spesialisasi_operasi_id: '#modal-input_reservasi_ok_spesialisasi_operasi_id',
        golongan_operasi_id: '#modal-input_reservasi_ok_golongan_operasi_id',
        tindakan_operasi_id: '#modal-input_reservasi_ok_tindakan_operasi_id',
        submit: '#btn-save_reservasi_ok',
    }

    $(".styled").uniform({
        radioClass: 'choice'
    });

    $('.reservasi-date').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        min: moment()
    });

    $('.reservasi-time').pickatime({
        format: 'HH:i',
    });

    let fillForm = (uid) => {
        initializeDiagnosa($(EL.diagnosa_utama));
        $.getJSON(`${base_url}/api/pelayanan_medis/rujukan/rujukan_ok/get_data/${uid}`, function (res, status) {
            if (status === 'success') {
                let data = res.data;
                $(modalReservasiOk).find(`input[name=uid]`).val(uid);
                $(EL.tanggal).val(moment(data.tanggal).format('DD/MM/YYYY'));
                $(EL.jam_mulai).val(data.jam_mulai);
                $(EL.jam_selesai).val(data.jam_selesai);
                $(EL.golongan_operasi_id).val(data.golongan_operasi_id).change();
                $(`input[name=klasifikasi][value="${data.klasifikasi}"]`).click();

                fillSelect({
                    url: `${base_url}/api/master/layanan/get_bedah`,
                    value: data.operasi_id,
                }, $(EL.operasi_id));

                fillSelect({
                    url: `${base_url}/api/master/spesialisasi/get_all`,
                    value: data.spesialisasi_operasi_id,
                }, $(EL.spesialisasi_operasi_id));

                fillSelect({
                    url: `${base_url}/api/master/ruang/get_by_jenis?jenis=ok`,
                    value: data.ruang_id,
                }, $(EL.ruang_id));
                
                if(data.diagnosa_utama) {
                    let optDiagnosaUtama = new Option(data.diagnosa_utama.code, data.diagnosa_utama.id, true, true);
                    $(EL.diagnosa_utama).append(optDiagnosaUtama).trigger('change');

                    $(EL.diagnosa_utama).trigger({
                        type: 'select2:select',
                        params: {
                            data: {
                                    id: data.diagnosa_utama.id,
                                    code: data.diagnosa_utama.title,
                                    text: data.diagnosa_utama.code,
                                    description: data.diagnosa_utama.description,
                                }
                        }
                    });
                }

                $(EL.submit).show();
                $(modalReservasiOk).find('.modal-title').html('Reservasi OK');
                $(modalReservasiOk).find('input, textarea, select').prop('disabled', false);  
                if(data.status == 1 || data.batal == 1) {
                    if(data.batal == 1) 
                        $(modalReservasiOk).find('.modal-title').html('Reservasi OK <span class="label label-warning text-center">Batal</span>');

                   $(EL.submit).hide();
                   $(modalReservasiOk).find('input, textarea, select').prop('disabled', true);  
                } 

                $.uniform.update();
                setTimeout(() => {
                    $(modalReservasiOk).modal('show');
                }, 300);

            }
        });
    }

    // DIAGNOSA
    let initializeDiagnosa = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.code}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.description}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.code || data.text || '- Pilih -';
        }

        el.select2({
            placeholder: "- Pilih -",
            ajax: {
                url: `${base_url}/api/master/icd_10/search`,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.id, description: obj.description, code: obj.code };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        el.on('select2:select', function (e) {
            var data = e.params.data;
            if(typeof data !== "undefined") {
                //
            }
        });
    }

    let fillSelect = (obj, element) => {
        let parent = element.parent();
        parent.find('.loading-select').show();
        $.getJSON(obj.url, function(data, status) {
            if (status === 'success') {
                var option = '';
                option += '<option value="" selected="selected">- Pilih -</option>';
                for (var i = 0; i < data.list.length; i++) {
                    let selected = ""
                    if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
                    option += `<option value="${data.list[i].id}" ${selected}>${data.list[i].nama}</option>`;
                }
                element.html(option).trigger("change");
            }
            parent.find('.loading-select').hide();
        });
    }

    $(btnReservasiOk).click(function() {
        let uid = $(this).data('uid') === undefined ? "" : $(this).data('uid');
        fillForm(uid);
    });

    $(formReservasiOk).validate({
        rules: {
            ruang_id: { required: true },
            tanggal: { required: true },
            jam_mulai: { required: true },
            jam_selesai: { required: true },
            golongan_operasi_id: { required: true },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalReservasiOk + ' .modal-dialog');

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: `${base_url}/api/pelayanan_medis/rujukan/rujukan_ok/save`,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    RUJUKAN.ok_uid = result;
                    successMessage('Success', "Reservasi OK berhasil disimpan.");
                    setTimeout(() => {
                        $(modalReservasiOk + ' .modal-dialog').unblock();
                        $(modalReservasiOk).modal('hide');
                    }, 300);
                },
                error: function () {
                    $(modalReservasiOk + ' .modal-dialog').unblock();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });
});