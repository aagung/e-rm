$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    $("#metode_numeric_rating_scale").ionRangeSlider({
        type: "single",
        min: 0,
        max: 10,
        step: 1,
        grid: true,
        grid_snap: true,
        onChange: function(data) {
            console.log(data);
        }
    });

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getData.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();

                let data = res.data;
                if(data.data_pemeriksaan) {
                    let pemeriksaan = data.data_pemeriksaan;

                    let label = Object.keys(pemeriksaan);
                    for (var i = 0; i < label.length; i++) {
                        switch(label[i]) {
                            case "metode_numeric_rating_scale":
                                let inputInstance = $(`#${label[i]}`).data("ionRangeSlider");
                                inputInstance.update({
                                    from: pemeriksaan[label[i]]
                                });
                                break;
                            default:
                                if($(`input[name="${label[i]}"]`).is(':text') || $(`[name="${label[i]}"]`).is('textarea') || $(`[name="${label[i]}"]`).is('select')) {
                                    if(pemeriksaan[label[i]]) $(`[name="${label[i]}"]`).val(pemeriksaan[label[i]]).change();
                                } else {   
                                    if(pemeriksaan[label[i]]) $(`input[name="${label[i]}"][value="${pemeriksaan[label[i]]}"]`).click();
                                }
                                break;
                        }
                    }
                    $.uniform.update();
                }

                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#pelayanan_id").val(data.pelayanan_id);
                $("#pelayanan_uid").val(data.pelayanan_uid);
                if(data.cito) $(`input[name="cito"][value="${data.cito}"]`).click();

                FORM.find(".label-nama_pasien").html(data.pasien.nama);
                FORM.find(".label-no_rm").html(data.pasien.no_rm);
                FORM.find(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                FORM.find(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                FORM.find(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                FORM.find(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                FORM.find(".label-alamat").html(data.pasien.alamat);
                FORM.find(".label-no_telepon").html(data.pasien.no_telepon_1);

                FORM.find(".label-no_register").html(data.pelayanan.no_register);
                FORM.find(".label-tanggal").html(moment(data.pelayanan.tanggal).format('DD-MM-YYYY HH:mm'));
                FORM.find(".label-cara_bayar").html(data.pelayanan.cara_bayar);
                FORM.find(".label-layanan").html(data.pelayanan.layanan);
                FORM.find(".label-dokter").html(data.pelayanan.dokter);

                FORM.find(".label-no_register").html(data.pelayanan.no_register);
                FORM.find(".label-layanan").html(data.pelayanan.layanan);
                FORM.find(".label-dokter").html(data.pelayanan.dokter);
                FORM.find(".label-cara_bayar").html(data.pelayanan.cara_bayar);
                FORM.find(".label-perusahaan").html(data.pelayanan.perusahaan);
                FORM.find(".label-no_jaminan").html(data.pelayanan.no_jaminan);
                FORM.find(".label-penjamin_perusahaan").html(data.pelayanan.penjamin_perusahaan);
                FORM.find(".label-penjamin_no_jaminan").html(data.pelayanan.penjamin_no_jaminan);

                switch (parseInt(data.pelayanan.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.pelayanan.penjamin_id) {
                            FORM.find(".div-label_penjamin_perusahaan").show();
                            FORM.find(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.pelayanan.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            FORM.find(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            FORM.find(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        FORM.find(".div-label_perusahaan").show();
                        FORM.find(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.pelayanan.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        FORM.find(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        FORM.find(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        FORM.find(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $.unblockUI();
            }
        });

        if(MODE === "view") {
            $("input[type=text], textarea, select").prop("disabled", true);
            $(".btn-save").hide();
        }
    }

    let initializeForm = () => {
        $('input[name="pernah_dirawat"]').click(function() {
            $(".div-detail_pernah_dirawat").hide();
            $("#kapan").val('');
            $("#dimana").val('');
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-detail_pernah_dirawat").show('slow');
        });

        $('input[name="alergi"]').click(function() {
            $(".div-alergi_ke").hide();
            $(".div-alergi_ke_lainnya").hide();
            $('input:radio[name=alergi_ke]').prop('checked', false);
            $.uniform.update();

            let val = $(this).val();
            if(val === "ya") $(".div-alergi_ke").show('slow');
        });

        $('input[name="alergi_ke"]').click(function() {
            $("#alergi_ke_lainnya").val('');
            $(".div-alergi_ke_lainnya").hide();

            let val = $(this).val();
            if(val === "lainnya") $(".div-alergi_ke_lainnya").show('slow');
        });

        $('input[name="status_ekonomi"]').click(function() {
            $("#status_ekonomi_lainnya").val('');
            $(".div-status_ekonomi_lainnya").hide();

            let val = $(this).val();
            if(val === "lainnya") $(".div-status_ekonomi_lainnya").show('slow');
        });

        $('.btn-kembali').click(function() {
            window.location.assign(URL.index);
        });

        FORM.validate({
            rules: {
                alasan_masuk_rs: { required: true },
                //diagnosis: { required: true },
                td: { required: true },
                frek_nadi: { required: true },
                frek_nafas: { required: true },
                suhu: { required: true },
                tinggi_badan: { required: true },
                berat_badan: { required: true },
            },
            messages: {},
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (result) {
                        $.unblockUI();
                        successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 2000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        fillForm(UID);
    }

    initializeForm();
});