$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    $('.input-tgl_lahir').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        max: moment().format('dd/mm/yyyy')
    });

    let historyKunjungan = (pasien_uid) => {
        if(pasien_uid === "") {
            $('#div-history_kunjungan').html('');
            return;
        }
        
        let ucwords = (label) => {
            return label.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        }

        $.getJSON(URL.getHistoryKunjungan.replace(':PASIEN_UID', pasien_uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                let tmp = '';
                if(data.length > 0) {
                    data = data[0];
                    tmp += `<p class="no-margin text-info"><b>Kunjungan Terakhir</b></p>` +
                            `<p class="no-margin"><b>Tanggal:</b> ${moment(data.tanggal).format('DD/MM/YYYY')}</p>` +
                            `<p class="no-margin"><b>Layanan:</b> ${ucwords(data.layanan)}</p>` +
                            `<p class="no-margin"><b>Dokter:</b> ${ucwords(data.dokter)}</p>`;
                }
                $('#div-history_kunjungan').html(tmp);
            }
        });
    }

    let initializeFormSearch = () => {
        BTN_SEARCH.click(function () {
            let data = {
                no_rekam_medis: FORM_SEARCH_FIELD.no_rekam_medis.val(),
                nama: FORM_SEARCH_FIELD.nama.val(),
                telepon: FORM_SEARCH_FIELD.telepon.val(),
                tgl_lahir: FORM_SEARCH_FIELD.tgl_lahir.val(),
            };

            // Set Modal Search Value
            FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val(FORM_SEARCH_FIELD.no_rekam_medis.val());
            FORM_SEARCH_MODAL_FIELD.nama.val(FORM_SEARCH_FIELD.nama.val());
            FORM_SEARCH_MODAL_FIELD.telepon.val(FORM_SEARCH_FIELD.telepon.val());
            
            let tgl_lahir = FORM_SEARCH_FIELD.tgl_lahir.pickadate('picker').get();
            if (tgl_lahir) FORM_SEARCH_MODAL_FIELD.tgl_lahir.pickadate('picker').set('select', tgl_lahir, {format: 'dd/mm/yyyy'});

            searchPasien(data);
        });

        BTN_SEARCH_MODAL.click(function () {
            let data = {
                no_rekam_medis: FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val(),
                nama: FORM_SEARCH_MODAL_FIELD.nama.val(),
                telepon: FORM_SEARCH_MODAL_FIELD.telepon.val(),
                tgl_lahir: FORM_SEARCH_MODAL_FIELD.tgl_lahir.val(),
            };

            // Set Modal Search Value
            FORM_SEARCH_FIELD.no_rekam_medis.val(FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val());
            FORM_SEARCH_FIELD.nama.val(FORM_SEARCH_MODAL_FIELD.nama.val());
            FORM_SEARCH_FIELD.telepon.val(FORM_SEARCH_MODAL_FIELD.telepon.val());
            
            let tgl_lahir = FORM_SEARCH_MODAL_FIELD.tgl_lahir.pickadate('picker').get();
            if (tgl_lahir) FORM_SEARCH_FIELD.tgl_lahir.pickadate('picker').set('select', tgl_lahir, {format: 'dd/mm/yyyy'});

            searchPasien(data);
        });
    }

    let initializeSearchResult = () => {
        let columnsDef = [
            {
                "orderable": true,
                "data": "no_rm",
                "render": (data, type, row, meta) => {
                    return `<a class="select-pasien">${data}</a>`;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "nama",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "alamat",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "no_telepon_1",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            {
                "orderable": true,
                "data": "tanggal_lahir",
                "render": (data, type, row, meta) => {
                    return moment(data).format('DD/MM/YYYY');
                },
                "className": "text-center"
            }
        ];

        TABLE_SEARCH_RESULT_DT = TABLE_SEARCH_RESULT.DataTable({
            data: [],
            columns: columnsDef,
            "fnDrawCallback": function (oSettings) {
                //
            }
        });

        TABLE_SEARCH_RESULT.on('click', '.select-pasien', function() {
            let trParent = $(this).parents('tr');
            let data = TABLE_SEARCH_RESULT_DT.row(trParent).data();

            FORM.find('#status_pasien_id').val(1).change();
            FORM.find('#title_id').val(data.title_id).change();
            FORM.find('#disp-no_rm').html(data.no_rm);
            FORM.find('#nama_pasien').val(data.nama);
            FORM.find('#jenis_kelamin').val(data.jenis_kelamin).change();
            FORM.find('#tgl_lahir').pickadate('picker').set('select', moment(data.tanggal_lahir).format('DD/MM/YYYY'), {format: 'dd/mm/yyyy'});
            FORM.find('#no_telepon').val(data.no_telepon_1);
            FORM.find('#pasien_id').val(data.id);
            FORM.find('#no_rm').val(data.no_rm);

            historyKunjungan(data.uid);
            MODAL_SEARCH_RESULT.modal('hide');
        });
    }

    let searchPasien = (data) => {
        blockElement(MODAL_SEARCH_RESULT.find('.modal-dialog').selector);
        let modalShown = MODAL_SEARCH_RESULT.hasClass('in');
        if (! modalShown) {
            MODAL_SEARCH_RESULT.modal('show');
        }
        $.getJSON(URL.searchPasien.replace(':NO_RM', data.no_rekam_medis).replace(':NAMA', data.nama).replace(':TELEPON', data.telepon).replace(':TGL_LAHIR', data.tgl_lahir), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                TABLE_SEARCH_RESULT_DT.clear();
                TABLE_SEARCH_RESULT_DT.rows.add(data).draw();

                MODAL_SEARCH_RESULT.find('.modal-dialog').unblock();
            }
        });
    }

    let fetchDataSelect = (id, url, element) => {
        id = id || "";
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let options = '';
                let datas = res.data;

                $(element).empty();
                $(element).append('<option value="" selected="selected">- Pilih -</option/>');
                for (let data of datas) {
                    let value = `<option value="${data.id}">${data.nama}</option>`;                    
                    switch(element) {
                        case "#title_id":
                            value = `<option value="${data.id}" data-jenis_kelamin="${data.jenis_kelamin}">${data.singkatan}</option>`;
                            break;
                        case "#cara_bayar_id":
                            value = `<option value="${data.id}" data-jenis="${data.jenis}">${data.nama}</option>`;
                            break;
                    }
                    options += value;
                }
                $(element).append(options);
                $(element).val(id).trigger('change');
            }
        });
    }

    let setValidateForm = (obj, mode) => {
        $(obj.element).rules("remove");
        if(mode === "add") $(obj.element).rules('add', obj.rules);
    }

    let fillForm = (uid) => {
        if(uid === "") {
            $("#tanggal").pickadate({
                format: 'dd/mm/yyyy',
                selectMonths: true,
                selectYears: 100,
                min: moment().format('dd/mm/yyyy')
            });

            fetchDataSelect('', URL.fetchTitle, '#title_id');
            fetchDataSelect('', URL.fetchCaraBayar, '#cara_bayar_id');
            fetchDataSelect('', URL.fetchLayanan, '#layanan_id');
            historyKunjungan('');
            return;
        }

        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                $("#tanggal").pickadate({
                    format: 'dd/mm/yyyy',
                    selectMonths: true,
                    selectYears: 100,
                    min: data.tanggal
                });

                FORM.find('#status_pasien_id').val(data.status_pasien_id).change().prop('disabled', true);
                FORM.find('#disp-no_rm').html(data.no_rm ? data.no_rm : "&mdash;");
                FORM.find('#nama_pasien').val(data.nama_pasien);
                FORM.find(`input:radio[name=jenis_kelamin][value=${data.jenis_kelamin}]`).click();
                FORM.find('#tgl_lahir').pickadate('picker').set('select', data.tanggal_lahir, {format: 'dd/mm/yyyy'});
                FORM.find('#no_telepon').val(data.no_telepon);
                FORM.find('#pasien_id').val(data.pasien_id);
                FORM.find('#no_rm').val(data.no_rm);
                FORM.find('#id').val(data.id);
                FORM.find('#uid').val(data.uid);
                FORM.find('#tmp_perusahaan_id').val(data.perusahaan_id);
                FORM.find('#tmp_dokter_id').val(data.dokter_id);

                FORM.find('#disp-no_register').html(data.no_register);
                FORM.find('#tanggal').pickadate('picker').set('select', data.tanggal, {format: 'dd/mm/yyyy'});
                fetchDataSelect(data.title_id, URL.fetchTitle, '#title_id');
                fetchDataSelect(data.cara_bayar_id, URL.fetchCaraBayar, '#cara_bayar_id');
                fetchDataSelect(data.layanan_id, URL.fetchLayanan, '#layanan_id');
                
                $.uniform.update();
                $('.section-search').hide('slow');
                if(parseInt(data.status) === -1) {
                    BTN_SAVE.hide();
                    BTN_CANCEL.text('Kembali');
                }
            }
        }); 
    }

    let initializeForm = () => {
        FORM.find('#status_pasien_id').change(function() {
            let val = parseInt($(this).val());
            if(val === 2) {
                FORM.find('#disp-no_rm').html('&mdash;');
                FORM.find('#nama_pasien').val('');
                FORM.find('#jenis_kelamin').val('').change();
                FORM.find('#tgl_lahir').pickadate('picker').set('select', '', {format: 'dd/mm/yyyy'});
                FORM.find('#tgl_lahir').val('');
                FORM.find('#no_telepon').val('');
                FORM.find('#pasien_id').val('');
                FORM.find('#no_rm').val('');
            }
        });

        FORM.find('#title_id').change(function() {
            let jenisKelamin = $(this).find('option:selected').data('jenis_kelamin');
            FORM.find(`input:radio[name=jenis_kelamin]`).prop('checked', false).parent().removeClass('checked');
            FORM.find(`input:radio[name=jenis_kelamin][value=${jenisKelamin}]`).click();
            $.uniform.update();
        });

        FORM.find('#cara_bayar_id').change(function() {
            let val = parseInt($(this).val());
            let jenis = $(this).find('option:selected').data('jenis');
            let optionText = $(this).find('option:selected').text();
            if(optionText.search(/asuransi/i) !== -1 || optionText.search(/perusahaan/i) !== -1) {
                fetchDataSelect(FORM.find('#tmp_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(jenis)), '#perusahaan_id');
                FORM.find('.section-perusahaan').show('slow');
                setValidateForm({
                    element: '#perusahaan_id',
                    rules: { required: true }   
                }, "add");

                FORM.find('.section-perusahaan').children('label').html('Asuransi');
                if(optionText.search(/perusahaan/i) !== -1) 
                    FORM.find('.section-perusahaan').children('label').html('Perusahaan');
            } else {
                FORM.find('.section-perusahaan').hide('slow');
                FORM.find('#perusahaan_id').val('').change();
            }
        });

        FORM.find('#layanan_id').change(function() {
            let val = parseInt($(this).val());
            fetchDataSelect(FORM.find('#tmp_dokter_id').val(), URL.fetchDokter.replace(':Q', btoa(val)), '#dokter_id');
        });

        FORM.validate({
            rules: {
                nama_pasien: { required: true },
                title_id: { required: true },
                jenis_kelamin: { required: true },
                tgl_lahir: { required: true },
                no_telepon: { required: true, minlength: 9 },
                tanggal: { required: true },
                cara_bayar_id: { required: true },
                layanan_id: { required: true },
                dokter_id: { required: true },
            },
            messages: {
                no_telepon: {
                    minlength: 'Minimal 9 Karakter',
                }
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        $.unblockUI();
                        result = JSON.parse(result);
                        var data = result.data;
                        successMessage('Success', "Booking berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 2000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        BTN_CANCEL.click(function() {
            window.location.assign(URL.index);
        });
    }

    initializeFormSearch();
    initializeSearchResult();
    initializeForm();
    fillForm(UID);
});