$(() => {
    let INITIALIZED = {
        daftar_booking: false,
    };

    let initializeDaftarBooking = () => {
        let subsDate = (range, tipe) => {
            let date = range.substr(0, 10);
            if(tipe === "sampai") date = range.substr(13, 10);
            return getDate(date);
        }

        // DATATABLE
        TABLE_DT = TABLE.DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
              "url": URL.loadData,
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate(TABLE_FILTER.tanggal.val(), 'dari');
                  p.tanggal_sampai = subsDate(TABLE_FILTER.tanggal.val(), 'sampai');
                  p.no_rekam_medis = TABLE_FILTER.no_rekam_medis.val();
                  p.nama = TABLE_FILTER.nama.val();
                  p.telepon = TABLE_FILTER.telepon.val();
                  p.layanan_id = TABLE_FILTER.layanan_id.val();
                  p.cara_bayar_id = TABLE_FILTER.cara_bayar_id.val();
              }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';
                        return tgl;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a href="${URL.form.replace(':UID', row.uid)}" data-popup="tooltip" title="Lihat Booking">${data}</a>`;
                        tmp += `<br/><span class="text-size-mini text-info"><b>Status Pasien:</b><br/> ${row.status_pasien}</span>`;
                        return tmp;
                    },
                },
                {
                    "data": "nama_pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                {
                    "data": "layanan",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "orderable": false,
                    "render": (data, type, row, meta) => {
                        var tmp = "";
                        switch(parseInt(row.status)) {
                            case -1:
                                tmp = `<label class="label label-warning text-center">Dibatalkan</label>`;
                                tmp += `<br/><span class="text-size-mini text-info"><b>Alasan:</b><br/> ${row.alasan_batal ? row.alasan_batal : "&mdash;"}</span>`;
                                break;
                            case 1:
                                tmp += `<a data-uid="${data}" data-popup="tooltip" title="Konfirmasi" class="btn btn-primary btn-xs confirm-row"><i class="fa fa-check"></i></a>`;
                                tmp += `&nbsp;<a data-uid="${data}" data-popup="tooltip" title="Batalkan?" class="btn btn-warning btn-xs batal-row"><i class="fa fa-times"></i></a>`;
                                break;
                            default:
                                tmp = `<label class="label label-success text-center">Dikonfirmasi</label>`;
                                break;
                        }
                        return tmp;
                    },
                }
            ],
            "order": [ [0, "asc"] ],
            "drawCallback": function (oSettings) {
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE.on('click', function () {
            TABLE_DT.draw(false);
        });

        BTN_RESET_TABLE.on('click', () => {
            TABLE_FILTER.no_rekam_medis.val('');
            TABLE_FILTER.nama.val('');
            TABLE_FILTER.telepon.val('');
            TABLE_FILTER.layanan_id.val(0).trigger('change');
            TABLE_FILTER.cara_bayar_id.val(0).trigger('change');
        });

        /**
         * FILTERS
         */
        TABLE_FILTER.tanggal.daterangepicker({
            startDate: moment(),
            endDate: moment(),
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            }
        });

        TABLE_FILTER.tanggal.on('apply.daterangepicker', (ev, picker) => {
            TABLE_DT.draw();
        });

        TABLE_FILTER.no_rekam_medis.on('change keyup blur', function () {
            TABLE_DT.draw();
        });

        TABLE_FILTER.nama.on('change keyup blur', function () {
            TABLE_DT.draw();
        });

        TABLE_FILTER.telepon.on('change keyup blur', function () {
            TABLE_DT.draw();
        });

        TABLE_FILTER.layanan_id.on('change keyup blur', function () {
            TABLE_DT.draw();
        });

        TABLE_FILTER.cara_bayar_id.on('change keyup blur', function () {
            TABLE_DT.draw();
        });

        /**
         * EVENT SOURCE
         */
        let listen = () => {
            if (TABLE_EVENT_SOURCE) {
                TABLE_EVENT_SOURCE.close();
            }
            let eventId = 'rawat_jalan-daftar_booking';
            TABLE_EVENT_SOURCE = new EventSource(URL.listen);
            TABLE_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DT.draw(false);
                }
            });
        }

        listen();

        INITIALIZED.daftar_booking = true;
    }

    let fetchLayanan = (layanan_id) => {
        layanan_id = layanan_id || 0;
        $.getJSON(URL.fetchLayanan, (res, status) => {
            if (status === 'success') {
                let data = res.data;

                TABLE_FILTER.layanan_id.empty();
                TABLE_FILTER.layanan_id.append(`<option value="0">${fieldAll}</option/>`);
                for (let layanan of data) {
                    $("<option/>")
                        .prop('value', layanan.id)
                        .html(layanan.nama)
                        .appendTo(TABLE_FILTER.layanan_id);
                }

                TABLE_FILTER.layanan_id.val(layanan_id).trigger('change');
            }
        });
    }

    let fetchCaraBayar = (cara_bayar_id) => {
        cara_bayar_id = cara_bayar_id || 0;
        $.getJSON(URL.fetchCaraBayar, (res, status) => {
            if (status === 'success') {
                let data = res.data;

                TABLE_FILTER.cara_bayar_id.empty();
                TABLE_FILTER.cara_bayar_id.append(`<option value="0">${fieldAll}</option>`);
                for (let cara_bayar of data) {
                    $("<option/>")
                        .prop('value', cara_bayar.id)
                        .html(cara_bayar.nama)
                        .appendTo(TABLE_FILTER.cara_bayar_id);
                }

                TABLE_FILTER.cara_bayar_id.val(cara_bayar_id).trigger('change');
            }
        });
    }

    TABLE.on("click", ".confirm-row", function() {
        let uid = $(this).data('uid');
        blockPage('Form konfirmasi sedang diproses ...');
        setTimeout(function() { 
            window.location.assign(URL.confirm.replace(':UID', uid));
        }, 1000);
    });

    TABLE.on("click", ".batal-row", function () {
        let tr = $(this).closest('tr');
        let data = TABLE_DT.row(tr).data();
        let uid = $(this).data('uid');
        swal({
            title: "Yakin akan membatalkan booking tersebut?",
            text: `<div class="row form-horizontal text-left">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. RM</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold">${data.no_rm}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Nama</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-nama_pasien">${data.nama_pasien}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Jenis Kelamin</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-jenis_kelamin">${data.jenis_kelamin}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Tanggal Lahir</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-tanggal_lahir">${data.tanggal_lahir}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Umur</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-umur">${data.umur_tahun} Tahun</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">No. Telepon</label>
                                <div class="col-xs-8">
                                    <div class="form-control-static text-bold label-no_telepon">${data.no_telepon}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    Alasan pembatalan<br/><textarea class='form-control' id='swal-text' style='resize: vertical;'></textarea>`,
            html: true,
            showCancelButton: true,
            //customClass: 'swal-wide',
            confirmButtonColor: "#F44336",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top",
        },
        function() {        
            let alasan = $("#swal-text").val();
            if (alasan === "") {
              swal.showInputError("Silahkan isi alasan pembatalan booking.");
              $("#swal-text").focus();
              return;
            }

            $.post(URL.batal, { uid: uid, alasan: alasan}, function (data, status) {
                if (status === "success") {
                    swal.close();
                    TABLE_DT.draw();
                    successMessage('Berhasil !', 'Pembatalan booking berhasil dilakukan.');
                }
            })
            .fail(function (error) {
                swal.close();
                errorMessage('Gagal !', 'Terjadi kesalahan saat pembatalan booking.');
            });
        });
    });

    initializeDaftarBooking();
    fetchLayanan(0);
    fetchCaraBayar(0);
});