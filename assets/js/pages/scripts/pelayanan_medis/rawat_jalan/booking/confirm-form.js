/*
*    modalTindakan, modalTindakanLayananId, selectedTindakan, modalBtnTambahTindakan => didapat dari file modal 'pelayanan_medis/search-tarif-pelayanan-modal.php'
*/

$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    $('.input-tgl_lahir').pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        max: moment().format('dd/mm/yyyy')
    });

    $("#tanggal").pickadate({
        format: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: 100,
        min: moment().format('dd/mm/yyyy')
    });

    let historyKunjungan = (pasien_uid) => {
        if(pasien_uid === "") {
            $('#div-history_kunjungan').html('');
            return;
        }

        $.getJSON(URL.getHistoryKunjungan.replace(':PASIEN_UID', pasien_uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                let tmp = '';
                if(data.length > 0) {
                    data = data[0];
                    tmp += `<p class="no-margin text-info"><b>Kunjungan Terakhir</b></p>` +
                            `<p class="no-margin"><b>Tanggal:</b> ${moment(data.tanggal).format('DD/MM/YYYY')}</p>` +
                            `<p class="no-margin"><b>Layanan:</b> ${ucwords(data.layanan)}</p>` +
                            `<p class="no-margin"><b>Dokter:</b> ${ucwords(data.dokter)}</p>`;
                }
                $('#div-history_kunjungan').html(tmp);
            }
        });
    }

    let fetchDataSelect = (id, url, element) => {
        id = id || "";
        let parent = $(element).parent();
        parent.find('.loading-select').show();
        $.getJSON(url, (res, status) => {
            if (status === 'success') {
                let options = '';
                let datas = res.data;

                $(element).empty();
                $(element).append('<option value="" selected="selected">- Pilih -</option/>');
                for (let data of datas) {
                    let selected = "";
                    if(data.id === id) selected = `selected="selected"`;

                    let value = `<option value="${data.id}" ${selected}>${data.nama}</option>`;                    
                    switch(element) {
                        case "#title_id":
                            value = `<option value="${data.id}" ${selected} data-jenis_kelamin="${data.jenis_kelamin}">${data.singkatan}</option>`;
                            break;
                        case "#kelurahan_id":
                            value = `<option value="${data.id}" ${selected} data-kodepos="${data.kodepos}">${data.nama}</option>`;
                            break;
                        case "#cara_bayar_id":
                            value = `<option value="${data.id}" ${selected} data-jenis="${data.jenis}">${data.nama}</option>`;
                            break;
                    }
                    options += value;
                }
                $(element).append(options);
                $(element).trigger('change');
            }
            parent.find('.loading-select').hide();
        });
    }

    let setValidateForm = (obj, mode) => {
        FORM.find(obj.element).rules("remove");
        if(mode === "add") FORM.find(obj.element).rules('add', obj.rules);
    }

    // sementara digunakan untuk tarif pelayanan yang tidak memiliki layanan
    let getTarifPelayanan = (id, jenis_tindakan) => {
        $.getJSON(URL.getTarifPelayanan.replace(':Q', btoa(id)), (res, status) => {
            if (status === 'success') {
                addTarifPelayanan(res.data, jenis_tindakan);
            };
        });
    }

    let addTarifPelayanan = (obj, jenis_tindakan) => {
        let tbody = $("#table-tarif_pelayanan tbody");

        let tr = $("<tr/>")
            .data('uid', obj.tarif_pelayanan_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.tarif_pelayanan)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_detail_id[]')
            .val(0)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_id[]')
            .val(obj.tarif_pelayanan_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_uid[]')
            .val(obj.tarif_pelayanan_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan[]')
            .val(obj.tarif_pelayanan)
            .appendTo(tdNama);
        let inputTarif = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_tarif[]')
            .val(obj.tarif)
            .appendTo(tdNama);
        let inputJenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_jenis_tindakan[]')
            .val(jenis_tindakan)
            .appendTo(tdNama);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(obj.quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdDisc = $("<td/>")
            .addClass('text-right')
            .hide()
            .appendTo(tr);
        let labelDisc = $("<span>")
            .html(0)
            .appendTo(tdDisc);
        let inputDisc = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_disc[]')
            .val(0)
            .appendTo(tdDisc);
        let dispInputDisc = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdDisc);
        dispInputDisc.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'vMax': 100, 'aSign': '%', 'pSign': 's'});
        dispInputDisc.autoNumeric('set', 0).hide();

        let tdTarif = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.tarif).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handler
        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQty.hide();
            labelDisc.hide();
            dispInputQty.show();
            dispInputDisc.show();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            labelDisc.show();
            dispInputQty.hide();
            dispInputDisc.hide();
        });

        btnDel.on('click', (e) => {
            tr.remove();
            updateTarifTotal();
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            labelDisc.show();
            dispInputQty.hide();
            dispInputDisc.hide();
        });

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let disc = parseFloat(inputDisc.val()) / 100;
            let tarif = parseFloat(inputTarif.val());
            let total = (tarif - (tarif * disc)) * quantity;
            tdTarif.html('Rp.' + numeral(total).format());
            
            updateTarifTotal();
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputDisc.on('keyup change blur', (e) => {
            let val = dispInputDisc.autoNumeric('get') == "" ? 0 : dispInputDisc.autoNumeric('get');
            inputDisc.val(val);
            labelDisc.html(numeral(val).format('#.##') + "%");

            updateTarifRow();
        });

        updateTarifTotal();
    }

    let addPemeriksaan = (obj) => {
        let tbody = $("#table-tarif_pelayanan tbody");

        let tr = $("<tr/>")
            .data('uid', obj.uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.nama)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_id[]')
            .val(0)
            .appendTo(tdNama);
        let inputRujukanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_rujukan_id[]')
            .val(0)
            .appendTo(tdNama);
        let inputPemeriksaanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_pemeriksaan_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputTarifPelayananId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_tarif_pelayanan_id[]')
            .val(obj.tarif_pelayanan_id)
            .appendTo(tdNama);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_harga[]')
            .val(obj.harga)
            .appendTo(tdNama);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(1)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_quantity[]')
            .val(1)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', 1).hide();

        let tdDisc = $("<td/>")
            .addClass('text-right')
            .hide()
            .appendTo(tr);
        let labelDisc = $("<span>")
            .html(0)
            .appendTo(tdDisc);
        let inputDiscountJenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_discount_jenis[]')
            .val("nominal")
            .appendTo(tdDisc);
        let inputDisc = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_discount[]')
            .val(0)
            .appendTo(tdDisc);
        let dispInputDisc = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdDisc);
        dispInputDisc.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'vMax': 100, 'aSign': '%', 'pSign': 's'});
        dispInputDisc.autoNumeric('set', 0).hide();

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handler
        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQty.hide();
            labelDisc.hide();
            dispInputQty.show();
            dispInputDisc.show();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            labelDisc.show();
            dispInputQty.hide();
            dispInputDisc.hide();
        });

        btnDel.on('click', (e) => {
            tr.remove();
            updateTarifTotal();
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQty.show();
            labelDisc.show();
            dispInputQty.hide();
            dispInputDisc.hide();
        });

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let disc = parseFloat(inputDisc.val()) / 100;
            let harga = parseFloat(inputHarga.val());
            let total = (harga - (harga * disc)) * quantity;
            tdHarga.html('Rp.' + numeral(total).format());
            
            updateTarifTotal();
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputDisc.on('keyup change blur', (e) => {
            let val = dispInputDisc.autoNumeric('get') == "" ? 0 : dispInputDisc.autoNumeric('get');
            inputDisc.val(val);
            labelDisc.html(numeral(val).format('#.##') + "%");

            updateTarifRow();
        });

        updateTarifTotal();
    }

    let updateTarifTotal = () => {
        let total = 0;
        $("#table-tarif_pelayanan tbody").find('tr').each((i, el) => {
            total += numeral($(el).find('td:eq(3)').html())._value;
        });
        FORM.find("#label-total").html('Rp.' + numeral(total).format());
    }

    let fillForm = (uid) => {
        if(uid === "") {
            swal({
                title: "Peringatan !!",
                text: "Data tidak ditemukan ...",
                type: "warning",
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                animation: "slide-from-top",
            },
            function() {        
                window.location.assign(URL.index);
            });
            return;
        }

        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                FORM.find('#status_pasien_id').val(data.status_pasien_id);
                FORM.find('#label-status_pasien').html(data.status_pasien);
                FORM.find('#disp-no_rm').html(data.no_rm ? data.no_rm : "&mdash;");
                FORM.find('#nama').val(data.nama_pasien);
                FORM.find(`input:radio[name=jenis_kelamin][value=${data.jenis_kelamin}]`).click();
                FORM.find('#tanggal_lahir').pickadate('picker').set('select', data.tanggal_lahir, {format: 'dd/mm/yyyy'});
                FORM.find('#no_telepon_1').val(data.no_telepon);
                FORM.find('#id').val(data.id);
                FORM.find('#uid').val(data.uid);
                FORM.find('#pasien_id').val(data.pasien_id);

                FORM.find('#tmp_perusahaan_id').val(data.perusahaan_id);
                FORM.find('#tmp_dokter_id').val(data.dokter_id);
                fetchDataSelect(data.title_id, URL.fetchTitle, '#title_id');
                fetchDataSelect(data.cara_bayar_id, URL.fetchCaraBayar, '#cara_bayar_id');
                fetchDataSelect(data.layanan_id, URL.fetchLayanan, '#layanan_id');

                if(data.status_pasien.search(/lama/i) !== -1) {
                    FORM.find('#tmp_kabupaten_id').val(data.pasien.kabupaten_id);
                    FORM.find('#tmp_kecamatan_id').val(data.pasien.kecamatan_id);
                    FORM.find('#tmp_kelurahan_id').val(data.pasien.kelurahan_id);                    
                    
                    FORM.find(`input:radio[name=kewarganegaraan][value=${data.pasien.kewarganegaraan}]`).click();
                    FORM.find('#provinsi_id').val(data.pasien.provinsi_id).change();                    
                    FORM.find('#kodepos').val(data.pasien.kodepos);                    
                    FORM.find('#alamat').val(data.pasien.alamat);                    
                    FORM.find('#jenis_identitas').val(data.pasien.jenis_identitas).change();                    
                    FORM.find('#no_identitas').val(data.pasien.no_identitas);                    
                    FORM.find('#jenis_telepon_1').val(data.pasien.jenis_telepon_1).change();                    
                    FORM.find('#jenis_telepon_2').val(data.pasien.jenis_telepon_2).change();                    
                    FORM.find('#no_telepon_2').val(data.pasien.no_telepon_2);
                    FORM.find('#tempat_lahir').val(data.pasien.tempat_lahir);
                    FORM.find('#golongan_darah').val(data.pasien.golongan_darah).change();                    
                    FORM.find('#agama').val(data.pasien.agama).change();                    
                    FORM.find('#status_kawin').val(data.pasien.status_kawin).change();                    
                    FORM.find('#pendidikan_id').val(data.pasien.pendidikan_id).change();                    
                    FORM.find('#pekerjaan_id').val(data.pasien.pekerjaan_id).change();                    
                    getTarifPelayanan(DEFTARIF.pendaftaranUlang, imediscode.JENIS_TINDAKAN_RAJAL_PENDAFTARAN);
                } else {
                    FORM.find('#provinsi_id').val("").change();
                    getTarifPelayanan(DEFTARIF.pendaftaranBaru, imediscode.JENIS_TINDAKAN_RAJAL_PENDAFTARAN);
                }
                
                historyKunjungan(data.pasien.uid);
                $.uniform.update();
                if(parseInt(data.status) === -1 || parseInt(data.status) === 2) {
                    BTN_SAVE.hide();
                    BTN_CANCEL.text('Kembali');
                }
            }
        }); 
    }

    let initializeForm = () => {
        FORM.find('#provinsi_id').change(function() {
            let val = isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
            fetchDataSelect(FORM.find('#tmp_kabupaten_id').val(), URL.fetchKabupaten.replace(':Q', val), '#kabupaten_id');
        });

        FORM.find('#kabupaten_id').change(function() {
            let val = isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
            fetchDataSelect(FORM.find('#tmp_kecamatan_id').val(), URL.fetchKecamatan.replace(':Q', val), '#kecamatan_id');
        });

        FORM.find('#kecamatan_id').change(function() {
            let val = isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
            fetchDataSelect(FORM.find('#tmp_kelurahan_id').val(), URL.fetchKelurahan.replace(':Q', val), '#kelurahan_id');
        });

        FORM.find('#kelurahan_id').change(function() {
            let kodePos = $(this).find('option:selected').data('kodepos');
            FORM.find('#kodepos').val(kodePos);
        });

        FORM.on('change click', '.set-title', function() {
            let umur = FORM.find('#umur_tahun').val();
            let jenisKelamin = FORM.find('[name=jenis_kelamin]:checked').data('name');
            let statusKawin = FORM.find('#status_kawin').find('option:selected').text();
            let result = setTitleName(jenisKelamin, umur.split(" ")[0], statusKawin);

            let titleId = "";
            $('#title_id option').each(function() {
                if(result == $(this).text().toLowerCase()) 
                    titleId = $(this).val();
            });
            FORM.find('#title_id').val(titleId).change();
        });

        /*FORM.find('#title_id').change(function() {
            let jenisKelamin = $(this).find('option:selected').data('jenis_kelamin');
            if(jenisKelamin > 0) {
                FORM.find(`input:radio[name=jenis_kelamin]`).prop('checked', false).parent().removeClass('checked');
                FORM.find(`input:radio[name=jenis_kelamin][value=${jenisKelamin}]`).click();
                $.uniform.update();
            }
        });*/

        FORM.on('change keyup', '#alamat', function() {
            FORM.find("#pj_alamat").val($(this).val());
        });

        FORM.find('#tanggal_lahir').change(function() {
            let val = moment($(this).val(), 'DD/MM/YYYY').isValid() ? moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');
            let result = calculateAge(val);

            FORM.find('#umur_tahun').val(`${result.year} Tahun`);
        });

        FORM.on('change keyup', '#umur_tahun', function() {
            let val = parseInt($(this).val());
            if(!isNaN(val)) {
                var yearNow = moment().year(),
                    yearAgo = ((yearNow - val) > 0) ? yearNow - val : 0;

                var dob = moment([yearAgo, moment().month(), moment().date()]).format();
                tglLahir = moment(new Date(dob)).format("DD/MM/YYYY");

                FORM.find('#tanggal_lahir').pickadate('picker').set('select', tglLahir, {format: 'dd/mm/yyyy'});
            }
        });

        FORM.find('#rujukan_dari').change(function() {
            FORM.find('.section-rujukan_fktp').hide('slow');
            FORM.find('.section-rujukan_fkrtl').hide('slow');
            setValidateForm({element: '#nama_perujuk'}, "remove");
            setValidateForm({element: '#rumah_sakit_id'}, "remove");

            let el = {};
            let optionText = $(this).find('option:selected').text();
            if(optionText.search(/fktp/i) !== -1) {
                el['sectionInput'] = '.section-rujukan_fktp';
                el['element'] = '#nama_perujuk';
            } else if(optionText.search(/fkrtl/i) !== -1) {
                el['sectionInput'] = '.section-rujukan_fkrtl';
                el['element'] = '#rumah_sakit_id';
                fetchDataSelect('', URL.fetchRumahSakit, '#rumah_sakit_id');
            }

            FORM.find(el.sectionInput).show('slow');
            setValidateForm({
                element: el.element,
                rules: { required: true }   
            }, "add");
        });

        FORM.find('#cara_bayar_id').change(function() {
            let val = parseInt($(this).val());
            let jenis = $(this).find('option:selected').data('jenis');
            let optionText = $(this).find('option:selected').text();

            FORM.find('.section-perusahaan').hide('slow');
            FORM.find('.section-no_jaminan').hide('slow');
            FORM.find('.section-no_kartu').hide('slow');
            FORM.find('.section-penjamin').hide('slow');
            FORM.find('.section-penjamin_perusahaan').hide('slow');
            FORM.find('.section-penjamin_no_jaminan').hide('slow');
            FORM.find('.input-cara_bayar').val('').change();

            // SEP BPJS
            FORM.find('#btn-sep').hide('slow');
            FORM.find('.section-no_jaminan input').attr('readonly', false);

            setValidateForm({element: '#perusahaan_id'}, "remove");
            setValidateForm({element: '#no_jaminan'}, "remove");
            setValidateForm({element: '#no_kartu'}, "remove");
            setValidateForm({element: '#penjamin_perusahaan_id'}, "remove");
            setValidateForm({element: '#penjamin_no_jaminan'}, "remove");

            switch (jenis) {
                case imediscode.CARA_BAYAR_BPJS:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('No. SEP');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-no_kartu').show('slow');
                    setValidateForm({
                        element: '#no_kartu',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-penjamin').show('slow');
                    FORM.find('#btn-sep').show('slow');
                    FORM.find('.section-no_jaminan input').attr('readonly', true);
                    break;
                case imediscode.CARA_BAYAR_JAMKESDA:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('No. Jamkesda');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
                case imediscode.CARA_BAYAR_ASURANSI:
                case imediscode.CARA_BAYAR_PERUSAHAAN:
                    let labelPerusahaan = 'Perusahaan';
                    let labelNoJaminan = 'NIK';
                    if(parseInt(jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                        labelPerusahaan = 'Asuransi';
                        labelNoJaminan = 'No. Anggota';

                        let q = 0;
                        $('#penjamin_id option').each(function() {
                            if("perusahaan" == $(this).text().toLowerCase()) 
                                q = $(this).val();
                        });
                        fetchDataSelect(FORM.find('#tmp_penjamin_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(q)), '#penjamin_perusahaan_id');
                        FORM.find('.section-penjamin_perusahaan').show('slow').children('label').html('Perusahaan');
                    }

                    fetchDataSelect(FORM.find('#tmp_perusahaan_id').val(), URL.fetchPerusahaan.replace(':Q', btoa(jenis)), '#perusahaan_id');
                    FORM.find('.section-perusahaan').show('slow').children('label').html(labelPerusahaan);
                    setValidateForm({
                        element: '#perusahaan_id',
                        rules: { required: true }   
                    }, "add");

                    FORM.find('.section-no_jaminan').show('slow').children('label').html(labelNoJaminan);
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
                case imediscode.CARA_BAYAR_INTERNAL:
                    FORM.find('.section-no_jaminan').show('slow').children('label').html('NIK');
                    setValidateForm({
                        element: '#no_jaminan',
                        rules: { required: true }   
                    }, "add");
                    break;
            }
        });

        FORM.find('#penjamin_id').change(function() {
            FORM.find('.section-penjamin_perusahaan').hide('slow');
            FORM.find('.section-penjamin_no_jaminan').hide('slow');

            let val = $(this).val();
            if(val !== "") {
                FORM.find('.input-penjamin').val('').change();
                setValidateForm({element: '#penjamin_perusahaan_id'}, "remove");
                setValidateForm({element: '#penjamin_no_jaminan'}, "remove");

                let labelPerusahaan = 'Perusahaan';
                let labelNoPenjamin = 'NIK';
                if(val === imediscode.CARA_BAYAR_ASURANSI) {
                    labelPerusahaan = 'Asuransi';
                    labelNoPenjamin = 'No. Anggota';
                }

                fetchDataSelect('', URL.fetchPerusahaan.replace(':Q', btoa(val)), '#penjamin_perusahaan_id');
                FORM.find('.section-penjamin_perusahaan').show('slow').children('label').html(labelPerusahaan);
                setValidateForm({
                    element: '#penjamin_perusahaan_id',
                    rules: { required: true }   
                }, "add");

                FORM.find('.section-penjamin_no_jaminan').show('slow').children('label').html(labelNoPenjamin);
                setValidateForm({
                    element: '#penjamin_no_jaminan',
                    rules: { required: true }   
                }, "add");
            }
        });

        FORM.find('#layanan_id').change(function() {
            let layanan = $(this).find('option:selected').text();
            FORM.find('[name="layanan"]').val(layanan);

            let val = parseInt($(this).val());
            fetchDataSelect(FORM.find('#tmp_dokter_id').val(), URL.fetchDokter.replace(':Q', btoa(val)), '#dokter_id');

            if(UID !== "")
                $(this).prop('disabled', true);
        });

        FORM.find('#dokter_id').change(function() {
            let dokter = $(this).find('option:selected').text();
            FORM.find('[name="dokter"]').val(dokter);
        });

        FORM.find('input[name="pj_yang_bersangkutan"]').click(function() {
            let val = parseInt($(this).val());
            if(val === 1) {
                setValidateForm({element: '#pj_hubungan'}, "remove");
                FORM.find('#pj_hubungan').parents('.form-group').hide('slow');
                FORM.find('#pj_nama').val($('#nama').val());
                FORM.find('#pj_alamat').val($('#alamat').val());
                FORM.find('#pj_no_telepon').val($('#no_telepon_1').val());
                FORM.find('#pj_no_identitas').val($('#no_identitas').val());
            } else {
                setValidateForm({
                    element: '#no_jaminan',
                    rules: { required: true }   
                }, "add");
                FORM.find('.input-pj').val('').change();
                FORM.find('#pj_hubungan').parents('.form-group').show('slow');     
            }
        });

        FORM.find("#btn-tambah_tarif_pelayanan").on('click', () => {
            let layanan = FORM.find('#layanan_id').find('option:selected').text();
            if(layanan.search(/laboratorium/i) !== -1) {
                MODAL_LAB_SEARCH_PEMERIKSAAN.modal('show');
                blockElement(MODAL_LAB_SEARCH_PEMERIKSAAN.find('.modal-dialog').selector);
                $.getJSON(URL.loadPemeriksaanLab, (res, status) => {
                    if (status === 'success') {
                        let data = res.data;
                        MODAL_LAB_SEARCH_PEMERIKSAAN.trigger('source', [data, []]);
                        MODAL_LAB_SEARCH_PEMERIKSAAN.find('.modal-dialog').unblock();
                    }
                });
            } else if(layanan.search(/radiologi/i) !== -1) {
                MODAL_RAD_SEARCH_PEMERIKSAAN.modal('show');
                blockElement(MODAL_RAD_SEARCH_PEMERIKSAAN.find('.modal-dialog').selector);
                $.getJSON(URL.loadPemeriksaanRad, (res, status) => {
                    if (status === 'success') {
                        let data = res.data;
                        MODAL_RAD_SEARCH_PEMERIKSAAN.trigger('source', [data, []]);
                        MODAL_RAD_SEARCH_PEMERIKSAAN.find('.modal-dialog').unblock();
                    }
                });
            } else {
                $(modalTindakanLayananId).val($('#layanan_id').val());
                $(modalTindakanCaraBayarId).val($('#cara_bayar_id').val());
                $(modalTindakanPerusahaanId).val(($('#perusahaan_id').val() === null ? "" : $('#perusahaan_id').val()));
                $(modalTindakanKelasId).val(imediscode.DEF_KELAS);
                $(modalTindakan).modal('show');
            }
        });

        // MODAL Handler
        $(modalBtnTambahTindakan).on('click', () => {
            for (var dt_index in selectedTindakan) {
                let trData = selectedTindakan[dt_index];
                    
                let isExists = false;
                $("#table-tarif_pelayanan tbody").find('tr').each((i, el) => {
                    if ($(el).data('uid') == trData.uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih tindakan ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    tindakan_id: 0,
                    tarif_pelayanan_id: trData.id,
                    tarif_pelayanan_uid: trData.uid,
                    kode: trData.kode,
                    tarif_pelayanan: trData.nama,
                    tarif: trData.tarif,
                    quantity: 1,
                    jenis_tindakan: '',
                };
                addTarifPelayanan(data, 0);
            }

            $(modalTindakan).modal('hide');
        });

        MODAL_LAB_SEARCH_PEMERIKSAAN.onSave = (data) => {
            for (let d of data) {
                addPemeriksaan(d);
            }
            
            MODAL_LAB_SEARCH_PEMERIKSAAN.modal('hide');
        };

        MODAL_RAD_SEARCH_PEMERIKSAAN.onSave = (data) => {
            for (let d of data) {
                addPemeriksaan(d);
            }
            
            MODAL_RAD_SEARCH_PEMERIKSAAN.modal('hide');
        };

        FORM.validate({
            rules: {
                nama: { required: true },
                title_id: { required: true },
                jenis_kelamin: { required: true },
                alamat: { required: true, maxlength: 255 },
                provinsi_id: { required: true },
                kabupaten_id: { required: true },
                kecamatan_id: { required: true },
                kelurahan_id: { required: true },
                kodepos: { required: true },
                jenis_identitas: { required: true },
                no_identitas: { required: true, maxlength: 16 },
                jenis_telepon_1: { required: true },
                no_telepon_1: { required: true, minlength: 9 },
                tempat_lahir: { required: true },
                tanggal_lahir: { required: true },
                golongan_darah: { required: true },
                //agama: { required: true },
                //status_kawin: { required: true },
                pendidikan_id: { required: true },
                pekerjaan_id: { required: true },
                tanggal: { required: true },
                rujukan_dari: { required: true },
                cara_bayar_id: { required: true },
                layanan_id: { required: true },
                dokter_id: { required: true },
                pj_yang_bersangkutan: { required: true },
                pj_hubungan: { required: true },
                pj_nama: { required: true },
                pj_no_telepon: { required: true, minlength: 9 },
            },
            messages: {
                alamat: {
                    maxlength: 'Maksimal 255 Karakter',
                },
                no_identitas: {
                    maxlength: 'Maksimal 16 Karakter',
                },
                no_telepon_1: {
                    minlength: 'Minimal 9 Karakter',
                },
                pj_no_telepon: {
                    minlength: 'Minimal 9 Karakter',
                },
            },
            focusInvalid: true,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;

                $('html, body').animate({
                    scrollTop: FORM.offset().top
                }, 1000);
            },
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();

                $('input, textarea, select').prop('disabled', false);

                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        $.unblockUI();
                        result = JSON.parse(result);
                        var data = result.data;
                        successMessage('Success', "Konfirmasi booking berhasil dilakukan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 2000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                    }
                });
            }
        });

        BTN_CANCEL.click(function() {
            window.location.assign(URL.index);
        });
    }

    initializeForm();
    fillForm(UID);
});