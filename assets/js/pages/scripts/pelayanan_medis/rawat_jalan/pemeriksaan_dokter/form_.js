$(() => {
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    $(".styled").uniform({
        radioClass: 'choice'
    });

    $(".select-signa").select2({
        placeholder: "- Pilih -",
    });

    let fillTabPemeriksaaan = (uid) => {
        blockElement(formPemeriksaan);
        $(formPemeriksaan).find("#id").val(0);
        $(formPemeriksaan).find("#uid").val('');

        $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'pemeriksaan'), function (res, status) {
            if (status === 'success') {
                enablePlanning = res.tab_planning;

                let data = res.data;
                if(data) {
                    $(formPemeriksaan).find("#id").val(data.id);
                    $(formPemeriksaan).find("#uid").val(data.uid);

                    if(data.data_pemeriksaan) {
                        let pemeriksaan = data.data_pemeriksaan;
                        let label = Object.keys(pemeriksaan);
                        for (var i = 0; i < label.length; i++) {
                            if($(`#${label[i]}`).is(':text')) {
                                if(pemeriksaan[label[i]]) $(`#${label[i]}`).val(pemeriksaan[label[i]]);
                            } else {
                                if(pemeriksaan[label[i]]) $(`input[name=${label[i]}][value="${pemeriksaan[label[i]]}"]`).click();
                            }
                        }
                        $.uniform.update();
                    }
                }
                $(formPemeriksaan).unblock();
            }
        });
    }

    let fillTabPlanning = (uid) => {
        listResepObat = [];

        blockElement(formPlanning);
        $(formPlanning).find("#id").val(0);
        $(formPlanning).find("#uid").val('');

        $.getJSON(URL.getData.replace(':UID', uid).replace(':BROWSE', 'planning'), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                if(data) {
                    $(formPlanning).find("#id").val(data.id);
                    $(formPlanning).find("#uid").val(data.uid);
                    $(formPlanning).find("#resep_id").val(data.resep.id);

                    // RESEP
                    dataRegistrasi.resep = data.resep;

                    // DIAGNOSA UTAMA
                    if(data.diagnosa_utama) {
                        let optDiagnosaUtama = new Option(data.diagnosa_utama.code, data.diagnosa_utama.uid, true, true);
                        $("#diagnosa_utama").append(optDiagnosaUtama).trigger('change');

                        $("#diagnosa_utama").trigger({
                            type: 'select2:select',
                            params: {
                                data: {
                                        id: data.diagnosa_utama.uid,
                                        code: data.diagnosa_utama.title,
                                        text: data.diagnosa_utama.code,
                                    }
                            }
                        });
                    }

                    // DIAGNOSA SEKUNDER
                    $("#diagnosa_sekunder").html('');
                    if(data.diagnosa_sekunder) {
                        let optDiagnosaSekunder;
                        for (var i = 0; i < data.diagnosa_sekunder.length; i++) {
                            optDiagnosaSekunder = new Option(data.diagnosa_sekunder[i].code, data.diagnosa_sekunder[i].uid, true, true);
                            $("#diagnosa_sekunder").append(optDiagnosaSekunder).trigger('change');
                        }
                        $("#diagnosa_sekunder").append(optDiagnosaSekunder).trigger('change');

                        $("#diagnosa_sekunder").trigger({
                            type: 'select2:select',
                            params: {
                                data: data.diagnosa_sekunder.map((item) => {
                                    return {
                                        id: item.uid,
                                        code: item.title,
                                        text: item.code,
                                    };
                                })
                            }
                        });
                    }

                    // TINDAKAN
                    $(tableTindakan + " tbody").empty();
                    for (var i = 0; i < data.tindakan_list.length; i++) {
                        addTindakan(data.tindakan_list[i]);
                    }

                    // BMHP
                    $(tableBmhp + " tbody").empty();
                    for (var i = 0; i < data.bmhp_list.length; i++) {
                        addBmhp(data.bmhp_list[i]);
                    }


                    let listMode = "view";
                    $('#btn-tambah_racikan').parents('tfoot').show();
                    $('#btn-tambah_non_racikan').parents('tfoot').show();
                    if(parseInt(data.resep.status) !== 0) {
                        listMode = "view_noedit";
                        $('#btn-tambah_racikan').parents('tfoot').hide();
                        $('#btn-tambah_non_racikan').parents('tfoot').hide();
                    }
                    // NON RACIKAN
                    $(tableNonRacikan + " tbody").empty();
                    for (var i = 0; i < data.non_racikan_list.length; i++) {
                        addNonRacikan(data.non_racikan_list[i], listMode);
                    }

                    // RACIKAN
                    $(tableRacikan + " tbody").empty();
                    for (var i = 0; i < data.racikan_list.length; i++) {
                        addRacikan(data.racikan_list[i], listMode);
                    }

                    initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.pelayanan.cara_bayar_id));
                }
                $(formPlanning).unblock();
            }
        });
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getDataRegistrasi.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();
    
                let data = res.data;
                fillTabPemeriksaaan(uid);
                $(formPemeriksaan).find("input[name=pelayanan_id]").val(data.id);
                $(formPemeriksaan).find("input[name=pelayanan_uid]").val(data.uid);
                $(formPlanning).find("input[name=pelayanan_id]").val(data.id);
                $(formPlanning).find("input[name=pelayanan_uid]").val(data.uid);
                $(formPlanning).find("#no_register").val(data.no_register);
                $(formPlanning).find("#pasien_id").val(data.pasien_id);
                $(formPlanning).find("#cara_bayar_id").val(data.cara_bayar_id);
                $(formPlanning).find("#perusahaan_id").val(data.perusahaan_id);
                $(formPlanning).find("#layanan_id").val(data.layanan_id);
                $(formPlanning).find("#dokter_id").val(data.dokter_id);

                $("#label-nama_pasien").html(data.pasien.nama);
                $("#label-no_rm").html(data.pasien.no_rm);
                $("#label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $("#label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $("#label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $("#label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $("#label-alamat").html(data.pasien.alamat);
                $("#label-no_telepon").html(data.pasien.no_telepon_1);

                $("#label-no_register").html(data.no_register);
                $("#label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $("#label-layanan").html(data.layanan);
                $("#label-dokter").html(data.dokter);
                $("#label-cara_bayar").html(data.cara_bayar);
                $("#label-perusahaan").html(data.perusahaan);
                $("#label-no_jaminan").html(data.no_jaminan);
                $("#label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $("#label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }
                $.unblockUI();

                dataRegistrasi = data;
            }
        });

        if(MODE === "view") {
            $("input[type=text], textarea, select").prop("disabled", true);
            $(".btn-save").hide();
        }

        $.uniform.update();
    }

    // TAB PEMERIKSAAN
    $(formPemeriksaan).validate({
        rules: {},
        messages: {},
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: URL.savePemeriksaan,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    successMessage('Success', "Pemeriksaan berhasil dilakukan.");

                    fillTabPemeriksaaan(UID);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    // TAB PLANNING
    // DIAGNOSA
    let initializeDiagnosa = (el) => {
        let formatHtml = function (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.code}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">${data.description}</div>
                    </div>
                </div>
            `;
        }

        let formatSelection = function (data) {
            return data.code || data.text || '- Pilih -';
        }

        el.select2({
            placeholder: "- Pilih -",
            ajax: {
                url: URL.getIcd10,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.items, function(obj) {
                            return { id: obj.uid, description: obj.description, code: obj.code };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatHtml, // omitted for brevity, see the source of this page
            templateSelection: formatSelection // omitted for brevity, see the source of this page
        });

        el.on('change', function (e) {
            let result = $(this).select2('data');
            let data = result.pop();
        });
    }
    initializeDiagnosa($("#diagnosa_utama"));
    initializeDiagnosa($("#diagnosa_sekunder"));

    // SIGNA
    function initializeSigna(el, signa_id) {
        $.getJSON(URL.getSigna, function(data, status) {
            if (status === "success") {
                let option = '';
                let arrSignaId = signa_id.split(",");
                for (var i = 0; i < data.data.length; i++) {
                    for(var j = 0; j < arrSignaId.length; j++) {
                        let selected = "";
                        if (arrSignaId[j] === data.data[i].id) selected = "selected='selected'";
                        option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].label}</option>`;
                    }
                }
                el.html(option).trigger("change");
            }
        });
    }
    initializeSigna($("#modal-input_racikan_signa"), "");

    // FARMASI UNIT
    function initializeFarmasiUnit(el, cara_bayar_id) {
        $.getJSON(URL.getFarmasiUnit.replace(':CARA_BAYAR_ID', cara_bayar_id), function(data, status) {
            if (status === "success") {
                let option = `<option value="0" selected="selected">- Pilih -</option>`;
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (data.data.length === 1) selected = "selected='selected'";
                    option += `<option value="${data.data[i].uid}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");

                el.prop('disabled', false);
                if (data.data.length === 1) el.prop('disabled', true);
            }
        });
    }

    // TINDAKAN
    let fetchTindakan = () => {
        let layanan_id = btoa($(formPlanning).find('#layanan_id').val());
        let cara_bayar_id = btoa($(formPlanning).find('#cara_bayar_id').val());
        let perusahaan_id = $(formPlanning).find('#perusahaan_id').val() === null ? "" : btoa($(formPlanning).find('#perusahaan_id').val());

        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_tarif_pelayanan'));
        if(isDTable === true) $('#table-modal_tarif_pelayanan').DataTable().destroy();

        $.getJSON(URL.fetchTarifPelayanan.replace(':LAYANAN_ID', layanan_id).replace(':CARA_BAYAR_ID', cara_bayar_id).replace(':PERUSAHAAN_ID', perusahaan_id), (res, status) => {
            if (status === 'success') {
                tableListTindakan = $('#table-modal_tarif_pelayanan').DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": res.data,
                    "columns": [
                        { 
                            "data": "id",
                            "render": function (data, type, row, meta) {
                                return `<div class="checkbox"><label><input type="checkbox" class="check" data-id="${data}" /></label></div>`;
                            },
                        },
                        { "data": "kode" },
                        { "data": "tarif_pelayanan" },
                        { 
                            "data": "tarif",
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        }
                    ],
                    "drawCallback": function (settings) {
                        $('.check').uniform();
                    },
                });
            };
        });
    }

    let getTindakan = (id) => {
        $.getJSON(URL.getTarifPelayanan.replace(':Q', btoa(id)), (res, status) => {
            if (status === 'success') {
                let isExists = false;
                $(tableTindakan + " tbody").find('tr').each((i, el) => {
                    if ($(el).data('tarif_pelayanan_uid') == res.data.tarif_pelayanan_uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih tindakan ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                addTindakan(res.data, "add");
            };
        });
    }

    let addTindakan = (obj, listMode) => {
        let tbody = $(tableTindakan + ' tbody');

        if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('tarif_pelayanan_uid', obj.tarif_pelayanan_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.tarif_pelayanan)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputTindakanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_tindakan_id[]')
            .val(obj.tindakan_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_id[]')
            .val(obj.tarif_pelayanan_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_uid[]')
            .val(obj.tarif_pelayanan_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan[]')
            .val(obj.tarif_pelayanan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.tarif).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_tarif[]')
            .val(obj.tarif)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'tarif_pelayanan_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.tarif * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        
        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);

            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="tarif_pelayanan_detail_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputQty.focus();
    }

    $(formPlanning).find("#btn-tambah_tindakan").on('click', () => {
        fetchTindakan();
        $(modalTindakan).modal('show');
    });

    // MODAL Handler
    $('#table-modal_tarif_pelayanan').on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListTindakan.row(tr).data();
        data.checked = $(this).prop('checked');
    });

    $("#btn-tambah_modal_tarif_pelayanan").on('click', () => {
        tableListTindakan.rows().nodes().each(function (i, dt_index) {
            let trData = tableListTindakan.row(dt_index).data();
            if(trData.checked) 
                getTindakan(trData.id);
        });
        $(modalTindakan).modal('hide');
    });

    // BMHP
    let fetchBmhp = () => {
        let layanan_id = btoa($(formPlanning).find('#layanan_id').val());

        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_bmhp'));
        if(isDTable === true) $('#table-modal_bmhp').DataTable().destroy();

        $.getJSON(URL.fetchBmhp.replace(':LAYANAN_ID', layanan_id), (res, status) => {
            if (status === 'success') {
                tableListBmhp = $('#table-modal_bmhp').DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": res.data,
                    "columns": [
                        { 
                            "data": "id",
                            "render": function (data, type, row, meta) {
                                let dataAttr = [
                                                `data-id="${data}"`,
                                                `data-barang_id="${row.barang_id}"`,
                                                `data-barang_uid="${row.barang_uid}"`,
                                                `data-barang="${row.barang}"`,
                                                `data-satuan_id="${row.satuan_id}"`,
                                                `data-satuan="${row.satuan}"`,
                                                `data-harga="${row.harga}"`,
                                                `data-harga_dasar="${row.harga_dasar}"`,
                                                `data-stock="${row.stock_tmp}"`,
                                                ];
                                return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
                            },
                        },
                        { "data": "kode_barang" },
                        { "data": "barang" },
                        { "data": "satuan" },
                        { 
                            "data": "harga",
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        },
                        { "data": "stock" },
                    ],
                    "drawCallback": function (settings) {
                        $('.check').uniform();
                    },
                });
            };
        });
    }

    let addBmhp = (obj, listMode) => {
        let tbody = $(tableBmhp + ' tbody');

        if(tbody.find('input[name="bmhp_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('bmhp_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputTindakanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_tindakan_id[]')
            .val(obj.tindakan_id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdPaket = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelPaket = $("<span/>")
            .html(obj.paket == 1 ? '<i class="fa fa-check"></i>' : '&mdash;')
            .appendTo(tdPaket);
        let divInputPaket = $("<div/>")
            .appendTo(tdPaket);
        let labelInputPaket = $("<label/>")
            .appendTo(divInputPaket);
        let inputPaket = $("<input/>")
            .prop('type', 'checkbox')
            .prop('checked', obj.paket == 1 ? true : false)
            .addClass('check')
            .appendTo(labelInputPaket);
        let inputHiddenPaket = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'bmhp_paket[]')
            .val(obj.paket)
            .appendTo(tdPaket);
        inputPaket.uniform({radioClass: 'choice'});
        divInputPaket.hide();

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
                labelPaket.hide();
                divInputPaket.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelPaket.show();
                divInputPaket.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="bmhp_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelPaket.show();
                divInputPaket.hide();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');


        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        inputPaket.on('change click', function () {
            labelPaket.html($(this).prop('checked') ? '<i class="fa fa-check"></i>' : '&mdash;');
            inputHiddenPaket.val($(this).prop('checked') ? 1 : 0);
        });

        dispInputQty.focus();
    }

    $(formPlanning).find("#btn-tambah_bmhp").on('click', () => {
        fetchBmhp();
        $(modalBmhp).modal('show');
    });

    // MODAL BMHP Handler
    $('#table-modal_bmhp').on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListBmhp.row(tr).data();
        data.checked = $(this).prop('checked');
    });

    $("#btn-tambah_modal_bmhp").on('click', () => {
        tableListBmhp.rows().nodes().each(function (i, dt_index) {
            let trData = tableListBmhp.row(dt_index).data();
            if(trData.checked) {
                let id = trData.id;
                let barang_id = trData.barang_id;
                let barang_uid = trData.barang_uid;
                let barang = trData.barang;
                let satuan_id = trData.satuan_id;
                let satuan = trData.satuan;
                let harga = trData.harga;
                let harga_dasar = trData.harga_dasar;
                let stock = trData.stock_tmp;

                let isExists = false;
                $(tableBmhp + " tbody").find('tr').each((i, el) => {
                    if ($(el).data('bmhp_uid') == barang_uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    tindakan_id: 0,
                    stock_id: id,
                    stock: stock,
                    obat_id: barang_id,
                    obat_uid: barang_uid,
                    obat: barang,
                    satuan_id: satuan_id,
                    satuan: satuan,
                    harga: harga,
                    harga_dasar: harga_dasar,
                    old_quantity: 0,
                    quantity: 1,
                    paket: 0,
                };
                addBmhp(data, "add");
            }
        });

        $(modalBmhp).modal('hide');
    });

    // NON RACIKAN
    let fetchObat = () => {
        blockElement(modalObat + ' .modal-dialog');
        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_obat'));
        if(isDTable === true) $('#table-modal_obat').DataTable().destroy();

        let farmasi_unit = $('#modal-obat_farmasi_unit').val();
        let cara_bayar_id = btoa($(formPlanning).find('#cara_bayar_id').val());
        
        $.getJSON(URL.fetchObat.replace(':FARMASI_UNIT', farmasi_unit).replace(':CARA_BAYAR_ID', cara_bayar_id), (res, status) => {
            if (status === 'success') {
                tableListObat = $('#table-modal_obat').DataTable({
                    "ordering": false,
                    "processing": true,
                    "data": res.data,
                    "columns": [
                        { 
                            "data": "id",
                            "render": function (data, type, row, meta) {
                                let dataAttr = [
                                                `data-id="${data}"`,
                                                `data-barang_id="${row.barang_id}"`,
                                                `data-barang_uid="${row.barang_uid}"`,
                                                `data-barang="${row.barang}"`,
                                                `data-satuan_id="${row.satuan_id}"`,
                                                `data-satuan="${row.satuan}"`,
                                                `data-harga="${row.harga}"`,
                                                `data-harga_dasar="${row.harga_dasar}"`,
                                                `data-stock="${row.stock_tmp}"`,
                                                ];
                                return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
                            },
                        },
                        { "data": "kode_barang" },
                        { "data": "barang" },
                        { "data": "satuan" },
                        { 
                            "data": "harga",
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        },
                        { "data": "stock_tmp" },
                    ],
                    "drawCallback": function (settings) {
                        $('.check').uniform();
                    },
                });
                setTimeout(() => {
                    $(modalObat + ' .modal-dialog').unblock();
                }, 500);
            };
        });
    }

    let addNonRacikan = (obj, listMode) => {
        obj.browse = 'non_racikan';
        listResepObat.push(obj);
        let tbody = $(tableNonRacikan + ' tbody');

        if(tbody.find('input[name="non_racikan_detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('non_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_detail_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_id[]')
            .addClass('input-non_racikan_obat_id')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan_id[]')
            .addClass('input-non_racikan_satuan_id')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_qty[]')
            .addClass('input-non_racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdSigna = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let labelSigna = $("<span/>")
            .html(obj.label_signa)
            .appendTo(tdSigna);
        let inputTextSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'non_racikan_signa[]')
            .prop('value', obj.signa)
            .appendTo(tdSigna);
        let inputSigna = $("<select/>")
            .addClass('form-control')
            .prop('multiple', 'multiple')
            .appendTo(tdSigna);
        inputSigna.select2({
            placeholder: "- Pilih -",
        });
        inputSigna.hide().next(".select2-container").hide();

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
                labelSigna.hide();
                inputSigna.show().next(".select2-container").show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
                labelSigna.show();
                inputSigna.hide().next(".select2-container").hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) listResepObat.splice(idxLro, 1);

                if(tbody.find('input[name="non_racikan_obat_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelSignaArr = [];
                inputSigna.find('option:selected').each(function() {
                    if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                        labelSignaArr.push($(this).html());
                });

                labelSigna.html('&mdash;');
                let tmpLabel = '&mdash;'
                if(labelSignaArr.length > 0) {
                    tmpLabel = `<ul class="text-left">`;
                    for(let i = 0; i < labelSignaArr.length; i++) {
                        tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                    }
                    tmpLabel += `</ul>`;
                    labelSigna.html(tmpLabel);
                }

                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) {
                    listResepObat[idxLro].quantity = inputQty.val();
                    listResepObat[idxLro].label_signa = tmpLabel;
                }

                labelQty.show();
                dispInputQty.hide();
                labelSigna.show();
                inputSigna.hide().next(".select2-container").hide();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        if(listMode === "view_noedit") tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());
        }
        initializeSigna(inputSigna, obj.signa);

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        inputSigna.on('change blur', (e) => {
            inputTextSigna.val(inputSigna.val());
        });

        dispInputQty.focus();

    }

    $(formPlanning).find("#btn-tambah_non_racikan").on('click', () => {
        $('#modal-obat-input_mode').val('non_racikan');
        fetchObat();
        $(modalObat).modal('show');
    });

    // RACIKAN
    let addRacikan = (obj, listMode) => {
        obj.browse = 'racikan';
        listResepObat.push(obj);
        let tbody = $(tableRacikan + ' tbody');

        if(tbody.find('.input-racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('racikan_uid', obj.uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .appendTo(tr);
        let labelNama = $("<span>")
            .html(obj.nama + obj.label_list_obat)
            .addClass('label-racikan_nama')
            .appendTo(tdNama);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_id[]')
            .addClass('input-racikan_id')
            .val(obj.id ? obj.id : 0)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_nama')
            .prop('name', 'racikan_nama[]')
            .val(obj.nama)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-racikan_uid')
            .prop('name', 'racikan_uid[]')
            .val(obj.uid)
            .appendTo(tdNama);
        let inputListObat = $("<textarea/>")
            .addClass('input-racikan_list_obat')
            .prop('name', 'racikan_list_obat[]')
            .val(json_encode(obj.list_obat))
            .html(json_encode(obj.list_obat))
            .appendTo(tdNama);
        inputListObat.hide();

        let tdMetode = $("<td/>")
            .appendTo(tr);
        let labelMetode = $("<span>")
            .html(obj.label_metode)
            .addClass('label-racikan_metode')
            .appendTo(tdMetode);
        let inputMetode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_metode[]')
            .addClass('input-racikan_metode')
            .val(obj.metode)
            .appendTo(tdMetode);

        let tdCaraBuat = $("<td/>")
            .appendTo(tr);
        let labelCaraBuat = $("<span>")
            .html(obj.label_cara_buat)
            .addClass('label-racikan_cara_buat')
            .appendTo(tdCaraBuat);
        let inputCaraBuat = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_cara_buat[]')
            .addClass('input-racikan_cara_buat')
            .val(obj.cara_buat)
            .appendTo(tdCaraBuat);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .addClass('label-racikan_harga')
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_harga[]')
            .addClass('input-racikan_harga')
            .val(obj.harga)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .addClass('label-racikan_qty')
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_qty[]')
            .addClass('input-racikan_qty')
            .val(obj.quantity)
            .appendTo(tdQty);

        let tdSigna = $("<td/>")
            .appendTo(tr);
        let labelSigna = $("<span>")
            .html(obj.label_signa)
            .addClass('label-racikan_signa')
            .appendTo(tdSigna);
        let inputSigna = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'racikan_signa[]')
            .addClass('input-racikan_signa')
            .val(obj.signa)
            .appendTo(tdSigna);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                $('#modal-input_racikan_uid').val(obj.uid);
                $(formRacikan).find('#modal-input_racikan_nama').val(obj.nama);
                $(formRacikan).find('#modal-input_racikan_metode').val(obj.metode).change();
                $(formRacikan).find('#modal-input_racikan_cara_buat').val(obj.metode).change();
                $(formRacikan).find('#modal-input_racikan_harga').autoNumeric('set', obj.harga);
                $(formRacikan).find('#modal-input_racikan_qty').autoNumeric('set', obj.quantity);
                initializeSigna($(formRacikan).find('#modal-input_racikan_signa'), inputSigna.val());

                $(tableRacikanDetail + ' tbody').empty();
                let listObat = obj.list_obat;
                for(let i = 0; i < listObat.length; i++) 
                    addRacikanDetail(listObat[i], "view");

                $(modalRacikan).modal('show');
            });

            btnDel.on('click', (e) => {
                tr.remove();
                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(obj.uid);
                if(idxLro !== -1) listResepObat.splice(idxLro, 1);

                if(tbody.find('.input-racikan_id').length <= 0) 
                    tbody.append(`<tr><td colspan="7" class="text-center">Tidak ada data</td></tr>`);

            });
        } else tdAction.html('&mdash;');

        if(listMode === "view_noedit") tdAction.html('&mdash;');
    }

    let addRacikanDetail = (obj, listMode) => {
        let tbody = $(tableRacikanDetail + ' tbody');

        if(tbody.find('.input-item_racikan_id').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('item_racikan_obat_uid', obj.obat_uid)
            .appendTo(tbody);

        let tdNama = $("<td/>")
            .html(obj.obat)
            .appendTo(tr);
        let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .addClass('input-item_racikan_id')
            .prop('name', 'item_racikan_id[]')
            .val(obj.id)
            .appendTo(tdNama);
        let inputStockId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_stock_id[]')
            .val(obj.stock_id)
            .appendTo(tdNama);
        let inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_id[]')
            .val(obj.obat_id)
            .appendTo(tdNama);
        let inputNama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat[]')
            .val(obj.obat)
            .appendTo(tdNama);
        let inputUid = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_obat_uid[]')
            .val(obj.obat_uid)
            .appendTo(tdNama);
        let inputSatuanId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan_id[]')
            .val(obj.satuan_id)
            .appendTo(tdNama);
        let inputSatuan = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_satuan[]')
            .val(obj.satuan)
            .appendTo(tdNama);

        let tdHarga = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelHarga = $("<span>")
            .html('Rp.' + numeral(obj.harga).format())
            .appendTo(tdHarga);
        let inputHarga = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga[]')
            .val(obj.harga)
            .appendTo(tdHarga);
        let inputHargaDasar = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_harga_dasar[]')
            .val(obj.harga_dasar)
            .appendTo(tdHarga);

        let tdQty = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQty = $("<span>")
            .html(numeral(obj.quantity).format())
            .appendTo(tdQty);
        let inputOldQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_old_qty[]')
            .val(obj.old_quantity)
            .appendTo(tdQty);
        let inputQty = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'item_qty[]')
            .val(obj.quantity)
            .appendTo(tdQty);
        let dispInputQty = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right')
            .appendTo(tdQty);
        dispInputQty.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQty.autoNumeric('set', obj.quantity).hide();

        let tdTotal = $("<td/>")
            .addClass('text-right')
            .html('Rp.' + numeral(obj.harga * obj.quantity).format())
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDone = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-success btn-xs')
                .html('<i class="fa fa-check"></i>')
                .appendTo(tdAction);
            btnDone.hide();
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);
            
            // Handler
            tr.on('click', (e) => {
                if (tr.data('done') == 1) {
                    tr.data('done', 0);
                    return;
                }
                tbody.find('tr').each((i, el) => {
                    if ($(el).data('uid') != tr.data('uid')) {
                        $(el).trigger('input_close');
                    }
                });

                btnDone.show();
                btnDel.hide();

                labelQty.hide();
                dispInputQty.show();
            }).on('input_close', () => {
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();
            });

            btnDel.on('click', (e) => {
                tr.remove();
                updateTotal();
                if(tbody.find('.input-item_racikan_id').length <= 0) 
                    tbody.append(`<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>`);

            });

            btnDone.on('click', (e) => {
                tr.data('done', 1);
                btnDone.hide();
                btnDel.show();

                labelQty.show();
                dispInputQty.hide();

                updateTarifRow();
            });

            if(listMode === "add") tr.click();
        } else tdAction.html('&mdash;');

        function updateTarifRow() {
            let quantity = parseFloat(inputQty.val());
            let tarif = parseFloat(inputHarga.val());
            let total = tarif * quantity;
            tdTotal.html('Rp.' + numeral(total).format());

            updateTotal();
        }

        function updateTotal() {
            let total = 0;
            $(tableRacikanDetail + " tbody").find('tr').each((i, el) => {
                total += numeral($(el).find('td:eq(3)').html())._value;
            });
            $("#modal-input_racikan_harga").autoNumeric('set', total);
        }

        dispInputQty.on('keyup change blur', (e) => {
            let val = dispInputQty.autoNumeric('get') == "" ? 0 : dispInputQty.autoNumeric('get');
            if(parseFloat(val) > parseFloat(obj.stock)) {
                val = 1;
                warningMessage('Peringatan', `Stock yang tersedia saat ini adalah ${obj.stock}`);
            }
            dispInputQty.autoNumeric('set', val);
            inputQty.val(val);
            labelQty.html(val);

            updateTarifRow();
        });

        dispInputQty.focus();
    }

    $(formPlanning).find("#btn-tambah_racikan").on('click', () => {
        $(tableRacikanDetail + ' tbody').empty();
        $(modalRacikan).find('input, select').val('').trigger('change');
        $(modalRacikan).modal('show');
    });

    $(formRacikan).find("#btn-tambah_racikan_detail").on('click', () => {
        $('#modal-obat-input_mode').val('racikan');
        fetchObat();
        $(modalObat).modal('show');
    });

    // MODAL OBAT Handler
    $('#modal-obat_farmasi_unit').on('change', '', function() {
        fetchObat();
    });

    $('#table-modal_obat').on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListObat.row(tr).data();
        data.checked = $(this).prop('checked');
    });

    $("#btn-tambah_modal_obat").on('click', () => {
        let mode = $('#modal-obat-input_mode').val();
        tableListObat.rows().nodes().each(function (i, dt_index) {
            let trData = tableListObat.row(dt_index).data();
            if(trData.checked) {
                let id = trData.id;
                let barang_id = trData.barang_id;
                let barang_uid = trData.barang_uid;
                let barang = trData.barang;
                let satuan_id = trData.satuan_id;
                let satuan = trData.satuan;
                let harga = trData.harga;
                let harga_dasar = trData.harga_dasar;
                let stock = trData.stock_tmp;

                let isExists = false;
                let param = {
                    table: tableRacikanDetail,
                    label: 'item_racikan_obat_uid'
                };
                if(mode === "non_racikan") {
                    param = {
                        table: tableNonRacikan,
                        label: 'non_racikan_obat_uid'
                    };
                }

                $(param.table + " tbody").find('tr').each((i, el) => {
                    if ($(el).data(param.label) == barang_uid) 
                        isExists = $(el);
                });

                if (isExists) {
                    errorMessage('Error', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali !');

                    let bg = isExists.find('td').css('background-color');
                    let highlightBg = 'rgba(255, 0, 0, 0.2)';
                    isExists.find('td').css('background-color', highlightBg);
                    setTimeout(() => {
                        isExists.find('td').css('background-color', bg);
                    }, 1500);
                    return;
                }

                let data = {
                    id: 0,
                    uid: makeid(50),
                    stock_id: id,
                    stock: stock,
                    obat_id: barang_id,
                    obat_uid: barang_uid,
                    obat: barang,
                    satuan_id: satuan_id,
                    satuan: satuan,
                    harga: harga,
                    harga_dasar: harga_dasar,
                    old_quantity: 0,
                    quantity: 1,
                    signa: '',
                    label_signa: '&mdash;',
                };

                if(mode === "non_racikan") {
                    addNonRacikan(data, "add");
                } else addRacikanDetail(data, "add");
            }
        });

        $(modalObat).modal('hide');
    });

    $(formRacikan).validate({
        rules: {
            nama: { required: true },
            metode: { required: true },
            cara_buat: { required: true },
            harga: { required: true },
            quantity: { required: true },
            "signa[]": { required: true },
        },
        messages: {
            //
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockElement(modalRacikan + ' .modal-dialog');
            let postData = $(formRacikan).serializeArray();

            let data = {};
            let d, fieldname, isArray, tmpLabel;
            for (let i = 0; i < postData.length; i++) {
                d = postData[i];
                isArray = false;

                if (d.name.search(/\[\]/) !== -1) {
                    fieldname = d.name.replace(/\[\]/, '');
                    isArray = true;
                } else {
                    fieldname = d.name;
                    isArray = false;
                }

                if (isArray) {
                    if (! data[fieldname]) {
                        data[fieldname] = [];
                        data[fieldname].push(d.value);
                    } else {
                        data[fieldname].push(d.value)
                    }
                } else {
                    data[fieldname] = d.value;
                }
            }

            data['label_metode'] = $('#modal-input_racikan_metode').find('option:selected').html();
            data['label_cara_buat'] = $('#modal-input_racikan_cara_buat').find('option:selected').html();
            data['harga'] = $('#modal-input_racikan_harga').autoNumeric('get');
            data['quantity'] = $('#modal-input_racikan_qty').autoNumeric('get');

            let labelSignaArr = [];
            $('#modal-input_racikan_signa').find('option:selected').each(function() {
                if(jQuery.inArray($(this).html(), labelSignaArr) === -1)
                    labelSignaArr.push($(this).html());
            });

            data['label_signa'] = '&mdash;';
            if(labelSignaArr.length > 0) {
                tmpLabel = `<ul class="text-left">`;
                for(let i = 0; i < labelSignaArr.length; i++) {
                    tmpLabel += `<li>${labelSignaArr[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_signa'] = tmpLabel;
            }

            let listObat = [];
            let labelListObat = [];
            for (let i = 0; i < data.item_racikan_id.length; i++) {
                listObat.push({
                    id: data.item_racikan_id[i],
                    stock_id: data.item_stock_id[i],
                    obat_id: data.item_obat_id[i],
                    obat_uid: data.item_obat_uid[i],
                    obat: data.item_obat[i],
                    satuan_id: data.item_satuan_id[i],
                    satuan: data.item_satuan[i],
                    harga: data.item_harga[i],
                    harga_dasar: data.item_harga_dasar[i],
                    old_quantity: data.item_old_qty[i],
                    quantity: data.item_qty[i],
                });
                labelListObat.push(`${data.item_obat[i]} X ${data.item_qty[i]} ${data.item_satuan[i]}`);
            }
            data['list_obat'] = listObat;
            if(data['uid'] === "") data['uid'] = makeid(50);

            if(labelListObat.length > 0) {
                tmpLabel = `<ul class="text-left text-size-mini text-slate-300">`;
                for(let i = 0; i < labelListObat.length; i++) {
                    tmpLabel += `<li>${labelListObat[i]}</li>`;
                }
                tmpLabel += `</ul>`;
                data['label_list_obat'] = tmpLabel;
            }

            let isExists = false;
            $(tableRacikan + " tbody").find('tr').each(function() {
                if(data['uid'] === $(this).data('racikan_uid')) 
                    isExists = $(this);
            });

            if(isExists) {
                isExists.find('.input-racikan_list_obat').val(json_encode(data.list_obat)).html(json_encode(data.list_obat));
                isExists.find('.input-racikan_nama').val(data.nama);
                isExists.find('.label-racikan_nama').html(data.nama + data.label_list_obat);
                isExists.find('.input-racikan_metode').val(data.metode);
                isExists.find('.label-racikan_metode').html(data.label_metode);
                isExists.find('.input-racikan_cara_buat').val(data.cara_buat);
                isExists.find('.label-racikan_cara_buat').html(data.label_cara_buat);
                isExists.find('.label-racikan_harga').html('Rp.' + numeral(data.harga).format());
                isExists.find('.input-racikan_harga').val(data.harga);
                isExists.find('.label-racikan_qty').html(numeral(data.quantity).format());
                isExists.find('.input-racikan_qty').val(data.quantity);
                isExists.find('.label-racikan_signa').html(data.label_signa);
                isExists.find('.input-racikan_signa').val(data.signa);

                let idxLro = listResepObat.map(function(e) { return e.uid; }).indexOf(data.uid);
                if(idxLro !== -1) {
                    listResepObat[idxLro].nama = data.nama;
                    listResepObat[idxLro].label_metode = data.label_metode;
                    listResepObat[idxLro].metode = data.metode;
                    listResepObat[idxLro].label_cara_buat = data.label_cara_buat;
                    listResepObat[idxLro].cara_buat = data.cara_buat;
                    listResepObat[idxLro].harga = data.harga;
                    listResepObat[idxLro].quantity = data.qty;
                    listResepObat[idxLro].label_signa = data.label_signa;
                    listResepObat[idxLro].signa = data.signa;
                }
            } else addRacikan(data, "add");
            setTimeout(() => {
                $(modalRacikan + ' .modal-dialog').unblock();
            
                $(modalRacikan).modal('hide');
            }, 400);
        }
    });

    $('#btn-preview_resep').click(function() {
        if(listResepObat.length <= 0) {
            warningMessage('Peringatan', 'Racikan dan Non Racikan belum terisi');
            return;
        }

        dataRegistrasi.resep.list_obat = listResepObat;
        $(modalPreviewResep).find('.label-no_resep').html(dataRegistrasi.resep.nomor);
        $(modalPreviewResep).find('.label-dokter').html(dataRegistrasi.dokter);
        $(modalPreviewResep).find('.label-layanan').html(dataRegistrasi.layanan);
        $(modalPreviewResep).find('.label-regMr').html(`${dataRegistrasi.no_register} / ${dataRegistrasi.pasien.no_rm}`);
        $(modalPreviewResep).find('.label-tanggal').html(moment(dataRegistrasi.resep.tanggal).format('DD-MM-YYYY HH:mm'));
        $(modalPreviewResep).find('.label-pro').html(dataRegistrasi.pasien.nama);
        $(modalPreviewResep).find('.label-umur').html(`${dataRegistrasi.pasien.umur_tahun} Tahun ${dataRegistrasi.pasien.umur_bulan} Bulan ${dataRegistrasi.pasien.umur_hari} Hari`);

        let tmpLabel = `<ul class="text-left" style="list-style-type: none;">`;
        for(let i = 0; i < listResepObat.length; i++) {
            if(listResepObat[i].browse === "non_racikan") {
                tmpLabel += `<li>${listResepObat[i].obat} X ${listResepObat[i].quantity} ${listResepObat[i].satuan}</li>`;
            } else 
                tmpLabel += `<li>${listResepObat[i].nama} - ${listResepObat[i].label_cara_buat}</li>`;

            if(listResepObat[i].label_signa !== "&mdash;")
                tmpLabel += `<li>${listResepObat[i].label_signa}</li>`;
        }
        tmpLabel += `</ul>`;
        $(modalPreviewResep).find('.label-list_obat').html(tmpLabel);

        $(modalPreviewResep).modal('show');
    });

    $(formPlanning).validate({
        rules: {
            diagnosa_utama: { required: true }
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
                url: URL.savePlanning,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    successMessage('Success', "Assesment dan Planning berhasil disimpan.");

                    fillTabPlanning(UID);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
            case '#tab-pemeriksaan':
                fillTabPemeriksaaan(UID);
                break;
            default:
                if(!enablePlanning) {
                    e.preventDefault();
                    errorMessage('Error', "Silahkan isi terlebih dahulu tab pemeriksaan.");   
                    return false;
                }
                fillTabPlanning(UID);
                break;
        }
    });

    fillForm(UID);
});