$(() => {
    $(".styled").uniform({
        radioClass: 'choice'
    });

    let initializeDokter = (el, dokter_id) => {
        $.getJSON(URL.getDokter, function(data, status) {
            if (status === "success") {
                let option = '<option value="">- Pilih -</option>';
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (dokter_id === data.data[i].id) selected = "selected='selected'";
                    option += `<option value="${data.data[i].id}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");
            }
        });
    }

    let fillForm = (uid) => {
        blockPage();
        $.getJSON(URL.getDataRegistrasi.replace(':UID', uid), function (res, status) {
            if (status === 'success') {
                $(".div-label_perusahaan").hide();
                $(".div-label_no_jaminan").hide();
                $(".div-label_penjamin_perusahaan").hide();
                $(".div-label_penjamin_no_jaminan").hide();
                $(".div-detail_pernah_dirawat").hide();
                $(".div-alergi_ke").hide();
                $(".div-alergi_ke_lainnya").hide();
                $(".div-status_ekonomi_lainnya").hide();
    
                let data = res.data;

                $("input[name=pelayanan_id]").val(data.id);
                $(".label-nama_pasien").html(data.pasien.nama);
                $(".label-no_rm").html(data.pasien.no_rm);
                $(".label-jenis_kelamin").html(parseInt(data.pasien.jenis_kelamin) === 1 ? 'Laki-laki' : 'Perempuan');
                $(".label-tanggal_lahir").html(data.pasien.tanggal_lahir);
                $(".label-umur").html(`${data.pasien.umur_tahun} Tahun ${data.pasien.umur_bulan} Bulan ${data.pasien.umur_hari} Hari`);
                $(".label-golongan_darah").html(data.pasien.golongan_darah_desc);
                $(".label-alamat").html(data.pasien.alamat);
                $(".label-no_telepon").html(data.pasien.no_telepon_1);

                $(".label-no_register").html(data.no_register);
                $(".label-tanggal").html(moment(data.tanggal).format('DD-MM-YYYY HH:mm'));
                $(".label-layanan").html(data.layanan);
                $(".label-dokter").html(data.dokter);
                $(".label-cara_bayar").html(data.cara_bayar);
                $(".label-perusahaan").html(data.perusahaan);
                $(".label-no_jaminan").html(data.no_jaminan);
                $(".label-penjamin_perusahaan").html(data.penjamin_perusahaan);
                $(".label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

                switch (parseInt(data.cara_bayar_jenis)) {
                    case imediscode.CARA_BAYAR_BPJS:
                        $(".div-label_no_jaminan").show().children('label').html('No. SEP');

                        if(data.penjamin_id) {
                            $(".div-label_penjamin_perusahaan").show();
                            $(".div-label_penjamin_no_jaminan").show();

                            let labelPerusahaan = 'Perusahaan';
                            let labelNoJaminan = 'NIK';
                            if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
                                labelPerusahaan = 'Asuransi';
                                labelNoJaminan = 'No. Anggota';
                            }
                            $(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
                            $(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
                        }
                        break;
                    case imediscode.CARA_BAYAR_JAMKESDA:
                        $(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
                        break;
                    case imediscode.CARA_BAYAR_ASURANSI:
                    case imediscode.CARA_BAYAR_PERUSAHAAN:
                        $(".div-label_perusahaan").show();
                        $(".div-label_no_jaminan").show();

                        let labelPerusahaan = 'Perusahaan';
                        let labelNoJaminan = 'NIK';
                        if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
                            labelPerusahaan = 'Asuransi';
                            labelNoJaminan = 'No. Anggota';
                        }
                        $(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
                        $(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
                        break;
                    case imediscode.CARA_BAYAR_INTERNAL:
                        $(".div-label_no_jaminan").show().children('label').html('NIK');
                        break;
                }


                $.unblockUI();

                fillDetail(uid);
            }
        });

        $.uniform.update();
    }

    let fillDetail = (pelayanan_uid) => {
        blockElement(tableDetail);
        $.getJSON(URL.getDataCppt.replace(':PELAYANAN_UID', pelayanan_uid), function (res, status) {
            if (status === 'success') {
                let data = res.data;
                if(data) {
                    $("input[name=id]").val(data.id);
                    $("input[name=uid]").val(data.uid);

                    for (var i = 0; i < data.details.length; i++) {
                        addDetail(data.details[i]);
                    }

                    if(MODE === "view") {
                        $(".btn-save").hide();
                        $(btnTambahDetail).parents('tr').hide();
                        $("input[type=text], textarea, select").prop("disabled", true);
                    }
                }
                $(tableDetail).unblock();
            }
        });
    }

    // BMHP
    let addDetail = (obj) => {
        let tanggal = moment(obj.tanggal).isValid() ? moment(obj.tanggal).format('DD/MM/YYYY') : moment().format('DD/MM/YYYY');
        let jam = moment(tanggal + ' ' + obj.jam).isValid() ? moment(tanggal + ' ' + obj.jam).format('HH:mm') : moment().format('HH:mm');

        let tbody = $(tableDetail + ' tbody');

        if(tbody.find('input[name="detail_id[]"]').length <= 0) 
            tbody.empty();

        let tr = $("<tr/>")
            .data('detail_id', obj.id)
            .appendTo(tbody);

        let tdTanggal = $("<td/>")
            .appendTo(tr);
            let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_id[]')
            .val(obj.id)
            .appendTo(tdTanggal);
            let inputTanggal = $("<input/>")
                .addClass('form-control')
                .prop('type', 'text')
                .prop('name', 'detail_tanggal[]')
                .prop('placeholder', 'DD/MM/YYYY')
                .val(tanggal)
                .appendTo(tdTanggal);
        inputTanggal.formatter({ pattern: '{{99}}/{{99}}/{{9999}}' });

        let tdJam = $("<td/>")
            .appendTo(tr);
            let inputJam = $("<input/>")
                .addClass('form-control')
                .prop('type', 'text')
                .prop('name', 'detail_jam[]')
                .prop('placeholder', 'HH:MM')
                .val(jam)
                .appendTo(tdJam);
        inputJam.formatter({ pattern: '{{99}}:{{99}}' });

        let tdDokter = $("<td/>")
            .appendTo(tr);
            let inputDokter = $("<select/>")
                .prop('name', 'detail_dokter_id[]')
                .addClass('form-control')
                .appendTo(tdDokter);
        inputDokter.select2();

        let tdHasilPemeriksaan = $("<td/>")
            .appendTo(tr);
            let divS = $("<div/>")
                .addClass('row col-lg-12 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelS = $("<div/>")
                    .addClass('col-xs-1')
                    .html('S')
                    .appendTo(divS);
                let divInputS = $("<div/>")
                    .addClass('col-xs-11')
                    .appendTo(divS);
                    let inputS = $("<textarea/>")
                        .addClass('form-control')
                        .prop('name', 'detail_s[]')
                        .val(obj.s)
                        .html(obj.s)
                        .appendTo(divInputS);
            let divO = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelO = $("<div/>")
                    .addClass('col-xs-1')
                    .html('O')
                    .appendTo(divO);
                let divInputO = $("<div/>")
                    .addClass('col-xs-11')
                    .appendTo(divO);
                    let inputO = $("<textarea/>")
                        .addClass('form-control')
                        .prop('name', 'detail_o[]')
                        .val(obj.o)
                        .html(obj.o)
                        .appendTo(divInputO);

            let divA = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelA = $("<div/>")
                    .addClass('col-xs-1')
                    .html('A')
                    .appendTo(divA);
                let divInputA = $("<div/>")
                    .addClass('col-xs-11')
                    .appendTo(divA);
                    let inputA = $("<textarea/>")
                        .addClass('form-control')
                        .prop('name', 'detail_a[]')
                        .val(obj.a)
                        .html(obj.a)
                        .appendTo(divInputA);

            let divP = $("<div/>")
                .addClass('row col-lg-12 mt-10 no-padding')
                .appendTo(tdHasilPemeriksaan);
                let divlabelP = $("<div/>")
                    .addClass('col-xs-1')
                    .html('P')
                    .appendTo(divP);
                let divInputP = $("<div/>")
                    .addClass('col-xs-11')
                    .appendTo(divP);
                    let inputP = $("<textarea/>")
                        .addClass('form-control')
                        .prop('name', 'detail_p[]')
                        .val(obj.p)
                        .html(obj.p)
                        .appendTo(divInputP);

        let tdVerifikasi = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
            let divInputVerifikasi = $("<div/>")
                .appendTo(tdVerifikasi);
                let labelInputVerifikasi = $("<label/>")
                    .appendTo(divInputVerifikasi);
                    let inputVerifikasi = $("<input/>")
                        .prop('type', 'checkbox')
                        .prop('checked', obj.verifikasi_dpjp == 1 ? true : false)
                        .addClass('check')
                        .appendTo(labelInputVerifikasi);
            let inputHiddenVerifikasi = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'detail_verifikasi_dpjp[]')
                .val(obj.verifikasi_dpjp)
                .appendTo(tdVerifikasi);
        inputVerifikasi.uniform({radioClass: 'choice'});

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);

        if(MODE !== "view") {
            let btnDel = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-danger btn-xs btn-b')
                .html('<i class="fa fa-trash-o"></i>')
                .appendTo(tdAction);

            btnDel.on('click', (e) => {
                tr.remove();
                if(tbody.find('input[name="detail_id[]"]').length <= 0) 
                    tbody.append(`<tr><td colspan="6" class="text-center">Tidak ada data</td></tr>`);
            });
        } else tdAction.html('&mdash;');

        inputVerifikasi.on('change click', function () {
            inputHiddenVerifikasi.val($(this).prop('checked') ? 1 : 0);
        });

        initializeDokter(inputDokter, obj.dokter_id);
        inputTanggal.focus();
    }

    $(btnTambahDetail).click(function (e) {
        addDetail({
            id: 0,
            tanggal: moment(),
            jam: "",
            dokter_id: "",
            s: "",
            o: "",
            a: "",
            p: "",
            verifikasi_dpjp: 0,
        })
    });

    $(form).validate({
        rules: {
            'detail_tanggal[]': { required: true },
            'detail_jam[]': { required: true },
            'detail_dokter_id[]': { required: true },
        },
        messages: {
            'detail_tanggal[]': { required: 'Tanggal Diperlukan' },
            'detail_jam[]': { required: 'Jam Diperlukan' },
            'detail_dokter_id[]': { required: 'Dokter Diperlukan' },
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            $(element).parent().append("<br/>").append(error);
        },
        submitHandler: function (form) {

            if($('input[name="detail_id[]"]').length <= 0) {
                $(btnTambahDetail).focus();
                warningMessage('Peringatan!', "Detail CPPT harus diisi");
                return;
            }

            $('input, textarea, select').prop('disabled', false);

            var postData = $(form).serializeArray();
            var formData = new FormData($(form)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            blockPage();
            $.ajax({
                url: URL.save,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $.unblockUI();
                    $('.btn-save').hide();
                    successMessage('Success', "CPPT berhasil disimpan.");

                    setTimeout(() => {
                        window.location.assign($('.btn-cancel').attr('href'));
                    }, 1000);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
                }
            });
        }
    });

    fillForm(UID);
});