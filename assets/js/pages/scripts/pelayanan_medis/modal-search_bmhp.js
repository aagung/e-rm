$(() => {
	let EL = {
		table: '#table-modal_bmhp',
	};

	let fetchBmhp = () => {
        blockElement(modalBmhp + ' .modal-dialog');

        let isDTable = $.fn.dataTable.isDataTable($(EL.table));
        if(isDTable === false) {
        	tableListBmhp = $(EL.table).DataTable({
	            "processing": true,
				"serverSide": true,
				"ordering": false,
				"ajax": {
					"url": urlFetchBmhp.replace(':LAYANAN_ID', $(modalBmhpLayananId).val()).replace(':RUANG_ID', $(modalBmhpRuangId).val()),
					"type": "POST",
				},
	            "columns": [
                        { 
                            "data": "id",
                            "render": function (data, type, row, meta) {
                                let dataAttr = [
                                                `data-id="${data}"`,
                                                `data-barang_id="${row.barang_id}"`,
                                                `data-barang_uid="${row.barang_uid}"`,
                                                `data-barang="${row.barang}"`,
                                                `data-satuan_id="${row.satuan_id}"`,
                                                `data-satuan="${row.satuan}"`,
                                                `data-harga="${row.harga}"`,
                                                `data-harga_dasar="${row.harga_dasar}"`,
                                                `data-stock="${row.stock_tmp}"`,
                                                ];
                                return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
                            },
                        },
                        { "data": "kode_barang" },
                        { "data": "barang" },
                        { "data": "satuan" },
                        { 
                            "data": "harga",
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        },
                        { "data": "stock" },
                ],
	            "drawCallback": function (settings) {
	                let tr = $(EL.table).find("tbody tr");
					tr.each(function(el) {
						let data = tableListBmhp.row(el).data();
						if(data) {
							for (var val in selectedBmhp) {
								if(data.barang_uid === val) {
									let cbEl = $(this).find('.check');
									cbEl.prop('checked', true);
								}
							}
						}
					});

	                $('.check').uniform();
	            },
	        });
        } else tableListBmhp.draw();

        setTimeout(() => {
            $(modalBmhp + ' .modal-dialog').unblock();
        }, 300);
    }

    $(EL.table).on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListBmhp.row(tr).data();
        
        if($(this).prop('checked') === true) {
        	selectedBmhp[data.barang_uid] = data;
        } else {
        	delete selectedBmhp[data.barang_uid];
        }
    });

    $(modalBmhp).on('show.bs.modal', function () {
    	console.log('Empty selectedBmhp');
    	fetchBmhp();
        selectedBmhp = [];
    });
});