$(() => {
	let EL = {
		table: '#table-modal_tarif_pelayanan',
	};

	let fetchTindakan = () => {
        blockElement(modalTindakan + ' .modal-dialog');

        let isDTable = $.fn.dataTable.isDataTable($(EL.table));
        if(isDTable === false) {
        	tableListTindakan = $(EL.table).DataTable({
	            "processing": true,
				"serverSide": true,
				"ordering": false,
				"ajax": {
					"url": urlTindakan.fetchData,
                    "data": function(p) {
                        p.layanan_id = $(modalTindakanLayananId).val();
                        p.cara_bayar_id = $(modalTindakanCaraBayarId).val();
                        p.perusahaan_id = $(modalTindakanPerusahaanId).val();
                        p.kelas_id = $(modalTindakanKelasId).val();
                        p.golongan_operasi = $(modalTindakanGolonganOperasi).val();
                    },
					"type": "POST",
				},
	            "columns": [
                        { 
                            "data": "id",
                            "searchable": false,
                            "render": function (data, type, row, meta) {
                                let dataAttr = [
                                                `data-id="${data}"`,
                                                `data-uid="${row.uid}"`,
                                                `data-kode="${row.kode}"`,
                                                `data-tarif_pelayanan="${row.nama}"`,
                                                ];
                                return `<div class="checkbox"><label ${dataAttr.join(" ")}><input type="checkbox" class="check" /></label></div>`;
                            },
                        },
                        { 
                            "data": "kode",
                            "searchable": false,
                        },
                        { 
                            "data": "nama",
                            "name": "b.nama",
                        },
                        { 
                            "data": "tarif",
                            "searchable": false,
                            "render": function (data, type, row, meta) {
                                return numeral(data).format('0.0,');
                            },
                            "className": "text-right"
                        },
                ],
	            "drawCallback": function (settings) {
	                let tr = $(EL.table).find("tbody tr");
					tr.each(function(el) {
						let data = tableListTindakan.row(el).data();
						if(data) {
							for (var val in selectedTindakan) {
								if(data.uid === val) {
									let cbEl = $(this).find('.check');
									cbEl.prop('checked', true);
								}
							}
						}
					});

	                $('.check').uniform();
	            },
	        });
        } else tableListTindakan.draw();

        setTimeout(() => {
            $(modalTindakan + ' .modal-dialog').unblock();
        }, 300);
    }

    $(EL.table).on('click', 'input[type=checkbox]', function() {
        let tr = $(this).closest('tr');
        let data = tableListTindakan.row(tr).data();
        
        if($(this).prop('checked') === true) {
        	selectedTindakan[data.uid] = data;
        } else {
        	delete selectedTindakan[data.uid];
        }
    });

    $(modalTindakan).on('show.bs.modal', function () {
    	console.log('Empty selectedTindakan');
    	fetchTindakan();
        selectedTindakan = [];
    });
});