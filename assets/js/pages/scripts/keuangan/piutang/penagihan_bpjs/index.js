var Index = function () {
	
	var oPemberkasanTable = null;
	var oKodingTable = null;
	var oDaftarVerifikasiTable = null;
	var oDaftarTagihanTable = null;
	var oDaftarLayakTable = null;
	var oDaftarTidakLayakTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handlePemberkasanTable = function() {

		oPemberkasanTable = $('#pemberkasan_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_pemberkasan,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_sep", "name": "no_sep"},
				{"data": "tanggal", "name": "tanggal"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "asal_pasien", "name": "asal_pasien"},
				{"data": "layanan", "name": "layanan"},
				{"data": "diagnosa", "name": "diagnosa"},
				{"data": "jml_tagihan", "name": "jml_tagihan"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<button type="button" class="bg-info btn-xs verify-button" data-uid="' + row.uid + '" title="Verifikasi kelengkapan Berkas">Verify</button>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadPemberkasan = function() {
        if (oPemberkasanTable == null) {
			handlePemberkasanTable();
        }
		else {
			oPemberkasanTable.ajax.reload();
		}
    };
	
	var handleKodingTable = function() {

		oKodingTable = $('#koding_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_koding,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_sep", "name": "no_sep"},
				{"data": "tanggal", "name": "tanggal"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "asal_pasien", "name": "asal_pasien"},
				{"data": "layanan", "name": "layanan"},
				{"data": "diagnosa", "name": "diagnosa"},
				{"data": "jml_tagihan", "name": "jml_tagihan"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<button type="button" class="bg-info btn-xs verify-button" data-uid="' + row.uid + '" title="Verifikasi kelengkapan Berkas">Verify</button>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadKoding = function() {
        if (oKodingTable == null) {
			handleKodingTable();
        }
		else {
			oKodingTable.ajax.reload();
		}
    };
	
	var handleDaftarVerifikasiTable = function() {

		oDaftarVerifikasiTable = $('#daftar_verifikasi_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_daftar_verifikasi,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_register", "name": "no_register"},
				{"data": "nama", "name": "nama"},
				{"data": "asal_pasien", "name": "asal_pasien"},
				{"data": "diagnosa", "name": "diagnosa"},
				{"data": "tagihan_rs", "name": "tagihan_rs"},
				{"data": "nilai_diajukan", "name": "nilai_diajukan"},
				{"data": "selisih", "name": "selisih"}
			]
		});
			
    };
	
	var reloadDaftarVerifikasi = function() {
        if (oDaftarVerifikasiTable == null) {
			handleDaftarVerifikasiTable();
        }
		else {
			oDaftarVerifikasiTable.ajax.reload();
		}
    };
	
	var handleDaftarTagihanTable = function() {

		oDaftarTagihanTable = $('#daftar_tagihan_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_daftar_tagihan,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tgl_kirim_txt", "name": "tgl_kirim_txt"},
				{"data": "no_tagihan", "name": "no_tagihan", "render": function(data, type, row, meta) {
					return '<a class="detail-tagihan-row" ref="#" data-uid="' + row.uid + '" title="Detail Tagihan">' + row.no_tagihan + '</a>';
				}},
				{"data": "tagihan_rs", "name": "tagihan_rs"},
				{"data": "nilai_diajukan", "name": "nilai_diajukan"},
				{"data": "tgl_kirim_berkas", "name": "tgl_kirim_berkas"},
				{"data": "tgl_jatuh_tempo", "name": "tgl_jatuh_tempo"}
			]
		});
			
    };
	
	var reloadDaftarTagihan = function() {
        if (oDaftarTagihanTable == null) {
			handleDaftarTagihanTable();
        }
		else {
			oDaftarTagihanTable.ajax.reload();
		}
    };
	
	var handleDaftarLayakTable = function() {

		oDaftarLayakTable = $('#daftar_layak_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_daftar_layak,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tgl_kirim_txt", "name": "tgl_kirim_txt"},
				{"data": "no_tagihan", "name": "no_tagihan"},
				{"data": "tagihan_rs", "name": "tagihan_rs"},
				{"data": "nilai_diajukan", "name": "nilai_diajukan"},
				{"data": "nilai_diakui", "name": "nilai_diakui"},
				{"data": "tgl_kirim_berkas", "name": "tgl_kirim_berkas"},
				{"data": "tgl_jatuh_tempo", "name": "tgl_jatuh_tempo"},
				{"data": "umur", "name": "umur"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<button type="button" class="bg-info btn-xs pembayaran-button" data-uid="' + row.uid + '" title="Pembayarang Tagihan BPJS">Pembayaran</button>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarLayak = function() {
        if (oDaftarLayakTable == null) {
			handleDaftarLayakTable();
        }
		else {
			oDaftarLayakTable.ajax.reload();
		}
    };
	
	var handleDaftarTidakLayakTable = function() {

		oDaftarTidakLayakTable = $('#daftar_tidak_layak_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_daftar_tidak_layak,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tgl_kirim_txt", "name": "tgl_kirim_txt"},
				{"data": "no_tagihan", "name": "no_tagihan"},
				{"data": "tagihan_rs", "name": "tagihan_rs"},
				{"data": "tgl_kirim_berkas", "name": "tgl_kirim_berkas"},
				{"data": "tgl_jatuh_tempo", "name": "tgl_jatuh_tempo"},
				{"data": "nilai_diajukan", "name": "nilai_diajukan"},
				{"data": "nilai_diakui", "name": "nilai_diakui"}
			]
		});
			
    };
	
	var reloadDaftarTidakLayak = function() {
        if (oDaftarTidakLayakTable == null) {
			handleDaftarTidakLayakTable();
        }
		else {
			oDaftarTidakLayakTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			$(".styled").uniform({
				radioClass: 'choice'
			});
			
			reloadPemberkasan();
			
			$('#pemberkasan_table').on('click', '.verify-button', function () {
				$('#verifikasi_berkas_modal').modal({backdrop: 'static'});
				$('#verifikasi_berkas_modal').modal('show');
			});
			
			$('#koding_table').on('click', '.verify-button', function () {
				$('#verifikasi_inacbgs_modal').modal({backdrop: 'static'});
				$('#verifikasi_inacbgs_modal').modal('show');
			});
			
			$('#buat_tagihan_button').on('click', function () {
				$('#buat_tagihan_modal').modal({backdrop: 'static'});
				$('#buat_tagihan_modal').modal('show');
			});
			
			$('#daftar_tagihan_table').on('click', '.detail-tagihan-row', function (event) {
				event.preventDefault();
				$('#Detail_tagihan_modal').modal({backdrop: 'static'});
				$('#Detail_tagihan_modal').modal('show');
			});
			
			$('#daftar_layak_table').on('click', '.pembayaran-button', function () {
				$('#pembayaran_tagihan_bpjs_modal').modal({backdrop: 'static'});
				$('#pembayaran_tagihan_bpjs_modal').modal('show');
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data_pemberkasan':
							if (oKodingTable === null) {
								reloadKoding();
							}
							break;
						case 'load_data_koding':
							if (oDaftarVerifikasiTable === null) {
								reloadDaftarVerifikasi();
							}
							break;
						case 'load_data_daftar_verifikasi':
							if (oDaftarTagihanTable === null) {
								reloadDaftarTagihan();
							}
							break;
						case 'load_data_daftar_tagihan':
							if (oDaftarLayakTable === null) {
								reloadDaftarLayak();
							}
							break;
						case 'load_data_daftar_layak':
							if (oDaftarTidakLayakTable === null) {
								reloadDaftarTidakLayak();
							}
							break;
					};
				}
            });
			
        }

    };

}();