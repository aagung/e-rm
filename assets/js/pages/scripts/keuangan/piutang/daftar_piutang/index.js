var Index = function () {
	
	var oDaftarPiutangTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPiutangTable = function() {

		oDaftarPiutangTable = $('#piutang_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data": function (d) {
					d.tanggal_dari = $('#filter_tanggal_dari').val();
					d.tanggal_sampai = $('#filter_tanggal_sampai').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "nama", "name": "nama"},
				{"data": "saldo_awal", "name": "saldo_awal"},
				{"data": "pergerakan", "name": "pergerakan"},
				{"data": "saldo_akhir", "name": "saldo_akhir"}
			]
		});
			
    };
	
	var reloadDaftarPiutang = function() {
        if (oDaftarPiutangTable == null) {
			handleDaftarPiutangTable();
        }
		else {
			oDaftarPiutangTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			$(".styled").uniform({
				radioClass: 'choice'
			});
			
			$('#disp_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarPiutang();
			});
			
			$('#disp_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarPiutang();
			});
			
			reloadDaftarPiutang();
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data':
							break;
					};
				}
            });
			
        }

    };

}();