var Index = function () {
	
	var oDaftarPiutangTable = null;
	var oDaftarHistoryTable = null;
	var oDaftarBatalTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPiutangTable = function() {

		oDaftarPiutangTable = $('#daftar_piutang_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_piutang,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal", "width": "15%", "render": function(data, type, row, meta) {
					return '<a class="kasir-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + moment(data).format('DD/MM/YYYY HH:MM') + '</a>';
				}},
				{"data": "no_register", "name": "no_register", "width": "18%"},
				{"data": "nama", "name": "nama", "width": "25%"},
				{"data": "alamat", "name": "alamat"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarPiutang = function() {
        if (oDaftarPiutangTable == null) {
			handleDaftarPiutangTable();
        }
		else {
			oDaftarPiutangTable.ajax.reload();
		}
    };
	
	var handleDaftarHistoryTable = function() {

		oDaftarHistoryTable = $('#history_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_history,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "nama", "name": "nama"},
				{"data": "status", "name": "status", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					if (data == 0) 
						return '<span class="label label-success">Aktif</span>';
					else
						return '<span class="label label-danger">Tidak Aktif</span>';
				}, "className": "text-center"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarHistory = function() {
        if (oDaftarHistoryTable == null) {
			handleDaftarHistoryTable();
        }
		else {
			oDaftarHistoryTable.ajax.reload();
		}
    };
	
	var handleDaftarBatalTable = function() {

		oDaftarBatalTable = $('#batal_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_batal,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "nama", "name": "nama"},
				{"data": "status", "name": "status", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					if (data == 0) 
						return '<span class="label label-success">Aktif</span>';
					else
						return '<span class="label label-danger">Tidak Aktif</span>';
				}, "className": "text-center"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarBatal = function() {
        if (oDaftarBatalTable == null) {
			handleDaftarBatalTable();
        }
		else {
			oDaftarBatalTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			handleDaftarPiutangTable();
			
			$("#pasien_table").on("click", ".kasir-row", function () {
				var uid = $(this).data('uid');
				window.location = url_kasir + '?from=front_desk&uid=' + uid;
			});
			
			$("#pasien_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data_pasien':
							reloadDaftarHistory();
							break;
						case 'load_data_history':
							reloadDaftarBatal();
							break;
					};
				}
            });
			
        }

    };

}();