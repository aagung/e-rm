var Edit = function () {

	var oTableLookupTarifPelayanan = null;
	var oTableDaftarPasien;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'};
	var lastLineNo = 0;
	var lastLineNoBayar = 0;
	var saveTempTarifPelayananId = 0;
	
	var lastLineNoFarmasi = 0;
	var lastLineNoLaboratorium = 0;
	var lastLineNoRadiologi = 0;
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};

	var fillBank = function(bankId, elementId) {
		//$('#kabupaten_spinner').show();
        $.getJSON(url_get_bank, function(data, status) {
            if (status === 'success') {
                var optionBank = '';
                var selected = parseInt(bankId) == 0 ? ' selected="selected"' : '';
                optionBank += '<option value="0"' + selected + '>[ Pilih Bank: ]</option>';
                for (var i = 0; i < data.bank_list.length; i++) {
                    selected = parseInt(bankId) == parseInt(data.bank_list[i].id) ? ' selected="selected"' : '';
                    optionBank += '<option value="' + data.bank_list[i].id + '"' + selected + '>' + data.bank_list[i].nama + '</option>';
                }
                $(elementId).html(optionBank);
                //$('#kabupaten_id').val(kabupatenId).trigger('change.select2');
				//$('#kabupaten_spinner').hide();
            }
        });
    };

	var handleLookupTarifPelayanan = function() {
        
        oTableLookupTarifPelayanan = $('#lookup_tarif_pelayanan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_tarif_pelayanan,
                "type": "POST",
				"data"    : function(d) {
								d.layanan_id = $('#layanan_id').val();
								d.cara_bayar_id = $('#cara_bayar_id').val();
								d.perusahaan_id = $('#perusahaan_id').val();
								d.kelas_id = $('#kelas_id').val();
								d.golongan_operasi = $('#golongan_operasi').val();
							},
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode", "name": "kode", 
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.id + '"';
						dataTag += ' data-uid="' + row.uid + '"';
						dataTag += ' data-kode="' + row.kode + '"';
						dataTag += ' data-nama="' + row.nama + '"';
						dataTag += ' data-tarif="' + row.tarif + '"';
						return '<a class="select-row"' + dataTag + ' href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "nama", "name": "nama"},
				{ "data": "tarif", "name": "tarif"}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_tarif_pelayanan_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupTarifPelayanan = function() {
		if (oTableLookupTarifPelayanan == null) {
			handleLookupTarifPelayanan();
		}
		else {
			oTableLookupTarifPelayanan.ajax.reload();
		}
	}
	
	var setMaxHeightModal = function(modalHeaderId, modalBodyId, modalFooterId) {
		var windowHeight = window.innerHeight;
		var margin = 60;
		var modalHeaderHeight = $(modalHeaderId).outerHeight(true);
		var modalFooterHeight = $(modalFooterId).outerHeight(true);
		var border = 4;
		var maxHeight = windowHeight - (margin + modalHeaderHeight + modalFooterHeight + border);
		$(modalBodyId).css('max-height', maxHeight + 'px');
	};
	
	var calculateAge = function() {
		if ($('#tanggal_lahir').val() === '') {
            $('#tanggal_lahir').val(0);
            $('#label_umur').text('');
        }
        else {
			var today = moment();
			var dob = moment($('#tanggal_lahir').val());
			
			var years = today.diff(dob, 'year');
			dob.add(years, 'years');
			
			var months = today.diff(dob, 'months');
			dob.add(months, 'months');
			
			var days = today.diff(dob, 'days');

            $('#label_umur').text(years + ' tahun, ' + months + ' bulan, ' + days + ' hari');
        }
	};
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };
	
	var calculateTotal = function() {
		var subTotal = parseFloat($('#sub_total').val());
		var nonTunai = parseFloat($('#non_tunai').val());
		var tunai = subTotal - nonTunai;
		$('#tunai').val(tunai);
		$('#label_tunai').autoNumeric('set', tunai);
		var uangDiterima = parseFloat($('#uang_diterima').val());
		var sisa = uangDiterima - tunai;
		var kembalian = 0;
		if (sisa > 0) {
			kembalian = sisa;
			sisa = 0;
		}
		else {
			sisa = sisa * (-1);
		}
		$('#sisa').val(sisa);
		$('#label_sisa').autoNumeric('set', sisa);
		$('#kembalian').val(kembalian);
		$('#label_kembalian').autoNumeric('set', kembalian);
	};
	
	var disabledButtons = function($tr, disabled) {
		if (disabled) {
			
			$tr.find('td:nth-child(1)').css('padding', '2px');
			$tr.find('td:nth-child(3)').css('padding', '2px');
			$tr.find('td:nth-child(4)').css('padding', '2px');
			$tr.find('td:nth-child(5)').css('padding', '2px');
			$tr.find('td:nth-child(6)').css('padding', '2px');
			$tr.find('td:nth-child(7)').css('padding', '2px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.edit-button').removeClass('edit-button').addClass('tambah-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.hapus-button').removeClass('hapus-button').addClass('tambah-batal-button').html('<i class="icon-undo"></i>');
			}
			else {
				$tr.find('.edit-button').removeClass('edit-button').addClass('edit-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.hapus-button').removeClass('hapus-button').addClass('edit-batal-button').html('<i class="icon-undo"></i>');
			}
			
			$('#kasir_detail_table').find('.edit-button').addClass('disabled');
			$('#kasir_detail_table').find('.edit-button').prop('disabled', true);
			$('#kasir_detail_table').find('.hapus-button').addClass('disabled');
			$('#kasir_detail_table').find('.hapus-button').prop('disabled', true);
			
			$('#tambah_button').addClass('disabled');
			$('#tambah_button').prop('disabled', true);

			$('#simpan_1_button').addClass('disabled');
			$('#simpan_1_button').prop('disabled', true);
			$('#batal_1_button').addClass('disabled');
			$('#batal_1_button').prop('disabled', true);
			
			$('#simpan_2_button').addClass('disabled');
			$('#simpan_2_button').prop('disabled', true);
			$('#batal_2_button').addClass('disabled');
			$('#batal_2_button').prop('disabled', true);
		}
		else {
			
			$tr.find('td:nth-child(1)').css('padding', '8px');
			$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			$tr.find('td:nth-child(6)').css('padding', '8px');
			$tr.find('td:nth-child(7)').css('padding', '8px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.tambah-simpan-button').removeClass('tambah-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.tambah-batal-button').removeClass('tambah-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');
			}
			else {
				$tr.find('.edit-simpan-button').removeClass('edit-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.edit-batal-button').removeClass('edit-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');
			}
			
			$('#kasir_detail_table').find('.edit-button').removeClass('disabled');
			$('#kasir_detail_table').find('.edit-button').prop('disabled', false);
			$('#kasir_detail_table').find('.hapus-button').removeClass('disabled');
			$('#kasir_detail_table').find('.hapus-button').prop('disabled', false);
			
			$('#tambah_button').removeClass('disabled');
			$('#tambah_button').prop('disabled', false);

			$('#simpan_1_button').removeClass('disabled');
			$('#simpan_1_button').prop('disabled', false);
			$('#batal_1_button').removeClass('disabled');
			$('#batal_1_button').prop('disabled', false);
			
			$('#simpan_2_button').removeClass('disabled');
			$('#simpan_2_button').prop('disabled', false);
			$('#batal_2_button').removeClass('disabled');
			$('#batal_2_button').prop('disabled', false);
		}
	}
	
	var kasirDetailHandle = function() {
		
		$('#tambah_button').on('click', function() {
            lastLineNo++;
			lineNo = lastLineNo;

            var templateString = $('#kasir-detail-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#kasir_footer_section').before(newString);
            var $tr = $('#kasir_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#label_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			
            $tr.data('mode', 'add');
			disabledButtons($tr, true);
            
            $('#disp_uraian_' + lineNo).focus();
		});
		
		$('#kasir_detail_table').on('click', '.tambah-simpan-button', function() {
            $tr = $(this).parent().parent();
			
            var lineNo = $tr.data('line_no');
			
			$('#tindakan_id_' + lineNo).val(saveTempTarifPelayananId);
            
			var kode = $('#disp_kode_' + lineNo).val();
            $('#label_kode_' + lineNo).text(kode).show();
            $('#kode_section_' + lineNo).hide();
			
			var uraian = $('#label_uraian_' + lineNo).text();
            $('#uraian_' + lineNo).val(uraian)

            var hargaSatuan = parseFloat($('#disp_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);
            $('#label_harga_satuan_' + lineNo).autoNumeric('set', hargaSatuan).show();
            $('#disp_harga_satuan_' + lineNo).hide();

            var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).autoNumeric('set', quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).autoNumeric('set', discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#disp_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
            $('#label_jumlah_' + lineNo).autoNumeric('set', jumlah).show();
            $('#disp_jumlah_' + lineNo).hide();
			
			var oldSubTotal = parseFloat($('#sub_total').val());
			var newSubTotal = subTotal + jumlah;
			$('#sub_total').val(newSubTotal);
			$('#label_sub_total').autoNumeric('set', newSubTotal);
			
			$('#total').val(newSubTotal);
			$('#label_total').autoNumeric('set', newSubTotal);
			
			calculateTotal();
			
			$tr.data('mode', 'add');
			disabledButtons($tr, false);
        });
		
		$('#kasir_detail_table').on('click', '.tambah-batal-button', function(event) {
            $tr = $(this).parent().parent();
			$tr.data('mode', 'add');
            disabledButtons($tr, false);
			$tr.remove();
			lastLineNo--;
        });
		
		$('#kasir_detail_table').on('click', '.edit-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');
			
			$('#label_kode_' + lineNo).hide();
            $('#kode_section_' + lineNo).show();

            $('#label_harga_satuan_' + lineNo).hide();
            $('#disp_harga_satuan_' + lineNo).show();

            $('#label_quantity_' + lineNo).hide();
            $('#disp_quantity_' + lineNo).show();
			
			$('#label_discount_' + lineNo).hide();
            $('#disp_discount_' + lineNo).show();
			
			$('#label_jumlah_' + lineNo).hide();
            $('#disp_jumlah_' + lineNo).show();
			
			$tr.data('mode', 'edit');
			disabledButtons($tr, true);
        });

        $('#kasir_detail_table').on('click', '.edit-simpan-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');

			$('#tindakan_id_' + lineNo).val(saveTempTarifPelayananId);
			
			var kode = $('#disp_kode_' + lineNo).val();
			$('#kode_' + lineNo).val(kode);
            $('#label_kode_' + lineNo).text(kode).show();
            $('#kode_section_' + lineNo).hide();
			
            var uraian = $('#label_uraian_' + lineNo).text();
			$('#uraian_' + lineNo).val(uraian);

            var hargaSatuan = parseFloat($('#disp_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);
            $('#label_harga_satuan_' + lineNo).autoNumeric('set', hargaSatuan).show();
            $('#disp_harga_satuan_' + lineNo).hide();
			
			var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).autoNumeric('set', quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).autoNumeric('set', discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#disp_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
            $('#label_jumlah_' + lineNo).autoNumeric('set', jumlah).show();
            $('#disp_jumlah_' + lineNo).hide();

			$tr.data('mode', 'edit');
			disabledButtons($tr, false);
        });

        $('#kasir_detail_table').on('click', '.edit-batal-button', function() {

            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');
			
			var kode = $('#label_kode_' + lineNo).text();
            $('#label_kode_' + lineNo).show();
			$('#disp_kode_' + lineNo).val(kode);
            $('#kode_section_' + lineNo).hide();
            
 			var uraian = $('#uraian_' + lineNo).val();
            $('#label_uraian_' + lineNo).text(uraian);

			var hargaSatuan = parseFloat($('#harga_satuan_' + lineNo).val());
            $('#label_harga_satuan_' + lineNo).show();
            $('#disp_harga_satuan_' + lineNo).val(hargaSatuan).hide();
			
			var quantity = parseFloat($('#quantity_' + lineNo).val());
            $('#label_quantity_' + lineNo).show();
            $('#disp_quantity_' + lineNo).val(quantity).hide();
			
			var discount = parseFloat($('#discount_' + lineNo).val());
            $('#label_discount_' + lineNo).show();
            $('#disp_discount_' + lineNo).val(discount).hide();
			
			var jumlah = parseFloat($('#jumlah_' + lineNo).val());
            $('#label_jumlah_' + lineNo).show();
            $('#disp_jumlah_' + lineNo).val(jumlah).hide();
			
			$tr.data('mode', 'edit');
			disabledButtons($tr, false);
        });

		$('#kasir_detail_table').on('click', '.hapus-button', function(event) {
            $(this).parent().parent().remove();
        });
		
		$('#kasir_detail_table').on('click', '.tarif-pelayanan-button', function(e) {
			$tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_tarif_pelayanan_modal').data('line_no', lineNo);
			
			//$('#tarif_pelayanan_uid').val($('#tarif_rawat_inap_uid').val());
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
		});
		
		$('#lookup_tarif_pelayanan_table').on('click', '.select-tarif-pelayanan', function() {
			event.preventDefault();
			
			var id = $(this).data('id');
			var uid = $(this).data('uid');
			var kode = $(this).data('kode');
			var nama = $(this).data('nama');
			var hargaSatuan = 0;
			var lineNo =  $('#lookup_tarif_pelayanan_modal').data('line_no');
			
			$('#disp_kode_' + lineNo).val(kode);
			$('#label_uraian_' + lineNo).text(nama);
			$('#disp_harga_satuan_' + lineNo).autoNumeric('set', hargaSatuan);
			
			saveTempTarifPelayananId = id;
			
			$("#lookup_tarif_pelayanan_modal").modal("hide");
		});
	};
	
	var disabledButtonsBayar = function($tr, disabled) {
		if (disabled) {
			
			$tr.find('td:nth-child(1)').css('padding', '2px');
			$tr.find('td:nth-child(2)').css('padding', '2px');
			$tr.find('td:nth-child(3)').css('padding', '2px');
			$tr.find('td:nth-child(4)').css('padding', '2px');
			$tr.find('td:nth-child(5)').css('padding', '2px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.bayar-edit-button').removeClass('bayar-edit-button').addClass('bayar-tambah-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.bayar-hapus-button').removeClass('bayar-hapus-button').addClass('bayar-tambah-batal-button').html('<i class="icon-undo"></i>');
			}
			else {
				$tr.find('.bayar-edit-button').removeClass('bayar-edit-button').addClass('bayar-edit-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.bayar-hapus-button').removeClass('bayar-hapus-button').addClass('bayar-edit-batal-button').html('<i class="icon-undo"></i>');
			}
			
			$('#bayar_detail_table').find('.bayar-edit-button').addClass('disabled');
			$('#bayar_detail_table').find('.bayar-edit-button').prop('disabled', true);
			$('#bayar_detail_table').find('.bayar-hapus-button').addClass('disabled');
			$('#bayar_detail_table').find('.bayar-hapus-button').prop('disabled', true);
			
			$('#bayar_tambah_button').addClass('disabled');
			$('#bayar_tambah_button').prop('disabled', true);

			$('#simpan_1_button').addClass('disabled');
			$('#simpan_1_button').prop('disabled', true);
			$('#batal_1_button').addClass('disabled');
			$('#batal_1_button').prop('disabled', true);
			
			$('#simpan_2_button').addClass('disabled');
			$('#simpan_2_button').prop('disabled', true);
			$('#batal_2_button').addClass('disabled');
			$('#batal_2_button').prop('disabled', true);
		}
		else {
			
			$tr.find('td:nth-child(1)').css('padding', '8px');
			$tr.find('td:nth-child(2)').css('padding', '8px');
			$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.bayar-tambah-simpan-button').removeClass('bayar-tambah-simpan-button').addClass('bayar-edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.bayar-tambah-batal-button').removeClass('bayar-tambah-batal-button').addClass('bayar-hapus-button').html('<i class="icon-trash"></i>');
			}
			else {
				$tr.find('.bayar-edit-simpan-button').removeClass('bayar-edit-simpan-button').addClass('bayar-edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.bayar-edit-batal-button').removeClass('bayar-edit-batal-button').addClass('bayar-hapus-button').html('<i class="icon-trash"></i>');
			}
			
			$('#bayar_detail_table').find('.bayar-edit-button').removeClass('disabled');
			$('#bayar_detail_table').find('.bayar-edit-button').prop('disabled', false);
			$('#bayar_detail_table').find('.bayar-hapus-button').removeClass('disabled');
			$('#bayar_detail_table').find('.bayar-hapus-button').prop('disabled', false);
			
			$('#bayar_tambah_button').removeClass('disabled');
			$('#bayar_tambah_button').prop('disabled', false);

			$('#simpan_1_button').removeClass('disabled');
			$('#simpan_1_button').prop('disabled', false);
			$('#batal_1_button').removeClass('disabled');
			$('#batal_1_button').prop('disabled', false);
			
			$('#simpan_2_button').removeClass('disabled');
			$('#simpan_2_button').prop('disabled', false);
			$('#batal_2_button').removeClass('disabled');
			$('#batal_2_button').prop('disabled', false);
		}
	}
	
	var kasirBayarDetailHandle = function() {
		
		lastLineNo = $('#bayar_footer_section').prevAll().length;
		
		$('#bayar_tambah_button').on('click', function() {
            lastLineNoBayar++;
			var lineNo = lastLineNoBayar;

            var templateString = $('#kasir-bayar-detail-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#bayar_footer_section').before(newString);
            var $tr = $('#bayar_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			fillBank(0, '#disp_bayar_nama_perkiraan_' + lineNo);
			$('#disp_bayar_jumlah_' + lineNo).autoNumeric('init', numericOptions);
		});

		$('#bayar_detail_table').on('click', '.bayar-hapus-button', function(event) {
            var $tr = $(this).parent().parent()
			var lineNo = $tr.data('line_no');
			
			var jumlah = parseFloat($('#bayar_jumlah_' + lineNo).val());
			var oldSubTotal = parseFloat($('#bayar_sub_total').val());
			var newSubTotal = oldSubTotal - jumlah;
			
			$('#bayar_sub_total').val(newSubTotal);
			$('#label_bayar_sub_total').autoNumeric('set', newSubTotal);
			
			$('#non_tunai').val(newSubTotal);
			$('#label_non_tunai').autoNumeric('set', newSubTotal);
			
			$tr.remove();
        });
		
		$('#bayar_detail_table').on('focus', '.bayar-jumlah-row', function(event) {
            $(this).select();
        });
		
		$('#bayar_detail_table').on('change input keyup', '.bayar-jumlah-row', function(event) {
            var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			var oldJumlah = parseFloat($('#bayar_jumlah_' + lineNo).val());
			var newJumlah = parseFloat($(this).autoNumeric('get'));
			
			$('#bayar_jumlah_' + lineNo).val(newJumlah);
			
			var oldTotal = parseFloat($('#bayar_sub_total').val());
			var newTotal = (oldTotal - oldJumlah) + newJumlah;
			
			$('#bayar_sub_total').val(newTotal);
			$('#label_bayar_sub_total').autoNumeric('set', newTotal);
			
        });
		
	};
	
	function initializeFarmasiUnit(el, cara_bayar_id) {
        $.getJSON(URL.getFarmasiUnit.replace(':CARA_BAYAR_ID', cara_bayar_id), function(data, status) {
            if (status === "success") {
                let option = `<option value="0" selected="selected">- Pilih -</option>`;
                for (var i = 0; i < data.data.length; i++) {
                    let selected = "";
                    if (data.data.length === 1) selected = "selected='selected'";
                    option += `<option value="${data.data[i].uid}" ${selected}>${data.data[i].nama}</option>`;
                }
                el.html(option).trigger("change");

                el.prop('disabled', false);
                if (data.data.length === 1) el.prop('disabled', true);
            }
        });
    }
	
	var fillFarmasi = function(uid) {
		$('#dlg_frm_disp_no_rm').html($('#disp_no_rm').val());
		$('#dlg_frm_disp_nama').html($('#nama').val());
		$('#dlg_frm_disp_alamat').html($('#alamat').val());
		$('#dlg_frm_disp_jenis_kelamin').html($('#disp_jenis_kelamin').val());
		$('#dlg_frm_disp_tanggal_lahir').html($('#disp_tanggal_lahir').val());
		$('#dlg_frm_disp_umur').html($('#label_umur').text());
		$('#dlg_frm_disp_no_telepon_1').html($('#no_telepon_1').val());
		
		$('#dlg_frm_disp_no_register').html($('#pelayanan_no_register').val());
		$('#dlg_frm_disp_tanggal').html($('#pelayanan_tanggal').val());
		$('#dlg_frm_disp_asal_pasien').html($('#asal_pasien').val());
		$('#dlg_frm_disp_cara_bayar').html($('#label_jaminan').text());
		$('#dlg_frm_disp_perusahaan').html($('#label_perusahaan').text());
		$('#dlg_frm_disp_no_jaminan').html($('#label_no_jaminan').text());
		$('#dlg_frm_disp_penjamin').html($('#label_penjamin').text());
		$('#dlg_frm_disp_penjamin_perusahaan').html($('#label_penjamin_perusahaan').text());
		$('#dlg_frm_disp_penjamin_no_jaminan').html($('#label_penjamin_no_jaminan').text());
		
		var jenisCaraBayar = parseInt($('#jenis_cara_bayar').val());
		switch (jenisCaraBayar) {
			case imediscode.CARA_BAYAR_UMUM:
				$('#dlg_frm_perusahaan_section').hide();
				$('#dlg_frm_no_jaminan_section').hide();
				$('#dlg_frm_penjamin_section').hide();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_no_jaminan').text('No. Jaminan');
				break;
			case imediscode.CARA_BAYAR_BPJS:
				$('#dlg_frm_perusahaan_section').hide();
				$('#dlg_frm_no_jaminan_section').show();
				$('#dlg_frm_penjamin_section').show();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_no_jaminan').text('No. SEP');
				break;
			case imediscode.CARA_BAYAR_JAMKESDA:
				$('#dlg_frm_perusahaan_section').hide();
				$('#dlg_frm_no_jaminan_section').show();
				$('#dlg_frm_penjamin_section').hide();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_no_jaminan').text('No. Jamkesda');
				break;
			case imediscode.CARA_BAYAR_ASURANSI:
				$('#dlg_frm_perusahaan_section').show();
				$('#dlg_frm_no_jaminan_section').show();
				$('#dlg_frm_penjamin_section').hide();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_perusahaan').text('Asuransi');
				$('#dlg_frm_title_no_jaminan').text('No. Anggota');
				break;
			case imediscode.CARA_BAYAR_PERUSAHAAN:
				$('#dlg_frm_perusahaan_section').show();
				$('#dlg_frm_no_jaminan_section').show();
				$('#dlg_frm_penjamin_section').hide();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_perusahaan').text('Perusahaan');
				$('#dlg_frm_title_no_jaminan').text('NIK');
				break;
			case imediscode.CARA_BAYAR_INTERNAL:
				$('#dlg_frm_perusahaan_section').hide();
				$('#dlg_frm_no_jaminan_section').show();
				$('#dlg_frm_penjamin_section').hide();
				$('#dlg_frm_penjamin_perusahaan_section').hide();
				$('#dlg_frm_penjamin_no_jaminan_section').hide();
				$('#dlg_frm_title_no_jaminan').text('NIK');
				break;
		}
		
		$('#dlg_frm_disp_layanan').html($('#label_layanan').text());
		$('#dlg_frm_disp_dokter').html($('#label_dokter').text());
		
		//initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.cara_bayar_id));
		
        $.getJSON(url_get_data_farmasi + '/' + uid, function (data, status) {
			if (status === 'success') {
				/*
				$(".div-label_perusahaan").hide();
				$(".div-label_no_jaminan").hide();
				$(".div-label_penjamin_perusahaan").hide();
				$(".div-label_penjamin_no_jaminan").hide();
				
				let data = res.data;
				$(form).find("#asal_pasien").val(data.asal_pasien);
				$(form).find("#farmasi_unit_id").val(data.farmasi_unit_id);
				$(form).find("input[name=pelayanan_id]").val(data.id);
				$(form).find("input[name=pelayanan_uid]").val(data.uid);
				$(form).find("#no_register").val(data.no_register);
				$(form).find("#pasien_id").val(data.pasien_id);
				$(form).find("#cara_bayar_id").val(data.cara_bayar_id);
				$(form).find("#perusahaan_id").val(data.perusahaan_id);
				$(form).find("#layanan_id").val(data.layanan_id);
				$(form).find("#dokter_id").val(data.dokter_id);

				$("#label-golongan_darah").html(data.pasien.golongan_darah_desc);

				
				$("#label-layanan").html(data.layanan);
				$("#label-dokter").html(data.dokter);
				$("#label-cara_bayar").html(data.cara_bayar);
				$("#label-perusahaan").html(data.perusahaan);
				$("#label-no_jaminan").html(data.no_jaminan);
				$("#label-penjamin_perusahaan").html(data.penjamin_perusahaan);
				$("#label-penjamin_no_jaminan").html(data.penjamin_no_jaminan);

				switch (parseInt(data.cara_bayar_jenis)) {
					case imediscode.CARA_BAYAR_BPJS:
						$(".div-label_no_jaminan").show().children('label').html('No. SEP');

						if(data.penjamin_id) {
							$(".div-label_penjamin_perusahaan").show();
							$(".div-label_penjamin_no_jaminan").show();

							let labelPerusahaan = 'Perusahaan';
							let labelNoJaminan = 'NIK';
							if(parseInt(data.penjamin_id) === imediscode.CARA_BAYAR_ASURANSI) {
								labelPerusahaan = 'Asuransi';
								labelNoJaminan = 'No. Anggota';
							}
							$(".div-label_penjamin_perusahaan").show().children('label').html(labelPerusahaan);
							$(".div-label_penjamin_no_jaminan").show().children('label').html(labelNoJaminan);
						}
						break;
					case imediscode.CARA_BAYAR_JAMKESDA:
						$(".div-label_no_jaminan").show().children('label').html('No. Jamkesda');
						break;
					case imediscode.CARA_BAYAR_ASURANSI:
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						$(".div-label_perusahaan").show();
						$(".div-label_no_jaminan").show();

						let labelPerusahaan = 'Perusahaan';
						let labelNoJaminan = 'NIK';
						if(parseInt(data.cara_bayar_jenis) === imediscode.CARA_BAYAR_ASURANSI) {
							labelPerusahaan = 'Asuransi';
							labelNoJaminan = 'No. Anggota';
						}
						$(".div-label_perusahaan").show().children('label').html(labelPerusahaan);
						$(".div-label_no_jaminan").show().children('label').html(labelNoJaminan);
						break;
					case imediscode.CARA_BAYAR_INTERNAL:
						$(".div-label_no_jaminan").show().children('label').html('NIK');
						break;
				}
				initializeFarmasiUnit($('#modal-obat_farmasi_unit'), btoa(data.cara_bayar_id));
				$(modalPelayanan).modal('hide');
				$(modalPelayanan + ' .modal-dialog').unblock();

				data.pelayanan = data;
				dataResep = data;
				*/
			}
		});
		/*
        let isDTable = $.fn.dataTable.isDataTable($('#table-modal_pelayanan'));
        if(isDTable === true) $('#table-modal_pelayanan').DataTable().destroy();

        tablePelayanan = $('#table-modal_pelayanan').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": URL.loadDataPelayanan,
                "type": "POST",
                "data": function(p) {
                    p.no_register = FORM_SEARCH_MODAL_FIELD.no_register.val();
                    p.no_rekam_medis = FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val();
                    p.nama = FORM_SEARCH_MODAL_FIELD.nama.val();
                }
            },
            "columns": [
                {
                    "data": "tanggal",
                    "render": (data, type, row, meta) => {
                        return moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                    },
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        let tmp = `<a data-uid="${row.uid}" data-popup="tooltip" class="select-row" title="Pilih Pasien">${data}</a>`;
                        return tmp;
                    },
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        tmp += `<br/><span class="text-size-mini text-info"><b>No. RM:</b><br/> ${row.no_rm ? row.no_rm : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "layanan" },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        let tmp = data;
                        if(data.search(/asuransi/i) !== -1 || data.search(/perusahaan/i) !== -1) 
                            tmp += `<br/><span class="text-size-mini text-info">${row.perusahaan ? row.perusahaan : "&mdash;"}</span>`;
                        return tmp;
                    },
                },
                { "data": "dokter" },
            ],
            "order": [ [1, "asc"] ],
            "drawCallback": function (settings) {
                $('#table-modal_pelayanan').find('[data-popup=tooltip]').tooltip();
            },
        });

        $('#table-modal_pelayanan').on('click', '.select-row', function() {
            let uid = $(this).data('uid');
            fillFormPelayanan(uid)
        });

        BTN_SEARCH.click(function () {
            FORM_SEARCH_MODAL_FIELD.no_register.val(FORM_SEARCH_FIELD.no_register.val());
            FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val(FORM_SEARCH_FIELD.no_rekam_medis.val());
            FORM_SEARCH_MODAL_FIELD.nama.val(FORM_SEARCH_FIELD.nama.val());

            $(modalPelayanan).modal('show');
            tablePelayanan.draw(false);
        });

        BTN_SEARCH_MODAL.click(function () {
            FORM_SEARCH_FIELD.no_register.val(FORM_SEARCH_MODAL_FIELD.no_register.val());
            FORM_SEARCH_FIELD.no_rekam_medis.val(FORM_SEARCH_MODAL_FIELD.no_rekam_medis.val());
            FORM_SEARCH_FIELD.nama.val(FORM_SEARCH_MODAL_FIELD.nama.val());

            tablePelayanan.draw(false);
        });
		*/
    };
	
	var farmasiHandle = function() {
		
		$('#btn-tambah_non_racikan').on('click', () => {
			$(modalObatMode).val('non_racikan');
			$(modalObat).modal('show');
		});
		
		var farmasiApp = {
			initFarmasiForm: function () {
				$('#farmasi_form').validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						farmasiApp.addFarmasi($(form));
					}
				});
			},
			addFarmasi: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		farmasiApp.initFarmasiForm();
	};
	
	var laboratoriumHandle = function() {
		var laboratoriumApp = {
			initLaboratoriumForm: function () {
				$('#laboratorium_form').validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						laboratoriumApp.addLaboratorium($(form));
					}
				});
			},
			addLaboratorium: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		laboratoriumApp.initLaboratoriumForm();
	};
	
	var radiologiHandle = function() {
		var radiologiApp = {
			initRadiologiForm: function () {
				$('#radiologi_form').validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						radiologiApp.addRadiologi($(form));
					}
				});
			},
			addRadiologi: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		radiologiApp.initRadiologiForm();
	};
	
	var kasirDetailFarmasiHandle = function() {
		
		$('#tambah_farmasi_button').on('click', function() {
			lastLineNoFarmasi++;
			var lineNo = lastLineNoFarmasi;

            var templateString = $('#kasir-detail-farmasi-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#farmasi_footer_section').before(newString);
            var $tr = $('#farmasi_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
		});
		
		$('#farmasi_table').on('click', '.farmasi-hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
		});
		
	};
	
	var kasirDetailLaboratoriumHandle = function() {
		
		$('#tambah_laboratorium_button').on('click', function() {
			lastLineNoLaboratorium++;
			var lineNo = lastLineNoLaboratorium;

            var templateString = $('#kasir-detail-laboratorium-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#laboratorium_footer_section').before(newString);
            var $tr = $('#laboratorium_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
		});
		
		$('#laboratorium_table').on('click', '.laboratorium-hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
		});
		
		$('#laboratorium_table').on('click', '.laboratorium-tarif-pelayanan-button', function() {
			var $tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_tarif_pelayanan_modal').data('line_no', lineNo);
			$('#lookup_tarif_pelayanan_modal').data('unit', 'laboratorium');
			
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
		});
		
		$('#lookup_tarif_pelayanan_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
			var lineNo = $('#lookup_tarif_pelayanan_modal').data('line_no');
			var unit = $('#lookup_tarif_pelayanan_modal').data('unit');
			
			switch (unit) {
				case 'laboratorium':
					break;
				case 'radiologi':
					break;
			}
			
			$('#lookup_tarif_pelayanan_modal').modal('hide');
		});
		
	};
	
	var kasirDetailRadiologiHandle = function() {
		
		$('#tambah_radiologi_button').on('click', function() {
			lastLineNoRadiologi++;
			var lineNo = lastLineNoRadiologi;

            var templateString = $('#kasir-detail-radiologi-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#radiologi_footer_section').before(newString);
            var $tr = $('#radiologi_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
		});
		
		$('#radiologi_table').on('click', '.radiologi-hapus-button', function() {
			var $tr = $(this).parent().parent();
			$tr.remove();
		});
		
		$('#radiologi_table').on('click', '.radiologi-tarif-pelayanan-button', function() {
			var $tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_tarif_pelayanan_modal').data('line_no', lineNo);
			$('#lookup_tarif_pelayanan_modal').data('unit', 'radiologi');
			
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
		});
		
	};
	
	var formHandle = function() {
		
		$('#label_sub_total_farmasi').autoNumeric('init', numericOptions);
		$('#label_sub_total_laboratorium').autoNumeric('init', numericOptions);
		$('#label_sub_total_radiologi').autoNumeric('init', numericOptions);
		
		$('#label_non_tunai').autoNumeric('init', numericOptions);
		$('#label_tunai').autoNumeric('init', numericOptions);
		$('#disp_uang_diterima').autoNumeric('init', numericOptions);
		$('#label_sisa').autoNumeric('init', numericOptions);
		$('#label_kembalian').autoNumeric('init', numericOptions);
		$('#label_bayar_sub_total').autoNumeric('init', numericOptions);
		
		$('#disp_tanggal').daterangepicker({
			autoUpdateInput: true,
			autoapply: true,
			singleDatePicker: true, 
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
		});
		
		$('#disp_tanggal_lahir').daterangepicker({
			autoUpdateInput: false,
			autoapply: true,
			singleDatePicker: true, 
			showDropdowns: true,
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal_lahir').val(chosen_date.format('YYYY-MM-DD'));
			calculateAge();
		});
		
		$('#disp_tanggal_lahir').on('apply.daterangepicker', function (ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY'));
		});

		$('#disp_tanggal_lahir').on('cancel.daterangepicker', function (ev, picker) {
			$(this).val('');
		});
		
		$('#rujukan_dari').on('change', function() {
			var rujukanDari = parseInt($(this).val());
			switch (rujukanDari) {
				case imediscode.BUKAN_RUJUKAN:
					$('#nama_perujuk_section').hide();
					$('#nama_rumah_sakit_section').hide();
					break;
				case imediscode.RUJUKAN_FKTP:
					$('#nama_perujuk_section').show();
					$('#nama_rumah_sakit_section').hide();
					break;
				case imediscode.RUJUKAN_FKRTL:
					$('#nama_perujuk_section').hide();
					$('#nama_rumah_sakit_section').show();
					break;
			}
		});
		
		$('#cara_bayar_id').on('change', function() {
			var jenis = parseInt($('#cara_bayar_id option:selected').attr('data-jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_UMUM:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').hide();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. Jaminan');
					break;
				case imediscode.CARA_BAYAR_BPJS:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').show();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. SEP');
					break;
				case imediscode.CARA_BAYAR_JAMKESDA:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. Jamkesda');
					break;
				case imediscode.CARA_BAYAR_ASURANSI:
					$('#perusahaan_section').show();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_perusahaan').text('Asuransi');
					$('#title_no_jaminan').text('No. Anggota');
					fillPerusahaan(0, imediscode.CARA_BAYAR_ASURANSI, '#perusahaan_id', '#perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					$('#perusahaan_section').show();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_perusahaan').text('Perusahaan');
					$('#title_no_jaminan').text('NIK');
					fillPerusahaan(0, imediscode.CARA_BAYAR_PERUSAHAAN, '#perusahaan_id', '#perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_INTERNAL:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('NIK');
					break;
			}
		});
		
		$('#penjamin_id').on('change', function() {
			var jenis = parseInt($('#penjamin_id option:selected').attr('data-jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_ASURANSI:
					$('#penjamin_perusahaan_section').show();
					$('#penjamin_no_jaminan_section').show();
					$('#title_penjamin_perusahaan').text('Asuransi');
					$('#title_penjamin_no_jaminan').text('No. Anggota');
					fillPerusahaan(0, imediscode.CARA_BAYAR_ASURANSI, '#penjamin_perusahaan_id', '#penjamin_perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					$('#penjamin_perusahaan_section').show();
					$('#penjamin_no_jaminan_section').show();
					$('#title_penjamin_perusahaan').text('Perusahaan');
					$('#title_penjamin_no_jaminan').text('NIK');
					fillPerusahaan(0, imediscode.CARA_BAYAR_PERUSAHAAN, '#penjamin_perusahaan_id', '#penjamin_perusahaan_spinner');
					break;
			}
		});
		
		$('#tambah_farmasi_button').on('click', function() {
			var uid = $('#uid').val();
			fillFarmasi(uid);
			$('#rujukan_resep_modal').modal({backdrop: 'static'});
			$('#rujukan_resep_modal').modal('show');
		});
		
		$('#rujukan_resep_modal').on('shown.bs.modal', function (e) {
			$('#rujukan_resep_modal .modal-body').scrollTop(0);
			setMaxHeightModal('#rujukan_resep_modal .modal-header', '#rujukan_resep_modal .modal-body', '#rujukan_resep_modal .modal-footer');
		});
		
		//$('#tambah_laboratorium_button').on('click', function() {
		//	$('#rujukan_resep_modal').modal({backdrop: 'static'});
		//	$('#rujukan_resep_modal').modal('show');
		//});
		
		$('#disp_uang_diterima').on('focus', function() {
			$(this).select();
		});
		
		$('#disp_uang_diterima').on('change input keyup', function() {
			var sisa = parseFloat($('#sisa').val());
			var uangDiterima = parseFloat($(this).autoNumeric('get'));
			
			$('#uang_diterima').val(uangDiterima);
			
			var kembalian = sisa - uangDiterima;
			
			kembalian = kembalian * (-1);
			
			$('#kembalian').val(kembalian);
			$('#label_kembalian').autoNumeric('set', kembalian);
		});
		
		var kasirApp = {
			initKasirForm: function () {
				$("#kasir_form").validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						kasirApp.addKasir($(form));
					}
				});
			},
			addKasir: function(form) {
				
				if (parseFloat($('#sisa').val()) > 0) {
					swal({
						title: "PERHATIAN!",
						text: "Masih ada sisa belum dibayar. Sisa bayar akan diutangkan?",
						type: "input",
						showCancelButton: true,
						confirmButtonColor: "#2196F3",
						confirmButtonText: "Ya",
						cancelButtonText: "Tidak",
						closeOnConfirm: false,
						animation: "slide-from-top",
						inputPlaceholder: "Jaminan Utang"
					},
					function(inputValue){
						if (inputValue === false) return false;
						if (inputValue === "") {
							swal.showInputError("Anda perlu memasukan Jaminan Utang!");
							return false
						}
						$('#utang').val($('#sisa').val());
						$('#jaminan_utang').val(inputValue);
					});
				}
				
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		kasirApp.initKasirForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = $('#url').val();
		});
		
	}
	
    return {

        init: function() {
			
			$('.label-no_rm').html($('#disp_no_rm').val());
			$('.label-nama_pasien').html($('#nama').val());
			
			formHandle();
			kasirDetailHandle();
			kasirBayarDetailHandle();
			
			//kasirDetailFarmasiHandle();
			//kasirDetailLaboratoriumHandle();
			//kasirDetailRadiologiHandle();
			
			farmasiHandle();
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'test':
							//$('#view_faktur_jual_modal .modal-body').scrollTop(0);
							break;
						case 'simpan':
							$.unblockUI();
							window.location = $('#url').val();
							break;
					};
				}
            });
			
        }

    };

}();