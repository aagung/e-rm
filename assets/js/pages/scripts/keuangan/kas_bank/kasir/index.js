var Index = function () {
	
	var oDaftarPasienTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPasienTable = function() {

		oDaftarPasienTable = $('#pasien_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "nama", "name": "nama"},
				{"data": "status", "name": "status", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					if (data == 0) 
						return '<span class="label label-success">Aktif</span>';
					else
						return '<span class="label label-danger">Tidak Aktif</span>';
				}, "className": "text-center"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarPasien = function() {
        if (oDaftarPasienTable == null) {
			handleDaftarPasienTable();
        }
		else {
			oDaftarPasienTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarPasien();
			
			$("#tarif_pelayanan_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location.assign(url.edit.replace(':UID', uid));
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();