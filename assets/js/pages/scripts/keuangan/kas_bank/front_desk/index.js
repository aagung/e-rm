var Index = function () {
	
	var oDaftarPasienTable = null;
	var oDaftarHistoryTable = null;
	var oDaftarBatalTable = null;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'};

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
	
	var handleDaftarPasienTable = function() {

		oDaftarPasienTable = $('#pasien_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_pasien,
                "type": "POST",
				"data": function (d) {
					d.no_register = $('#pasien_filter_no_register').val();
					d.no_rm = $('#pasien_filter_no_rm').val();
					d.nama = $('#pasien_filter_nama').val();
					d.jenis_unit = $('#Pasien_filter_asal_pesien').val();
					d.cara_bayar_id = $('#filter_cara_bayar_id').val();
					d.perusahaan_id = $('#filter_perusahaan_id').val();
					d.layanan_id = $('#pasien_filter_layanan_id').val();
					d.dokter_id = $('#pasien_filter_dokter_id').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal", "width": "15%", "render": function(data, type, row, meta) {
					return '<a class="kasir-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + moment(data).format('DD/MM/YYYY HH:MM') + '</a>';
				}},
				{"data": "no_register", "name": "no_register", "width": "18%"},
				{"data": "no_rm", "name": "no_rm", "width": "25%"},
				{"data": "nama", "name": "nama", "width": "25%"},
				{"data": "jenis_unit", "name": "jenis_unit", "render": function(data, type, row, meta) {
					var layanan = '';
					switch (parseInt(data)) {
						case 1:
							layanan = 'Rawat Jalan';
							break;
						case 2:
							layanan = 'IGD';
							break;
						case 3:
							layanan = 'Rawat Inap';
							break;
					}
					return layanan;
				}},
				{"data": "layanan", "name": "layanan"},
				{"data": "cara_bayar", "name": "cara_bayar"},
				{"data": "dokter", "name": "dokter"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			],
			"createdRow": function( row, data, dataIndex ) {
				$('td:nth-child(2)', row).attr("title", data.no_register);
				$('td:nth-child(3)', row).attr("title", data.no_rm);
				$('td:nth-child(4)', row).attr("title", data.nama);
				var layanan = '';
				switch (parseInt(data.jenis_unit)) {
					case 1:
						layanan = 'Rawat Jalan';
						break;
					case 2:
						layanan = 'IGD';
						break;
					case 3:
						layanan = 'Rawat Inap';
						break;
				}
				$('td:nth-child(5)', row).attr("title", layanan);
				$('td:nth-child(6)', row).attr("title", data.layanan);
				$('td:nth-child(7)', row).attr("title", data.cara_bayar);
				$('td:nth-child(8)', row).attr("title", data.dokter);
			}
		});
			
    };
	
	var reloadDaftarPasien = function() {
        if (oDaftarPasienTable == null) {
			handleDaftarPasienTable();
        }
		else {
			oDaftarPasienTable.ajax.reload();
		}
    };
	
	var handleDaftarHistoryTable = function() {

		oDaftarHistoryTable = $('#history_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_history,
                "type": "POST",
				"data": function (d) {
					d.tanggal_dari = $('#history_filter_tanggal_dari').val();
					d.tanggal_sampai = $('#history_filter_tanggal_sampai').val();
					d.jenis_unit = $('#history_filter_asal_pesien').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal", render: function(data, type, row, meta) {
					return moment(data).format("DD/MM/YYYY HH:mm");
				}},
				{"data": "no_kwitansi", "name": "no_kwitansi", "render": function(data, type, row, meta) {
					return '<a class="view-row" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Lihat Rincian Transaksi Pasien">' + data + '</a>';
				}},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "layanan", "name": "layanan", "searchable": false},
				{"data": "cara_bayar", "name": "cara_bayar", "searchable": false},
				{"data": "petugas", "name": "petugas", "searchable": false},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="batal-row text-danger" data-id="' + row.id + '" data-uid="' + row.uid + '" data-no_kwitansi="' + row.no_kwitansi + '" data-nama="' + row.nama + '" title="Batal"><i class="fa fa-ban"></i></a>';
				}, "className": "text-center"}
			],
			"createdRow": function( row, data, dataIndex ) {
				$('td:nth-child(4)', row).attr("title", data.nama);
				$('td:nth-child(5)', row).attr("title", data.layanan);
				$('td:nth-child(6)', row).attr("title", data.cara_bayar);
				$('td:nth-child(7)', row).attr("title", data.petugas);
			}
		});
		
		//$("#history_table_length label select").css("width", "auto").select2();
			
    };
	
	var reloadDaftarHistory = function() {
        if (oDaftarHistoryTable == null) {
			handleDaftarHistoryTable();
        }
		else {
			oDaftarHistoryTable.ajax.reload();
		}
    };
	
	var handleDaftarBatalTable = function() {

		oDaftarBatalTable = $('#batal_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_batal,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "tanggal", "name": "tanggal"},
				{"data": "no_kwitansi", "name": "no_kwitansi"},
				{"data": "no_rm", "name": "no_rm"},
				{"data": "nama", "name": "nama"},
				{"data": "pelayanan", "name": "pelayanan"},
				{"data": "jaminan", "name": "jaminan"},
				{"data": "petugas", "name": "petugas"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="rollback-row" data-id="' + row.id + '" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadDaftarBatal = function() {
        if (oDaftarBatalTable == null) {
			handleDaftarBatalTable();
        }
		else {
			oDaftarBatalTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarPasien();
			
			$('#view_sub_total').autoNumeric('init', numericOptions);
			$('#view_non_tunai').autoNumeric('init', numericOptions);
			$('#view_tunai').autoNumeric('init', numericOptions);
			$('#view_uang_diterima').autoNumeric('init', numericOptions);
			$('#view_sisa').autoNumeric('init', numericOptions);
			$('#view_kembalian').autoNumeric('init', numericOptions);
			
			$('#pasien_filter_no_register').on('change input keyup', function() {
				if ($(this).val().length > 2) {
					reloadDaftarPasien();
				}
			});
			
			$('#pasien_filter_no_rm').on('change input keyup', function() {
				if ($(this).val().length > 2) {
					reloadDaftarPasien();
				}
			});
			
			$('#pasien_filter_nama').on('change input keyup', function() {
				if ($(this).val().length > 2) {
					reloadDaftarPasien();
				}
			});
			
			$('#Pasien_filter_asal_pesien').on('change', function() {
				reloadDaftarPasien();
			});
			
			$('#filter_cara_bayar_id').on('change', function() {
				reloadDaftarPasien();
			});
			
			$('#pasien_filter_dokter_id').on('change', function() {
				reloadDaftarPasien();
			});
			
			$("#pasien_table").on("click", ".kasir-row", function () {
				var uid = $(this).data('uid');
				window.location = url_kasir + '?from=front_desk&uid=' + uid;
			});
			
			$("#pasien_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
			$('#print_rincian_button').on('click', function() {
				$.getJSON(url_print_rincian_transaksi_pasien, function(data, status) {
					if (status === 'success') {
						window.open(data.file_name, '_blank');
					}
				});
			});
			
			$('#disp_history_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#history_filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarHistory();
			});
			
			$('#disp_history_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#history_filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarHistory();
			});
			
			$('#history_table').on('click', '.view-row', function(event) {
				event.preventDefault();
				
				var id = $(this).data('id');
				$.getJSON(url_get_rincian_transaksi_pasien + '?id=' + id, function(data, status) {
					if (status === 'success') {
						$('#view_tanggal').text(data.kasir.disp_tanggal);
						$('#view_no_kwitansi').text(data.kasir.no_kwitansi);
						$('#view_no_rm').text(data.kasir.no_rm);
						$('#view_nama').text(data.kasir.nama);
						$('#view_alamat').text(data.kasir.alamat);
						$('#view_jenis_kelamin').text(data.kasir.disp_jenis_kelamin);
						$('#view_tanggal_lahir').text(data.kasir.disp_tanggal_lahir);
						$('#view_umur').text(data.kasir.umur);
						$('#view_rujukan_dari').text(data.kasir.disp_rujukan_dari);
						switch (parseInt(data.kasir.rujukan_dari)) {
							case imediscode.BUKAN_RUJUKAN:
								$('#nama_perujuk_section').hide();
								$('#rumah_sakit_section').hide();
								break;
							case imediscode.RUJUKAN_FKTP:
								$('#nama_perujuk_section').show();
								$('#rumah_sakit_section').hide();
								break;
							case imediscode.RUJUKAN_FKRTL:
								$('#nama_perujuk_section').hide();
								$('#rumah_sakit_section').show();
								break;
						}
						$('#view_nama_perujuk').text(data.kasir.nama_perujuk);
						$('#view_rumah_sakit').text(data.kasir.disp_rumah_sakit);
						$('#view_cara_bayar').text(data.kasir.disp_cara_bayar);
						switch (parseInt(data.kasir.jenis_cara_bayar)) {
							case imediscode.CARA_BAYAR_UMUM:
								$('#mdl_fd_perusahaan_section').hide();
								$('#no_jaminan_section').hide();
								break;
							case imediscode.CARA_BAYAR_BPJS:
								$('#mdl_fd_perusahaan_section').hide();
								$('#title_no_jaminan').text('SEP');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_JAMKESDA:
								$('#mdl_fd_perusahaan_section').hide();
								$('#title_no_jaminan').text('No. Jamkesda');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_ASURANSI:
								$('#mdl_fd_perusahaan_section').show();
								$('#title_no_jaminan').text('No. Asuransi');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_PERUSAHAAN:
								$('#mdl_fd_perusahaan_section').show();
								$('#title_no_jaminan').text('NIP')
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_INTERNAL:
								$('#mdl_fd_perusahaan_section').hide();
								$('#title_no_jaminan').text('NIP')
								$('#no_jaminan_section').show();
								break;
						}
						$('#view_perusahaan').text(data.kasir.disp_perusahaan);
						$('#view_no_jaminan').text(data.kasir.no_jaminan);
						$('#view_layanan').text(data.kasir.disp_layanan);
						$('#view_dokter').text(data.kasir.disp_dokter);
						
						$('#kasir_detail_footer').prev().remove();
						
						var rows = '';
						var jumlah = 0;
						
						if (typeof data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN] !== 'undefined') {
							for (var i = 0; i < data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN].length; i++) {
								jumlah = (parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].tarif) * parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].quantity)) - parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].discount);
								rows = '<tr>';
								rows += '	<td>' + data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].uraian + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].tarif).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].quantity).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_POLIKLINIK_TINDAKAN][i].discount).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + jumlah.formatMoney(2, ',', '.') + '</td>';
								rows += '</tr>';
								$('#kasir_detail_footer').before(rows);
							}
						}
						
						if (typeof data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN] !== 'undefined') {
							for (var i = 0; i < data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN].length; i++) {
								jumlah = (parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].tarif) * parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].quantity)) - parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].discount);
								rows = '<tr>';
								rows += '	<td>' + data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].uraian + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].tarif).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].quantity).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RAWAT_INAP_TINDAKAN][i].discount).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + jumlah.formatMoney(2, ',', '.') + '</td>';
								rows += '</tr>';
								$('#kasir_detail_footer').before(rows);
							}
						}
						
						if (typeof data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI] !== 'undefined') {
							for (var i = 0; i < data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI].length; i++) {
								jumlah = (parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].tarif) * parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].quantity)) - parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].discount);
								rows = '<tr>';
								rows += '	<td>' + data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].uraian + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].tarif).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].quantity).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_FARMASI][i].discount).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + jumlah.formatMoney(2, ',', '.') + '</td>';
								rows += '</tr>';
								$('#kasir_detail_footer').before(rows);
							}
						}
						
						if (typeof data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM] !== 'undefined') {
							for (var i = 0; i < data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM].length; i++) {
								jumlah = (parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].tarif) * parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].quantity)) - parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].discount);
								rows = '<tr>';
								rows += '	<td>' + data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].uraian + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].tarif).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].quantity).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_LABORATORIUM][i].discount).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + jumlah.formatMoney(2, ',', '.') + '</td>';
								rows += '</tr>';
								$('#kasir_detail_footer').before(rows);
							}
						}
						
						if (typeof data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI] !== 'undefined') {
							for (var i = 0; i < data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI].length; i++) {
								jumlah = (parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].tarif) * parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].quantity)) - parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].discount);
								rows = '<tr>';
								rows += '	<td>' + data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].uraian + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].tarif).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].quantity).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + parseFloat(data.kasir.detail_list[imediscode.JENIS_TINDAKAN_RADIOLOGI][i].discount).formatMoney(2, ',', '.') + '</td>';
								rows += '	<td style="text-align:right;">' + jumlah.formatMoney(2, ',', '.') + '</td>';
								rows += '</tr>';
								$('#kasir_detail_footer').before(rows);
							}
						}
						
						$('#view_sub_total').autoNumeric('set', data.kasir.sub_total);
						$('#view_non_tunai').autoNumeric('set', data.kasir.non_tunai);
						$('#view_tunai').autoNumeric('set', data.kasir.tunai);
						$('#view_uang_diterima').autoNumeric('set', data.kasir.uang_diterima);
						$('#view_sisa').autoNumeric('set', data.kasir.belum_bayar);
						$('#view_kembalian').autoNumeric('set', data.kasir.kembalian);
					}
				});
				
				$('#rincian_transaksi_pasien_modal').modal({backdrop: 'static'});
				$('#rincian_transaksi_pasien_modal').modal('show');
			});
			
			$('#history_table').on('click', '.batal-row', function(event) {
				event.preventDefault();
				
				var noKwitansi = $(this).data('no_kwitansi');
				var nama = $(this).data('nama');
				$('#batal_message').text('Kwitansi No. ' + noKwitansi + ' a.n. ' + nama + ' akan dibatalkan.');
				
				$('#batal_modal').modal({backdrop: 'static'});
				$('#batal_modal').modal('show');
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data_pasien':
							if (oDaftarHistoryTable === null) {
								reloadDaftarHistory();
								
								$('#history_table_filter input[type="search"]')
									.attr('data-toggle', 'tooltip')
									.attr('data-placement', 'bottom')
									.attr('title', 'Masukan No. Kwitansi, No. Rm atau Nama Pasien disini.')
									.tooltip();
							}
							break;
						case 'load_data_history':
							if (oDaftarBatalTable === null) {
								reloadDaftarBatal();
							}
							break;
					};
				}
            });
			
        }

    };

}();