var Edit = function () {

	var oTableLookupPasien = null;
	var oTableLookupTarifPelayanan = null;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}
	var lastLineNo = 0;
	var saveTempTarifPelayananId = 0;
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};

	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	var fillBank = function(bankId, elementId) {
		//$('#kabupaten_spinner').show();
        $.getJSON(url_get_bank, function(data, status) {
            if (status === 'success') {
                var optionBank = '';
                var selected = parseInt(bankId) == 0 ? ' selected="selected"' : '';
                optionBank += '<option value="0"' + selected + '>[ Pilih Bank: ]</option>';
                for (var i = 0; i < data.bank_list.length; i++) {
                    selected = parseInt(bankId) == parseInt(data.bank_list[i].id) ? ' selected="selected"' : '';
                    optionBank += '<option value="' + data.bank_list[i].id + '"' + selected + '>' + data.bank_list[i].nama + '</option>';
                }
                $(elementId).html(optionBank);
                //$('#kabupaten_id').val(kabupatenId).trigger('change.select2');
				//$('#kabupaten_spinner').hide();
            }
        });
    };

	var fillPerusahaan = function(perusahaanId, jenis, elementId, elementSpinner) {
		$(elementSpinner).show();
        $.getJSON(url_get_perusahaan_by_jenis + '?jenis=' + jenis, function(data, status) {
            if (status === 'success') {
				var title = '';
				switch (parseInt(jenis)) {
					case imediscode.CARA_BAYAR_ASURANSI:
						title = 'Asuransi';
						break;
					case imediscode.CARA_BAYAR_PERUSAHAAN:
						title = 'Perusahaan';
						break;
				}
                var optionPerusahaan = '';
                var selected = parseInt(perusahaanId) == 0 ? ' selected="selected"' : '';
                optionPerusahaan += '<option value="0"' + selected + '>[ ' + title + ': ]</option>';
                for (var i = 0; i < data.perusahaan_list.length; i++) {
                    selected = parseInt(perusahaanId) == parseInt(data.perusahaan_list[i].id) ? ' selected="selected"' : '';
                    optionPerusahaan += '<option value="' + data.perusahaan_list[i].id + '"' + selected + '>' + data.perusahaan_list[i].nama + '</option>';
                }
                $(elementId).html(optionPerusahaan);
                $(elementId).val(perusahaanId).trigger('change.select2');
				$(elementSpinner).hide();
            }
        });
    };
	
	var handleLookupPasien = function() {
        
        oTableLookupPasien = $('#lookup_pasien_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_pasien,
                "type": "GET"
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "no_rm", "name": "no_rm",
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.id + '"';
						dataTag += ' data-uid="' + row.uid + '"';
						dataTag += ' data-no_rm="' + row.no_rm + '"';
						dataTag += ' data-nama="' + row.nama + '"';
						dataTag += ' data-alamat="' + row.alamat + '"';
						dataTag += ' data-tanggal_lahir="' + row.tanggal_lahir + '"';
						return '<a class="select-pasien"' + dataTag + ' href="#">' + data + '</a>';
				}, "width": "10%"},
				{ "data": "nama", "name": "nama"},
				{ "data": "jenis_kelamin", "name": "jenis_kelamin"},
				{ "data": "alamat", "name": "alamat"},
				{ "data": "tanggal_lahir", "name": "tanggal_lahir"},
				{ "data": "no_telepon_1", "name": "no_telepon_1"}
			]
         });
		
		//var tableWrapper = $('#lookup_pasien_table_wrapper');
        //tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupPasien = function() {
		if (oTableLookupPasien == null) {
			handleLookupPasien();
		}
		else {
			oTableLookupPasien.ajax.reload();
		}
	}
	
	var handleLookupTarifPelayanan = function() {
        
        oTableLookupTarifPelayanan = $('#lookup_tarif_pelayanan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_tarif_pelayanan,
                "type": "POST",
				"data"    : function(d) {
								d.layanan_id = $('#layanan_id').val();
								d.cara_bayar_id = $('#cara_bayar_id').val();
								d.perusahaan_id = $('#perusahaan_id').val();
								d.kelas_id = 0;
								d.golongan_operasi = 0;
							},
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "id", "name": "id", "visible": false, "targets": [ 0 ] },
				{ "data": "uid", "name": "uid", "visible": false, "targets": [ 1 ] },
				{ "data": "kode", "name": "kode", 
					"render": function(data, type, row, meta) {
						var dataTag = ' data-id="' + row.id + '"';
						dataTag += ' data-uid="' + row.uid + '"';
						dataTag += ' data-kode="' + row.kode + '"';
						dataTag += ' data-nama="' + row.nama + '"';
						dataTag += ' data-tarif="' + row.tarif + '"';
						return '<a class="select-tarif-pelayanan"' + dataTag + ' href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "nama", "name": "nama", "targets": [ 3 ]}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_tarif_pelayanan_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupTarifPelayanan = function() {
		if (oTableLookupTarifPelayanan == null) {
			handleLookupTarifPelayanan();
		}
		else {
			oTableLookupTarifPelayanan.ajax.reload();
		}
	}
	
	var calculateAge = function() {
		if ($('#tanggal_lahir').val() === '') {
            $('#tanggal_lahir').val(0);
            $('#label_umur').text('');
        }
        else {
			var today = moment();
			var dob = moment($('#tanggal_lahir').val());
			
			var years = today.diff(dob, 'year');
			dob.add(years, 'years');
			
			var months = today.diff(dob, 'months');
			dob.add(months, 'months');
			
			var days = today.diff(dob, 'days');

            $('#label_umur').text(years + ' tahun, ' + months + ' bulan, ' + days + ' hari');
        }
	};
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };
	
	var disabledButtons = function($tr, disabled) {
		if (disabled) {
			
			$tr.find('td:nth-child(1)').css('padding', '2px');
			//$tr.find('td:nth-child(2)').css('padding', '2px');
			//$tr.find('td:nth-child(3)').css('padding', '2px');
			$tr.find('td:nth-child(4)').css('padding', '2px');
			$tr.find('td:nth-child(5)').css('padding', '2px');
			//$tr.find('td:nth-child(6)').css('padding', '2px');
			//$tr.find('td:nth-child(7)').css('padding', '2px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.edit-button').removeClass('edit-button').addClass('tambah-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.hapus-button').removeClass('hapus-button').addClass('tambah-batal-button').html('<i class="icon-undo"></i>');
			}
			else {
				$tr.find('.edit-button').removeClass('edit-button').addClass('edit-simpan-button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.hapus-button').removeClass('hapus-button').addClass('edit-batal-button').html('<i class="icon-undo"></i>');
			}
			
			$('#front_desk_detail_table').find('.edit-button').addClass('disabled');
			$('#front_desk_detail_table').find('.edit-button').prop('disabled', true);
			$('#front_desk_detail_table').find('.hapus-button').addClass('disabled');
			$('#front_desk_detail_table').find('.hapus-button').prop('disabled', true);
			
			$('#tambah_button').addClass('disabled');
			$('#tambah_button').prop('disabled', true);

			$('#simpan_1_button').addClass('disabled');
			$('#simpan_1_button').prop('disabled', true);
			$('#batal_1_button').addClass('disabled');
			$('#batal_1_button').prop('disabled', true);
			
			$('#simpan_2_button').addClass('disabled');
			$('#simpan_2_button').prop('disabled', true);
			$('#batal_2_button').addClass('disabled');
			$('#batal_2_button').prop('disabled', true);
		}
		else {
			
			$tr.find('td:nth-child(1)').css('padding', '8px');
			//$tr.find('td:nth-child(2)').css('padding', '8px');
			//$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			//$tr.find('td:nth-child(6)').css('padding', '8px');
			//$tr.find('td:nth-child(7)').css('padding', '8px');
			
			if ($tr.data('mode') === 'add') {
				$tr.find('.tambah-simpan-button').removeClass('tambah-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.tambah-batal-button').removeClass('tambah-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');
			}
			else {
				$tr.find('.edit-simpan-button').removeClass('edit-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
				$tr.find('.edit-batal-button').removeClass('edit-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');
			}
			
			$('#front_desk_detail_table').find('.edit-button').removeClass('disabled');
			$('#front_desk_detail_table').find('.edit-button').prop('disabled', false);
			$('#front_desk_detail_table').find('.hapus-button').removeClass('disabled');
			$('#front_desk_detail_table').find('.hapus-button').prop('disabled', false);
			
			$('#tambah_button').removeClass('disabled');
			$('#tambah_button').prop('disabled', false);

			$('#simpan_1_button').removeClass('disabled');
			$('#simpan_1_button').prop('disabled', false);
			$('#batal_1_button').removeClass('disabled');
			$('#batal_1_button').prop('disabled', false);
			
			$('#simpan_2_button').removeClass('disabled');
			$('#simpan_2_button').prop('disabled', false);
			$('#batal_2_button').removeClass('disabled');
			$('#batal_2_button').prop('disabled', false);
		}
	}
	
	var frontDeskDetailHandle = function() {
		
		$('#tambah_button').on('click', function() {
            lastLineNo++;
			lineNo = lastLineNo;

            var templateString = $('#front-desk-detail-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#front_desk_footer_section').before(newString);
            var $tr = $('#front_desk_footer_section').prev('tr');
			
			$('#label_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			
			$tr.data('line_no', lineNo);
			$tr.data('mode', 'add');
			disabledButtons($tr, true);
            
            $('#disp_uraian_' + lineNo).focus();
		});
		
		$('#front_desk_detail_table').on('click', '.tambah-simpan-button', function() {
            var $tr = $(this).parent().parent();
			
            var lineNo = $tr.data('line_no');
            
			var kode = $('#disp_kode_' + lineNo).val();
            $('#label_kode_' + lineNo).text(kode).show();
            $('#kode_section_' + lineNo).hide();
			
			var uraian = $('#label_uraian_' + lineNo).text();
			$('#uraian_' + lineNo).val(uraian);

            var hargaSatuan = parseFloat($('#label_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);

            var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).autoNumeric('set', quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).autoNumeric('set', discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#label_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
			
			var oldSubTotal = parseFloat($('#sub_total').val());
			var newSubTotal = oldSubTotal + jumlah;
			
			$('#sub_total').val(newSubTotal);
			$('#label_sub_total').autoNumeric('set', newSubTotal);
			
			$tr.data('mode', 'add');
			disabledButtons($tr, false);
        });
		
		$('#front_desk_detail_table').on('click', '.tambah-batal-button', function(event) {
			$tr = $(this).parent().parent();
			$tr.data('mode', 'add');
            disabledButtons($tr, false);
			$tr.remove();
			lastLineNo--;
        });
		
		$('#front_desk_detail_table').on('click', '.edit-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');
			
			$('#label_kode_' + lineNo).hide();
            $('#kode_section_' + lineNo).show();

            $('#label_quantity_' + lineNo).hide();
            $('#disp_quantity_' + lineNo).show();
			
			$('#label_discount_' + lineNo).hide();
            $('#disp_discount_' + lineNo).show();
			
			$tr.data('mode', 'edit');
			disabledButtons($tr, true);
        });

        $('#front_desk_detail_table').on('click', '.edit-simpan-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');

			var kode = $('#disp_kode_' + lineNo).val();
			$('#kode_' + lineNo).val(kode);
            $('#label_kode_' + lineNo).text(kode).show();
            $('#kode_section_' + lineNo).hide();
			
            var uraian = $('#label_uraian_' + lineNo).text();
			$('#uraian_' + lineNo).val(uraian);

            var hargaSatuan = parseFloat($('#label_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);
			
			var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).autoNumeric('set', quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).autoNumeric('set', discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#label_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
			
			$tr.data('mode', 'edit');
			disabledButtons($tr, false);
        });

        $('#front_desk_detail_table').on('click', '.edit-batal-button', function() {

            $tr = $(this).parent().parent();

            var lineNo = $tr.data('line_no');
			
			var kode = $('#kode_' + lineNo).val();
            $('#label_kode_' + lineNo).show();
            $('#kode_section_' + lineNo).val(uraian).hide();
            
 			var uraian = $('#uraian_' + lineNo).val();
            $('#label_uraian_' + lineNo).text(uraian);

			var hargaSatuan = parseFloat($('#harga_satuan_' + lineNo).val());
            $('#label_harga_satuan_' + lineNo).autoNumeric('set', hargaSatuan);
			
			var quantity = parseFloat($('#quantity_' + lineNo).val());
            $('#label_quantity_' + lineNo).show();
            $('#disp_quantity_' + lineNo).val(quantity).hide();
			
			var discount = parseFloat($('#discount_' + lineNo).val());
            $('#label_discount_' + lineNo).show();
            $('#disp_discount_' + lineNo).val(discount).hide();
			
			var jumlah = parseFloat($('#jumlah_' + lineNo).val());
            $('#label_jumlah_' + lineNo).autoNumeric('set', jumlah);

			$tr.data('mode', 'edit');
			disabledButtons($tr, false);
        });

		$('#front_desk_detail_table').on('click', '.hapus-button', function(event) {
            $(this).parent().parent().remove();
        });
		
		$('#front_desk_detail_table').on('click', '.tarif-pelayanan-button', function(e) {
			$tr = $(this).parent().parent().parent().parent();
			var lineNo = $tr.data('line_no');
			
			$('#lookup_tarif_pelayanan_modal').data('line_no', lineNo);
			
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
		});
		
		$('#lookup_tarif_pelayanan_table').on('click', '.select-tarif-pelayanan', function(event) {
			event.preventDefault();
			
			var id = $(this).data('id');
			var uid = $(this).data('uid');
			var kode = $(this).data('kode');
			var nama = $(this).data('nama');
			var tarif = $(this).data('tarif');
			var lineNo =  $('#lookup_tarif_pelayanan_modal').data('line_no');
			
			$('#disp_kode_' + lineNo).val(kode);
			$('#label_uraian_' + lineNo).text(nama);
			$('#label_harga_satuan_' + lineNo).autoNumeric('set', tarif);
			
			saveTempTarifPelayananId = id;
			
			$("#lookup_tarif_pelayanan_modal").modal("hide");
		});
	
		$('#front_desk_detail_table').on('focus', '.quantity-row', function() {
			$(this).select();
		});
		
		$('#front_desk_detail_table').on('change input keyup', '.quantity-row', function() {
			var lineNo = $(this).parent().parent().data('line_no');
			var hargaSatuan = parseFloat($('#label_harga_satuan_' + lineNo).autoNumeric('get'));
			var quantity = parseFloat($(this).autoNumeric('get'));
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
			var jumlah = (hargaSatuan - discount) * quantity;
			$('#label_jumlah_' + lineNo).autoNumeric('set', jumlah);
		});
		
		$('#front_desk_detail_table').on('focus', '.discount-row', function() {
			$(this).select();
		});
		
		$('#front_desk_detail_table').on('change input keyup', '.discount-row', function() {
			var lineNo = $(this).parent().parent().data('line_no');
			var hargaSatuan = parseFloat($('#label_harga_satuan_' + lineNo).autoNumeric('get'));
			var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
			var discount = parseFloat($(this).autoNumeric('get'));
			var jumlah = (hargaSatuan - discount) * quantity;
			$('#label_jumlah_' + lineNo).autoNumeric('set', jumlah);
		});
	
	};
	
	var formHandle = function() {
		
		$('#label_sub_total').autoNumeric('init', numericOptions);
		$('#dlg_deposit_disp_nominal').autoNumeric('init', numericOptions);
		
		$('.styled').uniform({
			radioClass: 'choice'
		});
		
		$('#tambah_deposit_button').on('click', function() {
			
			$('#dlg_deposit_metode_bayar').val(1);
			fillBank(0, '#dlg_deposit_bank');
			$('#dlg_deposit_no_kartu').val('');
			$('#dlg_deposit_old_nominal').val(0);
			$('#dlg_deposit_nominal').val(0);
			$('#dlg_deposit_disp_nominal').autoNumeric('set', 0);
			$('#dlg_deposit_pasien_id').val($('#view_pasien_id').val());
			$('#dlg_deposit_nama').val($('#view_nama').val());
			
			$('#deposit_modal').modal({backdrop: 'static'});
			$('#deposit_modal').modal('show');
		});
		
		$('#dlg_deposit_disp_nominal').on('focus', function() {
			$(this).select();
		});
		
		$('#dlg_deposit_disp_nominal').on('change input keyup', function() {
			$('#dlg_deposit_nominal').val($(this).autoNumeric('get'));
		});
		
		$('#dlg_deposit_simpan_button').on('click', function() {
			var tanggal = moment(new Date()).format("YYYY/MM/DD HH:mm:ss");
			var formatTanggal = moment(new Date()).format("DD/MM/YYYY");
			var metodeBayar = parseInt($('#dlg_deposit_metode_bayar').val());
			var kasBankId = $('#dlg_deposit_bank').val();
			var noKartu = $('#dlg_deposit_no_kartu').val();
			var pasienId = $('#dlg_deposit_pasien_id').val();
			var nama = $('#dlg_deposit_nama').val();
			var oldNominal = parseFloat($('#dlg_deposit_old_nominal').val());
			var newNominal = parseFloat($('#dlg_deposit_nominal').val());
			var dispMetodeBayar = '';
			switch (metodeBayar) {
				case 1:
					dispMetodeBayar = "Cash";
					break;
				case 2:
					dispMetodeBayar = "Debit";
					break;
				case 3:
					dispMetodeBayar = "Kredit";
					break;
			}
			
			lastLineNo++;
			var lineNo = lastLineNo;
			
			var rows = ''
			rows = '<tr>';
			rows += '	<td style="padding:8px;">';
			rows += '		<input type="hidden" id="deposit_detail_id_' + lineNo + '" name="deposit_detail_id[]" value="new_deposit_detail_id_' + lineNo + '" />';
			rows += '		<input type="hidden" id="deposit_tanggal_' + lineNo + '" name="deposit_tanggal[]" value="' + tanggal + '" />';
			rows += '		<input type="hidden" id="deposit_kas_bank_id_' + lineNo + '" name="deposit_kas_bank_id[]" value="' + kasBankId + '" />';
			rows += '		<input type="hidden" id="deposit_no_kartu_' + lineNo + '" name="deposit_no_kartu[]" value="' + noKartu + '" />';
			rows += '		<input type="hidden" id="deposit_pasien_id_' + lineNo + '" name="deposit_pasien_id[]" value="' + pasienId + '" />';
			rows += '		<input type="hidden" id="deposit_nama_' + lineNo + '" name="deposit_nama[]" value="' + nama + '" />';
			rows += '		<label style="margin-bottom:0;">' + formatTanggal + '</label>';
			rows += '	</td>';
			rows += '	<td style="padding:8px;text-align:right;">';
			rows += '		<input type="hidden" id="deposit_nominal_' + lineNo + '" name="deposit_nominal[]" value="' + newNominal + '" />';
			rows += '		<label style="margin-bottom:0;">' + newNominal.formatMoney(2, ",", ".") + '</label>';
			rows += '	</td>';
			rows += '	<td style="padding:8px;">';
			rows += '		<input type="hidden" id="deposit_metode_bayar_' + lineNo + '" name="deposit_metode_bayar[]" value="' + metodeBayar + '" />';
			rows += '		<label style="margin-bottom:0;">' + dispMetodeBayar + '</label>';
			rows += '	</td>';
			rows += '	<td style="padding:2px;text-align:center;vertical-align:middle;">';
			rows += '		<a class="edit-row"><i class="fa fa-edit"></i></a>';
			rows += '	</td>';
			rows += '</tr>';
			$('#deposit_detail_footer_section').before(rows);
            var $tr = $('#deposit_detail_footer_section').prev('tr');
			$tr.data('line_no', lineNo);
			
			var oldSubTotal = parseFloat($('#sub_total').val());
			var newSubTotal = (oldSubTotal - oldNominal) + newNominal;
			
			$('#sub_total').val(newSubTotal);
			$('#label_sub_total').autoNumeric('set', newSubTotal);
			
			$('#deposit_modal').modal('hide');
		});
		
		$('#deposit_detail_table').on('click', '.edit-row', function() {
			$('#dlg_deposit_metode_bayar').val(1);
			fillBank(0, '#dlg_deposit_bank');
			$('#dlg_deposit_no_kartu').val('');
			$('#dlg_deposit_old_nominal').val(0);
			$('#dlg_deposit_nominal').val(0);
			$('#dlg_deposit_disp_nominal').autoNumeric('set', 0);
			
			$('#deposit_modal').modal({backdrop: 'static'});
			$('#deposit_modal').modal('show');
		});
		
		$('#disp_tanggal').daterangepicker({
			autoUpdateInput: true,
			autoapply: true,
			singleDatePicker: true, 
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
		});
		
		$('#disp_tanggal_lahir').daterangepicker({
			autoUpdateInput: false,
			autoapply: true,
			singleDatePicker: true, 
			showDropdowns: true,
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal_lahir').val(chosen_date.format('YYYY-MM-DD'));
			calculateAge();
		});
		
		$('#disp_tanggal_lahir').on('apply.daterangepicker', function (ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY'));
		});

		$('#disp_tanggal_lahir').on('cancel.daterangepicker', function (ev, picker) {
			$(this).val('');
		});
		
		$('#disp_pasien_filter_tanggal_lahir').daterangepicker({
			autoUpdateInput: false,
			autoapply: true,
			singleDatePicker: true, 
			showDropdowns: true,
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#pasien_filter_tanggal_lahir').val(chosen_date.format('YYYY-MM-DD'));
		});
		
		$('#disp_pasien_filter_tanggal_lahir').on('apply.daterangepicker', function (ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY'));
		});

		$('#disp_pasien_filter_tanggal_lahir').on('cancel.daterangepicker', function (ev, picker) {
			$(this).val('');
		});
		
		$('#pasien_button').on('click', function() {
			reloadLookupPasien();
			
			$('#lookup_pasien_modal').modal({backdrop: 'static'});
			$('#lookup_pasien_modal').modal('show');
		});
		
		$('#lookup_pasien_table').on('click', '.select-pasien', function(event) {
			event.preventDefault();
			
			var pasienId = $(this).data('id');
			var noRM = $(this).data('no_rm');
			var nama = $(this).data('nama');
			var alamat = $(this).data('alamat');
			var tanggalLahir = $(this).data('tanggal_lahir');
			
			$('#disp_no_rm').val(noRM);
			$('#nama').val(nama);
			$('#alamat').val(alamat);
			$('#tanggal_lahir').val(tanggalLahir);
			$('#disp_tanggal_lahir').val(moment(tanggalLahir).format('DD/MM/YYYY'));
			
			calculateAge();
			
			$('#pasien_id').val(pasienId);
			
			$('#lookup_pasien_modal').modal('hide');
		});
		
		$('#rujukan_dari').on('change', function() {
			var rujukanDari = parseInt($(this).val());
			switch (rujukanDari) {
				case imediscode.BUKAN_RUJUKAN:
					$('#nama_perujuk_section').hide();
					$('#rumah_sakit_section').hide();
					break;
				case imediscode.RUJUKAN_FKTP:
					$('#nama_perujuk_section').show();
					$('#rumah_sakit_section').hide();
					$('#nama_perujuk').focus();
					break;
				case imediscode.RUJUKAN_FKRTL:
					$('#nama_perujuk_section').hide();
					$('#rumah_sakit_section').show();
					$('#rumah_sakit_id').focus();
					break;
			}
		});
		
		$('#cara_bayar_id').on('change', function() {
			var jenis = parseInt($('#cara_bayar_id option:selected').attr('data-jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_UMUM:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').hide();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. Jaminan');
					break;
				case imediscode.CARA_BAYAR_BPJS:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').show();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. SEP');
					break;
				case imediscode.CARA_BAYAR_JAMKESDA:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('No. Jamkesda');
					break;
				case imediscode.CARA_BAYAR_ASURANSI:
					$('#perusahaan_section').show();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_perusahaan').text('Asuransi');
					$('#title_no_jaminan').text('No. Anggota');
					fillPerusahaan(0, imediscode.CARA_BAYAR_ASURANSI, '#perusahaan_id', '#perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					$('#perusahaan_section').show();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_perusahaan').text('Perusahaan');
					$('#title_no_jaminan').text('NIK');
					fillPerusahaan(0, imediscode.CARA_BAYAR_PERUSAHAAN, '#perusahaan_id', '#perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_INTERNAL:
					$('#perusahaan_section').hide();
					$('#no_jaminan_section').show();
					$('#penjamin_section').hide();
					$('#penjamin_perusahaan_section').hide();
					$('#penjamin_no_jaminan_section').hide();
					$('#title_no_jaminan').text('NIK');
					break;
			}
		});
		
		$('#penjamin_id').on('change', function() {
			var jenis = parseInt($('#penjamin_id option:selected').attr('data-jenis'));
			switch (jenis) {
				case imediscode.CARA_BAYAR_ASURANSI:
					$('#penjamin_perusahaan_section').show();
					$('#penjamin_no_jaminan_section').show();
					$('#title_penjamin_perusahaan').text('Asuransi');
					$('#title_penjamin_no_jaminan').text('No. Anggota');
					fillPerusahaan(0, imediscode.CARA_BAYAR_ASURANSI, '#penjamin_perusahaan_id', '#penjamin_perusahaan_spinner');
					break;
				case imediscode.CARA_BAYAR_PERUSAHAAN:
					$('#penjamin_perusahaan_section').show();
					$('#penjamin_no_jaminan_section').show();
					$('#title_penjamin_perusahaan').text('Perusahaan');
					$('#title_penjamin_no_jaminan').text('NIK');
					fillPerusahaan(0, imediscode.CARA_BAYAR_PERUSAHAAN, '#penjamin_perusahaan_id', '#penjamin_perusahaan_spinner');
					break;
			}
		});
		
		var depositApp = {
			initDepositForm: function () {
				$("#deposit_form").validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						depositApp.addDeposit($(form));
					}
				});
			},
			addDeposit: function(form) {
				$.blockUI({ 
					message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Simpan data</span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 2,
						'-moz-border-radius': 2,
						backgroundColor: '#333'
					}
				});
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		depositApp.initDepositForm();
		
		$('#batal_1_button, #batal_2_button').on('click', function() {
			window.location = url_index;
		});
		
		$('#nama').focus().select();
		
	}
	
    return {

        init: function() {
			
			formHandle();
			frontDeskDetailHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'simpan':
							$.unblockUI();
							window.location = url_index + '?uid=' + xhr.responseJSON.uid + '&from=form';
							break;
					};
				}
            });
			
        }

    };

}();