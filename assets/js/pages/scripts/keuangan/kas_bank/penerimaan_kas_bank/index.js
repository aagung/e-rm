var Index = function () {
    
    var oTableDaftarPenerimaanBank;
	var oTableInfo;
    
    var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
    
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	
    var handleDaftarPenerimaanBank = function() {
		
		function format(data) {
			var sOut = '<table class="table detail">';
            sOut += '    <thead>';
            sOut += '        <tr>';
            sOut += '            <th style="width:12%;">Kode Perk.</th>';
            sOut += '            <th>Nama Perkiraan</th>';
            sOut += '            <th style="width:15%;">Debit (Rp)</th>';
            sOut += '            <th style="width:15%;">Kredit (Rp)</th>';
            sOut += '        </tr>';
            sOut += '    </thead>';
            sOut += '    <tbody>';
			for (var i = 0; i < data.penerimaan_kas_bank_detail_list.length; i++) {
				sOut += '        <tr>';
				sOut += '            <td>' + data.penerimaan_kas_bank_detail_list[i].kode_perkiraan + '</td>';
				sOut += '            <td>' + data.penerimaan_kas_bank_detail_list[i].nama_perkiraan + '</td>';
				sOut += '            <td style="width:15%;text-align:right;">' + data.penerimaan_kas_bank_detail_list[i].disp_debit + '</td>';
				sOut += '            <td style="width:15%;text-align:right;">' + data.penerimaan_kas_bank_detail_list[i].disp_kredit + '</td>';
				sOut += '        </tr>';
			}
            sOut += '    <tfoot>';
            sOut += '        <tr>';
            sOut += '            <th colspan="4">Jumlah</th>';
            sOut += '            <th style="text-align: right;">' + data.disp_jumlah_debit + '</th>';
            sOut += '            <th style="text-align: right;">' + data.disp_jumlah_kredit + '</th>';
            sOut += '        </tr>';
            sOut += '    </tfoot>';
            sOut += '    </tbody>';
            sOut += '</table>';
			return sOut;
		}
	
        oTableDaftarPenerimaanBank = $('#daftar_penerimaan_kas_bank_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"          : {
                                  "url"     : url_load_data,
                                  "type"    : "POST",
                                  "data"    : function(d) {
									  d.tanggal_dari = $('#search_tanggal_dari').val();
									  d.tanggal_sampai = $('#search_tanggal_sampai').val();
                                  },
            },
            "order"         : [[ 3, "asc" ]],
            "columns": [
				{"data": "detail", "name": "detail", "class": "details-control", "width": "5%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<span data-id="' + row.id + '"></span>';
				}},
                {"data": "tanggal", "name": "tanggal", "width": "10%", "render": function(data, type, row, meta) {
					return moment(data).format('DD/MM/YYYY');
				}},
                {"data": "no_bukti", "name": "no_bukti"},
				{"data": "keterangan", "name": "keterangan"},
                {"data": "jumlah", "name": "jumlah", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}},
				{"data": "uid", "name": "uid", "width": "10%", "className": "text-center", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var button = '';
					if (parseInt(row.posting)) {
						button = '<button class="bg-info btn-xs unposting-button" type="button" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Unposting">Unposting</button>';
					}
					else {
						button = '<button class="bg-info btn-xs posting-button" type="button" data-id="' + row.id + '" data-uid="' + row.id + '" title="Posting">Posting</button>';
					}
					return button;
				}}
            ]
        });
		
		$('#daftar_penerimaan_kas_bank_table tbody').on('click', 'td.details-control', function() {
			var tr = $(this).closest('tr');
			var row = oTableDaftarPenerimaanBank.row(tr);
	 
			if (row.child.isShown()) {
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var id = $(this).children().data("id");
				$.getJSON(url_get_penerimaan_kas_bank_detail + "?id=" + id, function(data, status) {
                    if (status === 'success') {
                        row.child(format(data), 'no-padding').show();
						tr.addClass('shown');
                    }
                });
			}
		});
        
    };
    
    return {

        init: function () {
            
            handleDaftarPenerimaanBank();
			
			$('#disp_search_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#search_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				oTableDaftarPenerimaanBank.ajax.reload();
			});
			
			$('#disp_search_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#search_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				oTableDaftarPenerimaanBank.ajax.reload();
			});
			
			$('#tambah_button').on('click', function() {
				window.location = url_edit + '?uid=&page=' + oTableInfo.page;
				return false;
			});
			
			$('#daftar_penerimaan_bank_table').on('click', '.edit-row', function(e) {
				e.preventDefault();
				var id = $(this).data('id');
				window.location = url_edit + '?id=' + id + '&page=' + oTableInfo.page;
				return false;
			});
			
			$('#daftar_penerimaan_bank_table').on('click', '.posting-button', function(e) {
				e.preventDefault();
				var id = $(this).data('id');
				bootbox.confirm("Posting transaksi tersebut?", function(result) {
					if (result) {
						$.blockUI({ 
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#1b2024',
								opacity: 0.8,
								zIndex: 1200,
								cursor: 'wait'
							},
							css: {
								border: 0,
								color: '#fff',
								padding: 0,
								zIndex: 1201,
								backgroundColor: 'transparent'
							}
						});
						$.getJSON(url_posting + '?id=' + id, function(data, status) {
							if (status === 'success') {
								oTableDaftarPenerimaanBank.ajax.reload();
								showMessage('Hapus', 'Data tersebut sudah di Posting!', 'success');
							}
							else {
								showMessage('Hapus', 'Posting transaksi gagal!', 'danger');
							}
							$.unblockUI();
						});
					}
				});
				return false;
			});
			
			$('#daftar_penerimaan_bank_table').on('click', '.unposting-button', function(e) {
				e.preventDefault();
				var id = $(this).data('id');
				bootbox.confirm("Unposting transaksi tersebut?", function(result) {
					if (result) {
						$.blockUI({ 
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#1b2024',
								opacity: 0.8,
								zIndex: 1200,
								cursor: 'wait'
							},
							css: {
								border: 0,
								color: '#fff',
								padding: 0,
								zIndex: 1201,
								backgroundColor: 'transparent'
							}
						});
						$.getJSON(url_unposting + '?id=' + id, function(data, status) {
							if (status === 'success') {
								oTableDaftarPenerimaanBank.ajax.reload();
								showMessage('Hapus', 'Data tersebut sudah di Unposting!', 'success');
							}
							else {
								showMessage('Hapus', 'Unposting transaksi gagal!', 'danger');
							}
							$.unblockUI();
						});
					}
				});
				return false;
			});
			
			$('#daftar_penerimaan_bank_table').on('click', '.hapus-button', function() {
				var id = $(this).data('id');
				bootbox.confirm("Hapus data tersebut?", function(result) {
					if (result) {
						$.getJSON(url_delete + '?id=' + id, function(data, status) {
							if (status === 'success') {
								oTableDaftarPenerimaanBank.ajax.reload();
								showMessage('Hapus', 'Data tersebut sudah dihapus!', 'success');
							}
							else {
								showMessage('Hapus', 'Penghapusan data gagal!', 'danger');
							}
						});
					}
				});
				return false;
			});
			
			$('#disp_filter_kasir_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_kasir_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				oTableDaftarKasir.ajax.reload();
			});
			
			$('#disp_filter_kasir_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_kasir_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				oTableDaftarKasir.ajax.reload();
			});
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
                switch (xhr.responseJSON.tag) {
                    case 'load_data':
                        oTableInfo = oTableDaftarPenerimaanBank.page.info();
                        break;
                };
            });
			
        }

    };

}();