var Edit = function () {

	var oTableLookupPerkiraan = null;
	var lastLineNo = 0;
	
	var oNumericOptions = {
		aSep: '.', 
		aDec: ',', 
		mDec: 2, 
		vMax: '99999999999999.99', 
		vMin: '-99999999999999.99'
	};
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleLookupPerkiraan = function() {
        
        oTableLookupPerkiraan = $('#lookup_perkiraan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_perkiraan,
                "type": "POST"
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode", "name": "kode",
					"render": function(data, type, row, meta) {
						var sDataTag = '';
						var sLink = '';
						switch (parseInt(row.jenis)) {
							case 1:
								sLink = data;
								break;
							case 2:
								if (parseInt(row.has_rincian) === 0) {
									sDataTag += ' data-id="' + row.id + '"';
									sDataTag += ' data-uid="' + row.uid + '"';
									sDataTag += ' data-kode="' + row.kode + '"';
									sDataTag += ' data-nama="' + row.nama + '"';
									sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								}
								else {
									sLink = data;
								}
								break;
							case 3:
								sDataTag += ' data-id="' + row.id + '"';
								sDataTag += ' data-uid="' + row.uid + '"';
								sDataTag += ' data-kode="' + row.kode + '"';
								sDataTag += ' data-nama="' + row.nama + '"';
								sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								break;
						}
						return sLink;
				}, "width": "20%"},
				{ "data": "nama", "name": "nama" }
			],
			"createdRow": function( row, data, dataIndex ) {
				var indent = '';
				var lvl = parseInt(data.lvl) - 1;
				var nama = '';
				for (var i = 0; i < lvl; i++) {
					indent += '<span class="gi">|&mdash;</span>';
				}
				nama = indent + data.nama;
				
				if ((parseInt(data.jenis) === 1) || ((parseInt(data.jenis) === 2) && (parseInt(data.has_rincian) > 0))) {
					$(row).find('td:eq(0)').html('<strong>' + data.kode + '</strong>');
					$(row).find('td:eq(1)').html('<strong>' + nama + '</strong>');
				}
				else {
					$(row).find('td:eq(1)').html(nama);
				}
			}
        });
        
    };
	
	var reloadLookupPerkiraan = function() {
        if (oTableLookupPerkiraan == null) {
            handleLookupPerkiraan();
        }
        else {
            oTableLookupPerkiraan.ajax.reload();
        }
    };
	
	var fill = function(template, data) {
		$.each(data, function(key, value) {
			var placeholder = "{{" + key + "}}";
			var value = data[key];
			while (template.indexOf(placeholder) !== -1) {
				template = template.replace(placeholder, value);
			}
		});
		return template;
	}
	
	var handleForm = function() {

		$('#label_total').autoNumeric('init', oNumericOptions);
		
		$('#disp_tanggal').daterangepicker({
			//minDate: moment(),
			//maxDate: moment(new Date().setFullYear(new Date().getFullYear() + 1)),
			autoUpdateInput: true,
			autoapply: true,
			singleDatePicker: true, 
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
		});
		
		$('#disp_tanggal_realisasi').daterangepicker({
			//minDate: moment(),
			//maxDate: moment(new Date().setFullYear(new Date().getFullYear() + 1)),
			autoUpdateInput: true,
			autoapply: true,
			singleDatePicker: true, 
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal_realisasi').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
		});
		
		$('#disp_jenis_pasien').on('change', function() {
			var jenis = $(this).find('option:selected').data('jenis');
			$('#piutang_id').val($(this).val());
			switch (parseInt(jenis)) {
				case 0:
					$('#piutang_section').hide();
					$('#perusahaan_section').hide();
					break;
				case 2:
					$('#piutang_section').hide();
					$('#perusahaan_section').hide();
					break;
				case 3:
					$('#piutang_section').hide();
					$('#perusahaan_section').hide();
					break;
				case 5:
					$('#piutang_section').hide();
					$('#perusahaan_section').show();
					$('#title_asuransi').text('Asuransi');
					fillAsuransi(0, '#disp_Perusahaan');
					break;
				case 4:
					$('#piutang_section').hide();
					$('#perusahaan_section').show();
					$('#title_asuransi').text('Mitra');
					fillPerusahaan(0, '#disp_Perusahaan');
					break;
				case 6:
					$('#piutang_section').hide();
					$('#perusahaan_section').hide();
					break;
			}
		});
		
		$('#disp_perkiraan_bank').on('change', function() {
			$('#kp_bank_id').val($(this).val());
		});
		
		$('#tambah_button').click(function() {
			lastLineNo++;
			var lineNo = lastLineNo;
			
			var templateString = $('#edit-penerimaan-detail-template').html();
			var newString = fill(templateString, {line_no: lineNo});
			$('#penerimaan_kas_bank_detail_footer').before(newString);
			var $tr = $('#penerimaan_kas_bank_detail_footer').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#disp_jumlah_' + lineNo).autoNumeric('init', oNumericOptions);
			$('#disp_jumlah_' + lineNo).focus();
		});
		
		$('#penerimaan_kas_bank_detail_table').on('click', '.hapus_button', function(event) {
            var $tr = $(this).parent().parent();
			var lineNo = $(this).data('line_no');
			
			var oldJumlah = parseFloat($tr.find('#jumlah_' + lineNo).val());
			var oldTotal = parseFloat($('#total').val());
			var newTotal = (oldTotal - oldJumlah);
			
			$('#total').val(newTotal);
			$('#label_total').autoNumeric('set', newTotal);
			
			$tr.remove();
        });
		
		$('#penerimaan_kas_bank_detail_table').on('click', '.cari-perkiraan-button', function(e) {
			var $tr = $(this).parent().parent().parent().parent();
            var lineNo = $tr.data('line_no');
			
			$('#lookup_perkiraan_modal').data('line_no', lineNo);
			
			reloadLookupPerkiraan();
			
			$('#lookup_perkiraan_modal').modal({backdrop: 'static'});
			$('#lookup_perkiraan_modal').modal('show');
		});
		
		$('#lookup_perkiraan_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
			var lineNo = $('#lookup_perkiraan_modal').data('line_no');
			var id = $(this).data('id');
			var kode = $(this).data('kode');
			var nama = $(this).data('nama');
			
			$('#perkiraan_id_' + lineNo).val(id);
			$('#disp_kode_perkiraan_' + lineNo).val(kode);
			$('#label_nama_perkiraan_' + lineNo).text(nama);
			
			$('#disp_jumlah_' + lineNo).focus().select();
			
			$("#lookup_perkiraan_modal").modal("hide");
		});
        
		$('#penerimaan_kas_bank_detail_table').on('focus', '.jumlah-row', function(e) {
			$(this).select();
		});
		
		$('#penerimaan_kas_bank_detail_table').on('change', '.jumlah-row', function(e) {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
			var oldJumlah = parseFloat($('#jumlah_' + lineNo).val());
			var oldTotal = parseFloat($('#total').val());
			var newJumlah = parseFloat($('#disp_jumlah_' + lineNo).autoNumeric('get'));
			var newTotal = (oldTotal - oldJumlah) + newJumlah;
			
			$('#jumlah_' + lineNo).val(newJumlah);
			$('#total').val(newTotal);
			$('#label_total').autoNumeric('set', newTotal);
		});
		
        var penerimaanKasBankApp = {
            initPenerimaanKasBankForm: function () {
                $("#penerimaan_kas_bank_form").validate({
                    rules: {
						keterangan: { required: true, minlength: 1}
                    },
                    messages: {
						keterangan: { required: "Keterangan harus diisi" }
                    },
                    submitHandler: function (form) {
                        penerimaanKasBankApp.addPenerimaanKasBank($(form));
                    }
                });
            },
            addPenerimaanKasBank: function(form) {
				$.blockUI({ 
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#1b2024',
						opacity: 0.8,
						zIndex: 1200,
						cursor: 'wait'
					},
					css: {
						border: 0,
						color: '#fff',
						padding: 0,
						zIndex: 1201,
						backgroundColor: 'transparent'
					}
				});
                var postData = form.serialize();
                $.post(url_simpan, postData, function(data, status) {
                    if (status === "success") {
                        showMessage('Simpan', 'Record telah di simpan!', 'success');
                        return;
                    }
                    showMessage('Simpan', 'Record gagal di simpan!', 'error');
                }, 'json');
            }
        };
        penerimaanKasBankApp.initPenerimaanKasBankForm();
        
    };
    
    return {

        init: function() {
			
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});
			
			handleForm();
			
			$('#batal_button').on('click', function() {
				window.location = url_index;
				return false;
			});
			
			$.unblockUI();
			
			$(document).ajaxComplete(function(event, xhr, settings ) {
                switch (xhr.responseJSON.action) {
                    case 'simpan':
						$.unblockUI();
                        window.location = url_index;
                        break;
                };
            });
        }

    };

}();