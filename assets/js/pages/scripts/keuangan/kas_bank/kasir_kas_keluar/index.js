var Index = function () {
	
	var oDaftarPasienTable = null;
	var oDaftarHistoryTable = null;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'};

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleDaftarPasienTable = function() {

		oDaftarPasienTable = $('#pasien_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_pasien,
                "type": "POST",
				"data": function (d) {
					d.cara_bayar_id = $('#filter_cara_bayar_id').val();
					d.perusahaan_id = $('#filter_perusahaan_id').val();
					d.layanan_id = $('#pasien_filter_layanan_id').val();
					d.dokter_id = $('#pasien_filter_dokter_id').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_kwitansi", "name": "no_kwitansi", "width": "10%", "render": function(data, type, row, meta) {
					return data; //'<a class="edit-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + data + '</a>';
				}},
				{"data": "tanggal", "name": "tanggal", "width": "10%", "render": function(data, type, row, meta) {
					return moment(data).format('DD/MM/YYYY HH:MM');
				}},
				{"data": "no_rm", "name": "no_rm", "width": "10%"},
				{"data": "nama", "name": "nama", "render": function(data, type, row, meta) {
					return data;
				}},
				{"data": "jenis_unit", "name": "jenis_unit", "width": "10%", "render": function(data, type, row, meta) {
					var asalPasien = '';
					switch (parseInt(data)) {
						case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
							asalPasien = "Rawat Jalan";
							break;
						case imediscode.UNIT_LAYANAN_IGD:
							asalPasien = "IGD";
							break;
						case imediscode.UNIT_LAYANAN_RAWAT_DARURAT:
							asalPasien = "Rawat Inap";
							break;
					}
					return asalPasien;
				}},
				{"data": "layanan", "name": "layanan", "width": "10%"},
				{"data": "cara_bayar", "name": "cara_bayar", "width": "10%"},
				{"data": "dokter", "name": "dokter", "width": "10%"}
			],
			"createdRow": function( row, data, dataIndex ) {
				$('td:nth-child(1)', row).attr("title", data.no_kwitansi);
				$('td:nth-child(2)', row).attr("title", moment(data.tanggal).format('DD/MM/YYYY HH:MM'));
				$('td:nth-child(3)', row).attr("title", data.no_rm);
				$('td:nth-child(4)', row).attr("title", data.nama);
				$('td:nth-child(5)', row).attr("title", data.jenis_unit);
				$('td:nth-child(6)', row).attr("title", data.layanan);
				$('td:nth-child(7)', row).attr("title", data.cara_bayar);
				$('td:nth-child(8)', row).attr("title", data.dokter);
			}
		});
			
    };
	
	var reloadDaftarPasien = function() {
        if (oDaftarPasienTable == null) {
			handleDaftarPasienTable();
        }
		else {
			oDaftarPasienTable.ajax.reload();
		}
    };
	
	var handleDaftarHistoryTable = function() {

		oDaftarHistoryTable = $('#history_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_history,
                "type": "POST",
				"data": function (d) {
					d.tanggal_dari = $('#history_filter_tanggal_dari').val();
					d.tanggal_sampai = $('#history_filter_tanggal_sampai').val();
				}
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "no_kwitansi", "name": "no_kwitansi", "width": "10%", "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + data + '</a>';
				}},
				{"data": "tanggal", "name": "tanggal", "width": "15%", "render": function(data, type, row, meta) {
					return moment(data).format('DD/MM/YYYY HH:MM');
				}},
				{"data": "no_rm", "name": "no_rm", "width": "10%", "render": function(data, type, row, meta) {
					var sNoRm = '';
					if (row.no_rm === null) {
						sNoRm = "-";
					}
					else {
						sNoRm = row.no_rm;
					}
					return sNoRm;
				}},
				{"data": "nama", "name": "nama", "width": "10%", "render": function(data, type, row, meta) {
					var sNama = '';
					switch (parseInt(row.jenis)) {
						case 1:
							sNama = row.nama_pasien;
							break;
						case 2:
							sNama = row.nama_pasien;
							break;
						case 3:
							sNama = row.nama;
						case imediscode.JENIS_KASIR_PENGELUARAN_LAIN_LAIN:
							sNama = row.nama;
							break;
					}
					return sNama;
				}},
				{"data": "jenis_unit", "name": "jenis_unit", "render": function(data, type, row, meta) {
					var asalPasien = '';
					switch (parseInt(data)) {
						case imediscode.UNIT_LAYANAN_RAWAT_JALAN:
							asalPasien = "Rawat Jalan";
							break;
						case imediscode.UNIT_LAYANAN_IGD:
							asalPasien = "IGD";
							break;
						case imediscode.UNIT_LAYANAN_RAWAT_DARURAT:
							asalPasien = "Rawat Inap";
							break;
					}
					return asalPasien;
				}},
				{"data": "layanan", "name": "layanan", "width": "10%"},
				{"data": "cara_bayar", "name": "cara_bayar", "width": "10%"},
				{"data": "dokter", "name": "dokter", "width": "10%"}
			],
			"createdRow": function( row, data, dataIndex ) {
				$('td:nth-child(1)', row).attr("title", data.no_kwitansi);
				$('td:nth-child(2)', row).attr("title", moment(data.tanggal).format('DD/MM/YYYY HH:MM'));
				$('td:nth-child(3)', row).attr("title", data.no_rm);
				$('td:nth-child(4)', row).attr("title", data.nama);
				$('td:nth-child(5)', row).attr("title", data.jenis_unit);
				$('td:nth-child(6)', row).attr("title", data.layanan);
				$('td:nth-child(7)', row).attr("title", data.cara_bayar);
				$('td:nth-child(8)', row).attr("title", data.dokter);
			}
		});
		
		//$("#history_table_length label select").css("width", "auto").select2();
			
    };
	
	var reloadDaftarHistory = function() {
        if (oDaftarHistoryTable == null) {
			handleDaftarHistoryTable();
        }
		else {
			oDaftarHistoryTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			reloadDaftarHistory();
								
			$('#history_table_filter input[type="search"]')
				.attr('data-toggle', 'tooltip')
				.attr('data-placement', 'bottom')
				.attr('title', 'Masukan No. Kwitansi, No. Rm atau Nama Pasien disini.')
				.tooltip();
			
			$('#view_sub_total').autoNumeric('init', numericOptions);
			$('#view_non_tunai').autoNumeric('init', numericOptions);
			$('#view_tunai').autoNumeric('init', numericOptions);
			$('#view_uang_diterima').autoNumeric('init', numericOptions);
			$('#view_sisa').autoNumeric('init', numericOptions);
			$('#view_kembalian').autoNumeric('init', numericOptions);
			
			$('#pasien_filter_layanan_id').on('change', function() {
				reloadDaftarPasien();
			});
			
			$('#pasien_filter_dokter_id').on('change', function() {
				reloadDaftarPasien();
			});
			
			$("#pasien_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});
			
			$("#pasien_table").on("click", ".deposit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_deposit + '?uid=' + uid;
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
			$('#print_rincian_button').on('click', function() {
				$.getJSON(url_print_rincian_transaksi_pasien, function(data, status) {
					if (status === 'success') {
						window.open(data.file_name, '_blank');
					}
				});
			});
			
			$('#disp_history_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#history_filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarHistory();
			});
			
			$('#disp_history_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#history_filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarHistory();
			});
			
			$('#history_table').on('click', '.view-row', function(event) {
				event.preventDefault();
				
				var id = $(this).data('id');
				$.getJSON(url_get_rincian_transaksi_pasien + '?id=' + id, function(data, status) {
					if (status === 'success') {
						$('#view_tanggal').text(data.kasir.disp_tanggal);
						$('#view_no_kwitansi').text(data.kasir.no_kwitansi);
						$('#view_no_rm').text(data.kasir.no_rm);
						$('#view_nama').text(data.kasir.nama);
						$('#view_alamat').text(data.kasir.alamat);
						$('#view_jenis_kelamin').text(data.kasir.disp_jenis_kelamin);
						$('#view_tanggal_lahir').text(data.kasir.disp_tanggal_lahir);
						$('#view_umur').text(data.kasir.umur);
						$('#view_rujukan_dari').text(data.kasir.disp_rujukan_dari);
						switch (parseInt(data.kasir.rujukan_dari)) {
							case imediscode.BUKAN_RUJUKAN:
								$('#nama_perujuk_section').hide();
								$('#rumah_sakit_section').hide();
								break;
							case imediscode.RUJUKAN_FKTP:
								$('#nama_perujuk_section').show();
								$('#rumah_sakit_section').hide();
								break;
							case imediscode.RUJUKAN_FKRTL:
								$('#nama_perujuk_section').hide();
								$('#rumah_sakit_section').show();
								break;
						}
						$('#view_nama_perujuk').text(data.kasir.nama_perujuk);
						$('#view_rumah_sakit').text(data.kasir.disp_rumah_sakit);
						$('#view_cara_bayar').text(data.kasir.disp_cara_bayar);
						switch (parseInt(data.kasir.jenis_cara_bayar)) {
							case imediscode.CARA_BAYAR_UMUM:
								$('#perusahaan_section').hide();
								$('#no_jaminan_section').hide();
								break;
							case imediscode.CARA_BAYAR_BPJS:
								$('#perusahaan_section').hide();
								$('#title_no_jaminan').text('SEP');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_JAMKESDA:
								$('#perusahaan_section').hide();
								$('#title_no_jaminan').text('No. Jamkesda');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_ASURANSI:
								$('#perusahaan_section').show();
								$('#title_no_jaminan').text('No. Asuransi');
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_PERUSAHAAN:
								$('#perusahaan_section').show();
								$('#title_no_jaminan').text('NIP')
								$('#no_jaminan_section').show();
								break;
							case imediscode.CARA_BAYAR_INTERNAL:
								$('#perusahaan_section').hide();
								$('#title_no_jaminan').text('NIP')
								$('#no_jaminan_section').show();
								break;
						}
						$('#view_perusahaan').text(data.kasir.disp_perusahaan);
						$('#view_no_jaminan').text(data.kasir.no_jaminan);
						$('#view_layanan').text(data.kasir.disp_layanan);
						$('#view_dokter').text(data.kasir.disp_dokter);
						
						$('#view_sub_total').autoNumeric('set', data.kasir.sub_total);
						$('#view_non_tunai').autoNumeric('set', data.kasir.non_tunai);
						$('#view_tunai').autoNumeric('set', data.kasir.tunai);
						$('#view_uang_diterima').autoNumeric('set', data.kasir.uang_diterima);
						$('#view_sisa').autoNumeric('set', data.kasir.belum_bayar);
						$('#view_kembalian').autoNumeric('set', data.kasir.kembalian);
					}
				});
				
				$('#rincian_transaksi_pasien_modal').modal({backdrop: 'static'});
				$('#rincian_transaksi_pasien_modal').modal('show');
			});
			
			$('#history_table').on('click', '.batal-row', function(event) {
				event.preventDefault();
				
				var noKwitansi = $(this).data('no_kwitansi');
				var nama = $(this).data('nama');
				$('#batal_message').text('Kwitansi No. ' + noKwitansi + ' a.n. ' + nama + ' akan dibatalkan.');
				
				$('#batal_modal').modal({backdrop: 'static'});
				$('#batal_modal').modal('show');
			});
			
			$('#disp_piutang_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#piutang_filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarPiutang();
			});
			
			$('#disp_piutang_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: false,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#piutang_filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD'));
				reloadDaftarPiutang();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						/*case 'load_data_pasien':
							if (oDaftarHistoryTable === null) {
								reloadDaftarHistory();
								
								$('#history_table_filter input[type="search"]')
									.attr('data-toggle', 'tooltip')
									.attr('data-placement', 'bottom')
									.attr('title', 'Masukan No. Kwitansi, No. Rm atau Nama Pasien disini.')
									.tooltip();
							}
							break;*/
					};
				}
            });
			
        }

    };

}();