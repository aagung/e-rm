var Index = function () {

	var oTableTarget;
	var oTableInfo;
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
	
	var handleTargetTable = function() {

		oTableTarget = $('#target_table').DataTable({
			scrollY:        "300px",
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			fixedColumns:   {
				leftColumns: 2,
				rightColumns: 1
			},
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data"    : function(d) {
                    d.tahun = 2019;
                }
			},
            "order": [[ 0, "asc" ]],
			"columnDefs": [
				{ "orderable": false, "targets": "_all" },
				{ "width": 100, "targets": 14 }
			],
            "columns": [
				{"data": "kode", "name": "kode", "width": "10%", "render": function(data, type, row, meta) {
					return data;
				}},
				{"data": "nama", "name": "nama"},
				{"data": "target_1", "name": "target_1"},
				{"data": "target_1", "name": "target_2"},
				{"data": "target_1", "name": "target_3"},
				{"data": "target_1", "name": "target_4"},
				{"data": "target_1", "name": "target_5"},
				{"data": "target_1", "name": "target_6"},
				{"data": "target_1", "name": "target_7"},
				{"data": "target_1", "name": "target_8"},
				{"data": "target_1", "name": "target_9"},
				{"data": "target_1", "name": "target_10"},
				{"data": "target_1", "name": "target_11"},
				{"data": "target_1", "name": "target_12"},
				{"data": "total", "name": "total", "width": "15%"}
			]
		});
		
		$('#target_table').on('draw.dt', function() {
			$('#target_table thead tr').css('height', 0);
		});
			
    };
	
	var reloadTarget = function() {
        if (oTableTarget == null) {
			handleTargetTable();
        }
		else {
			oTableTarget.ajax.reload();
		}
    };
    
    return {

        init: function() {
			
			reloadTarget();
			
			$('#disp_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
				reloadTarget();
			});
			
			$('#disp_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
				reloadTarget();
			});
			
			$('#filter_button').on('click', function() {
				reloadTarget();
			});
			
			$(document).ajaxComplete(function(event, xhr, settings ) {

            });

        }

    };

}();