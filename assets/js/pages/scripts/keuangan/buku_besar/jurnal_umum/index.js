var Index = function () {
	
	var oDaftarJurnalUmumTable = null;
	var oTableInfo;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	
	var handleDaftarJurnalUmumTable = function() {
		
		function format(data) {
			var sOut = '<table class="table detail">';
            sOut += '    <thead>';
            sOut += '        <tr>';
            sOut += '            <th style="width:12%;">Kode Perk.</th>';
            sOut += '            <th>Nama Perkiraan</th>';
            sOut += '            <th style="width:15%;">Debit (Rp)</th>';
            sOut += '            <th style="width:15%;">Kredit (Rp)</th>';
            sOut += '        </tr>';
            sOut += '    </thead>';
            sOut += '    <tbody>';
			for (var i = 0; i < data.jurnal_umum_detail_list.length; i++) {
            sOut += '        <tr>';
            sOut += '            <td>' + data.jurnal_umum_detail_list[i].kode_perkiraan + '</td>';
            sOut += '            <td>' + data.jurnal_umum_detail_list[i].nama_perkiraan + '</td>';
            sOut += '            <td style="width:15%;text-align:right;">' + data.jurnal_umum_detail_list[i].disp_debit + '</td>';
            sOut += '            <td style="width:15%;text-align:right;">' + data.jurnal_umum_detail_list[i].disp_kredit + '</td>';
            sOut += '        </tr>';
			}
            sOut += '    <tfoot>';
            sOut += '        <tr>';
            sOut += '            <th colspan="2">Jumlah</th>';
            sOut += '            <th style="text-align: right;">' + data.disp_jumlah_debit + '</th>';
            sOut += '            <th style="text-align: right;">' + data.disp_jumlah_kredit + '</th>';
            sOut += '        </tr>';
            sOut += '    </tfoot>';
            sOut += '    </tbody>';
            sOut += '</table>';
			return sOut;
		}

		oDaftarJurnalUmumTable = $('#jurnal_umum_table').DataTable({
			"displayStart": start,
            "pageLength": length,
            "search": {
                "search": search
            },
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data"    : function( d ) {
					d.tanggal_dari = $('#filter_tanggal_dari').val();
					d.tanggal_sampai = $('#filter_tanggal_sampai').val();
				},
			},
            "order": [[ 1, "asc" ]],
            "columns": [
				{"data": "details", "name": "details", "class": "details-control", "width": "5%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<span data-id="' + row.id + '"></span>';
				}},
				{"data": "tanggal", "name": "tanggal", "width": "10%", "render": function(data, type, row, meta) {
					var link = '';
					if (parseInt(row.posting)) {
						link = moment(data).format('DD/MM/YYYY');
					}
					else {
						link = '<a class="edit-row" ref="#" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Edit">' + moment(data).format('DD/MM/YYYY') + '</a>';
					}
					return link;
				}},
				{"data": "no_bukti", "name": "no_bukti", "width": "15%"},
				{"data": "uraian", "name": "uraian"},
				{"data": "jumlah", "name": "jumlah", "width": "15%", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}},
				{"data": "uid", "name": "uid", "width": "10%", "className": "text-center", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var button = '';
					if (parseInt(row.posting)) {
						button = '<button class="bg-info btn-xs unposting-button" type="button" data-id="' + row.id + '" data-uid="' + row.uid + '" title="Unposting">Unposting</button>';
					}
					else {
						button = '<button class="bg-info btn-xs posting-button" type="button" data-id="' + row.id + '" data-uid="' + row.id + '" title="Posting">Posting</button>';
					}
					return button;
				}}
			]
		});
		
		$('#jurnal_umum_table tbody').on('click', 'td.details-control', function() {
			var tr = $(this).closest('tr');
			var row = oDaftarJurnalUmumTable.row(tr);
	 
			if (row.child.isShown()) {
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var id = $(this).children().data("id");
				$.getJSON(url_get_jurnal_umum_detail + "?id=" + id, function(data, status) {
                    if (status === 'success') {
                        row.child(format(data), 'no-padding').show();
						tr.addClass('shown');
                    }
                });
			}
		});
			
    };
	
	var reloadDaftarJurnalUmum = function() {
        if (oDaftarJurnalUmumTable == null) {
			handleDaftarJurnalUmumTable();
        }
		else {
			oDaftarJurnalUmumTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			$('#disp_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
			});
			
			$('#disp_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				showDropdowns: true,
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
			});
			
			reloadDaftarJurnalUmum();
			
			$('#tambah_button').on('click', function(event) {
				event.preventDefault();
				window.location = url_edit + '?uid=&tanggal_dari=' + $('#filter_tanggal_dari').val() + '&tanggal_sampai=' + $('#filter_tanggal_sampai').val() + '&search=' + oDaftarJurnalUmumTable.search();
			});
			
			$('#jurnal_umum_table').on("click", ".edit-row", function (event) {
				event.preventDefault();
				var uid = $(this).data('uid');
				var tanggalDari = $('#filter_tanggal_dari').val();
				var tanggalSampai = $('#filter_tanggal_sampai').val();
				window.location = url_edit + '?uid=' + uid + '&tanggal_dari=' + $('#filter_tanggal_dari').val() + '&tanggal_sampai=' + $('#filter_tanggal_sampai').val() + '&search=' + oDaftarJurnalUmumTable.search();
			});
			
			$('#jurnal_umum_table').on("click", ".posting-button", function (event) {
				var id = $(this).data('id');
				bootbox.confirm("Transaksi tersebut akan diposting?", function(result) {
					if (result) {
						$.getJSON(url_posting + '?id=' + id, function(data, status) {
							if (status === 'success') {
								alert('1 transaksi telah diposting!');
								reloadDaftarJurnalUmum();
							}
						});
					}
				});
			});
			
			$('#jurnal_umum_table').on("click", ".unposting-button", function (event) {
				var id = $(this).data('id');
				bootbox.confirm("Transaksi tersebut akan diunposting?", function(result) {
					if (result) {
						$.getJSON(url_unposting + '?id=' + id, function(data, status) {
							if (status === 'success') {
								alert('1 transaksi telah diunposting!');
								reloadDaftarJurnalUmum();
							}
						});
					}
				});
			});
			
			$('#filter_button').on('click', function() {
				reloadDaftarJurnalUmum();
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarJurnalUmum();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						case 'load_data':
							oTableInfo = oDaftarJurnalUmumTable.page.info();
							break;
					}
				}
            });
			
        }

    };

}();