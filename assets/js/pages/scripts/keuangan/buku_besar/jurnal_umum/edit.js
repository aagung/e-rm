var Edit = function () {

	var oTableLookupPerkiraan;
	var lastLineNo = 0;
	
	var oNumericOptions = {
		aSep: '.', 
		aDec: ',', 
		mDec: 2, 
		vMax: '99999999999999.99', 
		vMin: '-99999999999999.99'
	};
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};

	var handleLookupPerkiraan = function() {
        
        oTableLookupPerkiraan = $('#perkiraan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_perkiraan,
                "type": "POST"
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode", "name": "kode",
					"render": function(data, type, row, meta) {
						var sDataTag = '';
						var sLink = '';
						switch (parseInt(row.jenis)) {
							case 1:
								sLink = data;
								break;
							case 2:
								if (parseInt(row.has_rincian) === 0) {
									sDataTag += ' data-id="' + row.id + '"';
									sDataTag += ' data-uid="' + row.uid + '"';
									sDataTag += ' data-kode="' + row.kode + '"';
									sDataTag += ' data-nama="' + row.nama + '"';
									sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								}
								else {
									sLink = data;
								}
								break;
							case 3:
								sDataTag += ' data-id="' + row.id + '"';
								sDataTag += ' data-uid="' + row.uid + '"';
								sDataTag += ' data-kode="' + row.kode + '"';
								sDataTag += ' data-nama="' + row.nama + '"';
								sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								break;
						}
						return sLink;
				}, "width": "20%"},
				{ "data": "nama", "name": "nama" }
			],
			"createdRow": function( row, data, dataIndex ) {
				var indent = '';
				var lvl = parseInt(data.lvl) - 1;
				var nama = '';
				for (var i = 0; i < lvl; i++) {
					indent += '<span class="gi">|&mdash;</span>';
				}
				nama = indent + data.nama;
				
				if ((parseInt(data.jenis) === 1) || ((parseInt(data.jenis) === 2) && (parseInt(data.has_rincian) > 0))) {
					$(row).find('td:eq(0)').html('<strong>' + data.kode + '</strong>');
					$(row).find('td:eq(1)').html('<strong>' + nama + '</strong>');
				}
				else {
					$(row).find('td:eq(1)').html(nama);
				}
			}
        });
        
    };
	
	var reloadLookupPerkiraan = function() {
        if (oTableLookupPerkiraan == null) {
            handleLookupPerkiraan();
        }
        else {
            oTableLookupPerkiraan.ajax.reload();
        }
    };
	
	var fill = function(template, data) {
		$.each(data, function(key, value) {
			var placeholder = "{{" + key + "}}";
			var value = data[key];
			while (template.indexOf(placeholder) !== -1) {
				template = template.replace(placeholder, value);
			}
		});
		return template;
	}
	
	var handleForm = function() {

        $('#label_jumlah_debit').autoNumeric('init', oNumericOptions);
        $('#label_jumlah_kredit').autoNumeric('init', oNumericOptions);
        $('#label_selisih_debit').autoNumeric('init', oNumericOptions);
        $('#label_selisih_kredit').autoNumeric('init', oNumericOptions);
		
		$('#disp_tanggal').daterangepicker({
			autoUpdateInput: true,
			autoapply: true,
			singleDatePicker: true, 
			locale: {
				format: 'DD/MM/YYYY'
			}
		}, function(chosen_date) {
			$('#tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
		});
		
		$('#jurnal_umum_detail_footer').prevAll().each(function() {
			$(this).find('.debit-row').autoNumeric('init', oNumericOptions);
			$(this).find('.kredit-row').autoNumeric('init', oNumericOptions);
		});
		lastLineNo = $('#jurnal_umum_detail_footer').prevAll().length;
		
        $('#tambah_button').on('click', function() {
			lastLineNo++;
			var lineNo = lastLineNo;

            var templateString = $('#jurnal-umum-detail-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#jurnal_umum_detail_footer').before(newString);
            var $tr = $('#jurnal_umum_detail_footer').prev('tr');
			$tr.data('line_no', lineNo);
			
			$('#disp_kredit_' + lineNo).autoNumeric('init', oNumericOptions);
			$('#disp_debit_' + lineNo).autoNumeric('init', oNumericOptions);
		});

		$('#jurnal_umum_detail_table').on('click', '.hapus-button', function() {
            var $tr = $(this).parent().parent();
			var lineNo = $(this).data('line_no');
			
			var oldDebit = parseFloat($('#debit_' + lineNo).val());
			var oldKredit = parseFloat($('#kredit_' + lineNo).val());
			var oldJumlahDebit = parseFloat($('#jumlah_debit').val());
			var oldJumlahKredit = parseFloat($('#jumlah_kredit').val());
			var newJumlahDebit = (oldJumlahDebit - oldDebit);
			var newJumlahKredit = (oldJumlahKredit - oldKredit);
			var selisih = newJumlahDebit - newJumlahKredit;
            
            var selisihDebit = 0;
            var selisihKredit = 0;
            if (selisih > 0) {
                selisihKredit = selisih;
            }
            else {
                selisihDebit = selisih * (-1);
            }
			
			$('#jumlah_debit').val(newJumlahDebit);
			$('#label_jumlah_debit').autoNumeric('set', newJumlahDebit);
			$('#jumlah_kredit').val(newJumlahKredit);
			$('#label_jumlah_kredit').autoNumeric('set', newJumlahKredit);
			$('#selisih_debit').val(selisihDebit);
            $('#label_selisih_debit').autoNumeric('set', selisihDebit);
            $('#selisih_kredit').val(selisihKredit);
            $('#label_selisih_kredit').autoNumeric('set', selisihKredit);
			
			$tr.remove();
        });

        $('#jurnal_umum_detail_table').on('click', '.cari-perkiraan-button', function() {
            var $tr = $(this).parent().parent().parent().parent();
            var lineNo = $tr.data('line_no');

            $('#perkiraan_modal').data('line_no', lineNo);
            
            reloadLookupPerkiraan();
			
            $('#perkiraan_modal').modal({backdrop: 'static'});
            $('#perkiraan_modal').modal('show');
        });
        
        $('#perkiraan_table').on('click', '.select-row', function(event) {
			event.preventDefault();
			
            var id = $(this).data('id');
            var kode = $(this).data('kode');
            var nama = $(this).data('nama');
            var lineNo =  $('#perkiraan_modal').data('line_no');

			$('#perkiraan_id_' + lineNo).val(id);
            $('#disp_kode_perkiraan_' + lineNo).val(kode);
            $('#label_nama_perkiraan_' + lineNo).text(nama);

            $("#perkiraan_modal").modal("hide");
            
            var debit = parseFloat($('#selisih_debit').val());
            var kredit = parseFloat($('#selisih_kredit').val());
            var saldo = debit - kredit;
            
			if (saldo > 0) {
				$('#disp_debit_' + lineNo).autoNumeric('set', saldo);
				$('#disp_kredit_' + lineNo).autoNumeric('set', 0);
				$('#disp_debit_' + lineNo).focus();
			}
			else {
				$('#disp_debit_' + lineNo).autoNumeric('set', 0);
				$('#disp_kredit_' + lineNo).autoNumeric('set', saldo * (-1));
				if (saldo === 0) {
					$('#disp_debit_' + lineNo).focus();
				}
				else {
					$('#disp_kredit_' + lineNo).focus();
				}
			}
        });

        $('#jurnal_umum_detail_table').on('focus', '.debit-row', function() {
            this.select();
        });
		
		$('#jurnal_umum_detail_table').on('blur', '.debit-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
            var oldDebit = parseFloat($('#debit_' + lineNo).val());
			var oldJumlahDebit = parseFloat($('#jumlah_debit').val());
			var newDebit = parseFloat($(this).autoNumeric('get'));
			
			$('#debit_' + lineNo).val(newDebit);
			
			var newJumlahDebit = (oldJumlahDebit - oldDebit) + newDebit;
			
			var oldJumlahKredit = parseFloat($('#jumlah_kredit').val());
			var selisih = newJumlahDebit - oldJumlahKredit;
			
			var selisihDebit = 0;
            var selisihKredit = 0;
            if (selisih > 0) {
                selisihKredit = selisih;
            }
            else {
                selisihDebit = selisih * (-1);
            }
			
			$('#jumlah_debit').val(newJumlahDebit);
			$('#label_jumlah_debit').autoNumeric('set', newJumlahDebit);
			$('#selisih_debit').val(selisihDebit);
            $('#label_selisih_debit').autoNumeric('set', selisihDebit);
            $('#selisih_kredit').val(selisihKredit);
            $('#label_selisih_kredit').autoNumeric('set', selisihKredit);
        });
        
        $('#jurnal_umum_detail_table').on('change', '.debit-row', function() {
            var $tr = $(this).parent().parent();
            var lineNo = $tr.data('line_no');
            
            var kredit = parseFloat($('#disp_kredit_' + lineNo).autoNumeric('get'));
            
            if (kredit > 0) {
                $('#disp_kredit_' + lineNo).autoNumeric('set', 0);
            }
        });

        $('#jurnal_umum_detail_table').on('focus', '.kredit-row', function() {
            this.select();
        });
        
		$('#jurnal_umum_detail_table').on('blur', '.kredit-row', function() {
			var $tr = $(this).parent().parent();
			var lineNo = $tr.data('line_no');
            var oldKredit = parseFloat($('#kredit_' + lineNo).val());
			var oldJumlahKredit = parseFloat($('#jumlah_kredit').val());
			var newKredit = parseFloat($(this).autoNumeric('get'));
			
			$('#kredit_' + lineNo).val(newKredit);
			
			var newJumlahKredit = (oldJumlahKredit - oldKredit) + newKredit;
			
			var oldJumlahDebit = parseFloat($('#jumlah_debit').val());
			var selisih = oldJumlahDebit - newJumlahKredit;
			
			var selisihDebit = 0;
            var selisihKredit = 0;
            if (selisih > 0) {
                selisihKredit = selisih;
            }
            else {
                selisihDebit = selisih * (-1);
            }
			
			$('#jumlah_kredit').val(newJumlahKredit);
			$('#label_jumlah_kredit').autoNumeric('set', newJumlahKredit);
			$('#selisih_debit').val(selisihDebit);
            $('#label_selisih_debit').autoNumeric('set', selisihDebit);
            $('#selisih_kredit').val(selisihKredit);
            $('#label_selisih_kredit').autoNumeric('set', selisihKredit);
        });
		
        $('#jurnal_umum_detail_table').on('change', '.kredit-row', function() {
            var $tr = $(this).parent().parent();
            var lineNo = $tr.data('line_no');
            var debit = parseFloat($('#disp_debit_' + lineNo).autoNumeric('get'));
            
            if (debit > 0) {
                $('#disp_debit_' + lineNo).autoNumeric('set', 0);
            }
        });
        
        var jurnalUmumApp = {
            initJurnalUmumForm: function () {
                $("#jurnal_umum_form").validate({
                    rules: {
                        uraian: { required: true, minlength: 1 }
                    },
                    messages: {
                        uraian: { required: "Uraian harus diisi" }
                    },
                    submitHandler: function (form) {
                        jurnalUmumApp.addJurnalUmum($(form));
                    }
                });
            },
            addJurnalUmum: function(form) {
                var postData = form.serialize();
                $.post(url_simpan, postData, function(data, status) {
                    if (status === "success") {
                        showMessage('Simpan', 'Record telah di simpan!', 'success');
                        return;
                    }
                    showMessage('Simpan', 'Record gagal di simpan!', 'error');
                }, 'json');
            }
        };
        jurnalUmumApp.initJurnalUmumForm();
        
		$('#batal_button').on('click', function() {
			window.location = url_index + '?start=' + start + '&length=' + length + '&tanggal_dari=' + tanggalDari + '&tanggal_sampai=' + tanggalSampai + '&search=' + search;
		});
		
		$('#uraian').focus();
			
    };
    
    return {

        init: function() {
			
			handleForm();
			
			$(document).ajaxComplete(function(event, xhr, settings ) {
                switch (xhr.responseJSON.action) {
                    case 'simpan':
                        window.location = url_index;
                        break;
                };
            });
        }

    };

}();