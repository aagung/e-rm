var Index = function () {

	var oTableNeracaSaldo;
	var oTableInfo;
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
	
	var handleNeracaSaldoTable = function() {

		oTableNeracaSaldo = $('#neraca_saldo_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data,
                "type": "POST",
				"data"    : function(d) {
                    d.tanggal_dari = $('#filter_tanggal_dari').val();
                    d.tanggal_sampai = $('#filter_tanggal_sampai').val();
                }
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "kode", "name": "kode", "width": "10%", "render": function(data, type, row, meta) {
					return data;
				}},
				{"data": "nama", "name": "nama"},
				{"data": "saldo_awal_debit", "name": "saldo_awal_debit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}},
				{"data": "saldo_awal_kredit", "name": "saldo_awal_kredit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}},
				{"data": "pergerakan_debit", "name": "pergerakan_debit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}},
				{"data": "pergerakan_kredit", "name": "pergerakan_kredit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}},
				{"data": "saldo_akhir_debit", "name": "saldo_akhir_debit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}},
				{"data": "saldo_akhir_kredit", "name": "saldo_akhir_kredit", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					var saldo = '';
					switch (parseInt(row.jenis)) {
						case imediscode.KR_KELOMPOK:
							break;
						case imediscode.KR_RINCIAN:
							if (parseInt(row.has_rincian) === 0) {
								saldo = parseFloat(data).formatMoney(2, ",", ".");
							}
							break;
						case imediscode.KR_SUB_RINCIAN:
							saldo = parseFloat(data).formatMoney(2, ",", ".");
							break;
					}
					return saldo;
				}}
			],
			"createdRow": function( row, data, dataIndex ) {
				var indent = '';
				var lvl = parseInt(data.lvl) - 1;
				var nama = '';
				for (var i = 0; i < lvl; i++) {
					indent += '<span class="gi">|&mdash;</span>';
				}
				nama = indent + data.nama;
				
				if (parseInt(data.jenis) === 1) {
					$(row).find('td:eq(0)').html('<strong>' + data.kode + '</strong>');
					$(row).find('td:eq(1)').html('<strong>' + nama + '</strong>');
				}
				else {
					$(row).find('td:eq(1)').html(nama);
				}
				
				$('td:nth-child(1)', row).attr("title", data.kode);
				$('td:nth-child(2)', row).attr("title", data.nama);
				$('td:nth-child(3)', row).attr("title", parseFloat(data.saldo_awal_debit).formatMoney(2, ",", "."));
				$('td:nth-child(4)', row).attr("title", parseFloat(data.saldo_awal_kredit).formatMoney(2, ",", "."));
				$('td:nth-child(5)', row).attr("title", parseFloat(data.pergerakan_debit).formatMoney(2, ",", "."));
				$('td:nth-child(6)', row).attr("title", parseFloat(data.pergerakan_kredit).formatMoney(2, ",", "."));
				$('td:nth-child(7)', row).attr("title", parseFloat(data.saldo_akhir_debit).formatMoney(2, ",", "."));
				$('td:nth-child(8)', row).attr("title", parseFloat(data.saldo_akhir_kredit).formatMoney(2, ",", "."));
			}
		});
			
    };
	
	var reloadNeracaSaldo = function() {
        if (oTableNeracaSaldo == null) {
			handleNeracaSaldoTable();
        }
		else {
			oTableNeracaSaldo.ajax.reload();
		}
    };
    
    return {

        init: function() {
			
			reloadNeracaSaldo();
			
			$('#disp_filter_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
				reloadNeracaSaldo();
			});
			
			$('#disp_filter_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#filter_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
				reloadNeracaSaldo();
			});
			
			$('#filter_button').on('click', function() {
				reloadNeracaSaldo();
			});
			
			$(document).ajaxComplete(function(event, xhr, settings ) {

            });

        }

    };

}();