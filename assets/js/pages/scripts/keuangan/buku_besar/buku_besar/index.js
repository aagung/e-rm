var Index = function () {

	var oTableLookupPerkiraan = null;
	var oTableBukuBesar = null;
	var oTableInfo;
	var bFirst = true;
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	
	var handleLookupPerkiraan = function() {
        
        oTableLookupPerkiraan = $('#perkiraan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_perkiraan,
                "type": "POST"
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "kode", "name": "kode",
					"render": function(data, type, row, meta) {
						var sDataTag = '';
						var sLink = '';
						switch (parseInt(row.jenis)) {
							case 1:
								sLink = data;
								break;
							case 2:
								if (parseInt(row.has_rincian) === 0) {
									sDataTag += ' data-id="' + row.id + '"';
									sDataTag += ' data-uid="' + row.uid + '"';
									sDataTag += ' data-kode="' + row.kode + '"';
									sDataTag += ' data-nama="' + row.nama + '"';
									sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								}
								else {
									sLink = data;
								}
								break;
							case 3:
								sDataTag += ' data-id="' + row.id + '"';
								sDataTag += ' data-uid="' + row.uid + '"';
								sDataTag += ' data-kode="' + row.kode + '"';
								sDataTag += ' data-nama="' + row.nama + '"';
								sLink = '<a class="select-row"' + sDataTag + ' href="#">' + data + '</a>';
								break;
						}
						return sLink;
				}, "width": "20%"},
				{ "data": "nama", "name": "nama" }
			],
			"createdRow": function( row, data, dataIndex ) {
				var indent = '';
				var lvl = parseInt(data.lvl) - 1;
				var nama = '';
				for (var i = 0; i < lvl; i++) {
					indent += '<span class="gi">|&mdash;</span>';
				}
				nama = indent + data.nama;
				
				if (parseInt(data.jenis) === 1) {
					$(row).find('td:eq(0)').html('<strong>' + data.kode + '</strong>');
					$(row).find('td:eq(1)').html('<strong>' + nama + '</strong>');
				}
				else {
					$(row).find('td:eq(1)').html(nama);
				}
			}
        });
        
    };
    
	var reloadLookupPerkiraan = function() {
        if (oTableLookupPerkiraan == null) {
            handleLookupPerkiraan();
        }
        else {
            oTableLookupPerkiraan.ajax.reload();
        }
    };
	
	var handleBukuBesar = function() {
        oTableBukuBesar = $("#buku_besar_table").DataTable({
			"displayStart"	: 0,
			"processing"	: true,
            "serverSide"	: true,
			"ajax"          : {
                                  "url"     : url_load_data,
                                  "type"    : "POST",
                                  "data"    : function( d ) {
                                      d.perkiraan_id = $('#filter_perkiraan_id').val();
									  d.tanggal_dari = $('#filter_tanggal_dari').val();
									  d.tanggal_sampai = $('#filter_tanggal_sampai').val();
                                  },
            },
			"order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "tanggal", "name": "tanggal", "width": "10%", "render": function(data, type, row, meta) {
					return moment(data).format('DD/MM/YYYY');
				}},
				{ "data": "no_bukti", "name": "no_bukti", "width": "14%" },
				{ "data": "uraian", "name": "uraian" },
				{ "data": "debit", "name": "debit", "width": "14%", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}},
				{ "data": "kredit", "name": "kredit", "width": "14%", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}},
				{ "data": "saldo_debit", "name": "saldo_debit", "width": "14%", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}},
				{ "data": "saldo_kredit", "name": "saldo_kredit", "width": "14%", "render": function(data, type, row, meta) {
					return parseFloat(data).formatMoney(2, ",", ".");
				}}
			],
			"createdRow": function( row, data, dataIndex ) {
				$('td:nth-child(1)', row).attr("title", moment(data.tanggal).format('DD/MM/YYYY'));
				$('td:nth-child(2)', row).attr("title", data.no_bukti);
				$('td:nth-child(3)', row).attr("title", data.uraian);
			}
		});
    };
	
	var reloadBukuBesar = function() {
        if (oTableBukuBesar == null) {
            handleBukuBesar();
        }
        else {
            oTableBukuBesar.ajax.reload();
        }
    };
	
    return {

        init: function() {
			
			reloadBukuBesar();
			
			$('#disp_search_buku_besar_tanggal_dari').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#search_buku_besar_tanggal_dari').val(chosen_date.format('YYYY-MM-DD 00:00:00'));
				reloadBukuBesar();
			});
			
			$('#disp_search_buku_besar_tanggal_sampai').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#search_buku_besar_tanggal_sampai').val(chosen_date.format('YYYY-MM-DD 23:59:59'));
				reloadBukuBesar();
			});
			
			$('#perkiraan_button').on('click', function() {
				reloadLookupPerkiraan();
				$('#perkiraan_modal').modal({backdrop: 'static'});
				$('#perkiraan_modal').modal('show');
				return false;
			});
			
			$('#perkiraan_modal').on('shown.bs.modal', function () {
				$('#perkiraan_modal').animate({ scrollTop: 0 }, 'slow');
			});
			
			$('#perkiraan_table').on('click', '.select-row', function(event) {
				event.preventDefault();
				
				var id = $(this).data('id');
				var kode = $(this).data('kode');
				var nama = $(this).data('nama');
				
				$('#disp_perkiraan').val(kode + ' - ' + nama);
				
				$('#disp_kode').html(kode);
				$('#disp_nama').html(nama);
				$('#filter_perkiraan_id').val(id);
				
				reloadBukuBesar();
				
				$("#perkiraan_modal").modal("hide");
			});
			
			$(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON !== 'undefined') {
					switch (xhr.responseJSON.tag) {
						//
					}
				}
            });

        }

    };

}();