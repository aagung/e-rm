var Index = function () {
	
	var oKartuUtangTable = null;

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var handleKartuUtangTable = function() {

		oDaftarTagihanTable = $('#kartu_utang_table').DataTable({
            "processing":	true,
            "serverSide":	true,
            "ajax": {
                "url": url_load_data_kartu_utang,
                "type": "POST"
			},
            "order": [[ 0, "asc" ]],
            "columns": [
				{"data": "id", "name": "id", "width": "15%"},
				{"data": "tanggal", "name": "tanggal", "width": "15%", "render": function(data, type, row, meta) {
					return '<a class="kasir-row" data-id="' + row.id + '" data-uid="' + row.uid + '">' + moment(data).format('DD/MM/YYYY HH:MM') + '</a>';
				}},
				{"data": "no_faktur", "name": "no_faktur", "width": "18%"},
				{"data": "nama", "name": "nama", "width": "25%"},
				{"data": "jumlah", "name": "jumlah"},
				{"data": "uid", "name": "uid", "width": "10%", "orderable": false, "searchable": false, "render": function(data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				}, "className": "text-center"}
			]
		});
			
    };
	
	var reloadKartuUtang = function() {
        if (oKartuUtangTable == null) {
			handleKartuUtangTable();
        }
		else {
			oKartuUtangTable.ajax.reload();
		}
    };
	
    return {

        init: function() {
			
			handleKartuUtangTable();
			
			$("#pasien_table").on("click", ".kasir-row", function () {
				var uid = $(this).data('uid');
				window.location = url_kasir + '?from=front_desk&uid=' + uid;
			});
			
			$("#pasien_table").on("click", ".edit-row", function () {
				var uid = $(this).data('uid');
				window.location = url_edit + '?uid=' + uid;
			});
			
			$('#btn-refresh').on('click', function() {
				reloadDaftarTarifPelayanan();
			});
			
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.action) {
						//
					};
				}
            });
			
        }

    };

}();