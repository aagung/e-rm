$(() => {
    let INITIALIZED = {
        daftar_pemeriksaan: false,
        daftar_approval_dokter: false,
        hasil_pemeriksaan: false,
        history_hasil_pemeriksaan: false,
    };

    let getStatus = (data) => {
        let content = '';
        switch (parseInt(data)) {
            case 1: // STATUS_DEFAULT
                content = 'Menunggu';
                break;
            case 2: // STATUS_HASIL
                content = 'Hasil';
                break;
            case 3: // STATUS_DOKTER_APPROVE
                content = 'Approve';
                break;
            case 4: // STATUS_DOKTER_REJECT
                content = 'Reject';
                break;
        }

        return content;
    }

    let initializeDaftarPemeriksaan = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        let link = URL.form.replace(':UID', row.uid);
                        return `<a href="${link}">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "created_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
                {
                    "data": "status",
                    "render": (data, type, row, meta) => {
                        return getStatus(data);
                    },
                    "className": "text-left"
                },
                {
                    "data": "created_at",
                    "render": (data, type, row, meta) => {
                        let lama_tunggu = `<span data-popup="tooltip" data-title="Lama Tunggu" class="" data-tanggal="${data}">Lama Tunggu</span>`;

                        return lama_tunggu;
                    },
                    "className": "text-left"
                },
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                TABLE_BADGE.html(n);
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE.on('click', function () {
            TABLE_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 0);
        });
        $("#table_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 1);
        });
        $("#table_search_kode_transaksi").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 2);
        });
        $("#table_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 3);
        });
        $("#table_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 4);
        });
        $("#table_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 5);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_search_ruang_id").closest('.form-group').hide();
                    $("#table_search_poli_id").closest('.form-group').show();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_search_ruang_id").closest('.form-group').show();
                    $("#table_search_poli_id").closest('.form-group').hide();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_search_ruang_id").closest('.form-group').hide();
                    $("#table_search_poli_id").closest('.form-group').hide();
                    $("#table_search_ruang_id").val(0).trigger('change');
                    $("#table_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 6);
        });
        $("#table_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 7);
        });


        $("#table_search_layanan_id").trigger('change');

        /**
         * EVENT SOURCE
         */
         let listen = () => {
            if (TABLE_EVENT_SOURCE) {
                 TABLE_EVENT_SOURCE.close();
             }
             let eventId = 'radiologi-daftar_pelaksanaan';
             TABLE_EVENT_SOURCE = new EventSource(URL.listen);
             TABLE_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DT.fnDraw(false);
                }
             });
         }

         listen();

         /**
          * LAMA TUNGGU
          */
        setInterval(() => {
            TABLE.find('tbody').find('tr').each((i, el) => {
                let tanggal = moment($(el).find('[data-tanggal]').data('tanggal'));
                let jam = moment().diff(tanggal, 'hours') % 24;
                let menit = moment().diff(tanggal, 'minutes') % 60;
                let detik = moment().diff(tanggal, 'seconds') % 60;

                let contentChunk = [];
                if (jam > 0) {
                    contentChunk.push(`${jam} Jam`);
                }
                if (menit > 0) {
                    contentChunk.push(`${menit} Menit`);
                }
                if (detik > 0) {
                    contentChunk.push(`${detik} Detik`);
                }
                $(el).find('[data-tanggal]').html(contentChunk.join(' '));
            });
        }, 1000);


        INITIALIZED.daftar_pemeriksaan = true;
    }

    let initializeDaftarApprovalDokter = () => {
        // DATATABLE
        TABLE_DOKTER_DT = TABLE_DOKTER.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataDokter,
            "columns": [
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        let link = URL.approve_dokter.replace(':UID', row.uid);
                        return `<a href="${link}">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "hasil_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_DOKTER_TIMER) clearTimeout(TABLE_DOKTER_TIMER);
                TABLE_DOKTER_TIMER = setTimeout(function () {
                    // blockElement(TABLE_DOKTER.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE_DOKTER_BADGE.html(n);
                // TABLE_DOKTER.unblock();
                TABLE_DOKTER.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE_DOKTER.on('click', function () {
            TABLE_DOKTER_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_dokter_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 0);
        });
        $("#table_dokter_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 1);
        });
        $("#table_dokter_search_kode_transaksi").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 2);
        });
        $("#table_dokter_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 3);
        });
        $("#table_dokter_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 4);
        });
        $("#table_dokter_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 5);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_dokter_search_ruang_id").closest('.form-group').hide();
                    $("#table_dokter_search_poli_id").closest('.form-group').show();
                    $("#table_dokter_search_ruang_id").val(0).trigger('change');
                    $("#table_dokter_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_dokter_search_ruang_id").closest('.form-group').show();
                    $("#table_dokter_search_poli_id").closest('.form-group').hide();
                    $("#table_dokter_search_ruang_id").val(0).trigger('change');
                    $("#table_dokter_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_dokter_search_ruang_id").closest('.form-group').hide();
                    $("#table_dokter_search_poli_id").closest('.form-group').hide();
                    $("#table_dokter_search_ruang_id").val(0).trigger('change');
                    $("#table_dokter_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_dokter_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 6);
        });
        $("#table_dokter_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 7);
        });


        $("#table_dokter_search_layanan_id").trigger('change');

        /**
         * EVENT SOURCE
         */
        let listen = () => {
            if (TABLE_DOKTER_EVENT_SOURCE) {
                TABLE_DOKTER_EVENT_SOURCE.close();
            }
            let eventId = 'radiologi-daftar_pelaksanaan_dokter';
            TABLE_DOKTER_EVENT_SOURCE = new EventSource(URL.listenDokter);
            TABLE_DOKTER_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DOKTER_DT.fnDraw(false);
                }
            });
        }

        listen();


        INITIALIZED.daftar_approval_dokter = true;
    }

    let initializeHasilPemeriksaan = () => {
        // DATATABLE
        TABLE_HASIL_PEMERIKSAAN_DT = TABLE_HASIL_PEMERIKSAAN.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataHasilPemeriksaan,
            "columns": [
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        let link = URL.form.replace(':UID', row.uid) + '/view';
                        return `<a href="${link}" target="_blank">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "dokter_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        let linkPrint = URL.print.replace(':UID', data);
                        let linkPrintPatologi = URL.printPatologi.replace(':UID', data);
                        // let btnPrint = `<a href="${linkPrint}" target="_blank" class="btn btn-default btn-xs"><i class="icon-printer"></i></a>`;
                        let btnPanggilPasien = `<button type="button" class="btn btn-primary btn-xs" data-action="panggil_pasien" data-uid="${data}">Panggil Pasien</button>`;

                        let btnPrint = `
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Cetak 
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="${linkPrint}" target="_blank"><i class="icon-printer"></i> Klinik</a></li>
                                    <li><a href="${linkPrintPatologi}" target="_blank"><i class="icon-printer"></i> Patologi</a></li>
                                </ul>
                            </div>
                        `;

                        return btnPanggilPasien + '&nbsp;' + btnPrint;
                    },
                    "className": "text-center"
                },
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_HASIL_PEMERIKSAAN_TIMER) clearTimeout(TABLE_HASIL_PEMERIKSAAN_TIMER);
                TABLE_HASIL_PEMERIKSAAN_TIMER = setTimeout(function () {
                    // blockElement(TABLE_HASIL_PEMERIKSAAN.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE_DOKTER_BADGE.html(n);
                // TABLE_HASIL_PEMERIKSAAN.unblock();
                TABLE_HASIL_PEMERIKSAAN.find('[data-popup=tooltip]').tooltip();
            }
        });

        TABLE_HASIL_PEMERIKSAAN.on('click', '[data-action="panggil_pasien"]', function () {
            let uid = $(this).data('uid');
            confirmDialog({
                title: 'Panggil Pasien Tersebut?',
                text: '',
                btn_confirm: 'Ya',
                url: URL.panggil_pasien,
                data: {
                    uid: uid
                },
                onSuccess: (res) => {
                    successMessage('Success', 'Pasien Berhasil Dipanggil.');
                    TABLE_HASIL_PEMERIKSAAN_DT.fnDraw(false);
                },
                onError: (error) => {
                    console.log('ERROR', error);
                    errorMessage('Error', 'Terjadi kesalahan.');
                }
            });
        });

        BTN_REFRESH_TABLE_HASIL_PEMERIKSAAN.on('click', function () {
            TABLE_HASIL_PEMERIKSAAN_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_hasil_pemeriksaan_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 0);
        });
        $("#table_hasil_pemeriksaan_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 1);
        });
        $("#table_hasil_pemeriksaan_search_kode_transaksi").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 2);
        });
        $("#table_hasil_pemeriksaan_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 3);
        });
        $("#table_hasil_pemeriksaan_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 4);
        });
        $("#table_hasil_pemeriksaan_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 5);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_hasil_pemeriksaan_search_ruang_id").closest('.form-group').hide();
                    $("#table_hasil_pemeriksaan_search_poli_id").closest('.form-group').show();
                    $("#table_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_hasil_pemeriksaan_search_ruang_id").closest('.form-group').show();
                    $("#table_hasil_pemeriksaan_search_poli_id").closest('.form-group').hide();
                    $("#table_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_hasil_pemeriksaan_search_ruang_id").closest('.form-group').hide();
                    $("#table_hasil_pemeriksaan_search_poli_id").closest('.form-group').hide();
                    $("#table_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_hasil_pemeriksaan_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 6);
        });
        $("#table_hasil_pemeriksaan_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(val, 7);
        });

        /**
         * Filter Range Tanggal
         */
        $("#table_hasil_pemeriksaan_search_tanggal").daterangepicker({
            startDate: moment(),
            endDate: moment(),
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            }
        });
        $("#table_hasil_pemeriksaan_search_tanggal").on('apply.daterangepicker', (ev, picker) => {
            let tanggal_dari = picker.startDate.format('YYYY-MM-DD');
            let tanggal_sampai = picker.endDate.format('YYYY-MM-DD');
            TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(tanggal_dari + ':' + tanggal_sampai, 8);
            // TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 8);
            // TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 9); // Karena Kolom Kurang Dari 9
        });
        $("#table_hasil_pemeriksaan_btn_search_tanggal").click(() => {
            $("#table_hasil_pemeriksaan_search_tanggal").data('daterangepicker').toggle();
        });

        $("#table_hasil_pemeriksaan_btn_search_tanggal").click(function () {
            $("#table_hasil_pemeriksaan_search_tanggal").data('daterangepicker').toggle();
        });

        $("#table_hasil_pemeriksaan_search_layanan_id").trigger('change');
        TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(moment().format('YYYY-MM-DD') + ':' + moment().format('YYYY-MM-DD'), 8);
        // TABLE_HASIL_PEMERIKSAAN_DT.fnFilter(moment().format('YYYY-MM-DD'), 9); // Karena Kolom Kurang Dari 9

        /**
         * EVENT SOURCE
         */
        let listen = () => {
            if (TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE) {
                TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE.close();
            }
            let eventId = 'radiologi-hasil_pemeriksaan';
            TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE = new EventSource(URL.listenHasilPemeriksaan);
            TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_HASIL_PEMERIKSAAN_DT.fnDraw(false);
                }
            });
        }

        listen();


        INITIALIZED.hasil_pemeriksaan = true;
    }

    let initializeHistoryHasilPemeriksaan = () => {
        // DATATABLE
        TABLE_HISTORY_HASIL_PEMERIKSAAN_DT = TABLE_HISTORY_HASIL_PEMERIKSAAN.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataHistoryHasilPemeriksaan,
            "columns": [
                {
                    "data": "kode_transaksi",
                    "render": (data, type, row, meta) => {
                        let link = URL.form.replace(':UID', row.uid) + '/view';
                        return `<a href="${link}" target="_blank">${data}</a>`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "dokter_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rm",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "cara_bayar",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "",
                    "render": (data, type, row, meta) => {
                        if (parseInt(row.ruang_id) > 0) {
                            return row.ruang;
                        }
                        return row.layanan;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        let linkPrint = URL.print.replace(':UID', data);
                        let btnPrint = `<a href="${linkPrint}" target="_blank" class="btn btn-default btn-xs"><i class="icon-printer"></i></a>`;

                        return btnPrint;
                    },
                    "className": "text-center"
                },
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_HISTORY_HASIL_PEMERIKSAAN_TIMER) clearTimeout(TABLE_HISTORY_HASIL_PEMERIKSAAN_TIMER);
                TABLE_HISTORY_HASIL_PEMERIKSAAN_TIMER = setTimeout(function () {
                    // blockElement(TABLE_HISTORY_HASIL_PEMERIKSAAN.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE_DOKTER_BADGE.html(n);
                // TABLE_HISTORY_HASIL_PEMERIKSAAN.unblock();
                TABLE_HISTORY_HASIL_PEMERIKSAAN.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE_HISTORY_HASIL_PEMERIKSAAN.on('click', function () {
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_history_hasil_pemeriksaan_search_no_rm").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 0);
        });
        $("#table_history_hasil_pemeriksaan_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 1);
        });
        $("#table_history_hasil_pemeriksaan_search_kode_transaksi").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 2);
        });
        $("#table_history_hasil_pemeriksaan_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 3);
        });
        $("#table_history_hasil_pemeriksaan_search_cara_bayar_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 4);
        });
        $("#table_history_hasil_pemeriksaan_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 5);

            switch (val) {
                case 'rawat_jalan':
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").closest('.form-group').hide();
                    $("#table_history_hasil_pemeriksaan_search_poli_id").closest('.form-group').show();
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_history_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
                case 'rawat_inap':
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").closest('.form-group').show();
                    $("#table_history_hasil_pemeriksaan_search_poli_id").closest('.form-group').hide();
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_history_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
                default:
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").closest('.form-group').hide();
                    $("#table_history_hasil_pemeriksaan_search_poli_id").closest('.form-group').hide();
                    $("#table_history_hasil_pemeriksaan_search_ruang_id").val(0).trigger('change');
                    $("#table_history_hasil_pemeriksaan_search_poli_id").val(0).trigger('change');
                    break;
            }
        });
        $("#table_history_hasil_pemeriksaan_search_ruang_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 6);
        });
        $("#table_history_hasil_pemeriksaan_search_poli_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(val, 7);
        });

        /**
         * Filter Range Tanggal
         */
        $("#table_history_hasil_pemeriksaan_search_tanggal").daterangepicker({
            startDate: moment(),
            endDate: moment(),
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            }
        });
        $("#table_history_hasil_pemeriksaan_search_tanggal").on('apply.daterangepicker', (ev, picker) => {
            let tanggal_dari = picker.startDate.format('YYYY-MM-DD');
            let tanggal_sampai = picker.endDate.format('YYYY-MM-DD');
            TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(tanggal_dari + ':' + tanggal_sampai, 8);
            // TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 8);
            // TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 9); // Karena Kolom Kurang Dari 9
        });
        $("#table_history_hasil_pemeriksaan_btn_search_tanggal").click(() => {
            $("#table_history_hasil_pemeriksaan_search_tanggal").data('daterangepicker').toggle();
        });

        $("#table_history_hasil_pemeriksaan_btn_search_tanggal").click(function () {
            $("#table_history_hasil_pemeriksaan_search_tanggal").data('daterangepicker').toggle();
        });


        $("#table_history_hasil_pemeriksaan_search_layanan_id").trigger('change');
        TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(moment().format('YYYY-MM-DD') + ':' + moment().format('YYYY-MM-DD'), 8);
        // TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnFilter(moment().format('YYYY-MM-DD'), 9); // Karena Kolom Kurang Dari 9

        /**
         * EVENT SOURCE
         */
        let listen = () => {
            if (TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE) {
                TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE.close();
            }
            let eventId = 'radiologi-history_hasil_pemeriksaan';
            TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE = new EventSource(URL.listenHistoryHasilPemeriksaan);
            TABLE_HASIL_PEMERIKSAAN_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_HISTORY_HASIL_PEMERIKSAAN_DT.fnDraw(false);
                }
            });
        }

        listen();


        INITIALIZED.history_hasil_pemeriksaan = true;
    }

    let initialize = () => {
        $('a[data-toggle=tab][href="#tab-pemeriksaan"').click(() => {
            if (! INITIALIZED.daftar_pemeriksaan) {
                initializeDaftarPemeriksaan();
            }
        });

        $('a[data-toggle=tab][href="#tab-dokter"').click(() => {
            if (! INITIALIZED.daftar_approval_dokter) {
                initializeDaftarApprovalDokter();
            }
        });

        $('a[data-toggle=tab][href="#tab-hasil_pemeriksaan"').click(() => {
            if (! INITIALIZED.hasil_pemeriksaan) {
                initializeHasilPemeriksaan();
            }
        });

        $('a[data-toggle=tab][href="#tab-history_hasil_pemeriksaan"').click(() => {
            if (! INITIALIZED.history_hasil_pemeriksaan) {
                initializeHistoryHasilPemeriksaan();
            }
        });

        $("a[data-toggle=tab]").closest('li').first().find('a[data-toggle]').click();
    }
    
    initialize();
});