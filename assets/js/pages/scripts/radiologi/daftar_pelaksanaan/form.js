$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                
                let tanggal = moment(data.created_at);
                let tanggal_lahir = moment(data.tanggal_lahir);
                let umur_tahun = tanggal.diff(tanggal_lahir, 'year');
                let umur_bulan = tanggal.diff(tanggal_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal.diff(tanggal_lahir, 'days') % 31.5);

                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_tanggal").html(tanggal.format('DD/MM/YYYY'));
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);
                $("#disp_dokter").html(data.dokter);
                $("#disp_diagnosa").html(data.diagnosa);
                $("#disp_no_rm").html(data.no_rm);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_poli_ruang").html(parseInt(data.ruang_id) > 0 ? data.ruang : data.layanan);

                /**
                 * Alasan Penolakan
                 */
                if (parseInt(data.reject_flag) == 1) {
                    $("#alert-reject").show();
                    $("#alert-reject").find('.content').html(data.reject_alasan);
                } else {
                    $("#alert-reject").hide();
                }

                /**
                 * INPUT HIDDEN
                 */
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#register_id").val(data.register_id);
                console.log('fillForm', data);
                getPemeriksaan(uid);
            }
        });
    }

    let fillUploadImageForm = (id) => {
        let container = MODAL_UPLOAD_IMAGES.find('.modal-body');
        container.empty();

        let uploader = $("<form/>")
            .addClass('dropzone')
            .appendTo(container);

        uploader.dropzone({
            url: URL.uploadImages.replace(':ID', id),
            paramName: "file", // The name that will be used to transfer the file
            dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
            maxFilesize: 1 // MB
        });

        MODAL_UPLOAD_IMAGES.modal('show');
    }

    let fillViewImageForm = (id) => {
        $.getJSON(URL.getImages.replace(':ID', id), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                TABLE_VIEW_IMAGES.trigger('clear');
                for (let d of data) {
                    TABLE_VIEW_IMAGES.trigger('add', [d]);
                }

                MODAL_VIEW_IMAGES.modal('show');
            }
        });

    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                HASIL_CONTAINER.empty();
                for (let d of data) {
                    addPemeriksaan(d);
                }
            }
        });
    }

    let addPemeriksaan = (data) => {
        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_no_foto = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_no_foto[]')
            .val(data.no_foto)
            .appendTo(col);
        let input_ukuran_foto_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_ukuran_foto_id[]')
            .val(data.ukuran_foto_id)
            .appendTo(col);
        let input_faktor_eksposisi = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_faktor_eksposisi[]')
            .val(data.faktor_eksposisi)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan</strong>`);
        fieldsetLegend.append(`&nbsp;&nbsp;`);

        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);

        let tableDiv = $("<div/>")
            .addClass('table-responsive')
            .appendTo(fieldsetCol);
        let table = $("<table/>")
            .addClass('table table-bordered')
            .appendTo(tableDiv);
        let tableHead = $("<thead/>")
            .appendTo(table);
        let tableBody = $("<tbody/>")
            .appendTo(table);
        tableHead.html(`
            <tr class="bg-slate">
                <th class="text-center">
                    Pemeriksaan
                </th>
                <th class="text-center" style="width: 15%">
                    No. Foto
                </th>
                <th class="text-center" style="width: 15%">
                    Ukuran
                </th>
                <th class="text-center" style="width: 15%">
                    Faktor Eksposisi
                </th>
                <th class="text-center" style="width: 10%">
                    BMHP
                </th>
                <th class="text-center" style="width: 15%">
                    Qty
                </th>
                <th>&nbsp;</th>
            </tr>
        `);

        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10')
            .val(data.catatan)
            .appendTo(col);

        row.append('<br/><br/><br/>');

        
        // TABLE BODY
        let tr = $("<tr/>")
            .appendTo(tableBody);

        let tdPemeriksaan = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let labelPemeriksaan = $("<label/>")
            .addClass('label-pemeriksaan')
            .html(data.nama)
            .appendTo(tdPemeriksaan);

        let tdNoFoto = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputNoFoto = $("<input/>")
            .prop('type', 'text')
            .prop('name', 'pemeriksaan_disp_no_foto[]')
            .addClass('form-control')
            .val(data.no_foto)
            .appendTo(tdNoFoto);

        let tdUkuranFoto = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputUkuranFotoId = $("<select/>")
            .prop('name', 'pemeriksaan_disp_ukuran_foto_id[]')
            .addClass('form-control')
            .appendTo(tdUkuranFoto);
        dispInputUkuranFotoId.append('<option value="0" selected>- Pilih Ukuran -</option>');
        for (let ukuran_foto of data.ukuran_foto) {
            dispInputUkuranFotoId.append(`
                <option value="${ukuran_foto.id}">${ukuran_foto.nama}</option>
            `);
        }
        dispInputUkuranFotoId.select2();
        dispInputUkuranFotoId.val(data.ukuran_foto_id || 0).trigger('change');

        let tdFaktorEksposisi = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputFaktorEksposisi = $("<input/>")
            .prop('type', 'text')
            .prop('name', 'pemeriksaan_disp_faktor_eksposisi[]')
            .addClass('form-control')
            .val(data.faktor_eksposisi)
            .appendTo(tdFaktorEksposisi);

        let tdBmhp = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnBmhp = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-primary btn-labeled btn-bmhp')
            .appendTo(tdBmhp);
        let labelBmhpQuantity = $("<b/>")
            .addClass('bmhp-count')
            .html(`<i>${data.bmhp.length}</i>`)
            .appendTo(btnBmhp);
        btnBmhp.append('BMHP');

        let tdQuantity = $("<td/>")
            .addClass('text-left')
            .html(data.quantity || 1)
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnUpload = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-link btn-upload')
            .html('<i class="icon-upload7"></i>')
            .appendTo(tdAction);
        let btnView = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-link btn-view')
            .html('<i class="icon-eye"></i>')
            .appendTo(tdAction);


        // Handler
        btnBmhp.on('click', () => {
            FORM_PENGGUNAAN_OBAT.trigger('fillForm', [{
                id: input_id.val(),
                bmhp: json_decode(input_bmhp.val()),
            }]);
        });

        dispInputNoFoto.on('change keyup blur', function () {
            input_no_foto.val($(this).val());
        });
        dispInputUkuranFotoId.on('change keyup blur', function () {
            input_ukuran_foto_id.val($(this).val());
        });
        dispInputFaktorEksposisi.on('change keyup blur', function () {
            input_faktor_eksposisi.val($(this).val());
        });

        btnUpload.on('click', () => {
            fillUploadImageForm(input_id.val());
        });

        btnView.on('click', () => {
            fillViewImageForm(input_id.val());
        });
    }

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                HASIL_CONTAINER.find('.row-pemeriksaan').each((i, el) => {
                    let dispCatatan = $(el).find('.disp_catatan');
                    let inputCatatan = $(el).find('[name="pemeriksaan_catatan[]"]');
                    if (dispCatatan.length > 0) {
                        inputCatatan.val(dispCatatan.summernote('code'));
                    }
                });

                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Hasil berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Hasil.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });
    }

    let initializePenggunaanObat = () => {
        FORM_PENGGUNAAN_OBAT.on('add', (evt, data) => {
            console.log('FORM_PENGGUNAAN_OBAT.add', data);
            let tbody = TABLE_PENGGUNAAN_OBAT.find('tbody');

            let tr = $("<tr/>")
                .appendTo(tbody);

            let input_obat = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat[]')
                .val(data.obat)
                .appendTo(tr);
            let input_obat_alias = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_alias[]')
                .val(data.obat_alias)
                .appendTo(tr);
            let input_obat_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_id[]')
                .val(data.obat_id)
                .appendTo(tr);
            let input_obat_kode = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_obat_kode[]')
                .val(data.obat_kode)
                .appendTo(tr);
            let input_pemeriksaan_id = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_pemeriksaan_id[]')
                .val(data.pemeriksaan_id)
                .appendTo(tr);
            let input_quantity = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_quantity[]')
                .val(data.quantity)
                .appendTo(tr);
            let input_satuan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan[]')
                .val(data.satuan)
                .appendTo(tr);
            let input_satuan_singkatan = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'penggunaan_bmhp_satuan_singkatan[]')
                .val(data.satuan_singkatan)
                .appendTo(tr);

            let tdKode = $("<td/>")
                .addClass('text-center')
                .html(data.obat_kode)
                .appendTo(tr);

            let tdNama = $("<td/>")
                .addClass('text-left')
                .html(data.obat)
                .appendTo(tr);

            let tdQuantity = $("<td/>")
                .addClass('text-left')
                .appendTo(tr);
            let dispInputQuantity = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control')
                .appendTo(tdQuantity);
            dispInputQuantity.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
            dispInputQuantity.autoNumeric('set', data.quantity);

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let btnDelete = $("<button/>")
                .addClass('btn btn-danger btn-xs btn-delete')
                .html('<i class="icon-cross3"></i>')
                .appendTo(tdAction);

            btnDelete.on('click', () => {
                tr.remove();
            });

            dispInputQuantity.on('change keyup blur', () => {
                input_quantity.val(dispInputQuantity.autoNumeric('get'));
            });
            dispInputQuantity.on('click', () => {
                dispInputQuantity.select();
            });
        });
        FORM_PENGGUNAAN_OBAT.on('clear', (evt) => {
            TABLE_PENGGUNAAN_OBAT.find('tbody').empty();
        })

        FORM_PENGGUNAAN_OBAT.on('fillForm', (evt, data) => {
            // TODO SHOW PENGGUNAAN OBAT
            FORM_PENGGUNAAN_OBAT.find('[name="id"]').val(data.id);

            FORM_PENGGUNAAN_OBAT.trigger('clear');
            for (let bmhp of data.bmhp) {
                FORM_PENGGUNAAN_OBAT.trigger('add', [bmhp]);
            }

            MODAL_PENGGUNAAN_OBAT.modal('show');
        });

        BTN_TAMBAH_PENGGUNAAN_OBAT.on('click', () => {
            MODAL_PENGGUNAAN_OBAT.modal('hide');
            MODAL_LOOKUP_OBAT.modal('show');
        });

        /**
         * FORM VALIDATION
         */
        FORM_PENGGUNAAN_OBAT.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockElement(MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').selector);
                let postData = $(form).serializeArray();

                let data = {};
                let d, fieldname, isArray;
                for (let i = 0; i < postData.length; i++) {
                    d = postData[i];
                    isArray = false;

                    if (d.name.search(/\[\]/) !== -1) {
                        fieldname = d.name.replace(/\[\]/, '');
                        isArray = true;
                    } else {
                        fieldname = d.name;
                        isArray = false;
                    }

                    if (isArray) {
                        if (! data[fieldname]) {
                            data[fieldname] = [];
                            data[fieldname].push(d.value);
                        } else {
                            data[fieldname].push(d.value)
                        }
                    } else {
                        data[fieldname] = d.value;
                    }
                }

                console.log('DATA', data);
                let obat;
                let list_obat = [];
                if (data.penggunaan_bmhp_obat_id) {
                    for (let i = 0; i < data.penggunaan_bmhp_obat_id.length; i++) {
                        obat = {
                            obat: data['penggunaan_bmhp_obat'][i],
                            obat_alias: data['penggunaan_bmhp_obat_alias'][i],
                            obat_id: data['penggunaan_bmhp_obat_id'][i],
                            obat_kode: data['penggunaan_bmhp_obat_kode'][i],
                            pemeriksaan_id: data['id'],
                            quantity: data['penggunaan_bmhp_quantity'][i],
                            satuan: data['penggunaan_bmhp_satuan'][i],
                            satuan_singkatan: data['penggunaan_bmhp_satuan_singkatan'][i],
                        };
                        list_obat.push(obat);
                    }
                }

                HASIL_CONTAINER.find('.row-pemeriksaan').each((i, el) => {
                    if ($(el).find('[name="pemeriksaan_id[]"]').val() == data.id) {
                        $(el).find('[name="pemeriksaan_bmhp[]"]').val(json_encode(list_obat));
                        $(el).find('.bmhp-count').html('<i>' + list_obat.length + '</i>');
                    }
                });

                MODAL_PENGGUNAAN_OBAT.find('.modal-dialog').unblock();
                MODAL_PENGGUNAAN_OBAT.modal('hide');
            }
        });
    }

    let initializeLookupObat = () => {
        /**
         * COLUMNS OPTIONS
         */
        let COLUMNS_OPT_LOOKUP_OBAT = [
            {
                "orderable": false,
                "render": (data, type, row, meta) => {
                    let dataAttr = `data-id="${row.id}" data-uid="${row.uid}" data-kode="${row.kode}" data-nama="${row.nama}" data-alias="${row.alias}" data-satuan="" data-satuan_singkatan=""`;

                    let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

                    return checkbox;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "kode",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-center"
            },
            {
                "orderable": true,
                "data": "nama",
                "render": (data, type, row, meta) => {
                    return data;
                },
                "className": "text-left"
            },
            // {
            //     "orderable": true,
            //     "data": "jenis",
            //     "render": (data, type, row, meta) => {
            //         return data;
            //     },
            //     "className": "text-left"
            // }
        ];

        TABLE_LOOKUP_OBAT.on('fetch', (evt, cb) => {
            $.getJSON(URL.fetchLookupObat, function (res, status) {
                if (status === 'success') {
                    TABLE_LOOKUP_OBAT_DT.clear().draw();
                    TABLE_LOOKUP_OBAT_DT.rows.add(res.data).draw();

                    if (cb) {
                        cb();
                    }
                } else {
                    //
                }
            });
        });

        TABLE_LOOKUP_OBAT.on('initialize', (evt) => {
            TABLE_LOOKUP_OBAT_DT = TABLE_LOOKUP_OBAT.DataTable({
                data: [],
                columns: COLUMNS_OPT_LOOKUP_OBAT,
                fnDrawCallback: function (oSettings) {
                    TABLE_LOOKUP_OBAT.find('tbody tr input[type=checkbox]').uniform();
                }
            });
        });

        BTN_LOOKUP_OBAT_SIMPAN.on('click', (evt) => {
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    let obat = {
                        obat: tr.find('input[type=checkbox]').data('nama'),
                        obat_alias: tr.find('input[type=checkbox]').data('alias'),
                        obat_id: tr.find('input[type=checkbox]').data('id'),
                        obat_kode: tr.find('input[type=checkbox]').data('kode'),
                        pemeriksaan_id: 0, // TODO PEMERIKSAAN ID
                        quantity: 1,
                        satuan: tr.find('input[type=checkbox]').data('satuan'),
                        satuan_singkatan: tr.find('input[type=checkbox]').data('satuan_singkatan'),
                    }

                    if (tr.find('input[type=checkbox]').is(':checked')) {
                        TABLE_PENGGUNAAN_OBAT.trigger('add', [obat]);
                    }
                });

                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
                MODAL_LOOKUP_OBAT.modal('hide');
            }, 500);
        });

        MODAL_LOOKUP_OBAT.on('shown.bs.modal', () => {
            TABLE_LOOKUP_OBAT.trigger('fetch');
        });
        MODAL_LOOKUP_OBAT.on('hide.bs.modal', () => {
            MODAL_PENGGUNAAN_OBAT.modal('show');
        });


        TABLE_LOOKUP_OBAT.trigger('initialize');
    }

    let initializeViewImages = () => {
        TABLE_VIEW_IMAGES.on('clear', (e) => {
            TABLE_VIEW_IMAGES.find('tbody').empty();
        });

        TABLE_VIEW_IMAGES.on('add', (e, data) => {
            let tbody = TABLE_VIEW_IMAGES.find('tbody');

            let tr = $("<tr/>")
                .appendTo(tbody);

            let tdFoto = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            tdFoto.html(`
                <a href="${data.link}" data-popup="lightbox">
                    <img src="${data.link}" alt="" class="img-rounded img-preview">
                </a>
            `);

            let tdMime = $("<td/>")
                .addClass('text-left')
                .html(data.mime)
                .appendTo(tr);

            let tdSize = $("<td/>")
                .addClass('text-right')
                .html(numeral(data.size / 1024).format(',.##') + ' Kb')
                .appendTo(tr);

            let tdAction = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            let btnDelete = $("<button/>")
                .prop('type', 'button')
                .addClass('btn btn-xs btn-danger btn-delete')
                .html('<i class="icon-trash"></i>')
                .appendTo(tdAction);

            btnDelete.on('click', () => {
                confirmDialog({
                    title: "Hapus Foto Tersebut?",
                    text: '',
                    btn_confirm: 'Hapus',
                    url: URL.deleteImage,
                    data: {
                        uid: data.uid
                    },
                    onSuccess: (res) => {
                        tr.remove();
                    },
                    onError: (error) => {
                        errorMessage('Error', 'Terjadi Kesalahan');
                    }
                });
            })
        });
    }

    fillForm(UID);
    initializeForm();
    initializePenggunaanObat();
    initializeLookupObat();
    initializeViewImages();
});