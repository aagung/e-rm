$(() => {
    let TREE;
    let FT; // Fancy Tree

    let initialize = () => {
        MODAL_RAD_SEARCH_PEMERIKSAAN.on('source', (e, data, selected) => {
            data = data || [];
            selected = selected || [];
            
            FT = null;

            if (TREE) {
                TREE.remove();
            }

            TREE = $("<div/>")
                .addClass('tree-rad_pemeriksaan tree-checkbox-hierarchical well');

            MODAL_RAD_SEARCH_PEMERIKSAAN.find('.tree-container').html(TREE);

            TREE.fancytree({
                extensions: ["filter"],
                quicksearch: true,
                filter: {
                    autoApply: true,   // Re-apply last filter if lazy data is loaded
                    autoExpand: true, // Expand all branches that contain matches while filtered
                    counter: false,     // Show a badge with number of matching child nodes near parent icons
                    fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                    hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                    hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                    highlight: true,   // Highlight matches by wrapping inside <mark> tags
                    leavesOnly: false, // Match end nodes only
                    nodata: true,      // Display a 'no data' status node if result is empty
                    mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                },
                source: data,
                checkbox: true,
                selectMode: 3,
            });

            FT = TREE.fancytree("getTree");

            setTimeout(() => {
                var rootNode = TREE.fancytree("getRootNode");
                rootNode.sortChildren(function(a, b) {
                    var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                    y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                    return x === y ? 0 : x > y ? 1 : -1;
                }, true);

                // Hide Unselected Node
                // Untuk menghilangkan parent yang tidak dipilih sama sekali
                // FT.visit(function (node) {
                //     node.setExpanded(true);
                //     if (!node.partsel && !node.selected) {
                //         $(node.li).addClass('hide');
                //     }
                // });

                // untuk menghilangkan node dari parent yang dipilih (Partial|semua)
                // FT.visit(function (node) {
                //     if (!node.partsel && !node.selected) {
                //         $(node.li).addClass('hide');
                //     }
                // });
            }, 500);

            // SET SELECTED
            let node;
            for (let sel_id of selected) {
                node = FT.getNodeByKey(sel_id);
                if (! node) node = FT.getNodeByKey(sel_id + '');

                if (node) {
                    node.setSelected(true);
                    node.setActive(true);
                    node.setExpanded(true);
                }
            }
        });

        BTN_RAD_SIMPAN_PEMERIKSAAN.on('click', () => {
            let data = [];

            let selectedNodes = FT.getSelectedNodes();
            for (let node of selectedNodes) {
                if (! node.isFolder()) {
                    data.push(node.data);
                }
            }

            console.log('BTN_RAD_SIMPAN_PEMERIKSAAN', data);

            if (MODAL_RAD_SEARCH_PEMERIKSAAN.onSave) {
                MODAL_RAD_SEARCH_PEMERIKSAAN.onSave(data);
            }
        });

        MODAL_RAD_SEARCH_PEMERIKSAAN.on('click', '[data-action=cari]', () => {
            if (FT) {
                let query = MODAL_RAD_SEARCH_PEMERIKSAAN.find('[name="nama"]').val();
                var filterFunc = FT.filterNodes;
                var opts = { autoExpand: true, highlight: true };

                // if (query != "") {
                    var n = filterFunc.call(FT, query, opts);
                // }
            }
        });

        MODAL_RAD_SEARCH_PEMERIKSAAN.find('[name="nama"]').on('keyup change blur', () => {
            let timer = MODAL_RAD_SEARCH_PEMERIKSAAN.find('[name="nama"]').data('timer');

            if (timer) {
                clearTimeout(timer);
            }

            timer = setTimeout(() => {
                MODAL_RAD_SEARCH_PEMERIKSAAN.find('[data-action=cari]').click();
            }, 2000);

            MODAL_RAD_SEARCH_PEMERIKSAAN.find('[name="nama"]').data('timer', timer);
        });
    }

    initialize();
});