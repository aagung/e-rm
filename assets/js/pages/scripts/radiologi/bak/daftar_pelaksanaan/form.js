$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                
                let tanggal = moment(data.created_at);
                let tgl_lahir = moment(data.tgl_lahir);
                let umur_tahun = tanggal.diff(tgl_lahir, 'year');
                let umur_bulan = tanggal.diff(tgl_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal.diff(tgl_lahir, 'days') % 31.5);

                // Pemeriksaan
                $("#disp_tanggal").html(tanggal.format('DD/MM/YYYY HH:mm'));
                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_kode_hasil").html(data.kode_hasil);
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_kelas").html(data.kelas);
                $("#disp_ruang").html(data.ruang);
                $("#disp_bed").html(data.bed);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);
                $("#disp_dokter").html(data.dokter);
                $("#disp_diagnosa").html(data.diagnosa);

                // Pasien
                $("#disp_no_rekam_medis").html(data.no_rekam_medis);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_alamat").html(data.alamat);
                $("#disp_tgl_lahir").html(tgl_lahir.format('DD/MM/YYYY'));
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_telepon").html(data.nomor_hp);

                if (parseInt(data.status) == 4) {
                    ALERT.show();
                    ALERT.find('.content').html(data.reject_alasan);
                } else {
                    ALERT.hide();
                }

                /**
                 * INPUT HIDDEN
                 */
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#register_id").val(data.register_id);
                console.log('fillForm', data);
                getPemeriksaan(uid);

                // TODO GET OBAT
            }
        });
    }

    let fillFormPreview = (pemeriksaan_id) => {
        $.getJSON(URL.getFotoPemeriksaan.replace(':ID', pemeriksaan_id), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                console.log('fillFormPreview', data);

                PREVIEW_CONTAINER.empty();
                let row = $("<div/>")
                    .addClass('row')
                    .appendTo(PREVIEW_CONTAINER);
                for (let foto of data) {
                    addFoto(foto, row);
                }
            }
        });
    }

    let fillFormInputHasil = (pemeriksaan_id) => {
        $.getJSON(URL.getHasilPemeriksaan.replace(':ID', pemeriksaan_id), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                FORM_INPUT_HASIL.find('input[name=id]').val(data.id);
                DISP_INPUT_HASIL.summernote('code', data.hasil);

                $.getJSON(URL.getFotoPemeriksaan.replace(':ID', pemeriksaan_id), (res, status) => {
                    if (status === 'success') {
                        let data = res.data;

                        FORM_INPUT_HASIL.find('.preview').empty();
                        let row = $("<div/>")
                            .addClass('row')
                            .appendTo(FORM_INPUT_HASIL.find('.preview'));
                        for (let foto of data) {
                            addFoto(foto, row);
                        }
                    }
                });
            }
        });
    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                for (let parentkey in data) {
                    for (let i = 0; i < data[parentkey].length; i++) {
                        addPemeriksaan(parentkey, data[parentkey][i]);
                    }
                }
            }
        });
    }

    let addPemeriksaan = (parentKey, data) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let parent = getPemeriksaanParent(parentKey);
        let elementAfter = getElementAfter(parentKey);
        // console.log(elementAfter);

        /**
         * Pemeriksaan
         */
        let level = parent.data('level') + 1;
        let trPemeriksaan = $("<tr/>").data('key', data.kode)
            .data('parent', parent.data('key'))
            .data('level', level);
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(trPemeriksaan);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(trPemeriksaan);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(trPemeriksaan);
        let input_ukuran_foto_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_ukuran_foto_id[]')
            .val(data.ukuran_foto_id)
            .appendTo(trPemeriksaan);

        let tdPemeriksaan = $("<td/>")
            .addClass('')
            .html('&nbsp'.repeat(level * 4) + data.nama)
            .appendTo(trPemeriksaan);

        let tdUkuran = $("<td/>")
            .addClass('')
            .html(data.ukuran_foto)
            .appendTo(trPemeriksaan);

        let tdHasilBaca = $("<td/>")
            .addClass('text-center')
            .appendTo(trPemeriksaan);
        let btnInput = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-default btn-xs mr-5 ml-5')
            .html('Input Hasil')
            .appendTo(tdHasilBaca);
        let btnUpload = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-primary btn-xs mr-5 ml-5')
            .html('<i class="icon-upload7"></i>')
            .appendTo(tdHasilBaca);
        let btnPreview = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-info btn-xs mr-5 ml-5')
            .html('<i class="icon-image5"></i>')
            .appendTo(tdHasilBaca);

        // HANDLER
        btnInput.on('click', function (e) {
            fillFormInputHasil(input_id.val());
            MODAL_INPUT_HASIL.modal('show');
        });

        btnUpload.on('click', function (e) {
            initializeFormUpload($("#uid").val(), input_id.val());
            MODAL_UPLOAD.modal('show');
        });

        btnPreview.on('click', function (e) {
            fillFormPreview(input_id.val());
            MODAL_PREVIEW.modal('show');
        });

        // console.log('nama', data.nama);
        // console.log('elementAfter', elementAfter.data());
        elementAfter.after(trPemeriksaan);
    }

    let getPemeriksaanParent = (parentKey) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let keys = parentKey.split(',');
        let parents = [];
        let parentTr;
        let parentLvl = 0;
        let lastParentKey = null;
        for (let key of keys) {
            // console.log('key', key);
            key = key.trim();
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data(), key);
                if ($(el).data('key') == key) {
                    parentTr = $(el);
                }
            });

            if (! parentTr) {
                // console.log(key, 'not found');
                parentTr = $("<tr/>")
                    .data('key', key)
                    .data('parent', lastParentKey)
                    .data('level', parentLvl)
                    .appendTo(tbody);

                $("<td/>")
                    .prop('colspan', 4)
                    .addClass('text-bold')
                    .html('&nbsp'.repeat(parentLvl * 4) + key)
                    .appendTo(parentTr);
            }

            parents.push(parentTr);
            parentTr = null;
            parentLvl++;
            lastParentKey = key;
        }

        let lastParent = parents.pop();
        let lastRow = null;
        if (lastParent) {
            // console.log('lastParent', lastParent.data());
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data('parent'), lastParent.data('key'));
                if ($(el).data('parent') == lastParent.data('key')) {
                    lastRow = $(el);
                }
            });
        }

        // if (lastRow) {
        //     console.log('lastRow', lastRow.data());
        // } else {
        //     console.log('lastRow Not Found', lastParent.data());
        // }
        return lastParent;
    }

    /**
     * Untuk Mendapatkan Element After
     *  
     * - H
     * -- B
     * --- c
     * --- d
     * --- e
     * --A
     * --- a
     * --- b
     * 
     * getElementAfter('H,B') => e
     * getElementAfter('H,A') => b
     * getElementAfter('H') => b
     */
    let getElementAfter = (parentKey) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let keys = parentKey.split(',');
        let parents = [];
        let parentTr;
        let parentLvl = 0;
        let lastParentKey = null;
        for (let key of keys) {
            // console.log('key', key);
            key = key.trim();
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data(), key);
                if ($(el).data('key') == key) {
                    parentTr = $(el);
                }
            });

            if (! parentTr) {
                // console.log(key, 'not found');
                parentTr = $("<tr/>")
                    .data('key', key)
                    .data('parent', lastParentKey)
                    .data('level', parentLvl)
                    .appendTo(tbody);

                $("<td/>")
                    .prop('colspan', 4)
                    .addClass('text-bold')
                    .html('&nbsp'.repeat(parentLvl * 4) + key)
                    .appendTo(parentTr);
            }

            parents.push(parentTr);
            parentTr = null;
            parentLvl++;
            lastParentKey = key;
        }

        let lastParent = parents.pop();
        let lastRow = null;
        if (lastParent) {
            // console.log('lastParent', lastParent.data());
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data('parent'), lastParent.data('key'));
                if ($(el).data('parent') == lastParent.data('key')) {
                    lastRow = $(el);
                }
            });
        }

        let hasChildren = false;
        if (lastRow) {
            tbody.find('tr').each((i, el) => {
                if ($(el).data('parent') == lastRow.data('key')) {
                    hasChildren = true;
                }
            });
        }

        if (hasChildren) {
            return getElementAfter(lastRow.data('key'));
        }

        // if (lastRow) {
        //     console.log('lastRow', lastRow.data());
        // } else {
        //     console.log('lastRow Not Found', lastParent.data());
        // }
        return lastRow || lastParent;
    }

    /**
     * ADD FOTO MEDIA
     */
    let addFoto = (data, container) => {
        let col = $("<div/>")
            .addClass('col-lg-3 col-sm-6')
            .appendTo(container);

        let thumbnail = $("<div/>")
            .addClass('thumbnail')
            .appendTo(col);

        let thumb = $("<div/>")
            .addClass('thumb')
            .appendTo(thumbnail);

        let img = $("<img/>")
            .prop('src', data.link)
            .prop('alt', 'Foto Hasil')
            .appendTo(thumb);

        let captionOverflow = $("<div/>")
            .addClass('caption-overflow')
            .appendTo(thumb);
        let captionOverflowSpan = $("<span/>")
            .appendTo(captionOverflow);

        // captionOverflowSpan.append(`<a href="${data.link}" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-search4"></i></a>`);
        // let btnPreview = $("<a/>")
        //     .prop('href', data.link)
        //     .data('popup', 'lightbox')
        //     .prop('rel', 'gallery')
        //     .addClass('btn border-white text-white btn-flat btn-icon btn-rounded')
        //     .html('<i class="icon-search4"></i>')
        //     .appendTo(captionOverflowSpan);
        let btnDelete = $("<a/>")
            .prop('href', '#')
            .addClass('btn border-white text-white btn-flat btn-icon btn-rounded ml-5')
            .html('<i class="icon-trash"></i>')
            .appendTo(captionOverflowSpan);

        btnDelete.on('click', (e) => {
            e.preventDefault();
            confirmDialog({
                title: 'Hapus Foto Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: URL.deleteFoto.replace(':UID', data.uid),
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Foto Berhasil Dihapus.');
                    col.remove();
                },
                onError: (error) => {
                    errorMessage('Error', 'Foto Gagal Dihapus.');
                },
            });
        });

        // console.log('fancybox', captionOverflowSpan.find('[data-popup="lightbox"]'));
        // captionOverflowSpan.find('[data-popup="lightbox"]').fancybox({
        //     padding: 3
        // });
    }

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Hasil berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Hasil.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });
    }

    let initializeFormInputHasil = () => {
        FORM_INPUT_HASIL.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                INPUT_HASIL.val(DISP_INPUT_HASIL.summernote('code'));
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.saveHasil,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Hasil Pemeriksaan berhasil disimpan.");

                        MODAL_INPUT_HASIL.modal('hide');
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Hasil Pemeriksaan.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });
    }

    let initializeFormUpload = (uid, pemeriksaan_id) => {
        UPLOADER_CONTAINER.empty();
        UPLOADER = $("<input/>")
            .prop('type', 'file')
            .prop('multiple', 'multiple')
            .addClass('file-input-ajax')
            .appendTo(UPLOADER_CONTAINER);

        UPLOADER.fileinput({
            uploadUrl: URL.upload.replace(':UID', uid).replace(':PEMERIKSAAN_ID', pemeriksaan_id),
            uploadAsync: true,
            maxFileCount: 10,
            initialPreview: [],
            fileActionSettings: {
                removeIcon: '<i class="icon-bin"></i>',
                removeClass: 'btn btn-link btn-xs btn-icon',
                uploadIcon: '<i class="icon-upload"></i>',
                uploadClass: 'btn btn-link btn-xs btn-icon',
                indicatorNew: '<i class="icon-file-plus text-slate"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
            }
        });
    }

    fillForm(UID);
    initializeForm();
    initializeFormInputHasil();
});