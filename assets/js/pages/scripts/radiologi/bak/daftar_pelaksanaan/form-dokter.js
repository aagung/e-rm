$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                
                let tanggal = moment(data.created_at);
                let tgl_lahir = moment(data.tgl_lahir);
                let umur_tahun = tanggal.diff(tgl_lahir, 'year');
                let umur_bulan = tanggal.diff(tgl_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal.diff(tgl_lahir, 'days') % 31.5);

                // Pemeriksaan
                $("#disp_tanggal").html(tanggal.format('DD/MM/YYYY HH:mm'));
                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_kode_hasil").html(data.kode_hasil);
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_kelas").html(data.kelas);
                $("#disp_ruang").html(data.ruang);
                $("#disp_bed").html(data.bed);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);
                $("#disp_dokter").html(data.dokter);
                $("#disp_diagnosa").html(data.diagnosa);

                // Pasien
                $("#disp_no_rekam_medis").html(data.no_rekam_medis);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_alamat").html(data.alamat);
                $("#disp_tgl_lahir").html(tgl_lahir.format('DD/MM/YYYY'));
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_telepon").html(data.nomor_hp);

                DISP_CATATAN_DOKTER.summernote('code', data.catatan_dokter);
                CATATAN_DOKTER.val(data.catatan_dokter);

                /**
                 * INPUT HIDDEN
                 */
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#register_id").val(data.register_id);
                console.log('fillForm', data);
                getPemeriksaan(uid);
                getObat(uid);
            }
        });
    }

    let getObat = (uid) => {
        $.getJSON(URL.getObat.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                for (let i = 0; i < data.length; i++) {
                    addObat(data);
                }

                if (data.length <= 0) {
                    let tr = $("<tr/>")
                        .appendTo(TABLE_OBAT.find('tbody'));

                    let td = $("<td/>")
                        .prop('colspan', 4)
                        .addClass('text-center')
                        .html('Tidak ada penggunaan Obat/BMHP')
                        .appendTo(tr);
                }
            }
        });
    }

    let addObat = (data) => {
        let tbody = TABLE_OBAT.find('tbody');

        let tr = $("<tr/>")
            .appendTo(tbody);

        let tdKode = $("<td/>")
            .addClass('text-center')
            .html(data.kode)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .addClass('text-left')
            .html(data.nama)
            .appendTo(tr);

        let tdStock = $("<td/>")
            .addClass('text-right')
            .html(data.stock)
            .appendTo(tr);

        let tdQuantity = $("<td/>")
            .addClass('text-right')
            .html(data.quantity)
            .appendTo(tr);
    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                for (let parentkey in data) {
                    for (let i = 0; i < data[parentkey].length; i++) {
                        addPemeriksaan(parentkey, data[parentkey][i]);
                    }
                }
            }
        });
    }

    let addPemeriksaan = (parentKey, data) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let parent = getPemeriksaanParent(parentKey);
        let elementAfter = getElementAfter(parentKey);
        // console.log(elementAfter);

        /**
         * Pemeriksaan
         */
        let level = parent.data('level') + 1;
        let trPemeriksaan = $("<tr/>").data('key', data.kode)
            .data('parent', parent.data('key'))
            .data('level', level);
        let tdPemeriksaan = $("<td/>")
            .prop('colspan', 4)
            .addClass('text-bold')
            .html('&nbsp'.repeat(level * 4) + data.nama)
            .appendTo(trPemeriksaan);

        // console.log('nama', data.nama);
        // console.log('elementAfter', elementAfter.data());
        elementAfter.after(trPemeriksaan);

        let parentElement = trPemeriksaan;
        let lastElement = trPemeriksaan;
        for (let i = 0; i < data.hasil.length; i++) {
            lastElement = addPemeriksaanHasil(data.hasil[i], parentElement, lastElement);
        }
    }

    let addPemeriksaanHasil = (data, parent, lastElement) => {
        let level = parent.data('level') + 1;
        let tr = $("<tr/>")
            .data('key', data.kode + data.nama)
            .data('parent', parent.data('key'))
            .data('level', level);

        let tdNama = $("<td/>")
            .prop('colspan', 1)
            .html('&nbsp;'.repeat(level * 4) + data.nama)
            .appendTo(tr);

        let tdHasil = $("<td/>")
            .prop('colspan', 1)
            .html(getValueHasil(data))
            .appendTo(tr);

        let tdSatuan = $("<td/>")
            .prop('colspan', 1)
            .html(data.satuan)
            .appendTo(tr);

        let tdNilaiRujukan = $("<td/>")
            .prop('colspan', 1)
            .html(data.nilai_normal.replace("\r\n", '<br/>').replace("\n", '<br/>'))
            .appendTo(tr);

        lastElement.after(tr);

        return tr;
    }

    let getValueHasil = (data) => {
        let value;
        console.log('getValueHasil', data);
        switch (data.datatype) {
            case 'boolean':
                value = parseInt(data.value) == 1 ? 'Positif' : 'Negatif';
                break;
            case 'range':
            case 'lt':
            case 'gt':
            case 'lte':
            case 'gte':
            case 'text':
            default:
                value = data.value;
                break;
        }

        return value;
    }

    let getPemeriksaanParent = (parentKey) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let keys = parentKey.split(',');
        let parents = [];
        let parentTr;
        let parentLvl = 0;
        let lastParentKey = null;
        for (let key of keys) {
            // console.log('key', key);
            key = key.trim();
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data(), key);
                if ($(el).data('key') == key) {
                    parentTr = $(el);
                }
            });

            if (! parentTr) {
                // console.log(key, 'not found');
                parentTr = $("<tr/>")
                    .data('key', key)
                    .data('parent', lastParentKey)
                    .data('level', parentLvl)
                    .appendTo(tbody);

                $("<td/>")
                    .prop('colspan', 4)
                    .addClass('text-bold')
                    .html('&nbsp'.repeat(parentLvl * 4) + key)
                    .appendTo(parentTr);
            }

            parents.push(parentTr);
            parentTr = null;
            parentLvl++;
            lastParentKey = key;
        }

        let lastParent = parents.pop();
        let lastRow = null;
        if (lastParent) {
            // console.log('lastParent', lastParent.data());
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data('parent'), lastParent.data('key'));
                if ($(el).data('parent') == lastParent.data('key')) {
                    lastRow = $(el);
                }
            });
        }

        // if (lastRow) {
        //     console.log('lastRow', lastRow.data());
        // } else {
        //     console.log('lastRow Not Found', lastParent.data());
        // }
        return lastParent;
    }

    /**
     * Untuk Mendapatkan Element After
     *  
     * - H
     * -- B
     * --- c
     * --- d
     * --- e
     * --A
     * --- a
     * --- b
     * 
     * getElementAfter('H,B') => e
     * getElementAfter('H,A') => b
     * getElementAfter('H') => b
     */
    let getElementAfter = (parentKey) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');
        let keys = parentKey.split(',');
        let parents = [];
        let parentTr;
        let parentLvl = 0;
        let lastParentKey = null;
        for (let key of keys) {
            // console.log('key', key);
            key = key.trim();
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data(), key);
                if ($(el).data('key') == key) {
                    parentTr = $(el);
                }
            });

            if (! parentTr) {
                // console.log(key, 'not found');
                parentTr = $("<tr/>")
                    .data('key', key)
                    .data('parent', lastParentKey)
                    .data('level', parentLvl)
                    .appendTo(tbody);

                $("<td/>")
                    .prop('colspan', 4)
                    .addClass('text-bold')
                    .html('&nbsp'.repeat(parentLvl * 4) + key)
                    .appendTo(parentTr);
            }

            parents.push(parentTr);
            parentTr = null;
            parentLvl++;
            lastParentKey = key;
        }

        let lastParent = parents.pop();
        let lastRow = null;
        if (lastParent) {
            // console.log('lastParent', lastParent.data());
            tbody.find('tr').each((i, el) => {
                // console.log($(el).data('parent'), lastParent.data('key'));
                if ($(el).data('parent') == lastParent.data('key')) {
                    lastRow = $(el);
                }
            });
        }

        let hasChildren = false;
        if (lastRow) {
            tbody.find('tr').each((i, el) => {
                if ($(el).data('parent') == lastRow.data('key')) {
                    hasChildren = true;
                }
            });
        }

        if (hasChildren) {
            return getElementAfter(lastRow.data('key'));
        }

        // if (lastRow) {
        //     console.log('lastRow', lastRow.data());
        // } else {
        //     console.log('lastRow Not Found', lastParent.data());
        // }
        return lastRow || lastParent;
    }

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                CATATAN_DOKTER.val(DISP_CATATAN_DOKTER.summernote('code'));
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Hasil berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Hasil.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });

        $("body").on('click', '.btn-reject', function (e) {
            let uid = $("#uid").val();
            batalDialog({
                title: 'Anda yakin ingin menolak Hasil Pemeriksaan tersebut?',
                btn_confirm: 'Ya, Tolak',
                message_input_error: 'Silahkan isi alasan ditolaknya Hasil Pemeriksaan.',
                callback: (text_alasan, close) => {
                    let postData = {uid: uid, alasan: text_alasan};
                    $.ajax({
                        url: URL.reject,
                        type: 'POST',
                        dataType: "json",
                        data: postData,
                        success: function (data) {
                            successMessage('Success', 'Hasil Pemeriksaan berhasil ditolak.');
                            setTimeout(function () {
                                window.location.assign(URL.index);
                            }, 3000);
                        },
                        error: function (error) {
                            console.log('ERROR', error);
                            errorMessage('Error', 'Terjadi kesalahan saat hendak menolak Hasil Pemeriksaan.');
                        },
                        complete: function () {
                            close();
                        }
                    });
                }
            });
        });
    }

    fillForm(UID);
    initializeForm();
});