$(() => {
    let INITIALIZED = {
        daftar_pemeriksaan: false,
        daftar_approval_dokter: false,
        history: false,
    };

    let initializeDaftarPemeriksaan = () => {
        // DATATABLE
        TABLE_DT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "created_at",
                    "render": (data, type, row, meta) => {
                        let rejectIcon = '';
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                        let lama_tunggu = `<small data-popup="tooltip" data-title="Lama Tunggu" class="text-muted" data-tanggal="${data}">Lama Tunggu</small>`;

                        if (parseInt(row.status) == 4) {
                            rejectIcon = '<span class="position-left" data-popup="tooltip" data-title="Ditolak Oleh Dokter" data-placement="right"><i class="icon-cancel-circle2 text-danger"></i></span>';
                        }

                        return `${rejectIcon}${tgl}<br/>${lama_tunggu}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rekam_medis",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        let url_hasil = URL.form.replace(':UID', data);
                        let btnSampling = `<a href="${url_hasil}" data-popup="tooltip" data-title="Input Hasil" class="btn btn-primary btn-xs"><i class="icon-med-i-radiology"></i></a>`;

                        return `${btnSampling}`;
                    },
                    "className": "text-center"
                }
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                TABLE_BADGE.html(n);
                // TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE.on('click', function () {
            TABLE_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_search_no_rekam_medis").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 0);
        });
        $("#table_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 1);
        });
        $("#table_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 2);
        });
        $("#table_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DT.fnFilter(val, 3);
        });

        /**
         * EVENT SOURCE
         */
         let listen = () => {
            if (TABLE_EVENT_SOURCE) {
                 TABLE_EVENT_SOURCE.close();
             }
             let eventId = 'laboratorium-daftar_pelaksanaan';
             TABLE_EVENT_SOURCE = new EventSource(URL.listen);
             TABLE_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DT.fnDraw(false);
                }
             });
         }

         listen();

         /**
          * LAMA TUNGGU
          */
        setInterval(() => {
            TABLE.find('tbody').find('tr').each((i, el) => {
                let tanggal = moment($(el).find('[data-tanggal]').data('tanggal'));
                let jam = moment().diff(tanggal, 'hours') % 24;
                let menit = moment().diff(tanggal, 'minutes') % 60;
                let detik = moment().diff(tanggal, 'seconds') % 60;

                let contentChunk = [];
                if (jam > 0) {
                    contentChunk.push(`${jam} Jam`);
                }
                if (menit > 0) {
                    contentChunk.push(`${menit} Menit`);
                }
                if (detik > 0) {
                    contentChunk.push(`${detik} Detik`);
                }
                $(el).find('[data-tanggal]').html(contentChunk.join(' '));
            });
        }, 1000);


        INITIALIZED.daftar_pemeriksaan = true;
    }

    let initializeDaftarApprovalDokter = () => {
        // DATATABLE
        TABLE_DOKTER_DT = TABLE_DOKTER.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataDokter,
            "columns": [
                {
                    "data": "hasil_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';
                        let lama_tunggu = `<small data-popup="tooltip" data-title="Lama Tunggu" class="text-muted" data-tanggal="${data}">Lama Tunggu</small>`;

                        return `${tgl}<br/>${lama_tunggu}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rekam_medis",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        let url_check = URL.approve_dokter.replace(':UID', data);
                        let btn = `<a href="${url_check}" data-popup="tooltip" data-title="Tinjau Hasil" class="btn btn-primary btn-xs"><i class="icon-med-i-radiology"></i></a>`;

                        return `${btn}`;
                    },
                    "className": "text-center"
                }
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_DOKTER_TIMER) clearTimeout(TABLE_DOKTER_TIMER);
                TABLE_DOKTER_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                TABLE_DOKTER_BADGE.html(n);
                // TABLE.unblock();
                TABLE_DOKTER.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE_DOKTER.on('click', function () {
            TABLE_DOKTER_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_dokter_search_no_rekam_medis").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 0);
        });
        $("#table_dokter_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 1);
        });
        $("#table_dokter_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 2);
        });
        $("#table_dokter_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_DOKTER_DT.fnFilter(val, 3);
        });

        /**
         * EVENT SOURCE
         */
         let listen = () => {
            if (TABLE_DOKTER_EVENT_SOURCE) {
                 TABLE_DOKTER_EVENT_SOURCE.close();
             }
             let eventId = 'laboratorium-daftar_pelaksanaan_dokter';
             TABLE_DOKTER_EVENT_SOURCE = new EventSource(URL.listen);
             TABLE_DOKTER_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_DOKTER_DT.fnDraw(false);
                }
             });
         }

         listen();

         /**
          * LAMA TUNGGU
          */
        setInterval(() => {
            TABLE_DOKTER.find('tbody').find('tr').each((i, el) => {
                let tanggal = moment($(el).find('[data-tanggal]').data('tanggal'));
                let jam = moment().diff(tanggal, 'hours') % 24;
                let menit = moment().diff(tanggal, 'minutes') % 60;
                let detik = moment().diff(tanggal, 'seconds') % 60;

                let contentChunk = [];
                if (jam > 0) {
                    contentChunk.push(`${jam} Jam`);
                }
                if (menit > 0) {
                    contentChunk.push(`${menit} Menit`);
                }
                if (detik > 0) {
                    contentChunk.push(`${detik} Detik`);
                }
                $(el).find('[data-tanggal]').html(contentChunk.join(' '));
            });
        }, 1000);


        INITIALIZED.daftar_approval_dokter = true;
    }

    let initializeHistory = () => {
        // DATATABLE
        TABLE_HISTORY_DT = TABLE_HISTORY.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadDataHistory,
            "columns": [
                {
                    "data": "hasil_at",
                    "render": (data, type, row, meta) => {
                        let tgl = moment(data).isValid() ? moment(data).format('DD/MM/YYYY HH:mm') : '-';

                        return `${tgl}`;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_register",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "no_rekam_medis",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-center"
                },
                {
                    "data": "pasien",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": (data, type, row, meta) => {
                        return `<span class="alamat">${data}</span>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "rujukan_dari",
                    "render": (data, type, row, meta) => {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "uid",
                    "render": (data, type, row, meta) => {
                        let linkPrint = URL.print.replace(':UID', data);
                        let btnPrint = `<a href="${linkPrint}" target="_blank" class="btn btn-default btn-xs"><i class="icon-printer"></i></a>`;

                        return btnPrint;
                    },
                    "className": "text-center"
                }
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_HISTORY_TIMER) clearTimeout(TABLE_HISTORY_TIMER);
                TABLE_HISTORY_TIMER = setTimeout(function () {
                    // blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                // TABLE_HISTORY_BADGE.html(n);
                // TABLE.unblock();
                TABLE_HISTORY.find('[data-popup=tooltip]').tooltip();
            }
        });

        BTN_REFRESH_TABLE_HISTORY.on('click', function () {
            TABLE_HISTORY_DT.fnDraw(false);
        });

        /**
         * FILTERS
         */
        $("#table_history_search_no_rekam_medis").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_DT.fnFilter(val, 0);
        });
        $("#table_history_search_nama").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_DT.fnFilter(val, 1);
        });
        $("#table_history_search_alamat").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_DT.fnFilter(val, 2);
        });
        $("#table_history_search_layanan_id").on('change keyup blur', function () {
            let val = $(this).val();
            TABLE_HISTORY_DT.fnFilter(val, 3);
        });

        /**
         * Filter Range Tanggal
         */
        $("#table_history_search_tanggal").daterangepicker({
            startDate: moment(),
            endDate: moment(),
            applyClass: "bg-slate-600",
            cancelClass: "btn-default",
            opens: "center",
            autoApply: true,
            locale: {
                format: "DD/MM/YYYY"
            }
        });
        $("#table_history_search_tanggal").on('apply.daterangepicker', (ev, picker) => {
            TABLE_HISTORY_DT.fnFilter(picker.startDate.format('YYYY-MM-DD'), 4);
            TABLE_HISTORY_DT.fnFilter(picker.endDate.format('YYYY-MM-DD'), 5);
        });
        $("#table_history_btn_search_tanggal").click(() => {
            $("#table_history_search_tanggal").data('daterangepicker').toggle();
        });

        $("#btn_search_tanggal").click(function () {
            $("#disp_search_tanggal").data('daterangepicker').toggle();
        });
        TABLE_HISTORY_DT.fnFilter(moment().format('YYYY-MM-DD'), 4);
        TABLE_HISTORY_DT.fnFilter(moment().format('YYYY-MM-DD'), 5);

        /**
         * EVENT SOURCE
         */
        let listen = () => {
            if (TABLE_HISTORY_EVENT_SOURCE) {
                TABLE_HISTORY_EVENT_SOURCE.close();
            }
            let eventId = 'laboratorium-daftar_pelaksanaan_history';
            TABLE_HISTORY_EVENT_SOURCE = new EventSource(URL.listen);
            TABLE_HISTORY_EVENT_SOURCE.addEventListener(eventId, (e) => {
                let data = $.parseJSON(e.data);
                if (data.data) {
                    console.log('Reloading', data.timestamp);
                    TABLE_HISTORY_DT.fnDraw(false);
                }
            });
        }

         listen();

        INITIALIZED.history = true;
    }

    let initialize = () => {
        $('a[data-toggle=tab][href="#tab-pemeriksaan"').click(() => {
            if (! INITIALIZED.daftar_pemeriksaan) {
                initializeDaftarPemeriksaan();
            }
        });

        $('a[data-toggle=tab][href="#tab-dokter"').click(() => {
            if (! INITIALIZED.daftar_approval_dokter) {
                initializeDaftarApprovalDokter();
            }
        });

        $('a[data-toggle=tab][href="#tab-history"').click(() => {
            if (! INITIALIZED.history) {
                initializeHistory();
            }
        });

        $("a[data-toggle=tab]").closest('li').first().find('a[data-toggle]').click();
    }
    
    initialize();
});