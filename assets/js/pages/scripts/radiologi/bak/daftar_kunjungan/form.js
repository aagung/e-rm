$(() => {
    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;
                console.log('fillForm', data);

                let tanggal_registrasi = moment(data.tanggal_registrasi);
                let tgl_lahir = moment(data.tgl_lahir);
                let umur_tahun = tanggal_registrasi.diff(tgl_lahir, 'year');
                let umur_bulan = tanggal_registrasi.diff(tgl_lahir, 'months') % 12;
                let umur_hari = Math.round(tanggal_registrasi.diff(tgl_lahir, 'days') % 31.5);

                // Input
                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#pelayanan_id").val(data.pelayanan_id);
                $("#jenis_pasien").val(data.jenis_pasien);
                $("#pasien_id").val(data.pasien_id);
                $("#dokter_perujuk_id").val(data.dokter_perujuk_id);
                $("#dokter_perujuk").val(data.dokter_perujuk);
                $("#rujukan_dari").val(data.rujukan_dari);
                $("#layanan_id").val(data.layanan_id);
                $("#cara_bayar_id").val(data.cara_bayar_id);
                $("#kontraktor_id").val(data.kontraktor_id);
                $("#asuransi_id").val(data.asuransi_id);
                $("#ruang_id").val(data.ruang_id);
                $("#kelas_id").val(data.kelas_id);
                $("#bed_id").val(data.bed_id);
                $("#dokter_id").val(data.dokter_id).trigger('change');
                $("#diagnosa").val(data.diagnosa).trigger('change');

                $("#disp_tanggal_registrasi").html(tanggal_registrasi.format('DD/MM/YYYY HH:mm'));
                $("#disp_kode_transaksi").html(data.kode_transaksi);
                $("#disp_cara_bayar").html(data.cara_bayar);
                $("#disp_rujukan_dari").html(data.rujukan_dari);
                $("#disp_dokter_perujuk").html(data.dokter_perujuk);

                // PASIEN
                $("#disp_no_rekam_medis").html(data.no_rekam_medis);
                $("#disp_nama").html(data.nama);
                $("#disp_jenis_kelamin").html(parseInt(data.jenis_kelamin) == 1 ? 'Laki-Laki' : 'Perempuan');
                $("#disp_alamat").html(data.alamat);
                $("#disp_tgl_lahir").html(tgl_lahir.format('DD/MM/YYYY'));
                $("#disp_umur").html((umur_tahun > 0 ? `${umur_tahun} Thn ` : '') + (umur_bulan > 0 ? `${umur_bulan} Bln ` : '') + (umur_hari > 0 ? `${umur_hari} Hr` : ''));
                $("#disp_telepon").html(data.nomor_hp);

                // RAWAT INAP
                $("#disp_kelas").html(data.kelas);
                $("#disp_ruang").html(data.ruang);
                $("#disp_bed").html(data.bed);

                getPemeriksaan(uid);
            }
        });
    }

    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getPemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                console.log('getPemeriksaan', data);
                initializeTreePemeriksaan(data);
            }
        });
    }

    let initializeTreePemeriksaan = (data) => {
        console.log('initializeTreePemeriksaan', data);
        // Tree Tindakan
        TREE_TINDAKAN_TAG.fancytree({
            extensions: ["filter"],
            quicksearch: true,
            filter: {
                autoApply: true,   // Re-apply last filter if lazy data is loaded
                autoExpand: true, // Expand all branches that contain matches while filtered
                counter: false,     // Show a badge with number of matching child nodes near parent icons
                fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                highlight: true,   // Highlight matches by wrapping inside <mark> tags
                leavesOnly: false, // Match end nodes only
                nodata: true,      // Display a 'no data' status node if result is empty
                mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
            },
            source: data,
            checkbox: true,
            selectMode: 3,
        });

        TREE_TINDAKAN = TREE_TINDAKAN_TAG.fancytree("getTree");

        setTimeout(() => {
            var rootNode = TREE_TINDAKAN_TAG.fancytree("getRootNode");
            rootNode.sortChildren(function(a, b) {
                var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                return x === y ? 0 : x > y ? 1 : -1;
            }, true);

            // Hide Unselected Node
            // Untuk menghilangkan parent yang tidak dipilih sama sekali
            TREE_TINDAKAN.visit(function (node) {
                node.setExpanded(true);
                if (!node.partsel && !node.selected) {
                    $(node.li).addClass('hide');
                }
            });

            // untuk menghilangkan node dari parent yang dipilih (Partial|semua)
            TREE_TINDAKAN.visit(function (node) {
                if (!node.partsel && !node.selected) {
                    $(node.li).addClass('hide');
                }
            });

            $("<div/>")
                .css('z-index', '1000')
                .css('border', 'none')
                .css('margin', '0px')
                .css('padding', '0px')
                .css('width', '100%')
                .css('height', '100%')
                .css('top', '0px')
                .css('left', '0px')
                .css('position', 'absolute')
                .appendTo(TREE_TINDAKAN_TAG);
        }, 500);
    };

    let initializeForm = () => {
        FORM.validate({
            rules: {
                //
            },
            messages: {
                //
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Data Sampling berhasil disimpan.");

                        BTN_SAVE.prop('disabled', true);

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan Data Sampling.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        BTN_CANCEL.on('click', () => {
            window.location.assign(URL.index);
        });
    }

    fillForm(UID);
    initializeForm();
});