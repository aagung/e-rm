var Radiologi = {
    fillForm: function (data) {
        if (typeof(data) != "undefined" && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                this.addKunjungan(data[i]);
            }
        } else {
            $("#tab-radiologi").hide();
            $("a[href=#tab-radiologi]").closest('li').hide();
        }

        var me = this;
        // Handler
        $("#table_kunjungan_radiologi").on('click', '.btn-modal-radiologi', function (e) {
            e.preventDefault();

            var uid = $(this).data('uid');
            me.fillModal_rad(uid);
        });
    },
    addKunjungan: function (data) {
        var tbody = $("#table_kunjungan_radiologi tbody");

        var tr = $("<tr/>")
            .appendTo(tbody);

        var td = $("<td/>")
            .appendTo(tr);
        var media = $("<div/>")
            .addClass('media-left media-middle')
            .appendTo(td);
        var mediaLink = $("<a/>")
            .prop('target', '_blank')
            .data('id', data.pelaksanaan_id)
            .data('uid', data.pelaksanaan_uid)
            .appendTo(media);
        var mediaIcon = $("<i/>")
            .addClass('icon-med-i-radiology')
            .appendTo(mediaLink);
        var mediaContent = $("<div/>")
            .addClass('media-left')
            .appendTo(td);
        var mediaLabel = $('<div class=""><a class="text-default text-semibold btn-modal-radiologi" data-uid="' + data.pelaksanaan_uid + '">Radiologi</a></div>')
            .appendTo(mediaContent);
        var mediaContentContainer = $("<div/>")
            .addClass("text-muted text-size-small")
            .appendTo(mediaContent);
        var mediaStatus = $("<span/>")
            .addClass("status-mark position-left")
            .appendTo(mediaContentContainer);
        var tanggalRujuk = moment(data.tanggal).isValid() ? moment(data.tanggal).format('DD/MM/YYYY HH:mm') : '-';

        var tanggalPemeriksaan = moment(data.tanggal_ditindak).isValid() ? moment(data.tanggal_ditindak).format('DD/MM/YYYY HH:mm') + ' ' + data.waktu_selesai : 'Sekarang';
        mediaContentContainer.append(tanggalRujuk + ' - ' + tanggalPemeriksaan);

        var tdKode = $("<td/>")
            .appendTo(tr);
        var dispKode = $("<span/>")
            .addClass('text-semibold')
            .html(data.kode_transaksi.trim() != "" ? data.kode_transaksi : '-')
            .appendTo(tdKode);
        var tdStatus = $("<td/>")
            .appendTo(tr);
        var dispStatus = $("<span/>")
            .addClass("label")
            .appendTo(tdStatus);

        switch (parseInt(data.status)) {
            case -1: // Dirujuk
                mediaLink.addClass("text-default");
                mediaStatus.addClass("border-default");
                dispStatus.addClass("label-default").html("Dirujuk");
                break;
            case 0: // Sedang diperiksa
                mediaLink.addClass("text-info");
                mediaStatus.addClass("border-info");
                dispStatus.addClass("bg-info").html("Sedang Diperiksa");
                break;
            case 1: // Selesai
                mediaLink.addClass("text-success");
                mediaStatus.addClass("border-success");
                dispStatus.addClass("bg-success").html("Selesai");
                break;
            case 2: // Batal
                mediaLink.addClass("text-danger");
                mediaStatus.addClass("border-danger");
                dispStatus.addClass("bg-danger").html("Batal");
                break;
        }
    },
    fillModal_rad: function (uid) {
        var me = this;
        var form = $("#form_modal_radiologi");
        var modal = $("#modal_radiologi");
        $.getJSON(URL.getRadiologi.replace(':UID', uid), function (data, status) {
            if (status === 'success') {
                data = data.data;
                console.log(data);

                // Data Transaksi
                form.find('.disp_kode_transaksi').html(data.kode_transaksi);
                form.find('.disp_kode_hasil').html(data.kode_hasil);

                form.find('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            }
        });
        getPemeriksaan_rad(uid);
        modal.modal('show');
    }
}

    let fillViewImageForm = (id) => {
        $.getJSON(URL.getImages.replace(':ID', id), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                TABLE_VIEW_IMAGES.trigger('clear');
                for (let d of data) {
                    TABLE_VIEW_IMAGES.trigger('add', [d]);
                }

                MODAL_VIEW_IMAGES.modal('show');
            }
        });

    }

    let getPemeriksaan_rad = (uid) => {
        $.getJSON(URL.getRadiologi_pemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                HASIL_CONTAINER_RAD.empty();
                for (let d of data) {
                    addPemeriksaan_rad(d);
                }
            }
        });
    }

    let addPemeriksaan_rad = (data) => {
        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER_RAD);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_no_foto = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_no_foto[]')
            .val(data.no_foto)
            .appendTo(col);
        let input_ukuran_foto_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_ukuran_foto_id[]')
            .val(data.ukuran_foto_id)
            .appendTo(col);
        let input_faktor_eksposisi = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_faktor_eksposisi[]')
            .val(data.faktor_eksposisi)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan</strong>`);
        fieldsetLegend.append(`&nbsp;&nbsp;`);

        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);

        let tableDiv = $("<div/>")
            .addClass('table-responsive')
            .appendTo(fieldsetCol);
        let table = $("<table/>")
            .addClass('table table-bordered')
            .appendTo(tableDiv);
        let tableHead = $("<thead/>")
            .appendTo(table);
        let tableBody = $("<tbody/>")
            .appendTo(table);
        tableHead.html(`
            <tr class="bg-slate">
                <th class="text-center">
                    Pemeriksaan
                </th>
                <th class="text-center" style="width: 15%">
                    No. Foto
                </th>
                <th class="text-center" style="width: 15%">
                    Ukuran
                </th>
                <th class="text-center" style="width: 15%">
                    Faktor Eksposisi
                </th>
                <th class="text-center" style="width: 15%">
                    Qty
                </th>
                <th>&nbsp;</th>
            </tr>
        `);

        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10')
            .val(data.catatan)
            .appendTo(col);

        row.append('<br/><br/><br/>');

        
        // TABLE BODY
        let tr = $("<tr/>")
            .appendTo(tableBody);

        let tdPemeriksaan = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let labelPemeriksaan = $("<label/>")
            .addClass('label-pemeriksaan')
            .html(data.nama)
            .appendTo(tdPemeriksaan);

        let tdNoFoto = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputNoFoto = $("<input/>")
            .prop('readonly', 'true')
            .prop('type', 'text')
            .prop('name', 'pemeriksaan_disp_no_foto[]')
            .addClass('form-control')
            .val(data.no_foto)
            .appendTo(tdNoFoto);

        let tdUkuranFoto = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputUkuranFotoId = $("<select/>")
            .prop('disabled', 'true')
            .prop('name', 'pemeriksaan_disp_ukuran_foto_id[]')
            .addClass('form-control')
            .appendTo(tdUkuranFoto);
        dispInputUkuranFotoId.append('<option value="0" selected>- Pilih Ukuran -</option>');
        for (let ukuran_foto of data.ukuran_foto) {
            dispInputUkuranFotoId.append(`
                <option value="${ukuran_foto.id}">${ukuran_foto.nama}</option>
            `);
        }
        dispInputUkuranFotoId.select2();
        dispInputUkuranFotoId.val(data.ukuran_foto_id || 0).trigger('change');

        let tdFaktorEksposisi = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        let dispInputFaktorEksposisi = $("<input/>")
            .prop('disabled', 'true')
            .prop('type', 'text')
            .prop('name', 'pemeriksaan_disp_faktor_eksposisi[]')
            .addClass('form-control')
            .val(data.faktor_eksposisi)
            .appendTo(tdFaktorEksposisi);

        /*let tdBmhp = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnBmhp = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-primary btn-labeled btn-bmhp')
            .appendTo(tdBmhp);
        let labelBmhpQuantity = $("<b/>")
            .addClass('bmhp-count')
            .html(`<i>${data.bmhp.length}</i>`)
            .appendTo(btnBmhp);
        btnBmhp.append('BMHP');*/

        let tdQuantity = $("<td/>")
            .addClass('text-left')
            .html(data.quantity || 1)
            .appendTo(tr);

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnView = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-xs btn-link btn-view')
            .html('<i class="icon-eye"></i>')
            .appendTo(tdAction);


        // Handler
        /*btnBmhp.on('click', () => {
            FORM_PENGGUNAAN_OBAT.trigger('fillForm', [{
                id: input_id.val(),
                bmhp: json_decode(input_bmhp.val()),
            }]);
        });*/

        dispInputNoFoto.on('change keyup blur', function () {
            input_no_foto.val($(this).val());
        });
        dispInputUkuranFotoId.on('change keyup blur', function () {
            input_ukuran_foto_id.val($(this).val());
        });
        dispInputFaktorEksposisi.on('change keyup blur', function () {
            input_faktor_eksposisi.val($(this).val());
        });

        // btnUpload.on('click', () => {
        //     fillUploadImageForm(input_id.val());
        // });

        btnView.on('click', () => {
            fillViewImageForm(input_id.val());
        });
    }

    let initializeViewImages = () => {
        TABLE_VIEW_IMAGES.on('clear', (e) => {
            TABLE_VIEW_IMAGES.find('tbody').empty();
        });

        TABLE_VIEW_IMAGES.on('add', (e, data) => {
            let tbody = TABLE_VIEW_IMAGES.find('tbody');

            let tr = $("<tr/>")
                .appendTo(tbody);

            let tdFoto = $("<td/>")
                .addClass('text-center')
                .appendTo(tr);
            tdFoto.html(`
                <a href="${data.link}" data-popup="lightbox" target="_blank">
                    <img src="${data.link}" alt="" class="img-rounded img-preview">
                </a>
            `);

            let tdMime = $("<td/>")
                .addClass('text-left')
                .html(data.mime)
                .appendTo(tr);

            let tdSize = $("<td/>")
                .addClass('text-right')
                .html(numeral(data.size / 1024).format(',.##') + ' Kb')
                .appendTo(tr);
        });
    }

    initializeViewImages();