var Laboratorium = {
    fillForm: function (data) {
        if (typeof(data) != "undefined" && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                this.addKunjungan(data[i]);
            }
        } else {
            $("#tab-laboratorium").hide();
            $("a[href=#tab-laboratorium]").closest('li').hide();
        }

        var me = this;
        // Handler
        $("#table_kunjungan_laboratorium").on('click', '.btn-modal-laboratorium', function (e) {
            e.preventDefault();

            var uid = $(this).data('uid');
            me.fillModal(uid);
        });
    },
    addKunjungan: function (data) {
        var tbody = $("#table_kunjungan_laboratorium tbody");

        var tr = $("<tr/>")
            .appendTo(tbody);

        var td = $("<td/>")
            .appendTo(tr);
        var media = $("<div/>")
            .addClass('media-left media-middle')
            .appendTo(td);
        var mediaLink = $("<a/>")
            .prop('target', '_blank')
            .data('id', data.pelaksanaan_id)
            .data('uid', data.pelaksanaan_uid)
            .appendTo(media);
        var mediaIcon = $("<i/>")
            .addClass('icon-med-i-laboratory')
            .appendTo(mediaLink);
        var mediaContent = $("<div/>")
            .addClass('media-left')
            .appendTo(td);
        var mediaLabel = $('<div class=""><a   class="text-default text-semibold btn-modal-laboratorium" data-uid="' + data.pelaksanaan_uid + '">Laboratorium</a></div>')
            .appendTo(mediaContent);
        var mediaContentContainer = $("<div/>")
            .addClass("text-muted text-size-small")
            .appendTo(mediaContent);
        var mediaStatus = $("<span/>")
            .addClass("status-mark position-left")
            .appendTo(mediaContentContainer);
        var tanggalRujuk = moment(data.tanggal).isValid() ? moment(data.tanggal).format('DD/MM/YYYY HH:mm') : '-';
        var tanggalPemeriksaan = moment(data.pemeriksaan_at).isValid() ? moment(data.pemeriksaan_at).format('DD/MM/YYYY HH:mm') : 'Sekarang';
        mediaContentContainer.append(tanggalRujuk + ' - ' + tanggalPemeriksaan);

        var tdKode = $("<td/>")
            .appendTo(tr);
        var dispKode = $("<span/>")
            .addClass('text-semibold')
            .html(data.kode_transaksi.trim() != "" ? data.kode_transaksi : '-')
            .appendTo(tdKode);

        var tdStatus = $("<td/>")
            .appendTo(tr);
        var dispStatus = $("<span/>")
            .addClass("label")
            .appendTo(tdStatus);

        switch (parseInt(data.status)) {
            case -1: // Dirujuk
                mediaLink.addClass("text-default");
                mediaStatus.addClass("border-default");
                dispStatus.addClass("label-default").html("Dirujuk");
                break;
            case 0: // Sedang diperiksa
                mediaLink.addClass("text-info");
                mediaStatus.addClass("border-info");
                dispStatus.addClass("bg-info").html("Sedang Diperiksa");
                break;
            case 1: // Selesai
                mediaLink.addClass("text-success");
                mediaStatus.addClass("border-success");
                dispStatus.addClass("bg-success").html("Selesai");
                break;
            case 2: // Batal
                mediaLink.addClass("text-danger");
                mediaStatus.addClass("border-danger");
                dispStatus.addClass("bg-danger").html("Batal");
                break;
        }
    },
    fillModal: function (uid) {
        var me = this;
        var form = $("#form_modal_laboratorium");
        var modal = $("#modal_laboratorium");
        $.getJSON(URL.getLaboratorium.replace(':UID', uid), function (data, status) {

            if (status === 'success') {
                data = data.data;
                console.log(data);

                // Data Transaksi
                form.find('.disp_kode_transaksi').html(data.kode_transaksi);
                form.find('.disp_kode_hasil').html(data.kode_hasil);

                form.find('[data-popup=tooltip]').tooltip();
                
            }
        });
        
        getPemeriksaan(uid);
        modal.modal('show');
    },
    fillModalDetail: function (data, level, fromLis) {
        level = typeof(level) === 'undefined' ? 0 : level;
        fromLis = typeof(fromLis) === 'undefined' ? false : fromLis;

        var tbody = $("#table_modal_laboratorium tbody");

        var tr = $("<tr/>")
            .appendTo(tbody);

        if (! fromLis) {
            var nama = '&nbsp;'.repeat(3*level) + data.tarif_pelayanan;
            var hasil = data.hasil;
            var satuan = data.satuan;
            var nilai_normal = '';
            for (var i = 0; i < data.nilai_normal.length; i++) {
                nilai_normal += (data.nilai_normal[i].kategori ? data.nilai_normal[i].kategori : '') + data.nilai_normal[i].nilai + '<br/>';
            }


            var tdNama = $("<td/>")
                .addClass( (data.children || data.hasil_lis) ? 'text-bold' : '' )
                .html(nama)
                .appendTo(tr);
            var tdHasil = $("<td/>")
                .html(hasil)
                .appendTo(tr);
            var tdSatuan = $("<td/>")
                .html(satuan)
                .appendTo(tr);
            var tdNilaiNormal = $("<td/>")
                .html(nilai_normal)
                .appendTo(tr);
        } else {
            var resultClass = data.flag != "N" ? "text-bold" : "";
            var nama = '&nbsp;'.repeat(3*level) + data.test_name;
            var hasil = data.result_value;
            var satuan = data.unit;
            var nilai_normal = data.reference_range;

            var tdNama = $("<td/>")
                .html(nama)
                .appendTo(tr);
            var tdHasil = $("<td/>")
                .addClass(resultClass)
                .html(hasil)
                .appendTo(tr);
            var tdSatuan = $("<td/>")
                .html(satuan)
                .appendTo(tr);
            var tdNilaiNormal = $("<td/>")
                .html(nilai_normal)
                .appendTo(tr);
        }

        if (data.hasil_lis) {
            for (var i = 0; i < data.hasil_lis.length; i++) {
                this.fillModalDetail(data.hasil_lis[i], level + 1, true);
            }
        }

        if (data.children) {
            for (var i = 0; i < data.children.length; i++) {
                this.fillModalDetail(data.children[i], level + 1);
            }
        }
    },
}
    
    let getPemeriksaan = (uid) => {
        $.getJSON(URL.getLaboratorium_pemeriksaan.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                HASIL_CONTAINER.empty();
                for (let d of data) {
                    addPemeriksaan(d);
                }
            }
        });
    }
    let addPemeriksaan = (data) => {
        if (parseInt(data.patologi_flag) == 1) {
            addPemeriksaanPatologi(data);
            return false;
        }

        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan ${data.nama}</strong>`);
        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);

        let tableDiv = $("<div/>")
            .addClass('table-responsive')
            .appendTo(fieldsetCol);
        let table = $("<table/>")
            .addClass('table table-bordered')
            .appendTo(tableDiv);
        let tableHead = $("<thead/>")
            .appendTo(table);
        let tableBody = $("<tbody/>")
            .appendTo(table);
        tableHead.html(`
            <tr class="bg-slate">
                <th class="text-center" style="width: 35%;">
                    Pemeriksaan
                </th>
                <th class="text-center">
                    Hasil
                </th>
                <th class="text-center" style="width: 20%;">
                    Satuan
                </th>
                <th class="text-center" style="width: 20%;">
                    Nilai Rujukan
                </th>
            </tr>
        `);

        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('readonly', 'true')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10')
            .val(data.catatan)
            .appendTo(col);

        row.append('<br/><br/><br/>');

        // HASIL
        for (let hasil of data.hasil) {
            addPemeriksaanHasil(hasil, tableBody);
        }

    }
    let addPemeriksaanPatologi = (data) => {
        let row = $("<div/>")
            .addClass('row row-pemeriksaan mb-20 mt-20')
            .appendTo(HASIL_CONTAINER);
        let col = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(row);

        // INPUT
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_id[]')
            .val(data.id)
            .appendTo(col);
        let input_jenis = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_jenis[]')
            .val(data.jenis)
            .appendTo(col);
        let input_kode = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_kode[]')
            .val(data.kode)
            .appendTo(col);
        let input_lft = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lft[]')
            .val(data.lft)
            .appendTo(col);
        let input_lvl = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_lvl[]')
            .val(data.lvl)
            .appendTo(col);
        let input_nama = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_nama[]')
            .val(data.nama)
            .appendTo(col);
        let input_parent_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_parent_id[]')
            .val(data.parent_id)
            .appendTo(col);
        let input_pelaksanaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pelaksanaan_id[]')
            .val(data.pelaksanaan_id)
            .appendTo(col);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(col);
        let input_rgt = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_rgt[]')
            .val(data.rgt)
            .appendTo(col);
        let input_patologi_flag = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'pemeriksaan_patologi_flag[]')
            .val(data.patologi_flag)
            .appendTo(data.patologi_flag);
        let input_bmhp = $("<textarea/>")
            .addClass('hide')
            .prop('name', 'pemeriksaan_bmhp[]')
            .val(json_encode(data.bmhp))
            .appendTo(col);
        let inputDeskripsi = $("<textarea/>")
            .prop('name', 'pemeriksaan_catatan[]')
            .prop('placeholder', 'Catatan Pemeriksaan')
            .addClass('form-control mt-10 hide')
            .val(data.catatan)
            .appendTo(col);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let fieldsetLegend = $("<legend/>")
            .addClass('text-bold')
            .appendTo(fieldset);
        fieldsetLegend.append(`<i class="icon-clipboard3 position-left"></i>`);
        fieldsetLegend.append(`<strong>Pemeriksaan ${data.nama}</strong>`);
        let fieldsetRow = $("<div/>")
            .addClass('row')
            .appendTo(fieldset);
        let fieldsetCol = $("<div/>")
            .addClass('col-sm-12')
            .appendTo(fieldsetRow);
        let dispCatatan = $("<div/>")
            .addClass('disp_catatan')
            .appendTo(fieldsetCol);
        dispCatatan.summernote();
        dispCatatan.summernote('code', data.catatan);

        row.append('<br/><br/><br/>');

    }
    let addPemeriksaanHasil = (data, tbody) => {
        let tr = $("<tr/>")
            .appendTo(tbody);

        let input_id = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_pemeriksaan_id = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_kode = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_kode[]')
            .val(data.kode)
            .appendTo(tr);
        let input_nama = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_nama[]')
            .val(data.nama)
            .appendTo(tr);
        let input_datatype = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_datatype[]')
            .val(data.datatype)
            .appendTo(tr);
        let input_value = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_value[]')
            .val(data.value)
            .appendTo(tr);
        let input_satuan = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_satuan[]')
            .val(data.satuan)
            .appendTo(tr);
        let input_nilai_normal = $("<input/>").prop('type', 'hidden').prop('name', 'hasil_nilai_normal[]')
            .val(data.nilai_normal)
            .appendTo(tr);

        let tdNama = $("<td/>")
            .prop('colspan', 1)
            .html(data.nama)
            .appendTo(tr);

        let tdHasil = $("<td/>")
            .prop('colspan', 1)
            .appendTo(tr);
        let dispInputHasil = getInputHasil(tdHasil, input_value, data);
        // TODO

        let tdSatuan = $("<td/>")
            .prop('colspan', 1)
            .html(data.satuan)
            .appendTo(tr);

        let tdNilaiRujukan = $("<td/>")
            .prop('colspan', 1)
            .html(data.nilai_normal.replace("\r\n", '<br/>').replace("\n", '<br/>'))
            .appendTo(tr);
    }
    let getInputHasil = (td, input, data) => {
        let dispInput;
        switch (data.datatype) {
            case 'boolean':
                dispInput = $("<select/>")
                    .addClass('form-control')
                    .appendTo(td);

                dispInput.append($("<option/>").prop('value', 0).html('Negatif'));
                dispInput.append($("<option/>").prop('value', 1).html('Positif'));

                dispInput.on('change', (e) => {
                    input.val(dispInput.val());
                });

                dispInput.val(data.value);
                dispInput.trigger('change');
                break;
            case 'range':
                dispInput = $("<label/>")
                    //.prop('type', 'text')
                    //.addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'lt':
                dispInput = $("<label/>")
                    //.prop('type', 'text')
                    //.addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' < ', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'gt':
                dispInput = $("<label/>")
                    //.prop('type', 'text')
                    //.addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' > ', 'pSign': 's' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'lte':
                dispInput = $("<label/>")
                    //.prop('type', 'text')
                    //.addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' <= ', 'pSign': 'p' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'gte':
                dispInput = $("<label/>")
                    //.prop('type', 'text')
                    //.addClass('form-control text-right')
                    .appendTo(td);
                dispInput.autoNumeric('init', { 'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' >= ', 'pSign': 's' });
                dispInput.autoNumeric('set', data.value);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.autoNumeric('get'));
                });
                dispInput.trigger('change');
                break;
            case 'text':
            default:
                dispInput = $("<label/>")
                    //.addClass('form-control')
                    .appendTo(td);

                dispInput.on('change keyup blur', (e) => {
                    input.html(dispInput.val());
                });

                dispInput.html(data.value);
                dispInput.trigger('change');
                break;
        }
    }
