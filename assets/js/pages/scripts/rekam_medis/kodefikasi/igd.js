var IGD = {
    fillForm: function (data, hideIfNull) {
        if (data.pemeriksaan_awal) {
            // Pemeriksaan Awal
            $(".disp_pelayanan_tinggi_badan").html(formatTinggi(getValue(data.pemeriksaan_awal.tinggi_badan)));
            $(".disp_pelayanan_berat_badan").html(formatBerat(getValue(data.pemeriksaan_awal.berat_badan)));
            $(".disp_pelayanan_suhu_badan").html(formatSuhu(getValue(data.pemeriksaan_awal.suhu)));
            $(".disp_pelayanan_tensi").html(getValue(data.pemeriksaan_awal.td));
            $(".disp_pelayanan_kasus").html(getKasus(getValue(data.pemeriksaan_awal.kasus)));

            // Pemeriksaan Dokter
            $(".disp_pelayanan_anamnesa").html(getValue(data.pemeriksaan_awal.diagnosis));
            $(".disp_pelayanan_alergi").html(getValue(data.pemeriksaan_awal.alergi));
            $(".disp_pelayanan_pemeriksaan_fisik").html(getValue(data.pemeriksaan_awal.terapi));

            // Diagnosa
            $(".disp_list_pelayanan_diagnosa").empty();
            for (var i = 0; i < data.diagnosa_utama.length; i++) {
                $(".disp_list_pelayanan_diagnosa")
                    .append('<li>' + data.diagnosa_utama[i].code + ' - ' + data.diagnosa_utama[i].description + '</li>');
            }
            
            // Tindakan
            $("#table_igd_tindakan tbody").empty();
            if (data.tindakan.length > 0) {
                for (var i = 0; i < data.tindakan.length; i++) {
                    this.addTindakan(data.tindakan[i]);
                }
                $("#table_igd_tindakan").dataTable();
            } else {
                $("#table_igd_tindakan tbody").html('<tr><td colspan="3" class="text-center">Tidak ada tindakan yang dilakukan.</td></tr>');
            }
            
        } else {
            if (hideIfNull) {
                $("#tab-igd").hide();
                $("a[href=#tab-igd]").closest('li').hide();
            } else {
                $(".data-igd").hide();
                $(".empty-igd").show();
            }
        }
    },
    addTindakan: function (data) {
        var tbody = $("#table_igd_tindakan tbody");
        
        var tr = $("<tr/>")
            .appendTo(tbody);

        // td tanggal
        var tdTanggal = $("<td/>")
            .appendTo(tr);
        var labelTanggal = $("<label/>")
            .html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(tdTanggal);
        
        // td Tindakan
        var quantity = parseInt(data.quantity) > 1 ? ' X ' + parseInt(data.quantity) : '';
        var tdTindakan = $("<td/>")
            .appendTo(tr);
        var labelTindakan = $("<label/>")
            .html(data.nama + quantity)
            .appendTo(tdTindakan);
            
        tbody.append(tr);
    }
};