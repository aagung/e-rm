var DiagnosaSekunder = {
    ADD_MODE: 0,
    VIEW_MODE: 1,
    EDIT_MODE: 2,
    fillForm: function (data) {
        $("#table_diagnosa_sekunder tbody").empty();
        for (var i = 0; i < data.length; i++) {
            data[i].mode = this.VIEW_MODE;
            this.addRow(data[i]);
        }
    },
    addRow: function (data) {
        var me = this;
        var tbody = $("#table_diagnosa_sekunder tbody");
        var icd10 = {
            id: data.icd_10_id,
            code: data.icd_10_code,
            description: data.icd_10_description,
            severity: data.icd_10_severity,
            in_patient: data.icd_10_in_patient,
            out_patient: data.icd_10_out_patient,
            text: data.icd_10_code + ' - ' + data.icd_10_description
        };

        var tr = $("<tr/>")
            .appendTo(tbody);

        var tdSelect = $("<td/>")
            .css('width', '35%')
            .appendTo(tr);
        var inputId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'diagnosa_sekunder_id[]')
            .val(data.id ? data.id : 0)
            .appendTo(tdSelect);
        var select = $("<select/>")
            .prop('name', 'diagnosa_sekunder[]')
            .addClass('form-control')
            .appendTo(tdSelect);
        var label = $("<span/>")
            .html(icd10.text)
            .appendTo(tdSelect);
        var temp = $("<span/>")
            .data('data', icd10)
            .appendTo(tdSelect);

        // Set Select value
        if (data.icd_10_id) {
            select2AjaxSetValue(select, data.icd_10_id, {
                id: icd10.id,
                text: icd10.text
            });
        }

        var tdDescription = $("<td/>")
            .html(formatIcd10(icd10))
            .css('width', '50%')
            .appendTo(tr);

        var tdAction = $("<td/>")
            .appendTo(tr);
        var btnA = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-link btn-xs btn-a')
            .html('<i class="fa fa-save"></i>')
            .appendTo(tdAction);
        var btnB = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-link btn-xs btn-b')
            .html('<i class="fa fa-remove"></i>')
            .appendTo(tdAction);

        btnA.click(function () {
            var mode = $(this).data('mode');
            var selectedData = select.select2('data').pop();
            console.log(selectedData);
            switch (mode) {
                case me.ADD_MODE:
                    temp.data('data', selectedData);

                    select.next('.select2-container').hide();
                    label.html(selectedData.text).show();

                    btnA.data('mode', me.VIEW_MODE);
                    btnA.html('<i class="fa fa-edit"></i>');
                    btnB.data('mode', me.VIEW_MODE);
                    btnB.html('<i class="fa fa-trash"></i>');
                    break;
                case me.EDIT_MODE:
                    temp.data('data', selectedData);

                    select.next('.select2-container').hide();
                    label.html(selectedData.text).show();

                    btnA.data('mode', me.VIEW_MODE);
                    btnA.html('<i class="fa fa-edit"></i>');
                    btnB.data('mode', me.VIEW_MODE);
                    btnB.html('<i class="fa fa-trash"></i>');
                    break;
                case me.VIEW_MODE:
                    temp.data('data', selectedData);

                    select.next('.select2-container').show();
                    label.hide();

                    btnA.data('mode', me.EDIT_MODE);
                    btnA.html('<i class="fa fa-save"></i>');
                    btnB.data('mode', me.EDIT_MODE);
                    btnB.html('<i class="fa fa-remove"></i>');
                    break;
            }
        });

        btnB.click(function () {
            var mode = $(this).data('mode');
            switch (mode) {
                case me.ADD_MODE:
                    tr.remove();
                    break;
                case me.EDIT_MODE:
                    var oldData = temp.data('data');

                    console.log(oldData);
                    select.next('.select2-container').hide();
                    label.show();

                    // Restore Old Data
                    select2AjaxSetValue(select, oldData.id, {
                        id: oldData.id,
                        text: oldData.text
                    });
                    tdDescription.html(formatIcd10(oldData));

                    btnA.data('mode', me.VIEW_MODE);
                    btnA.html('<i class="fa fa-edit"></i>');
                    btnB.data('mode', me.VIEW_MODE);
                    btnB.html('<i class="fa fa-trash"></i>');
                    break;
                case me.VIEW_MODE:
                    tr.remove();
                    break;
            }
        });

        initializeICD10Select2(select, tdDescription, URL.getICD10);

        // Apply Mode
        switch (data.mode) {
            default:
            case me.ADD_MODE:
                select.next('.select2-container').show();
                label.hide();

                btnA.data('mode', me.ADD_MODE);
                btnA.html('<i class="fa fa-save"></i>');
                btnB.data('mode', me.ADD_MODE);
                btnB.html('<i class="fa fa-remove"></i>');
                break;
            case me.EDIT_MODE:
                select.next('.select2-container').show();
                label.hide();

                btnA.data('mode', me.EDIT_MODE);
                btnA.html('<i class="fa fa-save"></i>');
                btnB.data('mode', me.EDIT_MODE);
                btnB.html('<i class="fa fa-remove"></i>');
                break;
            case me.VIEW_MODE:
                select.next('.select2-container').hide();
                label.show();

                btnA.data('mode', me.VIEW_MODE);
                btnA.html('<i class="fa fa-edit"></i>');
                btnB.data('mode', me.VIEW_MODE);
                btnB.html('<i class="fa fa-trash"></i>');
                break;
        }
    }
}