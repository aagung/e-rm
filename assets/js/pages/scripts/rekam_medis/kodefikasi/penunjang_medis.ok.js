var OK = {
    _data: {},
    fillForm: function (data) {
        if (typeof(data) != "undefined" && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                this._data[data[i].uid] = data[i];
                this.addKunjungan(data[i]);
            }
        } else {
            $("#tab-ok").hide();
            $("a[href=#tab-ok]").closest('li').hide();
        }

        var me = this;
        // Handler
        $("#table_kunjungan_ok").on('click', '.btn-modal-ok', function (e) {
            e.preventDefault();

            var uid = $(this).data('uid');
            me.fillModal(uid);
        });
    },
    addKunjungan: function (data) {
        var tbody = $("#table_kunjungan_ok tbody");

        var tr = $("<tr/>")
            .appendTo(tbody);

        var td = $("<td/>")
            .appendTo(tr);
        var media = $("<div/>")
            .addClass('media-left media-middle')
            .appendTo(td);
        var mediaLink = $("<a/>")
            .prop('target', '_blank')
            .data('id', data.id)
            .data('uid', data.uid)
            .appendTo(media);
        var mediaIcon = $("<i/>")
            .addClass('icon-med-i-surgery')
            .appendTo(mediaLink);
        var mediaContent = $("<div/>")
            .addClass('media-left')
            .appendTo(td);
        var mediaLabel = $('<div class=""><a   class="text-default text-semibold btn-modal-ok" data-uid="' + data.uid + '">Operasi</a></div>')
            .appendTo(mediaContent);
        var mediaContentContainer = $("<div/>")
            .addClass("text-muted text-size-small")
            .appendTo(mediaContent);
        var mediaStatus = $("<span/>")
            .addClass("status-mark position-left")
            .appendTo(mediaContentContainer);
        var tanggalOperasi = moment(data.tanggal).isValid() ? moment(data.tanggal).format('DD/MM/YYYY HH:mm') : '-';
        mediaContentContainer.append(tanggalOperasi);

        var tdKode = $("<td/>")
            .appendTo(tr);
        var dispOperasi = $("<span/>")
            .addClass('text-semibold')
            .html(data.nama_operasi ? data.nama_operasi : '-')
            .appendTo(tdKode);
        tdKode.append('</br>');
        var dispJenisOperasi = $("<small/>")
            .addClass('text-muted small')
            .html(data.jenis_operasi ? data.jenis_operasi : '-')
            .appendTo(tdKode);

        var tdStatus = $("<td/>")
            .appendTo(tr);
        var dispStatus = $("<span/>")
            .addClass("label")
            .appendTo(tdStatus);

        
        // Status
        mediaLink.addClass("text-success");
        mediaStatus.addClass("border-success");
        dispStatus.addClass("bg-success").html("Selesai");
    },
    fillModal: function (uid) {
        var me = this;
        var form = $("#form_modal_ok");
        var modal = $("#modal_ok");
        var data = this._data[uid];
        console.log(data);
        
        var diagnosa = '';
        for (var i = 0; i < data.diagnosa.length; i++) {
            diagnosa += '<li>' + data.diagnosa[i].code + ' - ' + data.diagnosa[i].description + '</li>';
        }

        var asisten = '';
        for (var i = 0; i < data.asisten.length; i++) {
            asisten += '<li>' + data.asisten[i].text + '</li>';
        }

        var asistenAnestesi = '';
        for (var i = 0; i < data.asisten_anestesi.length; i++) {
            asistenAnestesi += '<li>' + data.asisten_anestesi[i].text + '</li>';
        }

        var asistenOperator = '';
        for (var i = 0; i < data.asisten_operator.length; i++) {
            asistenOperator += '<li>' + data.asisten_operator[i].text + '</li>';
        }

        var dokterAnestesi = '';
        for (var i = 0; i < data.dokter_anestesi.length; i++) {
            dokterAnestesi += '<li>' + data.dokter_anestesi[i].text + '</li>';
        }

        var dokterKonsulen = '';
        for (var i = 0; i < data.dokter_konsulen.length; i++) {
            dokterKonsulen += '<li>' + data.dokter_konsulen[i].text + '</li>';
        }

        var dokterOperator = '';
        for (var i = 0; i < data.dokter_operator.length; i++) {
            dokterOperator += '<li>' + data.dokter_operator[i].text + '</li>';
        }

        var statusClass = '';
        var statusText = '';

        var content = TEMPLATE_OK_PELAKSANAAN
            .replace(/\{\{TANGGAL\}\}/g, moment(data.tanggal).format('HH:mm'))
            .replace(/\{\{TANGGAL_OPERASI\}\}/g, moment(data.tanggal).format('DD/MM/YYYY'))
            .replace(/\{\{WAKTU_ANESTESI\}\}/g, data.jam_mulai_anesthesi.substring(0, 5))
            .replace(/\{\{RUANG\}\}/g, data.ruang)
            .replace(/\{\{OPERASI\}\}/g, data.operasi)
            .replace(/\{\{NAMA_OPERASI\}\}/g, data.nama_operasi)
            .replace(/\{\{JENIS_OPERASI\}\}/g, data.jenis_operasi)
            .replace(/\{\{DIAGNOSA\}\}/g, diagnosa)
            .replace(/\{\{ASISTEN\}\}/g, asisten)
            .replace(/\{\{ASISTEN_ANESTESI\}\}/g, asistenAnestesi)
            .replace(/\{\{ASISTEN_OPERATOR\}\}/g, asistenOperator)
            .replace(/\{\{DOKTER_ANESTESI\}\}/g, dokterAnestesi)
            .replace(/\{\{DOKTER_KONSULEN\}\}/g, dokterKonsulen)
            .replace(/\{\{DOKTER_OPERATOR\}\}/g, dokterOperator)
            .replace(/\{\{CATATAN\}\}/g, data.catatan)
            .replace(/\{\{STATUS_TITLE\}\}/g, statusText)
            .replace(/\{\{STATUS_CLASS\}\}/g, statusClass)
            .replace(/\{\{PETUGAS\}\}/g, data.petugas);

        form.empty();
        form.html(content);

        form.find('[data-popup=tooltip]').tooltip();
        $("#modal_ok").modal('show');
    }
}

var TEMPLATE_OK_PELAKSANAAN = 
'<div class="modal-content">' +
    '<div class="modal-header bg-grey">' +
        '<button type="button" class="close" data-dismiss="modal">×</button>' +
        '<h6 class="modal-title">Pelaksanaan Operasi {{OPERASI}}</h6>' +
        '<h5 class="no-margin-top">' +
        'Operasi {{NAMA_OPERASI}} <small data-popup="tooltip" data-trigger="hover" data-placement="top" title="Jenis Operasi">{{JENIS_OPERASI}}</small>' +
        '</h5>' +
    '</div>' +
    '<div class="modal-body">' +
        '<div class="row">' +
            '<div class="col-md-12">' +
                '<div class="list-group no-border no-padding-top">' +
                    '<a href="#" class="list-group-item text-bold">' +
                        '<i class="icon-med-i-inpatient" data-popup="tooltip" title="Ruang Operasi" data-trigger="hover" data-placement="left"></i>' +
                        '{{RUANG}}' +
                    '</a>' +
                    '<a href="#" class="list-group-item text-bold">' +
                        '<i class="icon-calendar22" data-popup="tooltip" title="Tanggal Operasi" data-trigger="hover" data-placement="left"></i>' +
                        '{{TANGGAL_OPERASI}}' +
                    '</a>' +
                    '<a href="#" class="list-group-item text-bold">' +
                        '<i class="icon-watch2" data-popup="tooltip" title="Waktu Anestesi" data-trigger="hover" data-placement="left"></i>' +
                        '{{WAKTU_ANESTESI}}' +
                    '</a>' +
                '</div>' +
                '<h6>Diagnosa</h6>' +
                '<ul>{{DIAGNOSA}}</ul>' +
            '</div>' +
        '</div>' +
        '<div class="row">' +
            '<div class="col-md-6">' +
                '<h6>Dokter Operator</h6>' +
                '<ul>{{DOKTER_OPERATOR}}</ul>' +
                '<h6>Dokter Anestesi</h6>' +
                '<ul>{{DOKTER_ANESTESI}}</ul>' +
                '<h6>Dokter Konsulen</h6>' +
                '<ul>{{DOKTER_KONSULEN}}</ul>' +
            '</div>' +
            '<div class="col-md-6">' +
                '<h6>Asisten</h6>' +
                '<ul>{{ASISTEN}}</ul>' +
                '<h6>Asisten Anestesi</h6>' +
                '<ul>{{ASISTEN_ANESTESI}}</ul>' +
                '<h6>Asisten Operator</h6>' +
                '<ul>{{ASISTEN_OPERATOR}}</ul>' +
            '</div>' +
            '<div class="col-md-12">' + 
                '<h6><i class="icon-clipboard"></i> Catatan</h6>' +
                '<blockquote>' +
                    '<p>{{CATATAN}}</p>' +
                '</blockquote>' +
            '</div>' +
        '</div>' +
    '</div>' +
    '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default tutup-button" data-dismiss="modal">Tutup</button>' +
    '</div>' +
'</div>';