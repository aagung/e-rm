var FARMASI = {
    _data: {},
    fillForm: function (data) {
        if (typeof(data) != "undefined" && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                this._data[data[i].uid] = data[i];
                this.addKunjungan(data[i]);
            }
        } else {
            $("#tab-farmasi").hide();
            $("a[href=#tab-farmasi]").closest('li').hide();
        }

        var me = this;
        // Handler
        $("#table_kunjungan_farmasi").on('click', '.btn-modal-farmasi', function (e) {
            e.preventDefault();

            var uid = $(this).data('uid');
            me.fillModal(uid);
        });
    },
    addKunjungan: function (data) {
        var tbody = $("#table_kunjungan_farmasi tbody");

        var tr = $("<tr/>")
            .appendTo(tbody);

        var td = $("<td/>")
            .appendTo(tr);
        var media = $("<div/>")
            .addClass('media-left media-middle')
            .appendTo(td);
        var mediaLink = $("<a/>")
            .prop('target', '_blank')
            .data('id', data.id)
            .data('uid', data.uid)
            .appendTo(media);
        var mediaIcon = $("<i/>")
            .addClass('icon-med-pharmacy')
            .appendTo(mediaLink);
        var mediaContent = $("<div/>")
            .addClass('media-left')
            .appendTo(td);
        var mediaLabel = $('<div class=""><a   class="text-default text-semibold btn-modal-farmasi" data-uid="' + data.uid + '">Resep</a></div>')
            .appendTo(mediaContent);
        var mediaContentContainer = $("<div/>")
            .addClass("text-muted text-size-small")
            .appendTo(mediaContent);
        var mediaStatus = $("<span/>")
            .addClass("status-mark position-left")
            .appendTo(mediaContentContainer);
        var tanggal = moment(data.tanggal).isValid() ? moment(data.tanggal).format('DD/MM/YYYY HH:mm') : '-';
        mediaContentContainer.append(tanggal);

        var tdKode = $("<td/>")
            .appendTo(tr);
        var dispKode = $("<span/>")
            .addClass('text-semibold')
            .html(data.kode ? data.kode : '-')
            .appendTo(tdKode);
        tdKode.append('</br>');
        var dispDepo = $("<small/>")
            .addClass('text-muted small')
            .html(data.depo ? data.depo : '-')
            .appendTo(tdKode);

        var tdStatus = $("<td/>")
            .appendTo(tr);
        var dispStatus = $("<span/>")
            .addClass("label")
            .appendTo(tdStatus);

        
        // Status
        mediaLink.addClass("text-success");
        mediaStatus.addClass("border-success");
        dispStatus.addClass("bg-success").html("Selesai");
    },
    fillModal: function (uid) {
        var me = this;
        var form = $("#form_modal_farmasi");
        var modal = $("#modal_farmasi");
        var data = this._data[uid];

        form.find('.disp_kode_transaksi').html(data.kode);
        form.find('.disp_tanggal').html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        form.find('.disp_dokter').html(data.dokter);
        
        var tbody = form.find('.table_obat tbody');
        tbody.empty();
        for (var i = 0; i < data.obat.length; i++) {
            me.addModalObat(data.obat[i]);
        }

        modal.modal('show');
    },
    addModalObat: function (data) {
        var form = $("#form_modal_farmasi");
        var tbody = form.find('.table_obat tbody');

        var nama = data.nama;
        var quantity = data.quantity;
        var satuan = data.satuan;
        var caraMakanContent = this.getCaraMakan(data.signa);
        var content = TEMPLATE_FARMASI_OBAT
            .replace(/\{\{NAMA\}\}/g, nama + ' <b>X</b> ' + quantity + ' ' + satuan)
            .replace(/\{\{CARA_MAKAN\}\}/g, caraMakanContent)
        tbody.append(content);
    },
    getCaraMakan: function (signa) {
        if (typeof signa.cara_makan == 'undefined' || !signa.cara_makan) return "";

        var content = '';
        for (var i = 0; i < signa.cara_makan.length; i++) {
            content += '<li>' + signa.cara_makan[i].label + '</li>';
        }

        return content;
    },
}

var TEMPLATE_FARMASI_OBAT = 
'<tr>' +
    '<td>' +
        '<div class="media-left media-middle">' +
            '<i class="icon-med-pharmacy"></i>' +
        '</div>' +
        '<div class="media-left">' +
            '<div class="">' +
                '<span class="text-default text-semibold btn-modal-farmasi" data-uid="undefined">{{NAMA}}</span>' +
            '</div>' +
        '</div>' +
    '</td>' +
    '<td>' +
        '<ul>{{CARA_MAKAN}}</ul>' +
    '</td>' +
'</tr>';