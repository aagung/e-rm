var RawatInap = {
    fillForm: function (data) {
        if (data) {
            // History Bed
            $("#table_history_bed tbody").empty();
            if (data.history_bed.length > 0) {
                for (var i = 0; i < data.history_bed.length; i++) {
                    this.addHistoryBed(data.history_bed[i]);
                }
            } else {
                $("#table_history_bed tbody").html('<tr><td class="text-center">Tidak ada history perpindahan bed yang dapat ditampilkan.</td></tr>');
            }

            // Tindakan
            $("#table_rawat_inap_tindakan tbody").empty();
            if (data.tindakan.length > 0) {
                for (var i = 0; i < data.tindakan.length; i++) {
                    this.addTindakan(data.tindakan[i]);
                }
                $("#table_rawat_inap_tindakan").dataTable();
            } else {
                $("#table_rawat_inap_tindakan tbody").html('<tr><td colspan="3" class="text-center">Tidak ada tindakan yang dilakukan.</td></tr>');
            }

            // Visite
            $("#table_rawat_inap_visite tbody").empty();
            if (data.visite.length > 0) {
                for (var i = 0; i < data.visite.length; i++) {
                    this.addVisite(data.visite[i]);
                }
                $("#table_rawat_inap_visite").dataTable();
            } else {
                $("#table_rawat_inap_visite tbody").html('<tr><td colspan="3" class="text-center">Tidak ada visite yang dilakukan.</td></tr>');
            }

            // BHP
            $("#table_rawat_inap_bhp tbody").empty();
            if (data.bhp.length > 0) {
                for (var i = 0; i < data.bhp.length; i++) {
                    this.addBHP(data.bhp[i]);
                }
                $("#table_rawat_inap_bhp").dataTable();
            } else {
                $("#table_rawat_inap_bhp tbody").html('<tr><td colspan="3" class="text-center">Tidak ada bhp yang diberikan.</td></tr>');
            }
            
        }
    },
    addHistoryBed: function (data) {
        var tbody = $("#table_history_bed tbody");

        var iconBed = '<i class="icon-bed2"></i>';
        var bedDari = '';
        var bedKe = '';

        var status = '';
        var statusClass = '';
        var statusTitle = '';
        var iconSeparator = '';
        switch (parseInt(data.status)) {
            case 0:
                status = 'Masuk (Masuk Baru)';
                statusClass = 'icon-enter3 text-muted';
                statusTitle = 'Masuk Baru';
                iconSeparator = ' <i class="icon-med-i-outpatient"></i> <i class="icon-arrow-right7"></i>';

                bedDari = iconBed + ' ' + getValue(data.bed_dari)
                break;
            case 1:
                status = 'Masuk (Dipindahkan)';
                statusClass = 'icon-transmission text-info';
                statusTitle = 'Dipindahkan';
                iconSeparator = '<i class="icon-arrow-right7"></i>';

                bedDari = iconBed + ' ' + getValue(data.bed_dari);
                break;
            case 2:
                /*status = 'Dipindahkan';
                statusClass = 'icon-transmission text-info';
                statusTitle = 'Dipindahkan';
                iconSeparator = '<i class="icon-arrow-right7"></i>';

                bedDari = iconBed + ' ' + getValue(data.bed_dari);*/
                break;
            case 3:
                status = 'Keluar (Checkout)';
                statusClass = 'icon-exit3 text-muted';
                statusTitle = 'Checkout';
                iconSeparator = '<i class="icon-med-i-outpatient"></i> <i class="icon-arrow-left7"></i>';

                bedDari = iconBed + ' ' + getValue(data.bed_dari);
                break;
        }


        var tr = $("<tr/>")
            .appendTo(tbody);

        var tdIcon = $("<td/>")
            .appendTo(tr);
        var mediaIcon = $("<div/>")
            .addClass('media-left media-middle')
            .html('<i class="' + statusClass + '"></i>')
            .appendTo(tdIcon);
        var mediaText = $("<div/>")
            .addClass('media-left')
            .appendTo(tdIcon);
        var mediaTextContent = $("<div/>")
            .html('<span class="text-default text-semibold">' + status + '</span>')
            .appendTo(mediaText);
        var mediaTextSubContent = $("<div/>")
            .addClass('text-muted text-size-small')
            .html('<span class="status-mark position-left border-success"></span> ' + moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(mediaText);

        var tdSeparator = $("<td/>")
            .html(iconSeparator)
            .addClass('text-center')
            .appendTo(tr);

        var tdBedDari = $("<td/>")
            .html(bedDari)
            .appendTo(tr);

    },
    addTindakan: function (data) {
        var tbody = $("#table_rawat_inap_tindakan tbody");
        
        var tr = $("<tr/>")
            .appendTo(tbody);

        // td tanggal
        var tdTanggal = $("<td/>")
            .appendTo(tr);
        var labelTanggal = $("<label/>")
            .html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(tdTanggal);
        
        // td Tindakan
        var quantity = parseInt(data.quantity) > 1 ? ' X ' + parseInt(data.quantity) : '';
        var tdTindakan = $("<td/>")
            .appendTo(tr);
        var labelTindakan = $("<label/>")
            .html(data.nama + quantity)
            .appendTo(tdTindakan);

        // Td Dokter
        var tdDokter = $("<td/>")
            .html(getValue(data.dokter))
            .appendTo(tr);
    },
    addVisite: function (data) {
        var tbody = $("#table_rawat_inap_visite tbody");
        
        var tr = $("<tr/>")
            .appendTo(tbody);

        // td tanggal
        var tdTanggal = $("<td/>")
            .appendTo(tr);
        var labelTanggal = $("<label/>")
            .html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(tdTanggal);
        
        // td Tindakan
        var quantity = parseInt(data.quantity) > 1 ? ' X ' + parseInt(data.quantity) : '';
        var tdTindakan = $("<td/>")
            .appendTo(tr);
        var labelTindakan = $("<label/>")
            .html(data.uraian)
            .appendTo(tdTindakan);

        // Td Dokter
        var tdDokter = $("<td/>")
            .html(getValue(data.dokter))
            .appendTo(tr);
    },
    addBHP: function (data) {
        var tbody = $("#table_rawat_inap_bhp tbody");
        
        var tr = $("<tr/>")
            .appendTo(tbody);

        // td tanggal
        var tdTanggal = $("<td/>")
            .appendTo(tr);
        var labelTanggal = $("<label/>")
            .html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(tdTanggal);
        
        // td Tindakan
        var quantity = parseInt(data.quantity) > 1 ? ' X ' + parseInt(data.quantity) : '';
        var tdTindakan = $("<td/>")
            .appendTo(tr);
        var labelTindakan = $("<label/>")
            .html(data.obat + quantity)
            .appendTo(tdTindakan);
    }
};