$(document).ready(function () {
    var FORM = $("#form");

    function fillForm(uid) {
        $.getJSON(URL.getData.replace(':UID', uid), function (data, status) {
            if (status === 'success') {
                console.log(data);
                data = data.data;
                OBJECT = data;

                var pasien = data.pasien;
                var pelayanan = data.pelayanan;
                var igd = data.igd;
                
                // Data Pelayanan
                $(".disp_pelayanan_no_register").html(pelayanan.no_register);
                $(".disp_pelayanan_tanggal").html(moment(pelayanan.created_at).format('DD/MM/YYYY'));
                $(".disp_pelayanan_layanan").html(data.layanan);
                $(".disp_pelayanan_dokter").html(data.pelayanan.dokter);

                // Biodata Pasien
                fillPasien(pasien);

                // Data Pemeriksaan
                IGD.fillForm(igd);
                Laboratorium.fillForm(data.laboratorium);
                Radiologi.fillForm(data.radiologi);
                OK.fillForm(data.ok);
                FARMASI.fillForm(data.farmasi);

                // Data Form
                DiagnosaSekunder.fillForm(data.igd.diagnosa_sekunder);
                TindakanInvasif.fillForm(data.tindakan_invasif);

                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#pelayanan_id").val(data.pelayanan_id);
                $("#pelayanan_uid").val(data.pelayanan_uid);
                $("#pasien_id").val(data.pasien_id);
                $("#pasien_uid").val(data.pasien_uid);
                $("#pasien_no_rekam_medis").val(data.pasien_no_rekam_medis);
                $("#pasien_nama").val(data.pasien_nama);
                $("#pasien_alamat").val(data.pasien_alamat);
                $("#pasien_jenis_kelamin").val(data.pasien_jenis_kelamin);
                $("#pasien_tgl_lahir").val(data.pasien_tgl_lahir);
                $("#status").val(data.status);
                $("#tindak_lanjut_id").val(data.tindak_lanjut_id).trigger('change');
                $("#tindak_lanjut_menolak_id").val(data.tindak_lanjut_menolak_id).trigger('change');
                $("#sebab_luar_id").val(data.sebab_luar_id).trigger('change');
                $("#kegawatan_id").val(data.kegawatan_id).trigger('change');

                if ($("#catatan").data('wysihtml5')) {
                    $("#catatan").data('wysihtml5').editor.setValue(data.catatan);
                } else {
                    $("#catatan").val(data.catatan);
                }


                $("#unit_kontrol_deskripsi").html(formatIcd10({
                    code: data.unit_kontrol_code, 
                    description: data.unit_kontrol_description
                }));
                $("#diagnosa_utama_deskripsi").html(formatIcd10({
                    code: data.igd.diagnosa_utama[0].code, 
                    description: data.igd.diagnosa_utama[0].description
                }));
                $("#external_cause_deskripsi").html(formatIcd10({
                    code: data.external_cause_code, 
                    description: data.external_cause_description
                }));

                // Select2 Ajax
                select2AjaxSetValue($("#unit_kontrol_id"), data.unit_kontrol_id, { 
                    id: data.unit_kontrol_id, text: data.unit_kontrol
                });
                 select2AjaxSetValue($("#diagnosa_utama_id"), data.igd.diagnosa_utama[0].id, { 
                    id: data.igd.diagnosa_utama[0].id, text: data.igd.diagnosa_utama[0].code +'-'+  data.igd.diagnosa_utama[0].description
                });
                select2AjaxSetValue($("#external_cause_id"), data.external_cause_id, { 
                    id: data.external_cause_id, text: data.external_cause
                });

                $("select").trigger('change');
            }
        })
    }

    function fillPasien(data) {
        var alamat = data.alamat + '<br>' + data.kelurahan + ", " + data.kecamatan + ", " + data.kabupaten + ", " + data.provinsi + " " + data.kodepos;
        $(".disp_pasien_nama").html(data.nama);
        $(".disp_pasien_no_rekam_medis").html(data.no_rm);
        $(".disp_pasien_alamat").html(alamat);
        $(".disp_pasien_jenis_kelamin").html(getJenisKelamin(data.jenis_kelamin));
        $(".disp_pasien_agama").html(data.agama);
        $(".disp_pasien_tempat_lahir").html(data.tempat_lahir_id);
        $(".disp_pasien_umur").html(data.umur_tahun + ' Tahun');
        $(".disp_pasien_tgl_lahir").html(moment(data.tgl_lahir).format('DD/MM/YYYY'));
        $(".disp_pasien_gol_darah").html(data.gol_darah);
        $(".disp_pasien_no_telepon_1").html(data.no_telepon_1);
        
        if (data.foto == "" || data.foto == null || ! data.foto) {
            if (parseInt(data.jenis_kelamin) === 1) {
                $(".disp_pasien_photo").prop('src', IMAGES.defaultMale);
            } else {
                $(".disp_pasien_photo").prop('src', IMAGES.defaultFemale);
            }
        } else {
            $(".disp_pasien_photo").prop('src', URL.getFoto.replace(':FOTO', data.foto));
        }
    }
    $("#tindak_lanjut_id").on('change', function () {
        var option = $(this).find('option:selected');
        var menolak = parseInt(option.data('menolak'));
        if (menolak == 1) {
            $("#tindak_lanjut_menolak_id").closest('.form-group').show('slow');
        } else {
            $("#tindak_lanjut_menolak_id").closest('.form-group').hide('slow');
        }
    });

    $("#tindak_lanjut_menolak_id").on('change', function () {
        //
    });

    $("#sebab_luar_id").on('change', function () {
        //
    });

    $("#kegawatan_id").on('change', function () {
        //
    });

    $("#btn-tambah_diagnosa_sekunder").click(function () {
        DiagnosaSekunder.addRow({mode: DiagnosaSekunder.ADD_MODE});
    });

    $("#btn-tambah_tindakan_icd9").click(function () {
        TindakanInvasif.addRow({mode: TindakanInvasif.ADD_MODE});
    });

    // Diagnosa Utama
    initializeICD10Select2($("#diagnosa_utama_id"), $("#diagnosa_utama_deskripsi"), URL.getICD10);

    // Unit Kontrol
    initializeICD10Select2($("#unit_kontrol_id"), $("#unit_kontrol_deskripsi"), URL.getICD10);

    // External Causes
    initializeICD10Select2($("#external_cause_id"), $("#external_cause_deskripsi"), URL.getICD10);

    fillForm(UID);

    $("#btn-submit").click(function (e) {
        e.preventDefault();
        FORM.submit();
    });

    FORM.validate({
        rules: {
            //
        },
        messages: {
            //
        },
        focusInvalid: true,
        errorPlacement: function(error, element) {
            var inputGroup = $(element).closest('.input-group');
            var checkbox = $(element).closest('.checkbox-inline');

            if (inputGroup.length) {
                error.insertAfter(inputGroup);
            } else if (checkbox.length) {
                checkbox.append(error);
            } else {
                $(element).closest("div").append(error);
            }
        },
        submitHandler: function (form) {
            blockPage();
            var postData = $(form).serialize();
            $.ajax({
                url: URL.save,
                type: 'POST',
                dataType: "json",
                data: postData,
                success: function (data) {
                    console.log(data);
                    successMessage('Success', 'Data berhasil disimpan.');

                    $('[type=submit], #btn-submit').prop('disabled', true);
                    $('input, select, textarea, [type=submit]').prop('disabled', true);

                    setTimeout(function () {
                        window.location.assign(URL.index);
                    }, 3000);
                },
                error: function () {
                    $.unblockUI();
                    errorMessage('Error', 'Terjadi kesalahan saat hendak menyimpan data.');
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }
    });
});
