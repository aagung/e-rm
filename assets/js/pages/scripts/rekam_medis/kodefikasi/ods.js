var ODS = {
    fillForm: function (data, hideIfNull) {
        if (data) {
            // Pemeriksaan Awal
            $(".disp_pelayanan_tinggi_badan").html(formatTinggi(getValue(data.tinggi)));
            $(".disp_pelayanan_berat_badan").html(formatBerat(getValue(data.berat)));
            $(".disp_pelayanan_suhu_badan").html(formatSuhu(getValue(data.suhu)));
            $(".disp_pelayanan_tensi").html(getValue(data.tensi));
            $(".disp_pelayanan_kasus").html(getKasus(getValue(data.kasus)));

            // Pemeriksaan Dokter
            $(".disp_pelayanan_anamnesa").html(getValue(data.anamnesa));
            $(".disp_pelayanan_alergi").html(getValue(data.alergi));
            $(".disp_pelayanan_pemeriksaan_fisik").html(getValue(data.terapi));

            // Diagnosa
            $(".disp_list_pelayanan_diagnosa").empty();
            for (var i = 0; i < data.diagnosa.length; i++) {
                $(".disp_list_pelayanan_diagnosa")
                    .append('<li>' + data.diagnosa[i].code + ' - ' + data.diagnosa[i].description + '</li>');
            }

            // Diagnosa
            $(".disp_list_pelayanan_diagnosa").empty();
            for (var i = 0; i < data.diagnosa.length; i++) {
                $(".disp_list_pelayanan_diagnosa")
                    .append('<li>' + data.diagnosa[i].code + ' - ' + data.diagnosa[i].description + '</li>');
            }

            // Tindakan
            $("#table_ods_tindakan tbody").empty();
            if (data.tindakan.length > 0) {
                for (var i = 0; i < data.tindakan.length; i++) {
                    this.addTindakan(data.tindakan[i]);
                }
                $("#table_ods_tindakan").dataTable();
            } else {
                $("#table_ods_tindakan tbody").html('<tr><td colspan="3" class="text-center">Tidak ada tindakan yang dilakukan.</td></tr>');
            }
            
        } else {
            if (hideIfNull) {
                $("#tab-ods").hide();
                $("a[href=#tab-ods]").closest('li').hide();
            } else {
                $(".data-ods").hide();
                $(".empty-ods").show();
            }
        }
    },
    addTindakan: function (data) {
        var tbody = $("#table_ods_tindakan tbody");
        
        var tr = $("<tr/>")
            .appendTo(tbody);

        // td tanggal
        var tdTanggal = $("<td/>")
            .appendTo(tr);
        var labelTanggal = $("<label/>")
            .html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'))
            .appendTo(tdTanggal);
        
        // td Tindakan
        var quantity = parseInt(data.quantity) > 1 ? ' X ' + parseInt(data.quantity) : '';
        var tdTindakan = $("<td/>")
            .appendTo(tr);
        var labelTindakan = $("<label/>")
            .html(data.nama + quantity)
            .appendTo(tdTindakan);
            
        tbody.append(tr);
    }
};