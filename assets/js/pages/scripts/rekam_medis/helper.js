// Functions
function parseBerkas(jenis) {
    switch (parseInt(jenis)) {
        case 1: // Rawat Jalan
            return 'Rawat Jalan';
        case 3: // Rawat Inap
            return 'Rawat Inap';
        case 2: // IGD
            return 'IGD';
        case 4: // Penunjang Medis
            return 'Penunjang Medis';
        default:
            return "";
    }
}

function getBerkasIcon(jenis) {
    switch (parseInt(jenis)) {
        case 1: // Rawat Jalan
            return '<i class="icon-med-outpatient" data-popup="tooltip" data-placement="top" data-trigger="hover" title="Rawat Jalan"></i>';
        case 3: // Rawat Inap
            return '<i class="icon-med-inpatient" data-popup="tooltip" data-placement="top" data-trigger="hover" title="Rawat Inap"></i>';
        case 2: // IGD
            return '<i class="icon-med-emergency" data-popup="tooltip" data-placement="top" data-trigger="hover" title="IGD"></i>';
        case 4: // ODS/ODC
            return '<i class="icon-med-surgery" data-popup="tooltip" data-placement="top" data-trigger="hover" title="ODS/ODC"></i>';
        case 5: // Penunjang Medis
            return '<i class="icon-med-internal-medicine" data-popup="tooltip" data-placement="top" data-trigger="hover" title="Penunjang Medis"></i>';
        default:
            return "";
    }
}

function getValue(value) {
    if (typeof(value) == 'undefined' || value == null || value == "") {
        return '-';
    }
    return value;
}

function getJenisKelamin(data) {
    if (parseInt(data) === 1) 
        return "Laki-Laki";
    return "Perempuan";
}

function formatTinggi(data) {
    return numeral(data).format('0,0') + ' cm';
}
function formatBerat(data) {
    return numeral(data).format('0,0') + ' Kg';
}
function formatSuhu(data) {
    return numeral(data).format('0,0') + ' °C';
}
function getKasus(data) {
    if (parseInt(data) === 1) {
        return "Baru";
    }

    return "Lama";
}


// Format displayed data
function formatIcd10 (icd10) {
    var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'><b>" + icd10.code + "</b></div>" +
                "<div class='select2-result-repository__description'>" + icd10.description + "</div>";

    markup += "<div class='select2-result-repository__statistics'>";

    if (icd10.severity) {
        markup += "<div class='select2-result-repository__forks'>Severity: " + icd10.severity + "</div>";
    }

    if (icd10.in_patient) {
        markup += "<div class='select2-result-repository__forks'>In: " + icd10.in_patient + "</div>";
    }

    if (icd10.out_patient) {
        markup += "<div class='select2-result-repository__forks'>Out: " + icd10.out_patient + "</div>";
    }

    markup += "</div></div>";

    return markup;
}

// Format selection
function formatIcd10Selection (icd10) {
    return icd10.text;
}

function initializeICD10Select2(jqSelect, jqDescription, url) {
    jqSelect.select2({
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {

                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatIcd10, // omitted for brevity, see the source of this page
        templateSelection: formatIcd10Selection // omitted for brevity, see the source of this page
    }).on("select2:select", function (e) {
        var data = e.params.data
        var content = formatIcd10(data);
        
        jqDescription.html(content);
    });
}

// ICD 9
function formatIcd9 (icd9) {
    var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'><b>" + icd9.nm_cbgs + "</b></div>";

    markup += "<div class='select2-result-repository__statistics'>";

    if (icd9.d2) {
        markup += "<div class='select2-result-repository__forks'>D2: " + icd9.d2 + "</div>";
    }

    if (icd9.d3) {
        markup += "<div class='select2-result-repository__forks'>D3: " + icd9.d3 + "</div>";
    }

    if (icd9.severity) {
        markup += "<div class='select2-result-repository__forks'>Severity: " + icd9.severity + "</div>";
    }

    markup += "</div></div>";

    return markup;
}

// Format selection
function formatIcd9Selection (icd9) {
    return icd9.text;
}

function initializeICD9Select2(jqSelect, jqDescription, url) {
    jqSelect.select2({
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {

                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatIcd9, // omitted for brevity, see the source of this page
        templateSelection: formatIcd9Selection // omitted for brevity, see the source of this page
    }).on("select2:select", function (e) {
        var data = e.params.data
        var content = formatIcd9(data);
        
        jqDescription.html(content);
    });
}

function select2AjaxSetValue(jqSelect2, value, option) {
    var opt = new Option(option.text, option.id, false, false);
    jqSelect2.append(opt).trigger('change');
    jqSelect2.val(value).trigger('change');
}

function getWaktu(data){
    var one_day=1000*60*60*24;

    var date1_ms = new Date().getTime();
    var date2_ms = new Date(data).getTime();

    var difference_ms = date1_ms - date2_ms;
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var hours = Math.floor(difference_ms % 24);  
    var days = Math.floor(difference_ms/24);

    return days + ' Hari ' + hours + ' Jam ' + minutes + ' Menit ';
}