var DaftarPasien = function () {

	var oTableDaftarPasien;
	
	var handleDaftarPasien = function() {
        
        if (!jQuery().DataTable) {
            return;
        }
        
        oTableDaftarPasien = $("#table_pasien").DataTable({
			"processing"	: true,
            "serverSide"	: true,
            "ajax"			: {
                "url": url_load_data,
                "type": "GET"
            },
			"order"         : [[ 2, "asc" ]],
			"columnDefs": [
                { "targets": [ 0 ], "visible": false },
                { "targets": [ 1 ], "visible": false }
            ]
		});
        
    };
    
    return {

        init: function() {
		
			handleDaftarPasien();
			
			$('#table_pasien').on('click', '.edit_row', function() {
				var uid = $(this).data('uid');
				window.location = url_kasir + '?pelayanan_uid=' + uid;
				return false;
			});
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
                /* switch (xhr.responseJSON.tag) {
                    case 'load_lookup_vendor':
                        $('#vendor_modal .modal-body').show();
                        $('#vendor_modal .loading').hide();
                        break;
                    case 'load_lookup_perkiraan':
                        $('#perkiraan_modal .modal-body').show();
                        $('#perkiraan_modal .loading').hide();
                        break;
                    case 'load_lookup_barang':
                        $('#barang_modal .modal-body').show();
                        $('#barang_modal .loading').hide();
                        break;
                }; */
            });
			
        }

    };

}();