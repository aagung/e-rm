function StatusLokalis (canvasId, options) {
    var me = this;
    this.canvas = $('#' + canvasId);
    this.imgColor = [0, 0, 0];
    this.lastPos = [50, 50];
    this.mode; // Text, Polygon, Select, paint, line

    // State
    this.buttonDown = false;

    this.settings = $.extend({
        font: {
            size: '12px',
            family: 'Arial',
            color: 'rgba(255, 0, 0, 0.8)'
        },
        polygon: {
            fill: true,
            color: 'rgba(255, 0, 0, 0.8)',
            foreground: 'rgba(255, 0, 0, 0.2)'
        },
        line: {
            color: '#ee0000',
            size: 3
        },
        point: {
            color: '#ee0000',
            size: 4
        }
    }, options);

    // 
    this.activeGeometry = null;
    this.geometry = [];
    this.redraw = function () {
        var canvas = this.canvas.get(0);
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
        ctx.canvas.width = this.canvas.innerWidth();
        ctx.canvas.height = this.canvas.innerHeight();

        if (this.background) { // Background
            ctx.drawImage(this.background.image, this.background.x, this.background.y, ctx.canvas.width, ctx.canvas.height);
        }

        var geometry, prevGeometry, nextGeometry;
        for (var i = 0; i < this.geometry.length; i++) {
            geometry = this.geometry[i];
            prevGeometry = i > 0 ? this.geometry[i-1] : null;
            nextGeometry = i+1 < this.geometry.length ? this.geometry[i+1] : null;
            renderGeometry(canvas, ctx, geometry, prevGeometry, nextGeometry);
        }

        if (this.activeGeometry) {
            renderGeometry(canvas, ctx, this.activeGeometry, null, null, true);
        }
    }

    this.canvas.mouseup(function () {
        me.buttonDown = false;
    });

    this.canvas.mouseout(function () {
        me.buttonDown = false;
    });

    this.canvas.on("contextmenu",function() {
        if (arguments.length > 0) {
            var arg = arguments[0];
            var ctx = me.canvas.get(0).getContext("2d");
            var offsets = getOffsets(arg);

            // // Get Last Geometry
            // var isDrawing = me.activeGeometry != null; // APakah sedang menggambar??
            // if (! me.activeGeometry) {
            //     me.activeGeometry = me.geometry.pop();
            // }

            // if (! me.activeGeometry) {
            //     return false;
            // }

            // switch (true) {
            //     case me.activeGeometry.type == 'paint':
            //         me.activeGeometry = null;
            //         break;
            //     case me.activeGeometry.type == 'line' && isDrawing:
            //         me.activeGeometry = null;
            //         break;
            //     case me.activeGeometry.type == 'polygon' && me.activeGeometry.coords > 2:
            //         me.activeGeometry.coords.done = false;
            //         me.activeGeometry.coords.pop();
            //         break;
            //     case me.activeGeometry.type == 'polygon' && me.activeGeometry.coords <= 2:
            //         me.activeGeometry = null;
            //         break;
            // }
        }
        return false;
    });

    // Double Click
    // this.canvas.dblclick(function () {
    //     console.log("DOUBLE CLICK");
    //     if (!me.mode)
    //         return;

    //     var arg = arguments[0];
    //     var ctx = me.canvas.get(0).getContext("2d");
    //     var offsets = getOffsets(arg);
    //     switch (true) {
    //         case me.mode == 'polygon' && me.activeGeometry && me.activeGeometry.type == 'polygon': // Close Polygon
    //             var coord = {
    //                 x: offsets.offsetX,
    //                 y: offsets.offsetY
    //             };

    //             me.activeGeometry.done = true;
    //             me.activeGeometry.coords.pop();
    //             me.activeGeometry.coords.push(coord);
    //             addPolygon(me.activeGeometry);
    //             me.activeGeometry = null;

    //             me.redraw();
    //             break;
    //     }

    //     return true;
    // });

    // Mouse Down
    this.canvas.mousedown(function (event) {
        console.log("MOUSEDONW");
        if (!me.mode)
            return;

        if (arguments.length > 0) {
            var arg = arguments[0];
            var ctx = me.canvas.get(0).getContext("2d");
            var offsets = getOffsets(arg);

            if (event.which == 1) { // Left Click
                me.buttonDown = true;
                switch (true) {
                    case me.mode == 'paint':
                        addPoint(offsets.offsetX, offsets.offsetY, false);
                        break;
                    case me.mode == 'line' && ! me.activeGeometry: // Create Line
                        me.activeGeometry = {
                            type: 'line',
                            x1: offsets.offsetX,
                            y1: offsets.offsetY,
                            x2: offsets.offsetX,
                            y2: offsets.offsetY
                        };
                        break;
                    case me.mode == 'line' && me.activeGeometry && me.activeGeometry.type == 'line':
                        addLine(me.activeGeometry.x1, me.activeGeometry.y1, offsets.offsetX, offsets.offsetY);
                        me.activeGeometry = null;
                        break;
                    case me.mode == 'polygon' && ! me.activeGeometry: // Create Polygon
                        me.activeGeometry = {
                            type: 'polygon',
                            done: false,
                            coords: [
                                {
                                    x: offsets.offsetX,
                                    y: offsets.offsetY
                                },
                                {
                                    x: offsets.offsetX,
                                    y: offsets.offsetY
                                }
                            ]
                        };
                        break;
                    case me.mode == 'polygon' && me.activeGeometry && me.activeGeometry.type == 'polygon': 
                        me.activeGeometry.coords.push({
                            x: offsets.offsetX,
                            y: offsets.offsetY
                        });
                        break;
                    case me.mode == 'text' && ! me.activeGeometry:
                        me.activeGeometry = {
                            type: 'text',
                            size: me.settings.font.size,
                            family: me.settings.font.family,
                            color: me.settings.font.color,
                            x: offsets.offsetX,
                            y: offsets.offsetY,
                            chars: []
                        }
                        break;
                    case me.mode == 'text' && me.activeGeometry && me.activeGeometry.type == 'text':
                        me.geometry.push(me.activeGeometry);
                        me.activeGeometry = null;
                        break;
                    // case me.activeGeometry && me.activeGeometry.type != me.mode:
                    //     me.geometry.push(me.activeGeometry);
                    //     me.activeGeometry = null;
                    //     break;
                }
            } else if (event.which == 3) { // Right Click
                // Get Last Geometry
                var isDrawing = me.activeGeometry != null; // APakah sedang menggambar??
                if (! me.activeGeometry) {
                    me.activeGeometry = me.geometry.pop();
                }

                if (me.activeGeometry) {
                    switch (true) {
                        case me.activeGeometry.type == 'paint':
                            me.activeGeometry = null;
                            break;
                        case me.activeGeometry.type == 'line' && isDrawing:
                            me.activeGeometry = null;
                            break;
                        case me.activeGeometry.type == 'polygon' && me.activeGeometry.coords.length > 2:
                            me.activeGeometry.coords.done = false;
                            me.activeGeometry.coords.pop();
                            break;
                        case me.activeGeometry.type == 'polygon' && me.activeGeometry.coords.length <= 2:
                            me.activeGeometry = null;
                            break;
                    }
                }
            }
        }

        me.redraw();
    });

    // Mouse Move
    this.canvas.mousemove(function (e) {
        console.log("MOUSE MOVE");
        if (! me.mode) 
            return;

        var arg = e;
        var ctx = me.canvas.get(0).getContext("2d");
        var offsets = getOffsets(e);
        switch (true) {
            case me.mode == 'paint' && me.buttonDown:
                addPoint(offsets.offsetX, offsets.offsetY, true);
                break;
            case me.mode == 'line' && me.activeGeometry && me.activeGeometry.type == 'line':
                me.activeGeometry.x2 = offsets.offsetX;
                me.activeGeometry.y2 = offsets.offsetY;
                break;
            case me.mode == 'polygon' && me.activeGeometry && me.activeGeometry.type == 'polygon':
                var coord = {
                    x: offsets.offsetX,
                    y: offsets.offsetY
                };

                me.activeGeometry.coords.pop();
                me.activeGeometry.coords.push(coord);
                break;
            case me.mode == 'erase':
                if (me.buttonDown) {
                    var geometryArr = getObjectIntersection(offsets.offsetX, offsets.offsetY);

                    var idx;
                    for (var i = 0; i < geometryArr.length; i++) {
                        idx = me.geometry.indexOf(geometryArr[i]);
                        if (idx > -1) {
                            me.geometry.splice(idx, 1);
                        }
                    }

                    me.activeGeometry = {
                        type: 'point',
                        x: offsets.offsetX,
                        y: offsets.offsetY,
                        strokeStyle: 'rgba(150, 150, 150, 1)',
                        lineJoin: "round",
                        lineWidth: 20
                    };
                } else {
                    me.activeGeometry = {
                        type: 'point',
                        x: offsets.offsetX,
                        y: offsets.offsetY,
                        strokeStyle: 'rgba(150, 150, 150, 0.4)',
                        lineJoin: "round",
                        lineWidth: 20
                    };
                }
                break;
        }

        me.redraw();
    });

    // On Key Press
    $(window).keypress(function (e) {
        if (me.mode == 'text') {
            var char = {
                code: e.which, 
            }

            if (me.activeGeometry) {
                me.activeGeometry.chars.push(char);
            }
        }

        me.redraw();

        if (e.which == 32 && me.mode == 'text' && me.activeGeometry) { // Prevent Space
            return false;
        }
    });
    $(window).keyup(function (e) {
        if (me.mode == 'text') {
            if (me.activeGeometry && me.activeGeometry.type == 'text') {
                switch (true) {
                    case e.which == 8: // Backspace
                        me.activeGeometry.chars.pop();
                        break;
                }

                me.redraw();
            }

            if (e.which == 32) { // Prevent Space
                return false;
            }
        }
    });

    // FUNCTIONS
    // Add Points
    function addPoint(x, y, dragging) {
        me.geometry.push({
            type: 'point',
            x: x,
            y: y,
            dragging: dragging,
            strokeStyle: me.settings.point.color,
            lineJoin: "round",
            lineWidth: me.settings.point.size
        });
    }

    function addLine(x1, y1, x2, y2) {
        me.geometry.push({
            strokeStyle: me.settings.line.color,
            lineJoin: "round",
            lineWidth: me.settings.line.size,
            type: 'line',
            x1: x1,
            y1: y1,
            x2: x2,
            y2: y2
        });
    }

    function addPolygon(geometry) {
        geometry.fillStyle = me.settings.polygon.foreground;
        geometry.strokeStyle = me.settings.polygon.color;
        me.geometry.push(geometry);
    }

    function renderGeometry(canvas, ctx, geometry, prevGeometry, nextGeometry, active) {
        switch (geometry.type) {
            case 'line':
                renderLine(canvas, ctx, geometry);
                break;
            case 'point':
                renderPoint(canvas, ctx, geometry, prevGeometry);
                break;
            case 'polygon':
                renderPolygon(canvas, ctx, geometry);
                break;
            case 'text':
                renderText(canvas, ctx, geometry, active);
                break;
        }
    }

    function renderLine(canvas, ctx, geometry) {
        ctx.strokeStyle = geometry.strokeStyle;
        ctx.lineJoin = geometry.lineJoin;
        ctx.lineWidth = geometry.lineWidth;
        ctx.beginPath();
        ctx.moveTo(geometry.x1, geometry.y1);
        ctx.lineTo(geometry.x2, geometry.y2);
        ctx.stroke();
    }

    function renderPoint(canvas, ctx, geometry, prevGeometry) {
        ctx.strokeStyle = geometry.strokeStyle;
        ctx.lineJoin = geometry.lineJoin;
        ctx.lineWidth = geometry.lineWidth;
        ctx.beginPath();
        // if (geometry.dragging && prevGeometry && prevGeometry.type == 'point') {
            // ctx.moveTo(prevGeometry.x, prevGeometry.y);
        // } else {
            ctx.moveTo(geometry.x-1, geometry.y);
        // }
        ctx.lineTo(geometry.x, geometry.y);
        ctx.closePath();
        ctx.stroke();
    }

    function renderPolygon(canvas, ctx, geometry) {
        ctx.fillStyle = geometry.fillStyle;
        ctx.strokeStyle = geometry.strokeStyle;

        ctx.beginPath();
        var coord;
        for (var i = 0; i < geometry.coords.length; i++) {
            coord = geometry.coords[i];
            if (i == 0) {
                ctx.moveTo(coord.x, coord.y);
            } else {
                ctx.lineTo(coord.x, coord.y);
            }
        }

        if (geometry.done) {
            ctx.closePath();

            if (me.settings.polygon.fill) {
                ctx.fill();
            }
        }

        ctx.stroke();
    }

    function renderText(canvas, ctx, geometry, active) {
        ctx.font = geometry.size + " " + geometry.family;
        ctx.textBaseline = "bottom";
        ctx.textAlign = "left";
        ctx.fillStyle = geometry.color;

        var lastPos = [geometry.x, geometry.y]; 
        var char, textMTX;
        for (var i = 0; i < geometry.chars.length; i++) {
            char = geometry.chars[i];
            if (char.code == 13) { // New Line
                lastPos[0] = geometry.x;
                lastPos[1] = lastPos[1] + parseFloat(geometry.size);
            } else {
                ctx.fillText(String.fromCharCode(char.code), lastPos[0], lastPos[1]);
                textMTX = ctx.measureText(String.fromCharCode(char.code));
                lastPos[0] += textMTX.width + 1;
            }
        }

        if (active) {
            ctx.fillText('_', lastPos[0], lastPos[1]);
        }
    }

    function getObjectIntersection(x, y, size) {
        size = size || 5;
        var geometryArr = []
        for (var i = 0; i < me.geometry.length; i++) {
            if (isCollide(x, y, size, me.geometry[i])) {
                geometryArr.push(me.geometry[i]);
            }
        }

        return geometryArr;
    }

    function isCollide(x, y, size, geometry) {
        switch (true) {
            case geometry.type == 'line':
                // TODO
                break;
            case geometry.type == 'polygon':
                break;
            case geometry.type == 'point':
                var x1 = geometry.x - geometry.lineWidth/2;
                var x2 = geometry.x + geometry.lineWidth/2;
                var y1 = geometry.y - geometry.lineWidth/2;
                var y2 = geometry.y + geometry.lineWidth/2;
                if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                    return true;
                }

                break;
            case geometry.type == 'text':
                var ctx = me.canvas.get(0).getContext("2d");
                var x1 = geometry.x;
                var y1 = geometry.y;
                var x2 = geometry.x;
                var y2 = geometry.y;
                var lastPos = [geometry.x, geometry.y];
                var char, textMTX;
                for (var i = 0; i < geometry.chars.length; i++) {
                    char = geometry.chars[i];
                    if (char.code == 13) { // New Line
                        lastPos[0] = geometry.x;
                        lastPos[1] = lastPos[1] + parseFloat(geometry.size);
                    } else {
                        textMTX = ctx.measureText(String.fromCharCode(char.code));
                        lastPos[0] += textMTX.width + 1;
                    }

                    x2 = x2 > lastPos[0] ? x2 : lastPos[0];
                    y2 = y2 > lastPos[1] ? y2 : lastPos[1];
                }

                if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                    return true;
                }

                break;
        }

        return false;
    }

    function getOffsets(evt) {
        var offsetX, offsetY;
        if (typeof evt.offsetX != 'undefined') {
            offsetX = evt.offsetX;
            offsetY = evt.offsetY;
        } else if (typeof evt.layerX != 'undefined') {
            offsetX = evt.layerX;
            offsetY = evt.layerY;
        }

        return { 'offsetX': offsetX, 'offsetY': offsetY };
    }
}

StatusLokalis.prototype.toDataURL = function () {
    return this.canvas.get(0).toDataURL();
}

StatusLokalis.prototype.setMode = function(mode) {
    var oldMode = this.mode;
    this.mode = mode;
    var cur;
    switch (this.mode) {
        case 'text':
            cur = 'text';
            break;
        case 'paint':
        case 'polygon':
            cur = 'crosshair';
            break;
        case 'erase':
            cur = 'none';
            break;
        default:
            cur = 'default';
            break;

    }
    var localCanvas = this.canvas;

    switch (oldMode) {
        case 'text':
            if (this.activeGeometry) {
                this.geometry.push(this.activeGeometry);
                this.activeGeometry = null;
            }
            break;
        case 'paint':
            break;
        case 'polygon':
        case 'erase':
        default:
            this.activeGeometry = null;
            break;
    }

    this.redraw();
    this.canvas.mouseover(function () {
        localCanvas.css('cursor', cur);
    });
};

StatusLokalis.prototype.clear = function() {
    this.geometry = [];
    this.activeGeometry = null;
    this.redraw();
}

StatusLokalis.prototype.setBackground = function (image, x, y, w, h) {
    this.background = {
        image: image,
        x: x,
        y: y,
        w: w,
        h: h
    };

    this.redraw();
}

StatusLokalis.prototype.setOption = function (options) {
    this.settings = $.extend(this.settings, options);
}